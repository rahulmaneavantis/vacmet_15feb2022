﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AddPreRequisite : System.Web.UI.Page
    {
        protected bool ChckDocument = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProcessAudits();
                bindPageNumber();
                BindProcess(Convert.ToInt32(AuthenticationHelper.CustomerID));
            }
        }

        private void BindProcess(int CustomerID)
        {
            try
            {
                int auditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    var data = FillProcessDropdownPerformerAndReviewer(CustomerID, auditID);

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();

                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = data;
                    ddlProcess.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(List<long> Processid)
        {
            try
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    var data = ProcessManagement.FillSubProcessListUserWise(Processid, "P", AuditID, AuthenticationHelper.UserID);
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = data;
                    ddlSubProcess.DataBind();
                    // ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static object FillProcessDropdownPerformerAndReviewer(int Customerid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.AuditID == AuditID
                         && row.UserID == AuthenticationHelper.UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        public void BindProcessAudits()
        {
            try
            {
                int customerID = -1;
                List<long> processid = new List<long>();
                List<long> subprocessid = new List<long>();
                List<int> DocumentRequire = new List<int>();
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int AuditID = 0;
                string TypeToSearch = string.Empty;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        for (int i = 0; i < ddlProcess.Items.Count; i++)
                        {
                            if (ddlProcess.Items[i].Selected)
                            {
                                processid.Add(Convert.ToInt32(ddlProcess.Items[i].Value));
                            }
                        }
                        for (int i = 0; i < ddlSubProcess.Items.Count; i++)
                        {
                            if (ddlSubProcess.Items[i].Selected)
                            {
                                subprocessid.Add(Convert.ToInt32(ddlSubProcess.Items[i].Value));
                            }
                        }
                        for (int i = 0; i < ddlDocRequire.Items.Count; i++)
                        {
                            if (ddlDocRequire.Items[i].Selected)
                            {
                                DocumentRequire.Add(Convert.ToInt32(ddlDocRequire.Items[i].Value));
                            }
                        }
                        if (!string.IsNullOrEmpty(tbxTypeToSearch.Text))
                        {
                            TypeToSearch = tbxTypeToSearch.Text.Trim();
                        }

                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);

                        var query = (from row in entities.SP_AddPrerequisiteData(AuditID, AuthenticationHelper.UserID)
                                     select row).ToList();

                        grdProcessAudit.DataSource = null;
                        grdProcessAudit.DataBind();

                        if (query != null)
                        {
                            if (processid.Count > 0)
                            {
                                query = query.Where(entry => processid.Contains(entry.ProcessId)).ToList();
                            }
                            if (subprocessid.Count > 0)
                            {
                                query = query.Where(entry => subprocessid.Contains((long)entry.SubProcessId)).ToList();
                            }
                            if (!string.IsNullOrEmpty(TypeToSearch))
                            {
                                query = query.Where(entry => entry.AuditStep.Contains(TypeToSearch) || entry.ProcessName.Contains(TypeToSearch) || entry.SubProcessName.Contains(TypeToSearch)).ToList();
                            }
                            if (DocumentRequire.Count > 0)
                            {
                                if (DocumentRequire.Count == 1)
                                {
                                    if (DocumentRequire.Contains(2))
                                    {
                                        query = query.Where(entry => entry.DocumentAvailable == 0).ToList();
                                    }
                                    else
                                    {
                                        query = query.Where(entry => entry.DocumentAvailable != 0).ToList();
                                    }
                                }
                            }

                            grdProcessAudit.DataSource = query;
                            Session["TotalRows"] = null;
                            Session["TotalRows"] = query.Count;
                            grdProcessAudit.DataBind();
                            if (query.Count > 0)
                                btnSave.Visible = true;
                            else
                                btnSave.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                    ListItem removeItem = DropDownListPageNo.Items.FindByText("0");
                    DropDownListPageNo.Items.Remove(removeItem);
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> processid = new List<long>();
            for (int i = 0; i < ddlProcess.Items.Count; i++)
            {
                if (ddlProcess.Items[i].Selected)
                {
                    processid.Add(Convert.ToInt32(ddlProcess.Items[i].Value));
                }
            }
            BindSubProcess(processid);
            BindProcessAudits();
            bindPageNumber();
        }

        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudits();
            bindPageNumber();
        }

        protected void ddlDocRequire_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudits();
            bindPageNumber();
            List<int> DocumentRequire = new List<int>();
            for (int i = 0; i < ddlDocRequire.Items.Count; i++)
            {
                if (ddlDocRequire.Items[i].Selected)
                {
                    DocumentRequire.Add(Convert.ToInt32(ddlDocRequire.Items[i].Value));
                }
            }
            if (DocumentRequire.Count > 0)
            {
                if (DocumentRequire.Count == 1)
                {
                    if (DocumentRequire.Contains(2))
                    {
                        ChckDocument = false;
                    }
                    else
                    {
                        ChckDocument = true;
                    }
                }
            }
        }

        protected void tbxTypeToSearch_TextChanged(object sender, EventArgs e)
        {
            BindProcessAudits();
            bindPageNumber();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindProcessAudits();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessAudit.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSizeView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int ATBDID = 0;
                GridViewChecklist.PageSize = Convert.ToInt32(ddlPageSizeView.SelectedValue);
                
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ATBDID"])))
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var Result = (from row in entities.StepChecklistMappings
                                      where row.ATBDID == ATBDID
                                      && row.IsActive == true
                                      select row).ToList();

                        GridViewChecklist.DataSource = Result;
                        GridViewChecklist.DataBind();
                    }
                }
                
                upEmditChecklist.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvChecklist.IsValid = false;
                cvChecklist.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSizeAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GrdAddChecklist.PageSize = Convert.ToInt32(ddlPageSizeAdd.SelectedValue);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvCheckListAdd.IsValid = false;
                CvCheckListAdd.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void GrdAddChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Add_CheckList"))
            {
                GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int RowIndex = gvr.RowIndex;
                int RowCount = GrdAddChecklist.Rows.Count;
                if (RowCount < 5)
                {
                    BindGrdAddChecklist(RowIndex, "ADD");
                }
            }
            else if (e.CommandName.Equals("Delete_CheckList"))
            {
                GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int RowIndex = gvr.RowIndex;
                BindGrdAddChecklist(RowIndex, "Delete");
            }
        }

        protected void GridViewChecklist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                AuditControlEntities entities = new AuditControlEntities();
                TextBox tbxDocumentName = (e.Row.FindControl("tbxDocumentName") as TextBox);
                Label lblID = (e.Row.FindControl("lblID") as Label);
                if (!string.IsNullOrEmpty(tbxDocumentName.Text))
                {
                    int ChecklistId = Convert.ToInt32(lblID.Text);
                    var result = (from row in entities.Master_AuditPrerequisite
                                  where row.ChecklistId == ChecklistId
                                  select row).ToList();
                    if (result.Count > 0)
                    {
                        tbxDocumentName.Enabled = false;
                    }
                }
            }
        }

        protected void grdProcessAudit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkEditChecklist = (e.Row.FindControl("lnkEditChecklist") as LinkButton);
                if (lnkEditChecklist.Text == "0")
                {
                    lnkEditChecklist.Enabled = false;
                }
            }
        }

        protected void grdProcessAudit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Add_CheckList"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        ViewState["AuditID"] = Convert.ToInt32(commandArgs[0]);
                        ViewState["ATBDID"] = Convert.ToInt32(commandArgs[1]);
                        int AuditID = Convert.ToInt32(commandArgs[0]);
                        int ATBDID = Convert.ToInt32(commandArgs[1]);
                        BindGrdAddChecklist(-1, "ADD");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowADDDOCDialog(" + AuditID + "," + ATBDID + ");", true);
                        upAddChecklist.Update();
                    }
                }
                else if (e.CommandName.Equals("Edit_CheckList"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        ViewState["AuditID"] = Convert.ToInt32(commandArgs[0]);
                        ViewState["ATBDID"] = Convert.ToInt32(commandArgs[1]);
                        int AuditID = Convert.ToInt32(commandArgs[0]);
                        int ATBDID = Convert.ToInt32(commandArgs[1]);
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var Result = (from row in entities.StepChecklistMappings
                                          where row.ATBDID == ATBDID
                                          && row.IsActive == true
                                          select row).ToList();

                            GridViewChecklist.DataSource = Result;
                            GridViewChecklist.DataBind();
                            upEmditChecklist.Update();
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowEditDOCDialog(" + AuditID + "," + ATBDID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something Went Wrong.";
            }
        }

        protected void BindGrdAddChecklist(int RowIndex, string Type)
        {
            try
            {
                DataTable dtTable = new DataTable();
                dtTable.Columns.Add("ChecklistDocument", typeof(string));
                if (Type == "ADD")
                {
                    if (RowIndex == -1)
                    {
                        dtTable.Rows.Add("");
                    }
                    else
                    {
                        for (int i = 0; i < GrdAddChecklist.Rows.Count; i++)
                        {
                            TextBox tbxDocumentName = ((TextBox)GrdAddChecklist.Rows[i].FindControl("tbxDocumentName"));

                            if (i == RowIndex)
                            {
                                dtTable.Rows.Add(tbxDocumentName.Text);
                                dtTable.Rows.Add("");
                            }
                            else
                            {
                                dtTable.Rows.Add(tbxDocumentName.Text);
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < GrdAddChecklist.Rows.Count; i++)
                    {
                        TextBox tbxDocumentName = ((TextBox)GrdAddChecklist.Rows[i].FindControl("tbxDocumentName"));

                        if (i == RowIndex)
                        {
                            if (RowIndex == 0)
                            {
                                dtTable.Rows.Add("");
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            dtTable.Rows.Add(tbxDocumentName.Text);
                        }
                    }
                }
                GrdAddChecklist.DataSource = dtTable;
                Session["AddChecklistTotalRows"] = dtTable.Rows.Count;
                GrdAddChecklist.DataBind();
                
                int RowCount = GrdAddChecklist.Rows.Count;
                if (RowCount >= 5)
                {
                    for (int i = 0; i < GrdAddChecklist.Rows.Count; i++)
                    {
                        LinkButton lnkAddChk = (GrdAddChecklist.Rows[i].FindControl("lnkAddChk") as LinkButton);
                        lnkAddChk.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvChecklist.IsValid = false;
                cvChecklist.ErrorMessage = "Something Went Wrong.";
            }
        }

        protected void btnAddChecklist_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ATBDID"])))
                {
                    bool Checkresult = false;
                    for (int i = 0; i < GrdAddChecklist.Rows.Count; i++)
                    {
                        TextBox tbxDocumentName = ((TextBox)GrdAddChecklist.Rows[i].FindControl("tbxDocumentName"));
                        if (!string.IsNullOrEmpty(tbxDocumentName.Text))
                        {
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                int ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                                var result = (from row in entities.StepChecklistMappings
                                              where row.ATBDID == ATBDID
                                              && row.ChecklistDocument.ToLower() == tbxDocumentName.Text.Trim().ToLower()
                                              select row).FirstOrDefault();
                                if (result == null)
                                {
                                    StepChecklistMapping objStepChecklistMapping = new StepChecklistMapping();
                                    objStepChecklistMapping.ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                                    objStepChecklistMapping.ChecklistDocument = tbxDocumentName.Text.Trim();
                                    objStepChecklistMapping.IsActive = true;
                                    entities.StepChecklistMappings.Add(objStepChecklistMapping);
                                    entities.SaveChanges();
                                    Checkresult = true;
                                }
                            }
                        }
                    }
                    if (Checkresult)
                    {
                        CvCheckListAdd.IsValid = false;
                        CvCheckListAdd.ErrorMessage = "Record Saved Successfully.";
                    }
                    else
                    {
                        CvCheckListAdd.IsValid = false;
                        CvCheckListAdd.ErrorMessage = "Record Already Exist or Something Went Wrong.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvCheckListAdd.IsValid = false;
                CvCheckListAdd.ErrorMessage = "Something Went Wrong.";
            }
        }

        protected void btnUpdateCheckList_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ATBDID"])))
                {
                    bool Checkresult = false;
                    for (int i = 0; i < GridViewChecklist.Rows.Count; i++)
                    {
                        TextBox tbxDocumentName = ((TextBox)GridViewChecklist.Rows[i].FindControl("tbxDocumentName"));
                        Label lblID = ((Label)GridViewChecklist.Rows[i].FindControl("lblID"));
                        if (!string.IsNullOrEmpty(tbxDocumentName.Text))
                        {
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                int ChecklistID = Convert.ToInt32(lblID.Text);
                                var result = (from row in entities.StepChecklistMappings
                                              where row.ID == ChecklistID
                                              select row).FirstOrDefault();
                                if (result != null)
                                {
                                    result.ChecklistDocument = tbxDocumentName.Text.Trim();
                                    result.IsActive = true;
                                    entities.SaveChanges();
                                    Checkresult = true;
                                }
                            }
                        }
                    }
                    if (Checkresult)
                    {
                        cvChecklist.IsValid = false;
                        cvChecklist.ErrorMessage = "Record Updated Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvChecklist.IsValid = false;
                cvChecklist.ErrorMessage = "Something Went Wrong.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcessAudit.PageIndex = chkSelectedPage - 1;
            grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindProcessAudits();
        }

        protected void GrdAddChecklist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAddChk = (e.Row.FindControl("lnkAddChk") as LinkButton);
                int RowCount = GrdAddChecklist.Rows.Count;
                if (RowCount >= 5)
                {
                    lnkAddChk.Enabled = false;
                }
            }
        }
    }
}