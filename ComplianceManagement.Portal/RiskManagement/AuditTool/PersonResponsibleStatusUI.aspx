﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="PersonResponsibleStatusUI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.PersonResponsibleStatusUI" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
                $('#divbtn').css('display', 'none');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Observation';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Observation';
                }
                else if (filterbytype == 'O') {
                    filterbytype = 'Open Observation';
                }
                else if (filterbytype == 'S') {
                    filterbytype = 'Submited Observation';
                }
                else if (filterbytype == 'C') {
                    filterbytype = 'Closed Observation';
                }
                //fhead('My Workspace/ ' + filterbytype);
                fhead('My Workspace/ Observation');
                $('#divbtn').css('display', 'none');
            }
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
            $('#divbtn').css('display','none')
        });

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.

            $(function () {
                var startDate = new Date(); // added by sagar more on 20-01-2020
                $('input[id*=tbxTimeLine]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate, //added by sagar more on 20-01-2020
                    });
            });

            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                });
            });
        }

        function ShowDialog(atbdid, custbranchid, FinancialYear, Forperiod, VID, AuditStatusID, AuditID, AuditName) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditControlPersonResponsible.aspx?FinYear=" + FinancialYear + "&ForMonth=" + Forperiod + "&ATBDID=" + atbdid + "&BID=" + custbranchid + "&VID=" + VID + "&AuditStatusID=" + AuditStatusID + "&AuditID=" + AuditID + "&AuditName=" + AuditName);
        };

        function ShowIMPDialog(resultid, custbranchid, FinancialYear, Forperiod, VID, scheduledonid, AuditStatusID, AuditID) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '1000px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#showdetails').attr('src', "../AuditTool/IMPStatusAuditPersonResponsible.aspx?FinYear=" + FinancialYear + "&ForMonth=" + Forperiod + "&ResultID=" + resultid + "&BID=" + custbranchid + "&VID=" + VID + "&scheduledonid=" + scheduledonid + "&AuditStatusID=" + AuditStatusID + "&AuditID=" + AuditID);
        };

        function checkAll(objRef) {
            var selectedRowCount = 0;
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                        selectedRowCount++;
                    }
                    else {

                        inputList[i].checked = false;
                        selectedRowCount--;
                    }
                }
            }
            if (selectedRowCount > 0) {
                $('#divbtn').css('display', 'block')
            }
            else {
                $('#divbtn').css('display', 'none')
            }
        }

        function Check_Click(objRef) {
            var selectedRowCount = 0;
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");

            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {            
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }
            if (selectedRowCount > 0) {
                $('#divbtn').css('display', 'block')
            }
            else {
                $('#divbtn').css('display', 'none')
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upAuditStatusUI" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                                <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">                                           
                                        <li class="active" id="liProcess" runat="server">
                                            <asp:LinkButton ID="lnkProcess" OnClick="ShowProcessGrid" CausesValidation="false" runat="server">Audit</asp:LinkButton>                                           
                                        </li>
                                          
                                        <li class=""  id="liImplementation" runat="server">
                                            <asp:LinkButton ID="lnkImplementation" OnClick="ShowImplementationGrid" CausesValidation="false" runat="server">Implementation</asp:LinkButton>                                        
                                        </li>                                        
                                    </ul>
                                </header>
                              
                                 <div class="clearfix"></div> 

                            <div class="panel-body">

                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                   <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 20%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True"/>
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    </div>  
                                     
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit" 
                                           class="form-control m-bot15 select_location" Style="float: left; width:90%;" Width="90%"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"    AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div> 
                                     
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Style="float: left; width:90%;" Width="90%"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"     AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>
                                     
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" 
                                        class="form-control m-bot15 select_location" Style="float: left; width:90%;" Width="90%"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                      
                                </div>
                                     
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" 
                                         class="form-control m-bot15 select_location" Style="float: left; width:90%;" Width="90%"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"     AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>                      
                                 </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">                                       
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" Width="90%" DataPlaceHolder="Location"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div> 
                                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" Width="90%"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"      class="form-control m-bot15 select_location" Style="float: left; width:90%;" DataPlaceHolder="Financial Year"
                                        OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ErrorMessage="Please Select Financial Year." ControlToValidate="ddlFinancialYear"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" />                    
                                    </div>    
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" Width="90%" DataPlaceHolder="Scheduling Type"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div>
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" DataPlaceHolder="Period"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" Width="90%" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div>
                                       <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                     <%{%>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                            <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" Width="90%" DataPlaceHolder="Vertical"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3"      OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;" >
                                            </asp:DropDownListChosen>  
                                        </div>   
                                     <%}%>   
                                </div> 
                                </div>  
                            <div class="clearfix"></div> 
                             <div class="clearfix"></div>                                                                 
                                <div style="margin-bottom: 4px">                                                                            
                                       <asp:GridView runat="server" ID="grdDrillDown" AutoGenerateColumns="false" AllowSorting="true" 
                                       ShowHeaderWhenEmpty="true" PageSize="20" OnPageIndexChanging="grdDrillDown_PageIndexChanging" OnRowCommand="grdDrillDown_RowCommand"
                                       CssClass="table" GridLines="None" AllowPaging="true" Width="100%" DataKeyNames="ATBDID"
                                       onrowdatabound="grdDrillDown_RowDataBound">                    
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                         <HeaderTemplate>
                                                                                           <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);" />
                                                                                           </HeaderTemplate>
                                                                                           <ItemTemplate>
                                                                                           <asp:CheckBox ID="CheckBox1" runat="server" onclick = "Check_Click(this)" />
                                                                                           </ItemTemplate>
                                                                                         </asp:TemplateField> 
                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" Visible="false">
                                                                                        <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Audit">
                                                                                        <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; width: 250px;">
                                                                                        <asp:Label ID="lblAuditName" runat="server" Text='<%# Eval("AuditName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                                                        </div>
                                                                                        </ItemTemplate>
                                                                                        </asp:TemplateField> 
                                                                                        <asp:TemplateField HeaderText="Observation">
                                                                                        <ItemTemplate>
                                                                                            <div class="text_NlinesusingCSS" style="width: 200px;"> 
                                                                                        <asp:Label ID="lblObservation" runat="server" Text='<%# ShowObservation((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>' 
                                                                                            tooltip='<%# ShowObservation((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>'  data-toggle="tooltip" data-placement="top"></asp:Label>
                                                                                        </div>
                                                                                                </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Management Response">
                                                                                        <ItemTemplate>      
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 285px;">                      
                                                                                        <asp:TextBox ID="tbxMgmtResponse" CssClass="form-control" runat="server" Text='<%# ShowMgmtResponse((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>' data-toggle="tooltip" data-placement="top"></asp:TextBox>
                                                                                        </div>
                                                                                        </ItemTemplate>
                                                                                        </asp:TemplateField>                      
                                                                                        <asp:TemplateField HeaderText="Time Line">
                                                                                        <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxTimeLine" runat="server"  AutoComplete="Off"  CssClass="form-control" Text='<%# ShowTimeline((int)Eval("ATBDID"),(int)Eval("VerticalId"),(long)Eval("ScheduledOnID"),(long)Eval("AuditID")) %>' data-toggle="tooltip" data-placement="top"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center"  HeaderText="Action">
                                                                                        <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnChangeStatus" runat="server"
                                                                                        CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ATBDID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth")+"," + Eval("VerticalID")+","+ Eval("AuditStatusID")+","+Eval("AuditID")+","+Eval("ScheduledOnID")+","+Eval("Status") %>' OnClick="btnChangeStatus_Click">
                                                                                        <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" data-toggle="tooltip" data-placement="top" title="Change Status" /></asp:LinkButton>                                                                                                                                                                                                                                                                                   
                                                                                                      </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                                        </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                                                        <PagerSettings Visible="false" />                      
                                                                                        <PagerTemplate>
                                                                                        </PagerTemplate>
                                                                                        <EmptyDataTemplate>
                                                                                        No Records Found.
                                                                                        </EmptyDataTemplate> 
                                                                                        </asp:GridView>                                                                                                
                                </div>

                            <div style="margin-bottom:4px;">  
                                <asp:GridView runat="server" ID="grdImplementationAudits" AutoGenerateColumns="false"
                                             AllowSorting="true" ShowHeaderWhenEmpty="true" CssClass="table" GridLines="None"
                                            AllowPaging="true" PageSize="5" Width="100%" OnRowCommand="grdImplementationAudits_RowCommand"
                                             onrowdatabound="grdImplementationAudits_RowDataBound">  
                                            <Columns>
                                            <asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; width: 200px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("AuditName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Total Observation" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ null %>'
                                                    Text='<%# Eval("Total") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Open Observations" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskNotDone" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "NotDone;"+ Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ null %>'
                                                    Text='<%# Eval("NotDone") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Submitted" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeSubmit;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+"AS" %>'
                                                    Text='<%# Eval("AuditeeSubmit") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Review Comment" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeSubmit" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+"PA" %>'
                                                    Text='<%# Eval("AuditeeReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Re-submitted" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeReviewComment" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "ReviewComment;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+"AR" %>'
                                                    Text='<%# Eval("ReviewComment") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Review 2" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Submitted;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ "PS"  %>'
                                                    Text='<%# Eval("Submited") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Team Review" ItemStyle-HorizontalAlign="Center" Visible="false"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTeamReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "TeamReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ null  %>'
                                                    Text='<%# Eval("TeamReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Review 3" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblFinalReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "FinalReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ "RS"  %>'
                                                    Text='<%# Eval("FinalReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Closed" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskClosed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Closed;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ "AC" %>'
                                                    Text='<%# Eval("Closed") %>' ></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <HeaderStyle BackColor="#ECF0F1" />                    
                                            <PagerTemplate>                                     
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                            No Records Found.
                                            </EmptyDataTemplate>                                         
                                       </asp:GridView>
                            </div>
                              
                            <div class="col-md-12 colpadding0">
                                   <div style="float: right;">
                                      <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                          class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                      </asp:DropDownListChosen>  
                                    </div>
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                          <%--  <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12" >
                                <div style="float:left; display:none;" id="divbtn">
                                <asp:Button Text="Save" runat="server" ID="btnAllSavechk" OnClick="btnAllSavechk_Click" CssClass="btn btn-search" />
                                </div>
                                    <div class="col-md-6" style="display:none;">
                                    <label style="background-color:blue">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <label>Blue means Auditee need to take action</label>
                                </div>
                                <div class="col-md-6" style="display:none;">
                                    <label style="background-color:black">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <label>Black means Auditee submited response</label>
                                </div>
                            </div>           
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
