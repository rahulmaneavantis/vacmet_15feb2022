﻿<%@ Page Title="Audit Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true"
    CodeBehind="MyReportAudit.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.MyReportAudit" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <%--../Images/icon-updated.png--%>
    <style type="text/css">
        .btuploadss {
            background-image: url(../../Images/icon-updated.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }

        .btdownss {
            background-image: url(../../Images/icon-download.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }

        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        /**, :after, :before {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            overflow: hidden;
        }*/
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            //setactivemenu('leftdocumentmenu');
            fhead('Final Audit Report');
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget" style="padding: 0px;">
                <div class="dashboard">
                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <section class="panel"> 
                               <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                                <li class="active" id="liPerformer" runat="server">
                                                    <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                                </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                                <li class=""  id="liReviewer" runat="server">
                                                    <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                                </li>
                                          <%}%>   
                                           <%if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")%>
                                           <%{%>
                                                <li class=""  id="liAuditHead" runat="server">
                                                    <asp:LinkButton ID="lnkAuditHead" OnClick="ShowAuditHead" runat="server">Audit Head</asp:LinkButton>                                        
                                                </li>
                                          <%}%>  
                                           <%if (DepartmentHead)%>
                                           <%{%>
                                                <li class=""  id="liDepartmentHead" runat="server">
                                                    <asp:LinkButton ID="lnkDepartmentHead" OnClick="ShowDepartmentHead" runat="server">Department Head</asp:LinkButton>                                        
                                                </li>
                                          <%}%> 
                                         <% if (AuditHeadOrManagerReport == null)%>
                                         <%{%>
                                               <%if (!DepartmentHead)%>
                                               <%{%>
                                                  <%if (!ManagementFlag)%>
                                                  <%{%>
                                          
                                                        <li style="float: right;">   
                                                            <div style="margin-right: 25px;">                                                        
                                                                    <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                                    <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                    <ul class="dropdown-menu">                                                                                                                                                                                                                                                               
                                                                        <%--<li><a href="../InternalAuditTool/AuditObservationDraft.aspx">Draft Observation</a></li>                                                                    
                                                                        <li><a href="../InternalAuditTool/FrmObservationReportWord.aspx">Observation-Word</a></li>--%>
                                                                        <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation-Excel</a></li>
                                                                        <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                        <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li>
                                                                        <li><a href="../AuditTool/PreRequsiteLogReport.aspx">Pre-Requsite Log Report</a></li>
                                                                        <li><a href="../AuditTool/ManagementResponseLogReport.aspx">Management Response Log Report</a></li>
                                                                        <li><a href="../InternalAuditTool/PastAuditUpload.aspx">Past Audit Reports</a></li>     
                                                                        <%--<li><a href="../InternalAuditTool/FrmMasterAuditObservationReport.aspx">Audit Observation</a></li>                                                    
                                                                        <li><a href="../InternalAuditTool/FrmInternalAuditReportInWord.aspx">Internal Observation Word</a></li>                                                                                                      
                                                                        <li><a href="../InternalAuditTool/InternalAuditReportInDocument.aspx">Internal Observation Revised</a></li> --%>                                                      
                                                                    </ul>                                                       
                                                            </div>
                                                        </li>  
                                                    <%}%> 
                                              <%}%> 
                                        <%}%>                                 
                                    </ul>
                                </header>
                                <div class="clearfix"></div>                                                                    
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"
                                             class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" 
                                            class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" 
                                            class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" 
                                             class="form-control m-bot15" Width="90%" Height="32px" AutoPostBack="true"  AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" 
                                            DataPlaceHolder="Sub Unit"  AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                            class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" AutoPostBack="true"
                                             class="form-control m-bot15" Width="90%" Height="32px">                                    
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true"
                                            DataPlaceHolder="Scheduling Type" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                             OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                            class="form-control m-bot15" Width="90%" Height="32px">                                                                               
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true"
                                             OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" DataPlaceHolder="Period" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                           class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>    
                                    <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                    <%{%>                             
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true"
                                             class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Vertical" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>  
                                    <%}%>                               
                                </div>
                                <div class="clearfix"></div>
                                 <asp:Panel ID="rptpnl" runat="server" style="overflow-x:scroll; overflow-y:hidden">
                                    <div style="margin-top:3%;">                                   
                                        <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                            CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%" Font-Size="12px" 
                                            ShowHeaderWhenEmpty="true"
                                            onrowdatabound="grdSummaryDetailsAuditCoverage_RowDataBound"
                                             OnRowCommand="grdSummaryDetailsAuditCoverage_RowCommand">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" Visible="false" ItemStyle-Width="15px"> 
                                                <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                                                                                
                                                <asp:TemplateField HeaderText="Location" >
                                                <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;">
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("HubName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("HubName") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 150px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("CProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>   
                                                <asp:TemplateField HeaderText="SubProcess">
                                                <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 150px;">
                                                <asp:Label ID="lblSubProcess" runat="server" Text='<%# Eval("SubProcess") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcess") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>                                       
                                                <asp:TemplateField HeaderText="FY" Visible="false"  ItemStyle-Width="10px"> 
                                                <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;">
                                                <asp:Label ID="lblFinYear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Period" Visible="false"  ItemStyle-Width="10px">
                                                <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("Period") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Period") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>  
                                   
                                                <asp:TemplateField ItemStyle-Width="15px" HeaderText="Draft Report">
                                                <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="updraft" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <asp:FileUpload ID="DraftFileUpload" Style="width: 185px;" runat="server"/>                                                                                                                
                                                                                                                                                
                                                <asp:Button ID="UploadDraft" runat="server" 
                                                OnClick="UploadDraft_Click"  CssClass="btuploadss"  data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID")  %>' CausesValidation="true" />                                                                                                               

                                                <asp:LinkButton ID="lblDocumentDownLoadDraftFile" runat="server" 
                                                    data-toggle="tooltip" data-placement="top" ToolTip="Download"
                                                    CssClass="btdownss" CommandName="DRDocumentDownLoadfile" 
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID") %>'></asp:LinkButton>                                                                                                              
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="UploadDraft" />
                                                <asp:PostBackTrigger ControlID="lblDocumentDownLoadDraftFile" />
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="15px" HeaderText="Final Deliverables">
                                                <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa3a" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <asp:FileUpload ID="FinalveriablesFileUpload" Style="width: 185px;" runat="server"/>                                                                                                
                                                <asp:Button ID="UploadFinalveriables" runat="server" 
                                                OnClick="btnUpload_Click"  CssClass="btuploadss"  data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID")  %>' CausesValidation="true" />                                                                                                               
                                                <asp:LinkButton ID="lblDocumentDownLoadfile1" runat="server" 
                                                    data-toggle="tooltip" data-placement="top" ToolTip="Download"
                                                    CssClass="btdownss" CommandName="FDDocumentDownLoadfile" 
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID") %>'></asp:LinkButton>                                                                                                              
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="UploadFinalveriables" />
                                                <asp:PostBackTrigger ControlID="lblDocumentDownLoadfile1" />
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="15px" HeaderText="Audit Committee"> <%--Audit Committee Presentation--%>
                                                <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa2a" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <asp:FileUpload ID="AuditcommitteeFileUpload" Style="width: 185px;" runat="server"/>                                                        
                                                <asp:Button ID="UploadAuditcommittee" runat="server"  CssClass="btuploadss" OnClick="btnACUpload_Click" 
                                                    data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID")  %>' CausesValidation="true" />                                                                                                               
                                                <asp:LinkButton ID="lblDocumentDownLoadfile" runat="server" 
                                                    CommandName="AuditCommitteeDocumentDownLoadfile" 
                                                CssClass="btdownss"  data-toggle="tooltip" data-placement="top" ToolTip="Download"
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID")  %>'></asp:LinkButton>                                                        
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDocumentDownLoadfile" />
                                                <asp:PostBackTrigger ControlID="UploadAuditcommittee" />
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                </ItemTemplate>
                                                </asp:TemplateField>  
                                                
                                              <%--  <asp:TemplateField ItemStyle-Width="15px" HeaderText="Final Annexure Document">--%>
                                                <asp:TemplateField ItemStyle-Width="15px" HeaderText="Feedback Form">
                                                <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa4a" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <asp:FileUpload ID="FeedbackFileUpload" Style="width: 185px;" runat="server"/>                                                     
                                                <asp:Button ID="UploadFeedback" runat="server"
                                                data-toggle="tooltip" data-placement="top" ToolTip="Upload" CssClass="btuploadss" 
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID")  %>' 
                                                CausesValidation="true" OnClick="UploadFeedback_Click"/>                                                                                                               
                                                <asp:LinkButton ID="lblFeedbackDownLoadfile1" runat="server" 
                                                data-toggle="tooltip" data-placement="top" ToolTip="Download" CssClass="btdownss"
                                                    CommandName="FeedBackFormDownload" 
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID") %>'></asp:LinkButton>                                                                                                              
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="UploadFeedback" />
                                                <asp:PostBackTrigger ControlID="lblFeedbackDownLoadfile1" />
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                </ItemTemplate>
                                                </asp:TemplateField>  

                                                <asp:TemplateField HeaderText="VerticalId" ItemStyle-Width="0px" Visible="false">
                                                <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblVerticalId" runat="server" Text='<%# Eval("VerticalId") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CustomerBranch" ItemStyle-Width="0px" Visible="false">
                                                <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("HubID") %>'></asp:Label>
                                                </div>
                                                </ItemTemplate>
                                                </asp:TemplateField>   
                                                <asp:TemplateField HeaderText="auditiD"  visible="false">
                                                <ItemTemplate>
                                                
                                                <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                     
                                                </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="70px" HeaderText="Action">
                                                <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa7a" UpdateMode="Conditional">
                                                <ContentTemplate>                                                     
                                                <asp:LinkButton ID="lblDocumentDownLoadfileActionAll" runat="server"
                                                    data-toggle="tooltip" data-placement="top" ToolTip="Download" CssClass="btdownss"
                                                    CommandName="FDDocumentDownLoadfileAll" 
                                                    CommandArgument='<%# Eval("HubID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("Period")+","+Eval("AuditID") %>'></asp:LinkButton>                                                                                                              
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDocumentDownLoadfileActionAll" />                                                     
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                               
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <HeaderStyle BackColor="#ECF0F1" />
                                            <HeaderStyle HorizontalAlign="Center"/>
                                           <HeaderStyle Height="40px" />
                                             <PagerSettings Visible="false" />  
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                       </div>
                                     </asp:Panel>
                                         <div style="float: right;">
                                          <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                              class="form-control m-bot15"  Width="120%" Height="30px"  AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                          </asp:DropDownListChosen>  
                                        </div> 
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-5 colpadding0">
                                            <div class="table-Selecteddownload">
                                                <div class="table-Selecteddownload-text">
                                                    <p>
                                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 colpadding0" style="float: right;">
                                            <div class="table-paging" style="margin-bottom: 10px;">                                    
                                                <div class="table-paging-text" style="margin-top:-35px;margin-left:19px;">
                                                    <p>Page</p>
                                                </div>                                    
                                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                            </div>
                                        </div>
                                    </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

