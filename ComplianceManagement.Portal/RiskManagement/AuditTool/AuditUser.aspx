﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditUser.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditUser" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->

    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script type="text/javascript">
        function CloseModel() {
            $('#tbxFirstName').val('');
            $('#tbxLastName').val('');
            $('#tbxDesignation').val('');
            $('#tbxEmail').val('');
            $('#tbxContactNo').val('');
            window.parent.CloseUserPage();
        }
    </script>
</head>
<body class="Dashboard-white-widget">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="Isdf" runat="server"></asp:ScriptManager>
        <div class="row Dashboard-white-widget" style="height: 390px">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12">
                    <section class="panel">
                        <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary15" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserValidationGroup5" />

                                        <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                                            ErrorMessage="Email already exists." ValidationGroup="UserValidationGroup5" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            First Name</label>
                                        <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 390px;" CssClass="form-control" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="First Name can not be empty."
                                            ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserValidationGroup5"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                            ValidationGroup="UserValidationGroup5" ErrorMessage="Please enter a valid first name."
                                            ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Last Name</label>
                                        <asp:TextBox runat="server" ID="tbxLastName" Style="width: 390px;" CssClass="form-control"
                                            MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Last Name can not be empty."
                                            ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserValidationGroup5"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                            ValidationGroup="UserValidationGroup5" ErrorMessage="Please enter a valid last name."
                                            ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Designation</label>
                                        <asp:TextBox runat="server" ID="tbxDesignation" Style="width: 390px;" CssClass="form-control"
                                            MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Designation can not be empty."
                                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserValidationGroup5"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                            ValidationGroup="UserValidationGroup5" ErrorMessage="Please enter a valid designation."
                                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Email</label>
                                        <asp:TextBox runat="server" ID="tbxEmail" Style="width: 390px;" MaxLength="200" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserValidationGroup5"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                            ValidationGroup="UserValidationGroup5" ErrorMessage="Please enter a valid email."
                                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Contact No</label>
                                        <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 390px;" CssClass="form-control"
                                            MaxLength="32" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Contact Number can not be empty."
                                            ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserValidationGroup5"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                            ValidationGroup="UserValidationGroup5" ErrorMessage="Please enter a valid contact number."
                                            ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                            ValidationGroup="UserValidationGroup5" ErrorMessage="Please enter only 10 digit."
                                            ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="UserValidationGroup5" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary"
                                             OnClientClick="CloseModel()" data-dismiss="modal" />
                                    </div>

                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 25px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </section>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
