﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalAuditReviewerStatus_OLD.aspx.cs" EnableEventValidation="false" ValidateRequest="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.InternalAuditReviewerStatus_OLD" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.tiny.cloud/1/oipsn3zuqpolvftgsefarew0qb52za3sjm4vm0bjouh8fd9x/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <%--<script src="../../Newjs/tinymce.min.js"></script>
    <script src="../../Newjs/KendoTable.min.js"></script>
    <link href="../../NewCSS/KendoTable.min.css" rel="stylesheet" />
    <link href="../../NewCSS/KendoTableBlueOpal.css" rel="stylesheet" />
    <link href="../../NewCSS/KendoTableCommonMin.css" rel="stylesheet" />
    <link href="../../NewCSS/KendoTableMobile.min.css" rel="stylesheet" />--%>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        label {
            font-weight: 400 !important;
        }

        legend {
            margin-bottom: 2px !important;
            font-size: 19px;
            color: #666 !important;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }


        /*added by sagar more on 07-01-2020*/
        .MultilineVideoLinkResizedTextbox {
            width: 940px;
            height: 60px;
            margin-bottom: 10px;
        }
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

            .chosen-container-single .chosen-single span {
                color: #8e8e93;
            }

        .chosen-container {
            margin-bottom: 3px;
        }
    </style>


    <script type="text/javascript">
        bind_tinyMCE();

        function bind_tinyMCE() {
            tinymce.remove();
            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "myTextEditor",
                width: '100%',
                height: 400,
                plugins: ["table", "code", "insertdatetime media table paste"],
                paste_webkit_styles: "color font-size",
                menu: {
                    table: { title: 'Table', items: 'inserttable' },
                },
                toolbar: "undo redo | bold italic",
                statusbar: false,
                setup: function (editor) {
                    editor.on('change', function () {
                        tinymce.triggerSave();
                    });
                }
            });
        }

        $(document).ready(function () {
            $('#updateProgress').show();
            setTimeout(function () { $("#updateProgress").hide(); $("#updateProgress").html(); }, 3000);
        });

        function fopendocfileReview(file) {

            $('#DocumentReviewPopUp1').modal('show');
            $('#IFrameImage').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function userValid() {
            var ObservationTitleval, Observationval, Riskval, Rootcouseval, financialImpactval
            , Recommendationval, Personresponsibleval, ControlOwnerval, RFVObRatingval, RFVObCatval, RFVObSubCatval;

            ObservationTitleval = document.getElementById('<%=RFVOBSTitle.ClientID%>');
            Observationval = document.getElementById('<%=RFVOBS.ClientID%>');
            Riskval = document.getElementById('<%=RFVRisk.ClientID%>');
            Rootcouseval = document.getElementById('<%=RVFRootCause.ClientID%>');
            financialImpactval = document.getElementById('<%=RFVfinancialImpact.ClientID%>');
            Recommendationval = document.getElementById('<%=RFVRecommendation.ClientID%>');
            Personresponsibleval = document.getElementById('<%=RFVPerRes.ClientID%>');
            RFVObRatingval = document.getElementById('<%=RFVObRating.ClientID%>');
            RFVObCatval = document.getElementById('<%=RFVObCat.ClientID%>')
            ControlOwnerval = document.getElementById('<%=RFVORName.ClientID%>')
          <%--  RFVObSubCatval = document.getElementById('<%=RFVObSubCat.ClientID%>')--%>
            Observation = document.getElementById("txtObservation").value;

            if (Observation != "") {
                if (ObservationTitleval != null) {
                    document.getElementById('<%=RFVOBSTitle.ClientID%>').enabled = true;
                }
                if (Observationval != null) {
                    document.getElementById('<%=RFVOBS.ClientID%>').enabled = true;
                }
                if (Riskval != null) {
                    document.getElementById('<%=RFVRisk.ClientID%>').enabled = true;
                }
                if (Rootcouseval != null) {
                    document.getElementById('<%=RVFRootCause.ClientID%>').enabled = true;
                }
                if (financialImpactval != null) {
                    document.getElementById('<%=RFVfinancialImpact.ClientID%>').enabled = true;
                }
                if (Recommendationval != null) {
                    document.getElementById('<%=RFVRecommendation.ClientID%>').enabled = true;
                }
                if (Personresponsibleval != null) {
                    document.getElementById('<%=RFVPerRes.ClientID%>').enabled = true;
                }
                if (RFVObRatingval != null) {
                    document.getElementById('<%=RFVObRating.ClientID%>').enabled = true;
                }
                if (RFVObCatval != null) {
                    document.getElementById('<%=RFVObCat.ClientID%>').enabled = true;
                }
                if (ControlOwnerval != null) {
                    document.getElementById('<%=RFVORName.ClientID%>').enabled = true;
                }
               <%-- if (RFVObSubCatval != null) {
                    document.getElementById('<%=RFVObSubCat.ClientID%>').enabled = true;
                }--%>
                return true;
            }
            else {
                if (ObservationTitleval != null) {
                    document.getElementById('<%=RFVOBSTitle.ClientID%>').enabled = false;
                }
                if (Observationval != null) {
                    document.getElementById('<%=RFVOBS.ClientID%>').enabled = false;
                }
                if (Riskval != null) {
                    document.getElementById('<%=RFVRisk.ClientID%>').enabled = false;
                }
                if (Rootcouseval != null) {
                    document.getElementById('<%=RVFRootCause.ClientID%>').enabled = false;
                }
                if (financialImpactval != null) {
                    document.getElementById('<%=RFVfinancialImpact.ClientID%>').enabled = false;
                }
                if (Recommendationval != null) {
                    document.getElementById('<%=RFVRecommendation.ClientID%>').enabled = false;
                }
                if (Personresponsibleval != null) {
                    document.getElementById('<%=RFVPerRes.ClientID%>').enabled = false;
                }
                if (RFVObRatingval != null) {
                    document.getElementById('<%=RFVObRating.ClientID%>').enabled = false;
                }
                if (RFVObCatval != null) {
                    document.getElementById('<%=RFVObCat.ClientID%>').enabled = false;
                }
                if (ControlOwnerval != null) {
                    document.getElementById('<%=RFVORName.ClientID%>').enabled = false;
                }
               <%-- if (RFVObSubCatval != null) {
                    document.getElementById('<%=RFVObSubCat.ClientID%>').enabled = false;
                }--%>
                return false;
            }
            return true;
        }

        function checkImageFile(obj) {
            var files = document.getElementById("FileUploadImageTable").files;
            for (var i = 0; i < files.length; i++) {
                var extension = files[i].name.split('.').pop().toLowerCase();
                var TypeList = ["jpeg", "jpg", "png", "gif", "bmp", "jfif"]
                if (!TypeList.includes(extension)) {
                    alert("Upload only '.jpeg','.jpg', '.png', '.gif', '.bmp','.jfif' formats file.");
                    document.getElementById("FileUploadImageTable").value = '';
                }
                //if ((extension != 'jpeg')) {
                //    alert("Upload only '.jpeg','.jpg', '.png', '.gif', '.bmp','.jfif' formats file.");
                //    document.getElementById("FileUploadImageTable").value = '';
                //}
            }
        }
    </script>
    <script>

        function ShowDialog(ScheduledOnID, RiskCreationID, FinancialYear, CustomerBranchId, ForMonth) {
            $('#divDetailsDialog').modal('show');
            $('#showdetails').attr('width', '95%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '100%');
            $('#showdetails').attr('src', "../Controls/ReviewerStatus.aspx?ScheduledOnID=" + ScheduledOnID + "&RiskCreationId=" + RiskCreationID + "&FinancialYear=" + FinancialYear + "&CustomerBranchID=" + CustomerBranchId + "&Period=" + ForMonth);
        };

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
        function initializeConfirmDatePicker1(date) {
            var startDate = new Date();
            $('#<%= txtTimeLine.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            $('#<%= txtResponseDueDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        function ConfirmtestViewREV(ink) {
            $('#DocumentPopUpREV').modal();
            $('#docViewerAllREV').attr('src', "../../docviewer.aspx?docurl=" + ink);
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUpREV').modal('hide');
            });
        });

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            var MaxLength = 200;
            $('#txtObservationTitle').keypress(function (e) {
                if ($(this).val().length >= MaxLength) {
                    e.preventDefault();
                }
            });

            $('#txtpopulation').keypress(function (e) {
                if ($('#txtpopulation').val().length >= MaxLength) {
                    e.preventDefault();
                }
            });
        });
        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        };

        function allowOnlyNumberWithDecimal(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        };

        function OpenUserPopup() {
            $('#AddUserModelPopup').modal('show');
            $('#IUser').attr('width', '100%');
            $('#IUser').attr('height', '400px');
            $('.modal-dialog').css('width', '60%');
            $('.modal-dialog').css('height', '500px');
            $('#IUser').attr('src', "../../RiskManagement/AuditTool/AuditUser.aspx");
        }

        // added by sagar more  on 21-01-2020
        function checkStringLengthOfPopulation(control) {
            var population = document.getElementById('txtpopulation').value;
            if (population.length > 200)
                alert('Population should not greater than 200 characters.')
        }
    </script>
    <script type="text/javascript">
        function ShowDetObsDialog(ProcessId, SubProcessId, AuditStepid, BranchId, StatusId, obs) {
            debugger;
            if (obs != 'false')
            {
                var value = sessionStorage.getItem('auditIdatbdId');
                sessionStorage.setItem('auditIdatbdId', value);
                $('#divPREDOCShowDialog').modal('show');
                $('#DetObsshowdetails').attr('width', '100%');
                $('#DetObsshowdetails').attr('height', '600px');
                $('.modal-dialog').css('width', '104%');
                $('.modal-dialog').css('margin-left', '-2%');
                $('#DetObsshowdetails').attr('src', "../InternalAuditTool/DetailedObservationList.aspx?ProcessId=" + ProcessId + "&SubProcessId=" + SubProcessId + "&AuditStepid=" + AuditStepid + "&BranchId=" + BranchId + "&StatusId=" + StatusId + "&Redirect=WS");
            }
            else {
                $('#divPREDOCShowDialog').modal('show');
                $('#DetObsshowdetails').attr('width', '100%');
                $('#DetObsshowdetails').attr('height', '600px');
                $('.modal-dialog').css('width', '104%');
                $('.modal-dialog').css('margin-left', '-2%');
                $('#DetObsshowdetails').attr('src', "../InternalAuditTool/DetailedObservationList.aspx?ProcessId=" + ProcessId + "&SubProcessId=" + SubProcessId + "&AuditStepid=" + AuditStepid + "&BranchId=" + BranchId + "&StatusId=" + StatusId + "&Redirect=WS");
            }
            
        };

        function HistoryMethod() {
            $('#divPREDOCShowDialog').modal('hide');
            var value = sessionStorage.getItem('auditIdatbdId');
            $('#ObsFromHistory').click();
            bind_tinyMCE();
        }

        function RemoveData() {
            $('#divPREDOCShowDialog').modal('hide');
            $('#RemoveData').click();
            bind_tinyMCE();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div runat="server" id="LblPageDetails" style="color: #666"></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div runat="server" id="Div1" style="color: #666; float: left;"></div>
                <div runat="server" id="divDetailedObs" style="color: #666; float: right; margin-right: 2%;">
                    <asp:Button Text="Historical Observations" runat="server" ID="btnObs" OnClick="btnObs_Click" CssClass="btn btn-search" />
                </div>
            </div>
            <div class="clearfix"></div>
            <table style="width: 100%; margin-top: 5px;" runat="server" visible="false">
                <tr>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlFilterLocation"
                            CssClass="form-control m-bot15" AutoPostBack="true" />
                    </td>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlFinancialYear"
                            CssClass="form-control m-bot15" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlSchedulingType"
                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" AutoPostBack="true" />
                    </td>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlPeriod"
                            CssClass="form-control m-bot15" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlFilterStatus" AutoPostBack="true"
                            CssClass="form-control m-bot15">
                        </asp:DropDownList></td>
                    <td style="width: 20%;"></td>
                    <td style="width: 30%;"></td>
                </tr>
            </table>
            <table style="width: 100%; margin-top: 5px;">
                <tr>
                    <td colspan="4">
                        <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False" ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Visible="false"> </asp:Label>
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td style="position: absolute;" valign="top" runat="server" id="TDTab">
                        <header class="panel-heading tab-bg-primary" style="background: none; !important;">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <li class="active" id="liAuditCoverage" runat="server">
                                    <asp:LinkButton ID="lnkAuditCoverage" OnClick="Tab1_Click" runat="server">Audit Coverage</asp:LinkButton>
                                </li>

                                <li class="" id="liActualTesting" runat="server">
                                    <asp:LinkButton ID="lnkActualTesting" OnClick="Tab2_Click" runat="server">Actual Testing/ Work Done</asp:LinkButton>
                                </li>

                                <li class="" id="liObservation" runat="server">
                                    <asp:LinkButton ID="lnkObservation" OnClick="Tab3_Click" runat="server">Observations</asp:LinkButton>
                                </li>

                                <li class="" id="liReviewLog" runat="server">
                                    <asp:LinkButton ID="lnkReviewLog" OnClick="Tab4_Click" runat="server">Review History & Log</asp:LinkButton>
                                </li>
                            </ul>
                        </header>
                        <div class="clearfix" style="height: 20px;"></div>
                        <div class="clearfix" style="height: 15px;"></div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="FirstView" runat="server">
                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                    <div style="margin-bottom: 4px;">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="FirstValidationGroup" />
                                        <div align="center" style="margin-top: 2px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                ValidationGroup="FirstValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>
                                    <table style="width: 100%; margin-top: 0px;">
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93; margin-bottom: 10px;">
                                                    Audit Methodology</label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtAuditObjective" Enabled="false" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 145px; width: 945px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 185px; display: block; float: left; font-size: 14px; color: #8e8e93; margin-bottom: 10px;">
                                                    Audit Steps</label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtAuditSteps" Enabled="false" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 145px; width: 945px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Analyis To Be Performed</label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtAnalysisToBePerformed" Enabled="false" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 145px; width: 945px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="display: block; text-align: center; margin: auto; width: 100%;">
                                                <asp:Button Text="Next" runat="server" ID="btnNext1" CssClass="btn btn-search" OnClick="btnNext1_Click"
                                                    ValidationGroup="FirstValidationGroup" />

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:View>
                            <asp:View ID="SecondView" runat="server">
                                <div style="width: 100%; float: left; margin-bottom: 15px">

                                    <div style="margin-bottom: 4px; width: auto">
                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="SecondValidationGroup" />
                                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label8" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator4" runat="server" EnableClientScript="False"
                                                ValidationGroup="SecondValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>

                                    <table>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Process  Walkthrough 
                                                </label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtWalkthrough" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Actual Work Done 
                                                </label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtActualWorkDone" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Population 
                                                </label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtpopulation" MaxLength="200" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" onKeyUp="checkStringLengthOfPopulation(this)" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Sample 
                                                </label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtSample" MaxLength="200" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;"></label>
                                                <label style="width: 150px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Remarks</label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbxRemarks" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                        <td style="vertical-align: top;">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Working File Upload</label>
                                        </td>
                                        <td>
                                            <div style="width: 100%;">
                                                <div style="width: 50%; float: left;">
                                                    <asp:FileUpload ID="fuSampleFile" runat="server" AllowMultiple="true" />
                                                </div>
                                                <div style="width: 50%; float: right;">
                                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-search" OnClick="btnUpload_Click" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"
                                                    OnLoad="upAuditDetails_Load">
                                                    <ContentTemplate>

                                                        <asp:GridView runat="server" ID="rptComplianceDocumnets"
                                                            AutoGenerateColumns="false" AllowSorting="true" PageSize="10"
                                                            AllowPaging="true" GridLines="None" BackColor="White" BorderColor="#DEDFDE"
                                                            CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%"
                                                            OnRowDataBound="rptComplianceDocumnets_RowDataBound"
                                                            OnRowDeleting="rptComplianceDocumnets_RowDeleting"
                                                            OnRowCommand="rptComplianceDocumnets_RowCommand" 
                                                            OnPageIndexChanging="rptComplianceDocumnets_PageIndexChanging" >
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Working Document" ItemStyle-Width="90%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton
                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                    CommandName="Download"
                                                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="btnComplianceDocumnets" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="aa1na" UpdateMode="Always">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandArgument='<%# Eval("FileID")%>'
                                                                                    AutoPostBack="true" CommandName="Delete"
                                                                                    OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                    ID="lbtLinkDocbutton" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' 
                                                                                                    alt="Delete" title="Delete" width="15px" height="15px" />
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lbtLinkDocbutton" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <EmptyDataTemplate>
                                                                No Records Found.
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>

                                                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td style="vertical-align: top;">
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Annexure Files</label>
                                        </td>
                                        <td>
                                            <div style="width: 100%;">
                                                <div style="width: 50%; float: left;">
                                                    <asp:FileUpload ID="AnnexureFileUpload" runat="server" AllowMultiple="true" />
                                                </div>
                                                <div style="width: 50%; float: right;">
                                                    <asp:Button ID="AnnexureButton" runat="server" Text="Upload" CssClass="btn btn-search" OnClick="btnAnnuxtureUpload_Click" />
                                                </div>
                                            </div>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="rptComplianceDocumnetsAnnexure" AutoGenerateColumns="false" AllowSorting="true"
                                                            GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                            OnRowCommand="rptComplianceDocumnetsAnnexure_RowCommand"
                                                            OnRowDataBound="rptComplianceDocumnetsAnnexure_RowDataBound"
                                                            OnPageIndexChanging="rptComplianceDocumnetsAnnexure_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Annexure Document" ItemStyle-Width="90%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton
                                                                                    CommandArgument='<%# Eval("Id")%>' CommandName="DownloadAnnuxture"
                                                                                    ID="btnComplianceDocumnetsAnnexure" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="btnComplianceDocumnetsAnnexure" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandArgument='<%# Eval("Id")%>'
                                                                                    AutoPostBack="true" CommandName="DeleteAnnuxture"
                                                                                    OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                    ID="lbtLinkDocbuttonAnnuxture" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="lbtLinkDocbuttonAnnuxture" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <EmptyDataTemplate>
                                                                No Records Found.
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" style="margin-top: 50px;">
                                                <asp:Button Text="Next" runat="server" ID="btnNext2" CssClass="btn btn-search" OnClick="btnNext2_Click"
                                                    ValidationGroup="SecondValidationGroup" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:View>

                            <asp:View ID="ThirdView" runat="server">
                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                    <div style="margin-bottom: 4px;">
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="None"
                                            class="alert alert-block alert-danger fade in"
                                            ValidationGroup="ThirdValidationGroup" />
                                        <div align="center" style="margin-top: 2px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label4" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                                ValidationGroup="ThirdValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table>
                                                <tr style="display: none;">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation Number
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtObservationNumber" CssClass="form-control" onkeypress="return allowOnlyNumber(event);" onpaste="return false;" Style="margin-bottom: 10px; width: 688px;" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation Title
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtObservationTitle" CssClass="form-control" MaxLength="200" Style="margin-bottom: 10px; width: 945px;" />
                                                        <asp:RequiredFieldValidator ID="RFVOBSTitle" ErrorMessage="Observation Title can not be empty." ControlToValidate="txtObservationTitle"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtObservation" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                        <asp:RequiredFieldValidator ID="RFVOBS" ErrorMessage="Observation can not be empty." ControlToValidate="txtObservation"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                        <asp:Button ID="ObsFromHistory" runat="server" OnClick="ObsFromHistory_Click" Style="display:none;" />
                                                        <asp:Button ID="RemoveData" runat="server" OnClick="RemoveData_Click" Style="display:none;" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Brief Observation 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="tbxBriefObservation" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation Background
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="tbxObjBackground" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Working File Upload</label>
                                                    </td>
                                                    <td>
                                                        <div style="width: 100%;">
                                                            <div style="width: 50%; float: left;">
                                                                <asp:FileUpload ID="FileObjUpload" runat="server" AllowMultiple="true" />
                                                            </div>
                                                            <div style="width: 50%; float: right;">
                                                                <asp:Button ID="btnObjfileupload" runat="server" Text="Upload" CssClass="btn btn-search" OnClick="btnObjfileupload_Click" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="GrdobservationFile"
                                                                    AutoGenerateColumns="false"
                                                                    AllowSorting="true" PageSize="10"
                                                                    AllowPaging="true" GridLines="none" CssClass="table"
                                                                    Font-Size="12px" Width="100%"
                                                                    OnRowCommand="rptComplianceDocumnets_RowCommand"
                                                                    OnRowDeleting="rptComplianceDocumnets_RowDeleting"
                                                                    OnRowDataBound="rptComplianceDocumnets_RowDataBound"
                                                                    OnPageIndexChanging="rptComplianceDocumnets_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Working Document" ItemStyle-Width="90%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                                            ID="btnComplianceDocumnetsObj" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnComplianceDocumnetsObj" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1na" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("FileID")%>'
                                                                                            AutoPostBack="true" CommandName="Delete"
                                                                                            OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                            ID="lbtLinkDocbuttonobj" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' 
                                                                                                    alt="Delete" title="Delete" width="15px" height="15px" />
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lbtLinkDocbuttonobj" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <HeaderStyle BackColor="#ECF0F1" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found.
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Annexure Title</label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAnnexueTitle" CssClass="form-control" MaxLength="200" Style="margin-bottom: 10px; width: 945px;" />
                                                    </td>
                                                </tr>
                                                <div runat="server" id="ShowTable">
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                Table 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbxTable" CssClass="form-control myTextEditor" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                            <%--<asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <textarea runat="server" id="editor" rows="10" cols="30" style="height: 400px; width: 750px;"> </textarea>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top; padding-top: 10px;">Upload Image
                                                        </td>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:FileUpload ID="FileUploadImageTable" runat="server" AllowMultiple="true" onchange="checkImageFile(this);" Style="display: inline-block; margin-top: 10px;" />
                                                                    <asp:Button Text="Upload" runat="server" ID="btnImageUpload" OnClick="btnImageUpload_Click"
                                                                        CssClass="btn btn-search pull-right" CausesValidation="false" Style="margin-bottom: 10px; margin-top: 10px;" />
                                                                    <div class="row">
                                                                        <asp:GridView runat="server" ID="grdObservationImg" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdObservationImg_PageIndexChanging"
                                                                            OnRowCommand="grdObservationImg_RowCommand">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Image" ItemStyle-Width="35%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("ImageName") %>'
                                                                                                data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ImageName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Always">
                                                                                            <ContentTemplate>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID") %>' AutoPostBack="true" CommandName="ViewImage"
                                                                                                    ID="lnkBtnViewDoc" runat="server" data-toggle="tooltip" data-placement="top" title="View Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="DeleteImage"
                                                                                                    OnClientClick="return confirm('Are you certain you want to delete this image?');"
                                                                                                    ID="lnkBtnDeleteDoc" runat="server" data-toggle="tooltip" data-placement="top" title="Delete Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" /> <%--width="15" height="15"--%>
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnViewDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnImageUpload" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">Audio/Video Link
                                                           <label title="Enter comma separated Audio/Video links!">(i)</label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMultilineVideolink" runat="server" TextMode="MultiLine" CssClass="MultilineVideoLinkResizedTextbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </div>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation Report
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ObjReport1" runat="server" CssClass="form-control m-bot15" Width="23%">
                                                            <asp:ListItem Value="1">Audit Committee</asp:ListItem>
                                                            <%--<asp:ListItem Value="0">MIS</asp:ListItem>
                                                            <asp:ListItem Value="2">Both of the above</asp:ListItem>--%>
                                                            <asp:ListItem Value="-1">None of the above</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RFObjReport" runat="server" ControlToValidate="ObjReport1"
                                                            Display="Dynamic" ErrorMessage="Please select Observation report type."
                                                            ValidationGroup="ThirdValidationGroup" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Business Implication 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtRisk" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                        <asp:RequiredFieldValidator ID="RFVRisk" ErrorMessage="Business Implication can not be Empty." ControlToValidate="txtRisk"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Root Cause 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtRootcost" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                        <asp:RequiredFieldValidator ID="RVFRootCause" ErrorMessage="Root Cause can not be Empty." ControlToValidate="txtRootcost"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Financial Impact 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtfinancialImpact" CssClass="form-control" Style="margin-bottom: 10px;  width: 945px;"  onkeypress="return this.value.length<=15" onpaste="return false;" />
                                                        <asp:RequiredFieldValidator ID="RFVfinancialImpact" ErrorMessage="Financial Impact can not be Empty." ControlToValidate="txtfinancialImpact"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />

                                                        <%--<asp:CompareValidator ID="cvFImpact" runat="server" ControlToValidate="txtfinancialImpact" ErrorMessage="Only Numbers in Financial Imapct"
                                                            ValidationGroup="ThirdValidationGroup" Display="None" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>--%>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Recommendation
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtRecommendation" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                        <asp:RequiredFieldValidator ID="RFVRecommendation" ErrorMessage="Recommendation can not be empty."
                                                            ControlToValidate="txtRecommendation"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Response Due Date
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox AutoComplete="off" runat="server" ID="txtResponseDueDate" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label id="M" runat="server" style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Management Response 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtMgtResponse" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label id="T" runat="server" style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Time Line
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtTimeLine"  AutoComplete="off" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;" />
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #666;">
                                                            Score
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAuditStepScore" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;"
                                                            onkeypress="return allowOnlyNumberWithDecimal(event);" onpaste="return false;" />
                                                        <asp:CompareValidator ID="cvRootCost" runat="server" ControlToValidate="txtAuditStepScore" ErrorMessage="Only Numbers in Score"
                                                            ValidationGroup="ThirdValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Person Responsible
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <asp:DropDownListChosen ID="ddlPersonresponsible" runat="server" CssClass="form-control m-bot15"
                                                                Width="50%" Height="32px" DataPlaceHolder="Person Responsible">
                                                            </asp:DropDownListChosen>
                                                            <asp:RequiredFieldValidator ID="RFVPerRes" ErrorMessage="Select Person Responsible."
                                                                ControlToValidate="ddlPersonresponsible" InitialValue="-1"
                                                                runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />

                                                            <img id="lnkAddNewUser" style="float: right; margin-right: 46%; margin-top: 2px;" src="../../Images/add_icon_new.png"
                                                                onclick="OpenUserPopup()" data-toggle="tooltip" data-placement="top" alt="Add New User" title="Add New User" />
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr style="display: none">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Unit Head Response
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="tbxUHComment" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Person Responsible
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownListChosen ID="ddlPersonresponsibleUH" runat="server"
                                                            CssClass="form-control m-bot15"
                                                            AllowSingleDeselect="false"
                                                            DisableSearchThreshold="3"
                                                            Width="50%" Height="32px" DataPlaceHolder="Person Responsible">
                                                        </asp:DropDownListChosen>
                                                    </td>
                                                </tr>

                                                <tr style="display: none">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            President Response
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="tbxPresidentComment" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Person Responsible
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownListChosen ID="ddlPersonresponsiblePresident" runat="server"
                                                            CssClass="form-control m-bot15"
                                                            AllowSingleDeselect="false"
                                                            DisableSearchThreshold="3"
                                                            Width="50%" Height="32px" DataPlaceHolder="Person Responsible">
                                                        </asp:DropDownListChosen>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Owner Name
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownListChosen ID="ddlOwnerName" runat="server" CssClass="form-control m-bot15"
                                                            Width="50%" Height="32px" DataPlaceHolder="Owner">
                                                        </asp:DropDownListChosen>
                                                        <asp:RequiredFieldValidator ID="RFVORName" ErrorMessage="Select OwnerName."
                                                            ControlToValidate="ddlOwnerName" InitialValue="-1"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation Rating
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlobservationRating" runat="server" CssClass="form-control m-bot15"
                                                            Width="50%" Height="32px" DataPlaceHolder="Observation Rating">
                                                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                                            <asp:ListItem Value="1">Major</asp:ListItem>
                                                            <asp:ListItem Value="2">Moderate</asp:ListItem>
                                                            <asp:ListItem Value="3">Minor</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RFVObRating" ErrorMessage="Select Observation Rating."
                                                            ControlToValidate="ddlobservationRating" InitialValue="-1"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr style="display: none;">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Defeciency Type
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlDefeciencyType" runat="server" CssClass="form-control m-bot15"
                                                            Width="50%" Height="32px" DataPlaceHolder="Observation Rating">
                                                            <asp:ListItem Value="-1">Select Defeciency Type</asp:ListItem>
                                                            <asp:ListItem Value="1">Design</asp:ListItem>
                                                            <asp:ListItem Value="2">Operation</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation Category
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlObservationCategory" runat="server" CssClass="form-control m-bot15" Width="50%" Height="32px"
                                                            DataPlaceHolder="Observation Category"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlObservationCategory_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RFVObCat" ErrorMessage="Select Observation Category."
                                                            ControlToValidate="ddlObservationCategory" InitialValue="-1"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Observation SubCategory
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlObservationSubCategory" runat="server" CssClass="form-control m-bot15"
                                                            Width="50%" Height="32px"
                                                            DataPlaceHolder="Observation SubCategory">
                                                        </asp:DropDownList>
                                                        <%--<asp:RequiredFieldValidator ID="RFVObSubCat" ErrorMessage="Select Observation SubCategory."
                                                            ControlToValidate="ddlObservationSubCategory" InitialValue="-1"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />--%>
                                                    </td>
                                                </tr>

                                                <tr style="display: none;">
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Reviewer Risk Rating
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownListChosen ID="ddlReviewerRiskRatiing" runat="server" CssClass="form-control m-bot15"
                                                            Width="50%" Height="32px" DataPlaceHolder="Reviewer Risk Rating">
                                                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                                            <asp:ListItem Value="1">High</asp:ListItem>
                                                            <asp:ListItem Value="2">Medium</asp:ListItem>
                                                            <asp:ListItem Value="3">Low</asp:ListItem>
                                                        </asp:DropDownListChosen>
                                                        <%--<asp:RequiredFieldValidator ID="RFVRiskRating" ErrorMessage="Select Risk Rating."
                                                            ControlToValidate="ddlReviewerRiskRatiing" InitialValue="-1"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" style="margin-top: 50px;"></td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnObjfileupload" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button Text="Next" runat="server" ID="btnNext3" OnClick="btnNext3_Click" CssClass="btn btn-search"
                                        CausesValidation="true" OnClientClick="javascript:userValid();bind_tinyMCE();"
                                        ValidationGroup="ThirdValidationGroup" />

                                    <div style="margin-top: 50px;">
                                        <label style="width: 342px; display: block; float: left; font-size: 14px; color: red;">Note :: (*) fields are compulsory In Case of Observation</label>
                                    </div>
                                </div>
                            </asp:View>

                            <asp:View ID="FourthView" runat="server">
                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                    <div style="margin-bottom: 4px;">
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="oplValidationGroup" />
                                        <div align="center" style="margin-top: 2px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label6" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator3" runat="server" EnableClientScript="False"
                                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                                <legend>Review History</legend>
                                                <table>
                                                    <tr id="ICFRLINK" runat="server" visible="false">
                                                        <td style="width: 50%">
                                                            <asp:DropDownList ID="ddlQuarter" runat="server" CssClass="form-control m-bot15" Width="50%" Height="32px"
                                                                DataPlaceHolder="Quarter" OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:Label ID="lblQutErrorMsg" runat="server" Text="" ForeColor="red"></asp:Label>
                                                            <asp:LinkButton ID="btnPopUpLink" runat="server" OnClick="btnPopUpLink_Click" Visible="false">ICFR Audit Step Close</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                                AllowPaging="true" GridLines="None" BackColor="White" BorderColor="#DEDFDE" CssClass="table"
                                                                CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%"
                                                                PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                OnPageIndexChanging="GridRemarks_OnPageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblATBDId" runat="server" Text='<%# Eval("ATBDId") %>'></asp:Label>
                                                                            <asp:Label ID="lblAuditId" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="riskID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("InternalAuditInstance") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Scheduleon" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                        <ItemTemplate>
                                                                            <div class="text_NlinesusingCSS" style="width: 400px;">
                                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' ToolTip='<%# Eval("Remarks") %>' data-toggle="tooltip" data-placement="top"></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Date" ItemStyle-Width="150px">
                                                                        <ItemTemplate>
                                                                            <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="lblDownLoadfile" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>' OnClick="DownLoadClick"></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lblViewFile" runat="server" Text='<%# ShowSampleDocumentNameView((string)Eval("Name")) %>' OnClick="lblViewFile_Click"></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                                    <asp:PostBackTrigger ControlID="lblViewFile" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <HeaderStyle BackColor="#ECF0F1" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found.
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <label style="width: 125px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                Review Remark</label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 100px; width: 945px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="width: 125px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                &nbsp;</label>
                                                        </td>
                                                        <td>
                                                            <asp:FileUpload ID="FileuploadAdditionalFile" runat="server" AllowMultiple="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div style="margin-bottom: 7px; margin-top: 10px;">
                                                                <label id="lblStatus" runat="server" style="width: 128px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                    Status</label>
                                                                <asp:RadioButtonList ID="rdbtnStatus" runat="server" RepeatDirection="Horizontal">
                                                                </asp:RadioButtonList>
                                                                <asp:RequiredFieldValidator ID="rfvRdoStatusButton" runat="server" Display="None" ValidationGroup="oplValidationGroup" ControlToValidate="rdbtnStatus"
                                                                    ErrorMessage="Please select Status."> </asp:RequiredFieldValidator>
                                                            </div>
                                                            <div style="margin-bottom: 7px" id="divDated" runat="server">
                                                                <label style="width: 128px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                    Date</label>
                                                                <asp:TextBox runat="server" ID="tbxDate" CssClass="form-control" Style="width: 115px; text-align: center" Enabled="false" />
                                                                <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="oplValidationGroup" Display="None" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td align="center" style="text-align: center;">
                                                            <asp:Button Text="Submit" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                                ValidationGroup="oplValidationGroup" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                            <div class="clearfix"></div>
                                            <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                                <legend>Audit Log</legend>

                                                <div style="width: 100%; margin-left: 2%">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory"
                                                            AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right" GridLines="None" CssClass="table"
                                                            CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px"
                                                            DataKeyNames="AuditTransactionID" OnRowCommand="grdTransactionHistory_RowCommand"
                                                            OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 300px;">
                                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Remarks") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Date" ItemStyle-Width="150px">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />

                                                            <EmptyDataTemplate>
                                                                No Records Found.
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                        <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSave" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </div>

        <div class="modal fade" id="divDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -6px;">&times;</button>
                    </div>
                    <div class="modal-body" style="background-color: #eee;">
                        <iframe id="showdetails" src="about:blank" width="70%" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <%--<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom">
                                    View Document(s)</label>--%>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: right; width: 90%">
                                <asp:Label runat="server" ID="Label9" Style="color: red;"></asp:Label>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="IFrameImage" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DocumentPopUpREV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="docViewerAllREV" runat="server" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="divPREDOCShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="DetObsshowdetails" src="about:blank" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField runat="server" ID="hidObservation" />
        <asp:HiddenField runat="server" ID="hidAuditObjective" />
        <asp:HiddenField runat="server" ID="hidAuditSteps" />
        <asp:HiddenField runat="server" ID="hidAnalysisToBePerformed" />
        <asp:HiddenField runat="server" ID="hidWalkthrough" />
        <asp:HiddenField runat="server" ID="hidActualWorkDone" />
        <asp:HiddenField runat="server" ID="hidpopulation" />
        <asp:HiddenField runat="server" ID="hidSample" />
        <asp:HiddenField runat="server" ID="hidObservationNumber" />
        <asp:HiddenField runat="server" ID="hidObservationTitle" />
        <asp:HiddenField runat="server" ID="hidRisk" />
        <asp:HiddenField runat="server" ID="hidRootcost" />
        <asp:HiddenField runat="server" ID="hidfinancialImpact" />
        <asp:HiddenField runat="server" ID="hidRecommendation" />
        <asp:HiddenField runat="server" ID="hidMgtResponse" />
        <asp:HiddenField runat="server" ID="hidRemarks" />
        <asp:HiddenField runat="server" ID="hidAuditStepScore" />
        <asp:HiddenField runat="server" ID="hidTimeLine" />
        <asp:HiddenField runat="server" ID="hidISACPORMIS" />
        <asp:HiddenField runat="server" ID="hidPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidObservationRating" />
        <asp:HiddenField runat="server" ID="hidObservationCategory" />
        <asp:HiddenField runat="server" ID="hidObservationSubCategory" />
        <asp:HiddenField runat="server" ID="hidOwner" />
        <asp:HiddenField runat="server" ID="hidUserID" />

        <asp:HiddenField runat="server" ID="hidUHPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidPRESIDENTPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidUHComment" />
        <asp:HiddenField runat="server" ID="hidPRESIDENTComment" />
        <asp:HiddenField runat="server" ID="hidBodyContent" />
        <asp:HiddenField runat="server" ID="hidResponseDueDate" />
        <asp:HiddenField runat="server" ID="hidAnnaxureTitle" />

        <asp:LinkButton  ID="btnHiddenUpdatUser" style="display:none" runat="server" OnClick="btnHiddenUpdatUser_Click" />

        <div class="modal fade" id="AddUserModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog p0" style="width: 50%; overflow: hidden;">
                <div class="modal-content">
                    <div class="modal-header" style="padding: 30px !important;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom" id="Usermmodel">
                            Add User</label>
                    </div>
                    <div class="modal-body Dashboard-white-widget" style="width: 100%; margin-top: -2px;">
                        <iframe src="about:blank" id="IUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function CloseUserPage() {

                $('#AddUserModelPopup').modal('hide');
                document.getElementById('<%= btnHiddenUpdatUser.ClientID %>').click();
        }
        </script>
    </form>
</body>
</html>
