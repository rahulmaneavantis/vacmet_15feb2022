﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class CheckListPreReqsiteDocuments : System.Web.UI.Page
    {
        List<PerformerFileUpload> performerFile = new List<PerformerFileUpload>();
        protected long ATBDID;
        protected long ChecklistID;
        protected long AuditID;

        #region oldCode
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        ATBDID = -1;
        //        ChecklistID = -1;
        //        AuditID = -1;
        //        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
        //        {
        //            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
        //            ViewState["AuditID"] = AuditID;
        //        }
        //        if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
        //        {
        //            ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
        //            ViewState["ATBDID"] = ATBDID;
        //        }
        //        if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
        //        {
        //            ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
        //            ViewState["ChecklistID"] = ChecklistID;
        //        }
        //        if (ATBDID != -1 && ChecklistID != -1 && AuditID != -1)
        //        {                   
        //            BindDocument(ChecklistID, ATBDID, AuditID);                    
        //        }
        //    }
        //}
        //protected string getstatus(long AuditID, long ATBDID, long ChecklistID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {

        //        var query = (from row in entities.RecentPTUs
        //                     where row.AuditID == AuditID && row.ATBDId == ATBDID
        //                     && row.checklistId == ChecklistID
        //                     select row.Status).FirstOrDefault();
        //        if (query != null)
        //        {
        //            return Convert.ToString(query);
        //        }
        //        else
        //        {
        //            return "";
        //        }
        //    }
        //}
        //public void rptComplianceDocumnets_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        //{

        //}
        //protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    try
        //    {
        //        rptComplianceDocumnets.PageIndex = e.NewPageIndex;
        //        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
        //        {
        //            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
        //        }
        //        else
        //        {
        //            AuditID = Convert.ToInt32(ViewState["AuditID"]);
        //        }

        //        if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
        //        {
        //            ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
        //        }
        //        else
        //        {
        //            ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
        //        }

        //        if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
        //        {
        //            ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
        //        }
        //        else
        //        {
        //            ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
        //        }

        //        BindDocument(ChecklistID, ATBDID, AuditID);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        //protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName.Equals("Download"))
        //        {
        //            DownloadFile(Convert.ToInt32(e.CommandArgument));
        //        }
        //        else if (e.CommandName.Equals("Page"))
        //        {                  
        //        }
        //        else if (e.CommandName.Equals("Delete"))
        //        {
        //            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
        //            {
        //                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
        //            }
        //            else
        //            {
        //                AuditID = Convert.ToInt32(ViewState["AuditID"]);
        //            }

        //            if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
        //            {
        //                ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
        //            }
        //            else
        //            {
        //                ATBDID = Convert.ToInt32(ViewState["ATID"]);
        //            }

        //            if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
        //            {
        //                ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
        //            }
        //            else
        //            {
        //                ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
        //            }                    
        //            BindDocument(ChecklistID, ATBDID, AuditID);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        //public static PrerequisiteTran_Upload GetFileInternalFileData_Risk(int fileID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var file = (from row in entities.PrerequisiteTran_Upload
        //                    where row.Id == fileID
        //                    select row).FirstOrDefault();

        //        return file;
        //    }
        //}
        //public void DownloadFile(int fileId)
        //{
        //    try
        //    {
        //        var file =GetFileInternalFileData_Risk(fileId);

        //        if (file.FilePath != null)
        //        {
        //            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
        //            if (filePath != null && File.Exists(filePath))
        //            {
        //                Response.Buffer = true;
        //                Response.Clear();
        //                Response.ClearContent();
        //                Response.ClearHeaders();
        //                Response.ContentType = "application/octet-stream";
        //                Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
        //                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
        //                Response.Flush(); // send it to the client to download
        //                Response.End();

        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;

        //    }
        //}     
        //public string ShowSampleDocumentName(string DownloadFileName)
        //{
        //    string processnonprocess = "";
        //    if (!String.IsNullOrEmpty(DownloadFileName))
        //    {
        //        processnonprocess = "Download";
        //    }
        //    else
        //    {
        //        processnonprocess = "File Not Uploded";
        //    }
        //    return processnonprocess;
        //}
        //public string ShowSampleDocumentNameView(string DownloadFileName)
        //{
        //    string processnonprocess = "";
        //    if (!String.IsNullOrEmpty(DownloadFileName))
        //    {
        //        processnonprocess = "/View";
        //    }

        //    return processnonprocess;
        //}
        //public static List<PrerequisiteTran_Upload> GetFileData(int id, int checklistID, int ATBDId, int AuditID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var fileData = (from row in entities.PrerequisiteTran_Upload
        //                        where row.Id == id && row.AuditID == AuditID
        //                        && row.checklistId == checklistID && row.ATBDId == ATBDId                              
        //                         && row.IsDeleted == false
        //                        select row).ToList();

        //        return fileData;
        //    }
        //}
        //protected void lblViewFile_Click(object sender, EventArgs e)
        //{
        //    LinkButton btn = (LinkButton)sender;
        //    GridViewRow gvr = (GridViewRow)btn.NamingContainer;          
        //    Label lblID = (Label)gvr.FindControl("lblID");
        //    Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
        //    Label lblchecklistId = (Label)gvr.FindControl("lblchecklistId");            
        //    Label lblAuditId = (Label)gvr.FindControl("lblAuditId");

        //    List<PrerequisiteTran_Upload> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblchecklistId.Text), Convert.ToInt32(lblATBDId.Text), Convert.ToInt32(lblAuditId.Text));

        //    foreach (var file in fileData)
        //    {
        //        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

        //        if (file.FilePath != null && File.Exists(filePath))
        //        {
        //            string Folder = "~/TempFiles";
        //            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

        //            string DateFolder = Folder + "/" + FileData;

        //            string extension = System.IO.Path.GetExtension(filePath);

        //            Directory.CreateDirectory(Server.MapPath(DateFolder));

        //            if (!Directory.Exists(DateFolder))
        //            {
        //                Directory.CreateDirectory(Server.MapPath(DateFolder));
        //            }

        //            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

        //            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

        //            string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

        //            string FileName = DateFolder + "/" + User + "" + extension;

        //            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
        //            BinaryWriter bw = new BinaryWriter(fs);
        //            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
        //            bw.Close();

        //            string CompDocReviewPath = FileName;
        //            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestView('" + CompDocReviewPath + "');", true);
        //        }
        //        break;
        //    }
        //}
        //public void BindDocument(long checklistid, long ATBDID, long AuditID)
        //{
        //    try
        //    {
        //        using (AuditControlEntities entities = new AuditControlEntities())
        //        {
        //            List<PreRequsiteBindDocument_Result> ComplianceDocument = new List<PreRequsiteBindDocument_Result>();

        //            ComplianceDocument = (from row in entities.PreRequsiteBindDocument(AuditID, ATBDID, checklistid)
        //                                  where row.ATBDId == ATBDID && row.AuditID == AuditID                                                                                                                       
        //                                  select row).ToList();

        //            if (ComplianceDocument != null && ComplianceDocument.Count>0)
        //            {
        //                string status = getstatus(AuditID, ATBDID, ChecklistID);
        //                if (status == "Approved")
        //                {
        //                    btnApprove.Visible = false;
        //                    btnReject.Visible = false;
        //                }
        //                else if (status == "Rejected")
        //                {
        //                    btnApprove.Visible = false;
        //                    btnReject.Visible = false;
        //                }
        //                else
        //                {
        //                    btnApprove.Visible = true;
        //                    btnReject.Visible = true;
        //                }                       
        //                rptComplianceDocumnets.DataSource = ComplianceDocument.ToList();
        //                rptComplianceDocumnets.DataBind();
        //            }
        //            else
        //            {
        //                btnApprove.Visible = false;
        //                btnReject.Visible = false;
        //                rptComplianceDocumnets.DataSource = null;
        //                rptComplianceDocumnets.DataBind();
        //            }                  
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        //protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

        //    if (lblDownLoadfile != null)
        //    {
        //        var scriptManager = ScriptManager.GetCurrent(this.Page);
        //        scriptManager.RegisterPostBackControl(lblDownLoadfile);
        //    }
        //    LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
        //    if (lbtLinkDocbutton != null)
        //    {
        //        var scriptManager = ScriptManager.GetCurrent(this.Page);
        //        scriptManager.RegisterPostBackControl(lbtLinkDocbutton);
        //    }
        //}

        //protected void btnApprove_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        using (AuditControlEntities entities = new AuditControlEntities())
        //        {
        //            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
        //            {
        //                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
        //            }
        //            else
        //            {
        //                AuditID = Convert.ToInt32(ViewState["AuditID"]);
        //            }

        //            if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
        //            {
        //                ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
        //            }
        //            else
        //            {
        //                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
        //            }

        //            if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
        //            {
        //                ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
        //            }
        //            else
        //            {
        //                ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
        //            }
        //            ApproveUpdatePrerequisiteTran_Upload(AuditID, ChecklistID, ATBDID, 3);
        //            cvDuplicateEntry.IsValid = false;
        //            cvDuplicateEntry.ErrorMessage = "Approved Sucessfully.";
        //            btnApprove.Visible = false;
        //            btnReject.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        //public static void ApproveUpdatePrerequisiteTran_Upload(long auid, long chkid, long atbdid,int status)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var fileData = (from row in entities.PrerequisiteTran_Upload
        //                        where row.AuditID == auid
        //                        && row.checklistId == chkid && row.ATBDId == atbdid
        //                        && row.Status !=4
        //                        && row.IsDeleted == false
        //                        select row).ToList();

        //        if (fileData.Count > 0)
        //        {
        //            fileData.ForEach(entry =>
        //            {
        //                entry.Status = status;                      
        //            });

        //            entities.SaveChanges();
        //        }
        //    }
        //}

        //public static void DeleteUpdatePrerequisiteTran_Upload(long auid, long chkid, long atbdid, int status,string remark)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<int> sid = new List<int>();
        //        sid.Add(1);
        //        sid.Add(4);
        //        var fileData = (from row in entities.PrerequisiteTran_Upload
        //                        where row.AuditID == auid
        //                        && row.checklistId == chkid && row.ATBDId == atbdid
        //                        && !sid.Contains(row.Status)
        //                        && row.IsDeleted == false
        //                        select row).ToList();

        //        if (fileData.Count > 0)
        //        {
        //            fileData.ForEach(entry =>
        //            {
        //                entry.Status = status;
        //                entry.Remark = remark;
        //                var interfiledata = (from row in entities.InternalFileData_Risk
        //                                     where row.AuditID == auid
        //                                     && row.ATBDId == atbdid
        //                                     && row.IsDeleted == false
        //                                     && row.Name==entry.FileName
        //                                     select row).FirstOrDefault();
        //                if (interfiledata !=null)
        //                {
        //                    interfiledata.IsDeleted = true;
        //                    entities.SaveChanges();
        //                }
        //            });

        //            entities.SaveChanges();
        //        }              
        //    }
        //}
        //protected void btnReject_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        using (AuditControlEntities entities = new AuditControlEntities())
        //        {
        //            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
        //            {
        //                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
        //            }
        //            else
        //            {
        //                AuditID = Convert.ToInt32(ViewState["AuditID"]);
        //            }

        //            if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
        //            {
        //                ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
        //            }
        //            else
        //            {
        //                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
        //            }

        //            if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
        //            {
        //                ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
        //            }
        //            else
        //            {
        //                ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
        //            }
        //            if (string.IsNullOrEmpty(txtRemark.Text))
        //            {
        //                cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Please enter remark.";
        //                return;
        //            }
        //            if (!string.IsNullOrEmpty(txtRemark.Text))
        //            {
        //                DeleteUpdatePrerequisiteTran_Upload(AuditID, ChecklistID, ATBDID, 4, txtRemark.Text.Trim());
        //                BindDocument(ChecklistID, ATBDID, AuditID);
        //                cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Rejected Sucessfully.";
        //                btnApprove.Visible = false;
        //                btnReject.Visible = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ATBDID = -1;
                ChecklistID = -1;
                AuditID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    ViewState["ATBDID"] = ATBDID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                {
                    ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    ViewState["ChecklistID"] = ChecklistID;
                }
                if (ATBDID != -1 && ChecklistID != -1 && AuditID != -1)
                {
                    BindDocument(ChecklistID, ATBDID, AuditID);
                }
            }
        }
        protected string getstatus(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.RecentPTUs
                             where row.AuditID == AuditID && row.ATBDId == ATBDID
                             && row.checklistId == ChecklistID
                             select row.Status).FirstOrDefault();
                if (query != null)
                {
                    return Convert.ToString(query);
                }
                else
                {
                    return "";
                }
            }
        }
        public void rptComplianceDocumnets_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                rptComplianceDocumnets.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                {
                    ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                }
                else
                {
                    ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                }

                BindDocument(ChecklistID, ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Page"))
                {
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                    {
                        ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    }
                    else
                    {
                        ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                    }
                    BindDocument(ChecklistID, ATBDID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static PrerequisiteTran_Upload GetFileInternalFileData_Risk(int fileID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var file = (from row in entities.PrerequisiteTran_Upload
                            where row.Id == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }
        public void DownloadFile(int fileId)
        {
            try
            {
                var file = GetFileInternalFileData_Risk(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                        Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }
            }
            catch (Exception)
            {
                throw;

            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName) && DownloadFileName != "NA")
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName) && DownloadFileName != "NA")
            {
                processnonprocess = "/View";
            }

            return processnonprocess;
        }
        public static List<PrerequisiteTran_Upload> GetFileData(int id, int checklistID, int ATBDId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.PrerequisiteTran_Upload
                                where row.Id == id && row.AuditID == AuditID
                                && row.checklistId == checklistID && row.ATBDId == ATBDId
                                 && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }
        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
            Label lblchecklistId = (Label)gvr.FindControl("lblchecklistId");
            Label lblAuditId = (Label)gvr.FindControl("lblAuditId");

            List<PrerequisiteTran_Upload> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblchecklistId.Text), Convert.ToInt32(lblATBDId.Text), Convert.ToInt32(lblAuditId.Text));

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestView('" + CompDocReviewPath + "');", true);
                }
                break;
            }
        }
        public void BindDocument(long checklistid, long ATBDID, long AuditID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<PreRequsiteBindDocument_Result> ComplianceDocument = new List<PreRequsiteBindDocument_Result>();

                    ComplianceDocument = (from row in entities.PreRequsiteBindDocument(AuditID, ATBDID, checklistid)
                                          where row.ATBDId == ATBDID && row.AuditID == AuditID
                                          select row).ToList();

                    if (ComplianceDocument != null && ComplianceDocument.Count > 0)
                    {
                        string status = getstatus(AuditID, ATBDID, ChecklistID);
                        if (status == "Approved")
                        {
                            btnApprove.Visible = false;
                            btnReject.Visible = false;
                        }
                        else if (status == "Rejected")
                        {
                            btnApprove.Visible = false;
                            btnReject.Visible = false;
                        }
                        else
                        {
                            btnApprove.Visible = true;
                            btnReject.Visible = true;
                        }
                        rptComplianceDocumnets.DataSource = ComplianceDocument.ToList();
                        rptComplianceDocumnets.DataBind();
                    }
                    else
                    {
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                        rptComplianceDocumnets.DataSource = null;
                        rptComplianceDocumnets.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");
            Label lblRemark = (Label)e.Row.FindControl("lblRemark");
            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocbutton);
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {

                var CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                    {
                        ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    }
                    else
                    {
                        ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                    }
                    string Remark = string.Empty;
                    if (fileUploadDocument.HasFile)
                    {
                        HttpFileCollection fileCollection = Request.Files;
                        if (fileCollection.Count > 0)
                        {
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadfile = fileCollection[i];
                                string[] keys = fileCollection.Keys[i].Split('$');
                                //String fileName = "";
                                if (keys[keys.Count() - 1].Equals("fileUploadDocument"))
                                {
                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Session["bytes"] = bytes;
                                }

                            }
                            var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(ATBDID), Convert.ToInt32(AuditID));
                            string directoryPathIFD = "";
                            directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalId.ToString() + "/" + InstanceData.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ScheduleonID + "/2.0");
                            if (!Directory.Exists(directoryPathIFD))
                            {
                                DocumentManagement.CreateDirectory(directoryPathIFD);
                            }


                            string directoryPath = "";

                            if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                            {
                                directoryPath = Server.MapPath("~/AuditPreRequisiteDocument/" + CustomerId + "/"
                                  + Convert.ToInt32(AuditID) + "/" + Convert.ToInt32(ATBDID) + "/"
                                  + Convert.ToInt32(ChecklistID) + "/2.0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }

                            string fileName = fileUploadDocument.FileName;
                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(fileUploadDocument.FileName));
                            string finalPathPR = Path.Combine(directoryPathIFD, fileKey + Path.GetExtension(fileUploadDocument.FileName));
                            
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                Remark = txtRemark.Text.Trim();
                            }
                            else
                            {
                                Remark = "NA";
                            }
                            PerformerFileUpload pFile = new PerformerFileUpload
                            {
                                CustomerId = Convert.ToInt32(CustomerId),
                                AuditID = Convert.ToInt32(AuditID),
                                ATBDID = Convert.ToInt32(ATBDID),
                                ChecklistID = Convert.ToInt32(ChecklistID),
                                FileName = fileName,
                                FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                FileKey = fileKey,
                                Remark = Remark,
                                bytes = (Byte[])Session["bytes"]
                            };

                            performerFile.Add(pFile);
                        }
                    }
                    if (!string.IsNullOrEmpty(txtRemark.Text))
                    {
                        Remark = txtRemark.Text.Trim();
                    }
                    else
                    {
                        Remark = "NA";
                    }
                    ApproveUpdatePrerequisiteTran_Upload(AuditID, ChecklistID, ATBDID, 3, Remark, performerFile);
                    UpdateFlagOfUploadedDocument(AuditID, ATBDID, "Approved");
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Approved Sucessfully.";
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                    txtRemark.Text = string.Empty;
                    BindDocument(ChecklistID, ATBDID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void ApproveUpdatePrerequisiteTran_Upload(long auid, long chkid, long atbdid, int status, string remark, List<PerformerFileUpload> performerFile)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<PrerequisiteTran_Upload> fileData = new List<PrerequisiteTran_Upload>();
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.Status == 2 && row.checklistId == chkid
                             && row.AuditID == auid
                             && row.ATBDId == atbdid
                             select row).ToList();
                if (query.Count > 0)
                {
                    var Subquery = (from row in entities.PrerequisiteTran_Upload
                                    where row.Status == 5 && row.checklistId == chkid
                                    && row.AuditID == auid
                                    && row.ATBDId == atbdid
                                    select row).ToList();
                    if (Subquery.Count > 0)
                    {
                        fileData = (from row in entities.PrerequisiteTran_Upload
                                    where
                                    row.AuditID == auid
                                    && row.checklistId == chkid && row.ATBDId == atbdid
                                    && row.Status == 5
                                    && row.IsDeleted == false
                                    orderby row.Id descending
                                    select row).GroupBy(entry => entry.Status).FirstOrDefault().ToList();
                    }
                    else
                    {
                        fileData = (from row in entities.PrerequisiteTran_Upload
                                    where
                                    row.AuditID == auid
                                    && row.checklistId == chkid && row.ATBDId == atbdid
                                    && row.Status == 2
                                    && row.IsDeleted == false
                                    orderby row.Id descending
                                    select row).GroupBy(entry => entry.Status).FirstOrDefault().ToList();
                    }
                }

                if (performerFile.Count > 0)
                {
                    string fileName = performerFile[0].FileName;
                    string filePath = performerFile[0].FilePath;
                    string FileKey = Convert.ToString(performerFile[0].FileKey);
                    string Remark = performerFile[0].Remark;
                    Byte[] bytes = performerFile[0].bytes;

                    foreach (var item in fileData)
                    {
                        PrerequisiteTran_Upload objPrerequisiteTran_Upload = new PrerequisiteTran_Upload();
                        objPrerequisiteTran_Upload.CustomerId = item.CustomerId;
                        objPrerequisiteTran_Upload.checklistName = item.checklistName;
                        objPrerequisiteTran_Upload.checklistId = item.checklistId;
                        objPrerequisiteTran_Upload.ATBDId = item.ATBDId;
                        objPrerequisiteTran_Upload.FileName = fileName; // comment on 15-07-2020
                        objPrerequisiteTran_Upload.FilePath = filePath;// comment on 15-07-2020
                        objPrerequisiteTran_Upload.FileKey = FileKey;
                        objPrerequisiteTran_Upload.Version = item.Version;
                        objPrerequisiteTran_Upload.VersionDate = DateTime.Now;
                        objPrerequisiteTran_Upload.VersionComment = item.VersionComment;
                        objPrerequisiteTran_Upload.CreatedBy = AuthenticationHelper.UserID;
                        objPrerequisiteTran_Upload.CreatedDate = DateTime.Now;
                        //objPrerequisiteTran_Upload.FileKey = item.FileKey; // comment on 15-07-2020
                        objPrerequisiteTran_Upload.AuditID = item.AuditID;
                        objPrerequisiteTran_Upload.IsDeleted = item.IsDeleted;
                        objPrerequisiteTran_Upload.Status = 3;
                        objPrerequisiteTran_Upload.Remark = remark;
                        objPrerequisiteTran_Upload.MAPId = item.MAPId;
                        entities.PrerequisiteTran_Upload.Add(objPrerequisiteTran_Upload);
                        entities.SaveChanges();
                    }

                    #region SaveFile
                    List<InternalFileData_Risk> Workingfiles = new List<InternalFileData_Risk>();
                    List<KeyValuePair<string, int>> Workinglist = new List<KeyValuePair<string, int>>();
                    List<KeyValuePair<string, Byte[]>> WorkingFilelist = new List<KeyValuePair<string, Byte[]>>();
                    var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(atbdid), Convert.ToInt32(auid));

                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = InstanceData.ScheduleonID,
                        ProcessId = InstanceData.ProcessId,
                        FinancialYear = InstanceData.FinancialYear,
                        ForPerid = InstanceData.ForMonth,
                        CustomerBranchId = InstanceData.CustomerBranchID,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = Convert.ToInt32(atbdid),
                        RoleID = 3,
                        InternalAuditInstance = InstanceData.InternalAuditInstance,
                        VerticalID = InstanceData.VerticalId,
                        AuditID = Convert.ToInt32(auid),
                    };
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        AuditScheduleOnID = InstanceData.ScheduleonID,
                        FinancialYear = InstanceData.FinancialYear,
                        CustomerBranchId = InstanceData.CustomerBranchID,
                        InternalAuditInstance = InstanceData.InternalAuditInstance,
                        ProcessId = InstanceData.ProcessId,
                        ForPeriod = InstanceData.ForMonth,
                        ATBDId = Convert.ToInt32(atbdid),
                        RoleID = 3,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = InstanceData.VerticalId,
                        AuditID = Convert.ToInt32(auid)
                    };

                    MstRiskResult.AuditObjective = null;
                    MstRiskResult.AuditSteps = "";
                    MstRiskResult.AnalysisToBePerofrmed = "";
                    MstRiskResult.ProcessWalkthrough = "";
                    MstRiskResult.ActivityToBeDone = "";
                    MstRiskResult.Population = "";
                    MstRiskResult.Sample = "";
                    MstRiskResult.ObservationNumber = "";
                    MstRiskResult.ObservationTitle = "";
                    MstRiskResult.Observation = "";
                    MstRiskResult.Risk = "";
                    MstRiskResult.RootCost = null;
                    MstRiskResult.AuditScores = null;
                    MstRiskResult.FinancialImpact = null;
                    MstRiskResult.Recomendation = "";
                    MstRiskResult.ManagementResponse = "";
                    MstRiskResult.FixRemark = "";
                    MstRiskResult.TimeLine = null;
                    MstRiskResult.PersonResponsible = null;
                    MstRiskResult.Owner = null;
                    MstRiskResult.ObservationRating = null;
                    MstRiskResult.ObservationCategory = null;
                    MstRiskResult.ObservationSubCategory = null;
                    MstRiskResult.AStatusId = 2;

                    transaction.PersonResponsible = null;
                    transaction.Owner = null;
                    transaction.ObservatioRating = null;
                    transaction.ObservationCategory = null;
                    transaction.ObservationSubCategory = null;

                    List<PrerequisiteTran_Upload> AnnxetureList = new List<PrerequisiteTran_Upload>();
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();

                    int customerid = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    string directoryPathIFD = "";
                    directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + customerid + "/"
                                        + InstanceData.CustomerBranchID.ToString() + "/"
                                        + InstanceData.VerticalId.ToString() + "/"
                                        + InstanceData.FinancialYear + "/"
                                        + InstanceData.ProcessId.ToString() + "/"
                                        + InstanceData.ScheduleonID + "/2.0");

                    if (!Directory.Exists(directoryPathIFD))
                    {
                        DocumentManagement.CreateDirectory(directoryPathIFD);
                    }

                    string directoryPath = "";

                    if (!string.IsNullOrEmpty(Convert.ToString(auid)))
                    {
                        directoryPath = Server.MapPath("~/AuditPreRequisiteDocument/" + customerid + "/"
                          + Convert.ToInt32(auid) + "/" + Convert.ToInt32(atbdid) + "/"
                          + Convert.ToInt32(chkid) + "/2.0");
                    }
                    if (!Directory.Exists(directoryPath))
                    {
                        DocumentManagement.CreateDirectory(directoryPath);
                    }

                    string finalPath = Path.Combine(directoryPath, FileKey + Path.GetExtension(fileName));
                    string finalPathPR = Path.Combine(directoryPathIFD, FileKey + Path.GetExtension(fileName));

                    Workinglist.Add(new KeyValuePair<string, int>(fileName, 1));
                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                    WorkingFilelist.Add(new KeyValuePair<string, Byte[]>(finalPathPR, bytes));

                    InternalFileData_Risk file = new InternalFileData_Risk()
                    {
                        Name = fileName,
                        FilePath = directoryPathIFD.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                        FileKey = FileKey.ToString(),
                        Version = "0.0",
                        VersionDate = DateTime.UtcNow,
                        ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                        CustomerBranchId = Convert.ToInt32(InstanceData.CustomerBranchID),
                        FinancialYear = InstanceData.FinancialYear,
                        IsDeleted = false,
                        ATBDId = Convert.ToInt32(atbdid),
                        VerticalID = InstanceData.VerticalId,
                        AuditID = Convert.ToInt32(auid),
                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        //UploadedDocumentFlag = "Open"
                    };

                    #region added by sagar on 23-07-2020
                    if (!IsRecordExists(file.ProcessID, file.FinancialYear, file.ATBDId, Convert.ToInt32(file.AuditID), Convert.ToInt32(file.VerticalID)))
                    {
                        file.UploadedDocumentFlag = "Open";
                    }
                    #endregion

                    Workingfiles.Add(file);

                    var RCT = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(InstanceData.ScheduleonID), Convert.ToInt32(atbdid));

                    if (RCT == null)
                    {
                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        }
                        if (WorkingFilelist != null && Workinglist != null)
                        {
                            if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                            {
                                transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.UpdatedOn = DateTime.Now;
                                UpdateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                            }
                            else
                            {
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                                CreateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                            }
                        }
                    }
                    else
                    {
                        UpdateTransaction(RCT.AuditTransactionID, (long)RCT.AuditScheduleOnID, Workingfiles, Workinglist, WorkingFilelist);
                    }
                    if (Filelist.Count > 0)
                    {
                        DocumentManagement.Audit_SaveDocFiles(Filelist);
                    }
                    bool Success1 = false;
                    foreach (var item in AnnxetureList)
                    {
                        if (!Tran_PrerequisiteTran_UploadExists(item))
                        {
                            Success1 = CreatePrerequisiteTran_UploadResult(item);
                            if (Success1 == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "File Uploaded Successfully";
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Same Name Already Exists..";
                        }
                    }
                }

                #endregion

                else
                {
                    foreach (var item in fileData)
                    {
                        PrerequisiteTran_Upload objPrerequisiteTran_Upload = new PrerequisiteTran_Upload();
                        objPrerequisiteTran_Upload.CustomerId = item.CustomerId;
                        objPrerequisiteTran_Upload.checklistName = item.checklistName;
                        objPrerequisiteTran_Upload.checklistId = item.checklistId;
                        objPrerequisiteTran_Upload.ATBDId = item.ATBDId;
                        objPrerequisiteTran_Upload.FileName = "NA"; // comment on 15-07-2020
                                                                    //objPrerequisiteTran_Upload.FilePath = item.FilePath;// comment on 15-07-2020
                        objPrerequisiteTran_Upload.Version = item.Version;
                        objPrerequisiteTran_Upload.VersionDate = DateTime.Now;
                        objPrerequisiteTran_Upload.VersionComment = item.VersionComment;
                        objPrerequisiteTran_Upload.CreatedBy = AuthenticationHelper.UserID;
                        objPrerequisiteTran_Upload.CreatedDate = DateTime.Now;
                        //objPrerequisiteTran_Upload.FileKey = item.FileKey; // comment on 15-07-2020
                        objPrerequisiteTran_Upload.AuditID = item.AuditID;
                        objPrerequisiteTran_Upload.IsDeleted = item.IsDeleted;
                        objPrerequisiteTran_Upload.Status = 3;
                        objPrerequisiteTran_Upload.Remark = remark;
                        objPrerequisiteTran_Upload.MAPId = item.MAPId;
                        entities.PrerequisiteTran_Upload.Add(objPrerequisiteTran_Upload);
                        entities.SaveChanges();
                    }
                }
            }
        }


        public void DeleteUpdatePrerequisiteTran_Upload(long auid, long chkid, long atbdid, int status, string remark, List<PerformerFileUpload> performerFile)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<PrerequisiteTran_Upload> fileData = new List<PrerequisiteTran_Upload>();
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.Status == 2 && row.checklistId == chkid
                             && row.AuditID == auid
                             && row.ATBDId == atbdid
                             select row).ToList();
                if (query.Count > 0)
                {
                    var Subquery = (from row in entities.PrerequisiteTran_Upload
                                    where row.Status == 5 && row.checklistId == chkid
                                    && row.AuditID == auid
                                    && row.ATBDId == atbdid
                                    select row).ToList();
                    if (Subquery.Count > 0)
                    {
                        fileData = (from row in entities.PrerequisiteTran_Upload
                                    where
                                    row.AuditID == auid
                                    && row.checklistId == chkid && row.ATBDId == atbdid
                                    && row.Status == 5
                                    && row.IsDeleted == false
                                    orderby row.Id descending
                                    select row).GroupBy(entry => entry.Status).FirstOrDefault().ToList();
                    }
                    else
                    {
                        fileData = (from row in entities.PrerequisiteTran_Upload
                                    where
                                    row.AuditID == auid
                                    && row.checklistId == chkid && row.ATBDId == atbdid
                                    && row.Status == 2
                                    && row.IsDeleted == false
                                    orderby row.Id descending
                                    select row).GroupBy(entry => entry.Status).FirstOrDefault().ToList();
                    }
                }


                if (performerFile.Count > 0)
                {
                    string fileName = performerFile[0].FileName;
                    string filePath = performerFile[0].FilePath;
                    string FileKey = Convert.ToString(performerFile[0].FileKey);
                    string Remark = performerFile[0].Remark;
                    Byte[] bytes = performerFile[0].bytes;

                    foreach (var item in fileData)
                    {
                        PrerequisiteTran_Upload objPrerequisiteTran_Upload = new PrerequisiteTran_Upload();
                        objPrerequisiteTran_Upload.CustomerId = item.CustomerId;
                        objPrerequisiteTran_Upload.checklistName = item.checklistName;
                        objPrerequisiteTran_Upload.checklistId = item.checklistId;
                        objPrerequisiteTran_Upload.ATBDId = item.ATBDId;
                        objPrerequisiteTran_Upload.FileName = fileName; // comment on 15-07-2020
                        objPrerequisiteTran_Upload.FilePath = filePath;// comment on 15-07-2020
                        objPrerequisiteTran_Upload.FileKey = FileKey;
                        objPrerequisiteTran_Upload.Version = item.Version;
                        objPrerequisiteTran_Upload.VersionDate = DateTime.Now;
                        objPrerequisiteTran_Upload.VersionComment = item.VersionComment;
                        objPrerequisiteTran_Upload.CreatedBy = AuthenticationHelper.UserID;
                        objPrerequisiteTran_Upload.CreatedDate = DateTime.Now;
                        //objPrerequisiteTran_Upload.FileKey = item.FileKey; // comment on 15-07-2020
                        objPrerequisiteTran_Upload.AuditID = item.AuditID;
                        objPrerequisiteTran_Upload.IsDeleted = item.IsDeleted;
                        objPrerequisiteTran_Upload.Status = 4;
                        objPrerequisiteTran_Upload.Remark = remark;
                        objPrerequisiteTran_Upload.MAPId = item.MAPId;
                        entities.PrerequisiteTran_Upload.Add(objPrerequisiteTran_Upload);
                        entities.SaveChanges();
                    }

                    #region SaveFile
                    List<InternalFileData_Risk> Workingfiles = new List<InternalFileData_Risk>();
                    List<KeyValuePair<string, int>> Workinglist = new List<KeyValuePair<string, int>>();
                    List<KeyValuePair<string, Byte[]>> WorkingFilelist = new List<KeyValuePair<string, Byte[]>>();
                    var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(atbdid), Convert.ToInt32(auid));

                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = InstanceData.ScheduleonID,
                        ProcessId = InstanceData.ProcessId,
                        FinancialYear = InstanceData.FinancialYear,
                        ForPerid = InstanceData.ForMonth,
                        CustomerBranchId = InstanceData.CustomerBranchID,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = Convert.ToInt32(atbdid),
                        RoleID = 3,
                        InternalAuditInstance = InstanceData.InternalAuditInstance,
                        VerticalID = InstanceData.VerticalId,
                        AuditID = Convert.ToInt32(auid),
                    };
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        AuditScheduleOnID = InstanceData.ScheduleonID,
                        FinancialYear = InstanceData.FinancialYear,
                        CustomerBranchId = InstanceData.CustomerBranchID,
                        InternalAuditInstance = InstanceData.InternalAuditInstance,
                        ProcessId = InstanceData.ProcessId,
                        ForPeriod = InstanceData.ForMonth,
                        ATBDId = Convert.ToInt32(atbdid),
                        RoleID = 3,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = InstanceData.VerticalId,
                        AuditID = Convert.ToInt32(auid)
                    };

                    MstRiskResult.AuditObjective = null;
                    MstRiskResult.AuditSteps = "";
                    MstRiskResult.AnalysisToBePerofrmed = "";
                    MstRiskResult.ProcessWalkthrough = "";
                    MstRiskResult.ActivityToBeDone = "";
                    MstRiskResult.Population = "";
                    MstRiskResult.Sample = "";
                    MstRiskResult.ObservationNumber = "";
                    MstRiskResult.ObservationTitle = "";
                    MstRiskResult.Observation = "";
                    MstRiskResult.Risk = "";
                    MstRiskResult.RootCost = null;
                    MstRiskResult.AuditScores = null;
                    MstRiskResult.FinancialImpact = null;
                    MstRiskResult.Recomendation = "";
                    MstRiskResult.ManagementResponse = "";
                    MstRiskResult.FixRemark = "";
                    MstRiskResult.TimeLine = null;
                    MstRiskResult.PersonResponsible = null;
                    MstRiskResult.Owner = null;
                    MstRiskResult.ObservationRating = null;
                    MstRiskResult.ObservationCategory = null;
                    MstRiskResult.ObservationSubCategory = null;
                    MstRiskResult.AStatusId = 2;

                    transaction.PersonResponsible = null;
                    transaction.Owner = null;
                    transaction.ObservatioRating = null;
                    transaction.ObservationCategory = null;
                    transaction.ObservationSubCategory = null;

                    List<PrerequisiteTran_Upload> AnnxetureList = new List<PrerequisiteTran_Upload>();
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();

                    int customerid = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    string directoryPathIFD = "";
                    directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + customerid + "/"
                                        + InstanceData.CustomerBranchID.ToString() + "/"
                                        + InstanceData.VerticalId.ToString() + "/"
                                        + InstanceData.FinancialYear + "/"
                                        + InstanceData.ProcessId.ToString() + "/"
                                        + InstanceData.ScheduleonID + "/2.0");

                    if (!Directory.Exists(directoryPathIFD))
                    {
                        DocumentManagement.CreateDirectory(directoryPathIFD);
                    }

                    string directoryPath = "";

                    if (!string.IsNullOrEmpty(Convert.ToString(auid)))
                    {
                        directoryPath = Server.MapPath("~/AuditPreRequisiteDocument/" + customerid + "/"
                          + Convert.ToInt32(auid) + "/" + Convert.ToInt32(atbdid) + "/"
                          + Convert.ToInt32(chkid) + "/2.0");
                    }
                    if (!Directory.Exists(directoryPath))
                    {
                        DocumentManagement.CreateDirectory(directoryPath);
                    }

                    string finalPath = Path.Combine(directoryPath, FileKey + Path.GetExtension(fileName));
                    string finalPathPR = Path.Combine(directoryPathIFD, FileKey + Path.GetExtension(fileName));

                    Workinglist.Add(new KeyValuePair<string, int>(fileName, 1));
                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                    WorkingFilelist.Add(new KeyValuePair<string, Byte[]>(finalPathPR, bytes));

                    InternalFileData_Risk file = new InternalFileData_Risk()
                    {
                        Name = fileName,
                        FilePath = directoryPathIFD.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                        FileKey = FileKey.ToString(),
                        Version = "0.0",
                        VersionDate = DateTime.UtcNow,
                        ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                        CustomerBranchId = Convert.ToInt32(InstanceData.CustomerBranchID),
                        FinancialYear = InstanceData.FinancialYear,
                        IsDeleted = false,
                        ATBDId = Convert.ToInt32(atbdid),
                        VerticalID = InstanceData.VerticalId,
                        AuditID = Convert.ToInt32(auid),
                        CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        //UploadedDocumentFlag = "Open"
                    };


                    if (!IsRecordExists(file.ProcessID, file.FinancialYear, file.ATBDId, Convert.ToInt32(file.AuditID), Convert.ToInt32(file.VerticalID)))
                    {
                        file.UploadedDocumentFlag = "Open";
                    }
                    #endregion

                    Workingfiles.Add(file);

                    var RCT = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(InstanceData.ScheduleonID), Convert.ToInt32(atbdid));

                    if (RCT == null)
                    {
                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        }
                        if (WorkingFilelist != null && Workinglist != null)
                        {
                            if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                            {
                                transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.UpdatedOn = DateTime.Now;
                                UpdateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                            }
                            else
                            {
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                                CreateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                            }
                        }
                    }
                    else
                    {
                        UpdateTransaction(RCT.AuditTransactionID, (long)RCT.AuditScheduleOnID, Workingfiles, Workinglist, WorkingFilelist);
                    }
                    if (Filelist.Count > 0)
                    {
                        DocumentManagement.Audit_SaveDocFiles(Filelist);
                    }
                    bool Success1 = false;
                    foreach (var item in AnnxetureList)
                    {
                        if (!Tran_PrerequisiteTran_UploadExists(item))
                        {
                            Success1 = CreatePrerequisiteTran_UploadResult(item);
                            if (Success1 == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "File Uploaded Successfully";
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Same Name Already Exists..";
                        }
                    }
                }
                else
                {
                    foreach (var item in fileData)
                    {
                        PrerequisiteTran_Upload objPrerequisiteTran_Upload = new PrerequisiteTran_Upload();
                        objPrerequisiteTran_Upload.CustomerId = item.CustomerId;
                        objPrerequisiteTran_Upload.checklistName = item.checklistName;
                        objPrerequisiteTran_Upload.checklistId = item.checklistId;
                        objPrerequisiteTran_Upload.ATBDId = item.ATBDId;
                        objPrerequisiteTran_Upload.FileName = "NA";// comment on 15-07-2020
                                                                   //objPrerequisiteTran_Upload.FilePath = item.FilePath;// comment on 15-07-2020
                        objPrerequisiteTran_Upload.Version = item.Version;
                        objPrerequisiteTran_Upload.VersionDate = DateTime.Now;
                        objPrerequisiteTran_Upload.VersionComment = item.VersionComment;
                        objPrerequisiteTran_Upload.CreatedBy = AuthenticationHelper.UserID;
                        objPrerequisiteTran_Upload.CreatedDate = DateTime.Now;
                        // objPrerequisiteTran_Upload.FileKey = item.FileKey;// comment on 15-07-2020
                        objPrerequisiteTran_Upload.AuditID = item.AuditID;
                        objPrerequisiteTran_Upload.IsDeleted = item.IsDeleted;
                        objPrerequisiteTran_Upload.Status = 4;
                        objPrerequisiteTran_Upload.Remark = remark;
                        objPrerequisiteTran_Upload.MAPId = item.MAPId;
                        entities.PrerequisiteTran_Upload.Add(objPrerequisiteTran_Upload);
                        entities.SaveChanges();
                    }
                }


            }
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                var CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ATID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["CHID"]))
                    {
                        ChecklistID = Convert.ToInt32(Request.QueryString["CHID"]);
                    }
                    else
                    {
                        ChecklistID = Convert.ToInt32(ViewState["ChecklistID"]);
                    }
                    if (string.IsNullOrEmpty(txtRemark.Text))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please enter remark.";
                        return;
                    }

                    if (fileUploadDocument.HasFile)
                    {
                        HttpFileCollection fileCollection = Request.Files;
                        if (fileCollection.Count > 0)
                        {
                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                HttpPostedFile uploadfile = fileCollection[i];
                                string[] keys = fileCollection.Keys[i].Split('$');
                                //String fileName = "";
                                if (keys[keys.Count() - 1].Equals("fileUploadDocument"))
                                {
                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Session["bytes"] = bytes;
                                }

                            }
                            var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(ATBDID), Convert.ToInt32(AuditID));
                            string directoryPathIFD = "";
                            directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalId.ToString() + "/" + InstanceData.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ScheduleonID + "/2.0");
                            if (!Directory.Exists(directoryPathIFD))
                            {
                                DocumentManagement.CreateDirectory(directoryPathIFD);
                            }


                            string directoryPath = "";

                            if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                            {
                                directoryPath = Server.MapPath("~/AuditPreRequisiteDocument/" + CustomerId + "/"
                                  + Convert.ToInt32(AuditID) + "/" + Convert.ToInt32(ATBDID) + "/"
                                  + Convert.ToInt32(ChecklistID) + "/2.0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }

                            string fileName = fileUploadDocument.FileName;
                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(fileUploadDocument.FileName));
                            string finalPathPR = Path.Combine(directoryPathIFD, fileKey + Path.GetExtension(fileUploadDocument.FileName));
                            string Remark = string.Empty;
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                Remark = txtRemark.Text.Trim();
                            }
                            else
                            {
                                Remark = "NA";
                            }

                            PerformerFileUpload pFile = new PerformerFileUpload
                            {
                                CustomerId = Convert.ToInt32(CustomerId),
                                AuditID = Convert.ToInt32(AuditID),
                                ATBDID = Convert.ToInt32(ATBDID),
                                ChecklistID = Convert.ToInt32(ChecklistID),
                                FileName = fileName,
                                FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                FileKey = fileKey,
                                Remark = Remark,
                                bytes = (Byte[])Session["bytes"]
                            };

                            performerFile.Add(pFile);
                        }
                    }

                    if (!string.IsNullOrEmpty(txtRemark.Text))
                    {
                        DeleteUpdatePrerequisiteTran_Upload(AuditID, ChecklistID, ATBDID, 4, txtRemark.Text.Trim(), performerFile);
                        UpdateFlagOfUploadedDocument(AuditID, ATBDID, "Rejected");
                        BindDocument(ChecklistID, ATBDID, AuditID);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Rejected Sucessfully.";
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                        txtRemark.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static SP_getinstancedataprerequsite_Result GetInternalAuditInstanceData(long atbdid, long auditid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var instanceData = (from row in entities.SP_getinstancedataprerequsite(atbdid, auditid)
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        #region
        private void UpdateFlagOfUploadedDocument(long AuditID, long ATBDId, string Flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.InternalFileData_Risk
                             where cs.ATBDId == ATBDId && cs.AuditID == AuditID
                             select cs).ToList();
                if (query.Count > 0)
                {
                    foreach (var item in query)
                    {
                        item.UploadedDocumentFlag = Flag;
                        entities.SaveChanges();
                    }
                }
            }
        }
        #endregion

        public static bool UpdateTransaction(long transactionid, long Scheduleonid, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            long transactionId = transactionid;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = Scheduleonid;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                                       where row.ProcessId == transaction.ProcessId && row.FinancialYear == transaction.FinancialYear
                                                                           && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                                                                           && row.UserID == transaction.UserID && row.RoleID == transaction.RoleID && row.ATBDId == transaction.ATBDId
                                                                       select row).FirstOrDefault();

                            RecordtoUpdate.PersonResponsible = transaction.PersonResponsible;
                            RecordtoUpdate.ObservatioRating = transaction.ObservatioRating;
                            RecordtoUpdate.ObservationCategory = transaction.ObservationCategory;
                            RecordtoUpdate.ForPeriod = transaction.ForPeriod;
                            RecordtoUpdate.Dated = DateTime.Now;
                            long transactionId = RecordtoUpdate.ID;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool IsRecordExists(long ProcessID, string FinancialYear, long ATBDId, long AuditID, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.InternalFileData_Risk
                             where cs.ProcessID == ProcessID
                             && cs.FinancialYear == FinancialYear
                             && cs.ATBDId == ATBDId
                             && cs.AuditID == AuditID
                             && cs.VerticalID == VerticalID
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CreateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files,
            List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.InternalAuditTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateTransaction(List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, long ScheduleonID, long atbdid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var instanceData = (from row in entities.InternalAuditTransactions
                                                where row.AuditScheduleOnID == ScheduleonID
                                                && row.ATBDId == atbdid
                                                select row).OrderByDescending(a => a.ID).Take(1).FirstOrDefault();

                            if (instanceData != null)
                            {
                                DocumentManagement.Audit_SaveDocFiles(filesList);
                                if (files != null)
                                {
                                    foreach (InternalFileData_Risk fl in files)
                                    {
                                        fl.IsDeleted = false;
                                        entities.InternalFileData_Risk.Add(fl);
                                        entities.SaveChanges();

                                        InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                        fileMapping.TransactionID = instanceData.ID;
                                        fileMapping.FileID = fl.ID;
                                        fileMapping.ScheduledOnID = ScheduleonID;
                                        if (list != null)
                                        {
                                            fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                        }
                                        entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                    }
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool Tran_PrerequisiteTran_UploadExists(PrerequisiteTran_Upload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == ICR.AuditID
                             && row.ATBDId == ICR.ATBDId
                             && row.checklistId == ICR.checklistId
                             && row.FileName == ICR.FileName
                             && row.IsDeleted == false
                             && row.Status != 1
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CreatePrerequisiteTran_UploadResult(PrerequisiteTran_Upload DraftClosureResult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.PrerequisiteTran_Upload.Add(DraftClosureResult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

    }

    public class PerformerFileUpload
    {
        public int CustomerId { get; set; }
        public int AuditID { get; set; }
        public int ATBDID { get; set; }
        public int ChecklistID { get; set; }
        public string FileName { get; set; }
        public Guid FileKey { get; set; }
        public string FilePath { get; set; }
        public string Remark { get; set; }

        public Byte[] bytes { get; set; }
    }
}
