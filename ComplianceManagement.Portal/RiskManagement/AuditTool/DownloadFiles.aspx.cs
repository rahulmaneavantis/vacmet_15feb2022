﻿using com.VirtuosoITech.ComplianceManagement.Business;
using Spire.Pdf.Exporting.XPS.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class DownloadFiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int FileID = -1;
                string TypeOfFile = string.Empty;
                string FileType = string.Empty;
                int ATBDID = -1;
                int ScheduleOnID = -1;
                int AuditID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["FileID"]))
                {
                    FileID = Convert.ToInt32(Request.QueryString["FileID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["TypeOfFile"]))
                {
                    TypeOfFile = Convert.ToString(Request.QueryString["TypeOfFile"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FileType"]))
                {
                    FileType = Convert.ToString(Request.QueryString["FileType"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ScheduleOnID"]))
                {
                    ScheduleOnID = Convert.ToInt32(Request.QueryString["ScheduleOnID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                if (FileID != -1 && FileType == "WF1")
                {
                    var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(FileID);
                    if (file.FilePath != null)
                    {
                        string filePath = System.IO.Path.Combine(Server.MapPath(file.FilePath), file.FileKey + System.IO.Path.GetExtension(file.Name));
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            Response.End();
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('There is no file for download')", true);
                        return;
                    }
                }
                else if (FileID != -1 && FileType == "ANX")
                {
                    try
                    {
                        var file = Business.RiskCategoryManagement.GetFileInternalFileData_Annxeture(FileID);

                        if (file.FilePath != null)
                        {
                            string filePath = System.IO.Path.Combine(Server.MapPath(file.FilePath), file.FileKey + System.IO.Path.GetExtension(file.FileName));
                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                                }
                                Response.Flush(); // send it to the client to download
                                Response.End();
                            }
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('There is no file for download')", true);
                            return;
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                else if (FileID != -1 && FileType == "RHist")
                {
                    var file = RiskCategoryManagement.GetReviewHistoryFileData(FileID);
                    if (file.FilePath != null)
                    {
                        string filePath = System.IO.Path.Combine(Server.MapPath(file.FilePath), file.FileKey + System.IO.Path.GetExtension(file.Name));
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            Response.End();
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('There is no file for download')", true);
                        return;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}