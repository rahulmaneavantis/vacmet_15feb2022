﻿<%@ Page Title="Testing Status Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="TestingStatusReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.TestingStatusReport" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
       <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">
       
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
      
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>


    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Reports / Testing Status Report');

        });

    </script>

    <style type="text/css">
        .td1 {
            width: 5%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 5%;
        }

        .td4 {
            width: 25%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">       
                              <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                                <li class="active" id="liPerformer" runat="server">
                                                    <asp:LinkButton ID="lnkPerformer"  PostBackUrl="../../RiskManagement/AuditTool/TestingStatusReport.aspx"   runat="server">Testing Status</asp:LinkButton>                                           
                                                </li>
                                                <li class="" id="li1" runat="server">
                                                    <asp:LinkButton ID="LinkButton1" PostBackUrl="../../RiskManagement/AuditTool/PendingTestingReport.aspx"    runat="server">Pending Testing</asp:LinkButton>                                           
                                                </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                                <li class="active"  id="liReviewer" runat="server">
                                                    <asp:LinkButton ID="lnkReviewer" PostBackUrl="../../RiskManagement/AuditTool/ReviewerCommentReport.aspx" runat="server">Reviewer Comments</asp:LinkButton>                                        
                                                </li>
                                              <%-- <li class=""  id="li2" runat="server">
                                                    <asp:LinkButton ID="LinkButton2" PostBackUrl="../../RiskManagement/AuditTool/FailedControlReportReviewer" runat="server">Failed Control</asp:LinkButton>                                        
                                                </li>--%>
                                          <%}%>
                                    </ul>
                          </header>  
                                <div class="clearfix"></div>   
                                <div style="float:right;">                     
                                    <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" ValidationGroup="ComplianceInstanceValidationGroup" style="margin-top: 5px;  width: 126px;" OnClick="lbtnExportExcel_Click" runat="server"/>
                                </div>   
                                <div class="clearfix"></div>                                  
                                <div style="margin-bottom: 4px"/> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:150px">
                                        <div class="col-md-3 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="80%" Height="32px"
                                        AutoPostBack="true" Style="background:none;"   DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="80%" Height="32px"
                                        AutoPostBack="true"   DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="80%" Height="32px"
                                        AutoPostBack="true"   DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="80%" Height="32px"
                                        AutoPostBack="true"   DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                        DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                    
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial"  AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged"  class="form-control m-bot15" Width="80%" DataPlaceHolder="Financial year">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                        Display="None" />
                                    </div>
                                </div>
                                <div>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                                </div>
                                <div class="clearfix"></div>       
                    
                                <div class="clearfix"></div>                       
                                <div style="margin-bottom: 4px">
                                    <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table"  Width="100%"
                                    DataKeyNames="RiskCreationId" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RiskCategoryCreationId" SortExpression="RiskCreationId" Visible="false">
                                            <ItemTemplate>                                       
                                                <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%# Eval("RiskCreationId")%>'></asp:Label>        
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Branch")%>'></asp:Label>                                        
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("FinancialYear")%>'></asp:Label>                                                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ControlNo">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 125px;">
                                                    <asp:Label ID="lblControlNo" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' tooltip='<%# Eval("ControlNo")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                
                                        <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 125px;">
                                                    <asp:Label ID="lblProcess" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Process")%>' tooltip='<%# Eval("Process")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sub Process">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 125px;">
                                                    <asp:Label ID="lblSubProcess" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SubProcess")%>' tooltip='<%# Eval("SubProcess")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Risk Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription")%>' tooltip='<%# Eval("ActivityDescription")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Control Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblControlDescription" data-toggle="tooltip" data-placement="bottom" runat="server" Text='<%# Eval("ControlDescription")%>' tooltip='<%# Eval("ControlDescription")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Key / Non Key">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                    <asp:Label ID="lblKeyName" runat="server" Text='<%# Eval("KeyName")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TOD (Pass or Fail)" Visible="false">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                    <asp:Label ID="lblTOD" runat="server" Text='<%# Eval("TOD")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TOE (Pass or Fail)" Visible="false">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:70px;">
                                                    <asp:Label ID="lblTOE" runat="server" Text='<%# Eval("TOE")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                               
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />      
                                          <PagerSettings Visible="false" />                
                                    <PagerTemplate>
                                       <%-- <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </PagerTemplate> 
                                    <EmptyDataTemplate>
                                    No Record Found.
                                    </EmptyDataTemplate>
                                    </asp:GridView>
                                 <div style="float: right;">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                    </asp:DropDownListChosen>  
                                </div>
                                </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-5 colpadding0">
                                        <div class="table-Selecteddownload">
                                            <div class="table-Selecteddownload-text">
                                                <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                            </div>                                   
                                        </div>
                                    </div>
                                    <div class="col-md-6 colpadding0" style="float:right;">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                            <div class="table-paging-text" style="float:right;">
                                                <p>Page
                                                   <%-- <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                    <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                                </p>
                                            </div>
                                            <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
