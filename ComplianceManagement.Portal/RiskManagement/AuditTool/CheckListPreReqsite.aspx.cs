﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class CheckListPreReqsite : System.Web.UI.Page
    {
        protected int auditID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                BindPerformerFilter(ddlPerformerFilter);
                BindReviewerFilter();
                BindProcessAudits();
                bindPageNumber();
                BindProcess(customerID);
                BindData();
            }
        }
        public static object FillProcessDropdownPerformerAndReviewer(int Customerid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.AuditID == AuditID
                         && row.UserID == AuthenticationHelper.UserID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillSubProcess(long Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false && row.ProcessId == Processid
                             select row);
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }

        private void BindProcess(int CustomerID)
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);

                    var data = FillProcessDropdownPerformerAndReviewer(CustomerID, auditID);

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();

                    ddlSubProcess1.Items.Clear();
                    ddlProcess1.Items.Clear();

                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";

                    ddlProcess1.DataTextField = "Name";
                    ddlProcess1.DataValueField = "Id";

                    ddlProcess.DataSource = data;
                    ddlProcess.DataBind();

                    ddlProcess1.DataSource = data;
                    ddlProcess1.DataBind();

                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                    ddlProcess1.Items.Insert(0, new ListItem("Select Process", "-1"));

                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
                    }
                    if (!String.IsNullOrEmpty(ddlProcess1.SelectedValue))
                    {
                        BindSubProcess1(Convert.ToInt32(ddlProcess1.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid)
        {
            try
            {
                int AuditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    var data = ProcessManagement.FillSubProcessUserWise(Processid, "P", AuditID, Portal.Common.AuthenticationHelper.UserID);
                    //var data = FillSubProcess(Processid);
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = data;
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess1(long Processid)
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    var data = ProcessManagement.FillSubProcessUserWise(Processid, "P", auditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlSubProcess1.Items.Clear();
                    ddlSubProcess1.DataTextField = "Name";
                    ddlSubProcess1.DataValueField = "Id";
                    ddlSubProcess1.DataSource = data;
                    ddlSubProcess1.DataBind();
                    ddlSubProcess1.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            BindData();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Resetbuttonminisize();", true);
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Resetbuttonminisize();", true);
        }

        protected void ddlProcess1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess1.SelectedValue))
            {
                if (ddlProcess1.SelectedValue != "-1")
                {
                    BindSubProcess1(Convert.ToInt32(ddlProcess1.SelectedValue));
                }
                else
                {
                    ddlSubProcess1.Items.Clear();
                    ddlSubProcess1.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess1.Items.Clear();
                ddlSubProcess1.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            BindProcessAudits();
        }
        protected void ddlSubProcess1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudits();
        }
        private void BindData()
        {
            try
            {
                long processid = -1;
                long subprocessid = -1;
                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    string FinYear = string.Empty;
                    var AuditLists = GetAudits(FinYear, auditID);
                    if (processid != -1)
                    {
                        AuditLists = AuditLists.Where(entry => entry.ProcessID == processid).ToList();
                    }
                    if (subprocessid != -1)
                    {
                        AuditLists = AuditLists.Where(entry => entry.SubprocessID == subprocessid).ToList();
                    }
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdRiskActivityMatrix.DataBind();
                    AuditLists.Clear();
                    AuditLists = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<SP_PrequsiteReportData_Result> GetAudits(string FinYear, long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<SP_PrequsiteReportData_Result> Masterrecord = new List<SP_PrequsiteReportData_Result>();

                entities.Database.CommandTimeout = 300;
                Masterrecord = (from row in entities.SP_PrequsiteReportData(auditID, Portal.Common.AuthenticationHelper.UserID)
                                select row).ToList();
                
                if (FinYear != "")
                    Masterrecord = Masterrecord.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                return Masterrecord;
            }
        }

        public void BindPerformerFilter(DropDownList ddl)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            ddl.Items.Clear();
            ddl.DataTextField = "Name";
            ddl.DataValueField = "ID";
            ddl.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("Auditee ", "-1"));

        }
        public void BindReviewerFilter()
        {

            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            ddlReviewerFilter.Items.Clear();
            ddlReviewerFilter.DataTextField = "Name";
            ddlReviewerFilter.DataValueField = "ID";
            ddlReviewerFilter.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
            ddlReviewerFilter.DataBind();
            ddlReviewerFilter.Items.Insert(0, new ListItem("Reporting Manager", "-1"));
        }
        public void PerformerReviewerDropDownEnableDisable(DropDownList ddl, int auditID, int Checklistid, Label lbl, String flag, TextBox txt, List<Master_AuditPrerequisite> Records, int ATBDID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (Records.Count > 0)
                    {
                        entities.Database.CommandTimeout = 300;
                        var docupload = (from row in entities.PrerequisiteTran_Upload
                                         where row.AuditID == auditID
                                         && row.checklistId == Checklistid
                                         && row.Status != 1
                                         select row).ToList();

                        List<long> Data = new List<long>();
                        if (flag == "A")
                        {
                            Data = (from row in Records
                                    where row.ChecklistId == Checklistid
                                    && row.Atbdid == ATBDID && row.AuditId == auditID
                                    select (long)row.AuditeeId).Distinct().ToList();
                        }
                        else
                        {
                            Data = (from row in Records
                                    where row.ChecklistId == Checklistid
                                    && row.Atbdid == ATBDID && row.AuditId == auditID
                                    select (long)row.BossId).Distinct().ToList();
                        }
                        if (Data.Count == 1)
                        {
                            if (Data.ElementAt(0) != 0)
                            {
                                ddl.Items.FindByValue(Data.ElementAt(0).ToString()).Selected = true;
                                if (docupload.Count > 0)
                                {
                                    ddl.Enabled = false;
                                    txt.Enabled = false;
                                }
                                else
                                {
                                    ddl.Enabled = true;
                                    txt.Enabled = true;
                                }

                                lbl.Visible = false;
                            }
                        }
                        else if (Data.Count > 1)
                        {
                            ddl.ClearSelection();
                            if (docupload.Count > 0)
                            {
                                ddl.Enabled = false;
                                ddl.Visible = false;

                                txt.Enabled = false;
                                txt.Visible = false;
                            }
                            else
                            {
                                ddl.Enabled = true;
                                ddl.Visible = true;

                                txt.Enabled = true;
                                txt.Visible = true;
                            }

                            if (lbl != null)
                            {
                                lbl.Text = string.Empty;
                                Data.ForEach(EachUserID =>
                                {
                                    if (EachUserID != 0 && EachUserID != -1)
                                    {
                                        var UserDetails = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(EachUserID));
                                        lbl.Text = lbl.Text + UserDetails.FirstName + " " + UserDetails.LastName + ", ";
                                    }
                                });

                                lbl.Text = lbl.Text.Trim(',');
                                lbl.ToolTip = lbl.Text.Trim(',');
                                lbl.Visible = true;
                            }
                        }
                        else
                        {
                            txt.Enabled = true;
                            ddl.Enabled = true;
                            lbl.Visible = false;
                        }
                    }
                }
            }
            catch
            { }
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdProcessAudit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                        Label lblatbdID = (e.Row.FindControl("lblatbdID") as Label);
                        Label lblAuditID = (e.Row.FindControl("lblAuditID") as Label);
                        Label lblChecklistid = (e.Row.FindControl("lblChecklistid") as Label);
                        Label lblPerformerName = (e.Row.FindControl("lblPerformerName") as Label);
                        Label lblReviewerName = (e.Row.FindControl("lblReviewerName") as Label);
                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        TextBox txtStartDate = (e.Row.FindControl("txtStartDate") as TextBox);
                        if (lblStatus != null)
                        {
                            if (!string.IsNullOrEmpty(lblStatus.Text))
                            {
                                if (lblStatus.Text == "Submitted")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.White;
                                }
                                else if (lblStatus.Text == "Open")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.White;
                                }
                                else if (lblStatus.Text == "Re-Submitted")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.Orange;
                                }
                                else if (lblStatus.Text == "Approved")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.Green;

                                }
                                else if (lblStatus.Text == "Rejected")
                                {
                                    lblStatus.Text = "";
                                    lblStatus.BackColor = Color.Red;

                                }
                            }
                        }
                        List<Master_AuditPrerequisite> Records = new List<Master_AuditPrerequisite>();
                        if (HttpContext.Current.Cache["ProcessAuditPrerequisiteData"] == null)
                        {
                            int auditID = Convert.ToInt32(lblAuditID.Text);
                            Records = (from row in entities.Master_AuditPrerequisite.AsNoTracking()
                                       where row.AuditId == auditID
                                       select row).OrderByDescending(entry => entry.Timeline).ToList();

                            HttpContext.Current.Cache.Insert("ProcessAuditPrerequisiteData", Records, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero); // add it to cache
                        }
                        else
                            Records = (List<Master_AuditPrerequisite>)HttpContext.Current.Cache["ProcessAuditPrerequisiteData"];


                        #region Audit
                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem("Auditee", "-1"));

                            if (lblAuditID != null)
                            {
                                PerformerReviewerDropDownEnableDisable(ddlPerformer, Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblChecklistid.Text), lblPerformerName, "A", txtStartDate, Records, Convert.ToInt32(lblatbdID.Text));
                            }
                        }

                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("Reporting Manager", "-1"));

                            if (lblAuditID != null)
                            {
                                PerformerReviewerDropDownEnableDisable(ddlReviewer, Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblChecklistid.Text), lblReviewerName, "B", txtStartDate, Records, Convert.ToInt32(lblatbdID.Text));
                            }
                        }

                        int checklistid = Convert.ToInt32(lblChecklistid.Text);
                        var timelinedetails = Records.Where(entry => entry.ChecklistId == checklistid).FirstOrDefault();
                        if (timelinedetails != null)
                        {
                            txtStartDate.Text = timelinedetails.Timeline != null ? Convert.ToDateTime(timelinedetails.Timeline).ToString("dd-MM-yyyy") : "";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcessAudit.PageIndex = chkSelectedPage - 1;
            grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindProcessAudits();
        }
        protected void grdProcessAudit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("SHOW_SCHEDULE"))
            {
                //CommandArgument = '<%# Eval("AuditID") +"," + Eval("ATBDID")+","+Eval("ChecklistID")  %>'
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 0)
                {
                    int AuditID = Convert.ToInt32(commandArgs[0]);
                    int ATBDID = Convert.ToInt32(commandArgs[1]);
                    long ChecklistID = Convert.ToInt32(commandArgs[2]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDOCDialog(" + AuditID + "," + ATBDID + "," + ChecklistID + ");", true);
                }
            }
            else if (e.CommandName.Equals("Add_CheckList"))
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 0)
                {
                    //ViewState["AuditID"] = Convert.ToInt32(commandArgs[0]);
                    ViewState["ATBDID"] = Convert.ToInt32(commandArgs[1]);
                    tbxCheckListName.Text = string.Empty;
                    // ViewState["ChecklistID"] = Convert.ToInt32(commandArgs[2]);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAddCheckList();", true);
                }
            }
            else if (e.CommandName.Equals("Add_Remark_Document"))
            {
                Session["GridviewCommand"] = e;
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 0)
                {
                    int AuditID = Convert.ToInt32(commandArgs[0]);
                    int ATBDID = Convert.ToInt32(commandArgs[1]);
                    long ChecklistID = Convert.ToInt32(commandArgs[2]);
                    int ID = Convert.ToInt32(commandArgs[3]);
                    int ProcessId = Convert.ToInt32(commandArgs[4]);
                    int SubProcessId = Convert.ToInt32(commandArgs[5]);

                    Label DocumentName = (Label)grdProcessAudit.Rows[ID].Cells[2].FindControl("lblChecklistDocument");
                    string DocumentRequired = Convert.ToString(DocumentName.Text);
                    string Auditee = string.Empty;
                    string ReportingManager = string.Empty;
                    string ProcessName = string.Empty;
                    string SubProcessName = string.Empty;
                    string fileName = string.Empty;
                    string filePath = string.Empty;
                    string FileKey = string.Empty;
                    string Remark = string.Empty;
                    string PerformerID = string.Empty;
                    string ReviewerID = string.Empty;

                    DropDownList ddlPerformer = (DropDownList)grdProcessAudit.Rows[ID].FindControl("ddlPerformer");

                    if (!string.IsNullOrEmpty(ddlPerformer.SelectedValue))
                    {
                        if (ddlPerformer.SelectedValue != "-1")
                        {
                            Auditee = Convert.ToString(ddlPerformer.SelectedItem.Text);
                            PerformerID = Convert.ToString(ddlPerformer.SelectedValue);
                        }
                    }

                    DropDownList ddlReviewer = (DropDownList)grdProcessAudit.Rows[ID].FindControl("ddlReviewer");

                    if (!string.IsNullOrEmpty(ddlReviewer.SelectedValue))
                    {
                        if (ddlReviewer.SelectedValue != "-1")
                        {
                            ReportingManager = Convert.ToString(ddlReviewer.SelectedItem.Text);
                            ReviewerID = Convert.ToString(ddlReviewer.SelectedValue);
                        }
                    }

                    TextBox txtTimeLine = (TextBox)grdProcessAudit.Rows[ID].FindControl("txtStartDate");
                    string TimeLine = txtTimeLine.Text;

                    PrerequisiteTran_Upload pf = new PrerequisiteTran_Upload();
                    List<PrerequisiteTran_Upload> pfList = new List<PrerequisiteTran_Upload>();

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var ProcessList = (from row in entities.Mst_Process
                                           where row.Id == ProcessId
                                           && row.IsDeleted == false
                                           && row.CustomerID == customerID
                                           select row.Name).FirstOrDefault();

                        ProcessName = ProcessList;

                        var subProcessList = (from row in entities.mst_Subprocess
                                              where row.IsDeleted == false
                                              && row.ProcessId == ProcessId
                                              select row.Name).FirstOrDefault();

                        SubProcessName = subProcessList;


                        pf = (from row in entities.PrerequisiteTran_Upload
                              where row.CustomerId == customerID
                                     && row.AuditID == AuditID
                                     && row.checklistId == ChecklistID
                                     && row.ATBDId == ATBDID
                              select row).OrderByDescending(m => m.Id).FirstOrDefault();

                        pfList.Add(pf);
                    }

                    litProcess.Text = ProcessName;
                    litSubProcess.Text = SubProcessName;
                    litDocumentRequried.Text = DocumentRequired;
                    litAuditee.Text = Auditee;
                    litReportingManager.Text = ReportingManager;
                    litTimeLine.Text = TimeLine;
                    txtAuditId.Text = Convert.ToString(AuditID);
                    txtATBDID.Text = Convert.ToString(ATBDID);
                    txtChecklistID.Text = Convert.ToString(ChecklistID);
                    txtPerformerID.Text = Convert.ToString(PerformerID);
                    txtReviewerID.Text = Convert.ToString(ReviewerID);
                    if (pf != null)
                    {
                        if (pf.Remark != "" && pf.FileName != null)
                        {
                            if (pf.FileName != "NA")
                            {
                                rptComplianceDocumnets.DataSource = pfList;
                                rptComplianceDocumnets.DataBind();
                                fileUploadDocument.Enabled = false;
                                rptComplianceDocumnets.Visible = true;
                            }
                            else
                            {
                                fileUploadDocument.Enabled = true;
                                rptComplianceDocumnets.Visible = false;
                            }
                            if (string.IsNullOrEmpty(txtRemark.Text))
                            {
                                txtRemark.Text = pf.Remark;
                            }
                            else
                            {
                                txtRemark.Text = txtRemark.Text;
                            }
                            txtRemark.Enabled = true;
                            uploadDocument.Visible = true;
                            fileUploadDocument.Visible = true;
                        }
                        else if (pf.Remark == "" && pf.FileName != null)
                        {

                            if (pf.FileName != "NA")
                            {
                                rptComplianceDocumnets.DataSource = pfList;
                                rptComplianceDocumnets.DataBind();
                                fileUploadDocument.Enabled = false;
                                rptComplianceDocumnets.Visible = true;
                            }
                            else
                            {
                                fileUploadDocument.Enabled = true;
                                rptComplianceDocumnets.Visible = false;
                            }

                            txtRemark.Text = string.Empty;
                            txtRemark.Enabled = true;
                            uploadDocument.Visible = true;
                            fileUploadDocument.Visible = true;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(txtRemark.Text))
                            {
                                txtRemark.Text = pf.Remark;
                            }
                            else
                            {
                                txtRemark.Text = txtRemark.Text;
                            }
                            txtRemark.Enabled = true;
                            uploadDocument.Visible = true;
                            fileUploadDocument.Enabled = true;
                            fileUploadDocument.Visible = true;
                            rptComplianceDocumnets.Visible = false;
                        }
                    }
                    else
                    {
                        txtRemark.Text = Remark;
                        txtRemark.Enabled = true;
                        uploadDocument.Visible = true;
                        fileUploadDocument.Enabled = true;
                        fileUploadDocument.Visible = true;
                        rptComplianceDocumnets.Visible = false;
                    }

                    uploadedDocument.Visible = false;
                    btnSaveRemarkDoc.Visible = true;
                    btnSaveRemarkDoc.Enabled = true;

                    upPrerequisiteUpload.Update();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAddRemarkDoc();", true);

                }


            }
            else if (e.CommandName.Equals("Add_Remark_DocumentFirst"))
            {
                Session["GridviewCommand"] = e;
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 0)
                {
                    int AuditID = Convert.ToInt32(commandArgs[0]);
                    int ATBDID = Convert.ToInt32(commandArgs[1]);
                    long ChecklistID = Convert.ToInt32(commandArgs[2]);
                    int ID = Convert.ToInt32(commandArgs[3]);
                    int ProcessId = Convert.ToInt32(commandArgs[4]);
                    int SubProcessId = Convert.ToInt32(commandArgs[5]);

                    Label DocumentName = (Label)grdProcessAudit.Rows[ID].Cells[2].FindControl("lblChecklistDocument");
                    string DocumentRequired = Convert.ToString(DocumentName.Text);
                    string Auditee = string.Empty;
                    string ReportingManager = string.Empty;
                    string ProcessName = string.Empty;
                    string SubProcessName = string.Empty;
                    string fileName = string.Empty;
                    string filePath = string.Empty;
                    string FileKey = string.Empty;
                    string Remark = string.Empty;
                    string PerformerID = string.Empty;
                    string ReviewerID = string.Empty;

                    DropDownList ddlPerformer = (DropDownList)grdProcessAudit.Rows[ID].FindControl("ddlPerformer");

                    if (!string.IsNullOrEmpty(ddlPerformer.SelectedValue))
                    {
                        if (ddlPerformer.SelectedValue != "-1")
                        {
                            Auditee = Convert.ToString(ddlPerformer.SelectedItem.Text);
                            PerformerID = Convert.ToString(ddlPerformer.SelectedValue);
                        }
                    }

                    DropDownList ddlReviewer = (DropDownList)grdProcessAudit.Rows[ID].FindControl("ddlReviewer");

                    if (!string.IsNullOrEmpty(ddlReviewer.SelectedValue))
                    {
                        if (ddlReviewer.SelectedValue != "-1")
                        {
                            ReportingManager = Convert.ToString(ddlReviewer.SelectedItem.Text);
                            ReviewerID = Convert.ToString(ddlReviewer.SelectedValue);
                        }
                    }

                    TextBox txtTimeLine = (TextBox)grdProcessAudit.Rows[ID].FindControl("txtStartDate");
                    string TimeLine = txtTimeLine.Text;

                    PrerequisiteTran_Upload pf = new PrerequisiteTran_Upload();
                    List<PrerequisiteTran_Upload> pfList = new List<PrerequisiteTran_Upload>();

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var ProcessList = (from row in entities.Mst_Process
                                           where row.Id == ProcessId
                                           && row.IsDeleted == false
                                           && row.CustomerID == customerID
                                           select row.Name).FirstOrDefault();

                        ProcessName = ProcessList;

                        var subProcessList = (from row in entities.mst_Subprocess
                                              where row.IsDeleted == false
                                              && row.ProcessId == ProcessId
                                              select row.Name).FirstOrDefault();

                        SubProcessName = subProcessList;


                        pf = (from row in entities.PrerequisiteTran_Upload
                              where row.CustomerId == customerID
                                     && row.AuditID == AuditID
                                     && row.checklistId == ChecklistID
                                     && row.ATBDId == ATBDID
                              select row).OrderByDescending(m => m.Id).FirstOrDefault();

                        pfList.Add(pf);

                    }

                    litProcess.Text = ProcessName;
                    litSubProcess.Text = SubProcessName;
                    litDocumentRequried.Text = DocumentRequired;
                    litAuditee.Text = Auditee;
                    litReportingManager.Text = ReportingManager;
                    litTimeLine.Text = TimeLine;
                    txtAuditId.Text = Convert.ToString(AuditID);
                    txtATBDID.Text = Convert.ToString(ATBDID);
                    txtChecklistID.Text = Convert.ToString(ChecklistID);
                    txtPerformerID.Text = Convert.ToString(PerformerID);
                    txtReviewerID.Text = Convert.ToString(ReviewerID);
                    if (pf != null)
                    {
                        if (pf.Remark != "" && pf.FileName != null)
                        {
                            if (pf.FileName != "NA")
                            {
                                rptComplianceDocumnets.DataSource = pfList;
                                rptComplianceDocumnets.DataBind();
                                fileUploadDocument.Enabled = false;
                                rptComplianceDocumnets.Visible = true;
                            }
                            else
                            {
                                rptComplianceDocumnets.DataSource = null;
                                rptComplianceDocumnets.DataBind();
                                fileUploadDocument.Enabled = true;
                                rptComplianceDocumnets.Visible = false;
                            }
                            if (string.IsNullOrEmpty(txtRemark.Text))
                            {
                                txtRemark.Text = pf.Remark;
                            }
                            else
                            {
                                txtRemark.Text = txtRemark.Text;
                            }
                            txtRemark.Enabled = true;
                            uploadDocument.Visible = true;
                            fileUploadDocument.Visible = true;
                        }
                        else if (pf.Remark == "" && pf.FileName != null)
                        {
                            if (pf.FileName != "NA")
                            {
                                rptComplianceDocumnets.DataSource = pfList;
                                rptComplianceDocumnets.DataBind();
                                rptComplianceDocumnets.Visible = true;
                                fileUploadDocument.Enabled = false;
                            }
                            else
                            {
                                rptComplianceDocumnets.DataSource = null;
                                rptComplianceDocumnets.DataBind();
                                rptComplianceDocumnets.Visible = false;
                                fileUploadDocument.Enabled = true;
                            }
                            txtRemark.Text = string.Empty;
                            txtRemark.Enabled = true;
                            uploadDocument.Visible = true;

                            fileUploadDocument.Visible = true;

                        }
                        else
                        {
                            if (string.IsNullOrEmpty(txtRemark.Text))
                            {
                                txtRemark.Text = pf.Remark;
                            }
                            else
                            {
                                txtRemark.Text = txtRemark.Text;
                            }
                            txtRemark.Enabled = true;
                            uploadDocument.Visible = true;
                            fileUploadDocument.Enabled = true;
                            fileUploadDocument.Visible = true;
                            rptComplianceDocumnets.Visible = false;
                        }
                    }
                    else
                    {
                        txtRemark.Text = Remark;
                        txtRemark.Enabled = true;
                        uploadDocument.Visible = true;
                        fileUploadDocument.Enabled = true;
                        fileUploadDocument.Visible = true;
                        rptComplianceDocumnets.Visible = false;
                    }

                    uploadedDocument.Visible = false;
                    btnSaveRemarkDoc.Visible = true;
                    btnSaveRemarkDoc.Enabled = true;

                    upPrerequisiteUpload.Update();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAddRemarkDoc();", true);
                }
            }
            else if (e.CommandName.Equals("Add_Remark_DocumentFileUpload"))
            {
                int customerIDFU = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                string[] commandArgsFU = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgsFU.Length > 0)
                {
                    int AuditID = Convert.ToInt32(commandArgsFU[0]);
                    int ATBDID = Convert.ToInt32(commandArgsFU[1]);
                    int ChecklistID = Convert.ToInt32(commandArgsFU[2]);
                    int ID = Convert.ToInt32(commandArgsFU[3]);
                    int ProcessId = Convert.ToInt32(commandArgsFU[4]);
                    int SubProcessId = Convert.ToInt32(commandArgsFU[5]);
                    Label DocumentName = (Label)grdProcessAudit.Rows[ID].Cells[2].FindControl("lblChecklistDocument");
                    string DocumentRequired = Convert.ToString(DocumentName.Text);
                    string Auditee = string.Empty;
                    string ReportingManager = string.Empty;
                    string ProcessName = string.Empty;
                    string SubProcessName = string.Empty;
                    List<usp_Pre_requisite_details_Result> list = new List<usp_Pre_requisite_details_Result>();
                    usp_Pre_requisite_details_Result listresult = new usp_Pre_requisite_details_Result();

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        listresult = (from row in entities.usp_Pre_requisite_details(customerIDFU, AuditID, ATBDID, ChecklistID)
                                      select row).FirstOrDefault();

                        list.Add(listresult);
                    }

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var ProcessList = (from row in entities.Mst_Process
                                           where row.Id == ProcessId
                                           && row.IsDeleted == false
                                           && row.CustomerID == customerIDFU
                                           select row.Name).FirstOrDefault();

                        ProcessName = ProcessList;

                        var subProcessList = (from row in entities.mst_Subprocess
                                              where row.IsDeleted == false
                                              && row.ProcessId == ProcessId
                                              select row.Name).FirstOrDefault();

                        SubProcessName = subProcessList;
                    }


                    litProcess.Text = ProcessName;
                    litSubProcess.Text = SubProcessName;
                    litDocumentRequried.Text = list[0].checklistName;
                    litAuditee.Text = list[0].AuditeeName;
                    litReportingManager.Text = list[0].BossName;
                    litTimeLine.Text = Convert.ToDateTime(list[0].Timeline).ToString("dd-MMM-yyyy");
                    txtAuditId.Text = Convert.ToString(AuditID);
                    txtATBDID.Text = Convert.ToString(ATBDID);
                    txtChecklistID.Text = Convert.ToString(ChecklistID);
                    txtRemark.Text = list[0].Remark;
                    txtRemark.Enabled = false;
                    txtPerformerID.Text = Convert.ToString(list[0].AuditeeId);
                    txtReviewerID.Text = Convert.ToString(list[0].BossId);
                    btnSaveRemarkDoc.Visible = false;
                    fileUploadDocument.Visible = false;
                    uploadDocument.Visible = false;
                    rptComplianceDocumnets.Visible = false;
                    uploadedDocument.Visible = true;
                    if (list[0].FileName != "NA")
                    {
                        lnkUploadedDocument.Visible = true;
                        lnkUploadedDocument.Text = list[0].FileName;
                        lnkUploadedDocument.CommandArgument = list[0].FileName + "`" + Convert.ToString(ChecklistID);
                        lblUploadedFileName.Visible = false;
                    }
                    else
                    {
                        lblUploadedFileName.Text = list[0].FileName;
                        lnkUploadedDocument.Visible = false;
                        lblUploadedFileName.Visible = true;
                    }
                    upPrerequisiteUpload.Update();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAddRemarkDoc();", true);
                }
            }
        }
        public static long AddDetailsMasterAuditPrerequisites(Master_AuditPrerequisite MAP)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Master_AuditPrerequisite.Add(MAP);
                entities.SaveChanges();
                return MAP.Id;
            }

        }
        public static List<SP_PreRequsiteManualReminder_Result> GetPrerequsiteReminderEscalation(string flag, long processid, long subprocessid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var lstprerequsiteAssignedCurrentDate = (from row in entities.SP_PreRequsiteManualReminder(flag, processid, subprocessid, AuditID)
                                                         select row).ToList();

                return lstprerequsiteAssignedCurrentDate;
            }
        }
        protected void btnReminderEscalation_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //int chkValidationflag = 0;
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    foreach (GridViewRow rw in grdRiskActivityMatrix.Rows)
                    {
                        CheckBox chkBxrechild = (CheckBox)rw.FindControl("rechild");
                        CheckBox checkAll = (CheckBox)grdRiskActivityMatrix.HeaderRow.FindControl("checkAll");

                        CheckBox chkBxeschild = (CheckBox)rw.FindControl("eschild");
                        CheckBox escalationcheckAll = (CheckBox)grdRiskActivityMatrix.HeaderRow.FindControl("escalationcheckAll");

                        string lblAuditID = (rw.FindControl("lblAuditID") as Label).Text;
                        string lblProcessID = (rw.FindControl("lblProcessID") as Label).Text;
                        string lblsubprocessID = (rw.FindControl("lblsubprocessID") as Label).Text;
                        string lblPending = (rw.FindControl("lblPending") as Label).Text;

                        if (lblAuditID != null && lblProcessID != null && lblsubprocessID != null && lblPending != null)
                        {
                            #region reminder
                            if (chkBxrechild != null && chkBxrechild.Checked)
                            {
                                if (lblPending != "0")
                                {
                                    var lstprerequsite = GetPrerequsiteReminderEscalation("REM", Convert.ToInt32(lblProcessID), Convert.ToInt32(lblsubprocessID), Convert.ToInt32(lblAuditID));
                                    if (lstprerequsite.Count > 0)
                                    {
                                        lstprerequsite.ForEach(entry =>
                                        {
                                            if (!string.IsNullOrEmpty(entry.Email))
                                            {
                                                var message = Settings.Default.EmailTemplate_PreRequsiteBefore2Days
                                                                    .Replace("@User", entry.Username)
                                                                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                                    .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                                string Subject = "Audit Pre-Requisite Reminder: Cheklist Timeline";
                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { entry.Email }).ToList(), null, null, Subject, message);
                                            }
                                        });
                                    }
                                }
                            }
                            #endregion

                            #region Escalation 
                            if (chkBxeschild != null && chkBxeschild.Checked)
                            {
                                if (lblPending != "0")
                                {
                                    var lstprerequsite = GetPrerequsiteReminderEscalation("ESC", Convert.ToInt32(lblProcessID), Convert.ToInt32(lblsubprocessID), Convert.ToInt32(lblAuditID));
                                    if (lstprerequsite.Count > 0)
                                    {
                                        lstprerequsite.ForEach(entry =>
                                        {
                                            if (!string.IsNullOrEmpty(entry.Email))
                                            {
                                                var message = Settings.Default.EmailTemplate_PreRequsiteEscalation
                                                                   .Replace("@User", entry.Username)
                                                                   .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                                   .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                                string Subject = "Audit Pre-Requisite Reminder:Cheklist Escalation";
                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { entry.Email }).ToList(), null, null, Subject, message);
                                            }
                                        });
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool CreatePrerequisiteTran_UploadResult(PrerequisiteTran_Upload DraftClosureResult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.PrerequisiteTran_Upload.Add(DraftClosureResult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool Tran_PrerequisiteTran_UploadExists(PrerequisiteTran_Upload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == ICR.AuditID
                             && row.ATBDId == ICR.ATBDId
                             && row.checklistId == ICR.checklistId
                             && row.FileName == ICR.FileName
                             && row.IsDeleted == false
                             && row.Status != 1
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool masterauditprerequisiteexists(Master_AuditPrerequisite objinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (objinstance != null)
                {
                    var query = (from row in entities.Master_AuditPrerequisite
                                 where row.AuditId == objinstance.AuditId
                                 && row.Atbdid == objinstance.Atbdid
                                 && row.ChecklistId == objinstance.ChecklistId
                                 && row.Timeline == objinstance.Timeline
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            }
        }

        public static void Updatemasterauditprerequisite(Master_AuditPrerequisite objinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var RiskToUpdate = (from row in entities.Master_AuditPrerequisite
                                    where row.AuditId == objinstance.AuditId
                                         && row.Atbdid == objinstance.Atbdid
                                         && row.ChecklistId == objinstance.ChecklistId
                                    select row).FirstOrDefault();
                RiskToUpdate.IsActive = false;
                RiskToUpdate.AuditeeId = objinstance.AuditeeId;
                RiskToUpdate.BossId = objinstance.BossId;
                RiskToUpdate.Timeline = objinstance.Timeline;
                entities.SaveChanges();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool SaveFlag = false;
                #region 
                List<Master_AuditPrerequisite> tempauditpreList = new List<Master_AuditPrerequisite>();
                tempauditpreList.Clear();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    foreach (GridViewRow g1 in grdProcessAudit.Rows)
                    {
                        #region
                        DropDownList ddlper = (g1.FindControl("ddlPerformer") as DropDownList);
                        DropDownList ddlrev = (g1.FindControl("ddlReviewer") as DropDownList);
                        List<int> Auditstepdetails = new List<int>();
                        long MAPId = 0;
                        if (ddlper != null && ddlper.Enabled && ddlrev != null && ddlrev.Enabled)
                        {
                            string lblAuditID = (g1.FindControl("lblAuditID") as Label).Text;
                            string lblatbdID = (g1.FindControl("lblatbdID") as Label).Text;
                            string lblChecklistid = (g1.FindControl("lblChecklistid") as Label).Text;
                            string lblPerformerUserID = (g1.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                            string lblReviewerUserID = (g1.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                            string txtStartDate = (g1.FindControl("txtStartDate") as TextBox).Text;
                            string lblChecklistDocument = (g1.FindControl("lblChecklistDocument") as Label).Text;

                            if (lblAuditID != "" && lblatbdID != "" && lblChecklistid != "")
                            {
                                if (lblPerformerUserID != "-1" && lblReviewerUserID != "-1")
                                {
                                    DateTime stdate = DateTime.ParseExact(txtStartDate, "dd-MM-yyyy", null);

                                    #region Audit save
                                    Master_AuditPrerequisite instance = new Master_AuditPrerequisite();
                                    instance.AuditId = Convert.ToInt32(lblAuditID);
                                    instance.Atbdid = Convert.ToInt32(lblatbdID);
                                    instance.ChecklistId = Convert.ToInt32(lblChecklistid);
                                    instance.AuditeeId = Convert.ToInt32(lblPerformerUserID);
                                    instance.BossId = Convert.ToInt32(lblReviewerUserID);
                                    instance.CreatedOn = DateTime.Now;
                                    instance.Createdby = AuthenticationHelper.UserID;
                                    instance.Timeline = stdate;

                                    if (!masterauditprerequisiteexists(instance))
                                    {
                                        if (instance != null)
                                        {
                                            MAPId = AddDetailsMasterAuditPrerequisites(instance);
                                        }
                                        SaveFlag = true;

                                    }
                                    else
                                    {

                                        Updatemasterauditprerequisite(instance);
                                        MAPId = (from row in entities.Master_AuditPrerequisite
                                                 where row.AuditId == instance.AuditId
                                                      && row.Atbdid == instance.Atbdid
                                                      && row.ChecklistId == instance.ChecklistId
                                                 select row.Id).FirstOrDefault();
                                        SaveFlag = true;

                                    }

                                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    int auditId = Convert.ToInt32(lblAuditID);
                                    int atbdid = Convert.ToInt32(lblatbdID);
                                    int checklistId = Convert.ToInt32(lblChecklistid);
                                    PrerequisiteTran_Upload prerequisite = new PrerequisiteTran_Upload();
                                    prerequisite = (from row in entities.PrerequisiteTran_Upload
                                                    where row.CustomerId == customerid
                                                    && row.ATBDId == atbdid
                                                    && row.AuditID == auditId
                                                    && row.checklistId == checklistId
                                                    select row).OrderByDescending(m => m.MAPId).FirstOrDefault();

                                    if (prerequisite != null)
                                    {
                                        prerequisite.Flag = "AR";
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        PrerequisiteTran_Upload savePre = new PrerequisiteTran_Upload()
                                        {
                                            CustomerId = customerid,
                                            ATBDId = atbdid,
                                            FileName = "NA",
                                            AuditID = auditId,
                                            checklistId = checklistId,
                                            checklistName = lblChecklistDocument,
                                            IsDeleted = false,
                                            FilePath = null,
                                            FileKey = null,
                                            Version = null,
                                            VersionDate = DateTime.Now,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedDate = DateTime.Now,
                                            Status = 1,
                                            MAPId = MAPId,
                                            Remark = "",
                                            Flag = "AR",
                                        };

                                        entities.PrerequisiteTran_Upload.Add(savePre);
                                        entities.SaveChanges();
                                    }
                                    #endregion
                                }
                                if (SaveFlag)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Save Sucessfully.";
                                }
                            }
                        }
                        #endregion
                    }
                    BindProcessAudits();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdProcessAudit.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        //DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        DropDownListPageNo.SelectedValue = Convert.ToString(chkcindition);
                    }
                }
                Session["PerformerFile"] = null;
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveRemarkDoc_Click(object sender, EventArgs e)
        {
            try
            {
                string Remark = string.Empty;
                if (!string.IsNullOrEmpty(txtRemark.Text))
                {
                    Remark = txtRemark.Text.Trim();
                }
                else
                {
                    Remark = "NA";
                }    
                bool hasfile = fileUploadDocument.HasFile;

                string AuditeeName = litAuditee.Text;
                string BossName = litReportingManager.Text;
                string timeline = litTimeLine.Text;
                
                if (string.IsNullOrEmpty(AuditeeName) && !string.IsNullOrEmpty(BossName) && !string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Auditee.');</script>");
                    return;
                }
                else if (!string.IsNullOrEmpty(AuditeeName) && !string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Timeline.');</script>");
                    return;
                }
                else if (!string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && !string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Reporting Manager.');</script>");
                    return;
                }
                else if (!string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Reporting Manager and Timeline.');</script>");
                    return;
                }
                else if (string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && !string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Auditee and Reporting Manager.');</script>");
                    return;
                }
                else if (string.IsNullOrEmpty(AuditeeName) && !string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Auditee and Timeline.');</script>");
                    return;
                }
                else if (string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                    Response.Write("<script>alert('Please select Auditee, Reporting Manager and Timeline.');</script>");
                    return;
                }


                DateTime stdate = DateTime.ParseExact(litTimeLine.Text, "dd-MM-yyyy", null);
                string[] AuditeeFullName = AuditeeName.Split(' ');
                string AuditeeFName = AuditeeFullName[0];
                string AuditeeLName = string.Empty;
                if (string.IsNullOrEmpty(AuditeeFullName[1]))
                {
                    if (!string.IsNullOrEmpty(AuditeeFullName[2]))
                    {
                        AuditeeLName = AuditeeFullName[2];
                    }
                }
                else
                {
                    AuditeeLName = AuditeeFullName[1];
                }
                
                string[] BossFullName = BossName.Split(' ');
                string BossFName = BossFullName[0];
                string BossLName = string.Empty;
                if (string.IsNullOrEmpty(BossFullName[1]))
                {
                    if (!string.IsNullOrEmpty(BossFullName[2]))
                    {
                        BossLName = BossFullName[2];
                    }
                }
                else
                {
                    BossLName = BossFullName[1];
                }
                long AuditeeId = 0;
                long BossId = 0;


                int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int ChecklistID = Convert.ToInt32(txtChecklistID.Text);
                int ATBDID = Convert.ToInt32(txtATBDID.Text);
                int AuditID = Convert.ToInt32(txtAuditId.Text);
                string ChecklistDocument = Convert.ToString(litDocumentRequried.Text);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (string.IsNullOrEmpty(AuditeeName) && !string.IsNullOrEmpty(BossName) && !string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Auditee.');</script>");
                        return;
                    }
                    else if (!string.IsNullOrEmpty(AuditeeName) && !string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Timeline.');</script>");
                        return;
                    }
                    else if (!string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && !string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Reporting Manager.');</script>");
                        return;
                    }
                    else if (!string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Reporting Manager and Timeline.');</script>");
                        return;
                    }
                    else if (string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && !string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Auditee and Reporting Manager.');</script>");
                        return;
                    }
                    else if (string.IsNullOrEmpty(AuditeeName) && !string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Auditee and Timeline.');</script>");
                        return;
                    }
                    else if (string.IsNullOrEmpty(AuditeeName) && string.IsNullOrEmpty(BossName) && string.IsNullOrEmpty(litTimeLine.Text))
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                        Response.Write("<script>alert('Please select Auditee, Reporting Manager and Timeline.');</script>");
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRemark.Text) && hasfile == false)
                    {
                        string filename = string.Empty;
                        if (rptComplianceDocumnets.Visible == true)
                        {
                            LinkButton btn = (LinkButton)rptComplianceDocumnets.Rows[0].FindControl("btnComplianceDocumnets");
                            filename = btn.Text;
                        }

                        if (string.IsNullOrEmpty(filename))
                        {
                            cvDuplicateEntry1.IsValid = false;
                            cvDuplicateEntry1.ErrorMessage = "Please add Remarks or Upload file.";
                            Response.Write("<script>alert('Please add Remarks or Upload file.');</script>");
                            return;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                PrerequisiteTran_Upload preUpdate = new PrerequisiteTran_Upload();
                                preUpdate = (from row in entities.PrerequisiteTran_Upload
                                             where row.CustomerId == CustomerId
                                                    && row.AuditID == AuditID
                                                    && row.checklistId == ChecklistID
                                                    && row.ATBDId == ATBDID
                                             select row).OrderByDescending(m => m.Id).FirstOrDefault();

                                preUpdate.Remark = txtRemark.Text.Trim();
                                entities.SaveChanges();
                            }
                            else
                            {
                                PrerequisiteTran_Upload preUpdate = new PrerequisiteTran_Upload();
                                preUpdate = (from row in entities.PrerequisiteTran_Upload
                                             where row.CustomerId == CustomerId
                                                    && row.AuditID == AuditID
                                                    && row.checklistId == ChecklistID
                                                    && row.ATBDId == ATBDID
                                             select row).OrderByDescending(m => m.Id).FirstOrDefault();

                                preUpdate.Remark = txtRemark.Text.Trim();
                                entities.SaveChanges();

                                cvDuplicateEntry1.IsValid = false;
                                cvDuplicateEntry1.ErrorMessage = "Your Remarks have been deleted Successfully.";
                            }
                        }
                    }
                    else
                    {
                        //AuditeeId = (from row in entities.mst_User
                        //             where row.CustomerID == CustomerId
                        //             && row.FirstName == AuditeeFName
                        //             && row.LastName == AuditeeLName
                        //             select row.ID).FirstOrDefault();

                        AuditeeId = Convert.ToInt64(txtPerformerID.Text);

                        //BossId = (from row in entities.mst_User
                        //          where row.CustomerID == CustomerId
                        //          && row.FirstName == BossFName
                        //          && row.LastName == BossLName
                        //          select row.ID).FirstOrDefault();

                        BossId = Convert.ToInt64(txtReviewerID.Text);

                        var queryExists = (from row in entities.PrerequisiteTran_Upload
                                           where row.CustomerId == CustomerId
                                                  && row.AuditID == AuditID
                                                  && row.checklistId == ChecklistID
                                                  && row.ATBDId == ATBDID
                                           select row).OrderByDescending(m => m.Id).FirstOrDefault();

                        if (queryExists != null)
                        {
                            if (Remark != "" || hasfile)
                            {
                                if (Remark != "")
                                {
                                    queryExists.Remark = Remark;
                                    entities.SaveChanges();

                                    cvDuplicateEntry1.IsValid = false;
                                    cvDuplicateEntry1.ErrorMessage = "Your Remarks have been Updated Successfully.";

                                }
                                if (hasfile)
                                {
                                    List<InternalFileData_Risk> Workingfiles = new List<InternalFileData_Risk>();
                                    List<KeyValuePair<string, int>> Workinglist = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, Byte[]>> WorkingFilelist = new List<KeyValuePair<string, Byte[]>>();
                                    var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(ATBDID), Convert.ToInt32(AuditID));

                                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                    {
                                        AuditScheduleOnID = InstanceData.ScheduleonID,
                                        ProcessId = InstanceData.ProcessId,
                                        FinancialYear = InstanceData.FinancialYear,
                                        ForPerid = InstanceData.ForMonth,
                                        CustomerBranchId = InstanceData.CustomerBranchID,
                                        IsDeleted = false,
                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        ATBDId = ATBDID,
                                        RoleID = 3,
                                        InternalAuditInstance = InstanceData.InternalAuditInstance,
                                        VerticalID = InstanceData.VerticalId,
                                        AuditID = AuditID,
                                    };
                                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                                    {
                                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                        AuditScheduleOnID = InstanceData.ScheduleonID,
                                        FinancialYear = InstanceData.FinancialYear,
                                        CustomerBranchId = InstanceData.CustomerBranchID,
                                        InternalAuditInstance = InstanceData.InternalAuditInstance,
                                        ProcessId = InstanceData.ProcessId,
                                        ForPeriod = InstanceData.ForMonth,
                                        ATBDId = ATBDID,
                                        RoleID = 3,
                                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        VerticalID = InstanceData.VerticalId,
                                        AuditID = AuditID
                                    };

                                    MstRiskResult.AuditObjective = null;
                                    MstRiskResult.AuditSteps = "";
                                    MstRiskResult.AnalysisToBePerofrmed = "";
                                    MstRiskResult.ProcessWalkthrough = "";
                                    MstRiskResult.ActivityToBeDone = "";
                                    MstRiskResult.Population = "";
                                    MstRiskResult.Sample = "";
                                    MstRiskResult.ObservationNumber = "";
                                    MstRiskResult.ObservationTitle = "";
                                    MstRiskResult.Observation = "";
                                    MstRiskResult.Risk = "";
                                    MstRiskResult.RootCost = null;
                                    MstRiskResult.AuditScores = null;
                                    MstRiskResult.FinancialImpact = null;
                                    MstRiskResult.Recomendation = "";
                                    MstRiskResult.ManagementResponse = "";
                                    MstRiskResult.FixRemark = "";
                                    MstRiskResult.TimeLine = null;
                                    MstRiskResult.PersonResponsible = null;
                                    MstRiskResult.Owner = null;
                                    MstRiskResult.ObservationRating = null;
                                    MstRiskResult.ObservationCategory = null;
                                    MstRiskResult.ObservationSubCategory = null;
                                    MstRiskResult.AStatusId = 2;

                                    transaction.PersonResponsible = null;
                                    transaction.Owner = null;
                                    transaction.ObservatioRating = null;
                                    transaction.ObservationCategory = null;
                                    transaction.ObservationSubCategory = null;

                                    List<PrerequisiteTran_Upload> AnnxetureList = new List<PrerequisiteTran_Upload>();
                                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();

                                    if (fileUploadDocument.HasFile)
                                    {
                                        HttpFileCollection fileCollection = Request.Files;
                                        if (fileCollection.Count > 0)
                                        {
                                            string directoryPathIFD = "";
                                            directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalId.ToString() + "/" + InstanceData.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ScheduleonID + "/0.0");
                                            DocumentManagement.CreateDirectory(directoryPathIFD);

                                            string directoryPath = "";

                                            if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                                            {
                                                directoryPath = Server.MapPath("~/AuditPreRequisiteDocument/" + CustomerId + "/"
                                                  + Convert.ToInt32(AuditID) + "/" + Convert.ToInt32(ATBDID) + "/"
                                                  + Convert.ToInt32(ChecklistID) + "/0.0");
                                            }
                                            if (!Directory.Exists(directoryPath))
                                            {
                                                DocumentManagement.CreateDirectory(directoryPath);
                                            }

                                            for (int i = 0; i < fileCollection.Count; i++)
                                            {
                                                #region
                                                HttpPostedFile uploadfile = fileCollection[i];
                                                string[] keys = fileCollection.Keys[i].Split('$');
                                                String fileName = "";
                                                if (keys[keys.Count() - 1].Equals("fileUploadDocument"))
                                                {
                                                    fileName = uploadfile.FileName;
                                                    Guid fileKey = Guid.NewGuid();
                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                                    string finalPathPR = Path.Combine(directoryPathIFD, fileKey + Path.GetExtension(uploadfile.FileName));
                                                    Stream fs = uploadfile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                    if (uploadfile.ContentLength > 0)
                                                    {
                                                        Workinglist.Add(new KeyValuePair<string, int>(fileName, 1));
                                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                                        WorkingFilelist.Add(new KeyValuePair<string, Byte[]>(finalPathPR, bytes));

                                                        int sid = 1;
                                                        var query = (from row in entities.PrerequisiteTran_Upload
                                                                     where row.Status == 4 && row.checklistId == ChecklistID
                                                                     && row.AuditID == AuditID
                                                                     && row.ATBDId == ATBDID
                                                                     select row).ToList();
                                                        if (query.Count > 0)
                                                        {
                                                            sid = 5;
                                                        }

                                                        long MAPID = (from row in entities.PrerequisiteTran_Upload
                                                                      where row.checklistId == ChecklistID
                                                                      && row.AuditID == AuditID
                                                                      && row.ATBDId == ATBDID
                                                                      && row.MAPId != null
                                                                      select (long)row.MAPId).FirstOrDefault();

                                                        string filename1 = string.Empty;
                                                        if (string.IsNullOrEmpty(txtRemark.Text))
                                                        {
                                                            txtRemark.Text = "NA";
                                                        }
                                                        PrerequisiteTran_Upload DraftClosureUpload = new PrerequisiteTran_Upload()
                                                        {
                                                            CustomerId = CustomerId,
                                                            ATBDId = ATBDID,
                                                            FileName = fileName,
                                                            AuditID = AuditID,
                                                            checklistId = ChecklistID,
                                                            checklistName = ChecklistDocument,
                                                            IsDeleted = false,
                                                            FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                            FileKey = fileKey.ToString(),
                                                            Version = "0.0",
                                                            VersionDate = DateTime.Now,
                                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                                            CreatedDate = DateTime.Now,
                                                            Status = sid,
                                                            StatusRemak = "New Audit Checklist Assigned.",
                                                            Remark = txtRemark.Text,
                                                            MAPId = MAPID,
                                                            Flag = "PR",
                                                        };
                                                        #region 
                                                        var preRequisiteTran_Upload = GetDetailsFromPrerequisiteTran_Upload(DraftClosureUpload);
                                                        if (preRequisiteTran_Upload.Count > 0)
                                                        {
                                                            int fileVersionCount = preRequisiteTran_Upload.Count();
                                                            string[] fileNameArray = DraftClosureUpload.FileName.Split('.');
                                                            string newFileNameVersion = fileNameArray[0].ToString() + "_" + Convert.ToString(fileVersionCount);
                                                            DraftClosureUpload.FileName = newFileNameVersion + "." + fileNameArray[1].ToString();
                                                            filename1 = DraftClosureUpload.FileName;
                                                        }
                                                        else if (preRequisiteTran_Upload.Count == 0)
                                                        {
                                                            int fileVersionCount = preRequisiteTran_Upload.Count();
                                                            string[] fileNameArray = DraftClosureUpload.FileName.Split('.');
                                                            string newFileNameVersion = fileNameArray[0].ToString() + "_" + Convert.ToString(fileVersionCount);
                                                            DraftClosureUpload.FileName = newFileNameVersion + "." + fileNameArray[1].ToString();
                                                            filename1 = DraftClosureUpload.FileName;
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry1.IsValid = false;
                                                            cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                                                        }
                                                        #endregion
                                                        AnnxetureList.Add(DraftClosureUpload);

                                                        InternalFileData_Risk file = new InternalFileData_Risk()
                                                        {
                                                            Name = filename1,
                                                            FilePath = directoryPathIFD.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                            FileKey = fileKey.ToString(),
                                                            Version = "0.0",
                                                            VersionDate = DateTime.UtcNow,
                                                            ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                                                            CustomerBranchId = Convert.ToInt32(InstanceData.CustomerBranchID),
                                                            FinancialYear = InstanceData.FinancialYear,
                                                            IsDeleted = false,
                                                            ATBDId = ATBDID,
                                                            VerticalID = InstanceData.VerticalId,
                                                            AuditID = AuditID,
                                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                                            CreatedOn = DateTime.Now,
                                                        };

                                                        #region 
                                                        if (!IsRecordExists(file.ProcessID, file.FinancialYear, file.ATBDId, Convert.ToInt32(file.AuditID), Convert.ToInt32(file.VerticalID)))
                                                        {
                                                            file.UploadedDocumentFlag = "Open";
                                                        }
                                                        #endregion
                                                        Workingfiles.Add(file);
                                                    }
                                                }
                                                #endregion
                                            }//For Loop End  
                                        }
                                    }

                                    var RCT = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(InstanceData.ScheduleonID), ATBDID);

                                    if (RCT == null)
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        if (WorkingFilelist != null && Workinglist != null)
                                        {
                                            if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                            {
                                                transaction.UpdatedBy = AuthenticationHelper.UserID;
                                                transaction.UpdatedOn = DateTime.Now;
                                                UpdateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                                            }
                                            else
                                            {
                                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.CreatedOn = DateTime.Now;
                                                CreateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        UpdateTransaction(RCT.AuditTransactionID, (long)RCT.AuditScheduleOnID, Workingfiles, Workinglist, WorkingFilelist);
                                    }

                                    if (Filelist.Count > 0)
                                    {
                                        DocumentManagement.Audit_SaveDocFiles(Filelist);
                                    }
                                    bool Success1 = false;

                                    foreach (var item in AnnxetureList)
                                    {
                                        if (!Tran_PrerequisiteTran_UploadExists(item))
                                        {
                                            Success1 = CreatePrerequisiteTran_UploadResult(item);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "File Uploaded Successfully";
                                            }
                                            else
                                            {
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry1.IsValid = false;
                                            cvDuplicateEntry1.ErrorMessage = "Same Name Already Exists..";
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            Master_AuditPrerequisite instance = new Master_AuditPrerequisite();
                            instance.AuditId = Convert.ToInt32(AuditID);
                            instance.Atbdid = Convert.ToInt32(ATBDID);
                            instance.ChecklistId = Convert.ToInt32(ChecklistID);
                            instance.AuditeeId = Convert.ToInt32(AuditeeId);
                            instance.BossId = Convert.ToInt32(BossId);
                            instance.CreatedOn = DateTime.Now;
                            instance.Createdby = AuthenticationHelper.UserID;
                            instance.Timeline = stdate;
                            long MAPID = 0;
                            if (!masterauditprerequisiteexists(instance))
                            {
                                if (instance != null)
                                {
                                    MAPID = AddDetailsMasterAuditPrerequisites(instance);
                                }
                            }
                            else
                            {
                                MAPID = (from row in entities.Master_AuditPrerequisite
                                         where row.AuditId == instance.AuditId
                                              && row.Atbdid == instance.Atbdid
                                              && row.ChecklistId == instance.ChecklistId
                                         select row.Id).FirstOrDefault();
                            }

                            List<InternalFileData_Risk> Workingfiles = new List<InternalFileData_Risk>();
                            List<KeyValuePair<string, int>> Workinglist = new List<KeyValuePair<string, int>>();
                            List<KeyValuePair<string, Byte[]>> WorkingFilelist = new List<KeyValuePair<string, Byte[]>>();
                            var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(ATBDID), Convert.ToInt32(AuditID));

                            InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                            {
                                AuditScheduleOnID = InstanceData.ScheduleonID,
                                ProcessId = InstanceData.ProcessId,
                                FinancialYear = InstanceData.FinancialYear,
                                ForPerid = InstanceData.ForMonth,
                                CustomerBranchId = InstanceData.CustomerBranchID,
                                IsDeleted = false,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                ATBDId = ATBDID,
                                RoleID = 3,
                                InternalAuditInstance = InstanceData.InternalAuditInstance,
                                VerticalID = InstanceData.VerticalId,
                                AuditID = AuditID,
                            };
                            InternalAuditTransaction transaction = new InternalAuditTransaction()
                            {
                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                AuditScheduleOnID = InstanceData.ScheduleonID,
                                FinancialYear = InstanceData.FinancialYear,
                                CustomerBranchId = InstanceData.CustomerBranchID,
                                InternalAuditInstance = InstanceData.InternalAuditInstance,
                                ProcessId = InstanceData.ProcessId,
                                ForPeriod = InstanceData.ForMonth,
                                ATBDId = ATBDID,
                                RoleID = 3,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                VerticalID = InstanceData.VerticalId,
                                AuditID = AuditID
                            };

                            MstRiskResult.AuditObjective = null;
                            MstRiskResult.AuditSteps = "";
                            MstRiskResult.AnalysisToBePerofrmed = "";
                            MstRiskResult.ProcessWalkthrough = "";
                            MstRiskResult.ActivityToBeDone = "";
                            MstRiskResult.Population = "";
                            MstRiskResult.Sample = "";
                            MstRiskResult.ObservationNumber = "";
                            MstRiskResult.ObservationTitle = "";
                            MstRiskResult.Observation = "";
                            MstRiskResult.Risk = "";
                            MstRiskResult.RootCost = null;
                            MstRiskResult.AuditScores = null;
                            MstRiskResult.FinancialImpact = null;
                            MstRiskResult.Recomendation = "";
                            MstRiskResult.ManagementResponse = "";
                            MstRiskResult.FixRemark = "";
                            MstRiskResult.TimeLine = null;
                            MstRiskResult.PersonResponsible = null;
                            MstRiskResult.Owner = null;
                            MstRiskResult.ObservationRating = null;
                            MstRiskResult.ObservationCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                            MstRiskResult.AStatusId = 1;

                            transaction.PersonResponsible = null;
                            transaction.Owner = null;
                            transaction.ObservatioRating = null;
                            transaction.ObservationCategory = null;
                            transaction.ObservationSubCategory = null;

                            List<PrerequisiteTran_Upload> AnnxetureList = new List<PrerequisiteTran_Upload>();
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();

                            if (fileUploadDocument.HasFile)
                            {
                                HttpFileCollection fileCollection = Request.Files;
                                if (fileCollection.Count > 0)
                                {
                                    string directoryPathIFD = "";
                                    directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalId.ToString() + "/" + InstanceData.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ScheduleonID + "/0.0");
                                    DocumentManagement.CreateDirectory(directoryPathIFD);

                                    string directoryPath = "";

                                    if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                                    {
                                        directoryPath = Server.MapPath("~/AuditPreRequisiteDocument/" + CustomerId + "/"
                                          + Convert.ToInt32(AuditID) + "/" + Convert.ToInt32(ATBDID) + "/"
                                          + Convert.ToInt32(ChecklistID) + "/0.0");
                                    }
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath);
                                    }

                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        #region
                                        HttpPostedFile uploadfile = fileCollection[i];
                                        string[] keys = fileCollection.Keys[i].Split('$');
                                        String fileName = "";
                                        if (keys[keys.Count() - 1].Equals("fileUploadDocument"))
                                        {
                                            fileName = uploadfile.FileName;
                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                            string finalPathPR = Path.Combine(directoryPathIFD, fileKey + Path.GetExtension(uploadfile.FileName));
                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            if (uploadfile.ContentLength > 0)
                                            {
                                                Workinglist.Add(new KeyValuePair<string, int>(fileName, 1));
                                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                                WorkingFilelist.Add(new KeyValuePair<string, Byte[]>(finalPathPR, bytes));

                                                int sid = 1;
                                                var query = (from row in entities.PrerequisiteTran_Upload
                                                             where row.Status == 4 && row.checklistId == ChecklistID
                                                             && row.AuditID == AuditID
                                                             && row.ATBDId == ATBDID
                                                             select row).ToList();
                                                if (query.Count > 0)
                                                {
                                                    sid = 5;
                                                }
                                                string filename1 = string.Empty;
                                                if (string.IsNullOrEmpty(txtRemark.Text))
                                                {
                                                    txtRemark.Text = "NA";
                                                }
                                                PrerequisiteTran_Upload DraftClosureUpload = new PrerequisiteTran_Upload()
                                                {
                                                    CustomerId = CustomerId,
                                                    ATBDId = ATBDID,
                                                    FileName = fileName,
                                                    AuditID = AuditID,
                                                    checklistId = ChecklistID,
                                                    checklistName = ChecklistDocument,
                                                    IsDeleted = false,
                                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                    FileKey = fileKey.ToString(),
                                                    Version = "0.0",
                                                    VersionDate = DateTime.Now,
                                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                                    CreatedDate = DateTime.Now,
                                                    Status = sid,
                                                    StatusRemak = "New Audit Checklist Assigned.",
                                                    Remark = txtRemark.Text,
                                                    MAPId = MAPID,
                                                    Flag = "PR",
                                                };
                                                #region
                                                var preRequisiteTran_Upload = GetDetailsFromPrerequisiteTran_Upload(DraftClosureUpload);
                                                if (preRequisiteTran_Upload.Count > 0)
                                                {
                                                    int fileVersionCount = preRequisiteTran_Upload.Count();
                                                    string[] fileNameArray = DraftClosureUpload.FileName.Split('.');
                                                    string newFileNameVersion = fileNameArray[0].ToString() + "_" + Convert.ToString(fileVersionCount);
                                                    DraftClosureUpload.FileName = newFileNameVersion + "." + fileNameArray[1].ToString();
                                                    filename1 = DraftClosureUpload.FileName;
                                                }
                                                else if (preRequisiteTran_Upload.Count == 0)
                                                {
                                                    int fileVersionCount = preRequisiteTran_Upload.Count();
                                                    string[] fileNameArray = DraftClosureUpload.FileName.Split('.');
                                                    string newFileNameVersion = fileNameArray[0].ToString() + "_" + Convert.ToString(fileVersionCount);
                                                    DraftClosureUpload.FileName = newFileNameVersion + "." + fileNameArray[1].ToString();
                                                    filename1 = DraftClosureUpload.FileName;
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry1.IsValid = false;
                                                    cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                                                }
                                                #endregion
                                                AnnxetureList.Add(DraftClosureUpload);

                                                InternalFileData_Risk file = new InternalFileData_Risk()
                                                {
                                                    Name = filename1,
                                                    FilePath = directoryPathIFD.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                    FileKey = fileKey.ToString(),
                                                    Version = "0.0",
                                                    VersionDate = DateTime.UtcNow,
                                                    ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                                                    CustomerBranchId = Convert.ToInt32(InstanceData.CustomerBranchID),
                                                    FinancialYear = InstanceData.FinancialYear,
                                                    IsDeleted = false,
                                                    ATBDId = ATBDID,
                                                    VerticalID = InstanceData.VerticalId,
                                                    AuditID = AuditID,
                                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                };

                                                #region
                                                if (!IsRecordExists(file.ProcessID, file.FinancialYear, file.ATBDId, Convert.ToInt32(file.AuditID), Convert.ToInt32(file.VerticalID)))
                                                {
                                                    file.UploadedDocumentFlag = "Open";
                                                }
                                                #endregion
                                                Workingfiles.Add(file);
                                            }
                                        }
                                        #endregion
                                    }//For Loop End  
                                }
                            }
                            else if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                int sid = 1;
                                var query = (from row in entities.PrerequisiteTran_Upload
                                             where row.Status == 4 && row.checklistId == ChecklistID
                                             && row.AuditID == AuditID
                                             && row.ATBDId == ATBDID
                                             select row).ToList();
                                if (query.Count > 0)
                                {
                                    sid = 5;
                                }
                                PrerequisiteTran_Upload DraftClosureUpload = new PrerequisiteTran_Upload()
                                {
                                    CustomerId = CustomerId,
                                    ATBDId = ATBDID,
                                    FileName = "NA",
                                    AuditID = AuditID,
                                    checklistId = ChecklistID,
                                    checklistName = ChecklistDocument,
                                    IsDeleted = false,
                                    FilePath = null,
                                    FileKey = null,
                                    Version = null,
                                    VersionDate = DateTime.Now,
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    CreatedDate = DateTime.Now,
                                    Status = 1,
                                    StatusRemak = "New Audit Checklist Assigned.",
                                    MAPId = MAPID,
                                    Remark = txtRemark.Text.Trim(),
                                    Flag = "PR",
                                };
                                AnnxetureList.Add(DraftClosureUpload);

                                InternalFileData_Risk file = new InternalFileData_Risk()
                                {
                                    Name = null,
                                    FilePath = null,
                                    FileKey = null,
                                    Version = null,
                                    VersionDate = DateTime.UtcNow,
                                    ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                                    CustomerBranchId = Convert.ToInt32(InstanceData.CustomerBranchID),
                                    FinancialYear = InstanceData.FinancialYear,
                                    IsDeleted = false,
                                    ATBDId = ATBDID,
                                    VerticalID = InstanceData.VerticalId,
                                    AuditID = AuditID,
                                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };
                                Workingfiles.Add(file);
                            }

                            var RCT = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(InstanceData.ScheduleonID), ATBDID);

                            if (RCT == null)
                            {
                                if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                {
                                    MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                }
                                else
                                {
                                    MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                }
                                if (WorkingFilelist != null && Workinglist != null)
                                {
                                    if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                    {
                                        transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        transaction.UpdatedOn = DateTime.Now;
                                        UpdateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                                    }
                                    else
                                    {
                                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        transaction.CreatedOn = DateTime.Now;
                                        CreateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                                    }
                                }
                            }
                            else
                            {
                                UpdateTransaction(RCT.AuditTransactionID, (long)RCT.AuditScheduleOnID, Workingfiles, Workinglist, WorkingFilelist);
                            }
                            if (Filelist.Count > 0)
                            {
                                DocumentManagement.Audit_SaveDocFiles(Filelist);
                            }
                            bool Success1 = false;
                            foreach (var item in AnnxetureList)
                            {
                                if (!Tran_PrerequisiteTran_UploadExists(item))
                                {
                                    Success1 = CreatePrerequisiteTran_UploadResult(item);
                                    if (Success1 == true)
                                    {
                                        if (item.Remark != null && item.FileName != null)
                                        {
                                            cvDuplicateEntry1.IsValid = false;
                                            cvDuplicateEntry1.ErrorMessage = "Details Saved Successfully";
                                        }

                                    }
                                    else
                                    {
                                        cvDuplicateEntry1.IsValid = false;
                                        cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry1.IsValid = false;
                                    cvDuplicateEntry1.ErrorMessage = "Same Name Already Exists..";
                                }
                            }
                        }
                    }
                }
                txtRemark.Text = string.Empty;
                fileUploadDocument.Enabled = true;
                upPrerequisiteUpload.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowAddRemarkDoc();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static SP_getinstancedataprerequsite_Result GetInternalAuditInstanceData(long atbdid, long auditid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var instanceData = (from row in entities.SP_getinstancedataprerequsite(atbdid, auditid)
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        private bool UpdatePrerequisiteTran_UploadResult(PrerequisiteTran_Upload pTUinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == pTUinstance.AuditID
                             && row.ATBDId == pTUinstance.ATBDId
                             && row.checklistId == pTUinstance.checklistId
                             && row.CustomerId == pTUinstance.CustomerId
                             select row).FirstOrDefault();
                if (query != null)
                {
                    query.CustomerId = pTUinstance.CustomerId;
                    query.ATBDId = pTUinstance.ATBDId;
                    query.FileName = pTUinstance.FileName;
                    query.AuditID = pTUinstance.AuditID;
                    query.checklistId = pTUinstance.checklistId;
                    query.checklistName = pTUinstance.checklistName;
                    query.IsDeleted = pTUinstance.IsDeleted;
                    query.FilePath = pTUinstance.FilePath;
                    query.FileKey = pTUinstance.FileKey;
                    query.Version = pTUinstance.Version;
                    query.VersionDate = pTUinstance.VersionDate;
                    query.CreatedDate = pTUinstance.CreatedDate;
                    query.CreatedBy = pTUinstance.CreatedBy;
                    query.Status = pTUinstance.Status;
                    query.StatusRemak = pTUinstance.StatusRemak;
                    query.MAPId = pTUinstance.MAPId;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool checkPrerequisiteTran_UploadResultExist(PrerequisiteTran_Upload pTUinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (pTUinstance != null)
                {
                    var query = (from row in entities.PrerequisiteTran_Upload
                                 where row.AuditID == pTUinstance.AuditID
                                 && row.ATBDId == pTUinstance.ATBDId
                                 && row.checklistId == pTUinstance.checklistId
                                 && row.CustomerId == pTUinstance.CustomerId
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        protected void ddlPerformerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlPerformer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlPerformer");
                    if (ddlPerformer.Enabled)
                    {
                        ddlPerformer.ClearSelection();
                        ddlPerformer.SelectedValue = ddlPerformerFilter.SelectedValue;
                        upPR.Update();
                    }
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "setTimeLine();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void txtStartDateKickoff_TextChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    TextBox txtStartDate = (TextBox)grdProcessAudit.Rows[i].FindControl("txtStartDate");
                    if (txtStartDate.Enabled)
                    {
                        txtStartDate.Text = "";
                        txtStartDate.Text = txtStartDateKickoff.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlReviewerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcessAudit.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcessAudit.Rows[i].FindControl("ddlReviewer");
                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdProcessAudit.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindProcessAudits();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessAudit.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string getstatus(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RecentPTUs
                             where row.AuditID == AuditID && row.ATBDId == ATBDID
                             && row.checklistId == ChecklistID
                             select row.Status).FirstOrDefault();
                if (query != null)
                {
                    return Convert.ToString(query);
                }
                else
                {
                    return "";
                }
            }
        }
        protected bool CheckActionButtonEnable(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == AuditID && row.ATBDId == ATBDID
                             && row.checklistId == ChecklistID
                             && row.IsDeleted == false
                             && row.Status != 1
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public void BindProcessAudits()
        {
            try
            {
                string flag = string.Empty;
                string FinancialYear = string.Empty;
                int customerID = -1;
                long processid = -1;
                long subprocessid = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int RoleID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["RoID"]))
                {
                    RoleID = Convert.ToInt32(Request.QueryString["RoID"]);
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        if (!String.IsNullOrEmpty(ddlProcess1.SelectedValue))
                        {
                            if (ddlProcess1.SelectedValue != "-1")
                            {
                                processid = Convert.ToInt32(ddlProcess1.SelectedValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlSubProcess1.SelectedValue))
                        {
                            if (ddlSubProcess1.SelectedValue != "-1")
                            {
                                subprocessid = Convert.ToInt32(ddlSubProcess1.SelectedValue);
                            }
                        }

                        auditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        lblaname.Text = (from row in entities.SP_GetAuditName(auditID)
                                         select row).FirstOrDefault();
                        //aval

                        var query = (from row in entities.SP_prerequisiteData(auditID, AuthenticationHelper.UserID)
                                     select row).ToList();

                        if (RoleID != -1)
                        {
                            query = query.Where(entry => entry.RoleID == RoleID).ToList();
                        }
                        grdProcessAudit.DataSource = null;
                        grdProcessAudit.DataBind();

                        if (query != null)
                        {
                            if (processid != -1)
                            {
                                query = query.Where(entry => entry.ProcessId == processid).ToList();
                            }
                            if (subprocessid != -1)
                            {
                                query = query.Where(entry => entry.SubProcessId == subprocessid).ToList();
                            }

                            grdProcessAudit.DataSource = query;
                            Session["TotalRows"] = null;
                            Session["TotalRows"] = query.Count;
                            grdProcessAudit.DataBind();
                            if (query.Count > 0)
                                btnSave.Visible = true;
                            else
                                btnSave.Visible = false;
                        }
                    }

                    if (HttpContext.Current.Cache.Get("ProcessAuditPrerequisiteData") != null)
                        HttpContext.Current.Cache.Remove("ProcessAuditPrerequisiteData");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                    ListItem removeItem = DropDownListPageNo.Items.FindByText("0");
                    DropDownListPageNo.Items.Remove(removeItem);
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void btnHiddenUpdatUser_Click(object sender, EventArgs e)
        {
            BindPerformerFilter(ddlPerformerFilter); BindReviewerFilter();
            BindProcessAudits();
        }

        protected void btnAddCheckList_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbxCheckListName.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ATBDID"])))
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        StepChecklistMapping objStepChecklistMapping = new StepChecklistMapping();
                        objStepChecklistMapping.ATBDID = Convert.ToInt64(ViewState["ATBDID"]);
                        objStepChecklistMapping.ChecklistDocument = tbxCheckListName.Text.Trim();
                        objStepChecklistMapping.IsActive = true;

                        var result = (from row in entities.StepChecklistMappings
                                      where row.ATBDID == objStepChecklistMapping.ATBDID &&
                                      row.ChecklistDocument == tbxCheckListName.Text.Trim()
                                      select row).FirstOrDefault();

                        if (result == null)
                        {
                            entities.StepChecklistMappings.Add(objStepChecklistMapping);
                            entities.SaveChanges();
                            cvChecklist.IsValid = false;
                            cvChecklist.ErrorMessage = "Document Name Added Successfully.";
                            tbxCheckListName.Text = string.Empty;
                            BindProcessAudits();
                        }
                        else
                        {
                            cvChecklist.IsValid = false;
                            cvChecklist.ErrorMessage = "Already same document name exist.";
                        }
                    }
                }
            }
            else
            {
                cvChecklist.IsValid = false;
                cvChecklist.ErrorMessage = "Document name should not be blank.";
            }
        }

        /*Added by Madhur*/
        protected bool CheckRemarkDocButtonEnable(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int auditID = Convert.ToInt32(AuditID);
                int atbdID = Convert.ToInt32(ATBDID);
                int checklistID = Convert.ToInt32(ChecklistID);

                var query = (from row in entities.usp_PreRequisiteGridIconSelection(checklistID, auditID, customerID, atbdID, "PR")
                             where row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected bool CheckRemarkDocButtonEnableFirst(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int auditID = Convert.ToInt32(AuditID);
                int atbdID = Convert.ToInt32(ATBDID);
                int checklistID = Convert.ToInt32(ChecklistID);

                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.IsDeleted == false
                             && row.AuditID == auditID
                             && row.ATBDId == atbdID
                             && row.checklistId == checklistID
                             && row.CustomerId == customerID
                             select row).ToList();

                if (query.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        protected bool CheckRemarkDocButtonEnableFileUpload(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int auditID = Convert.ToInt32(AuditID);
                int atbdID = Convert.ToInt32(ATBDID);
                int checklistID = Convert.ToInt32(ChecklistID);
                var query = (from row in entities.usp_PreRequisiteGridIconSelection(checklistID, auditID, customerID, atbdID, "AR")
                             where row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<PrerequisiteTran_Upload> GetDetailsFromPrerequisiteTran_Upload(PrerequisiteTran_Upload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == ICR.AuditID
                                    && row.ATBDId == ICR.ATBDId
                                    && row.checklistId == ICR.checklistId
                                    && row.IsDeleted == false
                                    && row.Status != 1
                                    && row.Remark == ICR.Remark
                             select row).ToList();

                return query;
            }
        }

        private bool IsRecordExists(long ProcessID, string FinancialYear, long ATBDId, long AuditID, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.InternalFileData_Risk
                             where cs.ProcessID == ProcessID
                             && cs.FinancialYear == FinancialYear
                             && cs.ATBDId == ATBDId
                             && cs.AuditID == AuditID
                             && cs.VerticalID == VerticalID
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool UpdateTransaction(long transactionid, long Scheduleonid, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            long transactionId = transactionid;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = Scheduleonid;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                                       where row.ProcessId == transaction.ProcessId && row.FinancialYear == transaction.FinancialYear
                                                                           && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                                                                           && row.UserID == transaction.UserID && row.RoleID == transaction.RoleID && row.ATBDId == transaction.ATBDId
                                                                       select row).FirstOrDefault();

                            RecordtoUpdate.PersonResponsible = transaction.PersonResponsible;
                            RecordtoUpdate.ObservatioRating = transaction.ObservatioRating;
                            RecordtoUpdate.ObservationCategory = transaction.ObservationCategory;
                            RecordtoUpdate.ForPeriod = transaction.ForPeriod;
                            RecordtoUpdate.Dated = DateTime.Now;
                            long transactionId = RecordtoUpdate.ID;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CreateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files,
            List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.InternalAuditTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateTransaction(List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, long ScheduleonID, long atbdid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var instanceData = (from row in entities.InternalAuditTransactions
                                                where row.AuditScheduleOnID == ScheduleonID
                                                && row.ATBDId == atbdid
                                                select row).OrderByDescending(a => a.ID).Take(1).FirstOrDefault();

                            if (instanceData != null)
                            {
                                DocumentManagement.Audit_SaveDocFiles(filesList);
                                if (files != null)
                                {
                                    foreach (InternalFileData_Risk fl in files)
                                    {
                                        fl.IsDeleted = false;
                                        entities.InternalFileData_Risk.Add(fl);
                                        entities.SaveChanges();

                                        InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                        fileMapping.TransactionID = instanceData.ID;
                                        fileMapping.FileID = fl.ID;
                                        fileMapping.ScheduledOnID = ScheduleonID;
                                        if (list != null)
                                        {
                                            fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                        }
                                        entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                    }
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Download"))
            {
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int checkListId = Convert.ToInt32(commandArgs[0]);
                int auditId = Convert.ToInt32(commandArgs[1]);
                int atbdId = Convert.ToInt32(commandArgs[2]);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    PrerequisiteTran_Upload pre = new PrerequisiteTran_Upload();
                    pre = (from row in entities.PrerequisiteTran_Upload
                           where row.CustomerId == customerID
                           && row.checklistId == checkListId
                           && row.AuditID == auditId
                           && row.ATBDId == atbdId
                           && row.Flag == "PR"
                           select row).OrderByDescending(m => m.MAPId).FirstOrDefault();

                    if (pre != null)
                    {
                        if (pre.FilePath != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(pre.FilePath), pre.FileKey + Path.GetExtension(pre.FileName));
                            //string filePath = Path.Combine(pre.FilePath + "/" + pre.FileKey + Path.GetExtension(pre.FileName));
                            if (filePath != null && File.Exists(filePath))
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + pre.FileName);
                                //Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                Response.Flush(); // send it to the client to download
                                Response.End();
                            }
                        }

                    }
                }
            }
            else if (e.CommandName.Equals("Delete"))
            {
                int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int auditId = Convert.ToInt32(commandArgs[0]);
                int atbdId = Convert.ToInt32(commandArgs[1]);
                int checkListId = Convert.ToInt32(commandArgs[2]);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    PrerequisiteTran_Upload pre = new PrerequisiteTran_Upload();
                    pre = (from row in entities.PrerequisiteTran_Upload
                           where row.CustomerId == customerID
                           && row.checklistId == checkListId
                           && row.AuditID == auditId
                           && row.ATBDId == atbdId
                           && row.Flag == "PR"
                           select row).OrderByDescending(m => m.MAPId).ThenByDescending(m => m.Id).FirstOrDefault();

                    if (pre != null)
                    {
                        pre.FileName = "NA";
                        pre.FileKey = string.Empty;
                        pre.FilePath = string.Empty;
                        entities.SaveChanges();
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "File Deleted Successfully.";
                    }
                }
            }
        }

        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void rptComplianceDocumnets_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            rptComplianceDocumnets.DataSource = null;
            rptComplianceDocumnets.DataBind();
            grdProcessAudit_RowCommand(sender, (GridViewCommandEventArgs)Session["GridviewCommand"]);
        }

        protected void lnkUploadedDocument_Click(object sender, EventArgs e)
        {
            LinkButton lnkUploadedDoc = (LinkButton)(sender);
            string valUploadDoc = lnkUploadedDoc.CommandArgument;
            if (valUploadDoc != "")
            {
                string[] file_chkId = valUploadDoc.Split('`');

                string fileName = file_chkId[0];
                int chkId = Convert.ToInt32(file_chkId[1]);

                if (fileName != null)
                {
                    if (fileName != "NA")
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var query = (from row in entities.PrerequisiteTran_Upload
                                         where row.FileName == fileName && row.checklistId == chkId
                                         select row).FirstOrDefault();

                            if (query != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(query.FilePath), query.FileKey + Path.GetExtension(query.FileName));
                                string extn = Path.GetExtension(query.FileName);
                                string[] extnNew = extn.Split('.');
                                string extnUse = extnNew[1];
                                if (filePath != null && File.Exists(filePath))
                                {
                                    try
                                    {
                                        Response.Buffer = true;
                                        Response.Clear();
                                        Response.ClearContent();
                                        Response.ContentType = "application/" + extnUse;
                                        Response.AddHeader("content-disposition", "attachment; filename=" + query.FileName);
                                        Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        //Response.TransmitFile(filePath);
                                        //Response.Flush(); // send it to the client to download
                                        //Response.End();
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtRemark.Text = string.Empty;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#PrerequisiteUpload').modal('hide');", true);
        }
    }
}