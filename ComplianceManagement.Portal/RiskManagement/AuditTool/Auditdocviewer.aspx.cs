﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class Auditdocviewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int FileID = 0;
                int ScheduleOnID = 0;
                int ATBDID = 0;
                int AuditID = 0;
                bool IMG = false;
                string CompDocReviewPath = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["FileID"]))
                {
                    FileID = Convert.ToInt32(Request.QueryString["FileID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ScheduleOnID"]))
                {
                    ScheduleOnID = Convert.ToInt32(Request.QueryString["ScheduleOnID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IMG"]))
                {
                    IMG = true;
                }
                #region History File
                if (FileID != 0 && !IMG)
                {
                    InternalReviewHistory fileData = GetFileData(FileID, ScheduleOnID, ATBDID, AuditID);
                    if (fileData == null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('There is no file for view')", true);
                        return;
                    }
                    else
                    {
                        string filePath = Path.Combine(Server.MapPath(fileData.FilePath), fileData.FileKey + Path.GetExtension(fileData.Name));

                        if (fileData.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + FileData;

                            string extension = System.IO.Path.GetExtension(filePath);

                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (fileData.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();

                            doccontrol.Document = doccontrol.Document = Server.MapPath(FileName);
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('There is no file for view')", true);
                            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "CloseParent();", true);
                            return;
                        }
                        //break;
                    }
                }
                #endregion

                #region Image
                if (FileID != 0 && IMG)
                {
                    string EnType = "A";
                    ObservationImage AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFile(FileID);

                    if (AllinOneDocumentList == null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('There is no file for view')", true);
                        return;
                    }

                    string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.ImagePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.ImageName));

                    if (AllinOneDocumentList.ImagePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + FileData;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(Portal.Common.AuthenticationHelper.CustomerID);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();
                        doccontrol.Document = doccontrol.Document = Server.MapPath(FileName);
                    }
                }
                #endregion
            }
        }

        public static InternalReviewHistory GetFileData(int id, int AuditScheduleOnId, int ATBDId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.InternalReviewHistories
                                where row.ID == id
                                select row).FirstOrDefault();

                return fileData;
            }
        }
    }
}