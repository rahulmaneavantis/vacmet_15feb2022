﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class FailedControlTesting : System.Web.UI.Page
    {
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFnancialYear();
                BindAuditTransactions();
                bindPageNumber();
            };
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceTransactions.PageIndex = chkSelectedPage - 1;                     
            grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);         
            BindAuditTransactions();

        }

        public int GetQuarter(DateTime date)
        {
            if (date.Month >= 4 && date.Month <= 6)
                return 1;
            else if (date.Month >= 7 && date.Month <= 9)
                return 2;
            else if (date.Month >= 10 && date.Month <= 12)
                return 3;
            else
                return 4;

        }
        public string GetQuarterName(int number)
        {
            string quartername = "";
            if (number == 1)
            {
                quartername = "Apr - Jun";
            }
            else if (number == 2)
            {
                quartername = "Jul - Sep";
            }
            else if (number == 3)
            {
                quartername = "Oct - Dec";
            }
            else if (number == 4)
            {
                quartername = "Jan - Mar";
            }
            return quartername;
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
        }      
        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();            
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdComplianceTransactions.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlKeyNonKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdComplianceTransactions.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdComplianceTransactions.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdComplianceTransactions.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        

        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" All ", "-1"));
        }
       
        protected void ddlDesign_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList btn = (DropDownList)sender;            
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            DropDownList Design = (DropDownList)gvr.FindControl("ddlDesign");
            DropDownList OprationEffectiveness = (DropDownList)gvr.FindControl("ddlOprationEffectiveness");
            Label ProcessID = (Label)gvr.FindControl("lblProcessID");
            Label SubProcessID = (Label)gvr.FindControl("lblSubProcessId");
            if (!String.IsNullOrEmpty(Design.SelectedItem.Text))
            {
                if (Design.SelectedItem.Text == "Fail")
                {
                    OprationEffectiveness.Enabled = false;
                }
                else
                {
                    OprationEffectiveness.Enabled = true;
                }
            }
        }
        protected void ddlOprationEffectiveness_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        public static long GetKeyByKeyName(string KeyName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long catagoryIDs = (from row in entities.mst_Key
                                    where row.Name == KeyName
                                    select row.Id).SingleOrDefault();

                return catagoryIDs;
            }
        }
      
        public bool checkvisibility(string DownloadFileName)
        {
            bool testtcontrol = false;
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                testtcontrol = true;
            }
            else
            {
                testtcontrol = false;
            }
            return testtcontrol;
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        public string ShowTestingDocumentName(string DownloadFileName)
        {

            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }       
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int? ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int RiskCreationId = Convert.ToInt32(commandArgs[1]);
                string FinancialYear = Convert.ToString(commandArgs[2]);
                int CustomerBranchID = Convert.ToInt32(commandArgs[3]);
                string Period = Convert.ToString(commandArgs[4]);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowDialog('" + ScheduledOnID + "','" + RiskCreationId + "','" + FinancialYear + "','" + CustomerBranchID + "','" + Period + "');", true);              
                BindAuditTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindAuditTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

       

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //try
            //{
            //    if (!(e.CommandName.Equals("Sort")))
            //    {
            //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            //        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
            //        int RiskCreationId = Convert.ToInt32(commandArgs[1]);
            //        string FinancialYear = Convert.ToString(commandArgs[2]);
            //        int CustomerBranchID = Convert.ToInt32(commandArgs[3]);
            //        string Period = Convert.ToString(commandArgs[4]);
            //       // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowDialog('" + ScheduledOnID + "','" + RiskCreationId + "','" + FinancialYear + "','" + CustomerBranchID + "','" + Period + "');", true);                  
            //    }

            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }
        protected string GetReviewer(int AuditStatusID, long? scheduledonid, long RiskCreationId)
        {
            try
            {
                string result = "";
                result = DashboardManagementRisk.GetUserName(AuditStatusID, scheduledonid, RiskCreationId, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }
        /// <summary>
        /// this function bind the upcoming compliances to the grid view.
        /// </summary>
        /// 
        public string ShowFrequencyName(long? id)
        {
            List<FrequencyName_Result> a = new List<FrequencyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetFrequencyNameProcedure(Convert.ToInt32(id)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        protected void BindAuditTransactions()
        {
            try
            {
                string FlagStatus = "";
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    FlagStatus = Request.QueryString["Status"];
                }              
                List<FailedControlQuarterWiseDisplayView> assignmentList = null;
                //long pid = -1;
                //long psid = -1;
                //long Branchid = -1;
                string finacialyear = "";
                long Period = -1;
                string KeyNonKey = "";
                long AuditStatus = -1;
                long CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlQuarter.SelectedValue))
                {
                    if (ddlQuarter.SelectedValue != "-1")
                    {
                        Period = Convert.ToInt32(ddlQuarter.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlKeyNonKey.SelectedValue))
                {
                    if (ddlKeyNonKey.SelectedValue != "-1")
                    {
                        KeyNonKey = Convert.ToString(ddlKeyNonKey.SelectedItem.Text);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancial.SelectedItem.Text))
                {
                    if (ddlFilterFinancial.SelectedItem.Text != "All")
                    {
                        finacialyear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        AuditStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                    }
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                assignmentList = DashboardManagementRisk.DashboardDataForFailedControlTesting(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, FlagStatus, CustomerBranchId, -1, -1, finacialyear, KeyNonKey, Period, AuditStatus, CustomerId);
                grdComplianceTransactions.DataSource = assignmentList;
                Session["TotalRows"] = assignmentList.Count;
                grdComplianceTransactions.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 4 || statusID == 3;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }          
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);              
                //Reload the Grid
                BindAuditTransactions();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceTransactions.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }       
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindAuditTransactions();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceTransactions.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindAuditTransactions();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceTransactions.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindAuditTransactions();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceTransactions.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindAuditTransactions();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceTransactions.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindAuditTransactions();
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdComplianceTransactions.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.PersonResponsibleFillSubEntityDataICFR(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }    
    }
}