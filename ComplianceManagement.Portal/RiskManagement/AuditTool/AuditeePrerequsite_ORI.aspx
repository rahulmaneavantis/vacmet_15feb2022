﻿<%@ Page Title="My Workspace" Language="C#" MasterPageFile="~/AuditTool.Master" 
    AutoEventWireup="true" Async="true" EnableEventValidation="false" 
    CodeBehind="AuditeePrerequsite_ORI.aspx.cs" 
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditeePrerequsite_ORI" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        .btuploadss {
            background-image: url(../../Images/icon-updated.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }

        .btdownss {
            background-image: url(../../Images/icon-download.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function ShowDialog(RID, UID, AuditID) {
            $('#divPreShowDialog').modal('show');
            $('#Preshowdetails').attr('width', '98%');
            $('#Preshowdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#Preshowdetails').attr('src', "../AuditTool/CheckListPreReqsite.aspx?RoID=" + RID + "&USID=" + UID + "&AuditID=" + AuditID);
        };
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace/ ' + filterbytype);
            }
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.
            var now = new Date();
            var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var diffDays = now.getDate() - firstDayPrevMonth.getDate();
            if (diffDays > 15) {
                var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth(), 1);
            }

            //
            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    minDate: firstDayPrevMonth,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    minDate: firstDayPrevMonth,
                });
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <div class="panel-body">

                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 10%;">
                                    <div class="col-md-2 colpadding0" style="width: 20%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                               
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                                   <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Audit Name" Width="95%"
                                        Enabled="false"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location" Style="float: left; width: 90%;" AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    
                                   
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                      <asp:DropDownListChosen runat="server" ID="ddlProcess" DataPlaceHolder="Select Process" Width="95%"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location"
                                                        Style="float: left; width: 90%;" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>

                                </div>                               
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                       <asp:DropDownListChosen runat="server" ID="ddlSubProcess" DataPlaceHolder="Select Sub Process" Width="95%"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                        class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                      </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; text-align: right;">
                                    <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false"
                                    PageSize="20"
                                    AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                    OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true"
                                    OnRowCommand="grdRiskActivityMatrix_RowCommand">
                                    <Columns>
                                          <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                             <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>   
                                        <asp:TemplateField HeaderText="auditiD" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                <asp:Label ID="lblATBDID" runat="server" Text='<%# Eval("ATBDID") %>'></asp:Label>
                                                <asp:Label ID="lblChecklistID" runat="server" Text='<%# Eval("ChecklistID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblAuditName" runat="server" Text='<%# Eval("AuditName") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                       <%-- <asp:TemplateField HeaderText="Step">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblActivityTobeDone" runat="server" Text='<%# Eval("ActivityTobeDone") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ActivityTobeDone") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Document Requried">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblChecklistDocument" runat="server" Text='<%# Eval("ChecklistDocument") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ChecklistDocument") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Auditor">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblAuditor" runat="server" Text='<%# Eval("Auditor") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Auditor") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Reviewer">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TimeLine">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                                    <asp:Label ID="lblTimeLine" runat="server"
                                                        Text='<%# Eval("Timeline")!=null? Convert.ToDateTime(Eval("Timeline")).ToString("dd-MMM-yyyy"):"" %>'
                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Timeline")!=null? Convert.ToDateTime(Eval("Timeline")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="15px" HeaderText="Upload Document">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="updoc" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:FileUpload ID="DraftFileUpload" AllowMultiple="true" Style="width: 185px;" runat="server" />

                                                        <asp:Button ID="UploadDraft" runat="server"
                                                            OnClick="UploadDraft_Click"
                                                            CommandArgument='<%# Eval("AuditID") + "," + Eval("ATBDID") + "," + Eval("ChecklistID")  %>'
                                                            CssClass="btuploadss" data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                            CausesValidation="true" />


                                                        <asp:LinkButton ID="lblDocumentDownLoadDraftFile" runat="server"
                                                            data-toggle="tooltip" data-placement="top" ToolTip="Download"
                                                            CommandArgument='<%# Eval("AuditID") + "," + Eval("ATBDID") + "," + Eval("ChecklistID")  %>'
                                                            Visible='<%# CheckActionButtonEnable((long)Eval("AuditID"),(long)Eval("ATBDID"),(int)Eval("ChecklistID")) %>'
                                                            CssClass="btdownss" CommandName="DRDocumentDownLoadfile"></asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="UploadDraft" />
                                                        <asp:PostBackTrigger ControlID="lblDocumentDownLoadDraftFile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div style="float: right;">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15" Width="120%" Height="30px"
                                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p>
                                                <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0" style="float: right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">

                                        <div class="table-paging-text" style="float: right;">
                                            <p>
                                                Page
                                      
                                            </p>
                                        </div>

                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                         </section>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divPreShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="Preshowdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

