﻿<%@ Page Title="Upload Data" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="frmUploadData.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.frmUploadData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
    <script type="text/javascript">
    $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Upload Data');
        });
        </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div>
                    <asp:Button Text="Add Risk" ID="Tab1" CssClass="btn btn-primary" runat="server"
                        OnClick="Tab1_Click" />
                </div>
                <%--  <asp:Button Text="Update Compliance" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                    OnClick="Tab2_Click" />
                <asp:Button Text="Add Checklist" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                    OnClick="Tab3_Click" />--%>
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <div>                          
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ForeColor="Red" ValidationGroup="oplValidationGroup" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>
                        <div>
                            <asp:RadioButton ID="rdoAct" runat="server" AutoPostBack="false" style="color: #999;" Text="Process" GroupName="uploadContentGroup" /><br />
                            <asp:RadioButton ID="rdoCompliance" runat="server" AutoPostBack="false" style="color: #999;" Text="Risk Category Creation" GroupName="uploadContentGroup" /><br />
                        </div>
                        <div>
                            <p style="color: #999; margin-top: 5px; float: left; margin-right:5px;">Upload File :  </p>
                            <asp:Label ID="lblUploadFile" runat="server"></asp:Label>                        
                            <asp:FileUpload ID="MasterFileUpload" runat="server" style="color: #999;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File."  ControlToValidate="MasterFileUpload"
                                runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                        </div>                        
                        <div>
                            <div style="margin-bottom: 10px">
                     </div >
                            <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup" CssClass="btn btn-primary" OnClick="btnUploadFile_Click" />                        
                            </div>
                        <div style="margin-bottom: 7px">
                     </div>
                    </asp:View>
                </asp:MultiView>                 
            </div>
        </div>
    </div>
    <%--<table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Add Risk" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
              <%--  <asp:Button Text="Update Compliance" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                    OnClick="Tab2_Click" />
                <asp:Button Text="Add Checklist" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                    OnClick="Tab3_Click" />

                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2">
                                        <div>
                                            <asp:RadioButton ID="rdoAct" runat="server" AutoPostBack="false" Text="Process" GroupName="uploadContentGroup" /><br />
                                            <asp:RadioButton ID="rdoCompliance" runat="server" AutoPostBack="false" Text="Risk Category Creation" GroupName="uploadContentGroup" /><br />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>
                   <%-- <asp:View ID="View2" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup1" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage1" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage1" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />

                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile1" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload1" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload1"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile1" runat="server" Text="Upload" ValidationGroup="oplValidationGroup1"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile1_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>
                    <asp:View ID="View3" runat="server">

                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup2" />
                                <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMessage2" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="LblErormessage2" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:CustomValidator ID="cvDuplicateEntry2" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup2" Display="None" Enabled="true" ShowSummary="true" />

                                </div>
                            </div>
                            <table align="left" cellpadding="2" style="margin-left: 55px;">
                                <tr>
                                    <td style="text-align: left; padding-left: 80px;" colspan="2">
                                        <div>
                                            <asp:RadioButton ID="rdoAct1" runat="server" AutoPostBack="false" Text="Act" GroupName="uploadContentGroup1" /><br />
                                            <asp:RadioButton ID="rdoChecklist" runat="server" AutoPostBack="false" Text="Checklist" GroupName="uploadContentGroup1" /><br />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadFile2" runat="server" Text="Upload File :"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="MasterFileUpload2" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload2"
                                            runat="server" Display="None" ValidationGroup="oplValidationGroup2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 8px;" colspan="2">
                                        <asp:Button ID="btnUploadFile2" runat="server" Text="Upload" ValidationGroup="oplValidationGroup2"
                                            Style="margin-left: 75px;"
                                            OnClick="btnUploadFile2_Click" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>--%>
</asp:Content>
