﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;
using System.IO;
using OfficeOpenXml.Style;
using OfficeOpenXml;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class RiskAssignment : System.Web.UI.Page
    {
        //protected List<string> Quarterlist = new List<string>();
        public  List<long> Branchlist = new List<long>();
        public static string Flag;
        //List<ControlTestingAssignmentCheckExistView> checkeistslist = new List<ControlTestingAssignmentCheckExistView>();
        protected int CustomerId = 0;
        public static string linkclick;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["TotalRows"] = null;
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();              
                BindUsers(ddlFilterPerformer, ddlFilterReviewer);                
                BindFinancialYear();                
                BindddlQuater();
                BindddlQuaterReassing();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (string.IsNullOrEmpty(linkclick))
                {
                    linkclick = "Not Assigned";
                    btnNotAssigned_Click(sender, e);
                }
                else
                {
                    BindComplianceMatrix(linkclick);
                    bindPageNumber();
                }
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        protected void btnNotAssigned_Click(object sender, EventArgs e)
        {
            btnUploadFile1.Visible = false;
            liNotAssigned.Attributes.Add("class", "active");
            liAssigned.Attributes.Add("class", "");            
            linkclick = "Not Assigned";
            BindComplianceMatrix(linkclick);
            bindPageNumber();
        }
        protected void btnAssigned_Click(object sender, EventArgs e)
        {
            btnUploadFile1.Visible = true;
            liNotAssigned.Attributes.Add("class", "");
            liAssigned.Attributes.Add("class", "active");           
            linkclick = "Assigned";
            BindComplianceMatrix(linkclick);
            bindPageNumber();
        }
        private void BindComplianceMatrix(string buttonType)
        {
            try
            {
                Flag = "B";
                long pid = -1;
                long psid = -1;
                long CustomerBranchId = -1;
                string FnancialYear = "";
                string ForPeriod = "";
                if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        pid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        psid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedItem.Text != "-1")
                    {
                        FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                if (!String.IsNullOrEmpty(ddlQuarter.SelectedValue))
                {
                    if (ddlQuarter.SelectedItem.Text != "-1")
                    {
                        ForPeriod = Convert.ToString(ddlQuarter.SelectedItem.Text);
                    }
                }
                
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(CustomerBranchId));
                var Branchlistloop = Branchlist.ToList();

                if (buttonType == "Not Assigned")
                {
                    var AllComplianceRoleMatrix = RiskCategoryManagement.GetAllControlTestingAssignment(Portal.Common.AuthenticationHelper.UserID, CustomerId, Branchlist.ToList(), Convert.ToInt32(pid), Convert.ToInt32(psid), Flag, FnancialYear, ForPeriod);                   
                    grdComplianceRoleMatrix.DataSource = AllComplianceRoleMatrix;
                    grdComplianceRoleMatrix.DataBind();
                    Session["TotalRows"] = null;
                    Session["TotalRows"] = AllComplianceRoleMatrix.Count;                                   
                }
                else if (buttonType == "Assigned")
                {
                    var AllComplianceRoleMatrix = RiskCategoryManagement.GetAllControlTestingAssignmentAssigned(Portal.Common.AuthenticationHelper.UserID, CustomerId, Branchlist.ToList(), Convert.ToInt32(pid), Convert.ToInt32(psid), Flag, FnancialYear, ForPeriod);                   
                    grdComplianceRoleMatrix.DataSource = AllComplianceRoleMatrix;
                    grdComplianceRoleMatrix.DataBind();
                    Session["TotalRows"] = null;
                    Session["TotalRows"] = AllComplianceRoleMatrix.Count;
                    Session["grdControlTestingAssignment"] = (grdComplianceRoleMatrix.DataSource as List<SP_ControlTestingAssignmentAssignedView_Result>).ToDataTable();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindComplianceMatrixExport()
        {
            try
            {
                Flag = "E";
                long pid = -1;
                long psid = -1;
                long CustomerBranchId = -1;
                string FnancialYear = "";
                string ForPeriod = "";
                if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        pid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        psid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedItem.Text != "-1")
                    {
                        FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                if (!String.IsNullOrEmpty(ddlQuarter.SelectedValue))
                {
                    if (ddlQuarter.SelectedItem.Text != "-1")
                    {
                        ForPeriod = Convert.ToString(ddlQuarter.SelectedItem.Text);
                    }
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(CustomerBranchId));
                var Branchlistloop = Branchlist.ToList();
                var AllComplianceRoleMatrix = RiskCategoryManagement.GetAllControlTestingAssignmentAssigned(Portal.Common.AuthenticationHelper.UserID, CustomerId, Branchlist.ToList(), Convert.ToInt32(pid), Convert.ToInt32(psid), Flag, FnancialYear, ForPeriod);
                grdComplianceRoleMatrixExport.DataSource = AllComplianceRoleMatrix;
                grdComplianceRoleMatrixExport.DataBind();
                //Session["grdControlTestingAssignment"] = (grdComplianceRoleMatrixExport.DataSource as List<ControlTestingAssignmentView>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindddlQuater()
        {
            if (ddlQuarter.SelectedValue != null)
            {                
                ddlQuarter.DataTextField = "Name";
                ddlQuarter.DataValueField = "ID";
                ddlQuarter.DataSource = UserManagementRisk.GetQuarterList();
                ddlQuarter.DataBind();
            }
        }

        private void BindddlQuaterReassing()
        {
            if (ddlQuarterReassing.SelectedValue != null)
            {
                ddlQuarterReassing.DataTextField = "Name";
                ddlQuarterReassing.DataValueField = "ID";
                ddlQuarterReassing.DataSource = UserManagementRisk.GetQuarterList();
                ddlQuarterReassing.DataBind();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceRoleMatrix.PageIndex = chkSelectedPage - 1;          
            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            if (string.IsNullOrEmpty(linkclick))
            {
                linkclick = "Not Assigned";
                btnNotAssigned_Click(sender, e);
            }
            else
            {
                BindComplianceMatrix(linkclick);
                bindPageNumber();
            }
        }
        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(linkclick))
            {
                linkclick = "Not Assigned";
                btnNotAssigned_Click(sender, e);
            }
            else
            {
                BindComplianceMatrix(linkclick);
                bindPageNumber();
            }
          
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
           
        }
        public static string GetUserName(int riskcreationid, long processid, long subprocessid, long branchid,int Roleid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditAssignments
                                         join mu in entities.mst_User on row.UserID equals mu.ID
                                         where row.RiskCreationId == riskcreationid
                                         && row.RoleID == Roleid
                                         && row.ProcessId == processid 
                                         && row.SubProcessId == subprocessid
                                         && row.CustomerBranchID == branchid
                                         select mu.FirstName + " " + mu.LastName).FirstOrDefault();

                return transactionsQuery;
            }
        }
        protected string GetReviewer(int riskcreationid, long processid, long subprocessid,long branchid)
        {
            try
            {               
                string result = "";
                result = GetUserName(riskcreationid, processid, subprocessid, branchid, 4);               
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetPerformer(int riskcreationid, long processid, long subprocessid, long branchid)
        {
            try
            {
                string result = "";
                result = GetUserName(riskcreationid, processid, subprocessid, branchid, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            var data= AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = data;
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityPopup.DataTextField = "Name";
            ddlLegalEntityPopup.DataValueField = "ID";
            ddlLegalEntityPopup.Items.Clear();
            ddlLegalEntityPopup.DataSource = data;
            ddlLegalEntityPopup.DataBind();
            ddlLegalEntityPopup.Items.Insert(0, new ListItem("Unit", "-1"));
        }              
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));           
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));                    
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);           
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.ClearSelection();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.ClearSelection();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if(!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }               
            }
            
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.ClearSelection();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }
            
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }
           
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdComplianceRoleMatrix.DataSource = null;
            grdComplianceRoleMatrix.DataBind();

            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindProcess(Convert.ToInt32(ddlFilterLocation.SelectedValue));

                }
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlFilterProcess.SelectedValue != null)
            {
                if (ddlFilterProcess.SelectedValue != "-1")
                {
                    BindSubProcess();
                    if (string.IsNullOrEmpty(linkclick))
                    {
                        linkclick = "Not Assigned";
                        btnNotAssigned_Click(sender, e);
                    }
                    else
                    {
                        BindComplianceMatrix(linkclick);
                        bindPageNumber();
                    }
                }
            }

        }
        public void BindFinancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix(linkclick);
            bindPageNumber();            
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix(linkclick);
            bindPageNumber();
        }               
        private void BindUsers(DropDownList ddlPerformerUser, DropDownList ddlReviewerUser, List<long> ids = null)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                var users = RiskCategoryManagement.FillAuditKickOffUsers(CustomerId);

                ddlPerformerUser.DataTextField = "Name";
                ddlPerformerUser.DataValueField = "ID";
                ddlPerformerUser.Items.Clear();

                ddlPerformerUser.DataSource = users;
                ddlPerformerUser.DataBind();
                ddlPerformerUser.Items.Insert(0, new ListItem("Select ", "-1"));

                ddlReviewerUser.DataTextField = "Name";
                ddlReviewerUser.DataValueField = "ID";
                ddlReviewerUser.Items.Clear();

                ddlReviewerUser.DataSource = users;
                ddlReviewerUser.DataBind();
                ddlReviewerUser.Items.Insert(0, new ListItem("Select ", "-1"));
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddllist.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                ddllist.DataBind();

                ddllist.Items.Insert(0, new ListItem("Select User", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
                
        private void BindProcess(long branchid)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlFilterProcess.Items.Clear();

                ddlFilterProcess.DataTextField = "Name";
                ddlFilterProcess.DataValueField = "ID";

                ddlFilterProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID); 
                ddlFilterProcess.DataBind();
                ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSubProcess()
        {
            try
            {
                long ProcessId = -1;
                int CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        ProcessId = Convert.ToInt32(ddlFilterProcess.SelectedValue);                    
                    }
                }
                ddlFilterSubProcess.DataTextField = "Name";
                ddlFilterSubProcess.DataValueField = "ID";
                ddlFilterSubProcess.Items.Clear();
                ddlFilterSubProcess.DataSource = UserManagementRisk.FillCustomerTransactionSubProcess(ProcessId, CustomerBranchId);
                ddlFilterSubProcess.DataBind();
                ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }           
       
        public  List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public  void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindReAssingGrid()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                int processid = -1;
                string processName = string.Empty;

                string action = Convert.ToString(rbtEventReAssignment.SelectedValue);                
                        
                if (action.Equals("ICFR"))
                {
                    if (!string.IsNullOrEmpty(ddlProcessPopUp.SelectedValue))
                    {
                        processid = Convert.ToInt32(ddlProcessPopUp.SelectedValue);
                    }
                    Session["TotalRowsReAssging"] = null;
                    var AuditList = GetControlTestingAssignmentData();
                    grdReassign.DataSource = AuditList;
                    Session["TotalRowsReAssging"] = AuditList.Count;
                    grdReassign.DataBind();

                    if (AuditList.Count > 0)
                        divSavePageSummary.Attributes.Add("style", "display:block;");
                    else
                        divSavePageSummary.Attributes.Add("style", "display:none;");
                }

                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<ControlTestingAssignmentDetails> GetControlTestingAssignmentData()
        {
            int roleid = -1;            
            int branchid = -1;
            int processid = -1;
            int subprocessid = -1;
            int UserID = -1;
            string ReQuarter = string.Empty;
            string FinYear = String.Empty;
            string Period = String.Empty;

            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            if (!String.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntityPopup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3Popup.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlLocationPopup.SelectedValue))
            {
                if (ddlLocationPopup.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLocationPopup.SelectedValue);
                }
            }           

            if (!String.IsNullOrEmpty(ddlFinYearPopup.SelectedValue))
            {
                if (ddlFinYearPopup.SelectedValue != "-1")
                {
                    FinYear = ddlFinYearPopup.SelectedItem.Text;
                }
            }

            if (!String.IsNullOrEmpty(ddlQuarterReassing.SelectedValue))
            {
                if (ddlQuarterReassing.SelectedValue != "-1")
                {
                    ReQuarter = ddlQuarterReassing.SelectedItem.Text;
                }
            }

            if (!String.IsNullOrEmpty(ddlProcessPopUp.SelectedValue))
            {
                if (ddlProcessPopUp.SelectedValue != "-1")
                {
                    processid = Convert.ToInt32(ddlProcessPopUp.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubProcessPopUp.SelectedValue))
            {
                if (ddlSubProcessPopUp.SelectedValue != "-1")
                {
                    subprocessid = Convert.ToInt32(ddlSubProcessPopUp.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlRole.SelectedValue))
            {
                if (ddlRole.SelectedValue != "-1")
                {
                    roleid = Convert.ToInt32(ddlRole.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlUser.SelectedValue))
            {
                if (ddlUser.SelectedValue != "-1")
                {
                    UserID = Convert.ToInt32(ddlUser.SelectedValue);
                }
            }

            Branchlist.Clear();
            GetAllHierarchy(CustomerId, branchid);

            List<ControlTestingAssignmentDetails> AuditLists = new List<ControlTestingAssignmentDetails>();

            AuditLists = UserManagementRisk.GetAllReassignDataICFR(CustomerId, Branchlist, UserID, processid, subprocessid, roleid, FinYear, ReQuarter);

            return AuditLists;
        }


        public void BindFinancialYearPopup()
        {
            ddlFinYearPopup.DataTextField = "Name";
            ddlFinYearPopup.DataValueField = "ID";
            ddlFinYearPopup.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinYearPopup.DataBind();
            ddlFinYearPopup.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

     
        public void BindProcessPopup()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                
                if (action.Equals("ICFR"))
                {
                    ddlProcessPopUp.Items.Clear();
                    ddlProcessPopUp.DataTextField = "Name";
                    ddlProcessPopUp.DataValueField = "ProcessId";
                    ddlProcessPopUp.DataSource = UserManagementRisk.GetddlProcessDataICFR(UserID); ;
                    ddlProcessPopUp.DataBind();
                    ddlProcessPopUp.Items.Insert(0, new ListItem("Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSubProcessPopup(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcessPopUp.Items.Clear();
                    ddlSubProcessPopUp.DataTextField = "Name";
                    ddlSubProcessPopUp.DataValueField = "Id";
                    ddlSubProcessPopUp.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcessPopUp.DataBind();
                    ddlSubProcessPopUp.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlSubProcessPopUp.Items.Clear();
                    ddlSubProcessPopUp.DataTextField = "Name";
                    ddlSubProcessPopUp.DataValueField = "Id";
                    ddlSubProcessPopUp.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcessPopUp.DataBind();
                    ddlSubProcessPopUp.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }

            ViewState["ReAssignID"] = null;

            if (ddlUser.SelectedValue != "" || ddlUser.SelectedValue != "-1")
            {
                BindUserList(CustomerId, ddlREAssignUser);

                if (ddlUser.SelectedValue != "-1" || ddlUser.SelectedValue != null || ddlUser.SelectedValue != "")
                    ddlREAssignUser.Items.Remove(ddlREAssignUser.Items.FindByValue(ddlUser.SelectedValue));

                ViewState["ReAssignID"] = Convert.ToInt32(ddlUser.SelectedValue);
            }

            BindProcessPopup();          
            BindFinancialYearPopup();            
            BindReAssingGrid();            
        }

        protected void ddlREAssignUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdReassign.Rows.Count; i++)
                {
                    DropDownList ddlGridReAssignUser = (DropDownList)grdReassign.Rows[i].FindControl("ddlGridReAssignUser");

                    if (ddlGridReAssignUser.Enabled)
                    {
                        ddlGridReAssignUser.ClearSelection();
                        ddlGridReAssignUser.SelectedValue = ddlREAssignUser.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSizeReAssign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNoReAssign.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo <= GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }

                //Reload the Grid
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlProcessPopUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcessPopUp.SelectedValue))
            {
                if (ddlProcessPopUp.SelectedValue != "-1")
                {
                    BindSubProcessPopup(Convert.ToInt32(ddlProcessPopUp.SelectedValue), "P");
                }
                else
                {
                    // ddlSubProcess.Items.Clear();
                    ddlSubProcessPopUp.Items.Clear();
                    ddlSubProcessPopUp.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcessPopUp.Items.Clear();
                ddlSubProcessPopUp.Items.Insert(0, new ListItem("Sub Process", "-1"));
            }

            BindReAssingGrid();
        }

        protected void ddlSubProcessPopUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlFinYearPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);            
        }

        protected void ddlLegalEntityPopUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1Popup.Items.Count > 0)
                ddlSubEntity1Popup.Items.Clear();

            if (ddlSubEntity2Popup.Items.Count > 0)
                ddlSubEntity2Popup.Items.Clear();

            if (ddlSubEntity3Popup.Items.Count > 0)
                ddlSubEntity3Popup.Items.Clear();

            if (ddlLocationPopup.Items.Count > 0)
                ddlLocationPopup.Items.Clear();

            if (!string.IsNullOrEmpty(ddlLegalEntityPopup.SelectedValue))
            {
                if (ddlLegalEntityPopup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1Popup, Convert.ToInt32(ddlLegalEntityPopup.SelectedValue));                    
                }
            }
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2Popup.Items.Count > 0)
                ddlSubEntity2Popup.Items.Clear();

            if (ddlSubEntity3Popup.Items.Count > 0)
                ddlSubEntity3Popup.Items.Clear();

            if (ddlLocationPopup.Items.Count > 0)
                ddlLocationPopup.Items.Clear();

            if (!string.IsNullOrEmpty(ddlSubEntity1Popup.SelectedValue))
            {
                if (ddlSubEntity1Popup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2Popup, Convert.ToInt32(ddlSubEntity1Popup.SelectedValue));                    
                }
            }
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3Popup.Items.Count > 0)
                ddlSubEntity3Popup.Items.Clear();

            if (ddlLocationPopup.Items.Count > 0)
                ddlLocationPopup.Items.Clear();

            if (!string.IsNullOrEmpty(ddlSubEntity2Popup.SelectedValue))
            {
                if (ddlSubEntity2Popup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3Popup, Convert.ToInt32(ddlSubEntity2Popup.SelectedValue));                    
                }
            }
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3Popup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLocationPopup.Items.Count > 0)
                ddlLocationPopup.Items.Clear();

            if (!string.IsNullOrEmpty(ddlSubEntity3Popup.SelectedValue))
            {
                if (ddlSubEntity3Popup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlLocationPopup, Convert.ToInt32(ddlSubEntity3Popup.SelectedValue));                                   
                }
            }

            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void ddlLocationPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }

        protected void grdReassign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlGridReAssignUser = (e.Row.FindControl("ddlGridReAssignUser") as DropDownList);

                    if (ddlGridReAssignUser != null)
                    {
                        BindUserList(CustomerId, ddlGridReAssignUser);

                        if (ddlUser.SelectedValue != "-1" || ddlUser.SelectedValue != null || ddlUser.SelectedValue != "")
                            ddlREAssignUser.Items.Remove(ddlREAssignUser.Items.FindByValue(ddlUser.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbtEventReAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessPopup();
            BindReAssingGrid();
        }

        protected void lBPreviousReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNoReAssign.Text) > 1)
                {
                    SelectedPageNoReAssign.Text = (Convert.ToInt32(SelectedPageNoReAssign.Text) - 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindReAssingGrid();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lBNextReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo < GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo + 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummaryReAssign()
        {
            try
            {
                lTotalCountReAssign.Text = GetTotalPagesCountReAssign().ToString();

                if (lTotalCountReAssign.Text != "0")
                {
                    if (SelectedPageNoReAssign.Text == "")
                        SelectedPageNoReAssign.Text = "1";

                    if (SelectedPageNoReAssign.Text == "0")
                        SelectedPageNoReAssign.Text = "1";
                }
                else if (lTotalCountReAssign.Text == "0")
                {
                    SelectedPageNoReAssign.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCountReAssign()
        {
            try
            {
                TotalRowsReAssign.Value = Session["TotalRowsReAssging"].ToString();

                int totalPages = Convert.ToInt32(TotalRowsReAssign.Value) / Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRowsReAssign.Value) % Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        private void SaveCheckedValues()
        {
            try
            {

                List<RiskAssignmentProperties> AuditriskList = new List<RiskAssignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    RiskAssignmentProperties AuditriskProperties = new RiskAssignmentProperties();
                    CheckBox cheassin = (CheckBox) gvrow.FindControl("chkAssign");
                    AuditriskProperties.Riskcreationid = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    AuditriskProperties.Performer = cheassin.Checked; //((CheckBox) gvrow.FindControl("chkAssign")).Checked;
                    AuditriskProperties.CustomerBranchID =Convert.ToInt32(((Label) gvrow.FindControl("lblCustomerBranchId")).Text);
                    AuditriskProperties.ProcessId = Convert.ToInt32(((Label) gvrow.FindControl("lblProcessId")).Text);
                    AuditriskProperties.SubProcessId = Convert.ToInt32(((Label) gvrow.FindControl("lblSubProcessId")).Text);
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        AuditriskList = ViewState["CHECKED_ITEMS"] as List<RiskAssignmentProperties>;

                    if (AuditriskProperties.Performer && cheassin.Visible==true)
                    {
                        RiskAssignmentProperties rmdata = AuditriskList.Where(t => t.Riskcreationid == AuditriskProperties.Riskcreationid).FirstOrDefault();
                        if (rmdata != null)
                        {
                            AuditriskList.Remove(rmdata);
                            AuditriskList.Add(AuditriskProperties);
                        }
                        else
                        {
                            AuditriskList.Add(AuditriskProperties);
                        }
                    }
                    else
                    {
                        RiskAssignmentProperties rmdata = AuditriskList.Where(t => t.Riskcreationid == AuditriskProperties.Riskcreationid).FirstOrDefault();
                        if (rmdata != null)
                            AuditriskList.Remove(rmdata);
                    }

                }
                if (AuditriskList != null && AuditriskList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = AuditriskList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
     
        private void PopulateCheckedValues()
        {
            try
            {
                List<RiskAssignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<RiskAssignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        RiskAssignmentProperties rmdata = complianceList.Where(t => t.Riskcreationid == index).FirstOrDefault();
                        if (rmdata != null)
                        {                            
                            CheckBox chkPerformer = (CheckBox) gvrow.FindControl("chkAssign");                           
                            chkPerformer.Checked = rmdata.Performer;
                            Label lblCustomerBranchId = (Label) gvrow.FindControl("lblCustomerBranchId");
                            Label lblProcessId = (Label) gvrow.FindControl("lblProcessId");
                            Label lblSubProcessId = (Label) gvrow.FindControl("lblSubProcessId");
                            int CustomerBranchId = Convert.ToInt32(lblCustomerBranchId.Text);
                            CustomerBranchId = rmdata.CustomerBranchid;
                            long ProcessId = Convert.ToInt32(lblProcessId.Text);
                            ProcessId = rmdata.ProcessId;
                            long SubProcessId = Convert.ToInt32(lblSubProcessId.Text);
                            SubProcessId = rmdata.SubProcessId;                          
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {                    
                    Label lblAssigned = (Label)e.Row.FindControl("lblAssigned");
                    CheckBox chkworking = (CheckBox)e.Row.FindControl("chkAssign");
                    if (lblAssigned.Text == "NA")
                    {
                        chkworking.Visible = true;
                    }
                    else if (lblAssigned.Text == "A")
                    {
                        chkworking.Visible = false;
                    }                    
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();                
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
                BindComplianceMatrix(linkclick);
                PopulateCheckedValues();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        

        protected void grdReassignNew__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox headerchk = (CheckBox)grdReassign.HeaderRow.FindControl("chkReAssignSelectAll");
                    CheckBox childchk = (CheckBox)e.Row.FindControl("chkReAssign");
                    childchk.Attributes.Add("onclick", "javascript:SelectchildcheckboxesPopup('" + headerchk.ClientID + "')");                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void btnReAssign_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                lblReAssign.Text = string.Empty;
                ViewState["ReAssignID"] = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID; //userID;

                User user = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                lblReAssign.Text = user.FirstName + " " + user.LastName;

                BindUserList(CustomerId, ddlUser);

                ReAssign.Update();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenModel", "javascript:openModal()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnReassignAuditSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int selectedRowCount = 0;
                string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    
                    if (ddlUser.SelectedValue != "-1" && ddlUser.SelectedValue != "" && ddlRole.SelectedValue != "-1" && ddlRole.SelectedValue != "")
                    {
                        if (ddlREAssignUser.SelectedValue != "-1" && ddlREAssignUser.SelectedValue != "")
                        {
                            string action = Convert.ToString(rbtEventReAssignment.SelectedValue);

                            if (action.Equals("ICFR"))
                            {
                                for (int i = 0; i < grdReassign.Rows.Count; i++)
                                {
                                    CheckBox chkReAssign = (CheckBox)grdReassign.Rows[i].FindControl("chkReAssign");

                                    if (chkReAssign != null)
                                    {
                                        if (chkReAssign.Checked)
                                        {
                                            selectedRowCount++;
                                        }
                                    }
                                }


                                if (selectedRowCount > 0)
                                    saveSuccess = SaveReAssignControlTestingDetails();
                                else
                                {
                                    cvReAssignAudit.IsValid = false;
                                    cvReAssignAudit.ErrorMessage = "Please Select Control to Assign.";
                                }
                            }
                        }
                        else
                        {
                            cvReAssignAudit.IsValid = false;
                            cvReAssignAudit.ErrorMessage = "Please Select New User to Assign.";
                        }
                    }
                    else
                    {
                        cvReAssignAudit.IsValid = false;
                        cvReAssignAudit.ErrorMessage = "Please Select User and Role.";
                    }

                    //ReAssignCheckBoxValueSaved();
                    //List<Tuple<int, int>> chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];
                    
                    if (saveSuccess)
                    {
                        BindReAssingGrid();
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divReAssign\").dialog('close')", true);
                        ViewState["ReAssignedEventID"] = null;

                        cvReAssignAudit.IsValid = false;
                        cvReAssignAudit.ErrorMessage = "Control Re-Assign Details Save Successfully.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public bool SaveReAssignControlTestingDetails()
        {
            try
            {
                bool success = false;

                if (!String.IsNullOrEmpty(ddlREAssignUser.SelectedValue) && !String.IsNullOrEmpty(ddlRole.SelectedValue))
                {
                    if (ddlREAssignUser.SelectedValue != "-1" && ddlRole.SelectedValue != "-1")
                    {
                        for (int i = 0; i < grdReassign.Rows.Count; i++)
                        {
                            CheckBox chkReAssign = (CheckBox)grdReassign.Rows[i].FindControl("chkReAssign");

                            if (chkReAssign != null)
                            {
                                if (chkReAssign.Checked)
                                {
                                    Label lblRiskCategoryCreationId = (Label)grdReassign.Rows[i].FindControl("lblRiskCategoryCreationId");
                                    Label lblProcessId = (Label)grdReassign.Rows[i].FindControl("lblProcessId");
                                    Label lblSubProcessId = (Label)grdReassign.Rows[i].FindControl("lblSubProcessId");
                                    Label lblCustomerBranchId = (Label)grdReassign.Rows[i].FindControl("lblCustomerBranchId");
                                    Label lblFY = (Label)grdReassign.Rows[i].FindControl("lblFY");

                                    if (lblRiskCategoryCreationId != null && lblProcessId != null && lblSubProcessId != null && lblCustomerBranchId != null && lblFY != null)
                                    {
                                        if (lblRiskCategoryCreationId.Text != "" && lblProcessId.Text != "" && lblSubProcessId.Text != "" && lblCustomerBranchId.Text != "" && lblFY.Text != "")
                                        {
                                            success = UserManagementRisk.ReAssignControlTestingAssignment(Convert.ToInt32(lblRiskCategoryCreationId.Text), Convert.ToInt32(ddlUser.SelectedValue),
                                               Convert.ToInt32(ddlREAssignUser.SelectedValue), ddlREAssignUser.SelectedItem.Text, Convert.ToInt32(ddlRole.SelectedValue), Convert.ToInt32(lblCustomerBranchId.Text),
                                               Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblSubProcessId.Text), lblFY.Text);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return success;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private void ReAssignCheckBoxValueSaved()
        {
            List<Tuple<int, int>> chkList = new List<Tuple<int, int>>();
            int ProceID = -1;
            foreach (GridViewRow gvrow in grdReassign.Rows)
            {
                ProceID = Convert.ToInt32(grdReassign.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkEvent")).Checked;
                int BranchID = Convert.ToInt32(((Label)gvrow.FindControl("lblBrachID")).Text);

                //Tuple<int, string>  = new Tuple<int, string>();
                if (ViewState["ReAssignedEventID"] != null)
                    chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];

                var checkedData = chkList.Where(entry => entry.Item1 == ProceID && entry.Item2 == BranchID).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, int>(ProceID, BranchID));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["ReAssignedEventID"] = chkList;
        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }

        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                {
                    countCheckedCheckbox = countCheckedCheckbox + 1;
                }
            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
        }
        public static List<SP_ICFRZPerformerMail_Result> ProcessPerformerSendMailReminder(string todaydate,int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var CheckListReminders = entities.SP_ICFRZPerformerMail(todaydate, userid).ToList(); 
                return CheckListReminders;
            }
        }

        public static mst_User GetByID(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_User
                                where row.ID == userid && row.IsDeleted==false
                                && row.IsActive==true
                                select row).SingleOrDefault();

                return customer;
            }
        }
        
        public static bool Exists(int CustomerBranchID, int riskcreationid, int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (CustomerBranchID != -1 && riskcreationid != -1 && userid != -1)
                {
                    var query = (from row in entities.ICFRPerformerReminderNotifications
                                 where row.RiskCreationID == riskcreationid
                                     && row.CustomerBranchId == CustomerBranchID
                                     && row.UserID == userid
                                 select row);
                    return query.Select(entry => true).SingleOrDefault();
                }
                else
                {
                    return false;
                }
            }
        }
       
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string ForPeriod = "";
                string FinancialYear = "";
                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedValue != "-1")
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                var AuditList = new List<RiskAssignmentProperties>();
                SaveCheckedValues();
                AuditList = ViewState["CHECKED_ITEMS"] as List<RiskAssignmentProperties>;                
                if (AuditList != null)
                {

                    //Quarterlist.Clear();
                    if (!String.IsNullOrEmpty(ddlQuarter.SelectedValue))
                    {
                        if (ddlQuarter.SelectedItem.Text != "-1")
                        {
                            ForPeriod = ddlQuarter.SelectedItem.Text.Trim();
                        }
                    }
                    if(!string.IsNullOrEmpty(ForPeriod))
                    {
                        bool successNew = false;
                        for (int i = 0; i < AuditList.Count; i++)
                        {
                            List<AuditAssignment> Tempassignments = new List<AuditAssignment>();
                            bool success = false;
                            success = ProcessManagement.Exists(AuditList[i].Riskcreationid, AuditList[i].CustomerBranchid, AuditList[i].ProcessID, AuditList[i].SubProcessID, FinancialYear, ForPeriod);
                            if (success != true)
                            {
                                AuditInstance riskinstance = new AuditInstance();
                                riskinstance.RiskCreationId = AuditList[i].Riskcreationid;
                                riskinstance.CustomerBranchID = AuditList[i].CustomerBranchid;
                                riskinstance.IsDeleted = false;
                                riskinstance.CreatedOn = DateTime.Now;
                                riskinstance.ProcessId = AuditList[i].ProcessID;
                                riskinstance.SubProcessId = AuditList[i].SubProcessID;

                                UserManagementRisk.AddDetailsRiskInstance(riskinstance);
                                if (ddlFilterPerformer.SelectedValue != null)
                                {
                                    AuditAssignment TempAssP = new AuditAssignment();
                                    TempAssP.AuditInstanceId = riskinstance.ID;
                                    TempAssP.RiskCreationId = AuditList[i].Riskcreationid;
                                    TempAssP.CustomerBranchID = AuditList[i].CustomerBranchid;
                                    TempAssP.RoleID = 3;//RoleManagement.GetByCode("PERF").ID;
                                    TempAssP.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                                    TempAssP.IsActive = true;
                                    TempAssP.CreatedOn = DateTime.Now;
                                    TempAssP.ProcessId = AuditList[i].ProcessID;
                                    TempAssP.SubProcessId = AuditList[i].SubProcessID;
                                    Tempassignments.Add(TempAssP);
                                }
                                if (ddlFilterReviewer.SelectedValue != null)
                                {
                                    AuditAssignment TempAssR = new AuditAssignment();
                                    TempAssR.AuditInstanceId = riskinstance.ID;
                                    TempAssR.RiskCreationId = AuditList[i].Riskcreationid;
                                    TempAssR.CustomerBranchID = AuditList[i].CustomerBranchid;
                                    TempAssR.RoleID = 4; //RoleManagement.GetByCode("RVW1").ID;
                                    TempAssR.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                                    TempAssR.IsActive = true;
                                    TempAssR.CreatedOn = DateTime.Now;
                                    TempAssR.ProcessId = AuditList[i].ProcessID;
                                    TempAssR.SubProcessId = AuditList[i].SubProcessID;
                                    Tempassignments.Add(TempAssR);
                                }
                                if (Tempassignments.Count != 0)
                                {
                                    UserManagementRisk.AddDetailsTempAssignmentTable(Tempassignments);
                                }
                                successNew = UserManagementRisk.CreateScheduleOn(ForPeriod, riskinstance.ID, AuditList[i].Riskcreationid, Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.User, FinancialYear, AuditList[i].CustomerBranchid);
                            }
                        }
                        if (successNew)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Save Successfully.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Quarter.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at list One Control No.";
                }
                ViewState["CHECKED_ITEMS"] = null;                          
                if (!String.IsNullOrEmpty(ddlFilterPerformer.SelectedValue))
                {
                    if (ddlFilterPerformer.SelectedValue != "-1")
                    {                       
                        BindComplianceMatrix(linkclick);
                        bindPageNumber();
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdComplianceRoleMatrix.PageIndex;
                            string chkcindition = (gridindex + 1).ToString();
                            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                        }
                    }
                }                                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                if (string.IsNullOrEmpty(linkclick))
                {
                    linkclick = "Not Assigned";
                    btnNotAssigned_Click(sender, e);
                }
                else
                {
                    BindComplianceMatrix(linkclick);
                    bindPageNumber();
                }

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceRoleMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

       
        protected string ExportFileName()
        {
            string fileName = string.Empty;

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    fileName = ddlLegalEntity.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    fileName = ddlSubEntity1.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    fileName = ddlSubEntity2.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    fileName = ddlSubEntity3.SelectedItem.Text;
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    fileName = ddlFilterLocation.SelectedItem.Text;
                }
            }

            return fileName;
        }

        protected void btnUploadFile1_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    string locationName = string.Empty;
                    string fileName = string.Empty;
                    string customerName = string.Empty;

                    //BindComplianceMatrixExport();
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ControlTestingAssignmentDetails");
                    DataTable ExcelData = null;
                    //DataView view = new System.Data.DataView(GetGrid());                       
                    DataView view = new System.Data.DataView((DataTable) Session["grdControlTestingAssignment"]);
                    if (view.Table.Rows.Count > 0)
                    {
                        ExcelData = view.ToTable("Selected", false, "Branch", "ControlNo", "ActivityDescription", "ControlObjective", "ControlDescription","FinancialYear", "ForMonth", "RiskCategoryCreationId", "ProcessId", "SubProcessId", "CustomerBranchId");
                        ExcelData.Columns.Add("Performer");
                        ExcelData.Columns.Add("Reviewer");
                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["Performer"] = GetPerformer(Convert.ToInt32(item["RiskCategoryCreationId"].ToString()), Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["SubProcessId"].ToString()), Convert.ToInt32(item["CustomerBranchId"].ToString()));
                            item["Reviewer"] = GetReviewer(Convert.ToInt32(item["RiskCategoryCreationId"].ToString()), Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["SubProcessId"].ToString()), Convert.ToInt32(item["CustomerBranchId"].ToString()));
                        }
                        ExcelData.Columns.Remove("RiskCategoryCreationId");
                        ExcelData.Columns.Remove("ProcessId");
                        ExcelData.Columns.Remove("SubProcessId");
                        ExcelData.Columns.Remove("CustomerBranchId");

                        locationName = ExportFileName();

                        if (locationName == "")
                        {
                            locationName = "All";
                            fileName = "ControlTestingAssignment-All";
                        }
                        else
                        {
                            fileName = "ControlTestingAssignment-" + locationName;

                            if (fileName.Length > 250)
                                fileName = "ControlTestingAssignment";
                        }

                        customerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Company:";
                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = customerName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Branch:";
                        exWorkSheet.Cells["B2:C2"].Merge = true;
                        exWorkSheet.Cells["B2"].Value = locationName;

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report:";
                        exWorkSheet.Cells["B3:C3"].Merge = true;
                        exWorkSheet.Cells["B3"].Value = "Control Testing Assignment Details";

                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Value = "Report Generated On:";
                        exWorkSheet.Cells["B4:C4"].Merge = true;
                        exWorkSheet.Cells["B4"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data into Excel
                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, true);

                        //exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A6"].Value = "Branch";
                        exWorkSheet.Cells["A6"].AutoFitColumns(20);

                        exWorkSheet.Cells["B6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B6"].Value = "Control#";
                        exWorkSheet.Cells["B6"].AutoFitColumns(15);

                        exWorkSheet.Cells["C6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C6"].Value = "Risk Description";
                        exWorkSheet.Cells["C6"].AutoFitColumns(40);

                        exWorkSheet.Cells["D6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D6"].Value = "Control Objective";
                        exWorkSheet.Cells["D6"].AutoFitColumns(40);

                        exWorkSheet.Cells["E6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E6"].Value = "Control Description";
                        exWorkSheet.Cells["E6"].AutoFitColumns(40);
                      
                        exWorkSheet.Cells["F6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F6"].Value = "FinancialYear";
                        exWorkSheet.Cells["F6"].AutoFitColumns(20);

                        exWorkSheet.Cells["G6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G6"].Value = "ForMonth";
                        exWorkSheet.Cells["G6"].AutoFitColumns(20);

                        exWorkSheet.Cells["H6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H6"].Value = "Performer";
                        exWorkSheet.Cells["H6"].AutoFitColumns(20);

                        exWorkSheet.Cells["I6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I6"].Value = "Reviewer";
                        exWorkSheet.Cells["I6"].AutoFitColumns(20);


                        using (ExcelRange col = exWorkSheet.Cells[6, 1, 6 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Location.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        protected void ddlQuarterReassing_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSizeReAssign_SelectedIndexChanged(sender, e);
        }
    }
}