﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;


namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditUpload : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindFnancialYear();
                BindComplianceMatrix();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());


                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceRoleMatrix.PageIndex = chkSelectedPage - 1;

            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindComplianceMatrix();
        }
        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);                
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindProcess(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindProcess(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindProcess(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindProcess(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindProcess(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }
        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            if (ddlFilterProcess.SelectedValue != null)
            {
                if (ddlFilterProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), Convert.ToInt32(CustomerBranchId));
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        private void BindProcess(long branchid)
        {
            try
            {
                ddlFilterProcess.DataTextField = "Name";
                ddlFilterProcess.DataValueField = "ID";
                ddlFilterProcess.Items.Clear();
                ddlFilterProcess.DataSource = UserManagementRisk.FillCustomerTransactionProcess(branchid);
                ddlFilterProcess.DataBind();
                ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindSubProcess(long ProcessId, long branchid)
        {
            try
            {
                ddlFilterSubProcess.DataTextField = "Name";
                ddlFilterSubProcess.DataValueField = "ID";
                ddlFilterSubProcess.Items.Clear();
                ddlFilterSubProcess.DataSource = UserManagementRisk.FillCustomerTransactionSubProcess(ProcessId, branchid);
                ddlFilterSubProcess.DataBind();
                ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindComplianceMatrix()
        {
            try
            {
                long pid = -1;
                long psid = -1;
                int CustomerBranchId = -1;
                if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        pid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        psid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();
                var AllComplianceRoleMatrix = ProcessManagement.GetAuditGridDisplay(customerID, Branchlist.ToList(), Convert.ToInt32(pid), Convert.ToInt32(psid));
                grdComplianceRoleMatrix.DataSource = AllComplianceRoleMatrix;
                Session["TotalRows"] = AllComplianceRoleMatrix.Count;
                grdComplianceRoleMatrix.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }


        public string ShowSampleConditionDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "View";
            }
            else
            {
                processnonprocess = "";
            }
            return processnonprocess;
        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string confirmValue = Request.Form["confirm_value"];
                string flag = "false";
                if (confirmValue == "Yes")
                {
                    flag = "true";
                }
                else
                {
                    Button btn1 = (Button)sender;
                    GridViewRow gvr1 = (GridViewRow)btn1.NamingContainer;
                    LinkButton DownloadFile1 = (LinkButton)gvr1.FindControl("lblDownLoadfile");
                    string fileExistChk1 = DownloadFile1.Text.ToString();
                    if (fileExistChk1 == "File Not Uploded")
                    {
                        flag = "true";
                    }
                }
                if (flag == "true")
                {
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    Button btn = (Button)sender;
                    //Get the row that contains this button
                    GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                    FileUpload file = (FileUpload)gvr.FindControl("FileUpload1");
                    LinkButton DownloadFile = (LinkButton)gvr.FindControl("lblDownLoadfile");
                    string fileExistChk = DownloadFile.Text.ToString();
                    Label lblCustomerBranchId = (Label)gvr.FindControl("lblCustomerBranchId");
                    Label lblRiskCategoryCreationId = (Label)gvr.FindControl("lblRiskCategoryCreationId");
                    Label ProcessID = (Label)gvr.FindControl("lblProcessID");
                    Label SubProcessID = (Label)gvr.FindControl("lblSubProcessId");

                    Label FinancialYeartet = (Label)gvr.FindControl("lblFinancialYear");

                    if (file != null)
                    {
                        if (file.HasFile)
                        {
                            long pid = -1;
                            long psid = -1;
                            int CustomerBranchId = -1;
                            string FinancialYear = "";
                            pid = Convert.ToInt32(ProcessID.Text);
                            psid = Convert.ToInt32(SubProcessID.Text);
                            CustomerBranchId = Convert.ToInt32(lblCustomerBranchId.Text);
                            if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedItem.Text))
                            {
                                FinancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                            }
                            string directoryPath = "";
                            if (pid != -1 && psid != -1 && CustomerBranchId != -1)
                            {
                                directoryPath = Server.MapPath("~/AuditSampleDocument/" + lblRiskCategoryCreationId.Text + "/" + CustomerBranchId.ToString() + "/" + FinancialYear.ToString() + "/" + pid.ToString() + "/" + psid.ToString());
                            }
                            if (pid != -1 && CustomerBranchId != -1)
                            {
                                directoryPath = Server.MapPath("~/AuditSampleDocument/" + lblRiskCategoryCreationId.Text + "/" + CustomerBranchId.ToString() + "/" + FinancialYear.ToString() + "/" + pid.ToString());
                            }
                            if (directoryPath != "")
                            {
                                DocumentManagement.CreateDirectory(directoryPath);

                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(file.FileName));

                                Stream fs = file.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (file.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        AuditSampleForm file1 = new AuditSampleForm()
                                        {
                                            Name = file.PostedFile.FileName,
                                            FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey.ToString(),
                                            ProcessID = pid,
                                            SubProcessId = psid,
                                            RiskCreationID = Convert.ToInt32(lblRiskCategoryCreationId.Text),
                                            CustomerBranchId = CustomerBranchId,
                                            FinancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text),
                                            IsDeleted = false,
                                            FileData = file.FileBytes,
                                            EnType = "A",
                                        };
                                        RiskCategoryManagement.CreateAuditSampleForm(file1);
                                    }
                                    SaveDocFiles(Filelist);
                                    BindComplianceMatrix();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindComplianceMatrix();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<AuditSampleForm> GetFileData1(int RiskCreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.AuditSampleForms
                                where row.RiskCreationID == RiskCreationId
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblRiskCategoryCreationId = (Label)gvr.FindControl("lblRiskCategoryCreationId");
                List<AuditSampleForm> fileData = GetFileData1(Convert.ToInt32(lblRiskCategoryCreationId.Text));
                if (fileData.Count > 0)
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        int i = 1;
                        //string directoryName ="abc";
                        string directoryName;
                        string version = "1";
                        foreach (var file in fileData)
                        {
                            directoryName = file.FinancialYear;
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string[] filename = file.Name.Split('.');
                                //string str = filename[0] + i + "." + filename[1];
                                string str = filename[0] + "." + filename[1];
                                if (file.EnType == "M")
                                {
                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                i++;
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=AuditSampleForm.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "There is no file for Download.";
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void ViewDocument(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblRiskCategoryCreationId = (Label)gvr.FindControl("lblRiskCategoryCreationId");
            List<AuditSampleForm> fileData = GetFileData1(Convert.ToInt32(lblRiskCategoryCreationId.Text));

            if (fileData.Count > 0)
            {
                foreach (var file in fileData)
                {
                    //string sampleFormPath = file.FilePath;
                    // sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2)+"/"+ file.Name;

                    // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestView('" + sampleFormPath + "');", true);
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                    if (file.FilePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + FileData;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (file.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();

                        string CompDocReviewPath = FileName;
                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                        // ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestView('" + CompDocReviewPath + "');", true);
                    }
                    else
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                    }
                    break;
                }
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "There is no file for View.";
            }
        }
        protected void grdComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //Reload the Grid
                BindComplianceMatrix();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceRoleMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

    }
}