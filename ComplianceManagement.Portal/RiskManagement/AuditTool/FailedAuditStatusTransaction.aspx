﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FailedAuditStatusTransaction.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.FailedAuditStatusTransaction" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="FortuneCookie - Development Team">
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="../../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../NewCSS/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="../../NewCSS/fullcalendar.css" type="text/css" />
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="../../Newjs/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bs_leftnavi.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bs_leftnavi.js"></script>
   
    <title></title>
    <script type="text/javascript">
        $.noConflict();
        function initializeDatePickerforPerformer(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
        function initializeDatePickerforPerformer1(date1) {
            var startDate1 = new Date();
            $('#<%= txtDateResp.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate1,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            if (date1 != null) {
                $("#<%= txtDateResp.ClientID %>").datepicker("option", "defaultDate", date1);
            }
        }

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        };

        function CloseWindow() {

            window.parent.CloseWin3();
        };


        //var validFilesTypes = ["exe", "bat", "zip", "rar", "dll"];
        var validFilesTypes = ["exe", "bat", "dll"];
        function ValidateFile() {

            var label = document.getElementById("<%=Label1.ClientID%>");
        var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
        var FileuploadAdditionalFile = $("#<%=FileuploadAdditionalFile.ClientID%>").get(0).files;
        var isValidFile = true;

        for (var i = 0; i < fuSampleFile.length; i++) {
            var fileExtension = fuSampleFile[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }


        for (var i = 0; i < FileuploadAdditionalFile.length; i++) {
            var fileExtension = FileuploadAdditionalFile[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }

        if (!isValidFile) {
            label.style.color = "red";
            //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
            label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
        }
        return isValidFile;
    }

    </script>

    <style type="text/css">
        .tdAudit1 {
            width: 20%;
        }

        .tdAudit2 {
            width: 40%;
        }

        .tdAudit3 {
            width: 16%;
        }

        .tdAudit4 {
            width: 24%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManage" runat="server"></asp:ScriptManager>
         <div class="clearfix"></div>
        <div class="col-md-12 colpadding0">
                <div runat="server" id="LblPageDetails" style="color: #666"></div>
            </div>
            <div class="clearfix"></div>
        <div id="divComplianceDetailsDialog" style="background-color: #eee;">
            <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                </div>
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails_Load">
                <ContentTemplate>
                    <div style="margin-bottom: 4px">
                        <%--<asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />--%>
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdnAuditInstanceID" />
                        <asp:HiddenField runat="server" ID="hdnRiskCreatinID" />
                        <asp:HiddenField runat="server" ID="hdnAuditScheduledOnId" />
                    </div>

                    <div id="TOD" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                <h2>Test of Design (TOD)</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapseActDetails" class="panel-collapse collapse in">
                                        <fieldset>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 185px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Documents Examined</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:TextBox ID="txtDocumentExamined" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 50px; width: 297px;"></asp:TextBox></td>
                                                    <td class="tdAudit3">
                                                        <label style="width: 185px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Reason for Pass or Fail</label>
                                                    </td>
                                                    <td class="tdAudit4">
                                                        <asp:TextBox ID="txtReasonforPassorFail" runat="server" TextMode="MultiLine" CssClass="form-control" Style="height: 50px; width: 290px;"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">*</label>
                                                        <label style="width: 170px; display: block; float: left; font-size: 13px; color: #333;">
                                                            TOD (Pass or Fail)</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:DropDownList ID="ddlDesign" runat="server" AutoPostBack="true" class="form-control m-bot15" OnSelectedIndexChanged="ddlDesign_SelectedIndexChanged" Width="297px">
                                                            <asp:ListItem Value="-1">Select TOD</asp:ListItem>
                                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                                            <asp:ListItem Value="3">Pass With Mitigating Control</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RFVPerRes" ErrorMessage="Please select Test of Design (TOD)."
                                                            ControlToValidate="ddlDesign" InitialValue="-1"
                                                            runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />

                                                    </td>
                                                    <td class="tdAudit3"></td>
                                                    <td class="tdAudit4">
                                                        <asp:LinkButton ID="lbDownloadSample" Style="width: 290px; font-size: 13px; color: #333;"
                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                        <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload."
                                                            ControlToValidate="fuSampleFile"
                                                            runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup"
                                                            Display="None" Enabled="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div runat="server" id="divDeleteDocument" style="text-align: left;">
                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd;">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                    OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Compliance Related Documents</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton
                                                                    CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                </asp:LinkButton></td>
                                                            <td>
                                                                <asp:LinkButton
                                                                    CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                    OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                    ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                </asp:LinkButton></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>

                    <div id="TOE" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: -1px; background-color: #f7f7f7;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1">
                                                <h2>Test of Effectiveness (TOE) </h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapseActDetails1" class="panel-collapse collapse in">
                                        <fieldset>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 7px; display: block; float: left; font-size: 13px; color: Red;">*</label>
                                                        <label style="width: 170px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Upload Testing Document(s)</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" />
                                                        <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile"
                                                            runat="server" ID="fileuploadValidator" ValidationGroup="ComplianceValidationGroup" Display="None" Enabled="false" />
                                                    </td>
                                                    <td class="tdAudit3">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Sample Tested</label>
                                                    </td>
                                                    <td class="tdAudit4">
                                                        <asp:TextBox ID="txtSampleTested" runat="server" CssClass="form-control" Style="width: 250px;"
                                                            onkeypress="return allowOnlyNumber(event);" onpaste="return false;"></asp:TextBox>

                                                        <asp:CompareValidator ID="cvRootCost" runat="server" ControlToValidate="txtSampleTested"
                                                            ErrorMessage="Only Numbers in Sample Tested"
                                                            ValidationGroup="ComplianceValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">
                                                            &nbsp;</label>
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            TOE (Pass or Fail)</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:DropDownList ID="ddlOprationEffectiveness" runat="server" AutoPostBack="false" class="form-control m-bot15"
                                                            OnSelectedIndexChanged="ddlOprationEffectiveness_SelectedIndexChanged" Width="297px">
                                                            <asp:ListItem Value="-1">Select TOE</asp:ListItem>
                                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                                            <asp:ListItem Value="3">Pass With Extended Testing</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="tdAudit3">
                                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Deviation %</label></td>
                                                    <td class="tdAudit4">
                                                        <asp:TextBox ID="txtDeviation" runat="server" CssClass="form-control" Style="width: 250px;"
                                                            onkeypress="return allowOnlyNumber(event);" onpaste="return false;"></asp:TextBox>
                                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtDeviation"
                                                            ErrorMessage="Only Numbers in  Deviation %"
                                                            ValidationGroup="ComplianceValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <%--<label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Status</label>--%>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <%-- <asp:DropDownList runat="server" ID="ddlStatus" Width="297px" class="form-control m-bot15" />
                                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Status." ControlToValidate="ddlStatus"
                                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                                            Display="None" />--%>
                                                    </td>
                                                    <td class="tdAudit3">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: Red;">*</label>
                                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Date</label>
                                                    </td>
                                                    <td class="tdAudit4">
                                                        <asp:TextBox runat="server" ID="tbxDate" CssClass="form-control" Style="width: 250px;" />
                                                        <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                            runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Remarks</label>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" CssClass="form-control" Style="height: 60px; width: 750px;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="divpersonresponsible" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#testReviewcollopse">
                                                <h2>Responsible Person</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#testReviewcollopse"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="testReviewcollopse" class="panel-collapse collapse in">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="tdAudit1" style="vertical-align: top;">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Person Responsible</label></td>
                                                    <td class="tdAudit2">
                                                        <asp:DropDownList ID="ddlPersonResponsible" CssClass="form-control m-bot15" runat="server" AutoPostBack="true"
                                                            Width="297px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="tdAudit3">
                                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Date</label>
                                                    </td>
                                                    <td class="tdAudit4">
                                                        <asp:TextBox runat="server" ID="txtDateResp" CssClass="form-control" Style="width: 200px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1" style="vertical-align: top;">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Management Response</label></td>
                                                    <td class="tdAudit2">
                                                        <asp:TextBox runat="server" ID="txtMngntesponce" CssClass="form-control" TextMode="MultiLine" Style="height: 50px; width: 297px;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ReviewHistory" runat="server" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails2">
                                                <h2>Review History</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails2"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseActDetails2" class="panel-collapse collapse in">
                                        <fieldset style="margin-top: 5px;">

                                            <table style="width: 100%">
                                                <tr>
                                                    <td colspan="4">
                                                        <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                            <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                                AllowPaging="true" GridLines="None" Width="100%" CssClass="table">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="riskID" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Scheduleon" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Created By">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Remarks">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Date">
                                                                        <ItemTemplate>
                                                                            <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="lblDownLoadfile" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>' OnClick="DownLoadClick"></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <PagerTemplate>
                                                                    <table style="display: none">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </PagerTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 185px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Review Remark</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control" Style="height: 50px; width: 290px;"></asp:TextBox>
                                                        <%--               <asp:RequiredFieldValidator ErrorMessage="Remark Required" ControlToValidate="txtRemark" ForeColor="Red"
                                                            runat="server" ID="RequiredFieldValidator2" ValidationGroup="ComplianceValidationGroup" />--%>
                                                    </td>
                                                    <td class="tdAudit3"></td>
                                                    <td class="tdAudit4">
                                                        <asp:FileUpload ID="FileuploadAdditionalFile" runat="server" /></td>
                                                </tr>
                                            </table>
                                        </fieldset>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 340px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary"
                            OnClientClick="CloseWindow();" data-dismiss="modal" />
                        <%-- OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" --%>
                    </div>
                    <div id="Div1" runat="server" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails22">
                                                <h2>Logs</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails22"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapseActDetails22" class="panel-collapse collapse in">
                                        <fieldset style="margin-top: 5px;">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                    AllowPaging="true" PageSize="5" GridLines="None" Width="100%" CssClass="table" OnPageIndexChanging="grdTransactionHistory_PageIndexChanging" OnRowCreated="grdTransactionHistory_RowCreated"
                                                    OnSorting="grdTransactionHistory_Sorting" DataKeyNames="AuditTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--<asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />--%>
                                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                                    </Columns>

                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                    <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
        <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
        <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    </form>
</body>
</html>
