﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPreRequisite.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AddPreRequisite" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/fullcalendar.css" type="text/css" />
    <%--<link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .MarginTop {
            margin-top: 15px;
        }
    </style>
    <script type="text/javascript">
        function ShowADDDOCDialog(AuditID, ATBDID) {
            $('#divCheckListAddDocument').modal('show');
        };

        function ShowEditDOCDialog(AuditID, ATBDID) {
            $('#divCheckListEditDocument').modal('show');
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upPR">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <asp:UpdatePanel ID="upPR" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                    <div id="ActDetails" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 12%">
                                            <div class="col-md-2 colpadding0" style="margin-right: 24px;">
                                                <p style="color: #999; margin-top: 5px; margin-right: 5px;">Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; height: 32px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Text="5" />
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" Selected="True" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 5px">
                                            <asp:DropDownCheckBoxes ID="ddlProcess" runat="server" AutoPostBack="false" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 5px">
                                            <asp:DropDownCheckBoxes ID="ddlSubProcess" runat="server" AutoPostBack="false" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                                <Texts SelectBoxCaption="Select SubProcess" />
                                            </asp:DropDownCheckBoxes>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 5px">
                                            <asp:DropDownCheckBoxes ID="ddlDocRequire" runat="server" AutoPostBack="false" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlDocRequire_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop" />
                                                <Texts SelectBoxCaption="Select Document Required" />
                                                <Items>
                                                    <asp:ListItem Text="Available" Value="1" />
                                                    <asp:ListItem Text="Not Available" Value="2" Selected="True"/>
                                                </Items>
                                            </asp:DropDownCheckBoxes>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; margin-right: 5px">
                                            <asp:TextBox runat="server" ID="tbxTypeToSearch" AutoComplete="Off" OnTextChanged="tbxTypeToSearch_TextChanged"
                                                AutoPostBack="true" class="form-control" Style="width: 90%;"
                                                placeholder="Type to Search" />
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <asp:GridView runat="server" ID="grdProcessAudit" AutoGenerateColumns="false" GridLines="None" PageSize="20"
                                            AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                            ShowHeaderWhenEmpty="true" OnRowDataBound="grdProcessAudit_RowDataBound" OnRowCommand="grdProcessAudit_RowCommand">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Process">
                                                    <ItemTemplate>
                                                        <div class="text_NlinesusingCSS" style="width: 200px;">
                                                            <asp:Label runat="server" ID="lblProcess" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sub-Process">
                                                    <ItemTemplate>
                                                        <div class="text_NlinesusingCSS" style="width: 200px;">
                                                            <asp:Label runat="server" ID="lblSubProcess" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AuditID" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                            <asp:Label runat="server" ID="lblAuditID" Style="display: none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                                            <asp:Label runat="server" ID="lblatbdID" Style="display: none;" Text='<%#Eval("ATBDID") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Audit Step">
                                                    <ItemTemplate>
                                                        <div class="text_NlinesusingCSS" style="width: 200px;">
                                                            <asp:Label runat="server" ID="lblActivityTobeDone" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditStep") %>' ToolTip='<%# Eval("AuditStep") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Document" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkAddChecklist" runat="server" CommandName="Add_CheckList"
                                                            CommandArgument='<%# Eval("AuditID") +"," + Eval("ATBDID")  %>'>                                                           
                                                                    <img src="../../Images/add_icon_new.png" data-toggle="tooltip" data-placement="top"  title="Add Document" /></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Count" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <div class="text_NlinesusingCSS" style="width: 40px;">
                                                            <asp:LinkButton ID="lnkEditChecklist" runat="server" CommandName="Edit_CheckList" Text='<%# Eval("DocumentAvailable") %>'
                                                                CommandArgument='<%# Eval("AuditID") +"," + Eval("ATBDID") %>'>                                                           
                                                                    </asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <HeaderStyle BackColor="#ECF0F1" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                                   
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 60%">
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; display: none;">
                                            <asp:Button Text="Save" runat="server" ID="btnSave"
                                                ValidationGroup="ComplianceValidationGroup1" CssClass="btn btn-primary" />
                                            <%--OnClick="btnSave_Click"--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 31%">
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 4%">
                                            <p style="color: #999; margin-top: 5px; margin-right: 5px;">Page </p>
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 5%">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                                class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="modal fade" id="divCheckListEditDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="upEmditChecklist" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-md-12 colpadding0">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="ComplianceValidationGroup1" />
                                        <asp:CustomValidator ID="cvChecklist" runat="server" EnableClientScript="False"
                                            ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                                        <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12 colpadding0">
                                            <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                            <div class="col-md-2 colpadding0" style="margin-right: 24px;">
                                                <p style="color: #999; margin-top: 5px; margin-right: 5px;">Show </p>
                                            </div>
                                            <asp:DropDownListChosen runat="server" ID="ddlPageSizeView" class="form-control m-bot15" Style="width: 70px; height: 32px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSizeView_SelectedIndexChanged">
                                                <asp:ListItem Text="5" Selected="True"/>
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" />
                                            </asp:DropDownListChosen>
                                        </div>
                                            <asp:GridView runat="server" ID="GridViewChecklist" AutoGenerateColumns="false" GridLines="None" PageSize="5"
                                                AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" OnRowDataBound="GridViewChecklist_RowDataBound"
                                                ShowHeaderWhenEmpty="true">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document Required">
                                                        <ItemTemplate>
                                                            <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                <asp:textbox runat="server" ID="tbxDocumentName" CssClass="form-control" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ChecklistDocument") %>'></asp:textbox>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document Required" Visible="false">
                                                        <ItemTemplate>
                                                            <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                <asp:Label runat="server" ID="lblID" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ID") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <HeaderStyle BackColor="#ECF0F1" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <asp:Button runat="server" ID="btnUpdateCheckList" CssClass="btn btn-primary" Text="Save" AutoPostback="false" OnClick="btnUpdateCheckList_Click" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divCheckListAddDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="upAddChecklist" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-md-12 colpadding0">
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="ComplianceValidationGroup1" />
                                        <asp:CustomValidator ID="CvCheckListAdd" runat="server" EnableClientScript="False"
                                            ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                                        <asp:Label ID="Label2" runat="server" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 25%;display:none;">
                                            <div class="col-md-2 colpadding0" style="margin-right: 24px;">
                                                <p style="color: #999; margin-top: 5px; margin-right: 5px;">Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSizeAdd" class="form-control m-bot15" Style="width: 70px; height: 32px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSizeAdd_SelectedIndexChanged">
                                                <asp:ListItem Text="5" Selected="True"/>
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-12 colpadding0">
                                            <asp:GridView runat="server" ID="GrdAddChecklist" AutoGenerateColumns="false" GridLines="None" PageSize="5"
                                                AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" OnRowCommand="GrdAddChecklist_RowCommand" 
                                                ShowHeaderWhenEmpty="true" OnRowDataBound="GrdAddChecklist_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document Required">
                                                        <ItemTemplate>
                                                            <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                <asp:textbox runat="server" ID="tbxDocumentName" data-toggle="tooltip"  CssClass="form-control" data-placement="top" Text='<%# Eval("ChecklistDocument") %>'></asp:textbox>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action" >
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkAddChk" runat="server" CommandName="Add_CheckList">                                                           
                                                                    <img src="../../Images/add_icon_new.png" data-toggle="tooltip" data-placement="top"  title="Add Document" /></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Delete_CheckList">                                                           
                                                                    <img src="../../Images/delete_icon_new.png" data-toggle="tooltip" data-placement="top"  title="Delete Document" /></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <HeaderStyle BackColor="#ECF0F1" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Record Found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <asp:Button runat="server" ID="btnAddChecklist" CssClass="btn btn-primary" Text="Save" AutoPostback="false" OnClick="btnAddChecklist_Click" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
