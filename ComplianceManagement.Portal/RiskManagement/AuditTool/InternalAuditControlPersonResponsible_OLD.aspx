﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalAuditControlPersonResponsible_OLD.aspx.cs" EnableEventValidation="false" ValidateRequest="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.InternalAuditControlPersonResponsible_OLD" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <!-- bootstrap theme -->
    <link href='<%# ResolveUrl("NewCSS/bootstrap-theme.css")%>' rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%-- <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>--%>
<script type="text/javascript" src="https://cdn.tiny.cloud/1/oipsn3zuqpolvftgsefarew0qb52za3sjm4vm0bjouh8fd9x/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <%--<script src="../../Newjs/tinymce.min.js"></script>
    <script src="../../Newjs/KendoTable.min.js"></script>
    <link href="../../NewCSS/KendoTable.min.css" rel="stylesheet" />
    <link href="../../NewCSS/KendoTableBlueOpal.css" rel="stylesheet" />
    <link href="../../NewCSS/KendoTableCommonMin.css" rel="stylesheet" />
    <link href="../../NewCSS/KendoTableMobile.min.css" rel="stylesheet" />--%>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        label {
            font-weight: 400 !important;
        }

        legend {
            margin-bottom: 2px !important;
            font-size: 19px;
            color: #666 !important;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

            .chosen-container-single .chosen-single span {
                color: #8e8e93;
            }

        .chosen-container {
            margin-bottom: 3px;
        }

        /*added by sagar more on 07-01-2020*/
        .MultilineVideoLinkResizedTextbox {
            width: 940px;
            height: 60px;
            margin-bottom: 10px;
        }
    </style>

    <script type="text/javascript">
        bind_tinyMCE();

        function bind_tinyMCE() {
            tinymce.remove();
            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "myTextEditor",
                width: '100%',
                height: 400,
                plugins: ["table", "code", "insertdatetime media table paste"],
                paste_webkit_styles: "color font-size",
                menu: {
                    table: { title: 'Table', items: 'inserttable' },
                },
                toolbar: "undo redo | bold italic",
                statusbar: false,
                setup: function (editor) {
                    editor.on('change', function () {
                        tinymce.triggerSave();
                    });
                }
            });
        }

        function fopendocfileReview(file) {

            $('#DocumentReviewPopUp1').modal('show');
            $('#IFrameImage').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function userValid() {
            var ObservationTitleval, Observationval, Riskval, Rootcouseval, financialImpactval
            , Recommendationval, Personresponsibleval, RFVObRatingval, RFVObCatval, RFVObSubCatval;
            var MgtResval, TimeLineval;

            MgtResval = document.getElementById('<%=RFVMgtRes.ClientID%>');
            TimeLineval = document.getElementById('<%=RFVTimeLine.ClientID%>');
            Observation = document.getElementById("txtObservation").value;

            if (Observation != "") {

                if (MgtResval != null) {
                    document.getElementById('<%=RFVMgtRes.ClientID%>').enabled = true;
                }
                if (TimeLineval != null) {
                    document.getElementById('<%=RFVTimeLine.ClientID%>').enabled = true;
                }
                return true;
            }
            else {
                if (MgtResval != null) {
                    document.getElementById('<%=RFVMgtRes.ClientID%>').enabled = false;
                }
                if (TimeLineval != null) {
                    document.getElementById('<%=RFVTimeLine.ClientID%>').enabled = false;
                }
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            var MaxLength = 200;
            $('#txtObservationTitle').keypress(function (e) {
                if ($(this).val().length >= MaxLength) {
                    e.preventDefault();
                }
            });
            $('#txtpopulation').keypress(function (e) {
                if ($('#txtpopulation').val().length >= MaxLength) {
                    e.preventDefault();
                }
            });
        });
    </script>
    <script>
        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
        function initializeConfirmDatePicker1(date) {
            var startDate = new Date();
            $('#<%= txtTimeLine.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            $('#<%= txtResponseDueDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
    </script>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        var validFilesTypes = ["exe", "bat", "dll"];
        function ValidateFile() {

            var label = document.getElementById("<%=Label9.ClientID%>");
            var fuSampleFile = $("#<%=FileuploadAdditionalFile.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                label.style.color = "red";
                //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var MaxLength = 200;
            $('#txtObservationTitle').keypress(function (e) {
                if ($(this).val().length >= MaxLength) {
                    e.preventDefault();
                }
            });
        });
        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        };

        function allowOnlyNumberWithDecimal(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        };
    </script>
    <style type="text/css">
        .tdAudit1 {
            width: 20%;
        }

        .tdAudit2 {
            width: 40%;
        }

        .tdAudit3 {
            width: 16%;
        }

        .tdAudit4 {
            width: 24%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>        
        <div class="clearfix"></div>
        <div class="col-md-12 colpadding0">
            <div runat="server" id="LblPageDetails" style="color: #666"></div>
        </div>
        <div class="clearfix"></div>
        <table style="width: 100%; margin-top: 5px;">
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                        ValidationGroup="ComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Label2" runat="server" Visible="false"> </asp:Label>
                    <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Label9" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="position: absolute;" valign="top" runat="server" id="TDTab">
                    <header class="panel-heading tab-bg-primary" style="background: none; !important;">
                        <ul id="rblRole1" class="nav nav-tabs">
                            <li class="" id="liAuditCoverage" runat="server">
                                <asp:LinkButton ID="lnkAuditCoverage" OnClick="Tab1_Click" runat="server">Audit Coverage</asp:LinkButton>
                            </li>

                            <li class="" id="liActualTesting" runat="server">
                                <asp:LinkButton ID="lnkActualTesting" OnClick="Tab2_Click" runat="server">Actual Testing/ Work Done</asp:LinkButton>
                            </li>

                            <li class="active" id="liObservation" runat="server">
                                <asp:LinkButton ID="lnkObservation" OnClick="Tab3_Click" runat="server">Observations</asp:LinkButton>
                            </li>

                            <li class="" id="liReviewLog" runat="server">
                                <asp:LinkButton ID="lnkReviewLog" OnClick="Tab4_Click" runat="server">Review History & Log</asp:LinkButton>
                            </li>
                        </ul>
                    </header>
                    <asp:MultiView ID="MainView" runat="server">
                        <asp:View ID="FirstView" runat="server">
                            <div style="width: 100%; float: left; margin-bottom: 15px">
                                <div style="margin-bottom: 4px; width: auto">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="FirstValidationGroup" />
                                    <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                            ValidationGroup="FirstValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top: 0px;">
                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93; margin-bottom: 10px;">
                                                Audit Methodology</label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtAuditObjective" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 145px; width: 945px;" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 185px; display: block; float: left; font-size: 14px; color: #8e8e93; margin-bottom: 10px;">
                                                Audit Steps</label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtAuditSteps" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 145px; width: 945px;" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Analyis To Be Performed</label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtAnalysisToBePerformed" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 145px; width: 945px;" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="margin-top: 50px;">
                                            <asp:Button Text="Next" runat="server" ID="btnNext1" CssClass="button" OnClick="btnNext1_Click"
                                                ValidationGroup="FirstValidationGroup" Visible="false" />
                                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button"
                                                OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" Visible="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:View>
                        <asp:View ID="SecondView" runat="server">
                            <div style="width: 100%; float: left; margin-bottom: 15px">
                                <div style="margin-bottom: 4px; width: auto">
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="vdsummary" ValidationGroup="FirstValidationGroup" />
                                    <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                        <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label8" runat="server" Text="" ForeColor="red"></asp:Label>
                                        <asp:CustomValidator ID="CustomValidator4" runat="server" EnableClientScript="False"
                                            ValidationGroup="FirstValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                    </div>
                                </div>
                                <table>
                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Process  Walkthrough 
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtWalkthrough" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Actual Work Done 
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtActualWorkDone" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Population 
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtpopulation" MaxLength="200" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                            <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Sample 
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtSample" MaxLength="200" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" CssClass="form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;"></label>
                                            <label style="width: 150px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                Remarks</label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="tbxRemarks" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional"
                                                OnLoad="upComplianceDetails_Load">
                                                <ContentTemplate>

                                                    <asp:GridView runat="server" ID="rptComplianceDocumnets" AutoGenerateColumns="false" AllowSorting="true"
                                                        AllowPaging="true" GridLines="None" BackColor="White" CssClass="table"
                                                        CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%"
                                                        OnRowCommand="rptComplianceDocumnets_RowCommand"
                                                        OnRowDataBound="rptComplianceDocumnets_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Working Document" ItemStyle-Width="90%">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton
                                                                                CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' CommandName="Download"
                                                                                ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                            </asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnComplianceDocumnets" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                    <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr style="height: 20px; margin-top: 20px;">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView runat="server" ID="rptComplianceDocumnetsAnnexure" AutoGenerateColumns="false" AllowSorting="true"
                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                        OnRowCommand="rptComplianceDocumnetsAnnexure_RowCommand"
                                                        OnRowDataBound="rptComplianceDocumnetsAnnexure_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Annexure Document" ItemStyle-Width="90%">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton
                                                                                CommandArgument='<%# Eval("Id")%>' CommandName="DownloadAnnuxture"
                                                                                ID="btnComplianceDocumnetsAnnexure" runat="server" Text='<%# Eval("FileName") %>'>
                                                                            </asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnComplianceDocumnetsAnnexure" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="margin-top: 50px;">
                                            <asp:Button Text="Next" runat="server" ID="btnNext2" CssClass="button" OnClick="btnNext2_Click"
                                                ValidationGroup="FirstValidationGroup" Visible="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:View>

                        <asp:View ID="ThirdView" runat="server">
                            <div style="width: 100%; float: left; margin-bottom: 15px">
                                <div style="margin-bottom: 4px; width: auto">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="None"
                                        class="alert alert-block alert-danger fade in" ValidationGroup="ThirdValidationGroup" />
                                    <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label4" runat="server" Text="" ForeColor="red"></asp:Label>
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                            ValidationGroup="ThirdValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <tr style="display: none;">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation Number
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtObservationNumber" CssClass="form-control" onkeypress="return allowOnlyNumber(event);" onpaste="return false;" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation Title
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtObservationTitle" CssClass="form-control" MaxLength="200" Style="margin-bottom: 10px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation 
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtObservation" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Brief Observation 
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="tbxBriefObservation" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation Background
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="tbxObjBackground" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td></td>
                                                <td>
                                                    <asp:Button Text="Add Table" runat="server" ID="btnAddTable" CssClass="btn btn-primary pull-right" CausesValidation="true"
                                                        AutoPostBack="true" OnClick="btnAddTable_Click" />
                                                </td>
                                            </tr>--%>
                                            <tr style="display: none;">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Working File Upload</label>
                                                </td>
                                                <td>
                                                    <div style="width: 100%;">
                                                        <div style="width: 50%; float: left;">
                                                            <asp:FileUpload ID="FileObjUpload" runat="server" AllowMultiple="true" />
                                                        </div>
                                                        <div style="width: 50%; float: right;">
                                                            <asp:Button ID="btnObjfileupload" runat="server" Text="Upload" CssClass="btn btn-search" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="GrdobservationFile"
                                                                AutoGenerateColumns="false"
                                                                AllowSorting="true" PageSize="10"
                                                                AllowPaging="true" GridLines="none" CssClass="table"
                                                                Font-Size="12px" Width="100%"
                                                                OnRowCommand="rptComplianceDocumnetsObj_RowCommand"
                                                                OnRowDataBound="rptComplianceDocumnets_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Working Document" ItemStyle-Width="90%">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                                        ID="btnComplianceDocumnetsObj" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                    </asp:LinkButton>

                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="btnComplianceDocumnetsObj" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1na" UpdateMode="Always">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandArgument='<%# Eval("FileID")%>'
                                                                                        AutoPostBack="true" CommandName="Delete" Visible="false"
                                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                        ID="lbtLinkDocbuttonobj" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' 
                                                                                                    alt="Delete" title="Delete" width="15px" height="15px" />
                                                                                    </asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lbtLinkDocbuttonobj" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <HeaderStyle BackColor="#ECF0F1" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found.
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Annexure Title</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtAnnexueTitle" CssClass="form-control" MaxLength="200" Style="margin-bottom: 10px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <div runat="server" id="ShowTable">
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Table 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="tbxTable" CssClass="form-control myTextEditor" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                       <%-- <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <textarea runat="server" id="editor" rows="10" cols="30" style="height: 400px; width: 750px;"> </textarea>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;padding-top: 10px;">Upload Image </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="row" style="margin-top: 10px;">
                                                                    <asp:GridView runat="server" ID="grdObservationImg" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdObservationImg_PageIndexChanging"
                                                                        OnRowCommand="grdObservationImg_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Image" ItemStyle-Width="35%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                                                        <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("ImageName") %>'
                                                                                            data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ImageName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID") %>' AutoPostBack="true" CommandName="ViewImage"
                                                                                                ID="lnkBtnViewDoc" runat="server" data-toggle="tooltip" data-placement="top" title="View Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="DeleteImage" Visible="false"
                                                                                                OnClientClick="return confirm('Are you certain you want to delete this image?');"
                                                                                                ID="lnkBtnDeleteDoc" runat="server" data-toggle="tooltip" data-placement="top" title="Delete Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" /> <%--width="15" height="15"--%>
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnViewDoc" />
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDeleteDoc" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">Audio/Video Link
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtMultilineVideolink" runat="server" TextMode="MultiLine" CssClass="MultilineVideoLinkResizedTextbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </div>
                                            <tr style="display:none">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation Report
                                                    </label>
                                                </td>
                                                <td>
                                                    <%-- <asp:RadioButtonList ID="ObjReport" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Audit Committee" Value="AuditCommittee"></asp:ListItem>
                                                                <asp:ListItem Text="MIS" Value="MIS"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:RequiredFieldValidator ID="RFObjReport" runat="server" ControlToValidate="ObjReport"
                                                                Display="Dynamic" ErrorMessage="Observation report type should not be empty."
                                                                ValidationGroup="ThirdValidationGroup" />--%>
                                                    <asp:DropDownList ID="ObjReport1" runat="server" CssClass="form-control m-bot15" Width="23%">

                                                        <asp:ListItem Value="1">Audit Committee</asp:ListItem>
                                                        <%--<asp:ListItem Value="0">MIS</asp:ListItem>
                                                        <asp:ListItem Value="2">Both of the above</asp:ListItem>--%>
                                                        <asp:ListItem Value="-1">None of the above</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RFObjReport" runat="server" ControlToValidate="ObjReport1"
                                                        Display="Dynamic" ErrorMessage="Please select Observation report type."
                                                        ValidationGroup="ThirdValidationGroup" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Business Implication 
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtRisk" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Root Cause 
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtRootcost" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Financial Impact 
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtfinancialImpact" CssClass="form-control"
                                                        Style="margin-bottom: 10px; width: 945px;" /> <%--onkeypress="return allowOnlyNumber(event);" onpaste="return false;"--%>
                                                    <asp:RequiredFieldValidator ID="RFVfinancialImpact" ErrorMessage="Financial Impact can not be Empty." ControlToValidate="txtfinancialImpact"
                                                            runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                        <%--<asp:CompareValidator ID="cvFImpact" runat="server" ControlToValidate="txtfinancialImpact" ErrorMessage="Only Numbers in Financial Imapct"
                                                            ValidationGroup="ThirdValidationGroup" Display="None" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Recommendation
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtRecommendation" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" CssClass="form-control" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Response Due Date
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox AutoComplete="off" runat="server" ID="txtResponseDueDate" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Management Response 
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtMgtResponse" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                    <asp:RequiredFieldValidator ID="RFVMgtRes" ErrorMessage="Management Response can not be empty."
                                                        ControlToValidate="txtMgtResponse"
                                                        runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Time Line
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtTimeLine" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;" />
                                                    <asp:RequiredFieldValidator ID="RFVTimeLine" ErrorMessage="Time Line can not be empty."
                                                        ControlToValidate="txtTimeLine"
                                                        runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                </td>
                                            </tr>
                                            <tr style="display:none">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Score
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtAuditStepScore" CssClass="form-control"
                                                        Style="margin-bottom: 10px; width: 115px; text-align: center;"
                                                        onkeypress="return allowOnlyNumberWithDecimal(event);" onpaste="return false;" />

                                                    <asp:CompareValidator ID="cvRootCost" runat="server" ControlToValidate="txtAuditStepScore"
                                                        ErrorMessage="Only Numbers in Score"
                                                        ValidationGroup="ThirdValidationGroup" Display="None" Type="Double"
                                                        Operator="DataTypeCheck"></asp:CompareValidator>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Person Responsible
                                                    </label>
                                                </td>
                                                <td>

                                                    <asp:DropDownListChosen ID="ddlPersonresponsible" runat="server" CssClass="form-control m-bot15"
                                                        Width="50%" Height="32px" Style="margin-bottom: 10px;">
                                                    </asp:DropDownListChosen>

                                                </td>
                                            </tr>

                                            <tr style="display: none">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Unit Head Response
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="tbxUHComment" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Person Responsible
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownListChosen ID="ddlPersonresponsibleUH" runat="server"
                                                        CssClass="form-control m-bot15"
                                                        AllowSingleDeselect="false"
                                                        DisableSearchThreshold="3"
                                                        Width="50%" Height="32px" DataPlaceHolder="Person Responsible">
                                                    </asp:DropDownListChosen>
                                                </td>
                                            </tr>

                                            <tr style="display: none">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        President Response
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="tbxPresidentComment" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 945px;" />
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Person Responsible
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownListChosen ID="ddlPersonresponsiblePresident" runat="server"
                                                        CssClass="form-control m-bot15"
                                                        AllowSingleDeselect="false"
                                                        DisableSearchThreshold="3"
                                                        Width="50%" Height="32px" DataPlaceHolder="Person Responsible">
                                                    </asp:DropDownListChosen>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Owner Name
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownListChosen ID="ddlOwnerName" runat="server" CssClass="form-control m-bot15"
                                                        Width="50%" Height="32px" DataPlaceHolder="Owner">
                                                    </asp:DropDownListChosen>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Select OwnerName."
                                                        ControlToValidate="ddlOwnerName" InitialValue="-1"
                                                        runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation Rating
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlobservationRating" runat="server" CssClass="form-control m-bot15"
                                                        Width="50%" Height="32px" DataPlaceHolder="Observation Rating" Style="margin-bottom: 10px;">
                                                        <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                                        <asp:ListItem Value="1">Major</asp:ListItem>
                                                        <asp:ListItem Value="2">Moderate</asp:ListItem>
                                                        <asp:ListItem Value="3">Minor</asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Defeciency Type
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlDefeciencyType" runat="server" CssClass="form-control m-bot15"
                                                        Width="50%" Height="32px" DataPlaceHolder="Observation Rating">
                                                        <asp:ListItem Value="-1">Select Defeciency Type</asp:ListItem>
                                                        <asp:ListItem Value="1">Design</asp:ListItem>
                                                        <asp:ListItem Value="2">Operation</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation Category
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlObservationCategory" runat="server" CssClass="form-control m-bot15" Width="50%" Height="32px"
                                                        DataPlaceHolder="Observation Category" Style="margin-bottom: 10px;"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlObservationCategory_SelectedIndexChanged">
                                                    </asp:DropDownList>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Observation SubCategory
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlObservationSubCategory" runat="server" CssClass="form-control m-bot15" Width="50%" Height="32px"
                                                        DataPlaceHolder="Observation SubCategory" Style="margin-bottom: 10px;">
                                                    </asp:DropDownList>

                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td style="vertical-align: top;">
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                    <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                        Reviewer Risk Rating
                                                    </label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlReviewerRiskRatiing" runat="server" CssClass="form-control m-bot15"
                                                        Width="50%" Height="32px" DataPlaceHolder="Reviewer Rating" Style="margin-bottom: 10px;">
                                                        <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                                        <asp:ListItem Value="1">High</asp:ListItem>
                                                        <asp:ListItem Value="2">Medium</asp:ListItem>
                                                        <asp:ListItem Value="3">Low</asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" align="center" style="margin-top: 50px;">
                                                    <asp:Button Text="Next" runat="server" ID="btnNext3" OnClick="btnNext3_Click"
                                                        CssClass="btn btn-search" CausesValidation="true" OnClientClick="javascript:userValid();bind_tinyMCE();"
                                                        ValidationGroup="ThirdValidationGroup" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnObjfileupload" />
                                        <asp:PostBackTrigger ControlID="btnNext3" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </asp:View>

                        <asp:View ID="FourthView" runat="server">
                            <div style="width: 100%; float: left; margin-bottom: 15px">
                                <div style="margin-bottom: 4px; width: auto">
                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                                    <div style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                        <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label6" runat="server" Text="" ForeColor="red"></asp:Label>
                                        <asp:CustomValidator ID="CustomValidator3" runat="server" EnableClientScript="False"
                                            ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                    </div>
                                </div>
                                <%-- <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>--%>
                                <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                    <legend>Review History</legend>
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <div class="panel">
                                                    <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                        AllowPaging="true" PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right" GridLines="None" CssClass="table"
                                                        CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px"
                                                        OnPageIndexChanging="GridRemarks_OnPageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    <asp:Label ID="lblAuditId" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="riskID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("InternalAuditInstance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Scheduleon" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                <ItemTemplate>
                                                                    <div class="text_NlinesusingCSS" style="width: 400px;">
                                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' ToolTip='<%# Eval("Remarks") %>' data-toggle="tooltip" data-placement="top"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                <ItemTemplate>
                                                                    <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="lblDownLoadfile" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>' OnClick="DownLoadClick"></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdAudit1">
                                                <label style="width: 210px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Review Remark</label>
                                            </td>
                                            <td class="tdAudit2">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; height: 100px; width: 900px;"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label style="width: 125px; display: block; float: left; font-size: 14px; color: #666;">
                                                    &nbsp;</label>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="FileuploadAdditionalFile" runat="server" AllowMultiple="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label style="width: 220px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                    Date</label>
                                            </td>
                                            <td>
                                                <div style="margin-bottom: 7px" id="divDated" runat="server">

                                                    <asp:TextBox runat="server" ID="tbxDate" CssClass="form-control" Style="width: 115px; text-align: center" Enabled="false" />
                                                    <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                        runat="server" ID="RequiredFieldValidator1" ValidationGroup="oplValidationGroup" Display="None" />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td style="float: left;">
                                                <asp:Button Text="Submit" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                    ValidationGroup="oplValidationGroup" /><%--OnClientClick="if (!ValidateFile()) return false;"--%>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <div class="clearfix"></div>
                                <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                    <legend>Audit Log</legend>
                                    <div class="panel">
                                        <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                            AllowPaging="true" PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right" GridLines="None" CssClass="table"
                                            CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                            DataKeyNames="AuditTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                    <ItemTemplate>
                                                        <div class="text_NlinesusingCSS" style="width: 300px;">
                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Remarks") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <HeaderStyle BackColor="#ECF0F1" />
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                </fieldset>
                                <%--</ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>

        <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <%--<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label class="modal-header-custom">
                                    View Document(s)</label>--%>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: right; width: 90%">
                                <asp:Label runat="server" ID="Label10" Style="color: red;"></asp:Label>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="IFrameImage" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
        <asp:HiddenField runat="server" ID="hidObservation" />
        <asp:HiddenField runat="server" ID="hidAuditObjective" />
        <asp:HiddenField runat="server" ID="hidAuditSteps" />
        <asp:HiddenField runat="server" ID="hidAnalysisToBePerformed" />
        <asp:HiddenField runat="server" ID="hidWalkthrough" />
        <asp:HiddenField runat="server" ID="hidActualWorkDone" />
        <asp:HiddenField runat="server" ID="hidpopulation" />
        <asp:HiddenField runat="server" ID="hidSample" />
        <asp:HiddenField runat="server" ID="hidObservationNumber" />
        <asp:HiddenField runat="server" ID="hidObservationTitle" />
        <asp:HiddenField runat="server" ID="hidRisk" />
        <asp:HiddenField runat="server" ID="hidRootcost" />
        <asp:HiddenField runat="server" ID="hidfinancialImpact" />
        <asp:HiddenField runat="server" ID="hidRecommendation" />
        <asp:HiddenField runat="server" ID="hidMgtResponse" />
        <asp:HiddenField runat="server" ID="hidRemarks" />
        <asp:HiddenField runat="server" ID="hidAuditStepScore" />
        <asp:HiddenField runat="server" ID="hidTimeLine" />
        <asp:HiddenField runat="server" ID="hidISACPORMIS" />
        <asp:HiddenField runat="server" ID="hidPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidObservationRating" />
        <asp:HiddenField runat="server" ID="hidObservationCategory" />
        <asp:HiddenField runat="server" ID="hidObservationSubCategory" />
        <asp:HiddenField runat="server" ID="hidOwner" />
        <asp:HiddenField runat="server" ID="hidUserID" />
        <asp:HiddenField runat="server" ID="hidResponseDueDate" />
        <asp:HiddenField runat="server" ID="hidUHPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidPRESIDENTPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidUHComment" />
        <asp:HiddenField runat="server" ID="hidPRESIDENTComment" />
        <asp:HiddenField runat="server" ID="hidBodyContent" />
    </form>
</body>
</html>
