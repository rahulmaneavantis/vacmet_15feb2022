﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditStatusUI_IMP : System.Web.UI.Page
    {
        int Statusid;
        public static List<long> Branchlist = new List<long>();
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;

        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.IMPGetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            if (!IsPostBack)
            {                
                BindVertical();                
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"]= 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                }

                BindFinancialYear();
                BindLegalEntityData();
                if (!String.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    string FinancialYear = Convert.ToString(Request.QueryString["FinYear"]);
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                else
                {
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                    }
                }
                if (roles.Contains(3))
                {
                    PerformerFlag = true;
                    ShowPerformer(sender, e);
                }
                else if (roles.Contains(4))
                {
                    ReviewerFlag = true;
                    ShowReviewer(sender, e);
                }
                else
                {
                    PerformerFlag = true;                    
                }                              

                ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;
                        
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            
            BindData("P");

        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void BindCustomer()
        {
            int customerID = -1;
            //customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("Location ", "-1"));
        }

        public void BindFinancialYear()
        {            
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;            
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));           
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;           
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();           
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindVertical();
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindVertical();
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindVertical();
            }
            else
            {                
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindVertical();
            }
            else
            {               
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedItem.Text))
                {
                    if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                    {
                        BindSchedulingType(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindData("P"); BindVertical();
                        //GetPageDisplaySummary();
                        bindPageNumber();
                        BindVertical();
                    }
                }
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedItem.Text))
                {
                    if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                    {
                        BindData("N");
                        bindPageNumber();
                        //GetPageDisplaySummary();
                    }
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            //GetPageDisplaySummary();
            bindPageNumber();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            //{
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                //BindAuditSchedule("P", 5);

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            //}
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("P");                
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("N");
            }

            //GetPageDisplaySummary();
            bindPageNumber();
        }
        
        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            BindData("P");
            bindPageNumber();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            BindData("P");
            bindPageNumber();
        }

        private void BindData(string Flag)
        {
            try
            {
                List<int> roleid = new List<int>();
                int branchid = -1;
                int VerticalID = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;

                int customerID = -1;
                //customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                } 
                
                if(!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if(ddlFinancialYear.SelectedValue!="-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }               
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                Branchlist.Clear();
                GetAllHierarchy(customerID, branchid);
                Branchlist.ToList();

                if (PerformerFlag)
                    roleid.Add(3);

                else if (ReviewerFlag)
                    roleid.Add(4);

                
                if (Flag == "P")
                {
                    var AuditLists = InternalControlManagementDashboardRisk.GetAuditTotalCountUserWiseIMP(Portal.Common.AuthenticationHelper.UserID, roleid, customerID, Statusid, FinYear, Branchlist, Period, VerticalID);
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Rows.Count;
                    grdRiskActivityMatrix.DataBind();
                }
                //else
                //{
                //    var AuditLists = DashboardManagementRisk.IMPGetAudits(FinYear, Period, customerID, branchid, VerticalID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, roleid);
                //    grdRiskActivityMatrix.DataSource = AuditLists;
                //    Session["TotalRows"] = AuditLists.Count;
                //    grdRiskActivityMatrix.DataBind();
                //}
                //if (Flag == "P")
                //{                    
                //    var AuditLists = DashboardManagementRisk.IMPGetAudits(FinYear, Period, customerID, branchid, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID, roleid);
                //    grdRiskActivityMatrix.DataSource = AuditLists;
                //    Session["TotalRows"] = AuditLists.Count;
                //    grdRiskActivityMatrix.DataBind();

                //}
                //else
                //{
                //    var AuditLists = DashboardManagementRisk.IMPGetAudits(FinYear, Period, customerID, branchid, VerticalID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, roleid);
                //    grdRiskActivityMatrix.DataSource = AuditLists;
                //    Session["TotalRows"] = AuditLists.Count;
                //    grdRiskActivityMatrix.DataBind();
                //}

                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }      
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }       

        protected void upCompliance_Load(object sender, EventArgs e)
        {
          
        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {
           
        }

        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //try
            //{
            //    if (e.CommandName.Equals("ViewAuditStatusSummary"))
            //    {
            //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            //        //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
            //        //string ForMonth = Convert.ToString(commandArgs[1]);
            //        //string FinancialYear = Convert.ToString(commandArgs[2]);
            //        int RoleID = Convert.ToInt32(commandArgs[3]);
            //        //int UserID = Convert.ToInt32(commandArgs[4]);
            //        //int VerticalId = Convert.ToInt32(commandArgs[5]);
            //        long AuditID = Convert.ToInt64(commandArgs[6]);


            //            if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
            //            {
            //                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummaryIMP.aspx?Role=" + RoleID + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString()+ "&AuditID="+ AuditID, false);
            //            }
            //            else
            //            {
            //                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummaryIMP.aspx?Role=" + RoleID + "&ReturnUrl1=" + "&AuditID=" + AuditID, false);
            //            }

            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    String Args = e.CommandArgument.ToString();

                    string[] arg = Args.ToString().Split(';');
                    String URLStr = "";
                    string url = "";
                    string AuditID = "";
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToString(Request.QueryString["AuditID"]);
                    }
                    URLStr = "~/RiskManagement/InternalAuditTool/AuditMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FinYear=" + arg[4] + "&Period=" + arg[5] + "&RoleID=" + arg[6] + "&VID=" + arg[7] + "&AuditID=" + AuditID;
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
                    }
                    string url2 = HttpContext.Current.Request.Url.AbsolutePath;
                    if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                    {
                        URLStr = "~/RiskManagement/InternalAuditTool/AuditMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FinYear=" + arg[4] + "&Period=" + arg[5] + "&RoleID=" + arg[6] + "&VID=" + arg[7] + "&AuditID=" + AuditID + "&returnUrl2=Role@" + Request.QueryString["Role"].ToString() + url;
                    }
                    else
                    {
                        URLStr = "~/RiskManagement/InternalAuditTool/AuditMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FinYear=" + arg[4] + "&Period=" + arg[5] + "&RoleID=" + arg[6] + "&VID=" + arg[7] + "&AuditID=" + AuditID;
                    }

                    if (Args != "")
                        Response.Redirect(URLStr, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
          
        }

        protected void grdRiskActivityMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
        }

        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {             
                BindData("P");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}               
                //Reload the Grid
                BindData("P");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {                
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }              
        //        //Reload the Grid
        //        BindData("P");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {                
        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }             
        //        //Reload the Grid
        //        BindData("P");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            //GetPageDisplaySummary();
            bindPageNumber();
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    if (Session["CustomerID_new"] == null)
                        branchid =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    else
                        branchid = Convert.ToInt32(Session["CustomerID_new"]);
                    //branchid =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}