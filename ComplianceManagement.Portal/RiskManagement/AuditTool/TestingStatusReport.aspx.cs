﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class TestingStatusReport : System.Web.UI.Page
    {
        protected List<Int32> roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindFnancialYear();
                BindComplianceMatrix();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
            roles = CustomerManagementRisk.GetAssignedRolesICFR(Portal.Common.AuthenticationHelper.UserID);
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceRoleMatrix.PageIndex = chkSelectedPage - 1;

            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
         
            BindComplianceMatrix();

        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            //try
            //{                                
            //    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }
               
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix();
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                #region Previous Code
                //using (ExcelPackage exportPackge = new ExcelPackage())
                //{
                //    try
                //    {
                //        string LocationName = string.Empty;
                //        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                //        {
                //            if (ddlLegalEntity.SelectedValue != "-1")
                //            {
                //                LocationName = ddlLegalEntity.SelectedItem.Text;

                //            }
                //        }
                //        if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                //        {
                //            if (ddlSubEntity1.SelectedValue != "-1")
                //            {
                //                LocationName = ddlSubEntity1.SelectedItem.Text;
                //            }
                //        }
                //        if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                //        {
                //            if (ddlSubEntity2.SelectedValue != "-1")
                //            {
                //                LocationName = ddlSubEntity2.SelectedItem.Text;
                //            }
                //        }
                //        if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                //        {
                //            if (ddlSubEntity3.SelectedValue != "-1")
                //            {
                //                LocationName = ddlSubEntity3.SelectedItem.Text;
                //            }
                //        }
                //        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                //        {
                //            if (ddlSubEntity4.SelectedValue != "-1")
                //            {
                //                LocationName = ddlSubEntity4.SelectedItem.Text;
                //            }
                //        }
                //        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Internal Financial Control");
                //        DataTable ExcelData = null;
                //        BindComplianceMatrix();
                //        DataView view = new System.Data.DataView((grdComplianceRoleMatrix.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                //        ExcelData = view.ToTable("Selected", false, "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                //        var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                //        exWorkSheet.Cells["A2"].Value = customer.Name;
                //        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                //        exWorkSheet.Cells["A3"].Value = LocationName;
                //        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["A3"].Style.Font.Size = 12;

                //        exWorkSheet.Cells["A4"].Value = ddlFilterFinancial.SelectedItem.Text;
                //        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["A4"].Style.Font.Size = 12;

                //        exWorkSheet.Cells["A8"].LoadFromDataTable(ExcelData, true);

                //        exWorkSheet.Cells["A6"].Value = "Internal Financial Control";
                //        exWorkSheet.Cells["A6"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["A6"].AutoFitColumns(50);

                //        exWorkSheet.Cells["A8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["A8"].Style.Font.Size = 12;

                //        exWorkSheet.Cells["B8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["B8"].Value = "Sub Process";
                //        exWorkSheet.Cells["B8"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["B8"].AutoFitColumns(50);

                //        exWorkSheet.Cells["C8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["C8"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["C8"].Value = "Risk Description";
                //        exWorkSheet.Cells["C8"].AutoFitColumns(100);

                //        exWorkSheet.Cells["D8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["D8"].Value = "Control Description";
                //        exWorkSheet.Cells["D8"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["D8"].AutoFitColumns(100);

                //        exWorkSheet.Cells["E8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["E8"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["E8"].Value = "Key /NonKey";
                //        exWorkSheet.Cells["E8"].AutoFitColumns(30);

                //        exWorkSheet.Cells["F8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["F8"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["F8"].Value = "TOD (Pass or Fail)";
                //        exWorkSheet.Cells["F8"].AutoFitColumns(50);

                //        exWorkSheet.Cells["G8"].Value = "TOE (Pass or Fail)";
                //        exWorkSheet.Cells["G8"].Style.Font.Bold = true;
                //        exWorkSheet.Cells["G8"].Style.Font.Size = 12;
                //        exWorkSheet.Cells["G8"].AutoFitColumns(50);

                //        using (ExcelRange col = exWorkSheet.Cells[2, 1, 8 + ExcelData.Rows.Count, 8])
                //        {
                //            //col.Style.Numberformat.Format = "dd/MM/yyyy";
                //            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //            //col.AutoFitColumns();
                //            col.Style.Numberformat.Format = "dd/MM/yyyy";
                //            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                //            col.Style.WrapText = true;
                //            //col.AutoFitColumns(20);
                //            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                //            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                //            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                //            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;   
                //        }

                //        Byte[] fileBytes = exportPackge.GetAsByteArray();
                //        Response.ClearContent();
                //        Response.Buffer = true;
                //        Response.AddHeader("content-disposition", "attachment;filename=TestingStatusReport.xlsx");
                //        Response.Charset = "";
                //        Response.ContentType = "application/vnd.ms-excel";
                //        StringWriter sw = new StringWriter();
                //        Response.BinaryWrite(fileBytes);
                //    }
                //    catch (Exception)
                //    {
                //    }
                //    //Response.End();
                //}
                #endregion


                BindComplianceMatrix();
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        String FileName = String.Empty;
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Internal Financial Control");
                        DataTable ExcelData = null;                       
                        DataView view = new System.Data.DataView((grdComplianceRoleMatrix.DataSource as List<AuditTestingstatusExportView>).ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "ControlNo", "Branch", "FinancialYear", "Process", "SubProcess", "ActivityDescription", "ControlDescription", "KeyName", "TOD", "TOE");

                        var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                        FileName = "Testing Status Report";

                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A2"].Value = customer.Name;
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;


                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A3"].Value = FileName;
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);


                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].Value = "Control No";
                        exWorkSheet.Cells["A5"].AutoFitColumns(15);


                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].Value = "Location";
                        exWorkSheet.Cells["B5"].AutoFitColumns(25);


                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].Value = "Financial Year";
                        exWorkSheet.Cells["C5"].AutoFitColumns(25);

                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].Value = "Process";
                        exWorkSheet.Cells["D5"].AutoFitColumns(25);

                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].Value = "Sub Process";
                        exWorkSheet.Cells["E5"].AutoFitColumns(50);

                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F5"].Value = "Risk Description";
                        exWorkSheet.Cells["F5"].AutoFitColumns(50);


                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G5"].Value = "Control Description";
                        exWorkSheet.Cells["G5"].AutoFitColumns(50);

                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H5"].Value = "Key/Non Key";
                        exWorkSheet.Cells["H5"].AutoFitColumns(20);

                        exWorkSheet.Cells["I5"].Value = "TOD (Pass or Fail)";
                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I5"].AutoFitColumns(20);

                        exWorkSheet.Cells["J5"].Value = "TOE (Pass or Fail)";
                        exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J5"].AutoFitColumns(20);



                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 10])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }


                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=TestingStatusReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        private void BindComplianceMatrix()
        {
            try
            {
                long Branchid = -1;
                string FnancialYear = "";                
                if (!String.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                {
                    if (Convert.ToString(GetBranchID()) != "-1")
                    {
                        Branchid = Convert.ToInt32(GetBranchID());
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedValue != "-1")
                    {
                        FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                else
                {
                    FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                }

                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


                var TestingStatsReportList = ProcessManagement.GetTestingStatusGridDisplayView(customerID,Convert.ToInt32(Branchid), FnancialYear);
                grdComplianceRoleMatrix.DataSource = TestingStatsReportList;
                Session["TotalRows"] = TestingStatsReportList.Count;
                grdComplianceRoleMatrix.DataBind();                
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {           
        }       
        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }            
            return processnonprocess;
        }
        public static List<FileData_Risk> GetFileData1(int RiskCreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.FileData_Risk
                                where row.RiskCreationId == RiskCreationId
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblRiskCategoryCreationId = (Label)gvr.FindControl("lblRiskCategoryCreationId");

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<FileData_Risk> fileData = GetFileData1(Convert.ToInt32(lblRiskCategoryCreationId.Text));
                    int i = 0;
                    string directoryName = "abc";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];                            
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindComplianceMatrix();
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ////if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
               
                //Reload the Grid
                BindComplianceMatrix();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceRoleMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        BindComplianceMatrix();
        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        /// <summary>
        /// Added for next button in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        BindComplianceMatrix();
        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}


        /// <summary>
        /// Added for Get Total Page Count in grdview Total page row count.
        /// </summary>
        /// <returns></returns>
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

       

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindComplianceMatrix();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindComplianceMatrix();
                bindPageNumber();
               // GetPageDisplaySummary();
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindComplianceMatrix();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindComplianceMatrix();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public int GetBranchID()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            return CustomerBranchId;
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindComplianceMatrix();
           // GetPageDisplaySummary();
            bindPageNumber();
        }
    }
}