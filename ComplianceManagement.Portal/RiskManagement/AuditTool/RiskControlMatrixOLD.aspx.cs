﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class RiskControlMatrixOLD : System.Web.UI.Page
    {
        //public static List<int> ClientIdProcess = new List<int>();
        public static List<int> RiskIdProcess = new List<int>();
        public static List<int> AssertionsIdProcess = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomer();
               
                BindLocationType();
                BindIndustry();
                BindRiskCategory();
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                            && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "L");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                    && ddlFilterIndustry.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "I");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "R");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "S");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "P");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "B");
                    }
                    else
                    {
                        BindData("P", "A");
                    }
                }
                else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
                {
                    if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                   && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                       && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "L");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                    && ddlFilterIndustry.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "I");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "R");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "S");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "P");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "B");
                    }
                    else
                    {
                        BindData("N", "A");
                    }
                }
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("< All >", "-1"));
            ddlProcess.Items.Insert(0, new ListItem("< All >", "-1"));
            ddlSubProcess.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        private void BindProcess(string flag)
        {
            try
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    if (flag == "P")
                    {
                        ddlSubProcess.Items.Clear();
                        ddlProcess.Items.Clear();
                        ddlProcess.DataTextField = "Name";
                        ddlProcess.DataValueField = "Id";
                        ddlProcess.DataSource = ProcessManagement.FillProcess("P", Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        ddlProcess.DataBind();
                        ddlProcess.Items.Insert(0, new ListItem("< All >", "-1"));
                        if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                        }
                    }
                    else
                    {
                        ddlSubProcess.Items.Clear();
                        ddlProcess.Items.Clear();
                        ddlProcess.DataTextField = "Name";
                        ddlProcess.DataValueField = "Id";
                        ddlProcess.DataSource = ProcessManagement.FillProcess("N", Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        ddlProcess.DataBind();
                        ddlProcess.Items.Insert(0, new ListItem("< All >", "-1"));
                        if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("< All >", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("< All >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLocationType()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlFilterLocationType.DataTextField = "Name";
            ddlFilterLocationType.DataValueField = "ID";
            ddlFilterLocationType.Items.Clear();
            ddlFilterLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlFilterLocationType.DataBind();
            ddlFilterLocationType.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        public void BindIndustry()
        {
            ddlFilterIndustry.DataTextField = "Name";
            ddlFilterIndustry.DataValueField = "ID";
            ddlFilterIndustry.Items.Clear();
            ddlFilterIndustry.DataSource = ProcessManagement.FilliNDUSTRY();
            ddlFilterIndustry.DataBind();
            ddlFilterIndustry.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        public void BindRiskCategory()
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {

                ddlFilterRiskCategory.DataTextField = "Name";
                ddlFilterRiskCategory.DataValueField = "ID";
                ddlFilterRiskCategory.Items.Clear();
                ddlFilterRiskCategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("P");
                ddlFilterRiskCategory.DataBind();
                ddlFilterRiskCategory.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            else
            {
                ddlFilterRiskCategory.DataTextField = "Name";
                ddlFilterRiskCategory.DataValueField = "ID";
                ddlFilterRiskCategory.Items.Clear();
                ddlFilterRiskCategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("N");
                ddlFilterRiskCategory.DataBind();
                ddlFilterRiskCategory.Items.Insert(0, new ListItem("< All >", "-1"));
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");
                BindData("P", "B");
                setDateToGridView();
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("N");
                BindData("N", "B");
                setDateToGridView();
            }
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlProcess.SelectedValue != "< All >" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }

                BindData("P", "P");
                Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlClientProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlClientProcess.ClientID), true);
                }
                Saplin.Controls.DropDownCheckBoxes ddlRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlRiskCategoryProcess.ClientID), true);
                }
                setDateToGridView();
            }
            else
            {
                if (ddlProcess.SelectedValue != "< All >" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                }
                BindData("N", "P");
                Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlClientProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlClientProcess.ClientID), true);
                }
                Saplin.Controls.DropDownCheckBoxes ddlRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlRiskCategoryProcess.ClientID), true);
                }
                setDateToGridView();
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P", "S");
                Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlClientProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlClientProcess.ClientID), true);
                }
                Saplin.Controls.DropDownCheckBoxes ddlRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlRiskCategoryProcess.ClientID), true);
                }
                setDateToGridView();
            }
            else
            {
                BindData("N", "S");
                Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlClientProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlClientProcess.ClientID), true);
                }
                Saplin.Controls.DropDownCheckBoxes ddlRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryProcess");
                if (ddlClientProcess != null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlRiskCategoryProcess.ClientID), true);
                }
                setDateToGridView();
            }
        }
        protected void ddlFilterRiskCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P", "R");
            }
            else
            {
                this.BindData("N", "R");
            }
        }
        protected void ddlFilterIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P", "I");
            }
            else
            {
                this.BindData("N", "I");
            }
        }
        protected void ddlFilterLocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P", "L");
            }
            else
            {
                this.BindData("N", "L");
            }
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");
                BindData("P", "A");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("N");
                BindData("N", "A");
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdRiskActivityMatrix.Rows.Count > 0)
                {
                    bool suucess = false;
                    List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                    List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();
                    foreach (GridViewRow g1 in grdRiskActivityMatrix.Rows)
                    {
                        string taskid = (g1.FindControl("lbltaskidItemTemplate") as Label).Text;
                        string Assertions = (g1.FindControl("lblAssertions") as Label).Text;
                        string ExistingControlDescription = (g1.FindControl("txtExistingControlDescriptionItemTemplate") as TextBox).Text;
                        string MControlDescription = (g1.FindControl("txtMControlDescriptionItemTemplate") as TextBox).Text;
                        string PersonResponsible = (g1.FindControl("ddlPersonResponsibleItemTemplate") as DropDownList).SelectedItem.Value;
                        string EffectiveDate = (g1.FindControl("txtEffectiveDateItemTemplate") as TextBox).Text;
                        string Key = (g1.FindControl("ddlKeyItemTemplate") as DropDownList).SelectedItem.Value;
                        string Preventive = (g1.FindControl("ddlPreventiveItemTemplate") as DropDownList).SelectedItem.Value;
                        string Automated = (g1.FindControl("ddlAutomatedItemTemplate") as DropDownList).SelectedItem.Value;
                        string Frequency = (g1.FindControl("ddlFrequencytemTemplate") as DropDownList).SelectedItem.Value;
                        string lblClient = (g1.FindControl("lblClient") as Label).Text;
                        string GapDescriptionItemTemplate = (g1.FindControl("txtGapDescriptionItemTemplate") as TextBox).Text;
                        string RecommendationsItemTemplate = (g1.FindControl("txtRecommendationsItemTemplate") as TextBox).Text;
                        string ActionRemediationplanItemTemplate = (g1.FindControl("txtActionRemediationplanItemTemplate") as TextBox).Text;
                        string IPEItemTemplate = (g1.FindControl("txtIPEItemTemplate") as TextBox).Text;
                        string ERPsystemItemTemplate = (g1.FindControl("txtERPsystemItemTemplate") as TextBox).Text;
                        string FRCItemTemplate = (g1.FindControl("txtFRCItemTemplate") as TextBox).Text;
                        string UniqueReferredItemTemplate = (g1.FindControl("txtUniqueReferredItemTemplate") as TextBox).Text;
                        
                        int customerbranchid = RiskCategoryManagement.GetCustomerBranchIDByName(lblClient);
                        if (!String.IsNullOrEmpty(taskid))
                        {
                            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                            riskactivitytransaction.ProcessId = Convert.ToInt32(ddlProcess.SelectedValue);
                            riskactivitytransaction.SubProcessId = Convert.ToInt32(ddlSubProcess.SelectedValue);
                            riskactivitytransaction.RiskCreationId = Convert.ToInt32(taskid);
                            riskactivitytransaction.Assertion = Convert.ToInt32(Assertions);
                            riskactivitytransaction.ControlDescription = ExistingControlDescription;
                            riskactivitytransaction.MControlDescription = MControlDescription;
                            riskactivitytransaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                            riskactivitytransaction.EffectiveDate = Convert.ToDateTime(EffectiveDate);
                            riskactivitytransaction.Key_Value = Convert.ToInt32(Key);
                            riskactivitytransaction.PrevationControl = Convert.ToInt32(Preventive);
                            riskactivitytransaction.AutomatedControl = Convert.ToInt32(Automated);
                            riskactivitytransaction.Frequency = Convert.ToInt32(Frequency);
                            riskactivitytransaction.CustomerBranchId = customerbranchid;
                            riskactivitytransaction.GapDescription = GapDescriptionItemTemplate;
                            riskactivitytransaction.Recommendations = RecommendationsItemTemplate;
                            riskactivitytransaction.ActionRemediationplan = ActionRemediationplanItemTemplate;
                            riskactivitytransaction.IPE = IPEItemTemplate;
                            riskactivitytransaction.ERPsystem = ERPsystemItemTemplate;
                            riskactivitytransaction.FRC = FRCItemTemplate;
                            riskactivitytransaction.UniqueReferred = UniqueReferredItemTemplate;
                            //riskactivitytransaction.TestStrategy = TestStrategyItemTemplate;
                            //riskactivitytransaction.DocumentsExamined = DocumentsExaminedItemTemplate;
                            riskactivitytransactionlist.Add(riskactivitytransaction);
                        }
                    }
                    riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.Frequency == null).ToList();
                    if (riskactivitytransactionlist1.Count == 0)
                    {
                        suucess = RiskCategoryManagement.CreateRiskActivityTransaction(riskactivitytransactionlist);
                        if (suucess == true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Data Save successfully.";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Error.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowAssertionsName(long categoryid)
        {
            List<AssertionsName_Result> a = new List<AssertionsName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetAssertionsNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowPersonResposibleName(long categoryid)
        {
            List<PersonResponsibleName_Result> a = new List<PersonResponsibleName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetPersonResponsibleNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowKeyName(long categoryid)
        {
            List<KeyName_Result> a = new List<KeyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetKeyNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowControlName(long categoryid)
        {
            List<ControlName_Result> a = new List<ControlName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetControlNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowFrequencyName(long categoryid)
        {
            List<FrequencyName_Result> a = new List<FrequencyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetFrequencyNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowCustomerBranchName(long categoryid, long branchid)
        {
            List<CustomerBranchNameLocationWise_Result> a = new List<CustomerBranchNameLocationWise_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedureLocationWise(Convert.ToInt32(categoryid), Convert.ToInt32(branchid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowRiskCategoryName(long categoryid)
        {
            List<RiskCategoryName_Result> a = new List<RiskCategoryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetRiskCategoryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowClientName(long branchid)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetClientnameName(branchid);
            return processnonprocess;
        }
        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;

            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId,customerID);
            return processnonprocess;
        }

        public string ShowSubProcessName(long processId, long SubprocessId)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetSubProcessName(processId, SubprocessId);
            return processnonprocess;
        }

        public string ShowRiskRatingName(int? ValueID)
        {
            string processnonprocess = "";
            if (!string.IsNullOrEmpty(Convert.ToString(ValueID)))
            {
                processnonprocess = GetRiskControlRating(Convert.ToInt32(ValueID), "R");
            }             
            return processnonprocess.Trim();
        }
        public string GetRiskControlRating(int ValueID , string flag)
        {            
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = "";
                if (flag =="R")
                {
                     transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                             where row.Value == ValueID && row.IsActive == false
                                             && row.IsRiskControl == "R"
                                             select row.Name).FirstOrDefault();
                }
                else if (flag=="C")
                {
                    transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                             where row.Value == ValueID && row.IsActive == false
                                             && row.IsRiskControl == "C"
                                             select row.Name).FirstOrDefault();
                }
               
                return transactionsQuery;
               
            }
        }
        public string ShowControlRatingName(int? ValueID)
        {
            string processnonprocess = "";
            if (!string.IsNullOrEmpty(Convert.ToString(ValueID)))
            {
                processnonprocess = GetRiskControlRating(Convert.ToInt32(ValueID), "C"); 
            }                  
            return processnonprocess.Trim();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        private void BindDataExport(string Flag, string Flag2)
        {
            try
            {
                int customerID = -1;
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterRiskCategory.SelectedValue))
                {
                    if (ddlFilterRiskCategory.SelectedValue != "-1")
                    {
                        RiskCategory = Convert.ToInt32(ddlFilterRiskCategory.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterIndustry.SelectedValue))
                {
                    if (ddlFilterIndustry.SelectedValue != "-1")
                    {
                        Industry = Convert.ToInt32(ddlFilterIndustry.SelectedValue);
                    }
                }
                if (Flag == "P")
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "All");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Location");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Process");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "SubProcess");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "RiskCategory");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Industry");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "LocationType");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }

                }
                else
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "All");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Location");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Process");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "SubProcess");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "RiskCategory");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Industry");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "LocationType");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        GridExportExcel.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindData(string Flag, string Flag2)
        {
            try
            {
                int customerID = -1;
                //if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
                //{
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterRiskCategory.SelectedValue))
                {
                    if (ddlFilterRiskCategory.SelectedValue != "-1")
                    {
                        RiskCategory = Convert.ToInt32(ddlFilterRiskCategory.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterIndustry.SelectedValue))
                {
                    if (ddlFilterIndustry.SelectedValue != "-1")
                    {
                        Industry = Convert.ToInt32(ddlFilterIndustry.SelectedValue);
                    }
                }
                if (Flag == "P")
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "All");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Location");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Process");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "SubProcess");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "RiskCategory");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Industry");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "LocationType");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }


                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlClientProcess");
                    if (ddlClientProcess != null)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlClientProcess.ClientID), true);
                        this.countrybindClient(ddlClientProcess, customerID);
                    }
                    Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryProcess");
                    if (DrpRiskCategoryProcess != null)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", DrpRiskCategoryProcess.ClientID), true);
                        this.countrybindRisk(DrpRiskCategoryProcess, "P");
                    }

                    Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlAssertionsProcess");// (e.Row.FindControl("ddlAssertionsProcess") as DropDownCheckBoxes);
                    if (ddlAssertionsProcess != null)
                    {
                        this.countrybindAssertions(ddlAssertionsProcess);
                    }
                }
                else
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "All");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Location");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Process");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "SubProcess");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "RiskCategory");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Industry");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "LocationType");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        grdRiskActivityMatrix.DataBind();
                    }
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlClientProcess");
                    if (ddlClientProcess != null)
                    {
                        this.countrybindClient(ddlClientProcess, customerID);
                    }
                    Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryProcess");
                    if (DrpRiskCategoryProcess != null)
                    {
                        this.countrybindRisk(DrpRiskCategoryProcess, "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowDate(DateTime dt)
        {
            try
            {
                DateTime dt1 = dt;
                if (dt1 == null)
                    return String.Empty;
                else
                    return dt1.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void Add(object sender, EventArgs e)
        {
            try
            {
                Control control = null;
                if (grdRiskActivityMatrix.FooterRow != null)
                {
                    control = grdRiskActivityMatrix.FooterRow;
                }
                else
                {
                    control = grdRiskActivityMatrix.Controls[0].Controls[0];
                }
                int customerID = -1;
                //if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
                //{
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                string ActivityDescription = (control.FindControl("txtRiskActivityDescriptionFooterTemplate") as TextBox).Text;
                string controlObjective = (control.FindControl("txtControlObjectiveFooterTemplate") as TextBox).Text;
                //string AssertionsFooterTemplate = (control.FindControl("ddlAssertionsFooterTemplate") as DropDownList).SelectedItem.Value;
                string PersonResponsibleFooterTemplate = (control.FindControl("ddlPersonResponsibleFooterTemplate") as DropDownList).SelectedItem.Value;
                string KeyFooterTemplate = (control.FindControl("ddlKeyFooterTemplate") as DropDownList).SelectedItem.Value;
                string PreventiveFooterTemplate = (control.FindControl("ddlPreventiveFooterTemplate") as DropDownList).SelectedItem.Value;
                string AutomatedFooterTemplate = (control.FindControl("ddlAutomatedFooterTemplate") as DropDownList).SelectedItem.Value;
                string FrequencyFooterTemplate = (control.FindControl("ddlFrequencyFooterTemplate") as DropDownList).SelectedItem.Value;
                //branchname
                string ClientProcessFooterTemplate = (control.FindControl("ddlClientProcessFooterTemplate") as DropDownList).SelectedItem.Value;
                
                string EffectiveDateFooterTemplate = (control.FindControl("txtEffectiveDateFooterTemplate") as TextBox).Text;
                string MControlDescriptionFooterTemplate = (control.FindControl("txtMControlDescriptionFooterTemplate") as TextBox).Text;
                string ExistingControlDescriptionFooterTemplate = (control.FindControl("txtExistingControlDescriptionFooterTemplate") as TextBox).Text;
                string GapDescriptionFooterTemplate = (control.FindControl("txtGapDescriptionFooterTemplate") as TextBox).Text;
                string RecommendationsFooterTemplate = (control.FindControl("txtRecommendationsFooterTemplate") as TextBox).Text;
                string ActionRemediationplanFooterTemplate = (control.FindControl("txtActionRemediationplanFooterTemplate") as TextBox).Text;
                string IPEFooterTemplate = (control.FindControl("txtIPEFooterTemplate") as TextBox).Text;
                string ERPsystemFooterTemplate = (control.FindControl("txtERPsystemFooterTemplate") as TextBox).Text;
                string FRCFooterTemplate = (control.FindControl("txtFRCFooterTemplate") as TextBox).Text;
                string UniqueReferredFooterTemplate = (control.FindControl("txtUniqueReferredFooterTemplate") as TextBox).Text;

                string ddlRiskFooterTemplate = (control.FindControl("ddlRiskFooterTemplate") as DropDownList).SelectedItem.Value;
                string ddlControlFooterTemplate = (control.FindControl("ddlControlFooterTemplate") as DropDownList).SelectedItem.Value;
                DateTime dt = new DateTime();
                if (EffectiveDateFooterTemplate != null)
                {
                    dt = GetDate(EffectiveDateFooterTemplate);
                }
                long pid = -1;
                long psid = -1;

                if (ddlProcess.SelectedValue != "")
                {
                    if (ddlProcess.SelectedValue != "< All >")
                    {
                        pid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (ddlSubProcess.SelectedValue != "")
                {
                    if (ddlSubProcess.SelectedValue != "< All >")
                    {
                        psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }

                if (ActivityDescription != "" || ActivityDescription != null)
                {
                    if (controlObjective != "" || controlObjective != null)
                    {
                        RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                        {
                            ActivityDescription = ActivityDescription,
                            ControlObjective = controlObjective,
                            ProcessId = pid,
                            SubProcessId = psid,
                            IsInternalAudit = "N",
                            CustomerId = customerID,
                            CustomerBranchId=Convert.ToInt32(ClientProcessFooterTemplate),
                        };
                        if (RiskCategoryManagement.Exists(riskcategorycreation))
                        {
                            cvDuplicateEntry.ErrorMessage = "Risk Category already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            entities.RiskCategoryCreations.Add(riskcategorycreation);
                            entities.SaveChanges();
                        }

                        if (RiskIdProcess.Count > 0)
                        {
                            foreach (var aItem in RiskIdProcess)
                            {
                                RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                {
                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                    RiskCategoryId = Convert.ToInt32(aItem),
                                    ProcessId = pid,
                                    SubProcessId = psid,

                                };
                                RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                            }
                            RiskIdProcess.Clear();
                        }
                        //if (ClientIdProcess.Count > 0)
                        //{
                        //    foreach (var aItem in ClientIdProcess)
                        //    {
                        //        CustomerBranchMapping CustomerBranchMapping = new CustomerBranchMapping()
                        //        {
                        //            RiskCategoryCreationId = riskcategorycreation.Id,
                        //            BranchId = Convert.ToInt32(aItem),
                        //            ProcessId = pid,
                        //            SubProcessId = psid,
                        //        };
                        //        RiskCategoryManagement.CreateCustomerBranchMapping(CustomerBranchMapping);
                        //    }
                        //    ClientIdProcess.Clear();
                        //}
                        if (AssertionsIdProcess.Count > 0)
                        {
                            foreach (var aItem in AssertionsIdProcess)
                            {
                                AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                {
                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                    AssertionId = Convert.ToInt32(aItem),
                                    ProcessId = pid,
                                    SubProcessId = psid,
                                };
                                RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                            }
                            AssertionsIdProcess.Clear();
                        }
                        RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                        {
                            ControlDescription = ExistingControlDescriptionFooterTemplate,
                            MControlDescription = MControlDescriptionFooterTemplate,
                            ProcessId = pid,
                            SubProcessId = psid,
                            RiskCreationId = riskcategorycreation.Id,
                            //Assertion =Convert.ToInt32(AssertionsFooterTemplate),
                            PersonResponsible = Convert.ToInt32(PersonResponsibleFooterTemplate),
                            Key_Value = Convert.ToInt32(KeyFooterTemplate),
                            AutomatedControl = Convert.ToInt32(AutomatedFooterTemplate),
                            Frequency = Convert.ToInt32(FrequencyFooterTemplate),
                            PrevationControl = Convert.ToInt32(PreventiveFooterTemplate),
                            EffectiveDate = dt.Date,
                            IsDeleted = false,
                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                            GapDescription = GapDescriptionFooterTemplate,
                            Recommendations = RecommendationsFooterTemplate,
                            ActionRemediationplan = ActionRemediationplanFooterTemplate,
                            IPE = IPEFooterTemplate,
                            ERPsystem = ERPsystemFooterTemplate,
                            FRC = FRCFooterTemplate,
                            UniqueReferred = UniqueReferredFooterTemplate,
                            RiskRating = Convert.ToInt32(ddlRiskFooterTemplate),
                            ControlRating = Convert.ToInt32(ddlControlFooterTemplate),

                          
                        };
                        if (RiskCategoryManagement.RiskActivityTransactionExists(riskactivitytransaction))
                        {
                            cvDuplicateEntry.ErrorMessage = "Risk Transaction already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            entities.RiskActivityTransactions.Add(riskactivitytransaction);
                            entities.SaveChanges();
                        }

                    }
                }


                this.BindData("P", "A");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void checkBoxesClientProcess_SelcetedIndexChanged(object sender, EventArgs e)
        //{
        //    if (grdRiskActivityMatrix.Rows.Count >= 0)
        //    {
        //        Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)sender;
        //        ClientIdProcess.Clear();
        //        for (int i = 0; i < ddlClientProcess.Items.Count; i++)
        //        {
        //            if (ddlClientProcess.Items[i].Selected)
        //            {
        //                ClientIdProcess.Add(Convert.ToInt32(ddlClientProcess.Items[i].Value));
        //            }
        //        }
        //    }
        //}

        protected void Edit(object sender, GridViewEditEventArgs e)
        {


          

            Label lblPersonResponsible = (Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlPersonResponsibleItemTemplate");
            ViewState["PersonResponsible"] = lblPersonResponsible.Text;
            ViewState["Key"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlKeyItemTemplate")).Text;
            ViewState["PreventiveControl"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlPreventiveItemTemplate")).Text;
            ViewState["AutomatedControl"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlAutomatedItemTemplate")).Text;
            ViewState["Frequency"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlFrequencytemTemplate")).Text;

            ViewState["CustomerBranchName"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("lblClient")).Text;
            ViewState["Risk"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlRiskItemTemplate")).Text;
            ViewState["Control"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("ddlControlItemTemplate")).Text;

            grdRiskActivityMatrix.EditIndex = e.NewEditIndex;
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {

                if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                    && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                {
                    BindData("P", "L");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                && ddlFilterIndustry.SelectedItem.Text != "< All >")
                {
                    BindData("P", "I");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                {
                    BindData("P", "R");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >")
                {
                    BindData("P", "S");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                {
                    BindData("P", "P");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                {
                    BindData("P", "B");
                }
                else
                {
                    BindData("P", "A");
                }
            }
            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            {
                if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
               && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                   && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                {
                    BindData("N", "L");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                && ddlFilterIndustry.SelectedItem.Text != "< All >")
                {
                    BindData("N", "I");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                {
                    BindData("N", "R");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >")
                {
                    BindData("N", "S");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                {
                    BindData("N", "P");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                {
                    BindData("N", "B");
                }
                else
                {
                    BindData("N", "A");
                }
            }


        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdRiskActivityMatrix.EditIndex = -1;
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {

                if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                    && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                {
                    BindData("P", "L");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                && ddlFilterIndustry.SelectedItem.Text != "< All >")
                {
                    BindData("P", "I");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                {
                    BindData("P", "R");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >")
                {
                    BindData("P", "S");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                {
                    BindData("P", "P");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                {
                    BindData("P", "B");
                }
                else
                {
                    BindData("P", "A");
                }
            }
            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            {
                if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
               && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                   && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                {
                    BindData("N", "L");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                && ddlFilterIndustry.SelectedItem.Text != "< All >")
                {
                    BindData("N", "I");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                {
                    BindData("N", "R");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                && ddlSubProcess.SelectedItem.Text != "< All >")
                {
                    BindData("N", "S");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                {
                    BindData("N", "P");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                {
                    BindData("N", "B");
                }
                else
                {
                    BindData("N", "A");
                }
            }
            //if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            //    BindData("P", "A");
            //else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            //    BindData("N", "A");
            //BindData();
        }


        protected void UpdateRisk(object sender, GridViewUpdateEventArgs e)
        {
            long pid = -1;
            long psid = -1;

            try
            {
                if (ddlProcess.SelectedValue != "-1" && ddlSubProcess.SelectedValue != "-1")
                {
                    if (ddlProcess.SelectedItem.Text != "< All >")
                    {
                        pid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }

                    if (ddlSubProcess.SelectedItem.Text != "< All >")
                    {
                        psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }

                    int customerID = -1;
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    Label lbllbltaskidItemTemplate = ((Label)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("lbltaskidItemTemplate"));    
                    long RiskCreationId = Convert.ToInt32(lbllbltaskidItemTemplate.Text);
                    Label lblRiskActivityIDItemTemplate = ((Label)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("lblRiskActivityIDItemTemplate"));
                    int riskactivityid = Convert.ToInt32(lblRiskActivityIDItemTemplate.Text);
                    RiskCategoryCreation RiskData = new RiskCategoryCreation()
                    {
                        Id = RiskCreationId,
                        ActivityDescription = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtRiskActivityDescriptionEdit")).Text,
                        ControlObjective = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtControlObjectiveEdit")).Text,                       
                    };

                    RiskCategoryManagement.RiskUpdateRCM(RiskData);
                    RiskCategoryManagement.UpdateRiskCategoryMapping(RiskCreationId);
                    RiskCategoryManagement.UpdateAssertionsMapping(RiskCreationId);
                   // RiskCategoryManagement.UpdateCustomerBranchMapping(RiskCreationId);

                    if (grdRiskActivityMatrix.Rows.Count >= 0)
                    {
                        //Saplin.Controls.DropDownCheckBoxes ddlClientProcessEdit = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlClientProcessEdit");
                        //ClientIdProcess.Clear();
                        //for (int i = 0; i < ddlClientProcessEdit.Items.Count; i++)
                        //{
                        //    if (ddlClientProcessEdit.Items[i].Selected)
                        //    {
                        //        ClientIdProcess.Add(Convert.ToInt32(ddlClientProcessEdit.Items[i].Value));
                        //    }
                        //}

                        Saplin.Controls.DropDownCheckBoxes ddlRiskCategoryProcessEdit = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlRiskCategoryProcessEdit");
                        RiskIdProcess.Clear();
                        for (int i = 0; i < ddlRiskCategoryProcessEdit.Items.Count; i++)
                        {
                            if (ddlRiskCategoryProcessEdit.Items[i].Selected)
                            {
                                RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategoryProcessEdit.Items[i].Value));
                            }
                        }

                        Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcessEdit = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlAssertionsProcessEdit");
                        AssertionsIdProcess.Clear();
                        for (int i = 0; i < ddlAssertionsProcessEdit.Items.Count; i++)
                        {
                            if (ddlAssertionsProcessEdit.Items[i].Selected)
                            {
                                AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertionsProcessEdit.Items[i].Value));
                            }
                        }
                    }


                    if (RiskIdProcess.Count > 0)
                    {
                        foreach (var aItem in RiskIdProcess)
                        {
                            if (RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                            {
                                RiskCategoryManagement.EditRiskCategoryMapping(aItem, RiskCreationId);
                            }
                            else
                            {
                                if (pid != -1 && psid != -1)
                                {
                                    RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                    {
                                        RiskCategoryCreationId = RiskCreationId,
                                        RiskCategoryId = Convert.ToInt32(aItem),
                                        ProcessId = pid,
                                        SubProcessId = psid,

                                    };
                                    RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                }

                            }
                        }
                        RiskIdProcess.Clear();
                    }

                    //if (ClientIdProcess.Count > 0)
                    //{
                    //    foreach (var aItem in ClientIdProcess)
                    //    {
                    //        if (RiskCategoryManagement.CustomerBranchMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                    //        {
                    //            RiskCategoryManagement.EditCustomerBranchMapping(aItem, RiskCreationId);
                    //        }
                    //        else
                    //        {
                    //            if (pid != -1 && psid != -1)
                    //            {
                    //                CustomerBranchMapping CustomerBranchMapping = new CustomerBranchMapping()
                    //                {
                    //                    RiskCategoryCreationId = RiskCreationId,
                    //                    BranchId = Convert.ToInt32(aItem),
                    //                    ProcessId = pid,
                    //                    SubProcessId = psid,
                    //                };
                    //                RiskCategoryManagement.CreateCustomerBranchMapping(CustomerBranchMapping);
                    //            }
                    //        }
                    //    }
                    //    ClientIdProcess.Clear();
                    //}

                    if (AssertionsIdProcess.Count > 0)
                    {
                        foreach (var aItem in AssertionsIdProcess)
                        {
                            if (RiskCategoryManagement.AssertionMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                            {
                                RiskCategoryManagement.EditAssertionsMapping(aItem, RiskCreationId);
                            }
                            else
                            {
                                if (pid != -1 && psid != -1)
                                {
                                    AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                    {
                                        RiskCategoryCreationId = RiskCreationId,
                                        AssertionId = Convert.ToInt32(aItem),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                    };
                                    RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                }
                            }
                        }
                        AssertionsIdProcess.Clear();
                    }

                    DateTime dt = new DateTime();
                    TextBox txtEffectiveDateEdit = (TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtEffectiveDateEdit");

                    if (txtEffectiveDateEdit != null)
                    {
                        //dt = GetDate(txtEffectiveDateEdit.Text);
                    }

                    RiskActivityTransaction RiskActivityTxnData = new RiskActivityTransaction()
                    {
                        Id=riskactivityid,
                        ControlDescription = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtExistingControlDescriptionEdit")).Text,
                        MControlDescription = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtMControlDescriptionEdit")).Text,
                        ProcessId = pid,
                        SubProcessId = psid,
                        RiskCreationId = RiskCreationId,
                        PersonResponsible = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlPersonResponsibleEdit")).SelectedValue),
                        Key_Value = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlKeyEdit")).SelectedValue),
                        AutomatedControl = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlAutomatedEdit")).SelectedValue),
                        Frequency = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlFrequencyEdit")).SelectedValue),
                        PrevationControl = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlPreventiveEdit")).SelectedValue),
                        //EffectiveDate = dt.Date,
                        IsDeleted = false,
                        GapDescription = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtGapDescriptionEdit")).Text,
                        Recommendations = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtRecommendationsEdit")).Text,
                        ActionRemediationplan = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtActionRemediationplanEdit")).Text,
                        IPE = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtIPEItemEdit")).Text,
                        ERPsystem = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtERPsystemEdit")).Text,
                        FRC = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtFRCEdit")).Text,
                        UniqueReferred = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtUniqueReferredEdit")).Text,
                        RiskRating = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlRiskEdit")).SelectedValue),
                        ControlRating = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlControlEdit")).SelectedValue),
                        //TestStrategy = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtTestStrategyEdit")).Text,
                        //DocumentsExamined = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtDocumentsExaminedEdit")).Text,

                        CustomerBranchId = Convert.ToInt32(((DropDownList)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlClientProcessEdit")).SelectedValue),
                    };

                    RiskCategoryManagement.RiskActivityTxnUpdate(RiskActivityTxnData);

                    grdRiskActivityMatrix.EditIndex = -1;
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {

                        if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                            && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                        {
                            BindData("P", "L");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                        && ddlFilterIndustry.SelectedItem.Text != "< All >")
                        {
                            BindData("P", "I");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                        {
                            BindData("P", "R");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >")
                        {
                            BindData("P", "S");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                        {
                            BindData("P", "P");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                        {
                            BindData("P", "B");
                        }
                        else
                        {
                            BindData("P", "A");
                        }
                    }
                    else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
                    {
                        if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                       && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                           && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                        {
                            BindData("N", "L");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                        && ddlFilterIndustry.SelectedItem.Text != "< All >")
                        {
                            BindData("N", "I");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                        {
                            BindData("N", "R");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                        && ddlSubProcess.SelectedItem.Text != "< All >")
                        {
                            BindData("N", "S");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                        {
                            BindData("N", "P");
                        }
                        else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                        {
                            BindData("N", "B");
                        }
                        else
                        {
                            BindData("N", "A");
                        }
                    }
                    //if (rdRiskActivityProcess.SelectedItem.Text == "Proces")
                    //    this.BindData("P","A");
                    //else
                    //    this.BindData("N","A");
                }
                else
                {
                    rfvProcess.IsValid = false;
                    rfvSubProcess.IsValid = false;
                    cvDuplicateEntry.IsValid = false;                    
                }
            }


            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void checkBoxesAssertionsProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            if (grdRiskActivityMatrix.Rows.Count >= 0)
            {
                Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)sender;
                AssertionsIdProcess.Clear();
                for (int i = 0; i < ddlAssertionsProcess.Items.Count; i++)
                {
                    if (ddlAssertionsProcess.Items[i].Selected)
                    {
                        AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertionsProcess.Items[i].Value));
                    }
                }
            }
        }
        protected void checkBoxesRiskCategoryProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            if (grdRiskActivityMatrix.Rows.Count >= 0)
            {
                Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)sender;
                RiskIdProcess.Clear();
                for (int i = 0; i < DrpRiskCategoryProcess.Items.Count; i++)
                {
                    if (DrpRiskCategoryProcess.Items[i].Selected)
                    {
                        RiskIdProcess.Add(Convert.ToInt32(DrpRiskCategoryProcess.Items[i].Value));
                    }
                }
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        private void BindBranchesHierarchy(TreeNode parent, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DropDownList ddlRiskCategory = (e.Row.FindControl("ddlRiskCategory") as DropDownList);
                if (ddlRiskCategory != null)
                {
                    ddlRiskCategory.DataTextField = "Name";
                    ddlRiskCategory.DataValueField = "ID";
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        ddlRiskCategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("P");
                    }
                    else
                    {
                        ddlRiskCategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("N");
                    }
                    ddlRiskCategory.DataBind();
                    ddlRiskCategory.Items.Insert(0, new ListItem("< Select RiskCategory >", "-1"));
                }
                DropDownList ddlAssertionsItemTemplate = (e.Row.FindControl("ddlAssertionsItemTemplate") as DropDownList);
                if (ddlAssertionsItemTemplate != null)
                {
                    ddlAssertionsItemTemplate.DataTextField = "Name";
                    ddlAssertionsItemTemplate.DataValueField = "ID";
                    ddlAssertionsItemTemplate.DataSource = RiskCategoryManagement.FillAssertions();
                    ddlAssertionsItemTemplate.DataBind();
                    ddlAssertionsItemTemplate.Items.Insert(0, new ListItem("< Select Assertions >", "-1"));
                }

                DropDownList ddlAssertionsFooterTemplate = (e.Row.FindControl("ddlAssertionsFooterTemplate") as DropDownList);
                if (ddlAssertionsFooterTemplate != null)
                {
                    ddlAssertionsFooterTemplate.DataTextField = "Name";
                    ddlAssertionsFooterTemplate.DataValueField = "ID";
                    ddlAssertionsFooterTemplate.DataSource = RiskCategoryManagement.FillAssertions();
                    ddlAssertionsFooterTemplate.DataBind();
                    ddlAssertionsFooterTemplate.Items.Insert(0, new ListItem("< Select Assertions >", "-1"));
                }
                DropDownList ddlPersonResponsibleItemTemplate = (e.Row.FindControl("ddlPersonResponsibleItemTemplate") as DropDownList);
                if (ddlPersonResponsibleItemTemplate != null)
                {
                    int customerID = -1;
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlPersonResponsibleItemTemplate.DataTextField = "Name";
                    ddlPersonResponsibleItemTemplate.DataValueField = "ID";
                    ddlPersonResponsibleItemTemplate.DataSource = RiskCategoryManagement.FillUsers(customerID);
                    ddlPersonResponsibleItemTemplate.DataBind();
                    ddlPersonResponsibleItemTemplate.Items.Insert(0, new ListItem("< Select User >", "-1"));
                }
                DropDownList ddlPersonResponsibleFooterTemplate = (e.Row.FindControl("ddlPersonResponsibleFooterTemplate") as DropDownList);
                if (ddlPersonResponsibleFooterTemplate != null)
                {
                    int customerID = -1;
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlPersonResponsibleFooterTemplate.DataTextField = "Name";
                    ddlPersonResponsibleFooterTemplate.DataValueField = "ID";
                    ddlPersonResponsibleFooterTemplate.DataSource = RiskCategoryManagement.FillUsers(customerID);
                    ddlPersonResponsibleFooterTemplate.DataBind();
                    ddlPersonResponsibleFooterTemplate.Items.Insert(0, new ListItem("< Select User >", "-1"));
                }
                DropDownList ddlKeyItemTemplate = (e.Row.FindControl("ddlKeyItemTemplate") as DropDownList);
                if (ddlKeyItemTemplate != null)
                {
                    ddlKeyItemTemplate.DataTextField = "Name";
                    ddlKeyItemTemplate.DataValueField = "ID";
                    ddlKeyItemTemplate.DataSource = RiskCategoryManagement.FillKey();
                    ddlKeyItemTemplate.DataBind();
                    ddlKeyItemTemplate.Items.Insert(0, new ListItem("< Select Key >", "-1"));
                }
                DropDownList ddlKeyFooterTemplate = (e.Row.FindControl("ddlKeyFooterTemplate") as DropDownList);
                if (ddlKeyFooterTemplate != null)
                {
                    ddlKeyFooterTemplate.DataTextField = "Name";
                    ddlKeyFooterTemplate.DataValueField = "ID";
                    ddlKeyFooterTemplate.DataSource = RiskCategoryManagement.FillKey();
                    ddlKeyFooterTemplate.DataBind();
                    ddlKeyFooterTemplate.Items.Insert(0, new ListItem("< Select Key >", "-1"));
                }
                DropDownList ddlPreventiveItemTemplate = (e.Row.FindControl("ddlPreventiveItemTemplate") as DropDownList);
                if (ddlPreventiveItemTemplate != null)
                {
                    ddlPreventiveItemTemplate.DataTextField = "Name";
                    ddlPreventiveItemTemplate.DataValueField = "ID";
                    ddlPreventiveItemTemplate.DataSource = RiskCategoryManagement.FillPreventiveControl();
                    ddlPreventiveItemTemplate.DataBind();
                    ddlPreventiveItemTemplate.Items.Insert(0, new ListItem("< Select Control >", "-1"));
                }
                DropDownList ddlPreventiveFooterTemplate = (e.Row.FindControl("ddlPreventiveFooterTemplate") as DropDownList);
                if (ddlPreventiveFooterTemplate != null)
                {
                    ddlPreventiveFooterTemplate.DataTextField = "Name";
                    ddlPreventiveFooterTemplate.DataValueField = "ID";
                    ddlPreventiveFooterTemplate.DataSource = RiskCategoryManagement.FillPreventiveControl();
                    ddlPreventiveFooterTemplate.DataBind();
                    ddlPreventiveFooterTemplate.Items.Insert(0, new ListItem("< Select Control >", "-1"));
                }
                DropDownList ddlAutomatedItemTemplate = (e.Row.FindControl("ddlAutomatedItemTemplate") as DropDownList);
                if (ddlAutomatedItemTemplate != null)
                {
                    ddlAutomatedItemTemplate.DataTextField = "Name";
                    ddlAutomatedItemTemplate.DataValueField = "ID";
                    ddlAutomatedItemTemplate.DataSource = RiskCategoryManagement.FillAutomatedControl();
                    ddlAutomatedItemTemplate.DataBind();
                    ddlAutomatedItemTemplate.Items.Insert(0, new ListItem("< Select Control >", "-1"));
                }
                DropDownList ddlAutomatedFooterTemplate = (e.Row.FindControl("ddlAutomatedFooterTemplate") as DropDownList);
                if (ddlAutomatedFooterTemplate != null)
                {
                    ddlAutomatedFooterTemplate.DataTextField = "Name";
                    ddlAutomatedFooterTemplate.DataValueField = "ID";
                    ddlAutomatedFooterTemplate.DataSource = RiskCategoryManagement.FillAutomatedControl();
                    ddlAutomatedFooterTemplate.DataBind();
                    ddlAutomatedFooterTemplate.Items.Insert(0, new ListItem("< Select Control >", "-1"));
                }
                TextBox txtEffectiveDateItemTemplate = (e.Row.FindControl("txtEffectiveDateFooterTemplate") as TextBox);
                if (txtEffectiveDateItemTemplate != null)
                {
                    try
                    {                        
                        DateTime date = DateTime.Now;                      
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }

                    // ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', null);", txtEffectiveDateItemTemplate.ClientID), true);
                }
                DropDownList ddlFrequencytemTemplate = (e.Row.FindControl("ddlFrequencytemTemplate") as DropDownList);
                if (ddlFrequencytemTemplate != null)
                {
                    ddlFrequencytemTemplate.DataTextField = "Name";
                    ddlFrequencytemTemplate.DataValueField = "ID";
                    ddlFrequencytemTemplate.DataSource = RiskCategoryManagement.FillFrequency();
                    ddlFrequencytemTemplate.DataBind();
                    ddlFrequencytemTemplate.Items.Insert(0, new ListItem("< Select Frequency >", "-1"));
                }
                DropDownList ddlFrequencyFooterTemplate = (e.Row.FindControl("ddlFrequencyFooterTemplate") as DropDownList);
                if (ddlFrequencyFooterTemplate != null)
                {
                    ddlFrequencyFooterTemplate.DataTextField = "Name";
                    ddlFrequencyFooterTemplate.DataValueField = "ID";
                    ddlFrequencyFooterTemplate.DataSource = RiskCategoryManagement.FillFrequency();
                    ddlFrequencyFooterTemplate.DataBind();
                    ddlFrequencyFooterTemplate.Items.Insert(0, new ListItem("< Select Frequency >", "-1"));
                }

                DropDownList ddlRiskFooterTemplate = (e.Row.FindControl("ddlRiskFooterTemplate") as DropDownList);
                if (ddlRiskFooterTemplate != null)
                {
                    ddlRiskFooterTemplate.DataTextField = "Name";
                    ddlRiskFooterTemplate.DataValueField = "ID";
                    ddlRiskFooterTemplate.DataSource = RiskCategoryManagement.FillRiskRating();
                    ddlRiskFooterTemplate.DataBind();
                    ddlRiskFooterTemplate.Items.Insert(0, new ListItem("< Select Risk Rating >", "-1"));
                }

                DropDownList ddlControlFooterTemplate = (e.Row.FindControl("ddlControlFooterTemplate") as DropDownList);
                if (ddlControlFooterTemplate != null)
                {
                    ddlControlFooterTemplate.DataTextField = "Name";
                    ddlControlFooterTemplate.DataValueField = "ID";
                    ddlControlFooterTemplate.DataSource = RiskCategoryManagement.FillControlRating();
                    ddlControlFooterTemplate.DataBind();
                    ddlControlFooterTemplate.Items.Insert(0, new ListItem("< Select Control Rating >", "-1"));
                }

                DropDownList ddlClientProcessFooterTemplate = (e.Row.FindControl("ddlClientProcessFooterTemplate") as DropDownList);
                if (ddlClientProcessFooterTemplate != null)
                {
                    int customerID = -1;
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlClientProcessFooterTemplate.DataTextField = "Name";
                    ddlClientProcessFooterTemplate.DataValueField = "ID";
                    ddlClientProcessFooterTemplate.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                    ddlClientProcessFooterTemplate.DataBind();
                    ddlClientProcessFooterTemplate.Items.Insert(0, new ListItem("< Select Branch >", "-1"));
                  
                }
                //Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (e.Row.FindControl("ddlClientProcess") as DropDownCheckBoxes);
                //if (ddlClientProcess != null)
                //{
                //    int customerID = -1;
                //    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                //    this.countrybindClient(ddlClientProcess, customerID);
                //}
                Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (e.Row.FindControl("ddlRiskCategoryProcess") as DropDownCheckBoxes);
                if (DrpRiskCategoryProcess != null)
                {
                    this.countrybindRisk(DrpRiskCategoryProcess, "P");
                }
                Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (e.Row.FindControl("ddlAssertionsProcess") as DropDownCheckBoxes);
                if (ddlAssertionsProcess != null)
                {
                    this.countrybindAssertions(ddlAssertionsProcess);
                }


                if (e.Row.RowType == DataControlRowType.DataRow && grdRiskActivityMatrix.EditIndex == e.Row.RowIndex)
                {
                    Label lbllbltaskidItemTemplate = (e.Row.FindControl("lbltaskidItemTemplate") as Label);
                    long RiskID = Convert.ToInt32(lbllbltaskidItemTemplate.Text);//Convert.ToInt64(grdRiskActivityMatrix.DataKeys[e.Row.RowIndex].Values[0]);

                    DropDownCheckBoxes ddlRiskCategoryProcessEdit = (e.Row.FindControl("ddlRiskCategoryProcessEdit") as DropDownCheckBoxes);
                   // DropDownCheckBoxes ddlClientProcessEdit = (e.Row.FindControl("ddlClientProcessEdit") as DropDownCheckBoxes);
                    DropDownCheckBoxes ddlAssertionsProcessEdit = (e.Row.FindControl("ddlAssertionsProcessEdit") as DropDownCheckBoxes);
                    DropDownCheckBoxes DropDownCheckBoxesIndustryProcessEdit = (e.Row.FindControl("DropDownCheckBoxesProcessEdit") as DropDownCheckBoxes);


                    //if (ddlClientProcessEdit != null)
                    //{
                    //    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", ddlClientProcess.ClientID), true);

                    //    int customerID = -1;
                    //    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    //    this.countrybindClient(ddlClientProcessEdit, customerID);
                    //}
                    //SplitString(ShowCustomerBranchName(RiskID), ddlClientProcessEdit);


                    if (ddlRiskCategoryProcessEdit != null)
                    {
                        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationProcess');", DrpRiskCategoryProcess.ClientID), true);
                        this.countrybindRisk(ddlRiskCategoryProcessEdit, "P");
                    }
                    SplitString(ShowRiskCategoryName(RiskID), ddlRiskCategoryProcessEdit);


                    if (ddlAssertionsProcessEdit != null)
                    {
                        this.countrybindAssertions(ddlAssertionsProcessEdit);
                    }
                    SplitString(ShowAssertionsName(RiskID), ddlAssertionsProcessEdit);

                    DropDownList ddlPersonResponsibleEdit = (e.Row.FindControl("ddlPersonResponsibleEdit") as DropDownList);
                    if (ddlPersonResponsibleEdit != null)
                    {
                        int customerID = -1;
                        customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        ddlPersonResponsibleEdit.DataTextField = "Name";
                        ddlPersonResponsibleEdit.DataValueField = "ID";
                        ddlPersonResponsibleEdit.DataSource = RiskCategoryManagement.FillUsers(customerID);
                        ddlPersonResponsibleEdit.DataBind();
                        ddlPersonResponsibleEdit.Items.Insert(0, new ListItem("< Select User >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["PersonResponsible"])))
                        {
                            ddlPersonResponsibleEdit.Items.FindByText(ViewState["PersonResponsible"].ToString().Trim()).Selected = true;
                        }

                       
                    }

                    DropDownList ddlClientProcessEdit = (e.Row.FindControl("ddlClientProcessEdit") as DropDownList);
                    if (ddlClientProcessEdit != null)
                    {
                        int customerID = -1;
                        customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        ddlClientProcessEdit.DataTextField = "Name";
                        ddlClientProcessEdit.DataValueField = "ID";
                        ddlClientProcessEdit.DataSource =UserManagementRisk.FillCustomerNew(customerID);
                        ddlClientProcessEdit.DataBind();
                        ddlClientProcessEdit.Items.Insert(0, new ListItem("< Select Branch >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBranchName"])))
                        {
                            ddlClientProcessEdit.Items.FindByText(ViewState["CustomerBranchName"].ToString().Trim()).Selected = true;
                        }
                    }

                    DropDownList ddlKeyEdit = (e.Row.FindControl("ddlKeyEdit") as DropDownList);
                    if (ddlKeyEdit != null)
                    {
                        ddlKeyEdit.DataTextField = "Name";
                        ddlKeyEdit.DataValueField = "ID";
                        ddlKeyEdit.DataSource = RiskCategoryManagement.FillKey();
                        ddlKeyEdit.DataBind();
                        ddlKeyEdit.Items.Insert(0, new ListItem("< Select Key >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Key"])))
                        {
                            ddlKeyEdit.Items.FindByText(ViewState["Key"].ToString().Trim()).Selected = true;
                        }
                    }

                    DropDownList ddlPreventiveEdit = (e.Row.FindControl("ddlPreventiveEdit") as DropDownList);
                    if (ddlPreventiveEdit != null)
                    {
                        ddlPreventiveEdit.DataTextField = "Name";
                        ddlPreventiveEdit.DataValueField = "ID";
                        ddlPreventiveEdit.DataSource = RiskCategoryManagement.FillPreventiveControl();
                        ddlPreventiveEdit.DataBind();
                        ddlPreventiveEdit.Items.Insert(0, new ListItem("< Select Control >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["PreventiveControl"])))
                        {
                            ddlPreventiveEdit.Items.FindByText(ViewState["PreventiveControl"].ToString().Trim()).Selected = true;
                        }
                    }

                    DropDownList ddlAutomatedEdit = (e.Row.FindControl("ddlAutomatedEdit") as DropDownList);
                    if (ddlAutomatedEdit != null)
                    {
                        ddlAutomatedEdit.DataTextField = "Name";
                        ddlAutomatedEdit.DataValueField = "ID";
                        ddlAutomatedEdit.DataSource = RiskCategoryManagement.FillAutomatedControl();
                        ddlAutomatedEdit.DataBind();
                        ddlAutomatedEdit.Items.Insert(0, new ListItem("< Select Control >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AutomatedControl"])))
                        {
                            ddlAutomatedEdit.Items.FindByText(ViewState["AutomatedControl"].ToString().Trim()).Selected = true;
                        }
                    }

                    DropDownList ddlFrequencyEdit = (e.Row.FindControl("ddlFrequencyEdit") as DropDownList);
                    if (ddlFrequencyEdit != null)
                    {
                        ddlFrequencyEdit.DataTextField = "Name";
                        ddlFrequencyEdit.DataValueField = "ID";
                        ddlFrequencyEdit.DataSource = RiskCategoryManagement.FillFrequency();
                        ddlFrequencyEdit.DataBind();
                        ddlFrequencyEdit.Items.Insert(0, new ListItem("< Select Frequency >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Frequency"])))
                        {
                            ddlFrequencyEdit.Items.FindByText(ViewState["Frequency"].ToString().Trim()).Selected = true;
                        }
                    }

                  
                    DropDownList ddlRiskEdit = (e.Row.FindControl("ddlRiskEdit") as DropDownList);
                    if (ddlRiskEdit != null)
                    {
                        ddlRiskEdit.Items.Clear();
                        ddlRiskEdit.DataTextField = "Name";
                        ddlRiskEdit.DataValueField = "ID";
                        ddlRiskEdit.DataSource = RiskCategoryManagement.FillRiskRating();
                        ddlRiskEdit.DataBind();
                        ddlRiskEdit.Items.Insert(0, new ListItem("< Select Risk Rating >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Risk"])))
                        {
                            ddlRiskEdit.Items.FindByText(ViewState["Risk"].ToString().Trim()).Selected = true;
                        }                        
                    }

                    DropDownList ddlControlEdit = (e.Row.FindControl("ddlControlEdit") as DropDownList);
                    if (ddlControlEdit != null)
                    {
                        ddlControlEdit.Items.Clear();
                        ddlControlEdit.DataTextField = "Name";
                        ddlControlEdit.DataValueField = "ID";
                        ddlControlEdit.DataSource = RiskCategoryManagement.FillControlRating();
                        ddlControlEdit.DataBind();
                        ddlControlEdit.Items.Insert(0, new ListItem("< Select Control Rating >", "-1"));
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Control"])))
                        {
                            ddlControlEdit.Items.FindByText(ViewState["Control"].ToString().Trim()).Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void setDateToGridView()
        {
            try
            {
              
                DateTime date = DateTime.Now;
                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowCustomerBranchName(long categoryid)
        {
            List<CustomerBranchName_Result> a = new List<CustomerBranchName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        public void SplitString(string StrToSplit, Saplin.Controls.DropDownCheckBoxes DrpToFill)
        {
            string[] commandArgs = StrToSplit.Split(new char[] { ',' });
            string a = string.Empty;
            if (commandArgs.Length > 0)
            {
                for (int i = 0; i < commandArgs.Length; i++)
                {
                    a = commandArgs[i];
                    if (!string.IsNullOrEmpty(a))
                    {
                        DrpToFill.Items.FindByText(a.Trim()).Selected = true;
                    }
                }
            }
        }

        private void countrybindAssertions(Saplin.Controls.DropDownCheckBoxes DrpAssertions)
        {
            DrpAssertions.DataTextField = "Name";
            DrpAssertions.DataValueField = "ID";
            DrpAssertions.DataSource = RiskCategoryManagement.FillAssertions();
            DrpAssertions.DataBind();
        }
        private void countrybindRisk(Saplin.Controls.DropDownCheckBoxes Drpriskcategory, string Flag)
        {
            if (Flag == "P")
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("P");
                Drpriskcategory.DataBind();
            }
            else
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("N");
                Drpriskcategory.DataBind();
            }
        }
        private void countrybindClient(Saplin.Controls.DropDownCheckBoxes DrpClient, long customerid)
        {
            if (customerid != -1)
            {
                DrpClient.DataTextField = "Name";
                DrpClient.DataValueField = "ID";
                DrpClient.DataSource = RiskCategoryManagement.FillCustomerBranch(customerid);
                DrpClient.DataBind();
            }
        }
        protected void grdRiskActivityMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
        protected void grdRiskActivityMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {

                    if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                        && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "L");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                    && ddlFilterIndustry.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "I");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "R");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "S");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "P");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                    {
                        BindData("P", "B");
                    }
                    else
                    {
                        BindData("P", "A");
                    }
                }
                else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
                {
                    if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                   && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                       && ddlFilterIndustry.SelectedItem.Text != "< All >" && ddlFilterLocationType.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "L");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >"
                    && ddlFilterIndustry.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "I");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >" && ddlFilterRiskCategory.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "R");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >"
                    && ddlSubProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "S");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >" && ddlProcess.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "P");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "< All >")
                    {
                        BindData("N", "B");
                    }
                    else
                    {
                        BindData("N", "A");
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RiskCategoryTransaction.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (StringWriter sw = new StringWriter())
                {
                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                    //To Export all pages
                    GridExportExcel.AllowPaging = false;
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        this.BindDataExport("P", "A");
                    }
                    else
                    {
                        this.BindDataExport("N", "A");
                    }
                    //GridExportExcel.HeaderRow.BackColor = Color.Blue;
                    //foreach (TableCell cell in GridExportExcel.HeaderRow.Cells)
                    //{
                    //    cell.BackColor = GridExportExcel.HeaderStyle.BackColor;
                    //}
                    //GridExportExcel.RenderControl(hw);
                    if (GridExportExcel.Rows.Count > 0)
                    {
                        GridExportExcel.HeaderRow.BackColor = Color.Blue;
                        foreach (TableCell cell in GridExportExcel.HeaderRow.Cells)
                        {
                            cell.BackColor = GridExportExcel.HeaderStyle.BackColor;
                        }
                        GridExportExcel.RenderControl(hw);
                    }
                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}