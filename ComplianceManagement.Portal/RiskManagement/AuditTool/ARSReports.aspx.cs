﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class ARSReports : System.Web.UI.Page
    {
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public string linkclick;
        public List<long> Branchlist = new List<long>();
        protected string AuditHeadOrManagerReport;
        protected int CustomerId = 0;
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFinancialYear();
                linkclick = "Not Done";
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                            ShowPerformer(sender, e);
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                            ShowReviewer(sender, e);
                        }
                        else
                        {
                            PerformerFlag = true;
                            if (string.IsNullOrEmpty(linkclick))
                            {
                                linkclick = "Not Done";
                                btnNotDone_Click(sender, e);
                            }
                            else
                                BindGrid(linkclick);
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                        BindGrid(linkclick);
                    }
                }
            }
        }
        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            /*
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            */
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public void BindGrid(string buttonType)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = -1;
            int branchid = -1;
            int verticalid = -1;
            int Processid = -1;
            int SubProcessID = -1;
            string FinancialYear = string.Empty;
            string ForMonth = string.Empty;
            int RoleID = -1;
            string departmentheadFlag = string.Empty;
            if (DepartmentHead)
            {
                departmentheadFlag = "DH";
            }
            if (PerformerFlag)
                RoleID = 3;
            if (ReviewerFlag)
                RoleID = 4;
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    verticalid = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                {
                    if (ddlVerticalID.SelectedValue != "-1")
                    {
                        verticalid = Convert.ToInt32(ddlVerticalID.SelectedValue);
                    }
                }
            }

            if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    FinancialYear = ddlFinancialYear.SelectedItem.Text;
                }
            }
            if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
            {
                if (ddlPeriod.SelectedValue != "-1")
                {
                    ForMonth = ddlPeriod.SelectedItem.Text;
                }
            }
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    Processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
            {
                if (ddlSubProcess.SelectedValue != "-1")
                {
                    SubProcessID = Convert.ToInt32(ddlSubProcess.SelectedValue);
                }
            }
            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            List<AuditSummaryCountView> masterquery = new List<AuditSummaryCountView>();
            List<AuditSummaryCountView> transactionquery = new List<AuditSummaryCountView>();
            Branchlist.Clear();
            var bracnhes = GetAllHierarchy(CustomerId, branchid);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                masterquery = (from row in entities.AuditSummaryCountViews
                               where row.CustomerID == CustomerId
                               select row).ToList();

                if (departmentheadFlag == "DH")
                {
                    #region Department Head

                    transactionquery = (from row in masterquery
                                        join ICAA in entities.InternalControlAuditAssignments
                                        on row.AuditID equals ICAA.AuditID
                                        join EAAMR in entities.EntitiesAssignmentDepartmentHeads
                                        on row.CustomerBranchID equals EAAMR.BranchID
                                        join MSP in entities.Mst_Process
                                        on ICAA.ProcessId equals MSP.Id
                                        where EAAMR.DepartmentID == MSP.DepartmentID
                                        && row.CustomerID == CustomerId
                                        && EAAMR.UserID == UserID
                                        && EAAMR.ISACTIVE == true
                                        && row.RoleID == 4
                                        select row).Distinct().ToList();
                    #endregion
                }
                else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
                {
                    #region Management   
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentManagementRisks
                                         on row.CustomerBranchID equals EAAR.BranchID
                                        where EAAR.ProcessId == row.ProcessId
                                        && row.CustomerID == CustomerId
                                        && EAAR.UserID == UserID
                                        && EAAR.ISACTIVE == true
                                        && row.RoleID == 4
                                        select row).Distinct().ToList();
                    #endregion
                }
                else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                         on row.CustomerBranchID equals EAAR.BranchID
                                        where EAAR.ProcessId == row.ProcessId
                                        && row.CustomerID == CustomerId
                                        && EAAR.UserID == UserID
                                        && EAAR.ISACTIVE == true
                                        && row.RoleID == 4
                                        select row).Distinct().ToList();
                }
                
                else
                {
                    transactionquery = (from row in masterquery
                                        join ICAA in entities.InternalControlAuditAssignments
                                        on row.CustomerBranchID equals ICAA.CustomerBranchID
                                        where ICAA.AuditID == row.AuditID
                                        && ICAA.ProcessId == row.ProcessId
                                        && row.CustomerID == CustomerId
                                        && ICAA.UserID == UserID
                                        && row.RoleID == RoleID
                                        select row).Distinct().ToList();
                }

                if (Branchlist.Count > 0)
                {
                    transactionquery = transactionquery.Where(entity => Branchlist.Contains(entity.CustomerBranchID)).ToList();
                }
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    transactionquery = transactionquery.Where(entity => entity.FinancialYear == FinancialYear).ToList();
                }
                if (!string.IsNullOrEmpty(ForMonth))
                {
                    transactionquery = transactionquery.Where(entity => entity.ForMonth == ForMonth).ToList();
                }
                if (verticalid != -1)
                {
                    transactionquery = transactionquery.Where(entity => entity.VerticalId == verticalid).ToList();
                }
                if (Processid != -1)
                {
                    transactionquery = transactionquery.Where(entity => entity.ProcessId == Processid).ToList();
                }
                if (SubProcessID != -1)
                {
                    transactionquery = transactionquery.Where(entity => entity.SubProcessId == SubProcessID).ToList();
                }
                if (transactionquery.Count > 0)
                {
                    var TotalRowcount = UserManagementRisk.GetAllARSData(buttonType, transactionquery);
                    grdARSReport.DataSource = TotalRowcount;
                    Session["TotalRows"] = TotalRowcount.Count();
                    grdARSReport.DataBind();
                    bindPageNumber();
                }
                else
                {
                    grdARSReport.DataSource = null;
                    grdARSReport.DataBind();
                }
            }
        }

        public void BindProcess(int Branchid)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlProcess.Items.Clear();
            ddlProcess.DataTextField = "Name";
            ddlProcess.DataValueField = "Id";
            if (AuditHeadOrManagerReport == null)
            {
                AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            }
            if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
            }
            else
            {
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownPerformerAndReviewer(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
            }
        }

        public void BindSubProcess(int DDLProcessId)
        {
            ddlSubProcess.Items.Clear();
            ddlSubProcess.DataTextField = "Name";
            ddlSubProcess.DataValueField = "Id";
            ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(DDLProcessId);
            ddlSubProcess.DataBind();
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            // var legalentitydata = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (DepartmentHead)
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
             
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }
        public void BindVerticalID(int? BranchId)
        {
            if (BranchId != null)
            {
                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdARSReport.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid(linkclick);
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdARSReport.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if(AuditHeadOrManagerReport == null)
            {
                AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            }
            else if(AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {

                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindVerticalID(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindProcess(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindSchedulingType();
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlSubEntity4.Items.Count > 0)
                    ddlSubEntity4.Items.Clear();
            }
            BindGrid(linkclick);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindVerticalID(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindProcess(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindSchedulingType();
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlSubEntity4.Items.Count > 0)
                    ddlSubEntity4.ClearSelection();
            }
            BindGrid(linkclick);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindVerticalID(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindProcess(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindSchedulingType();
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlSubEntity4.Items.Count > 0)
                    ddlSubEntity4.ClearSelection();
            }
            BindGrid(linkclick);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindVerticalID(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindProcess(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindSchedulingType();
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity4.Items.Count > 0)
                    ddlSubEntity4.ClearSelection();
            }
            BindGrid(linkclick);
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindVerticalID(Convert.ToInt32(ddlSubEntity4.SelectedValue));
            BindProcess(Convert.ToInt32(ddlSubEntity4.SelectedValue));
            BindSchedulingType();
            BindGrid(linkclick);
        }

        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid(linkclick);
        }
        public static string GetUserName(int InstanceID, long processid, long branchid, int UserId, int Roleid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.InternalControlAuditAssignments
                                         join mu in entities.mst_User on row.UserID equals mu.ID
                                         where row.InternalAuditInstance == InstanceID
                                         && row.RoleID == Roleid
                                         && row.ProcessId == processid
                                         && row.CustomerBranchID == branchid
                                         select mu.FirstName + " " + mu.LastName).FirstOrDefault();

                return transactionsQuery;
            }
        }
        protected string GetReviewer(int InstanceID, long processid, long branchid, int Userid)
        {
            try
            {
                string result = "";
                result = GetUserName(InstanceID, processid, branchid, Userid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetPerformer(int InstanceID, long processid, long branchid, int Userid)
        {
            try
            {
                string result = "";
                result = GetUserName(InstanceID, processid, branchid, Userid, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid(linkclick);
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        String FileName = String.Empty;
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Audit Status Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((grdARSReport.DataSource as List<AuditSummaryCountView>).ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "ControlNo", "Branch", "VerticalName", "ProcessName", "SubProcessName", "FinancialYear", "ForMonth", "ActivityToBeDone", "ActivityDescription", "ControlDescription", "Status", "CustomerBranchid", "InstanceID", "ProcessId", "UserID");
                        ExcelData.Columns.Add("Performer");
                        ExcelData.Columns.Add("Reviewer");
                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["Performer"] = GetPerformer(Convert.ToInt32(item["InstanceID"].ToString()), Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["CustomerBranchid"].ToString()), Convert.ToInt32(item["UserID"].ToString()));
                            item["Reviewer"] = GetReviewer(Convert.ToInt32(item["InstanceID"].ToString()), Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["CustomerBranchid"].ToString()), Convert.ToInt32(item["UserID"].ToString()));

                            if (item["Status"].ToString() == null || item["Status"].ToString() == "")
                            {
                                item["Status"] = "Not Done";
                            }
                        }
                        ExcelData.Columns.Remove("InstanceID");
                        ExcelData.Columns.Remove("ProcessId");
                        ExcelData.Columns.Remove("CustomerBranchid");
                        ExcelData.Columns.Remove("UserID");

                        string CustomerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CustomerId));

                        FileName = "Audit Status Summary";
                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A2"].Value = CustomerName;
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A3"].Value = FileName;
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);

                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].Value = "Control No";
                        exWorkSheet.Cells["A5"].AutoFitColumns(50);

                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].Value = "Branch";
                        exWorkSheet.Cells["B5"].AutoFitColumns(25);

                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].Value = "Vertical";
                        exWorkSheet.Cells["C5"].AutoFitColumns(25);

                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].Value = "Process";
                        exWorkSheet.Cells["D5"].AutoFitColumns(25);

                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].Value = "Sub Process";
                        exWorkSheet.Cells["E5"].AutoFitColumns(25);

                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F5"].Value = "Financial Year";
                        exWorkSheet.Cells["F5"].AutoFitColumns(25);

                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G5"].Value = "Period";
                        exWorkSheet.Cells["G5"].AutoFitColumns(25);

                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H5"].Value = "Audit Step";
                        exWorkSheet.Cells["H5"].AutoFitColumns(25);

                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I5"].Value = "Risk Description";
                        exWorkSheet.Cells["I5"].AutoFitColumns(50);

                        exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J5"].Value = "Control Description";
                        exWorkSheet.Cells["J5"].AutoFitColumns(50);

                        exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K5"].Value = "Status";
                        exWorkSheet.Cells["K5"].AutoFitColumns(20);

                        exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L5"].Value = "Performer";
                        exWorkSheet.Cells["L5"].AutoFitColumns(20);

                        exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["M5"].Value = "Reviewer";
                        exWorkSheet.Cells["M5"].AutoFitColumns(15);

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }


                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AuditStatusReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcess.SelectedValue != "-1")
            {
                BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
            }
            else
            {
                BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
            }
            BindGrid(linkclick);
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;
                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }
                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                ddlPeriod.DataSource = null;
                ddlPeriod.DataBind();
                ddlPeriod.Items.Clear();
                ddlPeriod.Items.Insert(0, "Apr-Jun");
                ddlPeriod.Items.Insert(1, "Jul-Sep");
                ddlPeriod.Items.Insert(2, "Oct-Dec");
                ddlPeriod.Items.Insert(3, "Jan-Mar");
                BindGrid(linkclick);
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid(linkclick);
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid(linkclick);
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void btnNotDone_Click(object sender, EventArgs e)
        {
            liNotDone.Attributes.Add("class", "active");
            liSubmitted.Attributes.Add("class", "");
            liTeam.Attributes.Add("class", "");
            liAuditee.Attributes.Add("class", "");
            liFinal.Attributes.Add("class", "");
            liClosed.Attributes.Add("class", "");
            linkclick = "Not Done";
            BindGrid(linkclick);
        }
        protected void btnSubmitted_Click(object sender, EventArgs e)
        {
            liNotDone.Attributes.Add("class", "");
            liSubmitted.Attributes.Add("class", "active");
            liTeam.Attributes.Add("class", "");
            liAuditee.Attributes.Add("class", "");
            liFinal.Attributes.Add("class", "");
            liClosed.Attributes.Add("class", "");
            linkclick = "Submited Count";
            BindGrid(linkclick);
        }
        protected void btnTeamReview_Click(object sender, EventArgs e)
        {
            liNotDone.Attributes.Add("class", "");
            liSubmitted.Attributes.Add("class", "");
            liTeam.Attributes.Add("class", "active");
            liAuditee.Attributes.Add("class", "");
            liFinal.Attributes.Add("class", "");
            liClosed.Attributes.Add("class", "");
            linkclick = "Team Review";
            BindGrid(linkclick);
        }
        protected void btnAuditee_Click(object sender, EventArgs e)
        {
            liNotDone.Attributes.Add("class", "");
            liSubmitted.Attributes.Add("class", "");
            liTeam.Attributes.Add("class", "");
            liAuditee.Attributes.Add("class", "active");
            liFinal.Attributes.Add("class", "");
            liClosed.Attributes.Add("class", "");
            linkclick = "Auditee Review";
            BindGrid(linkclick);
        }
        protected void btnFinalReview_Click(object sender, EventArgs e)
        {
            liNotDone.Attributes.Add("class", "");
            liSubmitted.Attributes.Add("class", "");
            liTeam.Attributes.Add("class", "");
            liAuditee.Attributes.Add("class", "");
            liFinal.Attributes.Add("class", "active");
            liClosed.Attributes.Add("class", "");
            linkclick = "Final Review";
            BindGrid(linkclick);
        }
        protected void btnClosed_Click(object sender, EventArgs e)
        {
            liNotDone.Attributes.Add("class", "");
            liSubmitted.Attributes.Add("class", "");
            liTeam.Attributes.Add("class", "");
            liAuditee.Attributes.Add("class", "");
            liFinal.Attributes.Add("class", "");
            liClosed.Attributes.Add("class", "active");

            linkclick = "Closed";

            BindGrid(linkclick);
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;

            if (string.IsNullOrEmpty(linkclick))
            {
                linkclick = "Not Done";
                btnNotDone_Click(sender, e);
            }
            else
                BindGrid(linkclick);
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            if (string.IsNullOrEmpty(linkclick))
            {
                linkclick = "Not Done";
                btnNotDone_Click(sender, e);
            }
            else
                BindGrid(linkclick);
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();

                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdARSReport.PageIndex = chkSelectedPage - 1;
            grdARSReport.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid(linkclick);
        }
    }
}