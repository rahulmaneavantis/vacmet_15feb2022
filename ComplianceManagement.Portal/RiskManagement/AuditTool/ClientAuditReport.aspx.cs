﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class ClientAuditReport : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        public static string DocumentPath = "";
        protected bool DepartmentHead = false;
        protected string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindFinancialYear();
                BindLegalEntityData();
                BindVertical();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                    else
                    {
                        ViewState["Status"] = 3;
                    }
                }
                else
                {
                    ViewState["Status"] = 3;
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"] == "Process")
                    {
                        ShowProcessGrid(sender, e);
                    }
                    else if (Request.QueryString["Type"] == "Implementation")
                    {
                        ShowImplementationGrid(sender, e);
                    }
                    else
                    {
                        ShowProcessGrid(sender, e);
                    }
                }
                else
                {
                    ShowProcessGrid(sender, e);
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            /*
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            */
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            bool DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
            {
                if (ViewState["Type"].ToString() == "Process")
                {
                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdProcessAudits.PageIndex = chkSelectedPage - 1;
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                }
            }
            BindData();
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }

            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (DepartmentHead)
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                string Flag = String.Empty;
                int VerticalID = -1;
                string departmentheadFlag = string.Empty;
                if (DepartmentHead)
                {
                    departmentheadFlag = "DH";
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    Flag = Convert.ToString(ViewState["Type"]);
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                Branchlist.Clear();
                GetAllHierarchy(CustomerId, branchid);
                Branchlist.ToList();

                if (Flag == "Process")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = false;
                    grdProcessAudits.Visible = true;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAudits(FinYear, Period, CustomerId, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID, 4, departmentheadFlag);
                    grdProcessAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdProcessAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
                else if (Flag == "Implementation")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = true;
                    grdProcessAudits.Visible = false;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAuditsIMP(FinYear, Period, CustomerId, branchid, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID);
                    grdImplementationAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdImplementationAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }

                bindPageNumber();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindSchedulingType();
            BindData();
            BindVertical();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType();
                    BindVertical();
                    BindData();
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            bindPageNumber();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    if (ViewState["Type"].ToString() == "Process")
                    {
                        grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                    else if (ViewState["Type"].ToString() == "Implementation")
                    {
                        grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                }

                BindData();
                if (ViewState["Type"].ToString() == "Process")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdProcessAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdImplementationAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void grdProcessAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int AuditID = Convert.ToInt32(commandArgs[4]);
                    ViewState["AuditID"] = AuditID;
                    int VerticalID = -1;
                    string Area = string.Empty;
                    string ReportNameAndNumber = string.Empty;
                    if (RiskCategoryManagement.CheckGeneareReportExistOrNot(AuditID))
                    {
                        #region generate report
                        try
                        {
                            if (CustomerId == 0)
                            {
                                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            }

                            string CustomerBranchName = string.Empty;
                            string FinancialYear = string.Empty;
                            int CustomerBranchId = -1;
                            string PeriodName = string.Empty;
                            string VerticalName = string.Empty;
                            string AditHeadName = string.Empty;
                            string BranchName = string.Empty;

                            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                            {
                                if (ddlLegalEntity.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                    CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                            {
                                if (ddlSubEntity1.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                    CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                            {
                                if (ddlSubEntity2.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                    CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                            {
                                if (ddlSubEntity3.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                    CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                            {
                                if (ddlPeriod.SelectedValue != "-1")
                                {
                                    PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                                }
                            }
                            List<string> AssignedUserList = new List<string>();
                            var customerName = UserManagementRisk.GetCustomerName(CustomerId);
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                var AuditDetails = (from row in entities.AuditClosureDetails
                                                    where row.ID == AuditID
                                                    select row).FirstOrDefault();
                                CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                                FinancialYear = AuditDetails.FinancialYear;
                                PeriodName = AuditDetails.ForMonth;
                                VerticalID = Convert.ToInt32(AuditDetails.VerticalId);
                                if (string.IsNullOrEmpty(VerticalName))
                                {
                                    VerticalName = (from row in entities.mst_Vertical
                                                    where row.ID == VerticalID
                                                    select row.VerticalName).FirstOrDefault();
                                }
                                var ReportName = "SW/" + FinancialYear + "/" + VerticalName + "/";
                                int ReportNumber = RiskCategoryManagement.GetAuditReportNumber(AuditID, FinancialYear);
                                AuditDetails.ReportName = ReportName;
                                AuditDetails.ReportNumber = ReportNumber;
                                entities.SaveChanges();

                                AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                                    join row1 in entities.mst_User
                                                    on row.UserID equals row1.ID
                                                    where row.IsActive == true
                                                    && row.AuditID == AuditID
                                                    select row1.FirstName + " " + row1.LastName).Distinct().ToList();

                                ReportNameAndNumber = RiskCategoryManagement.GetReportNameAndNumber(AuditID);
                            }

                            DateTime draftDate = (DateTime)UserManagementRisk.GetDraftReportDate(CustomerId, CustomerBranchId, VerticalID, PeriodName, FinancialYear);
                            var draftReportReleaseDate = draftDate.ToString("MMM dd, yyyy");
                            var draftReportReleaseTime = draftDate.ToString("HH:mm");// added by sagar more on 23-01-2020

                            DateTime releaseDate = (DateTime)UserManagementRisk.GetCreatedDateForGenerateReport(CustomerId, CustomerBranchId, PeriodName, FinancialYear);
                            var finalReportReleaseDate = releaseDate.ToString("MMM dd, yyyy");
                            var finalReportReleaseTime = releaseDate.ToString("HH:mm"); // added by sagar more on 23-01-2020

                            #region Word Report 1  
                            {
                                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                                string SWReportImage = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/Images/SWReportImage.jpg";

                                using (AuditControlEntities entities = new AuditControlEntities())
                                {
                                    var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                                    select row).ToList();

                                    var AuditeeList = itemlist.Select(entry => entry.PersonResponsibleName).Distinct().ToList(); // added by sagar more on 21-01-2020

                                    if (itemlist.Count > 0)
                                    {
                                        if (string.IsNullOrEmpty(CustomerBranchName))
                                        {
                                            CustomerBranchName = itemlist.Select(b => b.Branch).FirstOrDefault();
                                        }
                                        #region first Report
                                        stringbuilderTOP.Append(@"<p style='margin-top:1%; font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                        + " Area:" + Area + " </b></p>");

                                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                       + " Location: " + CustomerBranchName + "</b></p>");

                                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b>"
                                       + "Date of Report: " + DateTime.Now.ToString("MMM dd, yyyy") + "</b></p>");

                                        stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt; text-align: center;'><b><u>"
                                       + "Part A: Executive Summary</u></b></p>");

                                        stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'><b>Distribution:</b></td><td rowspan='4' style='width:50%;border: 1px solid black; padding:10px;'>" +
                                        "<table style='width:100%; font-family:Calibri;'>" +
                                        "<tr><td><b>Audit duration:</b><br/><br/><br/></td></tr>" +
                                        "<tr><td><b>Closing meeting:</b><br/><br/><br/></td></tr>" +
                                        "<tr><td><b>Draft report:</b><br/><br/><br/></td></tr> " +
                                        "<tr><td><b>Management response:</b><br/><br/><br/></td></tr>" +
                                        "<tr><td><b>Audit team member(s):</b><br/><br/><br/></td></tr>" +
                                        "<tr><td><b>Report reference:</b><br/><br/><br/></td></tr>" +
                                        "</table>" +
                                        "</td>" +
                                        "</tr>" +
                                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr>" +
                                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr>" +
                                        "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + " : " + AditHeadName + "</td></tr></table>");

                                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 1.Executive Summary </b><br/><br/>");
                                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> Key findings are as under: </b>");

                                        int AsciiValue = 97;
                                        int SrNoCategoryTable = 97;
                                        var TotalMajorCatg = 0;
                                        var TotalModerateCatg = 0;
                                        var TotalMinorCatg = 0;
                                        var TotalCount = 0;
                                        var ProcessRatingName = string.Empty;
                                        foreach (var item in itemlist)
                                        {
                                            stringbuilderTOP.Append(@"<p style='font-family:Calibri; font-size:12pt;'>"
                                            + Convert.ToChar(AsciiValue) + ") " + item.BriefObservation +
                                            "<br/><b><u>Management response: </u></b>" + item.ManagementResponse + "</p>");
                                            //"<p style='margin-left:10px'>" + item.ManagementResponse + "</p><br/>"); <div style='text-align: left; margin-top:-10px'>
                                            AsciiValue++;
                                            if (!string.IsNullOrEmpty(item.ProcessRatingName))
                                            {
                                                ProcessRatingName = item.ProcessRatingName;
                                            }
                                        }

                                        // stringbuilderTOP.Append(@"<style>.break { page-break-after: always;}</style>");
                                        stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                        stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Calibri; font-size:12pt; text-align: left;'><b> 
                                A summary of findings based on risk rankings and nature of control gaps is given in the table below: </p><br/>");

                                        stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>
                                                        <thead><tr> 
                                                        <th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Sr.No." + "</th>" +
                                                                "<th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Nature of issues reported" + "</th>" +
                                                                "<th colspan='4' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Risk Rating" + "</th>" +
                                                                "<th rowspan='2' style='border: 1px solid black; background-color:#DAE8F6; font-weight:bold'>Page reference (in the detailed report)" +
                                                                "</th></tr><tr><th style='border: 1px solid black; background-color:#FDB924; font-weight:bold'>Major</th>" +
                                                                "<th style='border: 1px solid black; background-color:#FFFF00; font-weight:bold'>Moderate</th>" +
                                                                "<th style='border: 1px solid black; background-color:#92D050; font-weight:bold'>Minor</th>" +
                                                                "<th style='border: 1px solid black; background-color:#BEEAF7; font-weight:bold'>Total</th></tr></thead><tbody>"
                                            );

                                        var CategoryList = itemlist.Select(entry => entry.ObservationCategory).Distinct().ToList();
                                        foreach (var catgID in CategoryList)
                                        {
                                            var catgName = itemlist.Where(entry => entry.ObservationCategory == catgID).FirstOrDefault();
                                            var MajorCatg = itemlist.Where(entry => entry.ObservationRating == 1 && entry.ObservationCategory == catgID).ToList().Count();
                                            var ModerateCatg = itemlist.Where(entry => entry.ObservationRating == 2 && entry.ObservationCategory == catgID).ToList().Count();
                                            var MinorCatg = itemlist.Where(entry => entry.ObservationRating == 3 && entry.ObservationCategory == catgID).ToList().Count();
                                            var Total = MajorCatg + ModerateCatg + MinorCatg;

                                            TotalMajorCatg = TotalMajorCatg + MajorCatg;
                                            TotalModerateCatg = TotalModerateCatg + ModerateCatg;
                                            TotalMinorCatg = TotalMinorCatg + MinorCatg;
                                            TotalCount = TotalCount + Total;

                                            stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black; text-align: center;'>" + Convert.ToChar(SrNoCategoryTable) + "." +
                                            "</td><td style='border: 1px solid black;'>" +
                                            catgName.ObservationCategoryName + "</td><td style='border: 1px solid black; text-align: center;'>" + MajorCatg
                                            + "</td><td style='border: 1px solid black; text-align: center;'>" + ModerateCatg + "</td><td style='border: 1px solid black; text-align: center;'>" + MinorCatg + "</td><td style='border: 1px solid black;  text-align: center;'>" + Total + "</td><td style='border: 1px solid black;'></td></tr>"
                                            );
                                            SrNoCategoryTable++;
                                        }
                                        stringbuilderTOP.Append(@"</tbody><tfoot><tr><td Colspan='2' style='border: 1px solid black; background-color:#ECF6F9; font-weight:bold'>Total</td>
                                  <td style='border: 1px solid black; background-color:#FDB924; font-weight:bold; text-align: center;'>" + TotalMajorCatg
                                        + "</td><td style='border: 1px solid black; background-color:#FFFF00; font-weight:bold; text-align: center;'>" + TotalModerateCatg
                                        + "</td><td style='border: 1px solid black; background-color:#92D050; font-weight:bold; text-align: center;'>" + TotalMinorCatg
                                        + "</td><td style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold; text-align: center;'>" + TotalCount
                                        + "</td><td style='border: 1px solid black; background-color:#ECF6F9; font-weight:bold'></td></tr>"
                                        + "<tr><td colspan='2' style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Process Rating and Maturity Level</td>");
                                        if (!string.IsNullOrEmpty(ProcessRatingName))
                                        {
                                            stringbuilderTOP.Append(@"<td colspan='5' style='border: 1px solid black;'>" + ProcessRatingName + "</td></tr>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td colspan='5' style='border: 1px solid black;'></td></tr>");
                                        }
                                        stringbuilderTOP.Append(@"< tr><td colspan='2' style='border: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Overall conclusion</td><td colspan='5' style='border: 1px solid black; font-weight:bold'></td></tr>"
                                         + "</tfoot></table>"
                                         );

                                        stringbuilderTOP.Append(@"</table>");
                                        ProcessRequestWord(stringbuilderTOP.ToString(), CustomerBranchName);
                                        #endregion
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "There is no Data for download.";
                                    }
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "You need to generate report first";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void ProcessRequestWord(string contexttxt, string LocationName)
        {
            //string hpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_logo.jpg";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />")
            .Replace("“", " ")
            .Replace("”", " ");
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>

                 p.MsoHeader, li.MsoHeader, div.MsoHeader{
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                 }

                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                }
                

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                    
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.2in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Calibri;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;                   
                }

                div.Section1
                {
                    page:Section1;
                }
                table#hrdftrtbl
                {
                        margin:0in 0in 0in 900in;
                        width:1px;
                        height:1px;
                        overflow:hidden;
                }

                -->
                </style></head>");
            //
            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");
            sbTop.Append(context);
            sbTop.Append(@"<table id='hrdftrtbl'  style='width:100%;' border='1' cellspacing='0' cellpadding='0'>" +
               "<tr><td><div style='mso-element:header' id=h1>");

            sbTop.Append("<div style='border:none;mso-border-bottom-alt:solid windowtext.75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
           // "<div style='text-align:right;'> <img height='100' width='120' src='" + hpath + "' alt ='d'></img></div>" +
            "<div style='text-align:center;'> Executive Summary Report</div></div>");

            sbTop.Append(@"<p class=MsoHeader style='border:none;mso-border-bottom-alt:solid windowtext 50pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>");


            sbTop.Append("</div>");

            sbTop.Append("<div style='mso-element:footer' id=f1>" +
            "<p class=MsoFooter style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>" +
            "<font size='2'> <span> Confidential</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page&nbsp;&nbsp;<span style='mso-field-code: PAGE'><span style='mso-no-proof:yes'>1</span></span> of<span style='mso-field-code: NUMPAGES '></span></font></div>");

            sbTop.Append("</body></html>");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            #region Word Download Code
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + "Executive Summary" + " - " + LocationName + ".doc");
            Response.Write(strBody);
            #endregion
        }
        public void ProcessRequest_AsPerClientFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            string Headerpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_Logo_C.png";
            string Footerpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_Footer.png";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />");

            //string details = "Audit Report for the FY (" + FinancialYear + ")";
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                                                                      
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.5in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Arial;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;
                
                }
               
                div.Section1
                {
                    page:Section1;
                }               
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 900in;
                    width:1px;
                    height:1px;
                    overflow:hidden;
                }
                -->
                </style></head>");

            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

            sbTop.Append(context);
            sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                    <div style='mso-element:header' id=h1 >
                    <p class=MsoHeader style='text-align:right;font-family:Arial;'><b>");

            sbTop.Append("<img src ='" + Headerpath + "' alt ='d'  ></img >");
            sbTop.Append(@"</div></td>);

                  sbTop.Append(<td>             
                    <div style='mso-element:footer' id=h1 >
                    <p class=MsoFooter style='text-align:right;font-family:Arial;'><b>");

            sbTop.Append("<img src ='" + Footerpath + "' alt ='d'  ></img >");
            sbTop.Append(@"</div>
                </td></tr>
                </table>
                </body></html>
                ");

            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            //if (string.IsNullOrEmpty(PeriodName))
            //{
            //    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
            //    if (peridodetails != null)
            //    {
            //        if (peridodetails.Count > 0)
            //        {
            //            foreach (var ForPeriodName in peridodetails)
            //            {
            //                string fileName = "Consolidated Location Report" + FinancialYear + ForPeriodName + ".doc";
            //                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
            //                {
            //                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
            //                    FinancialYear = Convert.ToString(FinancialYear),
            //                    Period = Convert.ToString(ForPeriodName),
            //                    CustomerId = customerID,
            //                    VerticalID = Convert.ToInt32(VerticalID),
            //                    FileName = fileName,
            //                    AuditID = AuditID,

            //                };
            //                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            //                string directoryPath1 = "";
            //                bool Success1 = false;
            //                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
            //                {
            //                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
            //                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
            //                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
            //                   + "/1.0");
            //                    try
            //                    {
            //                        Directory.Delete(directoryPath1);
            //                    }
            //                    catch (Exception ex) { }
            //                    if (!Directory.Exists(directoryPath1))
            //                    {
            //                        DocumentManagement.CreateDirectory(directoryPath1);
            //                    }
            //                    Guid fileKey1 = Guid.NewGuid();
            //                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
            //                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
            //                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
            //                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
            //                    FinalDeleverableUpload.Version = "1.0";
            //                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
            //                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
            //                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            //                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
            //                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    string fileName = "Consolidated Location Report" + FinancialYear + PeriodName + ".doc";
            //    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
            //    {
            //        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
            //        FinancialYear = Convert.ToString(FinancialYear),
            //        Period = Convert.ToString(PeriodName),
            //        CustomerId = customerID,
            //        VerticalID = Convert.ToInt32(VerticalID),
            //        FileName = fileName,
            //        AuditID = AuditID,
            //    };
            //    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            //    string directoryPath1 = "";

            //    bool Success1 = false;
            //    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
            //    {
            //        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
            //       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
            //       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
            //       + "/1.0");
            //        try
            //        {
            //            Directory.Delete(directoryPath1);
            //        }
            //        catch (Exception ex) { }
            //        if (!Directory.Exists(directoryPath1))
            //        {
            //            DocumentManagement.CreateDirectory(directoryPath1);
            //        }

            //        Guid fileKey1 = Guid.NewGuid();
            //        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
            //        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
            //        FinalDeleverableUpload.FileName = fileName;
            //        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
            //        FinalDeleverableUpload.FileKey = fileKey1.ToString();
            //        FinalDeleverableUpload.Version = "1.0";
            //        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
            //        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
            //        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            //        DocumentManagement.Audit_SaveDocFiles(Filelist1);
            //        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
            //    }
            //}
            #region Word Download Code
            string abc = "ABC";
            string NameVertical = "testrahul";
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
            Response.Write(strBody);
            #endregion
            //}
        }
        public void ProcessRequest_AsPerClientSECOND(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Arial;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Audit Committee Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    //try { 
                                    //Directory.Delete(directoryPath1);
                                    //}
                                    //catch (Exception ex) { }
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Audit Committee Report" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest(string context, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string details = "Audit Report for the (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string NameVertical = VerticalName.VerticalName;
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + companyname + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }


        protected void grdImplementationAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    //string ForMonth = Convert.ToString(commandArgs[1]);
                    //string FinancialYear = Convert.ToString(commandArgs[2]);
                    //int Verticalid = Convert.ToInt32(commandArgs[3]);
                    long AuditID = Convert.ToInt64(commandArgs[4]);

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&AuditID=" + AuditID, false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=" + "&AuditID=" + AuditID, false);
                    }
                    // }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["Type"] = "Implementation";

            BindData();
            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["Type"] = "Process";

            BindData();

            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string GetProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.ProcessCommaSaperates
                                   where row.AuditID == AuditID
                                   select row.ProcessName).FirstOrDefault();

                return Processlist;
            }
        }

        public string GetSubProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.SubProcessCommaSeparates
                                   where row.AuditID == AuditID
                                   select row.SubProcessName).FirstOrDefault();

                return Processlist;
            }
        }
    }
}