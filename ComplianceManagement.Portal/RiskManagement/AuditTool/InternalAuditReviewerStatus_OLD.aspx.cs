﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class InternalAuditReviewerStatus_OLD : System.Web.UI.Page
    {
        protected string FinYear;
        protected string Period;
        protected int BranchID;
        protected int ATBDID;
        protected int StatusID;
        protected int ProcessID;
        protected int VerticalID;
        protected int AuditID;
        public static string DocumentPath = "";
        protected int SubProcessID;
        protected int SubProcessId;
        protected string AuditStep;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnObs.Visible = false;
                BindCustomer();
                BindObservationCategory();
                BindUsers();
                BindFinancialYear();

                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                    ViewState["FinYear"] = FinYear;
                    if (FinYear != "")
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.Items.FindByText(FinYear).Selected = true;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BID"] = BranchID;
                    if (BranchID != 0)
                    {
                        ddlFilterLocation.ClearSelection();
                        ddlFilterLocation.Items.FindByValue(BranchID.ToString()).Selected = true;
                        BindSchedulingType(BranchID);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    ViewState["Processed"] = ProcessID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SubProcessId = Convert.ToInt32(Request.QueryString["SPID"]);
                    ViewState["SPID"] = SubProcessId;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                    String SType = GetSchedulingType(BranchID, ProcessID, FinYear, Period, AuditID);
                    if (SType != "")
                    {
                        ddlSchedulingType.ClearSelection();

                        if (SType == "A")
                            ddlSchedulingType.Items.FindByText("Annually").Selected = true;
                        else if (SType == "H")
                            ddlSchedulingType.Items.FindByText("Half Yearly").Selected = true;
                        else if (SType == "Q")
                            ddlSchedulingType.Items.FindByText("Quarterly").Selected = true;
                        else if (SType == "M")
                            ddlSchedulingType.Items.FindByText("Monthly").Selected = true;
                        else if (SType == "P")
                            ddlSchedulingType.Items.FindByText("Phase").Selected = true;
                        else if (SType == "S")
                            ddlSchedulingType.Items.FindByText("Special Audit").Selected = true;
                        ddlSchedulingType_SelectedIndexChanged(null, null);

                        ddlPeriod.ClearSelection();
                        if (ddlPeriod.Items.Count > 0)
                        {
                            ddlPeriod.Items.FindByText(Period).Selected = true;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    ViewState["ATBDID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VerticalID"] = VerticalID;
                }

                bool checkPRFlag = false;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, BranchID);
                if (checkPRFlag == true)
                {
                    ddlFilterStatus.Items.Clear();
                    ddlFilterStatus.Items.Clear();
                    ddlFilterStatus.Items.Insert(0, new ListItem("Select Status", "-1"));
                    ddlFilterStatus.Items.Insert(1, new ListItem("Submitted", "2"));
                    ddlFilterStatus.Items.Insert(2, new ListItem("Team Review", "4"));
                    ddlFilterStatus.Items.Insert(3, new ListItem("Final Review", "5"));
                    ddlFilterStatus.Items.Insert(4, new ListItem("Auditee Review", "6"));
                    ddlFilterStatus.Items.Insert(5, new ListItem("Closed", "3"));
                    ddlFilterStatus.Items.Insert(6, new ListItem("Open", "1"));
                }
                else
                {
                    ddlFilterStatus.Items.Clear();
                    ddlFilterStatus.Items.Insert(0, new ListItem("Select Status", "-1"));
                    ddlFilterStatus.Items.Insert(1, new ListItem("Submitted", "2"));
                    ddlFilterStatus.Items.Insert(2, new ListItem("Team Review", "4"));
                    ddlFilterStatus.Items.Insert(3, new ListItem("Final Review", "5"));
                    ddlFilterStatus.Items.Insert(4, new ListItem("Closed", "3"));
                    ddlFilterStatus.Items.Insert(5, new ListItem("Open", "1"));
                    T.InnerText = "*";
                    M.InnerText = "*";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    StatusID = Convert.ToInt32(Request.QueryString["SID"]);
                    ViewState["StatusID"] = StatusID;
                    if (StatusID != 0)
                    {
                        ddlFilterStatus.ClearSelection();
                        ddlFilterStatus.Items.FindByValue(StatusID.ToString()).Selected = true;
                    }
                }
                tbxDate.Text = string.Empty;
                TDTab.Visible = false;
                BindddlQuater();
                tvProcessRisk_SelectedNodeChanged1(null, null);
                int ResultID = 0;
                string Tag = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["tag"]))
                {
                    Tag = Convert.ToString(Request.QueryString["tag"]);
                }
                if (Tag == "Implement" && ResultID != 0)
                {
                    EnableControl(false);
                }
                Tab1_Click(sender, e);
                //Page Title
                string ControlNo = string.Empty;
                string FY = string.Empty;
                string Period1 = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    ControlNo = "/ " + Convert.ToString(Request.QueryString["CID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FY = "/ " + Convert.ToString(Request.QueryString["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period1 = "/ " + Convert.ToString(Request.QueryString["ForMonth"]);
                }
                string Pname = string.Empty;
                string SPname = string.Empty;
                string VName = string.Empty;
                Mst_Process objprocess = new Mst_Process();
                mst_Subprocess objsubProcess = new mst_Subprocess();
                objprocess = ProcessManagement.GetByID(ProcessID, customerID);
                objsubProcess = ProcessManagement.GetSubProcessByID(SubProcessID);
                if (objprocess != null)
                {
                    Pname = "/" + objprocess.Name;
                }
                if (objsubProcess != null)
                {
                    SPname = "/" + objsubProcess.Name;
                }
                var BrachName = CustomerBranchManagement.GetByID(BranchID).Name;
                var VerticalName = UserManagementRisk.GetVerticalName(customerID, Convert.ToInt32((Request.QueryString["VID"])));
                if (string.IsNullOrEmpty(VerticalName))
                {
                    VName = "/ " + VerticalName;
                }
                LblPageDetails.InnerText = BrachName + FY + Period1 + Pname + SPname + VName + ControlNo;

                //End           
                bool roles = UserManagementRisk.CheckAssignedProcess(Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID), ProcessID, AuditID);
                //var roles = UserManagementRisk.SP_GETAssigned_AsPerformerORReviewerProcedure(Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID), Request.QueryString["FinYear"].ToString(), Request.QueryString["ForMonth"].ToString(), Convert.ToInt32(Request.QueryString["BID"]), Convert.ToInt32(Request.QueryString["VID"]), Convert.ToInt32(Request.QueryString["AuditID"]));
                if (roles)
                {
                    txtResponseDueDate.Enabled = true;
                }
                else
                {
                    txtResponseDueDate.Enabled = false;
                }
                if (ddlFilterStatus.SelectedValue == "5" || ddlFilterStatus.SelectedValue == "3")
                {
                    txtResponseDueDate.Enabled = false;
                }
            }
            DateTime date = DateTime.MinValue;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker112", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        }
        
        protected void Tab1_Click(object sender, EventArgs e)
        {
            btnObs.Visible = false;
            liAuditCoverage.Attributes.Add("class", "active");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 0;
            btnObs.Enabled = false;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            btnObs.Visible = false;
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "active");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;
            btnObs.Enabled = false;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            btnObs.Visible = true;
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "active");
            liReviewLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 2;
            if (txtObservation.Enabled == true)
            {
                btnObs.Enabled = true;
            }
            else
            {
                btnObs.Enabled = false;
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            btnObs.Visible = false;
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "active");
            MainView.ActiveViewIndex = 3;
            btnObs.Enabled = false;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void tvProcessRisk_SelectedNodeChanged1(object sender, EventArgs e)
        {
            List<long> ProcessList = new List<long>();
            if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
            {
                ProcessList.Add(Convert.ToInt32(Request.QueryString["PID"]));
            }
            MainView.ActiveViewIndex = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
                    {
                        if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlFilterLocation.SelectedValue != "-1")
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    if (ddlSchedulingType.SelectedValue != "-1")
                                    {
                                        if (ddlPeriod.SelectedValue != "0")
                                        {
                                            if (ddlPeriod.SelectedValue != "-1" && ddlPeriod.SelectedValue.ToString() != "Select Period")
                                            {
                                                var getScheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 4, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, Convert.ToInt32(AuditID));
                                                if (getScheduleondetails != null)
                                                {
                                                    TDTab.Visible = true;
                                                    TDTab.Attributes.Add("style", "position: absolute;");

                                                    lnkAuditCoverage.Enabled = true;
                                                    lnkActualTesting.Enabled = true;
                                                    lnkObservation.Enabled = true;
                                                    lnkReviewLog.Enabled = true;
                                                    if (Convert.ToInt32(ddlFilterStatus.SelectedValue) != 1)
                                                    {
                                                        BindTransactionDetails(Convert.ToInt32(getScheduleondetails.ID), Convert.ToInt32(getScheduleondetails.ProcessId), ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), Convert.ToInt32(getScheduleondetails.InternalAuditInstance), VerticalID, ddlPeriod.SelectedItem.Text, ATBDID, AuditID);
                                                    }


                                                    Tab1_Click(sender, e);
                                                    if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 3 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 1)
                                                    {
                                                        txtObservation.Enabled = false;
                                                        txtAuditObjective.Enabled = false;
                                                        txtAuditSteps.Enabled = false;
                                                        txtAnalysisToBePerformed.Enabled = false;
                                                        txtWalkthrough.Enabled = false;
                                                        txtActualWorkDone.Enabled = false;
                                                        txtpopulation.Enabled = false;
                                                        txtSample.Enabled = false;
                                                        txtObservationNumber.Enabled = false;
                                                        txtObservationTitle.Enabled = false;
                                                        txtRisk.Enabled = false;
                                                        txtRootcost.Enabled = false;
                                                        txtfinancialImpact.Enabled = false;
                                                        txtRecommendation.Enabled = false;
                                                        txtMgtResponse.Enabled = false;
                                                        ddlPersonresponsible.Enabled = false;
                                                        ddlOwnerName.Enabled = false;
                                                        ddlobservationRating.Enabled = false;
                                                        ddlObservationCategory.Enabled = false;
                                                        ddlObservationSubCategory.Enabled = false;
                                                        btnNext1.Enabled = false;
                                                        btnNext2.Enabled = false;
                                                        btnNext3.Enabled = false;
                                                        rptComplianceDocumnets.Enabled = false;
                                                        GridRemarks.Enabled = false;
                                                        txtRemark.Enabled = false;
                                                        FileuploadAdditionalFile.Enabled = false;
                                                        ddlReviewerRiskRatiing.Enabled = false;
                                                        txtTimeLine.Enabled = false;
                                                        txtResponseDueDate.Enabled = false;
                                                        btnSave.Enabled = false;
                                                        txtAuditStepScore.Enabled = false;
                                                        FileObjUpload.Enabled = false;
                                                        btnObjfileupload.Enabled = false;
                                                        GrdobservationFile.Enabled = false;
                                                        ObjReport1.Enabled = false;
                                                        tbxUHComment.Enabled = false;
                                                        tbxPresidentComment.Enabled = false;
                                                        ddlPersonresponsibleUH.Enabled = false;
                                                        ddlPersonresponsiblePresident.Enabled = false;
                                                        FileUploadImageTable.Enabled = true;
                                                        btnImageUpload.Enabled = true;
                                                        grdObservationImg.Enabled = true;
                                                        tbxBriefObservation.Enabled = false;
                                                        tbxObjBackground.Enabled = false;
                                                        ddlDefeciencyType.Enabled = false;
                                                        txtMultilineVideolink.Enabled = true;
                                                        txtAnnexueTitle.Enabled = true;
                                                    }
                                                    else
                                                    {
                                                        txtObservation.Enabled = true;
                                                        txtAuditObjective.Enabled = false;
                                                        txtAuditSteps.Enabled = false;
                                                        txtAnalysisToBePerformed.Enabled = false;
                                                        txtWalkthrough.Enabled = true;
                                                        txtActualWorkDone.Enabled = true;
                                                        txtpopulation.Enabled = true;
                                                        txtSample.Enabled = true;
                                                        txtObservationNumber.Enabled = true;
                                                        txtObservationTitle.Enabled = true;
                                                        txtRisk.Enabled = true;
                                                        txtRootcost.Enabled = true;
                                                        txtfinancialImpact.Enabled = true;
                                                        txtRecommendation.Enabled = true;
                                                        txtMgtResponse.Enabled = true;
                                                        btnNext1.Enabled = true;
                                                        btnNext2.Enabled = true;
                                                        btnNext3.Enabled = true;
                                                        rptComplianceDocumnets.Enabled = true;
                                                        GridRemarks.Enabled = true;
                                                        txtRemark.Enabled = true;
                                                        FileuploadAdditionalFile.Enabled = true;
                                                        ddlReviewerRiskRatiing.Enabled = true;
                                                        txtTimeLine.Enabled = true;
                                                        txtResponseDueDate.Enabled = true;
                                                        btnSave.Enabled = true;
                                                        txtAuditStepScore.Enabled = true;
                                                        ObjReport1.Enabled = true;
                                                        tbxUHComment.Enabled = true;
                                                        tbxPresidentComment.Enabled = true;
                                                        ddlPersonresponsibleUH.Enabled = true;
                                                        ddlPersonresponsiblePresident.Enabled = true;
                                                        FileUploadImageTable.Enabled = true;
                                                        btnImageUpload.Enabled = true;
                                                        grdObservationImg.Enabled = true;
                                                        tbxBriefObservation.Enabled = true;
                                                        tbxObjBackground.Enabled = true;
                                                        ddlDefeciencyType.Enabled = true;
                                                        FileObjUpload.Enabled = true;
                                                        btnObjfileupload.Enabled = true;
                                                        GrdobservationFile.Enabled = true;
                                                        txtMultilineVideolink.Enabled = true;
                                                        txtAnnexueTitle.Enabled = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var listOfRole = CustomerManagementRisk.GetListOfRoleInAudit(AuditID, Portal.Common.AuthenticationHelper.UserID, ProcessList);
            if (listOfRole.Contains(5))
            {
                if (ddlFilterStatus.SelectedValue == "2" || ddlFilterStatus.SelectedValue == "3" || ddlFilterStatus.SelectedValue == "4" || ddlFilterStatus.SelectedValue == "6")
                {
                    btnNext1.Enabled = false;
                    btnNext2.Enabled = false;
                    btnNext3.Enabled = false;
                    btnSave.Enabled = false;
                }
                if (ddlFilterStatus.SelectedValue == "6")
                {
                    if (string.IsNullOrEmpty(txtObservation.Text))
                    {
                        btnNext1.Enabled = true;
                        btnNext2.Enabled = true;
                        btnNext3.Enabled = true;
                        btnSave.Enabled = true;
                    }
                }
            }
            #region Audittee Status Enable Grid

            InternalControlAuditResult MstRiskResult = new InternalControlAuditResult();

            MstRiskResult.ATBDId = ATBDID;
            MstRiskResult.CustomerBranchId = BranchID;
            MstRiskResult.FinancialYear = FinYear;
            MstRiskResult.ForPerid = Period;
            MstRiskResult.ProcessId = ProcessID;
            MstRiskResult.VerticalID = VerticalID;
            MstRiskResult.AuditID = AuditID;

            //var AuditteeStatusID = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
            //if (AuditteeStatusID == "RS" || AuditteeStatusID == "RA")
            //{
            //    btnNext1.Enabled = false;
            //    btnNext2.Enabled = false;
            //    btnNext3.Enabled = false;
            //    btnSave.Enabled = false;
            //}

            #endregion
        }
        public void BindUsers()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var AllUser = RiskCategoryManagement.FillUsers(customerID);

            ddlPersonresponsible.Items.Clear();
            ddlPersonresponsible.DataTextField = "Name";
            ddlPersonresponsible.DataValueField = "ID";
            ddlPersonresponsible.DataSource = AllUser;
            ddlPersonresponsible.DataBind();
            ddlPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlPersonresponsibleUH.Items.Clear();
            ddlPersonresponsibleUH.DataTextField = "Name";
            ddlPersonresponsibleUH.DataValueField = "ID";
            ddlPersonresponsibleUH.DataSource = AllUser;
            ddlPersonresponsibleUH.DataBind();
            ddlPersonresponsibleUH.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlPersonresponsiblePresident.Items.Clear();
            ddlPersonresponsiblePresident.DataTextField = "Name";
            ddlPersonresponsiblePresident.DataValueField = "ID";
            ddlPersonresponsiblePresident.DataSource = AllUser;
            ddlPersonresponsiblePresident.DataBind();
            ddlPersonresponsiblePresident.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));

            ddlOwnerName.Items.Clear();
            ddlOwnerName.DataTextField = "Name";
            ddlOwnerName.DataValueField = "ID";
            ddlOwnerName.DataSource = AllUser;
            ddlOwnerName.DataBind();
            ddlOwnerName.Items.Insert(0, new ListItem("Select Owner", "-1"));
        }
        public void BindCustomer()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindObservationCategory()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlObservationCategory.DataTextField = "Name";
            ddlObservationCategory.DataValueField = "ID";
            ddlObservationCategory.Items.Clear();
            ddlObservationCategory.DataSource = ObservationSubcategory.FillObservationCategory(customerID);
            ddlObservationCategory.DataBind();
            ddlObservationCategory.Items.Insert(0, new ListItem("Select Observation Category", "-1"));
        }
        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void BindDocument(int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                if (ComplianceDocument.Count > 0)
                {
                    ComplianceDocument = ComplianceDocument.Where(entry => entry.Version == "1.0").ToList();
                    ComplianceDocument = ComplianceDocument.Where(e => e.TypeOfFile != "OB").ToList();
                    rptComplianceDocumnets.DataSource = ComplianceDocument.OrderBy(x => x.FileType).ToList();
                    rptComplianceDocumnets.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindObservationDocument(int ScheduledOnID, int ATBDID, int AuditID)
        {
            try
            {
                ViewState["ScheduledOnID"] = ScheduledOnID;
                ViewState["ATBDID"] = ATBDID;
                ViewState["AuditID"] = AuditID;

                List<GetInternalAuditDocumentsView> ComplianceDocument = new List<GetInternalAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetInternalAuditDocumentsView(ScheduledOnID, ATBDID, AuditID).ToList();
                if (ComplianceDocument.Count > 0)
                {
                    ComplianceDocument = ComplianceDocument.Where(entry => entry.Version == "1.0").ToList();
                    ComplianceDocument = ComplianceDocument.Where(e => e.TypeOfFile == "OB").ToList();
                    GrdobservationFile.DataSource = ComplianceDocument.OrderBy(x => x.FileType).ToList();
                    GrdobservationFile.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        private void BindRemarks(int ProcessId, int ScheduledOnID, int Atbtid, int AuditID)
        {
            try
            {
                // throw new NotImplementedException();
                GridRemarks.DataSource = Business.DashboardManagementRisk.GetInternalAllRemarks(ProcessId, ScheduledOnID, Atbtid, AuditID);
                GridRemarks.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                List<long> ProcessList = new List<long>();
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessList.Add(Convert.ToInt32(Request.QueryString["PID"]));
                }
                rdbtnStatus.Items.Clear();
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
                var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(Request.QueryString["AuditID"]), ProcessList);
                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        #region                        
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (RoleListInAudit.Contains(5))
                            {
                                if ((st.ID == 4) || (st.ID == 3) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                            else
                            {
                                if ((st.ID == 4) || (st.ID == 5) || (st.ID == 2)) //|| (st.ID == 6)
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                        #endregion
                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        #region
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(Request.QueryString["AuditID"]), Portal.Common.AuthenticationHelper.UserID, ProcessList);
                            if (RoleListInAudit1.Contains(5))
                            {
                                if ((st.ID == 4) || (st.ID == 6) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                            else
                            {
                                if ((st.ID == 4) || (st.ID == 6) || (st.ID == 3) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                }
                            }
                        }
                        #endregion
                    }
                    else if (ddlFilterStatus.SelectedValue == "4")
                    {
                        #region

                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (RoleListInAudit.Contains(5))
                            {
                                if (st.ID == 3 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                            else
                            {
                                if (st.ID == 5 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                        #endregion
                    }
                    else if (ddlFilterStatus.SelectedValue == "2")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (RoleListInAudit.Contains(5))
                            {
                                if (st.ID == 3 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                {
                                    var RoleListInAudit2 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(Request.QueryString["AuditID"]), Portal.Common.AuthenticationHelper.UserID, ProcessList);
                                    if (RoleListInAudit2.Contains(5))
                                    {
                                        if (st.ID == 3 || (st.ID == 2))
                                        {
                                        }
                                        else
                                        {
                                            rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                    else
                                    {
                                        if (st.ID == 5 || (st.ID == 2))
                                        {
                                        }
                                        else
                                        {
                                            rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        #region
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                //if (!(st.ID == 4))
                                //{
                                rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                //}
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(txtObservation.Text))
                                {
                                    if (!(st.ID == 6))
                                    {
                                        if (!(st.ID == 2))
                                        {
                                            rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                        }
                                    }
                                }
                                else
                                {
                                    if (!(st.ID == 5))
                                    {
                                        if (!(st.ID == 2))
                                        {
                                            rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                        }
                                    }
                                }
                            }

                        }
                        #endregion
                    }
                }//Personresponsible End
                else
                {
                    if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 4))
                            {
                                if (!(st.ID == 2))
                                {
                                    rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                if ((st.ID == 4) || (st.ID == 7) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                }
                            }
                            else
                            {
                                if (!(st.ID == 7))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                    }
                                }
                            }
                        }
                    }
                }

                lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTransactions(int ScheduledOnID, int Atbtid, int AuditID)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.DashboardManagementRisk.GetAllInternalAuditTransactions(ScheduledOnID, Atbtid, AuditID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EnableControl(bool Flag)
        {
            txtObservation.Enabled = Flag;
            txtAuditObjective.Enabled = false;
            txtAuditSteps.Enabled = false;
            txtAnalysisToBePerformed.Enabled = false;
            txtWalkthrough.Enabled = Flag;
            txtActualWorkDone.Enabled = Flag;
            txtpopulation.Enabled = Flag;
            txtSample.Enabled = Flag;
            txtObservationNumber.Enabled = Flag;
            txtObservationTitle.Enabled = Flag;
            txtRisk.Enabled = Flag;
            txtRootcost.Enabled = Flag;
            txtfinancialImpact.Enabled = Flag;
            txtRecommendation.Enabled = Flag;
            txtMgtResponse.Enabled = Flag;
            ddlPersonresponsible.Enabled = Flag;
            ddlOwnerName.Enabled = Flag;
            ddlobservationRating.Enabled = Flag;
            ddlObservationCategory.Enabled = Flag;
            ddlObservationSubCategory.Enabled = Flag;
            btnNext1.Enabled = Flag;
            btnNext2.Enabled = Flag;
            btnNext3.Enabled = Flag;
            btnSave.Enabled = Flag;
            rptComplianceDocumnets.Enabled = Flag;
            GridRemarks.Enabled = Flag;
            txtRemark.Enabled = Flag;
            FileuploadAdditionalFile.Enabled = Flag;
            ddlReviewerRiskRatiing.Enabled = Flag;
            txtTimeLine.Enabled = Flag;
            txtResponseDueDate.Enabled = Flag;
            btnSave.Enabled = Flag;
            txtAuditStepScore.Enabled = Flag;
            ObjReport1.Enabled = Flag;
            tbxUHComment.Enabled = Flag;
            tbxPresidentComment.Enabled = Flag;
            ddlPersonresponsibleUH.Enabled = Flag;
            ddlPersonresponsiblePresident.Enabled = Flag;
            FileUploadImageTable.Enabled = Flag;
            btnImageUpload.Enabled = Flag;
            grdObservationImg.Enabled = Flag;
            tbxBriefObservation.Enabled = Flag;
            tbxObjBackground.Enabled = Flag;
            ddlDefeciencyType.Enabled = Flag;
            txtMultilineVideolink.Enabled = Flag;
            txtAnnexueTitle.Enabled = Flag;
        }
        private void BindTransactionDetails(int ScheduledOnID, int Processed, string FinancialYear, int CustomerBranchID, int InternalAuditInstance, int verticalid, string ForPeriod, int Atbdid, int AuditID)
        {
            try
            {
                txtObservation.Text = string.Empty;
                txtAuditObjective.Text = string.Empty;
                txtAuditSteps.Text = string.Empty;
                txtAnalysisToBePerformed.Text = string.Empty;
                txtWalkthrough.Text = string.Empty;
                txtActualWorkDone.Text = string.Empty;
                txtpopulation.Text = string.Empty;
                txtSample.Text = string.Empty;
                txtObservationNumber.Text = string.Empty;
                txtObservationTitle.Text = string.Empty;
                txtRisk.Text = string.Empty;
                txtRootcost.Text = string.Empty;
                txtfinancialImpact.Text = string.Empty;
                txtRecommendation.Text = string.Empty;
                txtMgtResponse.Text = string.Empty;
                ddlPersonresponsible.SelectedValue = "-1";
                ddlOwnerName.SelectedValue = "-1";
                ddlobservationRating.SelectedValue = "-1";
                ddlObservationCategory.SelectedValue = "-1";
                ddlReviewerRiskRatiing.SelectedValue = "-1";
                tbxRemarks.Text = string.Empty;
                txtAuditStepScore.Text = string.Empty;
                txtAnnexueTitle.Text = string.Empty;
                tbxTable.Text = string.Empty;
                tbxUHComment.Text = string.Empty;
                tbxPresidentComment.Text = string.Empty;
                ddlPersonresponsibleUH.SelectedValue = "-1";
                ddlPersonresponsiblePresident.SelectedValue = "-1";
                tbxBriefObservation.Text = string.Empty;
                tbxObjBackground.Text = string.Empty;
                ddlDefeciencyType.SelectedValue = "-1";

                InternalControlAuditResult MstRiskResult = new InternalControlAuditResult();
                int ResultID = 0;
                string Tag = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["tag"]))
                {
                    Tag = Convert.ToString(Request.QueryString["tag"]);
                }
                if (Tag == "Implement" && ResultID != 0)
                {
                    MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(ResultID);
                }
                else
                {
                    MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(ScheduledOnID, Atbdid, verticalid, AuditID);
                }

                var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(ScheduledOnID), Atbdid);
                CheckStatus(Processed, Atbdid);
                if (MstRiskResult != null)
                {
                    if (MstRiskResult.ISACPORMIS != null)
                    {

                        ObjReport1.SelectedValue = Convert.ToString(MstRiskResult.ISACPORMIS);

                    }
                    txtObservation.Text = MstRiskResult.Observation;
                    txtAuditObjective.Text = MstRiskResult.AuditObjective;
                    txtAuditSteps.Text = MstRiskResult.AuditSteps;
                    txtAnalysisToBePerformed.Text = MstRiskResult.AnalysisToBePerofrmed;
                    txtWalkthrough.Text = MstRiskResult.ProcessWalkthrough;
                    txtActualWorkDone.Text = MstRiskResult.ActivityToBeDone;
                    txtpopulation.Text = MstRiskResult.Population;
                    txtSample.Text = MstRiskResult.Sample;
                    txtObservationNumber.Text = MstRiskResult.ObservationNumber;
                    txtObservationTitle.Text = MstRiskResult.ObservationTitle;
                    txtAnnexueTitle.Text = MstRiskResult.AnnexueTitle;
                    txtRisk.Text = MstRiskResult.Risk;
                    txtRootcost.Text = MstRiskResult.RootCost;
                    txtfinancialImpact.Text = Convert.ToString(MstRiskResult.FinancialImpact);
                    txtRecommendation.Text = MstRiskResult.Recomendation;
                    txtMgtResponse.Text = MstRiskResult.ManagementResponse;
                    txtTimeLine.Text = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                    txtResponseDueDate.Text = MstRiskResult.ResponseDueDate != null ? MstRiskResult.ResponseDueDate.Value.ToString("dd-MM-yyyy") : null;
                    tbxRemarks.Text = MstRiskResult.FixRemark;
                    txtAuditStepScore.Text = Convert.ToString(MstRiskResult.AuditScores);
                    hidObservation.Value = MstRiskResult.Observation;
                    hidAuditObjective.Value = MstRiskResult.AuditObjective;
                    hidAuditSteps.Value = MstRiskResult.AuditSteps;
                    hidAnalysisToBePerformed.Value = MstRiskResult.AnalysisToBePerofrmed;
                    hidWalkthrough.Value = MstRiskResult.ProcessWalkthrough;
                    hidActualWorkDone.Value = MstRiskResult.ActivityToBeDone;
                    hidpopulation.Value = MstRiskResult.Population;
                    hidSample.Value = MstRiskResult.Sample;
                    hidObservationNumber.Value = MstRiskResult.ObservationNumber;
                    hidObservationTitle.Value = MstRiskResult.ObservationTitle;
                    hidRisk.Value = MstRiskResult.Risk;
                    hidRootcost.Value = MstRiskResult.RootCost;
                    hidfinancialImpact.Value = Convert.ToString(MstRiskResult.FinancialImpact);
                    hidRecommendation.Value = MstRiskResult.Recomendation;
                    hidMgtResponse.Value = MstRiskResult.ManagementResponse;
                    hidRemarks.Value = MstRiskResult.FixRemark;
                    hidAuditStepScore.Value = Convert.ToString(MstRiskResult.AuditScores);
                    hidISACPORMIS.Value = Convert.ToString(MstRiskResult.ISACPORMIS);
                    hidTimeLine.Value = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                    //New Field
                    hidPersonResponsible.Value = Convert.ToString(MstRiskResult.PersonResponsible);
                    hidObservationRating.Value = Convert.ToString(MstRiskResult.ObservationRating);
                    hidObservationCategory.Value = Convert.ToString(MstRiskResult.ObservationCategory);
                    hidObservationSubCategory.Value = Convert.ToString(MstRiskResult.ObservationSubCategory);
                    hidOwner.Value = Convert.ToString(MstRiskResult.Owner);
                    hidUserID.Value = Convert.ToString(MstRiskResult.UserID);

                    hidUHPersonResponsible.Value = Convert.ToString(MstRiskResult.UHPersonResponsible);
                    hidPRESIDENTPersonResponsible.Value = Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible);
                    hidUHComment.Value = Convert.ToString(MstRiskResult.UHComment);
                    hidPRESIDENTComment.Value = Convert.ToString(MstRiskResult.PRESIDENTComment);
                    hidBodyContent.Value = Convert.ToString(MstRiskResult.BodyContent);
                    hidResponseDueDate.Value = Convert.ToString(MstRiskResult.ResponseDueDate);
                    hidAnnaxureTitle.Value = MstRiskResult.AnnexueTitle;

                    tbxBriefObservation.Text = MstRiskResult.BriefObservation;
                    tbxObjBackground.Text = MstRiskResult.ObjBackground;
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.DefeciencyType)))
                    {
                        ddlDefeciencyType.SelectedValue = Convert.ToString(MstRiskResult.DefeciencyType);
                    }

                    tbxTable.Text = MstRiskResult.BodyContent;
                    tbxUHComment.Text = MstRiskResult.UHComment;
                    tbxPresidentComment.Text = MstRiskResult.PRESIDENTComment;
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.UHPersonResponsible)))
                    {
                        ddlPersonresponsibleUH.SelectedValue = Convert.ToString(MstRiskResult.UHPersonResponsible);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible)))
                    {
                        ddlPersonresponsiblePresident.SelectedValue = Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible);
                    }

                    var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyID(ScheduledOnID, Atbdid, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, 4, verticalid, AuditID);
                    if (getDated != null)
                    {
                        tbxDate.Text = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                        if (getDated.ReviewerRiskRating != null)
                        {
                            if (getDated.ReviewerRiskRating != -1)
                            {
                                ddlReviewerRiskRatiing.SelectedValue = Convert.ToString(getDated.ReviewerRiskRating);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                    {
                        ddlPersonresponsible.SelectedValue = Convert.ToString(MstRiskResult.PersonResponsible);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Owner)))
                    {
                        ddlOwnerName.SelectedValue = Convert.ToString(MstRiskResult.Owner);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationRating)))
                    {
                        ddlobservationRating.SelectedValue = Convert.ToString(MstRiskResult.ObservationRating);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationCategory)))
                    {
                        ddlObservationCategory.SelectedValue = Convert.ToString(MstRiskResult.ObservationCategory);
                        BindObservationSubCategory(Convert.ToInt32(MstRiskResult.ObservationCategory));
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationSubCategory)))
                    {
                        ddlObservationSubCategory.SelectedValue = Convert.ToString(MstRiskResult.ObservationSubCategory);
                    }
                    BindObservationImages();
                }
                else if (MstRiskResult == null)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var checklistdetails = (from row in entities.RiskActivityToBeDoneMappings
                                                where row.ID == Atbdid
                                                select row).FirstOrDefault();
                        if (checklistdetails != null)
                        {
                            txtAuditSteps.Text = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.ActivityTobeDone)))
                            {
                                txtAuditSteps.Text = checklistdetails.ActivityTobeDone.Trim();
                            }
                            txtAuditObjective.Text = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.AuditObjective)))
                            {
                                txtAuditObjective.Text = checklistdetails.AuditObjective.Trim();
                            }
                            txtAnalysisToBePerformed.Text = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.Analyis_To_Be_Performed)))
                            {
                                txtAnalysisToBePerformed.Text = checklistdetails.Analyis_To_Be_Performed.Trim();
                            }
                        }
                    }
                }

                ViewState["ScheduledOnID"] = null;
                ViewState["ScheduledOnID"] = ScheduledOnID;

                lblNote.Visible = false;

                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();

                BindDocument(ScheduledOnID, Atbdid, AuditID);
                BindObservationDocument(ScheduledOnID, Atbdid, AuditID);
                BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), Atbdid, Processed, Convert.ToInt32(ddlFilterLocation.SelectedValue), verticalid, AuditID);
                bool checkPRFlag = false;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, CustomerBranchID);

                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                if (checkPRFlag == true)
                {
                    BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                }
                else
                {
                    BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                }

                BindTransactions(ScheduledOnID, Atbdid, AuditID);
                BindRemarks(Processed, ScheduledOnID, Atbdid, AuditID);

                if (GridRemarks.Rows.Count != 0)
                {
                    tbxRemarks.Enabled = false;

                }

                tbxDate.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";

                if (!string.IsNullOrEmpty(tbxDate.Text))
                {
                    tbxDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy");
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    string audioVideoLink = string.Empty;
                    List<ObservationAudioVideo> observationAudioVideoList = new List<ObservationAudioVideo>();
                    observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                 where cs.ATBTID == Atbdid && cs.AuditId == AuditID
                                                 && cs.IsActive == true
                                                 select cs).ToList();
                    if (observationAudioVideoList.Count > 0)
                    {
                        foreach (ObservationAudioVideo item in observationAudioVideoList)
                        {
                            audioVideoLink = audioVideoLink + "," + item.AudioVideoLink;
                        }
                        audioVideoLink = audioVideoLink.TrimStart(',');
                    }
                    txtMultilineVideolink.Text = audioVideoLink;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindObservationSubCategory(int ObservationId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            ddlObservationSubCategory.DataTextField = "Name";
            ddlObservationSubCategory.DataValueField = "ID";
            ddlObservationSubCategory.Items.Clear();
            ddlObservationSubCategory.DataSource = ObservationSubcategory.FillObservationSubCategory(ObservationId, customerID);
            ddlObservationSubCategory.DataBind();
            ddlObservationSubCategory.Items.Insert(0, new ListItem("Observation SubCategory", "-1"));
        }
        public static bool CheckAuditorFinalreviewComment(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var user = UserManagement.GetISAuditManagerID(Convert.ToInt32(customerID));
            bool checkexists = false;
            if (user != null)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var emailQuery = (from row in entities.InternalControlAuditResults
                                      where row.ProcessId == processid &&
                                      row.ForPerid == ForPeriod && row.FinancialYear == FinancialYear
                                      && row.AuditScheduleOnID == ScheduledOnID && row.AStatusId == 5
                                      && row.UserID == user.ID
                                      select row);

                    checkexists = emailQuery.Select(entry => true).FirstOrDefault();
                }
            }
            return checkexists;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static String GetSchedulingType(int CustBranchID, int processid, String FinYear, String Period, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Record = (from row in entities.InternalAuditSchedulings
                              where row.CustomerBranchId == CustBranchID
                              && row.Process == processid
                              && row.FinancialYear == FinYear
                              && row.TermName == Period
                              && row.AuditID == AuditID
                              select row.ISAHQMP).FirstOrDefault();

                if (Record != "")
                    return Record;
                else
                    return "";
            }
        }
        public static long GetPerfromer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalControlAuditAssignments
                                  join row1 in entities.InternalAuditScheduleOns
                                  on row.ProcessId equals row1.ProcessId
                                  where row.InternalAuditInstance == row1.InternalAuditInstance && row.ProcessId == processid &&
                                  row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
                                  && row1.ID == ScheduledOnID && row.RoleID == 3
                                  select row.UserID).FirstOrDefault();
                return (long)statusList;
            }
        }
        public static long GetPersonResponsible(int ATBDID, int processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.RiskActivityTransactions
                                  join row1 in entities.RiskActivityToBeDoneMappings
                                  on row.Id equals row1.RiskActivityId
                                  where row.ProcessId == row1.ProcessId && row.SubProcessId == row1.SubProcessId &&
                                  row1.ID == ATBDID && row1.ProcessId == processid
                                  select row.PersonResponsible).FirstOrDefault();
                return (long)statusList;
            }
        }
        protected void upAuditDetails_Load(object sender, EventArgs e)
        {
        }
        public int GetQuarter(DateTime date)
        {
            if (date.Month >= 4 && date.Month <= 6)
                return 1;
            else if (date.Month >= 7 && date.Month <= 9)
                return 2;
            else if (date.Month >= 10 && date.Month <= 12)
                return 3;
            else
                return 4;

        }
        public string GetQuarterNameNew(string period)
        {
            string quartername = "";
            if (period == "Apr-Jun")
            {
                quartername = "Apr - Jun";
            }
            else if (period == "Jul-Sep")
            {
                quartername = "Jul - Sep";
            }
            else if (period == "Oct-Dec")
            {
                quartername = "Oct - Dec";
            }
            else if (period == "Jan-Mar")
            {
                quartername = "Jan - Mar";
            }
            return quartername;
        }
        public string GetQuarterName(int number)
        {
            string quartername = "";
            if (number == 1)
            {
                quartername = "Apr - Jun";
            }
            else if (number == 2)
            {
                quartername = "Jul - Sep";
            }
            else if (number == 3)
            {
                quartername = "Oct - Dec";
            }
            else if (number == 4)
            {
                quartername = "Jan - Mar";
            }
            return quartername;
        }
        public void CheckStatus(int processid, int Atbdid)
        {
            int userID = -1;
            userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            bool TotalCount = UserManagementRisk.CheckRiskData(processid, userID, Atbdid);
            if (TotalCount == true)
            {
                ICFRLINK.Visible = true;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var getriskCreationId = (
                                   from row in entities.RiskActivityToBeDoneMappings
                                   where row.ID == Atbdid
                                   select row.RiskCategoryCreationId
                            ).FirstOrDefault();
                    if (ddlQuarter.SelectedItem.Text != "Select Quater")
                    {
                        if (ddlQuarter.SelectedValue != "-1")
                        {
                            string period = ddlQuarter.SelectedItem.Text;
                            var sdata = UserManagementRisk.GetScheduledOnID(Convert.ToInt32(ddlFilterLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, period, getriskCreationId, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 4, processid);
                            if (sdata != null)
                            {
                                if (sdata.StatusId == 2)
                                {
                                    lblQutErrorMsg.Text = "";
                                    btnPopUpLink.Visible = true;
                                }
                                else
                                {
                                    btnPopUpLink.Visible = false;
                                    lblQutErrorMsg.Text = "No record fount.";
                                }
                            }
                            else
                            {
                                btnPopUpLink.Visible = false;
                                lblQutErrorMsg.Text = "No record fount.";
                            }
                        }
                    }
                }
            }
        }
        #region  Button Click Events
        private void BindddlQuater()
        {
            if (ddlQuarter.SelectedValue != null)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchID = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                else
                {
                    ProcessID = Convert.ToInt32(ViewState["Processed"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                int userID = -1;
                userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                ddlQuarter.DataTextField = "Name";
                ddlQuarter.DataValueField = "ID";
                ddlQuarter.DataSource = UserManagementRisk.GetQuarterList(userID, BranchID, ProcessID, FinYear, ATBDID, "R");
                ddlQuarter.DataBind();
                ddlQuarter.Items.Insert(0, new ListItem("Select Quater", "-1"));
            }
        }
        protected void btnPopUpLink_Click(object sender, EventArgs e)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                else
                {
                    ProcessID = Convert.ToInt32(ViewState["Processed"]);
                }
                var getriskCreationId = (
                                    from row in entities.RiskActivityToBeDoneMappings
                                    where row.ID == ATBDID
                                    select row.RiskCategoryCreationId
                             ).FirstOrDefault();
                if (ddlPeriod.SelectedValue != "-1" && ddlPeriod.SelectedValue.ToString() != "Select Period")
                {
                    string period = ddlQuarter.SelectedItem.Text;
                    var sdata = UserManagementRisk.GetScheduledOnID(Convert.ToInt32(ddlFilterLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, period, getriskCreationId, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 4, ProcessID);
                    if (sdata != null)
                    {
                        if (sdata.StatusId == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowDialog('" + sdata.ScheduledOnID + "','" + sdata.RiskCreationID + "','" + sdata.FinancialYear + "','" + Convert.ToInt32(ddlFilterLocation.SelectedValue) + "','" + sdata.ForMonth + "');", true);
                        }
                    }
                }
            }
        }
        protected void btnNext1_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleonDetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getScheduleonDetails.ID,
                        ProcessId = getScheduleonDetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                    };

                    if (tbxTable.Text.Trim() == "")
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();

                    MstRiskResult.BriefObservation = tbxBriefObservation.Text;
                    MstRiskResult.ObjBackground = tbxObjBackground.Text;

                    if (tbxUHComment.Text.Trim() == "")
                        MstRiskResult.UHComment = "";
                    else
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();

                    if (tbxPresidentComment.Text.Trim() == "")
                        MstRiskResult.PRESIDENTComment = "";
                    else
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }
                    //End

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                    {
                        if (txtpopulation.Text.Length > 200)
                        {
                            MstRiskResult.Population = txtpopulation.Text.Substring(0, 200);
                        }
                        else
                        {
                            MstRiskResult.Population = txtpopulation.Text;
                        }
                    }

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();

                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());


                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                    {
                        responseDueDate = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.ResponseDueDate = responseDueDate.Date;
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                            MstRiskResult.PersonResponsible = null;
                        else
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                            MstRiskResult.Owner = null;
                        else
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {

                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {

                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }
                    bool Success = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                        {
                            if (ddlFilterStatus.SelectedValue == "4")
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                MstRiskResult.AStatusId = 4;
                                // MstRiskResult.AuditeeResponse = "RT";
                                MstRiskResult.AuditeeResponse = AuditeeResponse;
                                Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "6")
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                MstRiskResult.AStatusId = 6;
                                //MstRiskResult.AuditeeResponse = "RS";
                                MstRiskResult.AuditeeResponse = AuditeeResponse;
                                Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);

                            }
                            else if (ddlFilterStatus.SelectedValue == "5")
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                MstRiskResult.AStatusId = 5;
                                //MstRiskResult.AuditeeResponse = "RF";
                                MstRiskResult.AuditeeResponse = AuditeeResponse;
                                Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            }
                            else
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }
                    if (Success)
                    {
                        Tab2_Click(sender, e);
                    }
                }
            }


            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnNext2_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleonDetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getScheduleonDetails.ID,
                        ProcessId = getScheduleonDetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                    };
                    if (txtpopulation.Text.Length > 200)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Population should not greater than 200 characters.";
                        return;
                    }

                    if (tbxTable.Text.Trim() == "")
                    {
                        MstRiskResult.BodyContent = "";
                    }
                    else
                    {
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();
                    }

                    MstRiskResult.BriefObservation = tbxBriefObservation.Text;
                    MstRiskResult.ObjBackground = tbxObjBackground.Text;

                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }

                    //End

                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                    {
                        if (txtpopulation.Text.Length > 200)
                        {
                            MstRiskResult.Population = txtpopulation.Text.Substring(0, 200);
                        }
                        else
                        {
                            MstRiskResult.Population = txtpopulation.Text;
                        }
                    }

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();

                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());


                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                    {
                        responseDueDate = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.ResponseDueDate = responseDueDate.Date;
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {

                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }

                    bool Success1 = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                        {
                            if (ddlFilterStatus.SelectedValue == "4")
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                MstRiskResult.AStatusId = 4;
                                //MstRiskResult.AuditeeResponse = "RT";
                                MstRiskResult.AuditeeResponse = AuditeeResponse;
                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "6")
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                MstRiskResult.AStatusId = 6;
                                //MstRiskResult.AuditeeResponse = "RA";
                                MstRiskResult.AuditeeResponse = AuditeeResponse;
                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "5")
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                MstRiskResult.AStatusId = 5;
                                //MstRiskResult.AuditeeResponse = "RF";
                                MstRiskResult.AuditeeResponse = AuditeeResponse;
                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                            else
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }
                    if (Success1)
                    {
                        Tab3_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Oops....Error Occured During Saving, Please Try Again";
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnNext3_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                long roleid = 4;
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleonDetails != null)
                {
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getScheduleonDetails.ID,
                        ProcessId = getScheduleonDetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                        AnnexueTitle = txtAnnexueTitle.Text.Trim()
                    };
                    DateTime dt1 = new DateTime();
                    ObservationHistory objHistory = new ObservationHistory()
                    {
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        RoleID = roleid,
                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        ATBTID = ATBDID
                    };
                    if (ObjReport1.SelectedItem != null)
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                        objHistory.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);

                    }
                    objHistory.ObservationOld = hidObservation.Value;
                    objHistory.ProcessWalkthroughOld = hidObservation.Value;
                    objHistory.ActualWorkDoneOld = hidActualWorkDone.Value;
                    objHistory.PopulationOld = hidpopulation.Value;
                    objHistory.SampleOld = hidSample.Value;
                    objHistory.ObservationTitleOld = hidObservationTitle.Value;
                    objHistory.RiskOld = hidRisk.Value;
                    objHistory.RootCauseOld = hidRootcost.Value;
                    objHistory.FinancialImpactOld = hidfinancialImpact.Value;
                    objHistory.RecommendationOld = hidRecommendation.Value;
                    objHistory.ManagementResponseOld = hidMgtResponse.Value;
                    objHistory.RemarksOld = hidRemarks.Value;
                    objHistory.ScoreOld = hidAuditStepScore.Value;
                    //New Field

                    //Code added by Sushant
                    //objHistory.AnnexueTitleOld = hidAnnaxureTitle.Value;
                    //objHistory.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                    DateTime responseDueDatehist = new DateTime();
                    if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                    {
                        responseDueDatehist = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        objHistory.ResponseDueDate = responseDueDatehist.Date;
                    }
                    if (!string.IsNullOrEmpty(hidResponseDueDate.Value))
                    {
                        DateTime responsedate = Convert.ToDateTime(hidResponseDueDate.Value);
                        objHistory.ResponseDueDateOld = responsedate;
                    }
                    objHistory.OldBodyContent = hidBodyContent.Value;
                    objHistory.OldUHComment = hidUHComment.Value;
                    objHistory.OldPRESIDENTComment = hidPRESIDENTComment.Value;
                    if (!string.IsNullOrEmpty(hidUHPersonResponsible.Value))
                    {
                        objHistory.OldUHPersonResponsible = Convert.ToInt32(hidUHPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidPRESIDENTPersonResponsible.Value))
                    {
                        objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(hidPRESIDENTPersonResponsible.Value);
                    }

                    if (txtpopulation.Text.Length > 200)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Population should not greater than 200 characters.";
                        return;
                    }

                    if (tbxTable.Text.Trim() == "")
                    {
                        MstRiskResult.BodyContent = "";
                        objHistory.BodyContent = "";
                    }
                    else
                    {
                        MstRiskResult.BodyContent = tbxTable.Text.Trim();
                        objHistory.BodyContent = tbxTable.Text.Trim();
                    }

                    if (tbxUHComment.Text.Trim() == "")
                    {
                        MstRiskResult.UHComment = "";
                        objHistory.UHComment = "";
                    }
                    else
                    {
                        MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                        objHistory.UHComment = tbxUHComment.Text.Trim();
                    }

                    if (tbxPresidentComment.Text.Trim() == "")
                    {
                        MstRiskResult.PRESIDENTComment = "";
                        objHistory.PRESIDENTComment = "";
                    }
                    else
                    {
                        MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                        objHistory.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                    {
                        if (ddlPersonresponsibleUH.SelectedValue == "-1")
                        {
                            MstRiskResult.UHPersonResponsible = null;
                            objHistory.UHPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                            objHistory.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                    {
                        if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = null;
                            objHistory.PRESIDENTPersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                            objHistory.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                        }
                    }

                    //End

                    if (!string.IsNullOrEmpty(tbxBriefObservation.Text.Trim()))
                    {
                        MstRiskResult.BriefObservation = tbxBriefObservation.Text.Trim();
                        // transaction.BriefObservation = tbxBriefObservation.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.BriefObservation = null;
                        // transaction.BriefObservation = null;
                    }
                    if (!string.IsNullOrEmpty(tbxObjBackground.Text.Trim()))
                    {
                        MstRiskResult.ObjBackground = tbxObjBackground.Text.Trim();
                    }
                    else
                    {
                        MstRiskResult.ObjBackground = null;
                    }
                    if (ddlDefeciencyType.SelectedValue != "-1")
                    {
                        MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        // transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                    }
                    else
                    {
                        MstRiskResult.DefeciencyType = null;
                        // transaction.DefeciencyType = null;
                    }

                    if (!string.IsNullOrEmpty(hidPersonResponsible.Value))
                    {
                        objHistory.PersonResponsibleOld = Convert.ToInt32(hidPersonResponsible.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationRating.Value))
                    {
                        objHistory.ObservationRatingOld = Convert.ToInt32(hidObservationRating.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationCategory.Value))
                    {
                        objHistory.ObservationCategoryOld = Convert.ToInt32(hidObservationCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidObservationSubCategory.Value))
                    {
                        objHistory.ObservationSubCategoryOld = Convert.ToInt32(hidObservationSubCategory.Value);
                    }
                    if (!string.IsNullOrEmpty(hidOwner.Value))
                    {
                        objHistory.OwnerOld = Convert.ToInt32(hidOwner.Value);
                    }
                    if (!string.IsNullOrEmpty(hidUserID.Value))
                    {
                        objHistory.UserOld = Convert.ToInt32(hidUserID.Value);
                    }
                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }
                    if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                    {
                        objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                    }
                    DateTime dt = new DateTime();
                    dt = new DateTime();
                    if (!string.IsNullOrEmpty(hidTimeLine.Value))
                    {
                        dt = DateTime.ParseExact(hidTimeLine.Value, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLineOld = dt.Date;
                    }

                    objHistory.Remarks = tbxRemarks.Text;
                    dt1 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt1 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        objHistory.TimeLine = dt1.Date;
                    }

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                    {
                        responseDueDate = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.ResponseDueDate = responseDueDate.Date;
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }
                    objHistory.AuditId = AuditID;


                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                    {
                        MstRiskResult.ProcessWalkthrough = "";
                        objHistory.ProcessWalkthrough = "";
                    }
                    else
                    {
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                        objHistory.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                    }

                    if (txtActualWorkDone.Text.Trim() == "")
                    {
                        MstRiskResult.ActivityToBeDone = "";
                        objHistory.ActualWorkDone = "";
                    }
                    else
                    {
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();
                        objHistory.ActualWorkDone = txtActualWorkDone.Text.Trim();
                    }


                    if (txtpopulation.Text.Trim() == "")
                    {
                        MstRiskResult.Population = "";
                        objHistory.Population = "";
                    }
                    else
                    {
                        if (txtpopulation.Text.Length > 200)
                        {
                            MstRiskResult.Population = txtpopulation.Text.Substring(0, 200);
                            objHistory.Population = txtpopulation.Text.Substring(0, 200);
                        }
                        else
                        {
                            MstRiskResult.Population = txtpopulation.Text;
                            objHistory.Population = txtpopulation.Text;
                        }
                    }
                    if (txtSample.Text.Trim() == "")
                    {
                        MstRiskResult.Sample = "";
                        objHistory.Sample = "";
                    }
                    else
                    {
                        MstRiskResult.Sample = txtSample.Text.Trim();
                        objHistory.Sample = txtSample.Text.Trim();
                    }

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                    {
                        MstRiskResult.ObservationTitle = "";
                        objHistory.ObservationTitle = "";
                    }
                    else
                    {
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();
                        objHistory.ObservationTitle = txtObservationTitle.Text;
                    }
                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                    {
                        MstRiskResult.Observation = "";
                        objHistory.Observation = null;
                    }
                    else
                    {
                        MstRiskResult.Observation = txtObservation.Text.Trim();
                        objHistory.Observation = txtObservation.Text.Trim();
                    }
                    if (txtRisk.Text.Trim() == "")
                    {
                        MstRiskResult.Risk = "";
                        objHistory.Risk = "";
                    }
                    else
                    {
                        MstRiskResult.Risk = txtRisk.Text.Trim();
                        objHistory.Risk = txtRisk.Text;
                    }

                    if (txtRootcost.Text.Trim() == "")
                    {
                        MstRiskResult.RootCost = null;
                        objHistory.RootCause = null;
                    }
                    else
                    {
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();
                        objHistory.RootCause = txtRootcost.Text.Trim();
                    }

                    if (txtAuditStepScore.Text.Trim() == "")
                    {
                        MstRiskResult.AuditScores = null;
                        objHistory.Score = null;
                    }
                    else
                    {
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());
                        objHistory.Score = txtAuditStepScore.Text;
                    }

                    if (txtfinancialImpact.Text.Trim() == "")
                    {
                        MstRiskResult.FinancialImpact = null;
                        objHistory.FinancialImpact = null;
                    }
                    else
                    {
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();
                        objHistory.FinancialImpact = txtfinancialImpact.Text;
                    }
                    if (txtRecommendation.Text.Trim() == "")
                    {
                        MstRiskResult.Recomendation = "";
                        objHistory.Recommendation = "";
                    }
                    else
                    {
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();
                        objHistory.Recommendation = txtRecommendation.Text.Trim();
                    }

                    if (txtMgtResponse.Text.Trim() == "")
                    {
                        MstRiskResult.ManagementResponse = "";
                        objHistory.ManagementResponse = "";
                    }
                    else
                    {
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                        objHistory.ManagementResponse = txtMgtResponse.Text.Trim();
                    }

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt3 = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt3 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt3.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            objHistory.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            objHistory.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            objHistory.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            objHistory.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    //need to check
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                            objHistory.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            objHistory.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                            objHistory.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            objHistory.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                            objHistory.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            objHistory.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }


                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    bool Success1 = false;
                    if (ddlFilterStatus.SelectedValue == "4")
                    {
                        MstRiskResult.AStatusId = 4;
                        //MstRiskResult.AuditeeResponse = "RT";
                        MstRiskResult.AuditeeResponse = AuditeeResponse;
                    }
                    else if (ddlFilterStatus.SelectedValue == "6")
                    {
                        MstRiskResult.AStatusId = 6;
                        //MstRiskResult.AuditeeResponse = "RA";
                        MstRiskResult.AuditeeResponse = AuditeeResponse;
                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        MstRiskResult.AStatusId = 6;
                        //MstRiskResult.AuditeeResponse = "RF";
                        MstRiskResult.AuditeeResponse = AuditeeResponse;
                    }
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }

                    if (!string.IsNullOrEmpty(objHistory.Observation))
                    {
                        if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                        {
                            objHistory.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            objHistory.CreatedOn = DateTime.Now;
                            RiskCategoryManagement.AddObservationHistory(objHistory);
                        }
                    }
                    if (Success1)
                    {
                        BindTransactions(Convert.ToInt32(getScheduleonDetails.ID), ATBDID, AuditID);
                        bool checkPRFlag = false;
                        // int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                        var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                        if (checkPRFlag == true)
                        {
                            BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                        }
                        else
                        {
                            BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                        }

                        Tab4_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);

                    if (Success1)
                    {
                        if (txtMultilineVideolink.Text.Trim() != "")
                        {
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                            if (observationAudioVideoList.Count > 0)
                            {
                                // code for user delete one or multiple links and txtMultilineVideolink text box has atleast one value.
                                string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                if (filtered.Count > 0)
                                {
                                    foreach (var item in filtered)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                    }
                                }
                            }

                            ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                            {
                                AuditId = AuditID,
                                ATBTID = ATBDID,
                                IsActive = true,
                            };

                            if (txtMultilineVideolink.Text.Contains(',')) //checking entered single value or multiple values
                            {
                                string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                int lenght = audioVideoLinks.Length;
                                string link = string.Empty;
                                for (int i = 0; i < audioVideoLinks.Length; i++)
                                {
                                    link = audioVideoLinks[i].ToString();
                                    objObservationAudioVideo.AudioVideoLink = link.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                {
                                    objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                    objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                }
                            }
                        }
                        else
                        {
                            // when user want delete all existing link then we will get null from txtMultilineVideolink
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                }

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);

                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                int LogedInUserRole = UserManagementRisk.CheckAssignedRoleOfUser(AuditID, Portal.Common.AuthenticationHelper.UserID);
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(txtObservation.Text.Trim()))
                {
                    if (string.IsNullOrEmpty(txtObservationTitle.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Observation Title.";
                        MainView.ActiveViewIndex = 2;
                        txtObservationTitle.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRisk.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Business Implication.";
                        MainView.ActiveViewIndex = 2;
                        txtRisk.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRootcost.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Root Cause.";
                        MainView.ActiveViewIndex = 2;
                        txtRootcost.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtfinancialImpact.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Financial Impact.";
                        MainView.ActiveViewIndex = 2;
                        txtfinancialImpact.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtRecommendation.Text.Trim()))
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter Recommendation.";
                        MainView.ActiveViewIndex = 2;
                        txtRecommendation.Focus();
                        return;
                    }
                    //else if (string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    else if (string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue) || ddlPersonresponsible.SelectedValue == "-1")
                    {
                        //if (ddlPersonresponsible.SelectedValue == "-1")
                        //{
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Person Responsible.";
                        MainView.ActiveViewIndex = 2;
                        return;
                        //}
                    }
                    //else if (string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    else if (string.IsNullOrEmpty(ddlOwnerName.SelectedValue) || ddlOwnerName.SelectedValue == "-1")
                    {
                        //if (ddlOwnerName.SelectedValue == "-1")
                        //{
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Owner.";
                        MainView.ActiveViewIndex = 2;
                        return;
                        //}
                    }
                    else if (string.IsNullOrEmpty(ddlobservationRating.SelectedValue) || ddlobservationRating.SelectedValue == "-1")
                    {

                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Observation Rating.";
                        MainView.ActiveViewIndex = 2;
                        return;
                    }
                    else if (string.IsNullOrEmpty(ddlObservationCategory.SelectedValue) || ddlObservationCategory.SelectedValue == "-1")
                    {

                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Select Observation Category.";
                        MainView.ActiveViewIndex = 2;
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtTimeLine.Text.Trim()) && rdbtnStatus.SelectedItem.Text == "Closed")
                    {
                        CustomValidator2.IsValid = false;
                        CustomValidator2.ErrorMessage = "Please Enter TimeLine.";
                        MainView.ActiveViewIndex = 2;
                        txtTimeLine.Focus();
                        return;
                    }
                }

                if (txtpopulation.Text.Length > 200)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Population should not greater than 200 characters.";
                    return;
                }

                bool Flag = false;
                long roleid = 4;

                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                bool SubmitValue = false;
                List<long> ProcessList = new List<long>();
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleonDetails != null)
                {
                    ProcessList.Add(getScheduleonDetails.ProcessId);
                    var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(AuditID, Portal.Common.AuthenticationHelper.UserID, ProcessList);
                    var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(AuditID, ProcessList);

                    if (rdbtnStatus.SelectedValue != "")
                    {
                        string Currentstatus = ddlFilterStatus.SelectedItem.Text;
                        if (RoleListInAudit.Count() == 3 && rdbtnStatus.SelectedItem.Text == "Closed" && (Currentstatus == "Submitted" || Currentstatus == "TeamReview" || Currentstatus == "AuditeeReview"))
                        {
                            SubmitValue = false;
                        }
                        else if (RoleListInAudit.Count() == 2 && rdbtnStatus.SelectedItem.Text == "Final Review")
                        {
                            SubmitValue = false;
                        }
                        else
                        {
                            SubmitValue = true;
                        }
                        if (SubmitValue)
                        {
                            #region 
                            int StatusID = 0;
                            if (lblStatus.Visible)
                                StatusID = Convert.ToInt32(rdbtnStatus.SelectedValue);
                            InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                            {
                                AuditScheduleOnID = getScheduleonDetails.ID,
                                ProcessId = getScheduleonDetails.ProcessId,
                                FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                ForPerid = ddlPeriod.SelectedItem.Text,
                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                IsDeleted = false,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                ATBDId = ATBDID,
                                RoleID = roleid,
                                InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                VerticalID = VerticalID,
                                AuditID = AuditID,
                                AnnexueTitle = txtAnnexueTitle.Text.Trim()
                            };
                            InternalAuditTransaction transaction = new InternalAuditTransaction()
                            {
                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                                AuditScheduleOnID = getScheduleonDetails.ID,
                                FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                ProcessId = getScheduleonDetails.ProcessId,
                                SubProcessId = -1,
                                ForPeriod = ddlPeriod.SelectedItem.Text,
                                ATBDId = ATBDID,
                                RoleID = roleid,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                VerticalID = VerticalID,
                                AuditID = AuditID,
                            };
                            //added by Sushant
                            ComplianceManagement.Business.DataRisk.AuditClosure AuditClosureResult = new ComplianceManagement.Business.DataRisk.AuditClosure()
                            {
                                ProcessId = getScheduleonDetails.ProcessId,
                                FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                ForPeriod = ddlPeriod.SelectedItem.Text,
                                CustomerbranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                ACStatus = 1,
                                AuditCommiteRemark = "",
                                ATBDId = ATBDID,
                                VerticalID = VerticalID,
                                AuditCommiteFlag = 0,
                                AuditID = AuditID,
                                AnnexueTitle = txtAnnexueTitle.Text.Trim()
                            };
                            DateTime dt1 = new DateTime();
                            ObservationHistory objHistory = new ObservationHistory()
                            {
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                RoleID = roleid,
                                CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                ATBTID = ATBDID
                            };
                            if (ObjReport1.SelectedItem != null)
                            {
                                MstRiskResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                                objHistory.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                                AuditClosureResult.ISACPORMIS = Convert.ToInt16(ObjReport1.SelectedValue);
                            }
                            objHistory.ObservationOld = hidObservation.Value;
                            objHistory.ProcessWalkthroughOld = hidObservation.Value;
                            objHistory.ActualWorkDoneOld = hidActualWorkDone.Value;
                            objHistory.PopulationOld = hidpopulation.Value;
                            objHistory.SampleOld = hidSample.Value;
                            objHistory.ObservationTitleOld = hidObservationTitle.Value;
                            objHistory.RiskOld = hidRisk.Value;
                            objHistory.RootCauseOld = hidRootcost.Value;
                            objHistory.FinancialImpactOld = hidfinancialImpact.Value;
                            objHistory.RecommendationOld = hidRecommendation.Value;
                            objHistory.ManagementResponseOld = hidMgtResponse.Value;
                            objHistory.RemarksOld = hidRemarks.Value;
                            objHistory.ScoreOld = hidAuditStepScore.Value;
                            //New Field

                            //Code added by Sushant
                            //objHistory.AnnexueTitleOld = hidAnnaxureTitle.Value;
                            //objHistory.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                            DateTime responseDueDatehist = new DateTime();
                            if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                            {
                                responseDueDatehist = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                objHistory.ResponseDueDate = responseDueDatehist.Date;
                            }
                            if (!string.IsNullOrEmpty(hidResponseDueDate.Value))
                            {
                                DateTime responsedate = Convert.ToDateTime(hidResponseDueDate.Value);
                                objHistory.ResponseDueDateOld = responsedate;
                            }
                            objHistory.OldBodyContent = hidBodyContent.Value;
                            objHistory.OldUHComment = hidUHComment.Value;
                            objHistory.OldPRESIDENTComment = hidPRESIDENTComment.Value;
                            if (!string.IsNullOrEmpty(hidUHPersonResponsible.Value))
                            {
                                objHistory.OldUHPersonResponsible = Convert.ToInt32(hidUHPersonResponsible.Value);
                            }
                            if (!string.IsNullOrEmpty(hidPRESIDENTPersonResponsible.Value))
                            {
                                objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(hidPRESIDENTPersonResponsible.Value);
                            }


                            if (tbxTable.Text.Trim() == "")
                            {
                                MstRiskResult.BodyContent = "";
                                objHistory.BodyContent = "";
                                transaction.BodyContent = "";
                                AuditClosureResult.BodyContent = "";
                            }
                            else
                            {
                                MstRiskResult.BodyContent = tbxTable.Text.Trim();
                                objHistory.BodyContent = tbxTable.Text.Trim();
                                transaction.BodyContent = tbxTable.Text.Trim();
                                AuditClosureResult.BodyContent = tbxTable.Text.Trim();
                            }

                            if (txtAnnexueTitle.Text.Trim() == "")
                            {
                                MstRiskResult.AnnexueTitle = "";
                                AuditClosureResult.AnnexueTitle = "";
                            }
                            else
                            {
                                if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                                {
                                    MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                                    AuditClosureResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                                }
                                else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                                    return;
                                }
                            }

                            if (tbxUHComment.Text.Trim() == "")
                            {
                                MstRiskResult.UHComment = "";
                                objHistory.UHComment = "";
                                transaction.UHComment = "";
                                AuditClosureResult.UHComment = "";
                            }
                            else
                            {
                                MstRiskResult.UHComment = tbxUHComment.Text.Trim();
                                objHistory.UHComment = tbxUHComment.Text.Trim();
                                transaction.UHComment = tbxUHComment.Text.Trim();
                                AuditClosureResult.UHComment = tbxUHComment.Text.Trim();
                            }
                            if (!string.IsNullOrEmpty(tbxBriefObservation.Text.Trim()))
                            {
                                MstRiskResult.BriefObservation = tbxBriefObservation.Text.Trim();
                                transaction.BriefObservation = tbxBriefObservation.Text.Trim();
                                AuditClosureResult.BriefObservation = tbxBriefObservation.Text.Trim();
                            }
                            else
                            {
                                MstRiskResult.BriefObservation = null;
                                transaction.BriefObservation = null;
                                AuditClosureResult.BriefObservation = null;
                            }
                            if (!string.IsNullOrEmpty(tbxObjBackground.Text.Trim()))
                            {
                                MstRiskResult.ObjBackground = tbxObjBackground.Text.Trim();
                                transaction.ObjBackground = tbxObjBackground.Text.Trim();
                                AuditClosureResult.ObjBackground = tbxObjBackground.Text.Trim();
                            }
                            else
                            {
                                MstRiskResult.ObjBackground = null;
                                transaction.ObjBackground = null;
                                AuditClosureResult.ObjBackground = null;
                            }
                            if (ddlDefeciencyType.SelectedValue != "-1")
                            {
                                MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                AuditClosureResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                            }
                            else
                            {
                                MstRiskResult.DefeciencyType = null;
                                transaction.DefeciencyType = null;
                                AuditClosureResult.DefeciencyType = null;
                            }
                            if (tbxPresidentComment.Text.Trim() == "")
                            {
                                MstRiskResult.PRESIDENTComment = "";
                                objHistory.PRESIDENTComment = "";
                                transaction.PRESIDENTComment = "";
                                AuditClosureResult.PRESIDENTComment = "";
                            }
                            else
                            {
                                MstRiskResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                                objHistory.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                                transaction.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                                AuditClosureResult.PRESIDENTComment = tbxPresidentComment.Text.Trim();
                            }

                            if (!string.IsNullOrEmpty(ddlPersonresponsibleUH.SelectedValue))
                            {
                                if (ddlPersonresponsibleUH.SelectedValue == "-1")
                                {
                                    MstRiskResult.UHPersonResponsible = null;
                                    objHistory.UHPersonResponsible = null;
                                    transaction.UHPersonResponsible = null;
                                    AuditClosureResult.UHPersonResponsible = null;
                                }
                                else
                                {
                                    MstRiskResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                    objHistory.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                    transaction.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                    AuditClosureResult.UHPersonResponsible = Convert.ToInt32(ddlPersonresponsibleUH.SelectedValue);
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlPersonresponsiblePresident.SelectedValue))
                            {
                                if (ddlPersonresponsiblePresident.SelectedValue == "-1")
                                {
                                    MstRiskResult.PRESIDENTPersonResponsible = null;
                                    objHistory.PRESIDENTPersonResponsible = null;
                                    transaction.PRESIDENTPersonResponsible = null;
                                    AuditClosureResult.PRESIDENTPersonResponsible = null;
                                }
                                else
                                {
                                    MstRiskResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                    objHistory.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                    transaction.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                    AuditClosureResult.PRESIDENTPersonResponsible = Convert.ToInt32(ddlPersonresponsiblePresident.SelectedValue);
                                }
                            }

                            //End  

                            if (!string.IsNullOrEmpty(hidPersonResponsible.Value))
                            {
                                objHistory.PersonResponsibleOld = Convert.ToInt32(hidPersonResponsible.Value);
                            }
                            if (!string.IsNullOrEmpty(hidObservationRating.Value))
                            {
                                objHistory.ObservationRatingOld = Convert.ToInt32(hidObservationRating.Value);
                            }
                            if (!string.IsNullOrEmpty(hidObservationCategory.Value))
                            {
                                objHistory.ObservationCategoryOld = Convert.ToInt32(hidObservationCategory.Value);
                            }
                            if (!string.IsNullOrEmpty(hidObservationSubCategory.Value))
                            {
                                objHistory.ObservationSubCategoryOld = Convert.ToInt32(hidObservationSubCategory.Value);
                            }
                            if (!string.IsNullOrEmpty(hidOwner.Value))
                            {
                                objHistory.OwnerOld = Convert.ToInt32(hidOwner.Value);
                            }
                            if (!string.IsNullOrEmpty(hidUserID.Value))
                            {
                                objHistory.UserOld = Convert.ToInt32(hidUserID.Value);
                            }
                            if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                            {
                                objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                            }
                            if (!string.IsNullOrEmpty(hidISACPORMIS.Value))
                            {
                                objHistory.ISACPORMISOld = Convert.ToInt32(hidISACPORMIS.Value);
                            }
                            DateTime dt = new DateTime();
                            dt = new DateTime();
                            if (!string.IsNullOrEmpty(hidTimeLine.Value))
                            {
                                dt = DateTime.ParseExact(hidTimeLine.Value, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                                objHistory.TimeLineOld = dt.Date;
                            }
                            objHistory.AuditId = AuditID;
                            objHistory.Remarks = tbxRemarks.Text;
                            dt1 = new DateTime();
                            if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                            {
                                dt1 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                                objHistory.TimeLine = dt1.Date;
                            }

                            DateTime responseDueDate = new DateTime();
                            if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                            {
                                responseDueDate = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.ResponseDueDate = responseDueDate.Date;
                                transaction.ResponseDueDate = responseDueDate.Date;
                            }
                            else
                            {
                                MstRiskResult.ResponseDueDate = null;
                            }

                            string remark = string.Empty;
                            if (rdbtnStatus.SelectedItem.Text == "Closed")
                            {
                                transaction.StatusId = 3;
                                MstRiskResult.AStatusId = 3;
                                remark = "Audit Steps Closed.";
                                MstRiskResult.AuditeeResponse = "AC";
                            }
                            else if (rdbtnStatus.SelectedItem.Text == "Team Review")
                            {
                                transaction.StatusId = 4;
                                MstRiskResult.AStatusId = 4;
                                remark = "Audit Steps Under Team Review.";
                                MstRiskResult.AuditeeResponse = "RT";
                            }
                            else if (rdbtnStatus.SelectedItem.Text == "Final Review")
                            {
                                transaction.StatusId = 5;
                                MstRiskResult.AStatusId = 5;
                                remark = "Audit Steps Under Final Review.";
                                if (LogedInUserRole == 4)
                                {
                                    MstRiskResult.AuditeeResponse = "RF";
                                }
                                else if (LogedInUserRole == 5)
                                {
                                    MstRiskResult.AuditeeResponse = "HF";
                                }
                            }
                            else if (rdbtnStatus.SelectedItem.Text == "Auditee Review")
                            {
                                if (MstRiskResult != null)
                                {
                                    var AuditteeStatusID = RiskCategoryManagement.GetLatestStatusOfAudittee(MstRiskResult);
                                    if (AuditteeStatusID == 6)
                                    {
                                        var AuditteeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                                        if (AuditteeResponse == "RS")
                                        {
                                            MstRiskResult.AuditeeResponse = "RS";
                                        }
                                        else
                                        {
                                            MstRiskResult.AuditeeResponse = "RA";
                                        }
                                    }
                                    else
                                    {
                                        MstRiskResult.AuditeeResponse = "RS";
                                    }
                                }
                                transaction.StatusId = 6;
                                MstRiskResult.AStatusId = 6;
                                remark = "Audit Steps Under Auditee Review.";
                            }

                            if (txtAuditObjective.Text.Trim() == "")
                                MstRiskResult.AuditObjective = null;
                            else
                                MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                            if (txtAuditSteps.Text.Trim() == "")
                                MstRiskResult.AuditSteps = "";
                            else
                                MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                            if (txtAnalysisToBePerformed.Text.Trim() == "")
                                MstRiskResult.AnalysisToBePerofrmed = "";
                            else
                                MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                            if (txtWalkthrough.Text.Trim() == "")
                            {
                                MstRiskResult.ProcessWalkthrough = "";
                                objHistory.ProcessWalkthrough = "";
                            }
                            else
                            {
                                MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                                objHistory.ProcessWalkthrough = txtWalkthrough.Text.Trim();
                            }

                            if (txtActualWorkDone.Text.Trim() == "")
                            {
                                MstRiskResult.ActivityToBeDone = "";
                                objHistory.ActualWorkDone = "";
                            }
                            else
                            {
                                MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();
                                objHistory.ActualWorkDone = txtActualWorkDone.Text.Trim();
                            }
                            if (txtpopulation.Text.Trim() == "")
                            {
                                MstRiskResult.Population = "";
                                objHistory.Population = "";
                            }
                            else
                            {
                                if (txtpopulation.Text.Length > 200)
                                {
                                    MstRiskResult.Population = txtpopulation.Text.Substring(0, 200);
                                    objHistory.Population = txtpopulation.Text.Substring(0, 200);
                                }
                                else
                                {
                                    MstRiskResult.Population = txtpopulation.Text;
                                    objHistory.Population = txtpopulation.Text;
                                }
                            }
                            if (txtSample.Text.Trim() == "")
                            {
                                MstRiskResult.Sample = "";
                                objHistory.Sample = "";
                            }
                            else
                            {
                                MstRiskResult.Sample = txtSample.Text.Trim();
                                objHistory.Sample = txtSample.Text.Trim();
                            }

                            if (txtObservationNumber.Text.Trim() == "")
                            {
                                MstRiskResult.ObservationNumber = "";
                                AuditClosureResult.ObservationNumber = "";
                            }
                            else
                            {
                                MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();
                                AuditClosureResult.ObservationNumber = txtObservationNumber.Text.Trim();
                            }

                            if (txtObservationTitle.Text.Trim() == "")
                            {
                                MstRiskResult.ObservationTitle = "";
                                objHistory.ObservationTitle = "";
                                AuditClosureResult.ObservationTitle = "";
                            }
                            else
                            {
                                MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();
                                objHistory.ObservationTitle = txtObservationTitle.Text;
                                AuditClosureResult.ObservationTitle = txtObservationTitle.Text;
                            }

                            if (txtAnnexueTitle.Text.Trim() == "")
                            {
                                MstRiskResult.AnnexueTitle = "";
                                AuditClosureResult.AnnexueTitle = "";
                            }
                            else
                            {
                                if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                                {
                                    MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                                    AuditClosureResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                                }
                                else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                                    return;
                                }
                            }

                            if (txtObservation.Text.Trim() == "")
                            {
                                MstRiskResult.Observation = "";
                                objHistory.Observation = null;
                                AuditClosureResult.Observation = null;
                            }
                            else
                            {
                                MstRiskResult.Observation = txtObservation.Text.Trim();
                                objHistory.Observation = txtObservation.Text.Trim();
                                AuditClosureResult.Observation = txtObservation.Text.Trim();
                            }
                            if (txtRisk.Text.Trim() == "")
                            {
                                MstRiskResult.Risk = "";
                                objHistory.Risk = "";
                                AuditClosureResult.Risk = "";
                            }
                            else
                            {
                                MstRiskResult.Risk = txtRisk.Text.Trim();
                                objHistory.Risk = txtRisk.Text;
                                AuditClosureResult.Risk = txtRisk.Text;
                            }
                            if (txtRootcost.Text.Trim() == "")
                            {
                                MstRiskResult.RootCost = null;
                                objHistory.RootCause = null;
                                AuditClosureResult.RootCost = null;
                            }
                            else
                            {
                                MstRiskResult.RootCost = txtRootcost.Text.Trim();
                                objHistory.RootCause = txtRootcost.Text.Trim();
                                AuditClosureResult.RootCost = txtRootcost.Text.Trim();
                            }
                            if (txtAuditStepScore.Text.Trim() == "")
                            {
                                MstRiskResult.AuditScores = null;
                                objHistory.Score = null;
                            }
                            else
                            {
                                MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());
                                objHistory.Score = txtAuditStepScore.Text;
                            }

                            if (txtfinancialImpact.Text.Trim() == "")
                            {
                                MstRiskResult.FinancialImpact = null;
                                objHistory.FinancialImpact = null;
                                AuditClosureResult.FinancialImpact = null;
                            }
                            else
                            {
                                MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();
                                objHistory.FinancialImpact = txtfinancialImpact.Text;
                                AuditClosureResult.FinancialImpact = txtfinancialImpact.Text.Trim();
                            }
                            if (txtRecommendation.Text.Trim() == "")
                            {
                                MstRiskResult.Recomendation = "";
                                objHistory.Recommendation = "";
                                AuditClosureResult.Recomendation = "";
                            }
                            else
                            {
                                MstRiskResult.Recomendation = txtRecommendation.Text.Trim();
                                objHistory.Recommendation = txtRecommendation.Text.Trim();
                                AuditClosureResult.Recomendation = txtRecommendation.Text.Trim();
                            }
                            if (txtMgtResponse.Text.Trim() == "")
                            {
                                MstRiskResult.ManagementResponse = "";
                                objHistory.ManagementResponse = "";
                                AuditClosureResult.ManagementResponse = "";
                            }
                            else
                            {
                                MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();
                                objHistory.ManagementResponse = txtMgtResponse.Text.Trim();
                                AuditClosureResult.ManagementResponse = txtMgtResponse.Text.Trim();
                            }

                            if (tbxRemarks.Text.Trim() == "")
                                MstRiskResult.FixRemark = "";
                            else
                                MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                            DateTime dt4 = new DateTime();
                            if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                            {
                                dt4 = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.TimeLine = dt4.Date;
                                AuditClosureResult.TimeLine = dt4.Date;
                            }
                            else
                            {
                                MstRiskResult.TimeLine = null;
                                AuditClosureResult.TimeLine = null;
                            }
                            if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                            {
                                if (ddlPersonresponsible.SelectedValue == "-1")
                                {
                                    MstRiskResult.PersonResponsible = null;
                                    transaction.PersonResponsible = null;
                                    objHistory.PersonResponsible = null;
                                    AuditClosureResult.PersonResponsible = null;
                                }
                                else
                                {
                                    MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                    transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                    objHistory.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                    AuditClosureResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                            {
                                if (ddlOwnerName.SelectedValue == "-1")
                                {
                                    MstRiskResult.Owner = null;
                                    transaction.Owner = null;
                                    objHistory.Owner = null;
                                    AuditClosureResult.Owner = null;
                                }
                                else
                                {
                                    MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                    transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                    objHistory.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                    AuditClosureResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                            {
                                if (ddlobservationRating.SelectedValue == "-1")
                                {
                                    transaction.ObservatioRating = null;
                                    MstRiskResult.ObservationRating = null;
                                    objHistory.ObservationRating = null;
                                    AuditClosureResult.ObservationRating = null;
                                }
                                else
                                {
                                    transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                    MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                    objHistory.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                    AuditClosureResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                            {
                                if (ddlObservationCategory.SelectedValue == "-1")
                                {
                                    transaction.ObservationCategory = null;
                                    MstRiskResult.ObservationCategory = null;
                                    objHistory.ObservationCategory = null;
                                    AuditClosureResult.ObservationCategory = null;
                                }
                                else
                                {
                                    transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                    MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                    objHistory.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                    AuditClosureResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                            {
                                if (ddlObservationSubCategory.SelectedValue == "-1")
                                {
                                    transaction.ObservationSubCategory = null;
                                    MstRiskResult.ObservationSubCategory = null;
                                    objHistory.ObservationSubCategory = null;
                                    AuditClosureResult.ObservationSubCategory = null;
                                }
                                else
                                {
                                    transaction.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                    MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                    objHistory.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                    AuditClosureResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                                }
                            }
                            transaction.Remarks = remark.Trim();
                            if (!string.IsNullOrEmpty(ddlReviewerRiskRatiing.SelectedValue))
                            {
                                if (ddlReviewerRiskRatiing.SelectedValue == "-1")
                                {
                                    transaction.ReviewerRiskRating = null;
                                }
                                else
                                    transaction.ReviewerRiskRating = Convert.ToInt32(ddlReviewerRiskRatiing.SelectedValue);
                            }
                            bool Success1 = false;
                            bool Success2 = false;
                            if (rdbtnStatus.SelectedItem.Text == "Auditee Review")
                            {
                                if (string.IsNullOrEmpty(txtObservation.Text.Trim()))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "There is no observation, please select the other status.";
                                    return;
                                }
                            }
                            if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                            {
                                //if Exists With Status
                                // Submitted
                                if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 2 && (rdbtnStatus.SelectedItem.Text == "Team Review"))
                                {
                                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    }
                                }
                                else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 2 && (rdbtnStatus.SelectedItem.Text == "Auditee Review"))
                                {
                                    if (MstRiskResult.AStatusId == 6)
                                    {
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.CreatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 2 && (rdbtnStatus.SelectedItem.Text == "Final Review"))
                                {
                                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    }
                                }// Team Review
                                else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 && (rdbtnStatus.SelectedItem.Text == "Auditee Review"))
                                {
                                    if (MstRiskResult.AStatusId == 6)
                                    {
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.CreatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 && (rdbtnStatus.SelectedItem.Text == "Final Review"))
                                {
                                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    }
                                }//Auditee Review
                                else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6 && (rdbtnStatus.SelectedItem.Text == "Final Review"))
                                {
                                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    }
                                }
                                else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6 && (rdbtnStatus.SelectedItem.Text == "Auditee Review"))
                                {
                                    if (MstRiskResult.AStatusId == 6)
                                    {
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.CreatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                }
                                else
                                {
                                    if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    }
                                    else
                                    {
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.CreatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                    }
                                    //Code added by Sushant
                                    if (rdbtnStatus.SelectedItem.Text == "Closed")
                                    {
                                        if (RiskCategoryManagement.InternalControlResultClosedCount(MstRiskResult) == 1)
                                        {
                                            #region  AuditClosure 
                                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                            Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                            MstRiskResult.ForPerid, MstRiskResult.FinancialYear, AuditID);

                                            using (AuditControlEntities entities = new AuditControlEntities())
                                            {
                                                var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                      where row.ProcessId == MstRiskResult.ProcessId
                                                                      && row.FinancialYear == MstRiskResult.FinancialYear
                                                                      && row.ForPerid == MstRiskResult.ForPerid
                                                                      && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                      && row.UserID == MstRiskResult.UserID
                                                                      && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                      && row.VerticalID == MstRiskResult.VerticalID
                                                                      && row.AuditID == MstRiskResult.AuditID
                                                                      select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                if (RecordtoUpdate != null)
                                                {
                                                    AuditClosureResult.ResultID = RecordtoUpdate;
                                                    AuditClosureResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    AuditClosureResult.CreatedOn = DateTime.Now;
                                                    RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                //Code added by Sushant
                                if (rdbtnStatus.SelectedItem.Text == "Closed")
                                {
                                    if (RiskCategoryManagement.InternalControlResultClosedCount(MstRiskResult) == 1)
                                    {
                                        #region  AuditClosure 
                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                        Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                        MstRiskResult.ForPerid, MstRiskResult.FinancialYear, AuditID);

                                        using (AuditControlEntities entities = new AuditControlEntities())
                                        {
                                            var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                  where row.ProcessId == MstRiskResult.ProcessId
                                                                  && row.FinancialYear == MstRiskResult.FinancialYear
                                                                  && row.ForPerid == MstRiskResult.ForPerid
                                                                  && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                  && row.UserID == MstRiskResult.UserID
                                                                  && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                  && row.VerticalID == MstRiskResult.VerticalID
                                                                  && row.AuditID == MstRiskResult.AuditID
                                                                  select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                            if (RecordtoUpdate != null)
                                            {
                                                AuditClosureResult.ResultID = RecordtoUpdate;
                                                AuditClosureResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                AuditClosureResult.CreatedOn = DateTime.Now;
                                                RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(objHistory.Observation))
                            {
                                if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                {
                                    objHistory.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    objHistory.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.AddObservationHistory(objHistory);
                                }
                            }

                            if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                            {
                                if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                                {
                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.CreatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                }
                                else
                                {
                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.UpdatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewer(transaction);
                                }
                            }
                            else
                            {
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                                Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                            }
                            string Testremark = "";
                            if (string.IsNullOrEmpty(txtRemark.Text))
                            {
                                Testremark = "NA";
                            }
                            else
                            {
                                Testremark = txtRemark.Text.Trim();
                            }
                            HttpFileCollection fileCollection1 = Request.Files;
                            // string UniqueFlag = DateTime.Now.ToString("yyyyMMddHHmmss");
                            if (fileCollection1.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                //int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getScheduleonDetails.InternalAuditInstance));
                                string directoryPath1 = "";
                                directoryPath1 = Server.MapPath("~/InternalTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getScheduleonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getScheduleonDetails.ID + "/1.0");

                                DocumentManagement.CreateDirectory(directoryPath1);
                                for (int i = 0; i < fileCollection1.Count; i++)
                                {
                                    HttpPostedFile uploadfile1 = fileCollection1[i];
                                    string[] keys1 = fileCollection1.Keys[i].Split('$');
                                    String fileName1 = "";
                                    if (keys1[keys1.Count() - 1].Equals("FileuploadAdditionalFile"))
                                    {
                                        fileName1 = uploadfile1.FileName;
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                                    Stream fs = uploadfile1.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                    if (uploadfile1.ContentLength > 0)
                                    {
                                        InternalReviewHistory RH = new InternalReviewHistory()
                                        {
                                            ATBDId = ATBDID,
                                            ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            Dated = DateTime.Now,
                                            Remarks = Testremark,
                                            AuditScheduleOnID = getScheduleonDetails.ID,
                                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                            Name = fileName1,
                                            FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey1.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.UtcNow,
                                            FixRemark = tbxRemarks.Text.Trim(),
                                            VerticalID = VerticalID,
                                            AuditID = AuditID,
                                            CreatedOn = DateTime.Now,
                                            // UniqueFlag= UniqueFlag,
                                        };
                                        Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    }
                                    else
                                    {
                                        InternalReviewHistory RH = new InternalReviewHistory()
                                        {
                                            ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            Dated = DateTime.Now,
                                            Remarks = Testremark,
                                            AuditScheduleOnID = getScheduleonDetails.ID,
                                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                            ATBDId = ATBDID,
                                            FixRemark = tbxRemarks.Text.Trim(),
                                            VerticalID = VerticalID,
                                            AuditID = AuditID,
                                            CreatedOn = DateTime.Now,
                                            //UniqueFlag = UniqueFlag,
                                        };
                                        Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                    }
                                }
                            }
                            else
                            {
                                InternalReviewHistory RH = new InternalReviewHistory()
                                {
                                    ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                    ATBDId = ATBDID,
                                    FixRemark = tbxRemarks.Text.Trim(),
                                    VerticalID = VerticalID,
                                    AuditID = AuditID,
                                    // UniqueFlag = UniqueFlag,
                                };
                                Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                            }
                            BindRemarks(Convert.ToInt32(getScheduleonDetails.ProcessId), Convert.ToInt32(getScheduleonDetails.ID), ATBDID, AuditID);
                            if (Success1 == true && Success2 == true && Flag == true)
                            {

                                //if (rdbtnStatus.SelectedItem.Text == "Team Review")
                                //{
                                //    ProcessReminderOnReviewerTeamReview(ddlPeriod.SelectedItem.Text, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(getScheduleonDetails.ID), Convert.ToInt32(getScheduleonDetails.ProcessId),AuditID);
                                //}
                                //else if (rdbtnStatus.SelectedItem.Text == "Final Review")
                                //{
                                //    ProcessReminderOnAuditReviewerAuditManager(ddlPeriod.SelectedItem.Text, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(getScheduleonDetails.ID), Convert.ToInt32(getScheduleonDetails.ProcessId));
                                //}
                                //else if (rdbtnStatus.SelectedItem.Text == "Auditee Review")
                                //{
                                //    ProcessReminderOnReviewerAuditeeReview(ddlPeriod.SelectedItem.Text, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(getScheduleonDetails.ProcessId), ATBDID);
                                //}
                                BindTransactions(Convert.ToInt32(getScheduleonDetails.ID), ATBDID, AuditID);

                                //lnkAuditCoverage.Enabled = false;
                                //lnkActualTesting.Enabled = false;
                                //lnkObservation.Enabled = false;
                                //lnkReviewLog.Enabled = false;
                                btnNext1.Enabled = false;
                                btnNext2.Enabled = false;
                                btnNext3.Enabled = false;
                                btnSave.Enabled = false;
                                txtRemark.Text = string.Empty;
                                cvDuplicateEntry.IsValid = false;

                                cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";

                                //cvDuplicateEntry.ErrorMessage = "Remark Saved Successfully";

                                #region Audit Close 
                                if (rdbtnStatus.SelectedItem.Text == "Closed")
                                {
                                    if (RiskCategoryManagement.CheckAllStepClosed(AuditID))
                                    {
                                        AuditClosureClose auditclosureclose = new AuditClosureClose();
                                        auditclosureclose.CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                                        auditclosureclose.CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                                        auditclosureclose.FinancialYear = ddlFinancialYear.SelectedItem.Text;
                                        auditclosureclose.ForPeriod = ddlPeriod.SelectedItem.Text;
                                        auditclosureclose.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        auditclosureclose.CreatedDate = DateTime.Today.Date;
                                        auditclosureclose.ACCStatus = 1;
                                        auditclosureclose.VerticalID = VerticalID;
                                        auditclosureclose.AuditId = AuditID;
                                        if (!RiskCategoryManagement.AuditClosureCloseExists(auditclosureclose.CustomerBranchId, (int)auditclosureclose.VerticalID, auditclosureclose.FinancialYear, auditclosureclose.ForPeriod, AuditID))
                                        {
                                            RiskCategoryManagement.CreateAuditClosureClosed(auditclosureclose);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            }
                            #endregion

                            if (Success1)
                            {
                                if (txtMultilineVideolink.Text.Trim() != "")
                                {
                                    List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                                    if (observationAudioVideoList.Count > 0)
                                    {
                                        string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                        var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                        if (filtered.Count > 0)
                                        {
                                            foreach (var item in filtered)
                                            {
                                                RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                            }
                                        }
                                    }

                                    ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                                    {
                                        AuditId = AuditID,
                                        ATBTID = ATBDID,
                                        IsActive = true,
                                    };

                                    if (txtMultilineVideolink.Text.Contains(',')) //checking entered single value or multiple values
                                    {
                                        string[] audioVideoLinks = txtMultilineVideolink.Text.Split(',');
                                        int lenght = audioVideoLinks.Length;
                                        string link = string.Empty;
                                        for (int i = 0; i < audioVideoLinks.Length; i++)
                                        {
                                            link = audioVideoLinks[i].ToString();
                                            objObservationAudioVideo.AudioVideoLink = link.Trim();
                                            if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                            {
                                                objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                            }
                                            else
                                            {
                                                objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                        if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                        {
                                            objObservationAudioVideo.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            objObservationAudioVideo.CreatedOn = DateTime.Now;
                                            RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                        }
                                        else
                                        {
                                            objObservationAudioVideo.AudioVideoLink = txtMultilineVideolink.Text.Trim();
                                            objObservationAudioVideo.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                            RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                        }
                                    }
                                }
                                else
                                {
                                    // when user want delete all existing link then we will get null from txtMultilineVideolink
                                    List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(AuditID, ATBDID);
                                    if (observationAudioVideoList.Count > 0)
                                    {
                                        foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                        {
                                            RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, AuditID, ATBDID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                        }

                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            }
                            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "You don't have access to change status into " + rdbtnStatus.SelectedItem.Text + ".";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Status";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAnnuxtureUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedValue, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleonDetails != null)
                {
                    if (AnnexureFileUpload.HasFile)
                    {
                        HttpFileCollection fileCollection1 = Request.Files;
                        if (fileCollection1.Count > 0)
                        {
                            List<Tran_AnnxetureUpload> AnnxetureList = new List<Tran_AnnxetureUpload>();
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                            string directoryPath1 = "";
                            if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1 && Convert.ToInt32(getScheduleonDetails.ProcessId) != -1 && !string.IsNullOrEmpty(Convert.ToString(ddlFinancialYear.SelectedItem.Text)) && !string.IsNullOrEmpty(Convert.ToString(ddlPeriod.SelectedValue)))
                            {
                                directoryPath1 = Server.MapPath("~/AuditClosureAnnuxureDocument/" + customerID + "/"
                                    + Convert.ToInt32(ddlFilterLocation.SelectedValue) + "/" + VerticalID + "/" + Convert.ToInt32(getScheduleonDetails.ProcessId) + "/"
                                    + Convert.ToString(ddlFinancialYear.SelectedItem.Text) + "/" + Convert.ToString(ddlPeriod.SelectedValue) + "/"
                                    + AuditID + "/" +
                                    +ATBDID + "/1.0");
                            }
                            DocumentManagement.CreateDirectory(directoryPath1);

                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                #region
                                HttpPostedFile uploadfile1 = fileCollection1[i];
                                string[] keys1 = fileCollection1.Keys[i].Split('$');
                                String fileName1 = "";
                                if (keys1[keys1.Count() - 1].Equals("AnnexureFileUpload"))
                                {
                                    fileName1 = uploadfile1.FileName;

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                                    Stream fs = uploadfile1.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                    if (uploadfile1.ContentLength > 0)
                                    {
                                        Tran_AnnxetureUpload AnnxetureUpload = new Tran_AnnxetureUpload()
                                        {
                                            ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                            ATBDId = ATBDID,
                                            CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                            Period = ddlPeriod.SelectedValue,
                                            VerticalID = VerticalID,
                                            IsDeleted = false,
                                            FileName = fileName1,
                                            FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey1.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.Now,
                                            CreatedDate = DateTime.Today.Date,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            AuditID = AuditID,
                                        };
                                        AnnxetureList.Add(AnnxetureUpload);
                                    }

                                }
                                #endregion
                            }//For Loop End   
                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                            bool Success1 = false;
                            foreach (var item in AnnxetureList)
                            {
                                if (!RiskCategoryManagement.Tran_AnnxetureUploadResultExists(item))
                                {
                                    Success1 = RiskCategoryManagement.CreateTran_AnnxetureUploadResult(item);
                                    if (Success1 == true)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "File Uploaded Successfully";
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Same Name Already Exists..";
                                }
                            }
                            BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), ATBDID, Convert.ToInt32(getScheduleonDetails.ProcessId), Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                        }//File Collection End                        
                    }//Hasfile End
                }//GetAuditDetails End          
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                long roleid = 3;
                var getAuditScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedValue, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getAuditScheduleonDetails != null)
                {
                    #region
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getAuditScheduleonDetails.ID,
                        ProcessId = getAuditScheduleonDetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedValue,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getAuditScheduleonDetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                    };
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        AuditScheduleOnID = getAuditScheduleonDetails.ID,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        InternalAuditInstance = getAuditScheduleonDetails.InternalAuditInstance,
                        ProcessId = getAuditScheduleonDetails.ProcessId,
                        ForPeriod = ddlPeriod.SelectedValue,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        AuditID = AuditID
                    };
                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = txtpopulation.Text.Trim();

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    // added by sagar more on 09-01-2020
                    if (txtAnnexueTitle.Text.Trim() == "")
                        MstRiskResult.AnnexueTitle = "";
                    else
                        MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();


                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());

                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();// changed by sagar more on 24-01-2020

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            transaction.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            transaction.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            transaction.ObservatioRating = null;
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationCategory = null;
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationSubCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            transaction.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }

                    int checkStatusId = -1;
                    if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                    {
                        if (ddlFilterStatus.SelectedValue != "")
                        {
                            if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4)
                            {
                                checkStatusId = 4;
                            }
                            else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5)
                            {
                                checkStatusId = 5;
                            }
                            else
                            {
                                checkStatusId = 2;
                            }
                        }
                    }
                    if (checkStatusId != -1)
                    {
                        MstRiskResult.AStatusId = checkStatusId;
                    }
                    #endregion
                    bool Success1 = false;
                    bool Success2 = false;
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }

                    var RCT = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(getAuditScheduleonDetails.ID), ATBDID);
                    if (RCT != null)
                    {
                        transaction.StatusId = RCT.AuditStatusID;
                        transaction.Remarks = RCT.Remarks;

                        if (RiskCategoryManagement.InternalAuditTxnExistsNew(transaction))
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.UpdatedOn = DateTime.Now;
                            Success2 = UpdateInternalAuditTxnWithFileNew(getAuditScheduleonDetails, transaction, customerID);
                        }
                        else
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            Success2 = CreateInternalAuditTransaction(getAuditScheduleonDetails, transaction, customerID);
                        }
                    }
                    else
                    {
                        if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                        {
                            transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.UpdatedOn = DateTime.Now;
                            Success2 = UpdateInternalAuditTxnWithFile(getAuditScheduleonDetails, transaction, customerID, null);
                        }
                        else
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            Success2 = CreateInternalAuditTransaction(getAuditScheduleonDetails, transaction, customerID);
                        }
                    }
                    if (Success1 && Success2)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "File Uploaded Successfully";
                        BindDocument(Convert.ToInt32(getAuditScheduleonDetails.ID), ATBDID, AuditID);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }//getAuditScheduleonDetails End
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion
        
        #region Document Upload delete download view
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }
            return processnonprocess;
        }
        public static List<InternalReviewHistory> GetFileData(int id, int AuditScheduleOnId, int ATBDID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.InternalReviewHistories
                                where row.ID == id && row.ATBDId == ATBDID
                                //&& row.InternalAuditInstance == InternalAuditInstance
                                && row.AuditScheduleOnID == AuditScheduleOnId
                                && row.AuditID == AuditID
                                select row).ToList();

                return fileData;
            }
        }
        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblriskID = (Label)gvr.FindControl("lblriskID");
            Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
            Label lblAuditId = (Label)gvr.FindControl("lblAuditId");
            List<InternalReviewHistory> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), Convert.ToInt32(lblATBDId.Text), Convert.ToInt32(lblAuditId.Text));

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    if (file.EnType == "M")
                    {
                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    else
                    {
                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewREV('" + CompDocReviewPath + "');", true);
                }
                break;
            }
        }
        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileId);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    DashboardManagementRisk.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void DeleteFileAnnxeture(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Annxeture(fileId);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.FileName));
                    DashboardManagementRisk.DeleteFileAnnxeture(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void DownloadAnnxetureFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Annxeture(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindDocumentAnnxeture(string Period, string FinancialYear, int ATBDID, int ProcessId, int CustomerBranchId, int VerticalID, int AuditID)
        {
            try
            {

                List<Tran_AnnxetureUpload> ComplianceDocument = new List<Tran_AnnxetureUpload>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetTran_AnnxetureUpload(ATBDID, FinancialYear, Period, ProcessId, CustomerBranchId, VerticalID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                if (ComplianceDocument != null)
                {
                    rptComplianceDocumnetsAnnexure.DataSource = ComplianceDocument.ToList();
                    rptComplianceDocumnetsAnnexure.DataBind();
                }
                else
                {
                    rptComplianceDocumnetsAnnexure.DataSource = null;
                    rptComplianceDocumnetsAnnexure.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void btnDownload_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(Convert.ToInt32(hdlSelectedDocumentID.Value));
        //        Response.Buffer = true;
        //        Response.ClearContent();
        //        Response.ContentType = "application/octet-stream";
        //        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
        //        Response.End();
        //        Response.Flush(); // send it to the client to download
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }

        //}
        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
                Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
                Label lblAuditId = (Label)gvr.FindControl("lblAuditId");

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<InternalReviewHistory> fileData = GetFileData(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), Convert.ToInt32(lblATBDId.Text), Convert.ToInt32(lblAuditId.Text));
                    int i = 0;
                    string directoryName = "Review Comments";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GridEvents
        protected void rptComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                    BindObservationDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                }
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void rptComplianceDocumnets_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void rptComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnets");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbutton");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocbutton);
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.RiskCategoryManagement.GetFileInternalFileData_Risk(fileID);

                    if (file.FilePath == null)
                    {
                        using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                        {
                            int length = (int)fs.Length;
                            byte[] buffer;

                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                buffer = br.ReadBytes(length);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void GridRemarks_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                else
                {
                    ProcessID = Convert.ToInt32(ViewState["Processed"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                GridRemarks.PageIndex = e.NewPageIndex;
                BindRemarks(ProcessID, Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnetsAnnexure_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadAnnuxture"))
                {
                    DownloadAnnxetureFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("DeleteAnnuxture"))
                {
                    DeleteFileAnnxeture(Convert.ToInt32(e.CommandArgument));

                    if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                    {
                        ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    }
                    else
                    {
                        ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    }
                    else
                    {
                        AuditID = Convert.ToInt32(ViewState["AuditID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                    {
                        ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    }
                    else
                    {
                        ProcessID = Convert.ToInt32(ViewState["Processed"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                    {
                        VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    }
                    else
                    {
                        VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                    }
                    BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), ATBDID, ProcessID, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnetsAnnexure_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnetsAnnexure");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocdeletebutton = (LinkButton)e.Row.FindControl("lbtLinkDocbuttonAnnuxture");
            if (lbtLinkDocdeletebutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lbtLinkDocdeletebutton);
            }
        }

        protected void rptComplianceDocumnets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                rptComplianceDocumnets.PageIndex = e.NewPageIndex;

                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                BindObservationDocument(Convert.ToInt32(ViewState["ScheduledOnID"]), ATBDID, AuditID);
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnetsAnnexure_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            rptComplianceDocumnetsAnnexure.PageIndex = e.NewPageIndex;

            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
            {
                ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
            }
            else
            {
                ProcessID = Convert.ToInt32(ViewState["Processed"]);
            }
            BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), ATBDID, ProcessID, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
        }
        #endregion

        #region DropdownChange
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCountNew(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                        BindAuditSchedule("P", count);
                    }
                }
            }
        }
        protected void ddlObservationCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
            {
                if (ddlObservationCategory.SelectedValue != "-1")
                {
                    BindObservationSubCategory(Convert.ToInt32(ddlObservationCategory.SelectedValue));
                }
            }
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        #endregion

        #region EmailFunction
        //private void ProcessReminderOnReviewerTeamReview(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid,int AuditID)
        //{
        //    try
        //    {
        //        long userid = GetPerfromer(ForPeriod, FinancialYear, ScheduledOnID, processid);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;                    
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(customerID).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditReviewerTeamReview
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Team review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        //private void ProcessReminderOnReviewerAuditeeReview(string ForPeriod, string FinancialYear, int processid, int ATBDID)
        //{
        //    try
        //    {
        //        long userid = GetPersonResponsible(ATBDID, processid);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;                    
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(customerID).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditReviewerAuditeeReview
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Auditee Review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        //private void ProcessReminderOnAuditReviewerAuditManager(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid)
        //{
        //    try
        //    {
        //        int customerID = -1;                
        //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //        var user = UserManagement.GetISAuditManagerID(Convert.ToInt32(customerID));
        //        if (user != null)
        //        {
        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;
        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            bool valuecheck = CheckAuditorFinalreviewComment(ForPeriod, FinancialYear, ScheduledOnID, processid);
        //            if (valuecheck)
        //            {
        //                string message = Properties.Settings.Default.EmailTemplate_AuditReviewerAuditManagerSecond
        //                    .Replace("@User", username)
        //                    .Replace("@PeriodName", ForPeriod)
        //                    .Replace("@FinancialYear", FinancialYear)
        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                    .Replace("@From", ReplyEmailAddressName);

        //                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Final review", message);
        //            }
        //            else
        //            {

        //                string message = Properties.Settings.Default.EmailTemplate_AuditReviewerAuditManager
        //                     .Replace("@User", username)
        //                     .Replace("@PeriodName", ForPeriod)
        //                     .Replace("@FinancialYear", FinancialYear)
        //                     .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                     .Replace("@From", ReplyEmailAddressName);

        //                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Final review", message);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        #endregion

        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
            {
                ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
            }
            CheckStatus(ProcessID, ATBDID);
        }

        protected void btnAddTable_Click(object sender, EventArgs e)
        {
            ShowTable.Visible = true;
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }

        protected void btnImageUpload_Click(object sender, EventArgs e)
        {
            try
            {
                bool Success2 = false;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }
                #region File upload
                var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedValue, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getScheduleonDetails != null)
                {
                    if (FileUploadImageTable.HasFile)
                    {
                        string AllImagename = string.Empty;
                        string AllImagePath = string.Empty;
                        HttpFileCollection ImageCollection = Request.Files;
                        List<KeyValuePair<string, Byte[]>> FilelistImage = new List<KeyValuePair<string, Byte[]>>();

                        string directoryPathImg = "";
                        if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1 && Convert.ToInt32(getScheduleonDetails.ProcessId) != -1 && !string.IsNullOrEmpty(Convert.ToString(ddlFinancialYear.SelectedItem.Text)) && !string.IsNullOrEmpty(Convert.ToString(ddlPeriod.SelectedValue)))
                        {
                            directoryPathImg = Server.MapPath("~/ObservationImages/" + customerID + "/"
                                + Convert.ToInt32(ddlFilterLocation.SelectedValue) + "/" + VerticalID + "/" + Convert.ToInt32(getScheduleonDetails.ProcessId) + "/"
                                + Convert.ToString(ddlFinancialYear.SelectedItem.Text) + "/" + Convert.ToString(ddlPeriod.SelectedValue) + "/"
                                + AuditID + "/" +
                                +ATBDID + "/1.0");
                        }
                        if (!Directory.Exists(directoryPathImg))
                            Directory.CreateDirectory(directoryPathImg);

                        for (int i = 0; i < ImageCollection.Count; i++)
                        {
                            HttpPostedFile UploadedImage = ImageCollection[i];
                            string[] keys1 = ImageCollection.Keys[i].Split('$');
                            String fileNameImg = "";
                            if (keys1[keys1.Count() - 1].Equals("FileUploadImageTable"))
                            {
                                Guid fileKey1 = Guid.NewGuid();
                                fileNameImg = UploadedImage.FileName;
                                //string finalPath1 = Path.Combine(directoryPathImg, UploadedImage.FileName);
                                //string finalPath1 = Path.Combine(directoryPathImg, fileKey1 + Path.GetExtension(UploadedImage.FileName));
                                //Stream fs = UploadedImage.InputStream;
                                //BinaryReader br = new BinaryReader(fs);
                                //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                //FilelistImage.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));


                                string finalPath1 = Path.Combine(directoryPathImg, UploadedImage.FileName);
                                FileUploadImageTable.PostedFile.SaveAs(finalPath1);

                                // DocumentManagement.SaveDocFiles(FilelistImage);

                                ObservationImage ObjImg = new ObservationImage()
                                {
                                    AuditID = AuditID,
                                    ATBTID = ATBDID,
                                    ImageName = fileNameImg,
                                    ImagePath = directoryPathImg.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey1.ToString(),
                                    IsActive = true,
                                };

                                if (RiskCategoryManagement.CheckObservationImageExist(ObjImg))
                                {
                                    ObjImg.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    ObjImg.UpdatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.UpdateObservationImg(ObjImg);
                                }
                                else
                                {
                                    ObjImg.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    ObjImg.CreatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.CreateObservationImg(ObjImg);
                                    BindObservationImages();
                                }
                                FilelistImage.Clear();
                            }
                        }
                    }//For Loop End   
                }//File Collection End 
                #endregion
                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindObservationImages()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
            {
                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
            }
            else
            {
                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ImageListdata = (from row in entities.ObservationImages
                                     where row.ATBTID == ATBDID &&
                                     row.AuditID == AuditID && row.IsActive == true
                                     select row).ToList();

                if (ImageListdata.Count > 0)
                {
                    grdObservationImg.DataSource = ImageListdata;
                    grdObservationImg.DataBind();
                    UpdatePanel5.Update();
                    //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }
                else
                {
                    grdObservationImg.DataSource = null;
                    grdObservationImg.DataBind();
                    UpdatePanel5.Update();
                }
            }
        }
        protected void grdObservationImg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdObservationImg.PageIndex = e.NewPageIndex;
            BindObservationImages();
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }

        protected void grdObservationImg_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeleteImage"))
                    {
                        int ID = Convert.ToInt32(e.CommandArgument);
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var Imagelist = (from row in entities.ObservationImages
                                             where row.ID == ID
                                             select row).FirstOrDefault();
                            if (Imagelist != null)
                            {
                                Imagelist.IsActive = false;
                                entities.SaveChanges();
                                BindObservationImages();
                                //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewImage"))
                    {
                        ObservationImage AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFile(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.ImagePath), AllinOneDocumentList.ImageName);
                            if (AllinOneDocumentList.ImagePath != null && File.Exists(filePath))
                            {
                                DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                        }
                        //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnHiddenUpdatUser_Click(object sender, EventArgs e)
        {
            BindUsers();
            //editor.Value = HttpUtility.HtmlDecode(editor.Value.Trim());
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }

        protected void btnObjfileupload_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                }
                else
                {
                    ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                }

                long roleid = 3;
                var getAuditScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedValue, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);
                if (getAuditScheduleonDetails != null)
                {
                    #region
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = getAuditScheduleonDetails.ID,
                        ProcessId = getAuditScheduleonDetails.ProcessId,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        ForPerid = ddlPeriod.SelectedValue,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = getAuditScheduleonDetails.InternalAuditInstance,
                        VerticalID = VerticalID,
                        AuditID = AuditID,
                    };
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        AuditScheduleOnID = getAuditScheduleonDetails.ID,
                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        InternalAuditInstance = getAuditScheduleonDetails.InternalAuditInstance,
                        ProcessId = getAuditScheduleonDetails.ProcessId,
                        ForPeriod = ddlPeriod.SelectedValue,
                        ATBDId = ATBDID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        AuditID = AuditID
                    };
                    if (txtAuditObjective.Text.Trim() == "")
                        MstRiskResult.AuditObjective = null;
                    else
                        MstRiskResult.AuditObjective = txtAuditObjective.Text.Trim();

                    if (txtAuditSteps.Text.Trim() == "")
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = txtAuditSteps.Text.Trim();

                    if (txtAnalysisToBePerformed.Text.Trim() == "")
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = txtAnalysisToBePerformed.Text.Trim();

                    if (txtWalkthrough.Text.Trim() == "")
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = txtWalkthrough.Text.Trim();

                    if (txtActualWorkDone.Text.Trim() == "")
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = txtActualWorkDone.Text.Trim();

                    if (txtpopulation.Text.Trim() == "")
                        MstRiskResult.Population = "";
                    else
                    {
                        if (txtpopulation.Text.Length > 200)
                        {
                            MstRiskResult.Population = txtpopulation.Text.Substring(0, 200);
                        }
                        else
                        {
                            MstRiskResult.Population = txtpopulation.Text;
                        }
                    }

                    if (txtSample.Text.Trim() == "")
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = txtSample.Text.Trim();

                    if (txtObservationNumber.Text.Trim() == "")
                        MstRiskResult.ObservationNumber = "";
                    else
                        MstRiskResult.ObservationNumber = txtObservationNumber.Text.Trim();

                    if (txtObservationTitle.Text.Trim() == "")
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = txtObservationTitle.Text.Trim();

                    if (txtAnnexueTitle.Text.Trim() == "")
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        if (grdObservationImg.Rows.Count > 0 || !string.IsNullOrEmpty(tbxTable.Text.Trim()) || !string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            MstRiskResult.AnnexueTitle = txtAnnexueTitle.Text.Trim();
                        }
                        else if (grdObservationImg.Rows.Count == 0 && string.IsNullOrEmpty(tbxTable.Text.Trim()) && string.IsNullOrEmpty(txtMultilineVideolink.Text.Trim()))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Draw Table or Upload Images or Add Audio/Video link.";
                            return;
                        }
                    }

                    if (txtObservation.Text.Trim() == "")
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = txtObservation.Text.Trim();

                    if (txtRisk.Text.Trim() == "")
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = txtRisk.Text.Trim();

                    if (txtRootcost.Text.Trim() == "")
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = txtRootcost.Text.Trim();


                    if (txtAuditStepScore.Text.Trim() == "")
                        MstRiskResult.AuditScores = null;
                    else
                        MstRiskResult.AuditScores = Convert.ToDecimal(txtAuditStepScore.Text.Trim());

                    if (txtfinancialImpact.Text.Trim() == "")
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = txtfinancialImpact.Text.Trim();

                    if (txtRecommendation.Text.Trim() == "")
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = txtRecommendation.Text.Trim();

                    if (txtMgtResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtMgtResponse.Text.Trim();

                    if (tbxRemarks.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = tbxRemarks.Text.Trim();

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(txtResponseDueDate.Text.Trim()))
                    {
                        responseDueDate = DateTime.ParseExact(txtResponseDueDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.ResponseDueDate = responseDueDate.Date;
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }
                    if (!string.IsNullOrEmpty(ddlPersonresponsible.SelectedValue))
                    {
                        if (ddlPersonresponsible.SelectedValue == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                            transaction.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                            transaction.PersonResponsible = Convert.ToInt32(ddlPersonresponsible.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlOwnerName.SelectedValue))
                    {
                        if (ddlOwnerName.SelectedValue == "-1")
                        {
                            MstRiskResult.Owner = null;
                            transaction.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                            transaction.Owner = Convert.ToInt32(ddlOwnerName.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlobservationRating.SelectedValue))
                    {
                        if (ddlobservationRating.SelectedValue == "-1")
                        {
                            transaction.ObservatioRating = null;
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            transaction.ObservatioRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                            MstRiskResult.ObservationRating = Convert.ToInt32(ddlobservationRating.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationCategory.SelectedValue))
                    {
                        if (ddlObservationCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationCategory = null;
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            transaction.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                            MstRiskResult.ObservationCategory = Convert.ToInt32(ddlObservationCategory.SelectedValue);
                        }
                    }
                    if (!string.IsNullOrEmpty(ddlObservationSubCategory.SelectedValue))
                    {
                        if (ddlObservationSubCategory.SelectedValue == "-1")
                        {
                            transaction.ObservationSubCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            transaction.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(ddlObservationSubCategory.SelectedValue);
                        }
                    }

                    int checkStatusId = -1;
                    if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                    {
                        if (ddlFilterStatus.SelectedValue != "")
                        {
                            if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4)
                            {
                                checkStatusId = 4;
                            }
                            else if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5)
                            {
                                checkStatusId = 5;
                            }
                            else
                            {
                                checkStatusId = 2;
                            }
                        }
                    }
                    if (checkStatusId != -1)
                    {
                        MstRiskResult.AStatusId = checkStatusId;
                    }
                    #endregion
                    bool Success1 = false;
                    bool Success2 = false;
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                    }

                    var RCT = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(getAuditScheduleonDetails.ID), ATBDID);
                    if (RCT != null)
                    {
                        transaction.StatusId = RCT.AuditStatusID;
                        transaction.Remarks = RCT.Remarks;

                        if (RiskCategoryManagement.InternalAuditTxnExistsNew(transaction))
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.UpdatedOn = DateTime.Now;
                            Success2 = UpdateObservationFile(getAuditScheduleonDetails, transaction, customerID, "OB");
                        }
                        else
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            Success2 = CreateInternalAuditTransactionObservation(getAuditScheduleonDetails, transaction, customerID, "OB");
                        }
                    }
                    else
                    {
                        if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                        {
                            transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.UpdatedOn = DateTime.Now;
                            Success2 = UpdateObservationFile(getAuditScheduleonDetails, transaction, customerID, "OB");
                        }
                        else
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            Success2 = CreateInternalAuditTransactionObservation(getAuditScheduleonDetails, transaction, customerID, "OB");
                        }
                    }
                    if (Success1 && Success2)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "File Uploaded Successfully";
                        BindObservationDocument(Convert.ToInt32(getAuditScheduleonDetails.ID), ATBDID, AuditID);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }//getAuditScheduleonDetails End
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public bool UpdateObservationFile(InternalAuditScheduleOn getAuditScheduleonDetails, InternalAuditTransaction IAT, long customerID, string filetype)
        {
            try
            {
                bool flag = false;
                if (getAuditScheduleonDetails != null)
                {
                    if (FileObjUpload.HasFile)
                    {
                        HttpFileCollection fileCollection1 = Request.Files;
                        if (fileCollection1.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                            {
                                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                            }
                            else
                            {
                                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                            {
                                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            }
                            else
                            {
                                AuditID = Convert.ToInt32(ViewState["AuditID"]);
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                            {
                                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                            }
                            else
                            {
                                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                            }
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<InternalFileData_Risk> files = new List<InternalFileData_Risk>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getAuditScheduleonDetails.ProcessId), Convert.ToInt32(getAuditScheduleonDetails.InternalAuditInstance));
                            string directoryPath = "";
                            directoryPath = Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getAuditScheduleonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getAuditScheduleonDetails.ID + "/1.0");
                            if (!File.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                HttpPostedFile uploadfile = fileCollection1[i];
                                string[] keys = fileCollection1.Keys[i].Split('$');
                                string fileName = "";
                                if (keys[keys.Count() - 1].Equals("FileObjUpload"))
                                {
                                    fileName = uploadfile.FileName;
                                }
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (uploadfile.ContentLength > 0)
                                {
                                    InternalFileData_Risk file = new InternalFileData_Risk()
                                    {
                                        Name = fileName,
                                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        ProcessID = Convert.ToInt32(getAuditScheduleonDetails.ProcessId),
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        IsDeleted = false,
                                        ATBDId = ATBDID,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                    };
                                    if (!string.IsNullOrEmpty(filetype))
                                    {
                                        file.TypeOfFile = filetype;
                                    }
                                    files.Add(file);
                                }
                            }//For Loop End                            
                            if (Filelist != null && list != null)
                                flag = UserManagementRisk.UpdateTransactionNew(IAT, files, list, Filelist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                            }
                        }//FileCollectionEnd
                    } //HasUpload File End
                    {
                        return true;
                    }
                }// getAuditScheduleonDetails end
                return flag;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected bool CreateInternalAuditTransactionObservation(InternalAuditScheduleOn getAuditScheduleonDetails, InternalAuditTransaction transaction, long customerID, string FileType)
        {
            bool flag = false;
            if (getAuditScheduleonDetails != null)
            {
                if (FileObjUpload.HasFile)
                {
                    HttpFileCollection fileCollection1 = Request.Files;
                    if (fileCollection1.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                        {
                            VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                        }
                        else
                        {
                            VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        }
                        else
                        {
                            AuditID = Convert.ToInt32(ViewState["AuditID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                        {
                            ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                        }
                        else
                        {
                            ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                        }

                        List<InternalFileData_Risk> files = new List<InternalFileData_Risk>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getAuditScheduleonDetails.ProcessId), Convert.ToInt32(getAuditScheduleonDetails.InternalAuditInstance));
                        string directoryPath = "";
                        directoryPath = Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getAuditScheduleonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getAuditScheduleonDetails.ID + "/1.0");
                        DocumentManagement.CreateDirectory(directoryPath);

                        for (int i = 0; i < fileCollection1.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection1[i];
                            string[] keys = fileCollection1.Keys[i].Split('$');
                            string fileName = "";
                            if (keys[keys.Count() - 1].Equals("FileObjUpload"))
                            {
                                fileName = uploadfile.FileName;

                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (uploadfile.ContentLength > 0)
                                {
                                    InternalFileData_Risk file = new InternalFileData_Risk()
                                    {
                                        Name = fileName,
                                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        ProcessID = Convert.ToInt32(getAuditScheduleonDetails.ProcessId),
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        IsDeleted = false,
                                        ATBDId = ATBDID,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };
                                    if (!string.IsNullOrEmpty(FileType))
                                    {
                                        file.TypeOfFile = FileType;
                                    }
                                    files.Add(file);
                                }
                            }
                        }//For Loop End    
                        if (Filelist != null && list != null)
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            flag = UserManagementRisk.CreateTransaction(transaction, files, list, Filelist);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }
                    }//FileCollection Count End                    
                }//HasUpload File End
                else
                {
                    return true;
                }
            }//getAuditScheduleonDetails Is null End
            return flag;
        }

        protected void ObsFromHistory_Click(object sender, EventArgs e)
        {
            if (Session["AuditATBDID"] != null)
            {
                string Ids = Convert.ToString(Session["AuditATBDID"]);
                string[] IDS = Ids.Split(',');
                int AuditId = Convert.ToInt32(IDS[0]);
                int ATBDId = Convert.ToInt32(IDS[1]);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var List = (from row in entities.AuditClosures
                                where row.AuditID == AuditId
                                && row.ATBDId == ATBDId
                                select row).FirstOrDefault();

                    #region Commentcode
                    //if (string.IsNullOrEmpty(txtObservationTitle.Text))
                    //{
                    //    txtObservationTitle.Text = List.ObservationTitle;
                    //}
                    //txtObservation.Text = List.Observation;
                    //if (string.IsNullOrEmpty(tbxBriefObservation.Text))
                    //{
                    //    tbxBriefObservation.Text = List.BriefObservation;
                    //}
                    //if (string.IsNullOrEmpty(tbxObjBackground.Text))
                    //{
                    //    tbxObjBackground.Text = List.ObjBackground;
                    //}
                    //if (string.IsNullOrEmpty(txtRisk.Text))
                    //{
                    //    txtRisk.Text = List.Risk;
                    //}
                    //if (string.IsNullOrEmpty(txtRootcost.Text))
                    //{
                    //    txtRootcost.Text = List.RootCost;
                    //}
                    //if (string.IsNullOrEmpty(txtfinancialImpact.Text))
                    //{
                    //    txtfinancialImpact.Text = List.FinancialImpact;
                    //}
                    //if (string.IsNullOrEmpty(txtRecommendation.Text))
                    //{
                    //    txtRecommendation.Text = List.Recomendation;
                    //}
                    //if (string.IsNullOrEmpty(txtMgtResponse.Text))
                    //{
                    //    txtMgtResponse.Text = List.ManagementResponse;
                    //}
                    //if (ddlPersonresponsible.SelectedValue == "-1")
                    //{
                    //    ddlPersonresponsible.SelectedValue = Convert.ToString(List.PersonResponsible);
                    //}
                    //if (ddlobservationRating.SelectedValue == "-1")
                    //{
                    //    ddlobservationRating.SelectedValue = Convert.ToString(List.ObservationRating);
                    //}
                    //if (ddlObservationCategory.SelectedValue == "-1")
                    //{
                    //    ddlObservationCategory.SelectedValue = Convert.ToString(List.ObservationCategory);
                    //}
                    ////if (ddlObservationSubCategory.SelectedValue == "-1")
                    ////{
                    ////    if (!string.IsNullOrEmpty(List.ObservationSubCategory.ToString()))
                    ////    {
                    ////        ddlObservationSubCategory.SelectedValue = Convert.ToString(List.ObservationSubCategory);
                    ////    }
                    ////}
                    //if (ddlOwnerName.SelectedValue == "-1")
                    //{
                    //    if (!string.IsNullOrEmpty(ddlOwnerName.ToString()))
                    //    {
                    //        ddlOwnerName.SelectedValue = Convert.ToString(List.Owner);
                    //    }
                    //}
                    //txtTimeLine.Text = List.TimeLine.Value.ToString("dd-MM-yyyy");
                    #endregion
                    txtObservationTitle.Text = List.ObservationTitle;
                    txtObservation.Text = List.Observation;
                    tbxBriefObservation.Text = List.BriefObservation;
                    tbxObjBackground.Text = List.ObjBackground;
                    txtRisk.Text = List.Risk;
                    txtRootcost.Text = List.RootCost;
                    txtfinancialImpact.Text = List.FinancialImpact;
                    txtRecommendation.Text = List.Recomendation;
                    txtMgtResponse.Text = List.ManagementResponse;
                    ddlPersonresponsible.SelectedValue = Convert.ToString(List.PersonResponsible);
                    ddlobservationRating.SelectedValue = Convert.ToString(List.ObservationRating);
                    ddlObservationCategory.SelectedValue = Convert.ToString(List.ObservationCategory);
                    BindObservationSubCategory(Convert.ToInt32(List.ObservationCategory));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
                }

            }
        }

        protected void RemoveData_Click(object sender, EventArgs e)
        {
            txtObservationTitle.Text = string.Empty;
            txtObservation.Text = string.Empty;
            tbxBriefObservation.Text = string.Empty;
            tbxObjBackground.Text = string.Empty;
            txtRisk.Text = string.Empty;
            txtRootcost.Text = string.Empty;
            txtfinancialImpact.Text = string.Empty;
            txtRecommendation.Text = string.Empty;
            txtMgtResponse.Text = string.Empty;
            ddlPersonresponsible.SelectedValue = "-1";
            ddlobservationRating.SelectedValue = "-1";
            ddlObservationCategory.SelectedValue = "-1";
            ddlOwnerName.SelectedValue = "-1";
            ddlObservationSubCategory.SelectedValue = "-1";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "bind_tinyMCE();", true);
        }
        
        protected bool CreateInternalAuditTransaction(InternalAuditScheduleOn getAuditScheduleonDetails, InternalAuditTransaction transaction, long customerID)
        {
            bool flag = false;
            if (getAuditScheduleonDetails != null)
            {
                if (fuSampleFile.HasFile)
                {
                    HttpFileCollection fileCollection1 = Request.Files;
                    if (fileCollection1.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                        {
                            VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                        }
                        else
                        {
                            VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        }
                        else
                        {
                            AuditID = Convert.ToInt32(ViewState["AuditID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                        {
                            ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                        }
                        else
                        {
                            ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                        }

                        List<InternalFileData_Risk> files = new List<InternalFileData_Risk>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getAuditScheduleonDetails.ProcessId), Convert.ToInt32(getAuditScheduleonDetails.InternalAuditInstance));
                        string directoryPath = "";
                        directoryPath = Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getAuditScheduleonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getAuditScheduleonDetails.ID + "/1.0");
                        DocumentManagement.CreateDirectory(directoryPath);

                        for (int i = 0; i < fileCollection1.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection1[i];
                            string[] keys = fileCollection1.Keys[i].Split('$');
                            string fileName = "";
                            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                            {
                                fileName = uploadfile.FileName;

                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (uploadfile.ContentLength > 0)
                                {
                                    InternalFileData_Risk file = new InternalFileData_Risk()
                                    {
                                        Name = fileName,
                                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        ProcessID = Convert.ToInt32(getAuditScheduleonDetails.ProcessId),
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        IsDeleted = false,
                                        ATBDId = ATBDID,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };
                                    files.Add(file);
                                }
                            }
                        }//For Loop End    
                        if (Filelist != null && list != null)
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            flag = UserManagementRisk.CreateTransaction(transaction, files, list, Filelist);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }
                    }//FileCollection Count End                    
                }//HasUpload File End
                else
                {
                    return true;
                }
            }//getAuditScheduleonDetails Is null End
            return flag;
        }
        public bool UpdateInternalAuditTxnWithFileNew(InternalAuditScheduleOn getAuditScheduleonDetails, InternalAuditTransaction IAT, long customerID)
        {
            try
            {
                bool flag = false;
                if (getAuditScheduleonDetails != null)
                {
                    if (fuSampleFile.HasFile)
                    {
                        HttpFileCollection fileCollection1 = Request.Files;
                        if (fileCollection1.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                            {
                                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                            }
                            else
                            {
                                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                            {
                                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            }
                            else
                            {
                                AuditID = Convert.ToInt32(ViewState["AuditID"]);
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                            {
                                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                            }
                            else
                            {
                                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                            }
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<InternalFileData_Risk> files = new List<InternalFileData_Risk>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getAuditScheduleonDetails.ProcessId), Convert.ToInt32(getAuditScheduleonDetails.InternalAuditInstance));
                            string directoryPath = "";
                            directoryPath = Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getAuditScheduleonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getAuditScheduleonDetails.ID + "/1.0");
                            if (!File.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                HttpPostedFile uploadfile = fileCollection1[i];
                                string[] keys = fileCollection1.Keys[i].Split('$');
                                string fileName = "";
                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    fileName = uploadfile.FileName;
                                }
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (uploadfile.ContentLength > 0)
                                {
                                    InternalFileData_Risk file = new InternalFileData_Risk()
                                    {
                                        Name = fileName,
                                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        ProcessID = Convert.ToInt32(getAuditScheduleonDetails.ProcessId),
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        IsDeleted = false,
                                        ATBDId = ATBDID,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                    };
                                    files.Add(file);
                                }
                            }//For Loop End                            
                            if (Filelist != null && list != null)
                                flag = UserManagementRisk.UpdateTransactionNew(IAT, files, list, Filelist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                            }
                        }//FileCollectionEnd
                    } //HasUpload File End
                    {
                        return true;
                    }
                }// getAuditScheduleonDetails end
                return flag;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateInternalAuditTxnWithFile(InternalAuditScheduleOn getAuditScheduleonDetails, InternalAuditTransaction IAT, long customerID, string filetype)
        {
            try
            {
                bool flag = false;
                if (getAuditScheduleonDetails != null)
                {
                    if (fuSampleFile.HasFile)
                    {
                        HttpFileCollection fileCollection1 = Request.Files;
                        if (fileCollection1.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                            {
                                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                            }
                            else
                            {
                                VerticalID = Convert.ToInt32(ViewState["VerticalID"]);
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                            {
                                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            }
                            else
                            {
                                AuditID = Convert.ToInt32(ViewState["AuditID"]);
                            }
                            if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                            {
                                ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                            }
                            else
                            {
                                ATBDID = Convert.ToInt32(ViewState["ATBDID"]);
                            }
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<InternalFileData_Risk> files = new List<InternalFileData_Risk>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            var InstanceData = RiskCategoryManagement.GetInternalAuditInstanceData(Convert.ToInt32(getAuditScheduleonDetails.ProcessId), Convert.ToInt32(getAuditScheduleonDetails.InternalAuditInstance));
                            string directoryPath = "";
                            directoryPath = Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + getAuditScheduleonDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + getAuditScheduleonDetails.ID + "/1.0");
                            if (!File.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            for (int i = 0; i < fileCollection1.Count; i++)
                            {
                                HttpPostedFile uploadfile = fileCollection1[i];
                                string[] keys = fileCollection1.Keys[i].Split('$');
                                string fileName = "";
                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    fileName = uploadfile.FileName;
                                }
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (uploadfile.ContentLength > 0)
                                {
                                    InternalFileData_Risk file = new InternalFileData_Risk()
                                    {
                                        Name = fileName,
                                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                        FileKey = fileKey.ToString(),
                                        Version = "1.0",
                                        VersionDate = DateTime.UtcNow,
                                        ProcessID = Convert.ToInt32(getAuditScheduleonDetails.ProcessId),
                                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                        FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                        IsDeleted = false,
                                        ATBDId = ATBDID,
                                        VerticalID = VerticalID,
                                        AuditID = AuditID,
                                        UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                        UpdatedOn = DateTime.Now,
                                    };
                                    if (!string.IsNullOrEmpty(filetype))
                                    {
                                        file.TypeOfFile = filetype;
                                    }
                                    files.Add(file);
                                }
                            }//For Loop End                            
                            if (Filelist != null && list != null)
                                flag = UserManagementRisk.UpdateTransaction(IAT, files, list, Filelist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                            }
                        }//FileCollectionEnd
                    } //HasUpload File End
                    {
                        return true;
                    }
                }// getAuditScheduleonDetails end
                return flag;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void btnObs_Click(object sender, EventArgs e)
        {
            try
            {
                string obs = "false";
                if (txtObservation.Text != "")
                {
                    //Session["Observation"] = Convert.ToString(ViewState["AuditID"]) + "," + Convert.ToString(ViewState["ATBDID"]);
                    Session["Observation"] = txtObservation.Text.Replace("\n", "").Replace("\r", "").Replace("'", "`");
                    obs = "true";
                }
                else
                {
                    Session["Observation"] = "";
                }
                long ProcessId = Convert.ToInt64(ViewState["Processed"]);
                long ATBDID = Convert.ToInt64(ViewState["ATBDID"]);
                long SubProcessid = Convert.ToInt64(ViewState["SPID"]);
                long? AuditStepid = 0;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    AuditStepid = (from row in entities.RiskActivityToBeDoneMappings
                                   where row.ID == ATBDID
                                   select row.AuditStepMasterID).FirstOrDefault();
                }
                long BranchId = Convert.ToInt64(ViewState["BID"]);
                long StatusId = Convert.ToInt64(ViewState["StatusID"]);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDetObsDialog(" + ProcessId + "," + SubProcessid + "," + AuditStepid + "," + BranchId + "," + StatusId + "," + obs + ");", true);
            }
            catch (Exception ex)
            {

            }
        }
    }
}