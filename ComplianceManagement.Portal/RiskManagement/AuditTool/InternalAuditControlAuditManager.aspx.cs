﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class InternalAuditControlAuditManager : System.Web.UI.Page
    {
        #region variable Declear
        protected long AuditStepID;
        protected string FinYear;
        protected string Period;
        protected int BranchID;
        protected int ATBDID;
        protected int StatusID;
        protected int ProcessID;
        protected int SubProcessId;
        protected int VerticalID;
        protected long UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
        protected int AuditID;
        public static string DocumentPath = "";
        protected string AuditStep;
        protected int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
        protected int ScheduleOnID;
        protected int InstanceID;
        protected int ResultID = 0;
        protected string Tag = string.Empty;
        protected string ObjReport1;
        protected string Observation;
        protected string AuditObjective;
        protected string AuditSteps;
        protected string AnalysisToBePerofrmed;
        protected string ProcessWalkthrough;
        protected string ActivityToBeDone;
        protected string Population;
        protected string ObservationTitle;
        protected string Risk;
        protected string RootCost;
        protected string FinancialImpact;
        protected string ManagementResponse;
        protected string FixRemark;
        protected string TimeLine;
        protected string BriefObservation;
        protected string ObjBackground;
        protected string AnnexueTitle;
        protected string DefeciencyType;
        protected string ResponseDueDate;
        protected string BodyContent;
        protected string Dated;
        protected string PersonResponsible;
        protected string Owner;
        protected string ObservationRating;
        protected string ObservationCategory;
        protected string ObservationSubCategory;
        protected string GroupObservationCategoryID;
        protected string Sample;
        protected string Recomendation;
        protected string AudioVideoLink;
        protected string CurrentStatusID = "0";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchID = Convert.ToInt32(Request.QueryString["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    ViewState["Processed"] = ProcessID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SubProcessId = Convert.ToInt32(Request.QueryString["SPID"]);
                    ViewState["SPID"] = SubProcessId;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ATBDID"]))
                {
                    ATBDID = Convert.ToInt32(Request.QueryString["ATBDID"]);
                    ViewState["ATBDID"] = ATBDID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VerticalID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    StatusID = Convert.ToInt32(Request.QueryString["SID"]);
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    AuditStepID = (from row in entities.RiskActivityToBeDoneMappings
                                   where row.ID == ATBDID
                                   select (int)row.AuditStepMasterID).FirstOrDefault();
                }

                var getScheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(Portal.Common.AuthenticationHelper.UserID, 3, FinYear, Period, BranchID, ATBDID, VerticalID, AuditID);
                if (getScheduleondetails != null)
                {
                    ScheduleOnID = Convert.ToInt32(getScheduleondetails.ID);
                    InstanceID = Convert.ToInt32(getScheduleondetails.InternalAuditInstance);
                    if (ProcessID == 0)
                    {
                        ProcessID = Convert.ToInt32(getScheduleondetails.ProcessId);
                        //ProcessList.Add(ProcessID);
                    }
                }
                //Page Title
                string ControlNo = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    ControlNo = "/ " + Convert.ToString(Request.QueryString["CID"]);
                }
                string Pname = string.Empty;
                string SPname = string.Empty;
                string VName = string.Empty;
                string FY = string.Empty;
                string Period1 = string.Empty;
                Mst_Process objProcess = new Mst_Process();
                mst_Subprocess objsubProcess = new mst_Subprocess();
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FinYear"])))
                {
                    FY = "/" + Convert.ToString(Request.QueryString["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ForMonth"])))
                {
                    Period1 = "/" + Convert.ToString(Request.QueryString["ForMonth"]);
                }
                objProcess = ProcessManagement.GetByID(ProcessID, CustomerId);
                objsubProcess = ProcessManagement.GetSubProcessByID(SubProcessId);
                if (objProcess != null)
                {
                    Pname = "/" + objProcess.Name;
                }
                if (objsubProcess != null)
                {
                    SPname = "/" + objsubProcess.Name;
                }
                var BrachName = CustomerBranchManagement.GetByID(BranchID).Name;
                var VerticalName = UserManagementRisk.GetVerticalName(Convert.ToInt32(CustomerId), Convert.ToInt32((Request.QueryString["VID"])));
                if (string.IsNullOrEmpty(VerticalName))
                {
                    VName = "/" + VerticalName;
                }
                BindTransactionDetails(ScheduleOnID, ProcessID, FinYear, BranchID, InstanceID, VerticalID, Period, ATBDID, AuditID);
                //for Implementation

                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["tag"]))
                {
                    Tag = Convert.ToString(Request.QueryString["tag"]);
                }
                BindStatusList();
            }
        }

        private void BindTransactionDetails(int ScheduledOnID, int Processed, string FinancialYear, int CustomerBranchID, int InternalAuditInstance, int Verticalid, string ForPeriod, int Atbdid, int AuditID)
        {
            try
            {
                InternalControlAuditResult MstRiskResult = new InternalControlAuditResult();
                Sp_GetInternalAuditResultByID_Result MstRiskResult1 = new Sp_GetInternalAuditResultByID_Result();
                int ResultID = 0;
                string Tag = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["tag"]))
                {
                    Tag = Convert.ToString(Request.QueryString["tag"]);
                }
                if (Tag == "Implement" && ResultID != 0)
                {
                    MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(ResultID);
                    if (MstRiskResult != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ISACPORMIS)))
                        {
                            ObjReport1 = Convert.ToString(MstRiskResult.ISACPORMIS).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Observation)))
                        {
                            Observation = GetSafeTagName(MstRiskResult.Observation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AuditObjective)))
                        {
                            AuditObjective = GetSafeTagName(MstRiskResult.AuditObjective.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AuditSteps)))
                        {
                            AuditSteps = GetSafeTagName(MstRiskResult.AuditSteps.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AnalysisToBePerofrmed)))
                        {
                            AnalysisToBePerofrmed = GetSafeTagName(MstRiskResult.AnalysisToBePerofrmed.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ProcessWalkthrough)))
                        {
                            ProcessWalkthrough = GetSafeTagName(MstRiskResult.ProcessWalkthrough.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ActivityToBeDone)))
                        {
                            ActivityToBeDone = GetSafeTagName(MstRiskResult.ActivityToBeDone.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Population)))
                        {
                            Population = GetSafeTagName(MstRiskResult.Population.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Sample)))
                        {
                            Sample = GetSafeTagName(MstRiskResult.Sample.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationTitle)))
                        {
                            ObservationTitle = GetSafeTagName(MstRiskResult.ObservationTitle.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Risk)))
                        {
                            Risk = GetSafeTagName(MstRiskResult.Risk.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.RootCost)))
                        {
                            RootCost = GetSafeTagName(MstRiskResult.RootCost.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.FinancialImpact)))
                        {
                            FinancialImpact = GetSafeTagName(Convert.ToString(MstRiskResult.FinancialImpact).Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Recomendation)))
                        {
                            Recomendation = GetSafeTagName(MstRiskResult.Recomendation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ManagementResponse)))
                        {
                            ManagementResponse = GetSafeTagName(MstRiskResult.ManagementResponse.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.FixRemark)))
                        {
                            FixRemark = GetSafeTagName(MstRiskResult.FixRemark.Replace("\"", "'"));
                        }
                        TimeLine = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.BriefObservation)))
                        {
                            BriefObservation = GetSafeTagName(MstRiskResult.BriefObservation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObjBackground)))
                        {
                            ObjBackground = GetSafeTagName(MstRiskResult.ObjBackground.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AnnexueTitle)))
                        {
                            AnnexueTitle = GetSafeTagName(MstRiskResult.AnnexueTitle.Replace("\"", "'"));
                        }
                        ResponseDueDate = MstRiskResult.ResponseDueDate != null ? MstRiskResult.ResponseDueDate.Value.ToString("dd-MM-yyyy") : null;
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.DefeciencyType)))
                        {
                            DefeciencyType = Convert.ToString(MstRiskResult.DefeciencyType).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Observation)))
                        {
                            hidObservation.Value = GetSafeTagName(MstRiskResult.Observation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AuditObjective)))
                        {
                            hidAuditObjective.Value = GetSafeTagName(MstRiskResult.AuditObjective.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AuditSteps)))
                        {
                            hidAuditSteps.Value = GetSafeTagName(MstRiskResult.AuditSteps.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AnalysisToBePerofrmed)))
                        {
                            hidAnalysisToBePerformed.Value = GetSafeTagName(MstRiskResult.AnalysisToBePerofrmed.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ProcessWalkthrough)))
                        {
                            hidWalkthrough.Value = GetSafeTagName(MstRiskResult.ProcessWalkthrough.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ActivityToBeDone)))
                        {
                            hidActualWorkDone.Value = GetSafeTagName(MstRiskResult.ActivityToBeDone.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Population)))
                        {
                            hidpopulation.Value = GetSafeTagName(MstRiskResult.Population.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Sample)))
                        {
                            hidSample.Value = GetSafeTagName(MstRiskResult.Sample.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationNumber)))
                        {
                            hidObservationNumber.Value = GetSafeTagName(MstRiskResult.ObservationNumber);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationTitle)))
                        {
                            hidObservationTitle.Value = GetSafeTagName(MstRiskResult.ObservationTitle.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Risk)))
                        {
                            hidRisk.Value = GetSafeTagName(MstRiskResult.Risk.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.RootCost)))
                        {
                            hidRootcost.Value = GetSafeTagName(MstRiskResult.RootCost.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.FinancialImpact)))
                        {
                            hidfinancialImpact.Value = Convert.ToString(GetSafeTagName(MstRiskResult.FinancialImpact));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Recomendation)))
                        {
                            hidRecommendation.Value = GetSafeTagName(MstRiskResult.Recomendation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ManagementResponse)))
                        {
                            hidMgtResponse.Value = GetSafeTagName(MstRiskResult.ManagementResponse.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.FixRemark)))
                        {
                            hidRemarks.Value = GetSafeTagName(MstRiskResult.FixRemark.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.AnnexueTitle)))
                        {
                            hidAnnaxureTitle.Value = GetSafeTagName(MstRiskResult.AnnexueTitle.Replace("\"", "'"));
                        }
                        hidAuditStepScore.Value = Convert.ToString(MstRiskResult.AuditScores);
                        hidISACPORMIS.Value = Convert.ToString(MstRiskResult.ISACPORMIS);
                        hidTimeLine.Value = Convert.ToString(MstRiskResult.TimeLine);
                        hidPersonResponsible.Value = Convert.ToString(MstRiskResult.PersonResponsible);
                        hidObservationRating.Value = Convert.ToString(MstRiskResult.ObservationRating);
                        hidObservationCategory.Value = Convert.ToString(MstRiskResult.ObservationCategory);
                        hidObservationSubCategory.Value = Convert.ToString(MstRiskResult.ObservationSubCategory);
                        //hidGroupObservationCategory.Value = Convert.ToString(MstRiskResult.GroupObservationCategoryID);
                        hidOwner.Value = Convert.ToString(MstRiskResult.Owner);
                        hidUserID.Value = Convert.ToString(MstRiskResult.UserID);
                        hidResponseDueDate.Value = Convert.ToString(MstRiskResult.ResponseDueDate);
                        hidUHPersonResponsible.Value = Convert.ToString(MstRiskResult.UHPersonResponsible);
                        hidPRESIDENTPersonResponsible.Value = Convert.ToString(MstRiskResult.PRESIDENTPersonResponsible);
                        hidUHComment.Value = Convert.ToString(MstRiskResult.UHComment);
                        hidPRESIDENTComment.Value = Convert.ToString(MstRiskResult.PRESIDENTComment);
                        hidBodyContent.Value = Convert.ToString(MstRiskResult.BodyContent);
                        BodyContent = MstRiskResult.BodyContent;
                        var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyID(ScheduledOnID, Atbdid, FinYear, Period, 3, Verticalid, AuditID);
                        if (getDated != null)
                        {
                            Dated = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                        {
                            PersonResponsible = Convert.ToString(MstRiskResult.PersonResponsible);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.Owner)))
                        {
                            Owner = Convert.ToString(MstRiskResult.Owner);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationRating)))
                        {
                            ObservationRating = Convert.ToString(MstRiskResult.ObservationRating).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationCategory)))
                        {
                            ObservationCategory = Convert.ToString(MstRiskResult.ObservationCategory);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ObservationSubCategory)))
                        {
                            ObservationSubCategory = Convert.ToString(MstRiskResult.ObservationSubCategory);
                        }
                        //if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.GroupObservationCategoryID)))
                        //{
                        //    GroupObservationCategoryID = Convert.ToString(MstRiskResult.GroupObservationCategoryID);
                        //}
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            string audioVideoLink = string.Empty;
                            List<ObservationAudioVideo> observationAudioVideoList = new List<ObservationAudioVideo>();
                            observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                         where cs.ATBTID == Atbdid && cs.AuditId == AuditID
                                                         && cs.IsActive == true
                                                         select cs).ToList();
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo item in observationAudioVideoList)
                                {
                                    audioVideoLink = audioVideoLink + "," + item.AudioVideoLink;
                                }
                                audioVideoLink = audioVideoLink.TrimStart(',');
                            }
                            AudioVideoLink = GetSafeTagName(audioVideoLink.Replace("\"", "'")); ;
                        }
                    }
                    else if (MstRiskResult == null)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var data = entities.Sp_GetPreviousAuditProcessWalkthrough(ATBDID, CustomerBranchID, Verticalid, AuditID).FirstOrDefault();
                            if (data != null)
                            {
                                ProcessWalkthrough = GetSafeTagName(data.ProcessWalkthrough.Replace("\"", "'"));
                            }
                            var getpersonresponsiblebydefault = (
                                                from row in entities.RiskActivityTransactions
                                                join row1 in entities.RiskActivityToBeDoneMappings
                                                on row.Id equals row1.RiskActivityId
                                                where row.ProcessId == row1.ProcessId
                                                && row.CustomerBranchId == CustomerBranchID
                                                && row1.ID == ATBDID && row.VerticalsId == Verticalid
                                                select row
                                         ).FirstOrDefault();

                            if (getpersonresponsiblebydefault != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(getpersonresponsiblebydefault.PersonResponsible)))
                                {
                                    PersonResponsible = Convert.ToString(getpersonresponsiblebydefault.PersonResponsible);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(getpersonresponsiblebydefault.ControlOwner)))
                                {
                                    Owner = Convert.ToString(getpersonresponsiblebydefault.ControlOwner);
                                }
                            }

                            var checklistdetails = (from row in entities.RiskActivityToBeDoneMappings
                                                    where row.ID == ATBDID
                                                    select row).FirstOrDefault();
                            if (checklistdetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.ActivityTobeDone)))
                                {
                                    AuditSteps = GetSafeTagName(checklistdetails.ActivityTobeDone.Trim().Replace("\"", "'"));
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.AuditObjective)))
                                {
                                    AuditObjective = GetSafeTagName(checklistdetails.AuditObjective.Trim().Replace("\"", "'"));
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.Analyis_To_Be_Performed)))
                                {
                                    AnalysisToBePerofrmed = GetSafeTagName(checklistdetails.Analyis_To_Be_Performed.Trim().Replace("\"", "'"));
                                }
                            }
                        }
                    }
                }
                else
                {
                    MstRiskResult1 = RiskCategoryManagement.GetInternalControlAuditResultData(ScheduledOnID, Atbdid, Verticalid, AuditID);
                    if (MstRiskResult1 != null)
                    {
                        if (MstRiskResult1.ISACPORMIS != null)
                        {
                            ObjReport1 = Convert.ToString(MstRiskResult1.ISACPORMIS).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Observation)))
                        {
                            Observation = GetSafeTagName(MstRiskResult1.Observation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AuditObjective)))
                        {
                            AuditObjective = GetSafeTagName(MstRiskResult1.AuditObjective.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AuditSteps)))
                        {
                            AuditSteps = GetSafeTagName(MstRiskResult1.AuditSteps.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AnalysisToBePerofrmed)))
                        {
                            AnalysisToBePerofrmed = GetSafeTagName(MstRiskResult1.AnalysisToBePerofrmed.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ProcessWalkthrough)))
                        {
                            ProcessWalkthrough = GetSafeTagName(MstRiskResult1.ProcessWalkthrough.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ActivityToBeDone)))
                        {
                            ActivityToBeDone = GetSafeTagName(MstRiskResult1.ActivityToBeDone.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Population)))
                        {
                            Population = GetSafeTagName(MstRiskResult1.Population.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Sample)))
                        {
                            Sample = GetSafeTagName(MstRiskResult1.Sample.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObservationTitle)))
                        {
                            ObservationTitle = GetSafeTagName(MstRiskResult1.ObservationTitle.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Risk)))
                        {
                            Risk = GetSafeTagName(MstRiskResult1.Risk.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(MstRiskResult1.RootCost))
                        {
                            RootCost = Convert.ToString(MstRiskResult1.RootCost).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.FinancialImpact)))
                        {
                            FinancialImpact = Convert.ToString(MstRiskResult1.FinancialImpact).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Recomendation)))
                        {
                            Recomendation = GetSafeTagName(MstRiskResult1.Recomendation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ManagementResponse)))
                        {
                            ManagementResponse = GetSafeTagName(MstRiskResult1.ManagementResponse.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.FixRemark)))
                        {
                            FixRemark = GetSafeTagName(MstRiskResult1.FixRemark.Replace("\"", "'"));
                        }
                        TimeLine = MstRiskResult1.TimeLine != null ? MstRiskResult1.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                        ResponseDueDate = MstRiskResult1.ResponseDueDate != null ? MstRiskResult1.ResponseDueDate.Value.ToString("dd-MM-yyyy") : null;
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.BriefObservation)))
                        {
                            BriefObservation = GetSafeTagName(MstRiskResult1.BriefObservation.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObjBackground)))
                        {
                            ObjBackground = GetSafeTagName(MstRiskResult1.ObjBackground.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AnnexueTitle)))
                        {
                            AnnexueTitle = GetSafeTagName(MstRiskResult1.AnnexueTitle.Replace("\"", "'"));
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.DefeciencyType)))
                        {
                            DefeciencyType = Convert.ToString(MstRiskResult1.DefeciencyType);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Observation)))
                        {
                            hidObservation.Value = Convert.ToString(MstRiskResult1.Observation).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AuditObjective)))
                        {
                            hidAuditObjective.Value = Convert.ToString(MstRiskResult1.AuditObjective).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AuditSteps)))
                        {
                            hidAuditSteps.Value = Convert.ToString(MstRiskResult1.AuditSteps).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AnalysisToBePerofrmed)))
                        {
                            hidAnalysisToBePerformed.Value = Convert.ToString(MstRiskResult1.AnalysisToBePerofrmed).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ProcessWalkthrough)))
                        {
                            hidWalkthrough.Value = Convert.ToString(MstRiskResult1.ProcessWalkthrough).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ActivityToBeDone)))
                        {
                            hidActualWorkDone.Value = Convert.ToString(MstRiskResult1.ActivityToBeDone).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Population)))
                        {
                            hidpopulation.Value = Convert.ToString(MstRiskResult1.Population).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Sample)))
                        {
                            hidSample.Value = Convert.ToString(MstRiskResult1.Sample).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObservationNumber)))
                        {
                            hidObservationNumber.Value = MstRiskResult1.ObservationNumber;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObservationTitle)))
                        {
                            hidObservationTitle.Value = Convert.ToString(MstRiskResult1.ObservationTitle).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Risk)))
                        {
                            hidRisk.Value = Convert.ToString(MstRiskResult1.Risk).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.RootCost)))
                        {
                            hidRootcost.Value = Convert.ToString(MstRiskResult1.RootCost).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.FinancialImpact)))
                        {
                            hidfinancialImpact.Value = Convert.ToString(MstRiskResult1.FinancialImpact).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Recomendation)))
                        {
                            hidRecommendation.Value = Convert.ToString(MstRiskResult1.Recomendation).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ManagementResponse)))
                        {
                            hidMgtResponse.Value = Convert.ToString(MstRiskResult1.ManagementResponse).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.FixRemark)))
                        {
                            hidRemarks.Value = Convert.ToString(MstRiskResult1.FixRemark).Replace("\"", "'");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AuditScores)))
                        {
                            hidAuditStepScore.Value = Convert.ToString(MstRiskResult1.AuditScores);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ISACPORMIS)))
                        {
                            hidISACPORMIS.Value = Convert.ToString(MstRiskResult1.ISACPORMIS);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.AnnexueTitle)))
                        {
                            hidAnnaxureTitle.Value = Convert.ToString(MstRiskResult1.AnnexueTitle).Replace("\"", "'");
                        }
                        hidTimeLine.Value = Convert.ToString(MstRiskResult1.TimeLine);
                        hidPersonResponsible.Value = Convert.ToString(MstRiskResult1.PersonResponsible);
                        hidObservationRating.Value = Convert.ToString(MstRiskResult1.ObservationRating);
                        hidObservationCategory.Value = Convert.ToString(MstRiskResult1.ObservationCategory);
                        hidObservationSubCategory.Value = Convert.ToString(MstRiskResult1.ObservationSubCategory);
                        //hidGroupObservationCategory.Value = Convert.ToString(MstRiskResult1.GroupObservationCategoryID);
                        hidOwner.Value = Convert.ToString(MstRiskResult1.Owner);
                        hidUserID.Value = Convert.ToString(MstRiskResult1.UserID);

                        hidUHPersonResponsible.Value = Convert.ToString(MstRiskResult1.UHPersonResponsible);
                        hidPRESIDENTPersonResponsible.Value = Convert.ToString(MstRiskResult1.PRESIDENTPersonResponsible);
                        hidUHComment.Value = Convert.ToString(MstRiskResult1.UHComment);
                        hidPRESIDENTComment.Value = Convert.ToString(MstRiskResult1.PRESIDENTComment);
                        hidBodyContent.Value = Convert.ToString(MstRiskResult1.BodyContent);
                        hidResponseDueDate.Value = Convert.ToString(MstRiskResult1.ResponseDueDate);


                        BodyContent = MstRiskResult1.BodyContent;

                        var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyID(ScheduledOnID, Atbdid, FinYear, Period, 3, Verticalid, AuditID);
                        if (getDated != null)
                        {
                            Dated = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.PersonResponsible)))
                        {
                            PersonResponsible = Convert.ToString(MstRiskResult1.PersonResponsible);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.Owner)))
                        {
                            Owner = Convert.ToString(MstRiskResult1.Owner);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObservationRating)))
                        {
                            ObservationRating = Convert.ToString(MstRiskResult1.ObservationRating);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObservationCategory)))
                        {
                            ObservationCategory = Convert.ToString(MstRiskResult1.ObservationCategory);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.ObservationSubCategory)))
                        {
                            ObservationSubCategory = Convert.ToString(MstRiskResult1.ObservationSubCategory);
                        }
                        //if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult1.GroupObservationCategoryID)))
                        //{
                        //    GroupObservationCategoryID = Convert.ToString(MstRiskResult1.GroupObservationCategoryID);
                        //}
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            string audioVideoLink = string.Empty;
                            List<ObservationAudioVideo> observationAudioVideoList = new List<ObservationAudioVideo>();
                            observationAudioVideoList = (from cs in entities.ObservationAudioVideos
                                                         where cs.ATBTID == Atbdid && cs.AuditId == AuditID
                                                         && cs.IsActive == true
                                                         select cs).ToList();
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo item in observationAudioVideoList)
                                {
                                    audioVideoLink = audioVideoLink + "," + item.AudioVideoLink;
                                }
                                audioVideoLink = audioVideoLink.TrimStart(',');
                            }
                            AudioVideoLink = Convert.ToString(audioVideoLink).Replace("\"", "'");
                        }
                    }
                    else if (MstRiskResult1 == null)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var data = entities.Sp_GetPreviousAuditProcessWalkthrough(ATBDID, CustomerBranchID, Verticalid, AuditID).FirstOrDefault();
                            if (data != null)
                            {
                                ProcessWalkthrough = GetSafeTagName(data.ProcessWalkthrough.Replace("\"", "'"));
                            }
                            var getpersonresponsiblebydefault = (
                                                from row in entities.RiskActivityTransactions
                                                join row1 in entities.RiskActivityToBeDoneMappings
                                                on row.Id equals row1.RiskActivityId
                                                where row.ProcessId == row1.ProcessId
                                                && row.CustomerBranchId == CustomerBranchID
                                                && row1.ID == ATBDID && row.VerticalsId == Verticalid
                                                select row
                                         ).FirstOrDefault();


                            if (getpersonresponsiblebydefault != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(getpersonresponsiblebydefault.PersonResponsible)))
                                {
                                    PersonResponsible = Convert.ToString(getpersonresponsiblebydefault.PersonResponsible);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(getpersonresponsiblebydefault.ControlOwner)))
                                {
                                    Owner = Convert.ToString(getpersonresponsiblebydefault.ControlOwner);
                                }
                            }

                            var checklistdetails = (from row in entities.RiskActivityToBeDoneMappings
                                                    where row.ID == ATBDID
                                                    select row).FirstOrDefault();
                            if (checklistdetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.ActivityTobeDone)))
                                {
                                    AuditSteps = GetSafeTagName(checklistdetails.ActivityTobeDone.Trim().Replace("\"", "'"));
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.AuditObjective)))
                                {
                                    AuditObjective = GetSafeTagName(checklistdetails.AuditObjective.Trim().Replace("\"", "'"));
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(checklistdetails.Analyis_To_Be_Performed)))
                                {
                                    AnalysisToBePerofrmed = GetSafeTagName(checklistdetails.Analyis_To_Be_Performed.Trim().Replace("\"", "'"));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static string GetSafeTagName(string tag)
        {
            tag = tag.Replace("'", "`")
            .Replace('"', '`')
            .Replace('‐', '_');
            tag = Regex.Replace(tag, @"\s+", " "); //multiple spaces with single spaces
            return tag;
        }

        private void BindStatusList()
        {
            try
            {
                int checkstatus = 0;
                bool checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(CustomerId, BranchID);
                var isAuditmanager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                var rolelist = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                List<long> ProcessList = new List<long>();
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessList.Add(Convert.ToInt32(Request.QueryString["PID"]));
                }
                if (checkPRFlag)
                {
                    checkstatus = 7;
                }
                else
                {
                    checkstatus = 4;
                }
                rdbtnStatus.Items.Clear();
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(checkstatus);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
                var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(Request.QueryString["AuditID"]), ProcessList);
                if (checkPRFlag)
                {
                    if (StatusID == 6)
                    {
                        if (isAuditmanager == "AM" || isAuditmanager == "AH")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || st.ID == 6 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }

                    }
                    else if (StatusID == 5)
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 6) || (st.ID == 2))
                            {
                            }
                            else
                            {
                                rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else if (StatusID == 4)
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 5))
                            {
                                if (!(st.ID == 2))
                                {
                                    rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                            else
                            {
                                if (!(st.ID == 5))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                    }
                }//Personresponsible End
                else
                {
                    foreach (InternalAuditStatu st in allowedStatusList)
                    {
                        if (!(st.ID == 6))
                        {
                            if (!(st.ID == 2))
                            {
                                rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                    //lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
                }
                //lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}