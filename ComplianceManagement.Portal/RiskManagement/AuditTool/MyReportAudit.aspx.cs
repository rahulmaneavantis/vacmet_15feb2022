﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Spire.Presentation.Drawing.Animation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class MyReportAudit : System.Web.UI.Page
    {
        public static List<int?> Branchlist = new List<int?>();
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        static bool auditHeadFlag;
        static bool MgmtFlag;
        protected static bool ManagementFlag = false;
        static bool departmenthead;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected bool DepartmentHead = false;
        protected int CustomerId = 0;
        public static bool ApplyFilter; 
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (CustomerManagementRisk.CheckIsManagement(Portal.Common.AuthenticationHelper.UserID) == 8)
            {
                ManagementFlag = true;
            }

            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        auditHeadFlag = true;
                        ShowAuditHead(sender, e);
                    }
                }
                else if (DepartmentHead)
                {
                    departmenthead = true;
                    ShowDepartmentHead(sender, e);
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                }
                ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "active");
            liReviewer.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "");
            PerformerFlag = true;
            ReviewerFlag = false;
            departmenthead = false;
            auditHeadFlag = false;
            BindData();
            bindPageNumber();
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "active");
            liDepartmentHead.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "");
            PerformerFlag = false;
            ReviewerFlag = true;
            departmenthead = false;
            auditHeadFlag = false;
            BindData();
            bindPageNumber();
        }
        protected void ShowDepartmentHead(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "active");
            liAuditHead.Attributes.Add("class", "");
            PerformerFlag = false;
            ReviewerFlag = false;
            departmenthead = true;
            auditHeadFlag = false;
            BindData();
            bindPageNumber();
        }
        protected void ShowAuditHead(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "active");
            PerformerFlag = false;
            ReviewerFlag = false;
            departmenthead = false;
            auditHeadFlag = true;
            BindData();
            bindPageNumber();
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }
        public void BindLegalEntityData()
        {
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (DepartmentHead)
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
             
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            else
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataPerformerReviewer(CustomerId, userID);
            }
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (departmenthead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (auditHeadFlag)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdSummaryDetailsAuditCoverage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lvlfinyear = (Label)e.Row.Cells[3].FindControl("lblFinYear");
                string FinancialYear = lvlfinyear.Text;

                Label lblPeriod = (Label)e.Row.Cells[4].FindControl("lblPeriod");
                string Period = lblPeriod.Text;

                Label lblVerticalId = (Label)e.Row.Cells[9].FindControl("lblVerticalId");
                int VerticalId = Convert.ToInt32(lblVerticalId.Text);

                Label lblCustomerBranchId = (Label)e.Row.Cells[10].FindControl("lblCustomerBranchId");
                int CustomerBranchId = Convert.ToInt32(lblCustomerBranchId.Text);

                Label lblAuditID = (Label)e.Row.Cells[11].FindControl("lblAuditID");
                int AuditID = Convert.ToInt32(lblAuditID.Text);

                //Draft Report
                //List<Tran_DraftClosureUpload> DraftClosureDocument = new List<Tran_DraftClosureUpload>();
                //DraftClosureDocument = RiskCategoryManagement.GetFileDataDraftClosureDocument(AuditID).ToList();
                //if (DraftClosureDocument.Count == 0)
                //{
                //    LinkButton lblDocumentDownLoadDraftFile = (LinkButton) e.Row.Cells[5].FindControl("lblDocumentDownLoadDraftFile");
                //    lblDocumentDownLoadDraftFile.Visible = false;
                //}
                List<Tran_FinalDeliverableUpload> FinalDeliverableDocument1 = new List<Tran_FinalDeliverableUpload>();
                FinalDeliverableDocument1 = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID).ToList();
                if (FinalDeliverableDocument1.Count == 0)
                {
                    LinkButton lblDocumentDownLoadDraftFile = (LinkButton)e.Row.Cells[5].FindControl("lblDocumentDownLoadDraftFile");
                    lblDocumentDownLoadDraftFile.Visible = false;
                }
                //Final Deleverables
                List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID).ToList();
                FinalDeliverableDocument = FinalDeliverableDocument.Where(entry => entry.VersionComment == "FinalFile").ToList();
                if (FinalDeliverableDocument.Count == 0)
                {
                    LinkButton lblDocumentDownLoadfile1 = (LinkButton)e.Row.Cells[6].FindControl("lblDocumentDownLoadfile1");
                    lblDocumentDownLoadfile1.Visible = false;
                }
                //Audit Committe
                List<Tran_AuditCommitePresantionUpload> AuditCommitteeDocument = new List<Tran_AuditCommitePresantionUpload>();
                AuditCommitteeDocument = DashboardManagementRisk.GetFileDataAuditCommiteDocument(AuditID);
                if (AuditCommitteeDocument.Count == 0)
                {
                    LinkButton lblDocumentDownLoadfile = (LinkButton)e.Row.Cells[7].FindControl("lblDocumentDownLoadfile");
                    lblDocumentDownLoadfile.Visible = false; // changed by sagar more on 24-01-2020
                }
                //New method 1
                List<FeedbackFormUpload> ObjFeedbackFormUpload = new List<FeedbackFormUpload>();
                ObjFeedbackFormUpload = DashboardManagementRisk.GetFileDataFeedBackFormDocument(AuditID).ToList();
                if (ObjFeedbackFormUpload.Count == 0)
                {
                    LinkButton lblFeedbackDownLoadfile1 = (LinkButton)e.Row.Cells[8].FindControl("lblFeedbackDownLoadfile1");
                    lblFeedbackDownLoadfile1.Visible = false;
                }
            }
        }
        protected void grdSummaryDetailsAuditCoverage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int CustomerBranchId = Convert.ToInt32(commandArgs[0]);
                int VerticalId = Convert.ToInt32(commandArgs[1]);
                string FinancialYear = Convert.ToString(commandArgs[2]);
                string ForMonth = Convert.ToString(commandArgs[3]);
                int AuditID = Convert.ToInt32(commandArgs[4]);
                if (e.CommandName.Equals("AuditCommitteeDocumentDownLoadfile"))
                {
                    #region
                    List<Tran_AuditCommitePresantionUpload> AuditCommitteeDocument = new List<Tran_AuditCommitePresantionUpload>();
                    if (AuditCommitteeDocument != null)
                    {
                        using (ZipFile AuditZip = new ZipFile())
                        {
                            AuditCommitteeDocument = DashboardManagementRisk.GetFileDataAuditCommiteDocument(AuditID);
                            if (AuditCommitteeDocument.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in AuditCommitteeDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                AuditZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=AuditCommitteeDocuments.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                //Response.End();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            else
                            {
                                // code comment by Sagar More on 24-01-2020

                                //cvDuplicateEntry.IsValid = false;
                                //cvDuplicateEntry.ErrorMessage = "No Document available to Download.";

                                // added by sagar more on 24-01-2020
                                #region Sheet 1
                                using (ExcelPackage exportPackge = new ExcelPackage())
                                {
                                    try
                                    {
                                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Major Issues");
                                        String FileName = String.Empty;
                                        DataTable ExcelData = null;

                                        ApplyFilter = false;
                                        int RoleID = -1;

                                        RoleID = -1;
                                        if (PerformerFlag)
                                            RoleID = 3;
                                        if (ReviewerFlag)
                                            RoleID = 4;
                                        int customerID = -1;
                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        int CustBranchID = -1;
                                        int VerticalID = -1;
                                        int ProcessID = -1;
                                        String Period = String.Empty;
                                        string PeriodValueYear = string.Empty;
                                        string Year1 = string.Empty;
                                        string Year2 = string.Empty;

                                        if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                                        {
                                            if (ddlLegalEntity.SelectedValue != "-1")
                                            {
                                                CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                                        {
                                            if (ddlSubEntity1.SelectedValue != "-1")
                                            {
                                                CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                                        {
                                            if (ddlSubEntity2.SelectedValue != "-1")
                                            {
                                                CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                                        {
                                            if (ddlSubEntity3.SelectedValue != "-1")
                                            {
                                                CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                                        {
                                            if (ddlFilterLocation.SelectedValue != "-1")
                                            {
                                                CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                                        {
                                            if (ddlFinancialYear.SelectedValue != "-1")
                                            {
                                                FinancialYear = ddlFinancialYear.SelectedItem.Text;
                                                if (!string.IsNullOrEmpty(FinancialYear))
                                                {
                                                    Year1 = FinancialYear.Substring(0, FinancialYear.IndexOf("-")).Trim();
                                                    Year2 = FinancialYear.Substring(5, FinancialYear.IndexOf("-")).Trim();
                                                }
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                                        {
                                            if (ddlPeriod.SelectedValue != "-1")
                                            {
                                                Period = ddlPeriod.SelectedItem.Text;
                                                if (Period.Equals("Jan-Mar") || Period.Equals("Jan") || Period.Equals("Feb") || Period.Equals("Mar"))
                                                {
                                                    PeriodValueYear = Year2;
                                                }
                                                else if (Period.Equals("Oct-Mar"))
                                                {
                                                    PeriodValueYear = FinancialYear;
                                                }
                                                else if (Period.Equals("Apr-Sep") || Period.Equals("Apr-Jun") || Period.Equals("Jul-Sep") || Period.Equals("Oct-Dec") || Period.Equals("Apr") || Period.Equals("May") || Period.Equals("Jun") || Period.Equals("Jul") || Period.Equals("Aug") || Period.Equals("Sep") || Period.Equals("Oct") || Period.Equals("Nov") || Period.Equals("Dec"))
                                                {
                                                    PeriodValueYear = Year1;
                                                }
                                            }
                                        }

                                        if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                                        {
                                            int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                                            if (vid != -1)
                                            {
                                                VerticalID = vid;
                                            }
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                                            {
                                                if (ddlVertical.SelectedValue != "-1")
                                                {
                                                    VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                                                }
                                            }
                                        }

                                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                                        {
                                            if (RoleID == -1)
                                            {
                                                RoleID = 4;
                                            }
                                            Branchlist.Clear();
                                            var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                                            var Branchlistloop = Branchlist.ToList();
                                        }
                                        else
                                        {
                                            Branchlist.Clear();
                                            var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                                            var Branchlistloop = Branchlist.ToList();
                                        }

                                        List<CoalReviewKPUnitView> assignmentList = null;
                                        assignmentList = ProcessManagement.GetFinalAuditCommitteeView(RoleID, customerID, Portal.Common.AuthenticationHelper.UserID, VerticalID, ProcessID, FinancialYear, Period, Branchlist, AuditID);
                                        string locatioName = null;
                                        string ClientName = null;
                                        if (CustBranchID != 1)
                                        {
                                            using (AuditControlEntities entities = new AuditControlEntities())
                                            {
                                                locatioName = (from row in entities.mst_CustomerBranch
                                                               where row.ID == CustBranchID
                                                               select row.Name).FirstOrDefault();

                                                ClientName = (from row in entities.mst_Customer
                                                              where row.ID == customerID
                                                              select row.Name).FirstOrDefault();
                                            }
                                        }

                                        DataTable table = new DataTable();
                                        table.Columns.Add("Id", typeof(string));
                                        table.Columns.Add("ProcessId", typeof(long));
                                        table.Columns.Add("CustomerbranchId", typeof(long));
                                        table.Columns.Add("ForPeriod", typeof(string));
                                        table.Columns.Add("FinancialYear", typeof(string));
                                        table.Columns.Add("Rs", typeof(string));
                                        table.Columns.Add("Name", typeof(string));
                                        table.Columns.Add("Observation", typeof(string));
                                        table.Columns.Add("ManagementResponse", typeof(string));
                                        table.Columns.Add("PersonResponsible", typeof(string));
                                        table.Columns.Add("Owner", typeof(string));
                                        table.Columns.Add("TimeLine", typeof(string));
                                        table.Columns.Add("ObservationRating", typeof(string));

                                        int i = 0;
                                        int pIDchk = 0;
                                        int chkprcoessId = 1;

                                        int observaioncategoryIdchk = 0;
                                        int chkincrementobservaioncategoryId = 1;

                                        int locationidchk = 0;
                                        int checklocationid = 1;

                                        string observationtitlechk = string.Empty;
                                        int chkobservationtitleId = 1;
                                        string strAlpha = string.Empty;

                                        assignmentList = assignmentList.OrderBy(a => a.ProcessOrder).ToList();
                                        foreach (var cc in assignmentList)
                                        {
                                            string checktestATBTID = cc.ATBDId.ToString();

                                            if (observaioncategoryIdchk != cc.ObservationCategory)
                                            {
                                                checklocationid = 1;
                                                chkprcoessId = 1;
                                                chkobservationtitleId = 1;
                                                strAlpha = null;
                                                for (int f = 65; f <= 90; f++) //
                                                {
                                                    if ((chkincrementobservaioncategoryId + 64) == f)
                                                    {
                                                        strAlpha += ((char)f).ToString() + " ";
                                                    }
                                                }

                                                observaioncategoryIdchk = Convert.ToInt16(cc.ObservationCategory);
                                                table.Rows.Add(strAlpha, null, null, "", "", "", "", cc.ObsevationCategoryName, "", "", "", null, "0");

                                                string FetchRoman = ToRoman(checklocationid);
                                                locationidchk = Convert.ToInt16(cc.CustomerbranchId);
                                                table.Rows.Add(FetchRoman, null, null, "", "", "", "", cc.LocationName, "", "", "", null, "4");

                                                pIDchk = Convert.ToInt32(cc.ProcessId);
                                                table.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");

                                                string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;

                                                observationtitlechk = Convert.ToString(cc.ATBDId);
                                                table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                                                chkprcoessId++;
                                                chkincrementobservaioncategoryId += 1;
                                                checklocationid++;
                                                chkobservationtitleId++;
                                                i = 0;
                                            }
                                            else if (locationidchk != cc.CustomerbranchId)
                                            {
                                                chkprcoessId = 1;
                                                chkobservationtitleId = 1;

                                                string FetchRoman = ToRoman(checklocationid);
                                                locationidchk = Convert.ToInt16(cc.CustomerbranchId);
                                                table.Rows.Add(FetchRoman, null, null, "", "", "", "", cc.LocationName, "", "", "", null, "4");

                                                pIDchk = Convert.ToInt32(cc.ProcessId);
                                                table.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");

                                                string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;
                                                observationtitlechk = Convert.ToString(cc.ATBDId);
                                                table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                                                chkobservationtitleId++;
                                                checklocationid++;
                                                chkprcoessId++;
                                                i = 0;
                                            }
                                            else if (pIDchk != cc.ProcessId)
                                            {
                                                chkobservationtitleId = 1;
                                                pIDchk = Convert.ToInt32(cc.ProcessId);
                                                table.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");

                                                string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;
                                                observationtitlechk = Convert.ToString(cc.ATBDId);
                                                table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                                                chkobservationtitleId++;
                                                chkprcoessId++;
                                                i = 0;
                                            }
                                            else if (!observationtitlechk.Equals(checktestATBTID))
                                            {
                                                string fetchTitleId = (chkprcoessId - 1) + "." + chkobservationtitleId;
                                                observationtitlechk = Convert.ToString(cc.ObservationTitle);
                                                table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);
                                                chkobservationtitleId++;
                                                i = 0;
                                            }
                                            i++;
                                            table.Rows.Add("", cc.ProcessId, cc.CustomerbranchId, cc.ForPeriod, cc.FinancialYear, cc.Rs, cc.Name, cc.Observation, cc.ManagementResponse,
                                                cc.PersonResponsible, cc.Owner, Convert.ToString(cc.TimeLine), "9");
                                        }

                                        DataView view = new System.Data.DataView(table as DataTable);
                                        ExcelData = view.ToTable("Selected", false, "Id", "Observation", "Rs", "ManagementResponse", "PersonResponsible", "Owner", "TimeLine", "ObservationRating");

                                        exWorkSheet1.Cells["A1"].Value = "[A] MAJOR ISSUES";// "MANAGEMENT AUDIT REPORT";
                                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["A1:H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["A1:H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["A1:H1"].Merge = true;
                                        exWorkSheet1.Cells["A1:H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["A1:H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663

                                        exWorkSheet1.Cells["A2"].Value = "Audit Period";
                                        exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A2"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["A2:H2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["A2:H2"].Merge = true;

                                        exWorkSheet1.Cells["A3"].Value = ClientName; //"Location : " + locatioName;
                                        exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["A3:H3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["A3:H3"].Merge = true;

                                        foreach (DataRow item in ExcelData.Rows)
                                        {
                                            if (Convert.ToInt32(item["ObservationRating"]) == 1)
                                            {
                                                item["ObservationRating"] = "High";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 3)
                                            {
                                                item["ObservationRating"] = "Low";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 2)
                                            {
                                                item["ObservationRating"] = "Mediuam";

                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 0)
                                            {
                                                item["ObservationRating"] = "ObservationCategory";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 4)
                                            {
                                                item["ObservationRating"] = "Location";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 5)
                                            {
                                                item["ObservationRating"] = "Process";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 6)
                                            {
                                                item["ObservationRating"] = "Title";
                                            }
                                            else
                                            {
                                                item["ObservationRating"] = "";
                                            }

                                            if (!string.IsNullOrEmpty(Convert.ToString(item["TimeLine"])))
                                            {
                                                item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                                            }
                                        }
                                        exWorkSheet1.Cells["A5"].LoadFromDataTable(ExcelData, true);
                                        exWorkSheet1.Cells["A4"].Value = "Sl. No.";
                                        exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A4"].AutoFitColumns(10);
                                        exWorkSheet1.Cells["A4:A5"].Merge = true;
                                        exWorkSheet1.Cells["A4:A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["A4:A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["B4"].Value = "Observation";
                                        exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B4"].AutoFitColumns(60);
                                        exWorkSheet1.Cells["B4:B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["B4:B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["B4:B5"].Merge = true;
                                        exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["C4"].Value = "Amount";
                                        exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["C4:C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["C4:C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["C4:C5"].Merge = true;
                                        exWorkSheet1.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["D4"].Value = "Agreed Action";//"Auditee's Response"
                                        exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["D4:D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["D4:D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["D4"].AutoFitColumns(25);
                                        exWorkSheet1.Cells["D4:D5"].Merge = true;
                                        exWorkSheet1.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["E4"].Value = "Person Responsible";//Person Responsible
                                        exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["E4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["E4:F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["E4"].AutoFitColumns(25);
                                        exWorkSheet1.Cells["E4:F4"].Merge = true;
                                        exWorkSheet1.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["E4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["E5"].Value = "Person Concerned";//Person Responsible
                                        exWorkSheet1.Cells["E5"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["E5"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["E5"].AutoFitColumns(25);
                                        exWorkSheet1.Cells["E5"].Merge = true;
                                        exWorkSheet1.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        exWorkSheet1.Cells["F5"].Value = "Owner";//Owner
                                        exWorkSheet1.Cells["F5"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["F5"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["F5"].Merge = true;
                                        exWorkSheet1.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                        exWorkSheet1.Cells["F5"].AutoFitColumns(25);

                                        exWorkSheet1.Cells["G4"].Value = "Time Line";
                                        exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["G4:G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["G4:G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["G4:G5"].Merge = true;
                                        exWorkSheet1.Cells["G4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["G4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                        exWorkSheet1.Cells["G4:G5"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["H4"].Value = "Risk";//Observation Rating
                                        exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["H4:H5"].Merge = true;
                                        exWorkSheet1.Cells["H4:H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["H4:H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["H4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                                        int lengthchk = Convert.ToInt32(ExcelData.Rows.Count) + 6;
                                        for (int k = 6; k < lengthchk; k++)
                                        {
                                            string chkCondition = "H" + k;
                                            string chkConditionobservation = "B" + k;

                                            string checkallcond = exWorkSheet1.Cells[chkCondition].Value.ToString();
                                            string nameprocess = exWorkSheet1.Cells[chkConditionobservation].Value.ToString();

                                            string checkforRs = "c" + k;
                                            string fetchvalueforRs = exWorkSheet1.Cells[checkforRs].Value.ToString();
                                            if (!string.IsNullOrEmpty(fetchvalueforRs))
                                            {
                                                exWorkSheet1.Cells[checkforRs].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            }
                                            string checkTimeLine = "G" + k;
                                            string fetchTimeLine = exWorkSheet1.Cells[checkTimeLine].Value.ToString();
                                            if (!string.IsNullOrEmpty(fetchTimeLine))
                                            {
                                                exWorkSheet1.Cells[checkTimeLine].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            }

                                            string ConditionoSlnoallighment = "A" + k;

                                            exWorkSheet1.Cells[ConditionoSlnoallighment].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                                            if (checkallcond.Equals("Location") || checkallcond.Equals("Process"))
                                            {
                                                string chkConditionoSlno = "A" + k;
                                                string final = chkConditionobservation + ":" + chkCondition;

                                                exWorkSheet1.Cells[final].Merge = true;
                                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BCD6EE"));//#8696B8
                                                exWorkSheet1.Cells[final].Value = nameprocess;
                                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BCD6EE"));//#1F5663 

                                                if (checkallcond.Equals("Location"))
                                                {
                                                    exWorkSheet1.Cells[chkConditionoSlno].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                                    exWorkSheet1.Cells[chkConditionoSlno].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                }
                                            }
                                            if (checkallcond.Equals("Title"))
                                            {
                                                string chkConditionoSlno = "A" + k;
                                                string final = chkConditionobservation + ":" + chkCondition;

                                                exWorkSheet1.Cells[final].Merge = true;
                                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#F8CBAC"));//#1F5663
                                                exWorkSheet1.Cells[final].Value = nameprocess;
                                                exWorkSheet1.Cells[final].Style.Font.Bold = true;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#F8CBAC"));//#1F5663 
                                            }

                                            if (checkallcond.Equals("ObservationCategory"))
                                            {
                                                string chkConditionoSlno = "A" + k;
                                                string final = chkConditionobservation + ":" + chkCondition;

                                                exWorkSheet1.Cells[final].Merge = true;
                                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFC000"));//#1F5663
                                                exWorkSheet1.Cells[final].Value = nameprocess;
                                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFC000"));//#1F5663 

                                                exWorkSheet1.Cells[chkConditionoSlno].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            }

                                            if (checkallcond.Equals("High"))
                                            {
                                                string chkConditionoSlno = "A" + k;
                                                string final = chkConditionobservation + ":" + checkTimeLine;

                                                exWorkSheet1.Cells[final].Merge = true;
                                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                                                exWorkSheet1.Cells[final].Value = nameprocess;
                                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663

                                                exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);//#1F5663
                                                exWorkSheet1.Cells[chkCondition].Value = "";
                                            }
                                            else if (checkallcond.Equals("Mediuam"))
                                            {

                                                string chkConditionoSlno = "A" + k;
                                                string final = chkConditionobservation + ":" + checkTimeLine;

                                                exWorkSheet1.Cells[final].Merge = true;
                                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                                                exWorkSheet1.Cells[final].Value = nameprocess;
                                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663

                                                exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663
                                                exWorkSheet1.Cells[chkCondition].Value = "";
                                            }
                                            else if (checkallcond.Equals("Low"))
                                            {
                                                string chkConditionoSlno = "A" + k;
                                                string final = chkConditionobservation + ":" + checkTimeLine;

                                                exWorkSheet1.Cells[final].Merge = true;
                                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                                                exWorkSheet1.Cells[final].Value = nameprocess;
                                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663

                                                exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);//#1F5663
                                                exWorkSheet1.Cells[chkCondition].Value = "";
                                            }
                                        }
                                        int count = Convert.ToInt32(ExcelData.Rows.Count) + 5;
                                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 8])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                            string directoryPath = string.Empty;
                                            directoryPath = Server.MapPath("~/TempAuditCommitteeFinalAuditReport/" + customerID + "/"
                                                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                                           + Convert.ToString(FinancialYear) + "/" + AuditID);
                                            if (!Directory.Exists(directoryPath))
                                            {
                                                DocumentManagement.CreateDirectory(directoryPath);
                                            }
                                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                                            string fileName = "AuditCommitteeReport" + ".xlsx";
                                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(fileName));
                                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                            string finalFilePath = string.Empty;
                                            foreach (KeyValuePair<string, Byte[]> item in Filelist1)
                                            {
                                                finalFilePath = item.Key;
                                            }
                                            List<KeyValuePair<string, string>> reportFilePath = new List<KeyValuePair<string, string>>();
                                            if (!string.IsNullOrEmpty(finalFilePath))
                                                reportFilePath.Add(new KeyValuePair<string, string>(fileName, finalFilePath));
                                            if (reportFilePath.Count > 0)
                                            {
                                                AuditZip.AddDirectoryByName(FinancialYear);
                                                foreach (var file in reportFilePath)
                                                {
                                                    string filePath = file.Value;
                                                    if (file.Value != null && File.Exists(filePath))
                                                    {
                                                        string[] filename = file.Value.Split('.');
                                                        string[] docFilePath = filePath.Split(new string[] { FinancialYear }, StringSplitOptions.None);
                                                        AuditZip.AddEntry(FinancialYear + "/" + file.Key, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                var zipMs = new MemoryStream();
                                                AuditZip.Save(zipMs);
                                                zipMs.Position = 0;
                                                byte[] Filedata = zipMs.ToArray();
                                                Response.Buffer = true;
                                                Response.ClearContent();
                                                Response.ClearHeaders();
                                                Response.Clear();
                                                Response.ContentType = "application/zip";
                                                Response.AddHeader("content-disposition", "attachment; filename=AuditCommiteeDocuments.zip");
                                                Response.BinaryWrite(Filedata);
                                                Response.Flush();
                                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "There is no any file for download.";
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("DRDocumentDownLoadfile"))
                {
                    #region              
                    List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                    if (FinalDeliverableDocument != null)
                    {
                        using (ZipFile AuditZip = new ZipFile())
                        {
                            FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID);
                            //AuditZip.AddDirectoryByName(FinancialYear);
                            if (FinalDeliverableDocument.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in FinalDeliverableDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {

                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                AuditZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=DraftReport.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                //Response.End();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                            }
                        }
                    }
                    #endregion
                    #region              
                    //List<Tran_DraftClosureUpload> DraftClosureDocument = new List<Tran_DraftClosureUpload>();
                    //if (DraftClosureDocument != null)
                    //{
                    //    using (ZipFile AuditZip = new ZipFile())
                    //    {

                    //        DraftClosureDocument = RiskCategoryManagement.GetFileDataDraftClosureDocument(AuditID);
                    //        AuditZip.AddDirectoryByName(FinancialYear);
                    //        if (DraftClosureDocument.Count > 0)
                    //        {
                    //            int i = 0;
                    //            foreach (var file in DraftClosureDocument)
                    //            {
                    //                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    //                if (file.FilePath != null && File.Exists(filePath))
                    //                {

                    //                    int idx = file.FileName.LastIndexOf('.');
                    //                    string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                    //                    if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                    //                    {
                    //                        if (file.EnType == "M")
                    //                        {
                    //                            AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    //                        }
                    //                        else
                    //                        {
                    //                            AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    //                        }
                    //                    }                                        
                    //                    i++;
                    //                }
                    //            }

                    //            var zipMs = new MemoryStream();
                    //            AuditZip.Save(zipMs);
                    //            zipMs.Position = 0;
                    //            byte[] Filedata = zipMs.ToArray();
                    //            Response.Buffer = true;
                    //            Response.ClearContent();
                    //            Response.ClearHeaders();
                    //            Response.Clear();
                    //            Response.ContentType = "application/zip";
                    //            Response.AddHeader("content-disposition", "attachment; filename=DraftReportDocuments.zip");
                    //            Response.BinaryWrite(Filedata);
                    //            Response.Flush();
                    //            //Response.End();
                    //            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    //            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    //            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    //        }
                    //        else
                    //        {
                    //            cvDuplicateEntry.IsValid = false;
                    //            cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                    //        }
                    //    }
                    //}
                    #endregion
                }
                else if (e.CommandName.Equals("FDDocumentDownLoadfile"))
                {
                    #region              
                    List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                    if (FinalDeliverableDocument != null)
                    {
                        using (ZipFile AuditZip = new ZipFile())
                        {
                            FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID);
                            FinalDeliverableDocument = FinalDeliverableDocument.Where(entry => entry.VersionComment == "FinalFile").ToList();
                            //AuditZip.AddDirectoryByName(FinancialYear);
                            if (FinalDeliverableDocument.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in FinalDeliverableDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {

                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                AuditZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=FinalDeliverableDocuments.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                //Response.End();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                            }
                        }
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("FeedBackFormDownload"))
                {
                    #region
                    List<FeedbackFormUpload> ObjFeedbackFormUpload = new List<FeedbackFormUpload>();
                    if (ObjFeedbackFormUpload != null)
                    {
                        using (ZipFile AuditZip = new ZipFile())
                        {
                            ObjFeedbackFormUpload = DashboardManagementRisk.GetFileDataFeedBackFormDocument(AuditID).ToList();
                            //AuditZip.AddDirectoryByName(FinancialYear);
                            if (ObjFeedbackFormUpload.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ObjFeedbackFormUpload)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.Name.LastIndexOf('.');
                                        string str = file.Name.Substring(0, idx) + "_" + i + "." + file.Name.Substring(idx + 1);
                                        if (!AuditZip.ContainsEntry(FinancialYear + "/" + ForMonth + "/" + file.Version + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                AuditZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=FeedbackForm.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                //Response.End();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                            }
                        }
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("FDDocumentDownLoadfileAll"))
                {
                    #region
                    List<SP_MyReportFinalUnionAuditCommite_Result> FinalAllReportDocument = new List<SP_MyReportFinalUnionAuditCommite_Result>();
                    if (FinalAllReportDocument != null)
                    {
                        using (ZipFile AuditZip = new ZipFile())
                        {
                            FinalAllReportDocument = DashboardManagementRisk.GetFileDataAllReportDocument(AuditID).ToList();
                            //AuditZip.AddDirectoryByName(FinancialYear);
                            if (FinalAllReportDocument.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in FinalAllReportDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }
                                var zipMs = new MemoryStream();
                                AuditZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=FinalAllReportDocument.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                //Response.End();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            /*
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            */
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            bool DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindData()
        {
            try
            {
                String FinancialYear = String.Empty;
                String Period = String.Empty;
                string auditHeadManagerFlag = string.Empty;
                string departmentheadFlag = string.Empty;
                int userID = -1;
                int RoleID = -1;
                int CustBranchID = -1;
                int VerticalID = -1;

                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                if (auditHeadFlag)
                {
                    RoleID = 4;
                    auditHeadManagerFlag = AuditHeadOrManagerReport;
                }
                if (departmenthead)
                {
                    RoleID = 4;
                    departmentheadFlag = "DH";
                }
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        Session["BranchList"] = 0;
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                else
                {
                    List<long> branchs = new List<long>();
                    for (int i = 0; i < ddlLegalEntity.Items.Count; i++)
                    {
                        if (ddlLegalEntity.Items[i].Value != "-1")
                        {
                            if (ddlLegalEntity.Items[i].Value != "")
                            {
                                branchs.Add(Convert.ToInt64(ddlLegalEntity.Items[i].Value));
                                Session["BranchList"] = branchs;
                            }
                        }

                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                Branchlist.Clear();
                List<long> branch = new List<long>();
                branch = Session["BranchList"] as List<long>;
                if (branch != null)
                {
                    for (int i = 0; i < branch.Count; i++)
                    {
                        CustBranchID = Convert.ToInt32(branch[i]);
                        GetAllHierarchy(CustomerId, CustBranchID);
                    }
                }
                else
                {
                    var bracnhes = GetAllHierarchy(CustomerId, CustBranchID);
                }
                var Branchlistloop = Branchlist.ToList();

                var detailViewtest = UserManagementRisk.getAuditReports(CustomerId, Branchlist, VerticalID, FinancialYear, Period, RoleID, userID, auditHeadManagerFlag, departmentheadFlag);
                grdSummaryDetailsAuditCoverage.DataSource = detailViewtest;
                Session["TotalRows"] = detailViewtest.Count;
                grdSummaryDetailsAuditCoverage.DataBind();                
                //int count = Convert.ToInt32(GetTotalPagesCount());
                //if (count > 0)
                //{
                //    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                //    string chkcindition = (gridindex + 1).ToString();
                //    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }

            BindVertical();
            BindSchedulingType();
            BindData();
            bindPageNumber();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }
            BindVertical();
            BindSchedulingType();
            BindData();
            bindPageNumber();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }

            BindVertical();
            BindSchedulingType();
            BindData();
            bindPageNumber();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }
            BindVertical();
            BindSchedulingType();
            BindData();
            bindPageNumber();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVertical();
            BindSchedulingType();
            BindData();
            bindPageNumber();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                BindData();
                bindPageNumber();
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void btnACUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string flag = "true";
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (flag == "true")
                {
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    Button btn = (Button)sender;
                    GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                    FileUpload file = (FileUpload)gvr.FindControl("AuditcommitteeFileUpload");

                    Label lblCustomerBranchId = (Label)gvr.FindControl("lblCustomerBranchId");
                    int CustBranchId = Convert.ToInt32(lblCustomerBranchId.Text);

                    Label lblVerticalId = (Label)gvr.FindControl("lblVerticalId");
                    int VerticalId = Convert.ToInt32(lblVerticalId.Text);

                    Label lblFinYear = (Label)gvr.FindControl("lblFinYear");
                    string FinYear = Convert.ToString(lblFinYear.Text);

                    Label lblPeriod = (Label)gvr.FindControl("lblPeriod");
                    string Period = Convert.ToString(lblPeriod.Text);

                    Label lblAuditID = (Label)gvr.FindControl("lblAuditID");
                    int AuditID = Convert.ToInt32(lblAuditID.Text);
                    if (file != null)
                    {
                        if (file.HasFile)
                        {
                            Tran_AuditCommitePresantionUpload AuditCommitePresantionUpload = new Tran_AuditCommitePresantionUpload()
                            {
                                CustomerBranchId = Convert.ToInt32(CustBranchId),
                                FinancialYear = Convert.ToString(FinYear),
                                Period = Convert.ToString(Period),
                                CustomerId = CustomerId,
                                VerticalID = Convert.ToInt32(VerticalId),
                                FileName = Convert.ToString(file.FileName),
                                AuditID = AuditID,
                            };

                            int count = RiskCategoryManagement.Tran_AuditCommitteeUploadResultCount(AuditCommitePresantionUpload);
                            string directoryPath = "";

                            if (Convert.ToInt32(CustBranchId) != -1 && !string.IsNullOrEmpty(Convert.ToString(FinYear)) && !string.IsNullOrEmpty(Convert.ToString(Period)))
                            {
                                directoryPath = Server.MapPath("~/AuditCommitePresentationDocument/" + CustomerId + "/"
                                    + Convert.ToInt32(CustBranchId) + "/" + Convert.ToInt32(VerticalId) + "/"
                                    + Convert.ToString(FinYear) + "/" + Convert.ToString(Period) + "/"
                                    + "/" + (count + 1) + ".0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }

                            if (directoryPath != "")
                            {
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(file.FileName));

                                Stream fs = file.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (file.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        AuditCommitePresantionUpload.FileName = file.PostedFile.FileName;
                                        AuditCommitePresantionUpload.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        AuditCommitePresantionUpload.FileKey = fileKey.ToString();
                                        AuditCommitePresantionUpload.Version = (count + 1) + ".0";
                                        AuditCommitePresantionUpload.VersionDate = DateTime.UtcNow;
                                        AuditCommitePresantionUpload.CreatedDate = DateTime.Today.Date;
                                        AuditCommitePresantionUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        AuditCommitePresantionUpload.EnType = "A";
                                        bool Success1 = false;
                                        if (!RiskCategoryManagement.Tran_AuditCommitePresantionUploadResultExists(AuditCommitePresantionUpload))
                                        {
                                            Success1 = RiskCategoryManagement.CreateTran_AuditCommitePresantionUploadResult(AuditCommitePresantionUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                                bindPageNumber();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            Success1 = RiskCategoryManagement.CreateTran_AuditCommitePresantionUploadResult(AuditCommitePresantionUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                                bindPageNumber();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                    }
                                    SaveDocFiles(Filelist);
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                string flag = "true";

                if (flag == "true")
                {
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    Button btn = (Button)sender;
                    GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                    FileUpload file = (FileUpload)gvr.FindControl("FinalveriablesFileUpload");
                    Label lblCustomerBranchId = (Label)gvr.FindControl("lblCustomerBranchId");
                    int CustBranchId = Convert.ToInt32(lblCustomerBranchId.Text);

                    Label lblVerticalId = (Label)gvr.FindControl("lblVerticalId");
                    int VerticalId = Convert.ToInt32(lblVerticalId.Text);

                    Label lblFinYear = (Label)gvr.FindControl("lblFinYear");
                    string FinYear = Convert.ToString(lblFinYear.Text);

                    Label lblPeriod = (Label)gvr.FindControl("lblPeriod");
                    string Period = Convert.ToString(lblPeriod.Text);

                    Label lblAuditID = (Label)gvr.FindControl("lblAuditID");
                    int AuditID = Convert.ToInt32(lblAuditID.Text);

                    if (file != null)
                    {
                        if (file.HasFile)
                        {
                            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                            {
                                CustomerBranchId = Convert.ToInt32(CustBranchId),
                                FinancialYear = Convert.ToString(FinYear),
                                Period = Convert.ToString(Period),
                                CustomerId = CustomerId,
                                VerticalID = Convert.ToInt32(VerticalId),
                                FileName = Convert.ToString(file.FileName),
                                AuditID = AuditID,
                            };

                            string directoryPath = "";

                            int count = RiskCategoryManagement.Tran_FinalDeliverableUploadResultCount(FinalDeleverableUpload);

                            if (Convert.ToInt32(CustBranchId) != -1 && !string.IsNullOrEmpty(Convert.ToString(FinYear)) && !string.IsNullOrEmpty(Convert.ToString(Period)))
                            {
                                directoryPath = Server.MapPath("~/AuditFinalDeleverableDocument/" + CustomerId + "/"
                                    + Convert.ToInt32(CustBranchId) + "/" + Convert.ToInt32(VerticalId) + "/"
                                    + Convert.ToString(FinYear) + "/" + Convert.ToString(Period) + "/"
                                    + "/" + (count + 1) + ".0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            if (directoryPath != "")
                            {
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(file.FileName));

                                Stream fs = file.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (file.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        FinalDeleverableUpload.FileName = file.PostedFile.FileName;
                                        FinalDeleverableUpload.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        FinalDeleverableUpload.FileKey = fileKey.ToString();
                                        FinalDeleverableUpload.Version = (count + 1) + ".0";
                                        FinalDeleverableUpload.VersionDate = DateTime.UtcNow;
                                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        FinalDeleverableUpload.VersionComment = "FinalFile";

                                        bool Success1 = false;
                                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                        {
                                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                                bindPageNumber();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                                bindPageNumber();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                    }
                                    SaveDocFiles(Filelist);
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }
        protected void UploadFeedback_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                string flag = "true";

                if (flag == "true")
                {
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    Button btn = (Button)sender;
                    GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                    FileUpload file = (FileUpload)gvr.FindControl("FeedbackFileUpload");

                    Label lblCustomerBranchId = (Label)gvr.FindControl("lblCustomerBranchId");
                    int CustBranchId = Convert.ToInt32(lblCustomerBranchId.Text);

                    Label lblVerticalId = (Label)gvr.FindControl("lblVerticalId");
                    int VerticalId = Convert.ToInt32(lblVerticalId.Text);

                    Label lblFinYear = (Label)gvr.FindControl("lblFinYear");
                    string FinYear = Convert.ToString(lblFinYear.Text);

                    Label lblPeriod = (Label)gvr.FindControl("lblPeriod");
                    string Period = Convert.ToString(lblPeriod.Text);

                    Label lblAuditID = (Label)gvr.FindControl("lblAuditID");
                    int AuditID = Convert.ToInt32(lblAuditID.Text);


                    if (file != null)
                    {
                        if (file.HasFile)
                        {
                            string directoryPath = "";
                            FeedbackFormUpload objFeedbackFormUpload = new FeedbackFormUpload()
                            {
                                Name = file.FileName,
                                AuditId = Convert.ToInt64(AuditID),
                            };

                            int count = RiskCategoryManagement.FeedBackFormFileCount(objFeedbackFormUpload);

                            if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                            {
                                directoryPath = Server.MapPath("~/AuditFeedBackDocument/" + CustomerId + "/"
                                  + Convert.ToInt32(CustBranchId) + "/" + Convert.ToInt32(VerticalId) + "/"
                                  + Convert.ToString(FinYear) + "/" + Convert.ToString(Period) + "/"
                                  + "/" + (count + 1) + ".0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            if (directoryPath != "")
                            {
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(file.FileName));

                                Stream fs = file.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (file.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        objFeedbackFormUpload.Name = file.PostedFile.FileName;
                                        objFeedbackFormUpload.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        objFeedbackFormUpload.FileKey = fileKey.ToString();
                                        objFeedbackFormUpload.FileUploadedDate = DateTime.Today.Date;
                                        objFeedbackFormUpload.AuditId = AuditID;
                                        objFeedbackFormUpload.Version = (count + 1) + ".0";
                                        objFeedbackFormUpload.VersionDate = DateTime.UtcNow;

                                        bool Success1 = false;
                                        if (!RiskCategoryManagement.CheckFeedbackFormExists(objFeedbackFormUpload))
                                        {
                                            Success1 = RiskCategoryManagement.SaveFeedbackFormData(objFeedbackFormUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                                bindPageNumber();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                    }
                                    SaveDocFiles(Filelist);
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        protected void UploadDraft_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                string flag = "true";

                if (flag == "true")
                {
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    Button btn = (Button)sender;
                    GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                    FileUpload file = (FileUpload)gvr.FindControl("DraftFileUpload");
                    Label lblCustomerBranchId = (Label)gvr.FindControl("lblCustomerBranchId");
                    int CustBranchId = Convert.ToInt32(lblCustomerBranchId.Text);

                    Label lblVerticalId = (Label)gvr.FindControl("lblVerticalId");
                    int VerticalId = Convert.ToInt32(lblVerticalId.Text);

                    Label lblFinYear = (Label)gvr.FindControl("lblFinYear");
                    string FinYear = Convert.ToString(lblFinYear.Text);

                    Label lblPeriod = (Label)gvr.FindControl("lblPeriod");
                    string Period = Convert.ToString(lblPeriod.Text);

                    Label lblAuditID = (Label)gvr.FindControl("lblAuditID");
                    int AuditID = Convert.ToInt32(lblAuditID.Text);

                    if (file != null)
                    {
                        if (file.HasFile)
                        {
                            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                            {
                                CustomerBranchId = Convert.ToInt32(CustBranchId),
                                FinancialYear = Convert.ToString(FinYear),
                                Period = Convert.ToString(Period),
                                CustomerId = CustomerId,
                                VerticalID = Convert.ToInt32(VerticalId),
                                FileName = Convert.ToString(file.FileName),
                                AuditID = AuditID,
                            };

                            string directoryPath = "";

                            int count = RiskCategoryManagement.Tran_FinalDeliverableUploadResultCount(FinalDeleverableUpload);

                            if (Convert.ToInt32(CustBranchId) != -1 && !string.IsNullOrEmpty(Convert.ToString(FinYear)) && !string.IsNullOrEmpty(Convert.ToString(Period)))
                            {
                                directoryPath = Server.MapPath("~/AuditFinalDeleverableDocument/" + CustomerId + "/"
                                    + Convert.ToInt32(CustBranchId) + "/" + Convert.ToInt32(VerticalId) + "/"
                                    + Convert.ToString(FinYear) + "/" + Convert.ToString(Period) + "/"
                                    + "/" + (count + 1) + ".0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }
                            if (directoryPath != "")
                            {
                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(file.FileName));

                                Stream fs = file.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (file.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        FinalDeleverableUpload.FileName = file.PostedFile.FileName;
                                        FinalDeleverableUpload.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        FinalDeleverableUpload.FileKey = fileKey.ToString();
                                        FinalDeleverableUpload.Version = (count + 1) + ".0";
                                        FinalDeleverableUpload.VersionDate = DateTime.UtcNow;
                                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                                        bool Success1 = false;
                                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                        {
                                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                                bindPageNumber();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                            if (Success1 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
                                                BindData();
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                    }
                                    SaveDocFiles(Filelist);
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
            //try
            //{
            //    if (CustomerId == 0)
            //    {
            //        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //    }
            //    string flag = "true";

            //    if (flag == "true")
            //    {
            //        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            //        Button btn = (Button) sender;
            //        GridViewRow gvr = (GridViewRow) btn.NamingContainer;
            //        FileUpload file = (FileUpload) gvr.FindControl("DraftFileUpload");

            //        Label lblCustomerBranchId = (Label) gvr.FindControl("lblCustomerBranchId");
            //        int CustBranchId = Convert.ToInt32(lblCustomerBranchId.Text);

            //        Label lblVerticalId = (Label) gvr.FindControl("lblVerticalId");
            //        int VerticalId = Convert.ToInt32(lblVerticalId.Text);

            //        Label lblFinYear = (Label) gvr.FindControl("lblFinYear");
            //        string FinYear = Convert.ToString(lblFinYear.Text);

            //        Label lblPeriod = (Label) gvr.FindControl("lblPeriod");
            //        string Period = Convert.ToString(lblPeriod.Text);

            //        Label lblAuditID = (Label)gvr.FindControl("lblAuditID");
            //        int AuditID = Convert.ToInt32(lblAuditID.Text);

            //        if (file != null)
            //        {
            //            if (file.HasFile)
            //            {
            //                string directoryPath = "";

            //                Tran_DraftClosureUpload DraftClosureUpload = new Tran_DraftClosureUpload()
            //                {
            //                    CustomerBranchId = Convert.ToInt32(CustBranchId),
            //                    FinancialYear = Convert.ToString(FinYear),
            //                    Period = Convert.ToString(Period),
            //                    CustomerId = CustomerId,
            //                    VerticalID = Convert.ToInt32(VerticalId),
            //                    FileName = file.FileName,
            //                    AuditID= AuditID,
            //                };

            //                int count = RiskCategoryManagement.Tran_DraftClosureUploadResultCount(DraftClosureUpload);

            //                if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
            //                {                               
            //                    directoryPath = Server.MapPath("~/DraftClosureDocument/" + CustomerId + "/"
            //                      + Convert.ToInt32(CustBranchId) + "/" + Convert.ToInt32(VerticalId) + "/"
            //                      + Convert.ToString(FinYear) + "/" + Convert.ToString(Period) + "/"
            //                      + "/" + (count + 1) + ".0");
            //                }
            //                if (!Directory.Exists(directoryPath))
            //                {
            //                    DocumentManagement.CreateDirectory(directoryPath);
            //                }
            //                if (directoryPath != "")
            //                {
            //                    Guid fileKey = Guid.NewGuid();
            //                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(file.FileName));

            //                    Stream fs = file.PostedFile.InputStream;
            //                    BinaryReader br = new BinaryReader(fs);
            //                    Byte[] bytes = br.ReadBytes((Int32) fs.Length);
            //                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
            //                    if (file.PostedFile.ContentLength > 0)
            //                    {
            //                        using (AuditControlEntities entities = new AuditControlEntities())
            //                        {
            //                            DraftClosureUpload.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
            //                            DraftClosureUpload.FileKey = fileKey.ToString();
            //                            DraftClosureUpload.Version = (count + 1) + ".0";
            //                            DraftClosureUpload.VersionDate = DateTime.Today.Date;
            //                            DraftClosureUpload.CreatedDate = DateTime.Today.Date;
            //                            DraftClosureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

            //                            bool Success1 = false;
            //                            if (!RiskCategoryManagement.Tran_DraftClosureUploadResultExists(DraftClosureUpload))
            //                            {                                            
            //                                Success1 = RiskCategoryManagement.CreateTran_DraftClosureUploadResult(DraftClosureUpload);

            //                                if (Success1 == true)
            //                                {
            //                                    cvDuplicateEntry.IsValid = false;
            //                                    cvDuplicateEntry.ErrorMessage = "Uploaded Successfully";
            //                                    BindData();
            //                                }
            //                                else
            //                                {
            //                                    cvDuplicateEntry.IsValid = false;
            //                                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //                                }
            //                            }
            //                        }
            //                        SaveDocFiles(Filelist);
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                cvDuplicateEntry.IsValid = false;
            //                cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    //throw ex;
            //}
        }

        // added by sagar more on 24-01-2020
        private string ToRoman(int number)
        {

            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            //throw new ArgumentOutOfRangeException("something bad happened");
            throw new NotImplementedException();
        }

    }
}