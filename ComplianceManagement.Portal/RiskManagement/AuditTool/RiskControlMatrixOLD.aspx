﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RiskControlMatrixOLD.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.RiskControlMatrixOLD" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }           
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceInstanceValidationGroup" />
                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                </div>
                <table style="width: 100%">
                    <tr>
                        <td class="td1"></td>
                        <td class="td2">
                            <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 0px"
                                RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                <asp:ListItem Text="Process" Value="Process" Selected="True" />
                                <asp:ListItem Text="Others" Value="Others" />
                            </asp:RadioButtonList>
                        </td>
                        <td class="td3"></td>
                        <td class="td4"></td>
                        <td class="td5"></td>
                        <td class="td6">
                            <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" CausesValidation="false"
                                OnClick="lbtnExportExcel_Click">
                        <img src="../../Images/excel.png" alt="Export to Excel"
                            title="Export to Excel" width="30px" height="30px" /></asp:LinkButton>

                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Location:</label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" CssClass="txtbox"
                                Style="padding: 0px; margin: 0px; height: 22px; width: 250px;">
                            </asp:DropDownList>
                        </td>
                        <td class="td3">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 112px; display: block; float: left; font-size: 13px; color: #333;">
                                Process</label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList ID="ddlProcess" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                CssClass="txtbox" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlProcess" ID="rfvProcess"
                                runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                          
                        </td>
                        <td class="td5">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 112px; display: block; float: left; font-size: 13px; color: #333;">
                                Sub Process</label>
                        </td>
                        <td class="td6">
                            <asp:DropDownList ID="ddlSubProcess" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                CssClass="txtbox" OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                            </asp:DropDownList>
                             <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubProcess" ID="rfvSubProcess"
                                runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                         
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 112px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Risk Category:</label></td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlFilterRiskCategory" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlFilterRiskCategory_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;">
                            </asp:DropDownList></td>
                        <td class="td3">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 112px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Industry :</label></td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterIndustry" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlFilterIndustry_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;">
                            </asp:DropDownList></td>
                        <td class="td5">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 120px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Location Type:</label></td>
                        <td class="td6">
                            <asp:DropDownList runat="server" ID="ddlFilterLocationType" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlFilterLocationType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;">
                            </asp:DropDownList></td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Both" runat="server" Style="overflow: scroll; height: 420px; width: 1340px;">
                    <table>
                        <tr>
                            <td>
                                <div style="margin-bottom: 7px">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
                                        <ContentTemplate>                                       
                                            <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdRiskActivityMatrix_RowCreated"
                                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdRiskActivityMatrix_Sorting"
                                                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnRowDataBound="grdRiskActivityMatrix_RowDataBound" OnRowCancelingEdit="CancelEdit" OnRowUpdating="UpdateRisk" OnRowEditing="Edit"
                                                Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Task Id" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbltaskidItemTemplate" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                                             <asp:Label ID="lblRiskActivityIDItemTemplate" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>                                                           
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Control #">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lblControlNoEdit" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtControlNo" runat="server" Width="70px" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Risk Description" ItemStyle-HorizontalAlign="Justify">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRiskActivityDescriptionItemTemplate" runat="server" Text='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRiskActivityDescriptionEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("ActivityDescription") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtRiskActivityDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Control Objective" ItemStyle-HorizontalAlign="Justify">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblControlObjectiveItemTemplate" runat="server" Text='<%# Eval("ControlObjective") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtControlObjectiveEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("ControlObjective") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtControlObjectiveFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                                           
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                           <%-- <asp:DropDownCheckBoxes ID="ddlClientProcessEdit" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesClientProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="200"></Style>
                                                                <Texts SelectBoxCaption="Select Client" />
                                                            </asp:DropDownCheckBoxes>--%>
                                                             <asp:DropDownList ID="ddlClientProcessEdit" runat="server" Width="150px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                              <asp:DropDownList ID="ddlClientProcessFooterTemplate" runat="server" Width="150px"></asp:DropDownList>
                                                            <%--<asp:DropDownCheckBoxes ID="ddlClientProcess" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesClientProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Client" />
                                                            </asp:DropDownCheckBoxes>--%>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="RiskCategory">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRiskCategory" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownCheckBoxes ID="ddlRiskCategoryProcessEdit" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesRiskCategoryProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="200"></Style>
                                                                <Texts SelectBoxCaption="Select Risk Category" />
                                                            </asp:DropDownCheckBoxes>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownCheckBoxes ID="ddlRiskCategoryProcess" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesRiskCategoryProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Risk Category" />
                                                            </asp:DropDownCheckBoxes>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Assertions">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAssertions" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownCheckBoxes ID="ddlAssertionsProcessEdit" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesAssertionsProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="200"></Style>
                                                                <Texts SelectBoxCaption="Select Assertions" />
                                                            </asp:DropDownCheckBoxes>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownCheckBoxes ID="ddlAssertionsProcess" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesAssertionsProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Assertions" />
                                                            </asp:DropDownCheckBoxes>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Control Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtExistingControlDescriptionItemTemplate" runat="server" Width="200px" Text='<%# Eval("ControlDescription") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtExistingControlDescriptionEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("ControlDescription") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtExistingControlDescriptionFooterTemplate" runat="server" Width="194px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Mitigating Control">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtMControlDescriptionItemTemplate" runat="server" Width="100px" Text='<%# Eval("MControlDescription") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtMControlDescriptionEdit" runat="server" Width="100px" TextMode="MultiLine" Text='<%# Eval("MControlDescription") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtMControlDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Person Responsible">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlPersonResponsibleItemTemplate" runat="server" Width="100px" Text='<%#  ShowPersonResposibleName((long)Eval("PersonResponsible")) %>'></asp:Label>
                                                           
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlPersonResponsibleEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlPersonResponsibleFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Effective Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtEffectiveDateItemTemplate" runat="server"
                                                                Style="height: 16px; width: 100px;" CssClass="textbox" Text='<%# Eval("EffectiveDate")==null?"":ShowDate((DateTime)Eval("EffectiveDate")) %>' />                                                           
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEffectiveDateEdit" runat="server"  Style="height: 16px; width: 100px;" Text='<%# Eval("EffectiveDate")==null?"":ShowDate((DateTime)Eval("EffectiveDate")) %>' />
                                                         
                                                        </EditItemTemplate>
                                                        <FooterTemplate>                                                           
                                                            <asp:TextBox ID="txtEffectiveDateFooterTemplate" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Key">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlKeyItemTemplate" runat="server" Width="100px" Text='<%# ShowKeyName((long)Eval("Key")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlKeyEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlKeyFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Preventive Control">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlPreventiveItemTemplate" runat="server" Width="100px" Text='<%# ShowControlName((long)Eval("PrevationControl")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlPreventiveEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlPreventiveFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Automated Control">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlAutomatedItemTemplate" runat="server" Width="100px" Text='<%# ShowControlName((long)Eval("AutomatedControl")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlAutomatedEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlAutomatedFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Frequency">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlFrequencytemTemplate" runat="server" Width="100px" Text='<%# ShowFrequencyName((long)Eval("Frequency")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlFrequencyEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlFrequencyFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Gap Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtGapDescriptionItemTemplate" runat="server" Width="100px" Text='<%# Eval("GapDescription") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtGapDescriptionEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("GapDescription") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtGapDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Recommendations">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtRecommendationsItemTemplate" runat="server" Width="100px" Text='<%# Eval("Recommendations") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtRecommendationsEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("Recommendations") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtRecommendationsFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Action/ Remediation plan">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtActionRemediationplanItemTemplate" runat="server" Width="100px" Text='<%# Eval("ActionRemediationplan") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtActionRemediationplanEdit" runat="server" Width="100px" TextMode="MultiLine" Text='<%# Eval("ActionRemediationplan") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtActionRemediationplanFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="IPE(Information Produced by Entity)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtIPEItemTemplate" runat="server" Width="100px" Text='<%# Eval("IPE") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtIPEItemEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("IPE") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtIPEFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ERP System">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtERPsystemItemTemplate" runat="server" Width="100px" Text='<%# Eval("ERPSystem") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtERPsystemEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("ERPSystem") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtERPsystemFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="FRC(Fraud Risk Control)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtFRCItemTemplate" runat="server" Width="100px" Text='<%# Eval("FRC") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtFRCEdit" runat="server" Width="200px" TextMode="MultiLine" Text='<%# Eval("FRC") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtFRCFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Unique / Referred">
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtUniqueReferredItemTemplate" runat="server" Width="100px" Text='<%# Eval("UniqueReferred") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtUniqueReferredEdit" runat="server" Width="100px" TextMode="MultiLine" Text='<%# Eval("UniqueReferred") %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtUniqueReferredFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    
                                                     <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Risk Rating">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlRiskItemTemplate" runat="server" Width="100px" Text='<%# ShowRiskRatingName((int?)Eval("RiskRating")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlRiskEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlRiskFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Control Rating">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ddlControlItemTemplate" runat="server" Width="100px" Text='<%# ShowControlRatingName((int?)Eval("ControlRating")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlControlEdit" runat="server" Width="100px"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlControlFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField  ShowEditButton="true"  ButtonType="Image" EditImageUrl="~/Images/edit_icon.png"  CausesValidation="true"  ValidationGroup="ComplianceInstanceValidationGroup"/>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Add" Height="25px" Width="60px" CommandName="Footer" ValidationGroup="ComplianceInstanceValidationGroup" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="#CCCC99" />
                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                                <PagerSettings Position="Top" />
                                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                <AlternatingRowStyle BackColor="#E6EFF7" />

                                                <EmptyDataTemplate>
                                                    <tr style="background-color: #5c9ccc">
                                                        <th scope="col">Activity Description</th>
                                                        <th scope="col">Control Objective</th>
                                                        <th scope="col">Location</th>
                                                        <th scope="col">Risk Category</th>
                                                        <th scope="col">Assertions</th>
                                                        <th scope="col">Control Description</th>
                                                        <th scope="col">Mitigating Control</th>
                                                        <th scope="col">Person Responsible</th>
                                                        <th scope="col">EffectiveDate</th>
                                                        <th scope="col">Key</th>
                                                        <th scope="col">Preventive Control</th>
                                                        <th scope="col">Automated Control</th>
                                                        <th scope="col">Frequency</th>
                                                        <th scope="col">Gap Description</th>
                                                        <th scope="col">Recommendations</th>
                                                        <th scope="col">Action/ Remediation plan</th>
                                                        <th scope="col">IPC</th>
                                                        <th scope="col">ERP System</th>
                                                        <th scope="col">FRC</th>
                                                        <th scope="col">Unique / Referred</th>
                                                        <th scope="col">Risk Rating</th>
                                                        <th scope="col">Control Rating</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtRiskActivityDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtControlObjectiveFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                              <asp:DropDownList ID="ddlClientProcess" runat="server" Width="150px"></asp:DropDownList>
                                                           <%-- <asp:DropDownCheckBoxes ID="ddlClientProcess" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesClientProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Client" />
                                                            </asp:DropDownCheckBoxes>--%>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownCheckBoxes ID="ddlRiskCategoryProcess" runat="server" Width="200px"
                                                                AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True"
                                                                OnSelectedIndexChanged="checkBoxesRiskCategoryProcess_SelcetedIndexChanged">
                                                                <Style SelectBoxWidth="160" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                                                <Texts SelectBoxCaption="Select Risk Category" />
                                                            </asp:DropDownCheckBoxes>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAssertionsFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtExistingControlDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>

                                                        <td>
                                                            <asp:TextBox ID="txtMControlDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPersonResponsibleFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                           
                                                            <asp:TextBox ID="txtEffectiveDateFooterTemplate" CssClass="StartDate" runat="server"></asp:TextBox>

                                                            <asp:RegularExpressionValidator runat="server" ControlToValidate="txtEffectiveDateFooterTemplate" ValidationExpression="(((0|1)[1-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$"
                                                                ErrorMessage="Invalid Date Format" ForeColor="Red" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlKeyFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPreventiveFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAutomatedFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlFrequencyFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>

                                                        <td>
                                                            <asp:TextBox ID="txtGapDescriptionFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtRecommendationsFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtActionRemediationplanFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>

                                                        <td>
                                                            <asp:TextBox ID="txtIPEFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtERPsystemFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />

                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFRCFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />

                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtUniqueReferredFooterTemplate" runat="server" Width="100px" TextMode="MultiLine" />
                                                        </td>
                                                      
                                                         <td>
                                                            <asp:DropDownList ID="ddlRiskFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                         <td>
                                                            <asp:DropDownList ID="ddlControlFooterTemplate" runat="server" Width="100px"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Add" CommandName="EmptyDataTemplate" Height="25px" Width="60px" ValidationGroup="ComplianceInstanceValidationGroup" />
                                                        </td>
                                                    </tr>
                                                </EmptyDataTemplate>
                                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div style="display: none;">
                    <asp:GridView ID="GridExportExcel" runat="server" AutoGenerateColumns="false" AllowPaging="false" 
                        GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black">
                        <Columns>
                            <asp:BoundField DataField="ControlNo" HeaderText="Control No" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="RiskCreationId" HeaderText="Task Id" ItemStyle-Width="150px" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Process Name">
                                <ItemTemplate>
                                    <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="SubProcess Name">
                                <ItemTemplate>
                                    <asp:Label ID="SubProcessName" runat="server" Text='<%# ShowSubProcessName((long)Eval("ProcessId"),(long)Eval("SubProcessId")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Risk Description">
                                <ItemTemplate>
                                    <asp:Label ID="lblActivityDescription" runat="server" Text='<%# Eval("ActivityDescription") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Control Objective">
                                <ItemTemplate>
                                    <asp:Label ID="lblControlObjective" runat="server" Text='<%# Eval("ControlObjective") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="RiskCategory">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# ShowRiskCategoryName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Location">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Assertions">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# ShowAssertionsName((long)Eval("RiskCreationId")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ControlDescription" HeaderText="Control Description" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="MControlDescription" HeaderText="MitigatingControl Description" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="PersonResponsible" HeaderText="Person Responsible" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="EffectiveDate" HeaderText="EffectiveDate" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="Key" HeaderText="Key" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="PrevationControl" HeaderText="Preventive Control" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="AutomatedControl" HeaderText="Automated Control" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="GapDescription" HeaderText="Gap Description" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="Recommendations" HeaderText="Recommendations" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ActionRemediationplan" HeaderText="Action Remediation Plan" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="IPE" HeaderText="Information Produced by Entity" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="ERPsystem" HeaderText="ERP System" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="FRC" HeaderText="Fraud Risk Control" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="UniqueReferred" HeaderText="Unique Referred" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="TestStrategy" HeaderText="Test Strategy" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="DocumentsExamined" HeaderText="Documents Examined" ItemStyle-Width="150px" />
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                    </asp:GridView>
                </div>
                <div style="margin-bottom: 7px; float: right; margin-right: 467px; margin-top: 10px; display:none; clear: both">
                    <asp:Button Text="Save" runat="server" ID="btnSave"  OnClick="btnSave_Click" CssClass="button" ValidationGroup="ComplianceInstanceValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignComplianceDialog').dialog('close');" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>


  
    <script type="text/javascript">
        $(function () {
            $("[id$=txtEffectiveDateEdit]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: 'http://jqueryui.com/demos/datepicker/images/calendar.gif'
            });
        });
     </script>
          

   
</asp:Content>
