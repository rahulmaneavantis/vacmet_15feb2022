﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditUploadARS : System.Web.UI.Page
    {
        public  List<int> VerticalIdS = new List<int>();
        public  List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLegalEntityData(ddlLegalEntity);
                BindFnancialYear(ddlFilterFinancial);                
                BindComplianceMatrix();
                bindPageNumber();
               // GetPageDisplaySummary();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceRoleMatrix.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindComplianceMatrix();
        }
        public void BindVerticalID(DropDownList DRP, int? BranchId)
        {
            if (BranchId != null)
            {
                DRP.DataTextField = "VerticalName";
                DRP.DataValueField = "VerticalsId";
                DRP.Items.Clear();
                DRP.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                DRP.DataBind();
                DRP.Items.Insert(0, new ListItem("Select Vertical", "-1"));               
            }
        }

       
        public void BindFnancialYear( DropDownList PopFDRP)
        {
            var details= UserManagementRisk.FillFnancialYear();
            if (details !=null)
            {
                
                PopFDRP.DataTextField = "Name";
                PopFDRP.DataValueField = "ID";
                PopFDRP.Items.Clear();
                PopFDRP.DataSource = details;
                PopFDRP.DataBind();
                PopFDRP.Items.Insert(0, new ListItem("Financial Year", "-1"));
            }
           
        }
        protected void checkBoxesProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Saplin.Controls.DropDownCheckBoxes DrpVerticals = (DropDownCheckBoxes)sender;
                VerticalIdS.Clear();
                for (int i = 0; i < DrpVerticals.Items.Count; i++)
                {
                    if (DrpVerticals.Items[i].Selected)
                    {
                        VerticalIdS.Add(Convert.ToInt32(DrpVerticals.Items[i].Value));
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
         
        }

        public void BindLegalEntityData(DropDownList PopFDRP)
        {
            int customerID = -1;
            //customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            PopFDRP.DataTextField = "Name";
            PopFDRP.DataValueField = "ID";
            PopFDRP.Items.Clear();
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, UserID);
            }
            //PopFDRP.DataSource = details;
            PopFDRP.DataBind();
            PopFDRP.Items.Insert(0, new ListItem("Unit", "-1"));

            //var details = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            //if (details != null)
            //{

            //    PopFDRP.DataTextField = "Name";
            //    PopFDRP.DataValueField = "ID";
            //    PopFDRP.Items.Clear();
            //    PopFDRP.DataSource = details;
            //    PopFDRP.DataBind();
            //    PopFDRP.Items.Insert(0, new ListItem("Unit", "-1"));
            //}
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {                
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindVerticalID(ddlVerticalID,Convert.ToInt32(ddlLegalEntity.SelectedValue));                
            }
            else
            {                
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {                
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindVerticalID(ddlVerticalID,Convert.ToInt32(ddlSubEntity1.SelectedValue));                
            }
            else
            {
                
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindVerticalID(ddlVerticalID,Convert.ToInt32(ddlSubEntity2.SelectedValue));
                
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
              
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindVerticalID(ddlVerticalID,Convert.ToInt32(ddlSubEntity3.SelectedValue));
                
            }
            else
            {                
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindVerticalID(ddlVerticalID,Convert.ToInt32(ddlFilterLocation.SelectedValue));
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }
      
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
      
        

        
      
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                //this.countrybindIndustory(ddlVertical, 0);
                //BindSubEntityData(ddlSubEntity1PopPup, 0);
                //BindSubEntityData(ddlSubEntity2PopPup, 0);
                //BindSubEntityData(ddlSubEntity3PopPup, 0);
                //BindSubEntityData(ddlFilterLocationPopPup, 0);                
                
                ViewState["Mode"] = 0;                  
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindComplianceMatrix()
        {
            try
            {
                string FinancialYear = string.Empty;
                long VerticalId = -1;
                int CustomerBranchId = -1;               
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        Session["BranchList"] = 0;
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                else
                {
                    List<long> branchs = new List<long>();
                    for (int i = 0; i < ddlLegalEntity.Items.Count; i++)
                    {
                        if (ddlLegalEntity.Items[i].Value != "-1")
                        {
                            if (ddlLegalEntity.Items[i].Value != "")
                            {
                                branchs.Add(Convert.ToInt64(ddlLegalEntity.Items[i].Value));
                                Session["BranchList"] = branchs;
                            }
                        }

                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }               
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalId = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalId = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedValue != "-1")
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                int customerID = -1;
                //customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                Branchlist.Clear();
                List<long> branch = new List<long>();
                branch = Session["BranchList"] as List<long>;
                if (branch != null)
                {
                    for (int i = 0; i < branch.Count; i++)
                    {
                        CustomerBranchId = Convert.ToInt32(branch[i]);
                        GetAllHierarchy(customerID, CustomerBranchId);
                    }
                }
                else
                {
                    var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                }
                var Branchlistloop = Branchlist.ToList();
                var AllComplianceRoleMatrix = GetAuditGridDisplay(customerID, Branchlist.ToList(), VerticalId, FinancialYear);
                grdComplianceRoleMatrix.DataSource = AllComplianceRoleMatrix;
                Session["TotalRows"] = AllComplianceRoleMatrix.Count;
                grdComplianceRoleMatrix.DataBind();

            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        public static List<AuditObservationClass> GetAuditGridDisplay(int Customerid, List<long> BranchID,long VerticalID,string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditObservationClass> riskassignmentexport = new List<AuditObservationClass>();
              

                riskassignmentexport = (from C in entities.ARSObservationUploadViews
                                        where C.CustomerID == Customerid                          
                          group C by new
                          {
                              C.CustomerID,
                              C.CustomerBranchID,
                              C.BranchName,
                              C.FinancialYear,                           
                              C.VerticalID,
                              C.VerticalName
                          } into GCS
                          select new AuditObservationClass()
                          {
                              CustomerID = GCS.Key.CustomerID,
                              CustomerBranchID = GCS.Key.CustomerBranchID,
                              BranchName = GCS.Key.BranchName,                        
                              FinancialYear = GCS.Key.FinancialYear,
                              VerticalID = GCS.Key.VerticalID,
                              VerticalName = GCS.Key.VerticalName,
                          }).ToList();

                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long) entry.CustomerBranchID)).ToList();
                }
                if (VerticalID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.VerticalID == VerticalID).ToList();
                }
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                return riskassignmentexport;
            }
        }
        public static List<ARSObservationUploadView> GetAuditGridVersionDisplay(long BranchID, long VerticalID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ARSObservationUploadView> riskassignmentexport = new List<ARSObservationUploadView>();
                riskassignmentexport = (from C in entities.ARSObservationUploadViews                                       
                                        select C).ToList();
                if (BranchID !=-1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry =>entry.CustomerBranchID == BranchID).ToList();
                }
                if (VerticalID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.VerticalID == VerticalID).ToList();
                }
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                return riskassignmentexport;
            }
        }
        public static List<ARSObservationUploadView> GetAuditGridVersionDisplay(List<long> BranchID, long VerticalID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ARSObservationUploadView> riskassignmentexport = new List<ARSObservationUploadView>();
                riskassignmentexport = (from C in entities.ARSObservationUploadViews
                                        select C).ToList();

                if (BranchID.Count > 0)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => BranchID.Contains((long) entry.CustomerBranchID)).ToList();
                }
                if (VerticalID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.VerticalID == VerticalID).ToList();
                }
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                return riskassignmentexport;
            }
        }
       
        public  List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public  void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
   

        public string ShowSampleConditionDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "View";
            }
            else
            {
                processnonprocess = "";
            }
            return processnonprocess;
        }


     
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }
        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {               
                BindComplianceMatrix();
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<AuditObservation> GetFileData1(int uploadid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.AuditObservations
                                where row.Id == uploadid
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblUploadID = (Label)gvr.FindControl("lblUploadID");
                string downloadfilename = string.Empty;
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<AuditObservation> fileData = GetFileData1(Convert.ToInt32(lblUploadID.Text));
                    int i = 1;
                    //string directoryName ="abc";
                    string directoryName;
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        downloadfilename = file.FinancialYear;
                        directoryName = file.CustomerBranchID +"/" + file.VerticalID +"/"+file.FinancialYear;
                        version =Convert.ToString(file.Version);
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');                            
                            string str = filename[0] + "." + filename[1];                            
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename="+ downloadfilename + ".zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void grdComplianceRoleMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    int Verticalid = Convert.ToInt32(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);                   
                    var instanvedetails = GetAuditGridVersionDisplay(CustomerBranchID, Verticalid, FinancialYear);
                    if (instanvedetails.Count > 0)
                    {
                        grdVersionDownload.DataSource = instanvedetails;
                        grdVersionDownload.DataBind();

                    }
                    else
                    {
                        grdVersionDownload.DataSource = null;
                        grdVersionDownload.DataBind();
                    }
                    upVersion.Update();                    
                }
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //Reload the Grid
                BindComplianceMatrix();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceRoleMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindComplianceMatrix();
        //    }
        //    catch (Exception ex)
        //    {
        //        com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindComplianceMatrix();
        //    }
        //    catch (Exception ex)
        //    {
        //        com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);          
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}        
    }
}