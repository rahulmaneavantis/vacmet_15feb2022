﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FailedAuditStatusTransactionPR.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.FailedAuditStatusTransactionPR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">    
    
    
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="../../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../NewCSS/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="../../NewCSS/fullcalendar.css" type="text/css" />
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="../../Newjs/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bs_leftnavi.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <%-- <script type="text/javascript" src="../../Newjs/jquery-combobox.js"></script> --%>
    <script type="text/javascript" src="../../Newjs/bs_leftnavi.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD50uFhROVysfZducuIgGl1ltBV3Hx-Ig8"></script>

    <script type="text/javascript">
          function initializeDatePickerforPerformer(date) {
             var startDate = new Date();
             $('#<%= tbxDate.ClientID %>').datepicker({
                 dateFormat: 'dd-mm-yy',
                 minDate: startDate,
                 numberOfMonths: 1,
                 changeMonth: true,
                 changeYear: true
             });

             if (date != null) {
                 $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
             }
         }
        function initializeDatePickerforPerformer1(date1) {
            var startDate1 = new Date();
            $('#<%= txtDateResp.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate1,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

              if (date1 != null) {
                  $("#<%= txtDateResp.ClientID %>").datepicker("option", "defaultDate", date1);
             }
        }
        <%--function initializeDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1
        });

        if (date != null) {
            $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
        }

        $('#<%= txtDateResp.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            // maxDate: startDate,
            numberOfMonths: 1
        });

        if (date != null) {
            $("#<%= txtDateResp.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }--%>

    function CloseWindow() {
        window.close();
    };

    function enableControls() {
        $("#<%= btnSave.ClientID %>").removeAttr("disabled");
        $("#<%= rdbtnStatus.ClientID %>").removeAttr("disabled");
        $("#<%= txtRemark.ClientID %>").removeAttr("disabled");
        $("#<%= tbxDate.ClientID %>").removeAttr("disabled");
    }
    function initializeJQueryUIReviewer() {
    }
    function initializeJQueryUI() {
    }

    </script>
    <style type="text/css">
        .tdAudit1 {
            width: 20%;
        }

        .tdAudit2 {
            width: 40%;
        }

        .tdAudit3 {
            width: 16%;
            vertical-align: top;
        }

        .tdAudit4 {
            width: 24%;
            vertical-align: top;
        }
    </style>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManage" runat="server"></asp:ScriptManager>
         <div class="clearfix"></div>
        <div class="col-md-12 colpadding0">
                <div runat="server" id="LblPageDetails" style="color: #666"></div>
            </div>
            <div class="clearfix"></div>
        <div id="divReviewerComplianceDetailsDialog">
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ReviewerComplianceValidationGroup" Display="none" />
                            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                        </div>
                        <div id="TOD" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails2">
                                                    <h2>TOD</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails2"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseActDetails2" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd;">

                                                <table style="width: 100%">
                                                    <tr>
                                                        <td class="tdAudit1">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Documents Examined</label>
                                                        </td>
                                                        <td class="tdAudit2">
                                                            <asp:TextBox ID="txtDocumentExamined" runat="server" TextMode="MultiLine" CssClass="form-control" Style="height: 50px; width: 200px;" Enabled="false"></asp:TextBox></td>
                                                        <td class="tdAudit3">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Reason for Pass or Fail</label>
                                                        </td>
                                                        <td class="tdAudit4">
                                                            <asp:TextBox ID="txtReasonforPassorFail" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 50px; width: 200px;" Enabled="false"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                TOD (Pass or Fail)</label>
                                                        </td>
                                                        <td class="tdAudit2">
                                                            <asp:DropDownList ID="ddlDesign" runat="server" AutoPostBack="true" CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlDesign_SelectedIndexChanged1"
                                                                Width="200px" Enabled="false">
                                                                <asp:ListItem Value="-1">Select TOD</asp:ListItem>
                                                                <asp:ListItem Value="1">Pass</asp:ListItem>
                                                                <asp:ListItem Value="2">Fail</asp:ListItem>
                                                                <asp:ListItem Value="3">Pass With Mitigating Control</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                        <td class="tdAudit3"></td>
                                                        <td class="tdAudit4">

                                                            <asp:LinkButton ID="lbDownloadSample" Style="width: 300px; font-size: 12px; color: #333;"
                                                                runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                        </td>
                                                    </tr>
                                                </table>

                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="TOE" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1">
                                                    <h2>TOE</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;" id="collapseActDetails1">

                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Sample Tested</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:TextBox ID="txtSampleTested" runat="server" CssClass="form-control" Style="width: 200px;" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    <td class="tdAudit3">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            Deviation %</label></td>
                                                    <td class="tdAudit4">
                                                        <asp:TextBox ID="txtDeviation" runat="server" CssClass="form-control" Style="width: 200px;" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit4" colspan="5" style="height: 5px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdAudit1">
                                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                            TOE (Pass or Fail)</label>
                                                    </td>
                                                    <td class="tdAudit2">
                                                        <asp:DropDownList ID="ddlOprationEffectiveness" runat="server" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlOprationEffectiveness_SelectedIndexChanged" Width="200px" CssClass="form-control m-bot15" Enabled="false">
                                                            <asp:ListItem Value="-1">Select TOE</asp:ListItem>
                                                            <asp:ListItem Value="1">Pass</asp:ListItem>
                                                            <asp:ListItem Value="2">Fail</asp:ListItem>
                                                            <asp:ListItem Value="3">Pass With Extended Testing</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="tdAudit3"></td>
                                                    <td class="tdAudit4"></td>
                                                </tr>

                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Div1" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Downloadcollopse">
                                                    <h2>Step 1 Download Review</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Downloadcollopse"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Downloadcollopse" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                <legend></legend>
                                                <table style="width: 100%; text-align: left;">
                                                    <thead>
                                                        <tr>
                                                            <td valign="top">

                                                                <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                    OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>Versions</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                    runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                                            <td>
                                                                                <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                                                    ID="btnComplinceVersionDoc" runat="server" Text="Download">
                                                                                </asp:LinkButton></td>

                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>
                                                            <td valign="top">
                                                                <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                                    OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>Compliance Related Documents</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton
                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                    OnClientClick='javascript:enableControls()'
                                                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                </asp:LinkButton></td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>

                                                            </td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Step2" class="row Dashboard-white-widget" style="display:none;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Step2collopse">
                                                    <h2>Step 2</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step2collopse"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px" id="Step2collopse">

                                            <div style="margin-bottom: 7px; margin-top: 10px;">
                                                <label id="lblStatus" runat="server" style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Status</label>
                                                <asp:RadioButtonList ID="rdbtnStatus" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                    OnSelectedIndexChanged="rdbtnStatus_SelectedIndexChanged">
                                                </asp:RadioButtonList>

                                                <asp:RequiredFieldValidator ID="rfvRdoStatusButton" runat="server" ValidationGroup="ReviewerComplianceValidationGroup" ControlToValidate="rdbtnStatus"
                                                    ErrorMessage="Please select Status."> </asp:RequiredFieldValidator>
                                            </div>

                                            <div style="margin-bottom: 7px" id="divDated" runat="server">
                                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Date</label>
                                                <asp:TextBox runat="server" ID="tbxDate" CssClass="form-control" Style="width: 200px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divpersonresponsible" class="row Dashboard-white-widget" runat="server">
                            <%----%>
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#testReviewcollopse">
                                                    <h2>Responsible Person</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#testReviewcollopse"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="testReviewcollopse" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td class="tdAudit1" style="vertical-align: top;">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Person Responsible</label></td>
                                                        <td class="tdAudit2">
                                                            <asp:DropDownList ID="ddlPersonResponsible" CssClass="form-control m-bot15" runat="server" AutoPostBack="true" Enabled="false"
                                                                OnSelectedIndexChanged="ddlPersonResponsible_SelectedIndexChanged" Width="297px">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Person Responsible." ControlToValidate="ddlPersonResponsible" ForeColor="Red"
                                                                runat="server" ID="RequiredFieldValidator3" ValidationGroup="ReviewerComplianceValidationGroup" />
                                                        </td>
                                                        <td class="tdAudit3">
                                                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Date</label>
                                                        </td>
                                                        <td class="tdAudit4">
                                                            <asp:TextBox runat="server" ID="txtDateResp" CssClass="form-control" Style="width: 200px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1" style="vertical-align: top;">
                                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Management Response</label></td>
                                                        <td class="tdAudit2">
                                                            <asp:TextBox runat="server" ID="txtMngntesponce" CssClass="form-control" TextMode="MultiLine" Style="height: 50px; width: 295px;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Review" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Reviewcollopse">
                                                    <h2>Review History</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Reviewcollopse"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Reviewcollopse" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">

                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="4">
                                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                                    PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="riskID" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Scheduleon" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Created By">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Remarks">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="lblDownLoadfile" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>' OnClick="DownLoadClick"></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerTemplate>
                                                                        <table style="display: none">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Records Found.
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAudit1">
                                                            <label style="width: 140px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Remark</label>
                                                        </td>
                                                        <td class="tdAudit2">
                                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Style="height: 50px; width: 180px;"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ErrorMessage="Remark Required" ControlToValidate="txtRemark" ForeColor="Red"
                                                                runat="server" ID="RequiredFieldValidator2" ValidationGroup="ComplianceValidationGroup" />
                                                        </td>
                                                        <td class="tdAudit3"></td>
                                                        <td class="tdAudit4">
                                                            <asp:FileUpload ID="FileuploadAdditionalFile" runat="server" /></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom: 7px; margin-left: 340px; margin-top: 10px; display:none">
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="ReviewerComplianceValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="ClosePage();" OnClick="btnCancel_Click" />
                        </div>
                        <div id="Div2" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#logcollopse">
                                                    <h2>Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#logcollopse"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="margin-bottom: 7px; clear: both; margin-top: 10px;" id="logcollopse">
                                            <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false"
                                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                                OnPageIndexChanging="grdTransactionHistory_PageIndexChanging"
                                                OnSorting="grdTransactionHistory_Sorting"
                                                DataKeyNames="AuditTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                <Columns>
                                                    <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                    <asp:TemplateField HeaderText="Date">
                                                        <ItemTemplate>
                                                            <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                        <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    </form>
</body>
</html>
