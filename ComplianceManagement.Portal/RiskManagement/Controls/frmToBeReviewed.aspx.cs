﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Controls
{
    public partial class frmToBeReviewed : System.Web.UI.Page
    {
        protected List<Int32> roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                BindLegalEntityData();
                GetPageDisplaySummary();
                BindFnancialYear();
            }
            roles = CustomerManagementRisk.GetAssignedRolesARS(Portal.Common.AuthenticationHelper.UserID);
        }

        public void BindGrid()
        {
            BindAuditTransactions();
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
        }
        
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            GetPageDisplaySummary();
        }
        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            GetPageDisplaySummary();
        }
        protected void ddlReportee_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            GetPageDisplaySummary();
        }
        protected void ddlKeyNonKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            GetPageDisplaySummary();
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            GetPageDisplaySummary();
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
            GetPageDisplaySummary();
        }
        protected string GetPerformer(int AuditStatusID, long? scheduledonid, long RiskCreationId)
        {
            try
            {
                string result = "";
                result = DashboardManagementRisk.GetUserName(AuditStatusID, scheduledonid, RiskCreationId, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagementRisk.GetAllNVP(customerID, ids: ids, Flags: true);
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
                ddlUserList.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public void BindReportee(int BranchId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlReportee.DataTextField = "Name";
            ddlReportee.DataValueField = "ID";
            ddlReportee.Items.Clear();
            ddlReportee.DataSource = UserManagementRisk.FillReportee(customerID, BranchId);
            ddlReportee.DataBind();
            ddlReportee.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem("All", "-1"));
        }
        
        protected void ddlDesign_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList btn = (DropDownList)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            DropDownList Design = (DropDownList)gvr.FindControl("ddlDesign");
            DropDownList OprationEffectiveness = (DropDownList)gvr.FindControl("ddlOprationEffectiveness");
            Label ProcessID = (Label)gvr.FindControl("lblProcessID");
            Label SubProcessID = (Label)gvr.FindControl("lblSubProcessId");
            if (!String.IsNullOrEmpty(Design.SelectedItem.Text))
            {
                if (Design.SelectedItem.Text == "Fail")
                {
                    OprationEffectiveness.Enabled = false;
                }
                else
                {
                    OprationEffectiveness.Enabled = true;
                }
            }
        }
        protected void ddlOprationEffectiveness_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        public static long GetKeyByKeyName(string KeyName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long catagoryIDs = (from row in entities.mst_Key
                                    where row.Name == KeyName
                                    select row.Id).SingleOrDefault();

                return catagoryIDs;
            }
        }

        public bool checkvisibility(string DownloadFileName)
        {
            bool testtcontrol = false;
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                testtcontrol = true;
            }
            else
            {
                testtcontrol = false;
            }
            return testtcontrol;
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        public string ShowTestingDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;


        }
        
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }

        public static List<FileData_Risk> GetFileData1(int riskcreationID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.FileData_Risk
                                where row.ID == riskcreationID
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblRiskCategoryCreationId = (Label)gvr.FindControl("lblRiskCategoryCreationId");
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<FileData_Risk> fileData = GetFileData1(Convert.ToInt32(lblRiskCategoryCreationId.Text));
                    int i = 0;
                    string directoryName = "abc";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }                            
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=Test.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void DownLoadTestingClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblRiskCategoryCreationId = (Label)gvr.FindControl("lblRiskCategoryCreationId");
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<FileData_Risk> fileData = GetFileData1(Convert.ToInt32(lblRiskCategoryCreationId.Text));
                    int i = 0;
                    string directoryName = "DownLoadTesting";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.TestFilePath), file.TestFileKey + Path.GetExtension(file.TestUploadName));
                        if (file.TestFilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.TestUploadName.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }                            
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=DownLoadTestingUpload.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int RiskCreationId = Convert.ToInt32(commandArgs[1]);
                string FinancialYear = Convert.ToString(commandArgs[2]);
                int CustomerBranchID = Convert.ToInt32(commandArgs[3]);
                string Period = Convert.ToString(commandArgs[4]);

                 ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowDialog('" + ScheduledOnID + "','" + RiskCreationId + "','" + FinancialYear + "','" + CustomerBranchID + "','" + Period + "');", true);

                // udcReviewerStatusTranscatopn.OpenTransactionPage(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period);

                BindAuditTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<AuditInstanceTransactionView> assignmentList = null;
              
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    //ViewState["SortOrder"] = "Asc";
                    //ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    //ViewState["SortOrder"] = "Desc";
                    //ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }
        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                    int RiskCreationId = Convert.ToInt32(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);
                    int CustomerBranchID = Convert.ToInt32(commandArgs[3]);
                    string Period = Convert.ToString(commandArgs[4]);
                   // udcReviewerStatusTranscatopn.OpenTransactionPage(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowDialog('" + ScheduledOnID + "','" + RiskCreationId + "','" + FinancialYear + "','" + CustomerBranchID + "','" + Period + "');", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetReviewer(int AuditStatusID, long? scheduledonid, long RiskCreationId)
        {
            try
            {
                string result = "";
                result = DashboardManagementRisk.GetUserName(AuditStatusID, scheduledonid, RiskCreationId, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }
        /// <summary>
        /// this function bind the upcoming compliances to the grid view.
        /// </summary>
        protected void BindAuditTransactions()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                List<AuditInstanceTransactionView> assignmentList = null;
                long pid = -1;
                long psid = -1;
                //long Branchid = -1;
                string finacialyear = "";
                string Period = "";
                string KeyNonKey = "";
                long AuditStatus = -1;
                int Reportee = -1;

                string Flag = "A";
                long CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        Flag = "B";
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        Flag = "B";
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        Flag = "B";
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        Flag = "B";
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        Flag = "B";
                    }
                }
                if (!string.IsNullOrEmpty(ddlQuarter.SelectedValue))
                {
                    if (ddlQuarter.SelectedValue != "-1")
                    {
                        Period = Convert.ToString(ddlQuarter.SelectedItem.Text);
                        Flag = "PE";
                    }
                }
                if (!string.IsNullOrEmpty(ddlKeyNonKey.SelectedValue))
                {
                    if (ddlKeyNonKey.SelectedValue != "-1")
                    {
                        KeyNonKey = Convert.ToString(ddlKeyNonKey.SelectedItem.Text);
                        Flag = "K";
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedItem.Text != "All")
                    {
                        finacialyear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                        Flag = "F";
                    }
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        //AuditStatus = Convert.ToInt32(ddlStatus.SelectedItem.Text);
                        AuditStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                        Flag = "S";
                    }
                }
                if (!string.IsNullOrEmpty(ddlReportee.SelectedValue))
                {
                    if (ddlReportee.SelectedValue != "-1")
                    {
                        Reportee = Convert.ToInt32(ddlReportee.SelectedItem.Text);
                        Flag = "R";
                    }
                }

                if (Flag == "A")
                {                    
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "All", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "B")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "Location", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "F")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "FinacialYear", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "R")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "Reportee", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "PE")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "Period", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "P")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "Process", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "SU")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "SubProcess", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "K")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "KeyNonKey", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                else if (Flag == "S")
                {
                    assignmentList = DashboardManagementRisk.DashboardDataForReviewer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, "Status", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID, Reportee);
                }
                grdComplianceTransactions.DataSource = assignmentList;
                Session["TotalRows"] = assignmentList.Count;
                grdComplianceTransactions.DataBind();
                GetPageDisplaySummary();
            }
                
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                //bool result = true;
                bool result = false;

                //if (userID == com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                //{
                if (roleID == 3)
                {
                    // result = statusID == 1 || statusID == 6 || statusID == 10;
                    result = statusID == 2 || statusID == 3;
                }
                else if (roleID == 4)
                {
                    result = statusID == 2 || statusID == 3;
                }
                else if (roleID == 5)
                {
                    result = statusID == 2 || statusID == 3;
                }
                else if (roleID == 6)
                {
                    result = statusID == 4 || statusID == 5 || statusID == 6;
                }
                //}

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        /// <summary>
        /// this function is set sorting images.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="headerRow"></param>
        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                BindAuditTransactions();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                BindAuditTransactions();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                BindAuditTransactions();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindReportee(Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindReportee(Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindReportee(Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                BindReportee(Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public int GetBranchID()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            return CustomerBranchId;
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    BindReportee(Convert.ToInt32(ddlSubEntity4.SelectedValue));
                }
            }
        }
    }
}