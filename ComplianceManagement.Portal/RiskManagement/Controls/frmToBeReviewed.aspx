﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="frmToBeReviewed.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Controls.frmToBeReviewed" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

      <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }


        function AddCustomerPopup() {
            $('#divDetailsDialog').modal('show');
            return true;
        }

        //This is used for Close Popup after save or update data.
        function CloseWin1() {
            $('#divDetailsDialog').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller1() {
            setInterval(CloseWin1, 3000);
        };

       

    </script>

    <style type="text/css">
        .td1 {
            width: 16%;
        }

        .td2 {
            width: 17%;
        }

        .td3 {
            width: 16%;
        }

        .td4 {
            width: 17%;
        }

        .td5 {
            width: 16%;
        }

        .td6 {
            width: 17%;
        }
    </style>

    <script type="text/javascript">
     function ShowDialog(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID,Period) {         
            $('#divDetailsDialog').modal('show');
                $('#showdetails').attr('width', '1000px');
                $('#showdetails').attr('height', '1100px');
                $('.modal-dialog').css('width', '1100px');
                $('#showdetails').attr('src', "../Controls/ReviewerStatus.aspx?ScheduledOnID=" + ScheduledOnID + "&RiskCreationId=" + RiskCreationId + "&FinancialYear=" + FinancialYear + "&CustomerBranchID=" + CustomerBranchID + "&Period=" + Period);
     }
       </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%-- <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                        </div>
                        <section class="panel">    
      <header class="panel-heading tab-bg-primary ">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <%if (roles.Contains(3))%>
                                <%{%>
                                    <li class="" id="liPerformer" runat="server">
                                        <asp:LinkButton ID="lnkPerformer" PostBackUrl="../../RiskManagement/AuditTool/TestingUI.aspx"    runat="server">Performer</asp:LinkButton>                                           
                                    </li>
                                <%}%>
                                <%if (roles.Contains(4))%>
                                <%{%>
                                    <li class="active"  id="liReviewer" runat="server">
                                        <asp:LinkButton ID="lnkReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                    </li>
                                <%}%>
                        </ul>
                    </header> 
    <div class="clearfix"></div>                
                   <div style="margin-bottom: 4px"/> 
                  <div style="float:left;width:100%">
                       <div style="float:left;width:13%">
                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;width:150px">
                    <div class="col-md-2 colpadding0" style="width:40px">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                         </div> 
                      <div style="float:left;width:87%">
                          <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>
                          <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                          <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                      DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                          <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                       DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>
                             
                          <div class="clearfix"></div>
                          <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                        DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>
                          <div class="col-md-3  colpadding0 entrycount"  style="margin-top: 5px;">
                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                               DataPlaceHolder="Financial Year" OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                    </div>
                          <div class="col-md-3 colpadding0 entrycount"  style="margin-top: 5px;">
                        <asp:DropDownListChosen runat="server" ID="ddlReportee" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                               DataPlaceHolder="Reportee" OnSelectedIndexChanged="ddlReportee_SelectedIndexChanged">                                
                            </asp:DropDownListChosen>
                    </div>
                          <div class="col-md-3 colpadding0 entrycount"  style="margin-top: 5px;">
                        <asp:DropDownListChosen runat="server" ID="ddlQuarter" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                              DataPlaceHolder="Period" OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Period</asp:ListItem>    
                                <asp:ListItem Value="-1"> All </asp:ListItem>
                                <asp:ListItem Value="1">Apr - Jun</asp:ListItem>
                                <asp:ListItem Value="2">Jul - Sep</asp:ListItem>
                                <asp:ListItem Value="3">Oct - Dec</asp:ListItem>
                                <asp:ListItem Value="4">Jan - Mar</asp:ListItem>
                            </asp:DropDownListChosen>
                    </div>
                          <div class="clearfix"></div>  
                          <div class="col-md-3 colpadding0 entrycount"  style="margin-top: 5px;">
                     <asp:DropDownListChosen runat="server" ID="ddlKeyNonKey" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                         DataPlaceHolder="Key/Non Key" OnSelectedIndexChanged="ddlKeyNonKey_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Key/Non Key</asp:ListItem>
                                <asp:ListItem Value="-1"> All </asp:ListItem>
                                <asp:ListItem Value="1">Key</asp:ListItem>
                                <asp:ListItem Value="2">Non Key</asp:ListItem>
                            </asp:DropDownListChosen>
                    </div>              
                          <div class="col-md-3 colpadding0 entrycount"  style="margin-top: 5px;">
                     <asp:DropDownListChosen runat="server" ID="ddlStatus" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                              DataPlaceHolder="Status" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Status</asp:ListItem>
                                <asp:ListItem Value="-1"> All </asp:ListItem>
                                <asp:ListItem Value="2">Submitted</asp:ListItem>
                                <asp:ListItem Value="4">Review Comment</asp:ListItem>
                            </asp:DropDownListChosen>
                    </div>
                        </div></div>

                            <div style="margin-bottom: 4px">
                            <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                OnSorting="grdComplianceTransactions_Sorting" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                                OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand" OnRowCreated="grdComplianceTransactions_RowCreated">
                                <Columns>
                                     <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                                  <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                      </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task ID"  Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location-wise Compliance"  >
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 230px;">
                                                <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Control Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                <asp:Label ID="lblControlDescription" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlDescription")%>' ToolTip='<%# Eval("ControlDescription")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Strategy">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 180px;">
                                                <asp:Label ID="lblTestStrategy" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TestStrategy")%>' ToolTip='<%# Eval("TestStrategy")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Key/Non Key">
                                        <ItemTemplate>                                           
                                                <asp:Label ID="lblKeyNonKey" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("KeyName")%>' ToolTip='<%# Eval("KeyName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Performer">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label ID="lblPerformer" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformer((int)Eval("AuditStatusID") ,(long?)Eval("ScheduledOnID"), (long)Eval("RiskCreationId")) %>'  tooltip='<%# GetPerformer((int)Eval("AuditStatusID") ,(long?)Eval("ScheduledOnID"), (long)Eval("RiskCreationId")) %>'></asp:Label>
                                       </div>
                                           </ItemTemplate>                                        
                                    </asp:TemplateField>                              
                                    <asp:TemplateField ItemStyle-Width="5%" HeaderText="Action">
                                        <ItemTemplate>                        <%--  OnClientClick="AddCustomerPopup()"    --%>             
                                            <asp:LinkButton ID="btnChangeStatus" runat="server"    Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("AuditStatusID")) %>'
                                                CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("RiskCreationId") + "," + Eval("FinancialYear") +"," + Eval("CustomerBranchID") +"," + Eval("ForMonth")  %>'><img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                        </ItemTemplate>                                        
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />            
                                <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                </PagerTemplate>
                                </asp:GridView>
                                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" OnClick="lBPrevious_Click" ImageUrl="~/img/paging-left-active.png" />                                  <%--OnClick ="lBPrevious_Click"--%>
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" OnClick="lBNext_Click" ImageUrl="~/img/paging-right-active.png"/>             <%-- OnClick ="lBNext_Click" --%>                      
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

  <%--<div class="modal fade" id="divReviewerComplianceDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content" style="width: 850px; background-color: #f7f7f7;">
                <div class="modal-header" style="background-color: #f7f7f7;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="background-color: #f7f7f7;">
                    <vit:AuditReviewerStatus runat="server" ID="udcReviewerStatusTranscatopn" />
                </div>
            </div>
        </div>
    </div>--%>
        <div class="modal fade" id="divDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color:#eee">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="background-color:#eee">
                    <iframe id="showdetails" src="about:blank" width="1150px" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
