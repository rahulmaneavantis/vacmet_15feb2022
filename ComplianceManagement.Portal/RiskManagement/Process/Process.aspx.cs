﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
//using AuditManagement.Business;
//using AuditManagement.Business.Data;
//using AuditManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Process
{
    public partial class Process : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindProcess();
                //GetPageDisplaySummary();
                btnAddProcess.Visible = true;
                bindPageNumber();
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcessList.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindProcess();
            //bindPageNumber();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindProcess()
        {
            try
            {
                long customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var ProcessList = ProcessManagement.GetAllProcess(customerID);

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Name")
                    {
                        ProcessList = ProcessList.OrderBy(entry => entry.Name).ToList();
                    }

                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Name")
                    {
                        ProcessList = ProcessList.OrderByDescending(entry => entry.Name).ToList();
                    }

                    direction = SortDirection.Ascending;
                }
                grdProcessList.DataSource = ProcessList;
                Session["TotalRows"] = ProcessList.Count;
                grdProcessList.DataBind();
                upProcessList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDepartment()
        {
            try
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = CompDeptManagement.FillDepartmentAudit(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowDepartmentName(int DeptId)
        {
            string DepartmentName = "";
            DepartmentName = CompDeptManagement.GetDepartmentName(DeptId);
            return DepartmentName;
        }

        public string ShowProcessNonProcess(long processId)
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
            string processnonprocess = "";
            var cc = ProcessManagement.GetByID(processId, customerID);
            if (cc.IsProcessNonProcess == "P")
            {
                processnonprocess = "Process";
            }
            else
            {
                processnonprocess = "Others";
            }


            return processnonprocess;
        }
        protected void grdProcessList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string processnonprocess = "";
                int ProcessID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_Process"))
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                    ViewState["Mode"] = 1;
                    ViewState["ProcessID"] = ProcessID;
                    string isprocess = "";

                    BindDepartment();

                    Mst_Process eventData = ProcessManagement.GetByID(ProcessID, customerID);

                    if (eventData != null)
                    {
                        tbxName.Text = eventData.Name;
                        tbxName.ToolTip = eventData.Name;
                        txtDescription.Text = eventData.Description;
                        ddlDepartment.SelectedValue = eventData.DepartmentID.ToString();

                        isprocess = eventData.IsProcessNonProcess;
                    }
                    if (isprocess == "P")
                    {
                        rdRiskProcess.SelectedIndex = 0;
                    }
                    else
                    {
                        rdRiskProcess.SelectedIndex = 1;
                    }
                    upProcess.Update();
                }
                else if (e.CommandName.Equals("DELETE_Process"))
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                    ProcessManagement.Delete(ProcessID, customerID);
                    BindProcess();
                }
                else if (e.CommandName.Equals("VIEW_Process"))
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int RowIndex = gvr.RowIndex;


                    processnonprocess = ((Label)grdProcessList.Rows[RowIndex].Cells[2].FindControl("lblApprover")).Text.Trim(); //If its label 
                    Session["processId"] = null;
                    Session["processId"] = ProcessID;
                    Session["processnonprocess"] = null;
                    Session["processnonprocess"] = processnonprocess;
                    Session["ParentID"] = null;
                    Response.Redirect("~/RiskManagement/Process/SubProcessList.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdProcessList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //grdProcessList.PageIndex = e.NewPageIndex;
                BindProcess();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdProcessList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var ProcessList = ProcessManagement.GetAllProcess(customerID);
                if (direction == SortDirection.Ascending)
                {
                    ProcessList = ProcessList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    ProcessList = ProcessList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdProcessList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdProcessList.Columns.IndexOf(field);
                    }
                }

                grdProcessList.DataSource = ProcessList;
                grdProcessList.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        protected void grdProcessList_RowDataBound(object sender, GridViewRowEventArgs e)
        {


        }

        protected void btnAddProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                txtDescription.Text = string.Empty;
                BindDepartment();
                upProcess.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divProcessDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdProcessList.PageIndex = 0;
                BindProcess();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string processnonprocess = "";
                if (rdRiskProcess.SelectedItem.Text == "Business Process")
                {
                    processnonprocess = "P";
                }
                else
                {
                    processnonprocess = "N";
                }
                Mst_Process ProcessData = new Mst_Process()
                {
                    Name = Regex.Replace(tbxName.Text.Trim(), @"\t|\n|\r", ""),
                    Description = txtDescription.Text,
                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                    UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    IsDeleted = false,
                    IsProcessNonProcess = processnonprocess,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    CustomerID = customerID
                };

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ProcessManagement.Exists(customerID, ProcessData.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Process with same name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        ProcessManagement.Create(ProcessData);
                        cvDuplicateEntry.ErrorMessage = "Process Save Successfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ProcessData.Id = Convert.ToInt32(ViewState["ProcessID"]);
                    long ProcessIDnew = ProcessManagement.GetProcessbyName(customerID, ProcessData.Name);
                    if (ProcessData.Id != ProcessIDnew)
                    {
                        if (ProcessIDnew == 0)
                        {
                            ProcessManagement.Update(ProcessData);
                            cvDuplicateEntry.ErrorMessage = "Process Updated Successfully.";
                            cvDuplicateEntry.IsValid = false;
                        }
                        else
                        {
                            cvDuplicateEntry.ErrorMessage = "Process with same name already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        ProcessManagement.Update(ProcessData);
                        cvDuplicateEntry.ErrorMessage = "Process Updated Successfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divProcessDialog\").dialog('close')", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                BindProcess();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upProcess_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void ddlFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindProcess();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rdRiskProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                grdProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdProcessList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindProcess();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcessList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdProcessList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        BindProcess();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdProcessList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindProcess();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                return true;
                //if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                //{
                //    SelectedPageNo.Text = "1";
                //    return false;
                //}
                //else if (!IsNumeric(SelectedPageNo.Text))
                //{
                //    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                //    return false;
                //}
                //else
                //{
                //    return true;
                //}
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}