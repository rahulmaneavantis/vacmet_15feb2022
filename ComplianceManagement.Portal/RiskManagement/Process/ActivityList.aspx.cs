﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Process
{
    public partial class ActivityList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["processId"] = null;
                    ViewState["subprocessId"] = null;
                    ViewState["processId"] = Session["processId"];
                    ViewState["subprocessId"] = Session["subprocessId"];                    
                    BindProcessSubType();                   
                    btnAddSubEvent.Visible = true;
                    bindPageNumber();

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {              
                mst_Activity subactivity = new mst_Activity()
                {
                    Name = Regex.Replace(tbxName.Text.Trim(), @"\t|\n|\r", ""),
                    ProcessId = Convert.ToInt32(ViewState["processId"]),  
                    SubProcessId = Convert.ToInt32(ViewState["subprocessId"]),
                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                    UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                   
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    subactivity.Id = Convert.ToInt32(ViewState["ActivityID"]);
                }

                if (ProcessManagement.ActivityExists(subactivity))
                {
                    cvDuplicateEntry.ErrorMessage = "Activity name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }


                if ((int) ViewState["Mode"] == 0)
                {
                    ProcessManagement.SubActivityCreate(subactivity);
                    cvDuplicateEntry.ErrorMessage = "Activity Add successfully.";
                    cvDuplicateEntry.IsValid = false;
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    ProcessManagement.SubActivityUpdate(subactivity);
                    cvDuplicateEntry.ErrorMessage = "Activity Update successfully.";
                    cvDuplicateEntry.IsValid = false;
                }

                BindProcessSubType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdActivityList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                upSubActivity.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void PopulateInputForm()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                if (ViewState["processId"] != null)
                {
                    var pdata = ProcessManagement.GetByID(Convert.ToInt32(ViewState["processId"]), customerID);
                    if (pdata != null)
                    {
                        litProcess.Text = pdata.Name;
                        var spdata = ProcessManagement.GetSubProcessByID(Convert.ToInt32(ViewState["subprocessId"]));
                        if (spdata != null)
                        {
                            litSubProcess.Text = spdata.Name;
                        }
                    }
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddSubEvent_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                PopulateInputForm();
                upSubActivity.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProcessSubType()
        {
            try
            {
                if (ViewState["subprocessId"] !=null)
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                    dlBreadcrumb.DataSource = ProcessManagement.GetHierarchyActivity(Convert.ToInt32(ViewState["subprocessId"]));
                    dlBreadcrumb.DataBind();
                    if (ViewState["processId"] !=null)
                    {
                        var ActivityListList = ProcessManagement.GetAll(Convert.ToInt32(ViewState["processId"]), Convert.ToInt32(ViewState["subprocessId"]), customerID);
                        grdActivityList.DataSource = ActivityListList;
                        Session["TotalRows"] = ActivityListList.Count;
                        grdActivityList.DataBind();
                    }                   
                }
               
                upSubActivityList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindProcessSubType();
                    upSubActivityList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdActivityList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int activityid = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_Activity"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ActivityID"] = null;
                    ViewState["ActivityID"] = activityid;
                    mst_Activity subEvent = ProcessManagement.GetSubActivityByID(activityid);
                    PopulateInputForm();
                    tbxName.Text = subEvent.Name;
                    tbxName.ToolTip = subEvent.Name;
                    upSubActivity.Update();
                }
                else if (e.CommandName.Equals("DELETE_Activity"))
                {
                    ProcessManagement.SubActivityDelete(activityid);
                    BindProcessSubType();
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lnkBackToProcess_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/RiskManagement/Process/SubProcessList.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdActivityList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdActivityList.PageIndex = e.NewPageIndex;
                BindProcessSubType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdActivityList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                //    {
                //        e.Row.Cells[4].Visible = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[4].Visible = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdActivityList.PageIndex = chkSelectedPage - 1;
            grdActivityList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindProcessSubType();
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdActivityList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindProcessSubType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdActivityList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }



        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}