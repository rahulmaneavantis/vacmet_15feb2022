﻿<%@ Page Title="Process" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="Process.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Process.Process" %>


<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
        <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
        .modal-body label {
    padding: 5px;
}
    </style> 

    <script type="text/javascript">
        function fopenpopup() {
            $('#Processes').modal('show');
            return true;
        }

        //This is used for Close Popup after save or update data.
        function CloseWin() {
            //$('#Processes').modal('hide');
            location.Reload();
        };

        

    </script>

    <script type="text/javascript">       
     
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        $(document).ready(function () {
            //setactivemenu('Business Process');
            fhead('Business Process');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upProcessList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>            
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width:23%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div style="text-align:right">
                     <asp:LinkButton Text="Add New" runat="server" ID="btnAddProcess" OnClientClick="fopenpopup()" CssClass="btn btn-primary" OnClick="btnAddProcess_Click" Visible="false" />
                </div>
             <div style="margin-bottom: 4px"> 
                   &nbsp;        
                <asp:GridView runat="server" ID="grdProcessList" AutoGenerateColumns="false"  OnRowDataBound="grdProcessList_RowDataBound"
                        AllowSorting="true"  OnSorting="grdProcessList_Sorting" PageSize="5" AllowPaging="true"
                        AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                        DataKeyNames="ID" OnRowCommand="grdProcessList_RowCommand" OnPageIndexChanging="grdProcessList_PageIndexChanging">
                    <Columns>
                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department"  >
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width: 200px">
                                    <asp:Label ID="lbldepartment" runat="server" Text='<%# ShowDepartmentName((int)Eval("DepartmentID")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status" ItemStyle-Width="20%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                   <asp:Label ID="lblApprover" runat="server" Text='<%# ShowProcessNonProcess((long)Eval("ID")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderText="Sub Process" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" class="newlink" Font-Underline="True" CommandName="VIEW_Process" CommandArgument='<%# Eval("ID") %>'>sub-process</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>                                          
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="7%">
                            <ItemTemplate>                                
                                <asp:LinkButton ID="lbtEdit" CausesValidation="false" OnClientClick="fopenpopup()" runat="server" CommandName="EDIT_Process" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Process" title="Edit Process" data-toggle="tooltip"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Process" Visible="false" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Process?');"><img src="../../Images/delete_icon_new.png" alt="Delete Process" title="Delete Process" data-toggle="tooltip"/></asp:LinkButton>                                
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>        
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                    <HeaderStyle BackColor="#ECF0F1" /> 
                        <PagerSettings Visible="false" />                  
                    <PagerTemplate>
                      
                    </PagerTemplate> 
                </asp:GridView>  
                    <div style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                        </asp:DropDownListChosen>  
                    </div>  
                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                           <%-- <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="Processes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog"  style="width: 55%; height: 75%; margin-top:100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseWin()" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divProcessDialog">
                        <asp:UpdatePanel ID="upProcess" runat="server" UpdateMode="Conditional" OnLoad="upProcess_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px; display:none;">
                                        <asp:RadioButtonList runat="server" ID="rdRiskProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 115px; padding-right:10px;"  ForeColor="GrayText"
                                            RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskProcess_SelectedIndexChanged">
                                            <asp:ListItem Text="Business Process" Value="Business Process" Selected="True" />
                                            <asp:ListItem Text="Others" Value="Others" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Name</label>
                                        <asp:TextBox runat="server" ID="tbxName" autocomplete="off" Style="width: 500px;" MaxLength="100" ToolTip="Name" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                                            runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Department</label>
                                        <asp:DropDownList runat="server" ID="ddlDepartment" class="form-control m-bot15" Style="width: 500px;" />
                                        <%--<asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please Select Department." ControlToValidate="ddlDepartment"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                                            Display="None" />--%>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Description</label>
                                        <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control" Style="height: 50px; width: 500px;" MaxLength="200" ToolTip="" TextMode="MultiLine" />
                                    </div>
                                    <div style="margin-bottom: 5px; margin-left: 150px; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                           OnClientClick="CloseWin()" ValidationGroup="EventValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
