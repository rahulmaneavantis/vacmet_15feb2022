﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class admindashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";

                BindDashboardCount();
            }
        }

        public void BindDashboardCount()
        {
            try
            {
                long CustID = -1;
                //customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                CustID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int Count = 0;

                    //User Count
                    Count = (from row in entities.mst_User
                             where row.CustomerID == CustID
                             && row.IsDeleted == false
                             && row.IsActive == true
                             select row.ID).Distinct().Count();

                    divUserCount.InnerText = Count.ToString();
                    Count = 0;

                    //Location Count
                    Count = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustID
                             && row.IsDeleted == false
                             && row.Status == 1
                             select row.ID).Distinct().Count();

                    divLocationCount.InnerText = Count.ToString();
                    Count = 0;

                    //Process Count
                    Count = (from row in entities.ProcessSubProcessViews
                             where row.CustomerID == CustID
                             && row.ProcessIsDeleted == false
                             select row.ProcessID).Distinct().Count();

                    divProcessCount.InnerText = Count.ToString();
                    Count = 0;

                    //SubProcess Count
                    Count = (from row in entities.ProcessSubProcessViews
                             where row.CustomerID == CustID
                             && row.SubProcessIsDeleted == false
                             select row.SubProcessID).Distinct().Count();

                    divSubProcessCount.InnerText = Count.ToString();
                    Count = 0;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
        
