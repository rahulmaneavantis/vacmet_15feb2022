﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class AuditDashboard : System.Web.UI.Page
    {
        protected List<Int32> roles;
        protected bool personresponsibleapplicable = false;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            roles = CustomerManagementRisk.GetAssignedRolesICFR(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);         
            personresponsibleapplicable = UserManagementRisk.PersonResponsibleExists(Portal.Common.AuthenticationHelper.UserID);           
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                HiddenField home = (HiddenField) Master.FindControl("Ishome");
                home.Value = "true";
                using (AuditControlEntities entities = new AuditControlEntities())
                {                                 
                    entities.Database.CommandTimeout = 300;
                    var dataImpliment = (from c in entities.FailedControlQuarterWiseDisplayViews
                                         where c.CustomerID == CustomerId
                                         select c).ToList();

                    if (roles.Contains(3))
                    {
                        //divPerformerOpenAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 1, customerid, 3).ToString();
                        //divPerformerCloseAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditCount(dataAuditSummary,Portal.Common.AuthenticationHelper.UserID, 3, customerid, 3).ToString();

                        divPerformerOpenAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditOpenAuditCount(Portal.Common.AuthenticationHelper.UserID,3).ToString();
                        divPerformerCloseAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditClosedAuditCount(Portal.Common.AuthenticationHelper.UserID,3).ToString();

                        divperformerFailedControlDUE.InnerText = DashboardManagementRisk.GetAuditCount(dataImpliment,Portal.Common.AuthenticationHelper.UserID, "D", CustomerId, 3).ToString();
                        divperformerFailedControlNotDUE.InnerText = DashboardManagementRisk.GetAuditCount(dataImpliment,Portal.Common.AuthenticationHelper.UserID, "ND", CustomerId, 3).ToString();
                    }
                    if (roles.Contains(4))
                    {
                        //divReviewerOpenAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditCount(dataAuditSummary,Portal.Common.AuthenticationHelper.UserID, 1, customerid, 4).ToString();
                        //divReviewerCloseAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditCount(dataAuditSummary,Portal.Common.AuthenticationHelper.UserID, 3, customerid, 4).ToString();

                        divReviewerOpenAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditOpenAuditCount(Portal.Common.AuthenticationHelper.UserID, 4).ToString();
                        divReviewerCloseAuditCount.InnerText = DashboardManagementRisk.GetInternalAuditClosedAuditCount(Portal.Common.AuthenticationHelper.UserID, 4).ToString();


                        divReviewerFailedControlDUE.InnerText = DashboardManagementRisk.GetAuditCount(dataImpliment,Portal.Common.AuthenticationHelper.UserID, "D", CustomerId, 4).ToString();
                        divReviewerFailedControlNOTDUE.InnerText = DashboardManagementRisk.GetAuditCount(dataImpliment,Portal.Common.AuthenticationHelper.UserID, "ND", CustomerId, 4).ToString();
                    }
                    if (personresponsibleapplicable)
                    {
                        divPersonresponsiblesummaryDUE.InnerText = DashboardManagementRisk.getDataCountFailds(dataImpliment,Portal.Common.AuthenticationHelper.UserID, CustomerId, "D").ToString();
                        divPersonresponsiblesummaryNOTDUE.InnerText = DashboardManagementRisk.getDataCountFailds(dataImpliment,Portal.Common.AuthenticationHelper.UserID, CustomerId, "ND").ToString();
                    }
                }
            }
        }

        public decimal GetPercentageAuditPersonResponsibleNotDue()
        {
            decimal returnvalue = 0;

            int AuditDue = Convert.ToInt32(divPersonresponsiblesummaryDUE.InnerText);

            int AuditNotDue = Convert.ToInt32(divPersonresponsiblesummaryNOTDUE.InnerText);

            int total = AuditDue + AuditNotDue;

            if (AuditNotDue > 0)
            {
                if (AuditDue > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditNotDue) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditPersonResponsibleDue()
        {
            decimal returnvalue = 0;

            int AuditDue = Convert.ToInt32(divPersonresponsiblesummaryDUE.InnerText);

            int AuditNotDue = Convert.ToInt32(divPersonresponsiblesummaryNOTDUE.InnerText);

            int total = AuditDue + AuditNotDue;

            if (AuditDue > 0)
            {
                if (AuditNotDue > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditDue) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetPercentageAuditReviewerNotDue()
        {
            decimal returnvalue = 0;

            int AuditDue = Convert.ToInt32(divReviewerFailedControlDUE.InnerText);

            int AuditNotDue = Convert.ToInt32(divReviewerFailedControlNOTDUE.InnerText);

            int total = AuditDue + AuditNotDue;

            if (AuditNotDue > 0)
            {
                if (AuditDue > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditNotDue) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditReviewerDue()
        {
            decimal returnvalue = 0;

            int AuditDue = Convert.ToInt32(divReviewerFailedControlDUE.InnerText);

            int AuditNotDue = Convert.ToInt32(divReviewerFailedControlNOTDUE.InnerText);

            int total = AuditDue + AuditNotDue;

            if (AuditDue > 0)
            {
                if (AuditNotDue > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditDue) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetPercentageAuditReviewerClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divReviewerOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divReviewerCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditReviewerOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divReviewerOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divReviewerCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetPercentageAuditNotDue()
        {
            decimal returnvalue = 0;

            int AuditDue = Convert.ToInt32(divperformerFailedControlDUE.InnerText);

            int AuditNotDue = Convert.ToInt32(divperformerFailedControlNotDUE.InnerText);

            int total = AuditDue + AuditNotDue;

            if (AuditNotDue > 0)
            {
                if (AuditDue > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditNotDue) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditDue()
        {
            decimal returnvalue = 0;

            int AuditDue = Convert.ToInt32(divperformerFailedControlDUE.InnerText);

            int AuditNotDue = Convert.ToInt32(divperformerFailedControlNotDUE.InnerText);

            int total = AuditDue + AuditNotDue;

            if (AuditDue > 0)
            {
                if (AuditNotDue > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditDue) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetPercentageAuditPerformerClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divPerformerOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divPerformerCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditPerformerOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divPerformerOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divPerformerCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

    }
}