﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class PersonResponsibleDashboard : System.Web.UI.Page
    {
        protected List<Int32> roles;
        protected bool checkPRFlag = false;
        public List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        protected bool prerqusite = false;
        protected static string IsHiddenFY;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    HiddenField home = (HiddenField)Master.FindControl("Ishome");
                    home.Value = "true";
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    BindFinancialYear();
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                        HiddenFY.Value = ddlFinancialYear.SelectedItem.Text;
                        IsHiddenFY = ddlFinancialYear.SelectedItem.Text;
                    }
                    BindCount();
                }
                catch (Exception ex)
                {

                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        public void BindCount()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<string> SubmitList = new List<string>();
                List<string> ReviewcommentList = new List<string>();
                List<string> CloaseList = new List<string>();
                SubmitList.Add("AS");
                ReviewcommentList.Add("PA");
                CloaseList.Add("AC");
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var branchid = CustomerManagementRisk.GetUsersApplicableBranch(Portal.Common.AuthenticationHelper.UserID);
                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(CustomerId, branchid);

                entities.Database.CommandTimeout = 300;
                var result = (from row in entities.SP_PersonResponsible_AuditStatusSummary(Portal.Common.AuthenticationHelper.UserID)
                                     select row).Distinct().ToList();

                
                entities.Database.CommandTimeout = 300;
                var masterRecordsIMP = (from row in entities.Sp_ImplementationAuditSummaryCountView(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID))
                                        where row.PersonResponsibleOLD == Portal.Common.AuthenticationHelper.UserID
                                        && row.RoleID == 3
                                        select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                //row.CustomerID == CustomerId 

                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        result = result.Where(e => e.FinancialYear == ddlFinancialYear.SelectedItem.Text).ToList();
                        masterRecordsIMP = masterRecordsIMP.Where(e => e.FinancialYear == ddlFinancialYear.SelectedItem.Text).ToList();
                    }
                }

                var masterRecords = result.GroupBy(a => new { a.ATBDID, a.AuditID }).Select(entity => entity.FirstOrDefault()).ToList();
                //Person Responsible Summary Count
                divOpenAuditCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCountOpenSubmit(masterRecords, 1, "RS").ToString();
                divSubmitAuditCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCountOpenSubmit(masterRecords, 1, "AS").ToString();//Audittee Submitted
                divReviewCommentCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCountOpenSubmit(masterRecords, 1, "RA").ToString();//Reviever sent back to audittee
                //divCloseAuditCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCountOpenSubmit(masterRecords, 3, "C").ToString();

                divUpcomingCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditOpenSubmitCountIMP(masterRecordsIMP, 1, null, "Due").ToString();//Upcoming
                divOverdueCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditOpenSubmitCountIMP(masterRecordsIMP, 1, null, "OverDue").ToString();//Overdue
                divSubmitAuditIMPCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditOpenSubmitCountIMP(masterRecordsIMP, 1, SubmitList, "Submit").ToString();//Audittee Submitted
                divReviewCommentIMPCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditOpenSubmitCountIMP(masterRecordsIMP, 1, ReviewcommentList, "Review").ToString();//Reviever sent back to audittee
                divCloseAuditIMPCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditOpenSubmitCountIMP(masterRecordsIMP, 3, CloaseList, "Closed").ToString();


                List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();
                entities.Database.CommandTimeout = 300;
                Masterrecord = (from row in entities.SP_PrequsiteCount(Portal.Common.AuthenticationHelper.UserID)
                                select row).ToList();

                if (Masterrecord.Count > 0)
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        Masterrecord = Masterrecord.Where(e => e.FinancialYear == ddlFinancialYear.SelectedItem.Text).ToList();
                    }
                    prerqusite = true;
                    var opencount = Masterrecord.Where(entry => entry.DocStatus == "Open" && entry.Flag == "AR").ToList().Count;
                    var submittedcount = Masterrecord.Where(entry => entry.DocStatus == "Submitted" || entry.DocStatus == "Approved" || entry.DocStatus == "Re-Submitted").ToList().Count;
                    var Rejectedcount = Masterrecord.Where(entry => entry.DocStatus == "Rejected").ToList().Count;
                    divPreRequisiteOpenAuditCount.InnerText = Convert.ToString(opencount);
                    divPreRequisiteSubmittedAuditCount.InnerText = Convert.ToString(submittedcount);
                    divPreRequisiteRejectedCount.InnerText = Convert.ToString(Rejectedcount);
                }
                else
                {
                    prerqusite = false;
                    divPreRequisiteOpenAuditCount.InnerText = "0";
                    divPreRequisiteSubmittedAuditCount.InnerText = "0";
                    divPreRequisiteRejectedCount.InnerText = "0";
                }

                roles = CustomerManagementRisk.ARSGetAssignedRolesBOTH(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                if (roles.Count > 0)
                {
                    entities.Database.CommandTimeout = 300;

                    var dataAuditSummary = (from c in entities.AuditCountViews
                                            select c).ToList();

                    var dataImplimentInternal = (from c in entities.ImplimentInternalAuditClosedCountViews
                                                 select c).ToList();

                    if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        if (ddlFinancialYear.SelectedValue != "-1")
                        {
                            dataAuditSummary = dataAuditSummary.Where(e => e.FinancialYear == ddlFinancialYear.SelectedItem.Text).ToList();
                            dataImplimentInternal = dataImplimentInternal.Where(e => e.FinancialYear == ddlFinancialYear.SelectedItem.Text).ToList();
                        }
                    }
                    //Performer/Reviewer Count
                    divAuditOpenProcess.InnerText = GetAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 1, CustomerId).ToString();
                    divAuditClosedProcess.InnerText = GetAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 3, CustomerId).ToString();

                    divAuditOpenImplementation.InnerText = GetAuditCountIMP(dataImplimentInternal, Portal.Common.AuthenticationHelper.UserID, 1, CustomerId).ToString();
                    divAuditClosedImplementation.InnerText = GetAuditCountIMP(dataImplimentInternal, Portal.Common.AuthenticationHelper.UserID, 3, CustomerId).ToString();
                }
                upObservationStatus.Update();
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public static int GetAuditCount(List<AuditCountView> AuditSummaryCountRecords, int userid, int statusid, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();

                    count = record.Count();

                }
                else if (statusid == 3)
                {
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              && C.ACCStatus == 1
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();
                    count = record.Count();
                }
                return count;
            }
        }
        public static int GetAuditCountIMP(List<ImplimentInternalAuditClosedCountView> ImplimentInternalAuditClosedCountView, int userid, int statusid, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;

                if (statusid == 1)
                {
                    int flag = 0;
                    var OpenImplimentCount = (from row in ImplimentInternalAuditClosedCountView
                                              where row.UserID == userid && row.CustomerID == customerID
                                              && row.AllClosed != flag
                                              select row).Distinct().ToList();
                    
                    count = OpenImplimentCount.Count();
                }
                else if (statusid == 3)
                {

                    int flag = 0;
                    var closedImplimentCount = (from row in ImplimentInternalAuditClosedCountView
                                                where row.UserID == userid && row.CustomerID == customerID
                                                && row.AllClosed == flag
                                                select row).Distinct().ToList();
                    count = closedImplimentCount.Count;
                }
                return count;
            }
        }
        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public decimal GetPercentagePerOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divAuditOpenProcess.InnerText);

            int AuditClose = Convert.ToInt32(divAuditClosedProcess.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentagePerImpOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divAuditOpenImplementation.InnerText);

            int AuditClose = Convert.ToInt32(divAuditClosedImplementation.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentagePerformerIMPClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divAuditOpenImplementation.InnerText);

            int AuditClose = Convert.ToInt32(divAuditClosedImplementation.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentagePerformerProcessClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divAuditOpenProcess.InnerText);

            int AuditClose = Convert.ToInt32(divAuditClosedProcess.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        //public decimal GetPercentageImpOpen()
        //{
        //    decimal returnvalue = 0;

        //    int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

        //    int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

        //    int total = AuditOpen + AuditClose;

        //    if (AuditOpen > 0)
        //    {
        //        if (AuditClose > 0)
        //        {
        //            returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
        //        }
        //        else
        //        {
        //            returnvalue = 100;
        //        }
        //    }
        //    return returnvalue;
        //}
        //public decimal GetPercentageImpClose()
        //{
        //    decimal returnvalue = 0;

        //    int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

        //    int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

        //    int total = AuditOpen + AuditClose;

        //    if (AuditClose > 0)
        //    {
        //        if (AuditOpen > 0)
        //        {
        //            returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
        //        }
        //        else
        //        {
        //            returnvalue = 100;
        //        }
        //    }
        //    return returnvalue;
        //}
        //public decimal GetPercentageProcessClose()
        //{
        //    decimal returnvalue = 0;

        //    int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

        //    int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

        //    int total = AuditOpen + AuditClose;

        //    if (AuditClose > 0)
        //    {
        //        if (AuditOpen > 0)
        //        {
        //            returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
        //        }
        //        else
        //        {
        //            returnvalue = 100;
        //        }
        //    }
        //    return returnvalue;
        //}
        //public decimal GetPercentageProcessOpen()
        //{
        //    decimal returnvalue = 0;

        //    int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

        //    int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

        //    int total = AuditOpen + AuditClose;

        //    if (AuditOpen > 0)
        //    {
        //        if (AuditClose > 0)
        //        {
        //            returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
        //        }
        //        else
        //        {
        //            returnvalue = 100;
        //        }
        //    }
        //    return returnvalue;
        //}

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    BindCount();
                    HiddenFY.Value = ddlFinancialYear.SelectedItem.Text;
                    IsHiddenFY = ddlFinancialYear.SelectedItem.Text;
                }
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    BindCount();
                }
            }
        }
    }
}