﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admindashboard.aspx.cs" MasterPageFile="~/AuditTool.Master" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.admindashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <script type="text/javascript">

         $(document).ready(function () {
             setactivemenu('leftdashboardmenu');
             fhead('My Dashboard');
         });

        </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 20px"></div>
    <section class="wrapper">
                <div class="row1" style="background:none;">
                    <div class="dashboard1">  

                        <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2></h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        
                         <div id="collapsePerformer" class="panel-collapse collapse in">
                            <div class="row ">
                     
                                 
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg">
                                        <div class="title">Users</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">             
                                                <a href="../../Users/AuditUsers_List">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divUserCount">10</div>
                                                </a>
                                            </div>
                                                                                                
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg">
                                        <div class="title">Locations</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">             
                                                <a href="../Customers/AuditCustomerBranch_List.aspx">                                                                                                                                                                                                                                                                                        
                                                    <div class="count" runat="server" id="divLocationCount">50</div>
                                                </a>
                                              
                                                 
                                            </div>
                                                                                                
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg">
                                        <div class="title">Process</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">             
                                                <a href="../../RiskManagement/Process/Process">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divProcessCount">10</div>
                                                </a>
                                              
                                                
                                            </div>
                                                                                                      
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div>
                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg">
                                        <div class="title">Sub Process</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">             
                                                <a href="../../RiskManagement/Process/Process">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divSubProcessCount">100</div>
                                                </a>
                                            </div>
                                                                                                      
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div>
                           </div>
                      </div>
                </div>
            </div>   
        
        
        
        
        
        
                 
    </section>
</asp:Content>
