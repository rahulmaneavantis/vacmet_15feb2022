﻿<%@ Page Title="Audit Dashboard" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.AuditDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });
    </script>
     <style type="text/css">
        .info-box .count {
            font-size: 40px !important;
        }

        .col-lg-3 {
            width: 22% !important;
        }

        .border-box {
            border: 1px solid #1fd9e1;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="wrapper">
                <div class="row Dashboard-white-widget">
                    <div class="dashboard">  
                        <% if (roles.Contains(3))%>
                        <% { %>  
                            <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2>Performer Summary</h2>
                                </div>
                            </div>   
                        </div>                        
                            <div class="row" style="margin-top:10px;">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg  border-box">
                                        <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">           
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Open">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divPerformerOpenAuditCount">0</div>
                                                </a>  
                                            </div>                                                                                                            
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg  border-box">
                                        <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">        
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Closed">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divPerformerCloseAuditCount">0</div>
                                                </a>
                                            </div>                                                                                                                                                                                                                
                                    </div>   
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Failed Controls</div> 
                                        <div class="col-md-6  borderright">  
                                               <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=D">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divperformerFailedControlDUE">0</div>
                                                </a>
                                            <div class="desc">Due</div>
                                        </div> 
                                        <div class="col-md-6">
                                            <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=ND">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divperformerFailedControlNotDUE">0</div>
                                                </a>
                                                <div class="desc">Not Due</div>
                                        </div> 
                                        <div class="clearfix"></div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    </div>   
                                </div>
                           </div>         
                        <% } %>
                        <div class="clearfix"></div> 
                         <% if (roles.Contains(4))%>
                        <% { %>
                            <div id="Reviewersummary" class="col-lg-12 col-md-12 colpadding0">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2>Reviewer Summary</h2>
                                </div>
                            </div>   
                            </div>

                            <div class="row" style="margin-top:10px;">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">           
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Open">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerOpenAuditCount">0</div>
                                                </a>    
                                            </div>                                                                                                            
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">        
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Closed">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerCloseAuditCount">0</div>                                                                                          
                                                </a>
                                            </div>                                                                                                                                                                                                                
                                    </div>   
                                </div>

                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Failed Controls</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-6  borderright">        
                                                <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=D">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerFailedControlDUE">0</div>
                                                </a>
                                                <div class="desc">Due</div>
                                            </div>    
                                        <div class="col-md-6">
                                            <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=ND">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerFailedControlNOTDUE">0</div>
                                                </a>
                                                <div class="desc">Not Due</div>
                                        </div>                                                                                                                                                                                                            
                                    </div>   
                                </div>
                           </div>        
                          <% } %>
                         <div class="clearfix"></div> 
                           <% if (personresponsibleapplicable)%>
                        <% { %> 
                           <div id="Personresponsiblesummary" class="col-lg-12 col-md-12 colpadding0">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2>Person Responsible Summary</h2>
                                </div>
                            </div>   
                            </div>
                            <div class="row" style="margin-top:10px;">
                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg border-box">
                                        <div class="title">Failed Controls</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-6  borderright">         
                                                <a href="../../RiskManagement/AuditTool/FailedControlTestingPR.aspx?Status=D">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divPersonresponsiblesummaryDUE">0</div>
                                                </a>
                                                 <div class="desc">Due</div>
                                            </div> 
                                        <div class="col-md-6">
                                            <a href="../../RiskManagement/AuditTool/FailedControlTestingPR.aspx?Status=ND">                                                                                                                                                                                                                                                                                         
                                            <div class="count" runat="server" id="divPersonresponsiblesummaryNOTDUE">0</div>
                                            </a>
                                            <div class="desc">Not Due</div>
                                        </div>                                                                                                                                                                                                               
                                    </div>   
                                </div>
                           </div>
                          <% } %>

                </div>
            </div>
    </section>
</asp:Content>
