﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="PersonResponsibleDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.PersonResponsibleDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });
    </script>
    <style type="text/css">
        .info-box .count {
            font-size: 40px !important;
        }

        .col-lg-3 {
            width: 22% !important;
        }

        .border-box {
            border: 1px solid #1fd9e1;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="wrapper">
        <div class="row Dashboard-white-widget">
            <div class="dashboard">               
                    <div class="panel panel-default">       
                         <div class="panel-body">
                        <div style="margin-top:23px;margin-bottom:34px;">
                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true"
                        class="form-control m-bot15 select_location" Width="15%" Height="32px" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"
                        AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Financial Year"> 
                        </asp:DropDownListChosen>    
                        </div>                           
 
        <asp:UpdatePanel ID="upObservationStatus" runat="server" UpdateMode="Conditional">       
        <ContentTemplate>
        <div class="row">
            <div class="dashboard">  
            <% if (checkPRFlag == true)%>
            <% { %>
                <div id="PersonResponsibleSummary" class="col-lg-12 col-md-12 colpadding0"  >
                    <div class="panel panel-default" style="background:none;">
                        <div class="panel-heading" style="background:none;">
                            <h2>Person Responsible Summary</h2>
                        </div>
                    </div>   
                </div>        
                <div class="panel-body" style="padding-left: 0px;">
                    <div class="row">  
                        <div class="col-lg-5" style="padding: 0px;"> 
                            <div style="border-radius:5px;font-size: 21px;padding-left:10px;margin-bottom:10px;margin-top:10px; background: #1fd9e1;color: #ffffff; text-align: center;margin-right:11px;">
                                Audit Observation Status      
                            </div>                
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                            <div class="info-box white-bg border-box" style="min-height: 115px !important;">
                                <div class="title">Open</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    <div class="col-md-12">             
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Process&Status=RS&FinYear=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                      
                                            <div class="count" runat="server" id="divOpenAuditCount">0</div>
                                        </a>
                                    </div>                                                                                                                                                                                                                                          
                            </div>   
                        </div> 

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                            <div class="info-box white-bg  border-box" style="min-height: 115px !important;">
                                <div class="title">Submitted</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    <div class="col-md-6 borderright">             
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Process&Status=AS&FinYear=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                      
                                            <div class="count" runat="server" id="divSubmitAuditCount">0</div>
                                        </a>
                                        <div class="desc">Submit</div>
                                    </div> 
                                    <div class="col-md-6">             
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Process&Status=RA&FinYear=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                      
                                            <div class="count" runat="server" id="divReviewCommentCount">0</div>
                                        </a>
                                        <div class="desc">Comments</div>
                                    </div>                                                                                                                                                                                                                 
                            </div>   
                        </div>
                        </div>
                        <div  class="col-lg-7" >    
                        <div style="border-radius:5px;font-size: 21px;padding-left:10px; margin-bottom:10px;margin-top:10px;background: #1fd9e1;color: #ffffff; text-align: center;margin-right:11px;">
                            Implementation Status</div>  
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                            <div class="info-box white-bg  border-box" style="min-height: 115px !important;">
                                <div class="title">Open</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    <div class="col-md-6 borderright">
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Implementation&Status=null&tag=Due&FinYear=<%=IsHiddenFY%>">  
                                            <div class="count" runat="server" id="divUpcomingCount">0</div>
                                        </a>
                                        <div class="desc">Upcoming</div>
                                    </div> 
                                    <div class="col-md-6">
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Implementation&Status=null&tag=OverDue&FinYear=<%=IsHiddenFY%>">  
                                            <div class="count" runat="server" id="divOverdueCount">0</div>
                                        </a>
                                        <div class="desc">Overdue</div>
                                    </div>                                                              
                                <div class="clearfix"></div>                                                                                                                                                                                                                        
                            </div>   
                        </div> 

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                            <div class="info-box white-bg  border-box" style="min-height: 115px !important;">
                                <div class="title">Submitted for Review</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    <div class="col-md-6 borderright">
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Implementation&Status=AS&FinYear=<%=IsHiddenFY%>">   
                                            <div class="count" runat="server" id="divSubmitAuditIMPCount">0</div>
                                        </a>
                                        <div class="desc">Submitted</div>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Implementation&Status=PA&FinYear=<%=IsHiddenFY%>">   
                                            <div class="count" runat="server" id="divReviewCommentIMPCount">0</div>
                                        </a>
                                        <div class="desc">Comments</div>
                                    </div>                                                                                                                                                                                                                                          
                                </div>   
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 colpadding0 fixwidth">
                            <div class="info-box white-bg  border-box" style="min-height: 115px !important;">
                            <div class="title">Implemented</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                            <div class="col-md-12">
                            <a href="../AuditTool/PersonResponsibleStatusUI.aspx?Type=Implementation&Status=AC&FinYear=<%=IsHiddenFY%>">                                                  
                            <div class="count" runat="server" id="divCloseAuditIMPCount">0</div>
                            </a>
                            </div>                                                                                                                                                                                                                       
                            </div>   
                        </div>
                    </div>       
                     </div>                                   
                 </div>    
                <div class="clearfix"></div>      
            <%}%> 
            <%if ((roles.Contains(3) || roles.Contains(4)))%>
            <% { %>
                    <div id="PerformerReviewerSummary" class="col-lg-12 col-md-12 colpadding0">
                        <div class="panel panel-default" style="background:none;">
                            <div class="panel-heading" style="background:none;">
                                <h2>Performer/Reviewer Summary</h2>
                            </div>
                        </div>   
                    </div>
                    
                    <div class="panel-body" style="padding-left: 0px; margin-top: 10px;">
                        <div class="row">
                            <div class="col-lg-5" style="padding: 0px;"> 
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                <div class="info-box white-bg  border-box">
                                    <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    <div class="col-md-6  borderright">             
                                        <a href="../AuditTool/AuditStatusUI.aspx?Status=Open&FY=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                         
                                            <div class="count" runat="server" id="divAuditOpenProcess">0</div>
                                        </a>
                                        <div class="desc">Audit</div>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="../AuditTool/AuditStatusUI_IMP_New.aspx?Status=Open&FY=<%=IsHiddenFY%>">   
                                        <div class="count" runat="server" id="divAuditOpenImplementation">0</div>
                                        </a>
                                        <div class="desc">Implementation</div>
                                    </div>                                                                  
                                    <div class="clearfix"></div>                                                                                                                                                                                                                        
                                </div>   
                            </div> 
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                <div class="info-box white-bg  border-box">
                                    <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    <div class="col-md-6  borderright">             
                                        <a href="../AuditTool/AuditStatusUI.aspx?Status=Closed&FY=<%=IsHiddenFY%>">                                                                                                                                                                                                                                                                                         
                                            <div class="count" runat="server" id="divAuditClosedProcess">0</div>
                                        </a>
                                        <div class="desc">Audit</div>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="../AuditTool/AuditStatusUI_IMP_New.aspx?Status=Closed&FY=<%=IsHiddenFY%>">   
                                            <div class="count" runat="server" id="divAuditClosedImplementation">0</div>
                                        </a>
                                        <div class="desc">Implementation</div>
                                    </div>                                                                  
                                    <div class="clearfix"></div>                                                                                                                                                                                                                        
                                </div>   
                            </div>
                                </div>
                        </div>
                            </div>
            <%}%> 
             <% if (prerqusite)%>
            <%{%>  
                   <div id="Prerequisitesummary" class="col-lg-12 col-md-12 colpadding0">
                                            <div class="panel panel-default" style="background: none;">
                                                <div class="panel-heading" style="background: none;">
                                                    <h2>Pre-Requisite Summary</h2>
                                                </div>
                                            </div>
                                        </div>

                    <div class="panel-body" style="padding-left: 0px; margin-top: 10px;">
                    <div class="row">
                                             <div class="col-lg-5" style="padding-left: 0px;"> 
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                    <div class="info-box white-bg border-box" style="min-height: 115px !important;">
                                                        <div class="title">Open</div>
                                                        <div class="col-md-12">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Open&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteOpenAuditCount">0</div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
 
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                     <div class="info-box white-bg border-box" style="min-height: 115px !important;">
                                                        <div class="title">Submitted</div>
                                                        <div class="col-md-12">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Submitted&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteSubmittedAuditCount">0</div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                             <div class="col-lg-5"  style="padding-left: 0px;margin-left: -14px;">
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                        <div class="info-box white-bg border-box" style="min-height: 115px !important;">
                                                        <div class="title">Rejected</div>
                                                        <div class="col-md-12">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Rejected&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteRejectedCount">0</div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>  
                                                 </div>                                              
                                            </div>
                    </div>
             <%}%>

                <div class="clearfix"></div>                
            </div>
            <asp:HiddenField runat="server" ID="HiddenFY" />
        </div> 
        </ContentTemplate> 
        </asp:UpdatePanel>
                        </div>                                   
                    </div>
            </div>     
        </div>
    </section>
</asp:Content>
