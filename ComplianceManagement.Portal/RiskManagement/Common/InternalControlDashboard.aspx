﻿<%@ Page Title="Internal Control Dashboard" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="InternalControlDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.InternalControlDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });
    </script>
    <style type="text/css">
        .border-box {
            border: 1px solid #1fd9e1;
        }
        .info-box .count {
            font-size: 40px !important;
        }

        .col-lg-3 {
            width: 22% !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="wrapper">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="panel panel-default">       
                         <div class="panel-body">
                            <div style="margin-top:23px;margin-bottom:34px;">
                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true"
                        class="form-control m-bot15 select_location" Width="15%" Height="32px" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"
                        AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Financial Year"> 
                        </asp:DropDownListChosen>    
                        </div> 
            <div class="clearfix"></div>
            <% if (roles.Contains(3) || roles.Contains(4) || roles.Contains(5))%>
            <% { %>
            <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                <div class="panel panel-default" style="background: none;">
                    <div class="panel-heading" style="background: none;">
                        <h2>Performer/Reviewer Summary</h2>
                    </div>
                </div>
            </div>
<div class="panel-body" style="padding-left: 0px; margin-top: 10px;">
            <div class="row ">
                  <div class="col-lg-5" style="padding: 0px;"> 
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                        <div class="info-box white-bg border-box">
                            <div class="title">Open Audits</div>
                            <div class="col-md-6  borderright">
                                <a href="../AuditTool/AuditStatusUI.aspx?Status=Open&FY=<%=IsHiddenFY%>">
                                    <div class="count" runat="server" id="divOpenAuditCount">0</div>
                                </a>
                                <div class="desc">Audit</div>
                            </div>
                            <div class="col-md-6">
                                <a href="../AuditTool/AuditStatusUI_IMP_New.aspx?Status=Open&FY=<%=IsHiddenFY%>">
                                    <div class="count" runat="server" id="divOpenAuditIMPCount">0</div>
                                </a>
                                <div class="desc">Implementation</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                        <div class="info-box white-bg border-box" style="min-height: 115px !important;">
                            <div class="title">Closed Audits</div>
                            <div class="col-md-6  borderright">
                                <a href="../AuditTool/AuditStatusUI.aspx?Status=Closed&FY=<%=IsHiddenFY%>">
                                    <div class="count" runat="server" id="divCloseAuditCount">0</div>
                                </a>
                                <div class="desc">Audit</div>
                            </div>
                            <div class="col-md-6">
                                <a href="../AuditTool/AuditStatusUI_IMP_New.aspx?Status=Closed&FY=<%=IsHiddenFY%>">
                                    <div class="count" runat="server" id="divCloseAuditIMPCount">0</div>
                                </a>
                                <div class="desc">Implementation</div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
    </div>
            <%}%>


            <% if (prerqusite)%>
            <%{%>
            <div id="Prerequisitesummary" class="col-lg-12 col-md-12 colpadding0">
                <div class="panel panel-default" style="background: none;">
                    <div class="panel-heading" style="background: none;">
                        <h2>Pre-Requisite Summary</h2>
                    </div>
                </div>
            </div>

                <div class="row">
                                             <div class="col-lg-5"  style="padding-left: 0px; margin-top: 10px;"> 
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                    <div class="info-box white-bg  border-box">
                                                        <div class="title">Open</div>
                                                        <div class="col-md-12">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Open&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteOpenAuditCount">0</div>
                                                            </a>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                    <div class="info-box white-bg border-box">
                                                        <div class="title">Submitted</div>
                                                        <div class="col-md-12">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Submitted&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteSubmittedAuditCount">0</div>
                                                            </a>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                </div>
                                             <div class="col-lg-5"  style="padding-left: 0px; margin-top: 10px;margin-left: -14px;">
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                    <div class="info-box white-bg  border-box">
                                                        <div class="title">Rejected</div>
                                                        <div class="col-md-12">
                                                            <a href="../AuditTool/AuditeePrerequsite.aspx?Status=Rejected&FY=<%=IsHiddenFY%>">
                                                                <div class="count" runat="server" id="divPreRequisiteRejectedCount">0</div>
                                                            </a>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>  
                                                 </div>                                              
                                            </div>
            <%}%> 
        </div>
    </div>
 </section>
</asp:Content>
