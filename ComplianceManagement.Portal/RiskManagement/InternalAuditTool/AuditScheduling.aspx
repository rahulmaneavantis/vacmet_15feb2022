﻿<%@ Page Title="Audit Scheduling" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditScheduling.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <style type="text/css">
        .textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#divAuditSchedulingDialog').dialog({
                height: 600,
                width: 1020,
                autoOpen: false,
                draggable: true,
                title: "Audit Scheduling Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceScheduleDialog').dialog({
                height: 600,
                width: 1000,
                autoOpen: false,
                draggable: true,
                title: "Audit Scheduling Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

    </script>


    <script>
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }
        function BindControls() {
            // JQUERY DATE PICKER.
            $(function () {
                $('input[id*=txtExpectedStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
            $(function () {
                $('input[id*=txtExpectedEndDate]').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Scheduling');
        });   

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>
            <div style="margin: 5px 5px 80px;">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                        ValidationGroup="ComplianceValidationGroup1" />
                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                </div>
                <table width="100%">
                    <tr>                     
                        <td colspan="2">
                            <div id="FilterLocationdiv" runat="server" style="margin-bottom: 7px; position: relative; display: inline-block;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                    *</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Select  Location</label>
                                <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 210px; position: absolute; z-index: 10" id="divFilterLocation">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                        Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Location."
                                        ControlToValidate="tbxFilterLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                </div>
                            </div>                          
                        </td>
                        <td>                         
                        </td>
                        <td class="newlink" align="right">
                            <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" OnClick="btnAddCompliance_Click" Visible="true" />
                        </td>
                    </tr>
                </table>
                <%-- DataKeyNames="ProcessId"--%>
                <asp:GridView runat="server" ID="grdAuditScheduling" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdAuditScheduling_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdAuditScheduling_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="25" Width="100%" OnSorting="grdAuditScheduling_Sorting"
                    Font-Size="12px" OnPageIndexChanging="grdAuditScheduling_PageIndexChanging" OnRowCommand="grdAuditScheduling_RowCommand1">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:TemplateField HeaderText="Branch Name" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" SortExpression="CustomerBranchId">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label runat="server" Text='<%# ShowCustomerBranchName((long)Eval("CustomerBranchId")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Financial Year" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" SortExpression="FinancialYear">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <%--<asp:Label runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'  ToolTip='<%# Eval("ProcessId") %>'></asp:Label>--%>
                                    <asp:Label runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Term Name" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("TermName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%--  <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ProcessId") + "," + Eval("ISAHQMP") +"," + Eval("CustomerBranchId") %>'><img src="../../Images/package_icon.png"  title="Display Schedule Information" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SCHEDULE" CommandArgument='<%# Eval("ProcessId") + "," + Eval("ISAHQMP") +"," + Eval("CustomerBranchId") %>' OnClientClick="return confirm('Are you certain you want to delete this Scheduling Details?');"><img src="../../Images/delete_icon.png" alt="Delete Scheduling Details" title="Delete Scheduling Details" /></asp:LinkButton>--%>

                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") %>'><img src="../../Images/package_icon.png"  title="Display Schedule Information" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SCHEDULE" ValidationGroup="ComplianceValidationGroup1" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") %>' OnClientClick="return confirm('Are you certain you want to delete this Scheduling Details?');"><img src="../../Images/delete_icon.png" alt="Delete Scheduling Details" title="Delete Scheduling Details" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divAuditSchedulingDialog" style="max-height: 950px;">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>                                    
                    <div id="Locationdiv" runat="server" style="margin-bottom: 7px; position: relative; display: inline-block;" >
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <asp:TextBox runat="server" ID="tbxLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 210px; position: absolute; z-index: 10" id="divLocation">
                            <asp:TreeView runat="server" ID="tvLocation" BackColor="White" BorderColor="Black"
                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvLocation_SelectedNodeChanged">
                            </asp:TreeView>
                             <asp:CompareValidator ID="CompareValidator14" ErrorMessage="Please Select Location."
                            ControlToValidate="tbxLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Financial Year</label>
                        <asp:DropDownList runat="server" ID="ddlFinancialYear" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Financial Year."
                            ControlToValidate="ddlFinancialYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Scheduling Type</label>
                        <asp:DropDownList runat="server" ID="ddlSchedulingType" AutoPostBack="true" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator16" ErrorMessage="Please Select Scheduling Type."
                            ControlToValidate="ddlSchedulingType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" runat="server" visible="false" id="Divnophase">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            No Of Phases</label>
                        <asp:TextBox runat="server" ID="txtNoOfPhases" Style="height: 16px; width: 250px;" AutoPostBack="true" OnTextChanged="txtNoOfPhases_TextChanged" />
                    </div>
                    <div style="margin-bottom: 7px">
                        Schedule Details               
                    </div>
                    <div style="margin-left: 20px; background: white; border: 1px solid gray; height: 363px; margin-bottom: 7px;">

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="a1">
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="pnlAnnually" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdAnnually" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdAnnually_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" ShowHeader="true" DataKeyNames="ID" OnPageIndexChanging="grdAnnually_PageIndexChanging">
                                        <%--OnRowCreated="grdAnnually_RowCreated"--%>
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                             <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                             </ItemTemplate>
                                             </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Annually" ItemStyle-Width="200px" SortExpression="Annualy1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAnnualy1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Annually" ItemStyle-Width="200px" SortExpression="Annualy2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAnnualy2" runat="server"  />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Annually" ItemStyle-Width="200px" SortExpression="Annualy3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAnnualy3" runat="server"  />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlHalfYearly" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdHalfYearly" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdHalfYearly_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdHalfYearly_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                       <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apr-Sep" ItemStyle-Width="200px" SortExpression="Halfyearly1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkHalfyearly1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct-Mar" ItemStyle-Width="200px" SortExpression="Halfyearly2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkHalfyearly2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apr-Sep" ItemStyle-Width="200px" SortExpression="Halfyearly3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkHalfyearly3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct-Mar" ItemStyle-Width="200px" SortExpression="Halfyearly4" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkHalfyearly4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apr-Sep" ItemStyle-Width="200px" SortExpression="Halfyearly5" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkHalfyearly5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct-Mar" ItemStyle-Width="200px" SortExpression="Halfyearly6" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkHalfyearly6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlQuarterly" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdQuarterly" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdQuarterly_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdQuarterly_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apr-Jun" ItemStyle-Width="200px" SortExpression="Quarter1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jul-Sep" ItemStyle-Width="200px" SortExpression="Quarter2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct-Dec" ItemStyle-Width="200px" SortExpression="Quarter3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jan-Mar" ItemStyle-Width="200px" SortExpression="Quarter4" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Apr-Jun" ItemStyle-Width="200px" SortExpression="Quarter5" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jul-Sep" ItemStyle-Width="200px" SortExpression="Quarter6" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct-Dec" ItemStyle-Width="200px" SortExpression="Quarter7" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter7" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jan-Mar" ItemStyle-Width="200px" SortExpression="Quarter8" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter8" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Apr-Jun" ItemStyle-Width="200px" SortExpression="Quarter9" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter9" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jul-Sep" ItemStyle-Width="200px" SortExpression="Quarter10" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter10" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct-Dec" ItemStyle-Width="200px" SortExpression="Quarter11" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter11" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jan-Mar" ItemStyle-Width="200px" SortExpression="Quarter12" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkQuarter12" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlMonthly" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdMonthly" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdMonthly_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdMonthly_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apr" ItemStyle-Width="200px" SortExpression="Monthly1">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="May" ItemStyle-Width="200px" SortExpression="Monthly2">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jun" ItemStyle-Width="200px" SortExpression="Monthly3">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jul" ItemStyle-Width="200px" SortExpression="Monthly4">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Aug" ItemStyle-Width="200px" SortExpression="Monthly5">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sep" ItemStyle-Width="200px" SortExpression="Monthly6">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct" ItemStyle-Width="200px" SortExpression="Monthly7">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly7" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nov" ItemStyle-Width="200px" SortExpression="Monthly8">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly8" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Dec" ItemStyle-Width="200px" SortExpression="Monthly9">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly9" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jan" ItemStyle-Width="200px" SortExpression="Monthly10">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly10" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Feb" ItemStyle-Width="200px" SortExpression="Monthly11">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly11" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mar" ItemStyle-Width="200px" SortExpression="Monthly12">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly12" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Apr" ItemStyle-Width="200px" SortExpression="Monthly13">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly13" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="May" ItemStyle-Width="200px" SortExpression="Monthly14">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly14" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jun" ItemStyle-Width="200px" SortExpression="Monthly15">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly15" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jul" ItemStyle-Width="200px" SortExpression="Monthly16">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly16" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Aug" ItemStyle-Width="200px" SortExpression="Monthly17">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly17" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sep" ItemStyle-Width="200px" SortExpression="Monthly18">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly18" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct" ItemStyle-Width="200px" SortExpression="Monthly19">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly19" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nov" ItemStyle-Width="200px" SortExpression="Monthly20">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly20" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Dec" ItemStyle-Width="200px" SortExpression="Monthly21">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly21" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jan" ItemStyle-Width="200px" SortExpression="Monthly22">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly22" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Feb" ItemStyle-Width="200px" SortExpression="Monthly23">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly23" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mar" ItemStyle-Width="200px" SortExpression="Monthly24">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly24" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apr" ItemStyle-Width="200px" SortExpression="Monthly25">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly25" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="May" ItemStyle-Width="200px" SortExpression="Monthly26">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly26" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jun" ItemStyle-Width="200px" SortExpression="Monthly27">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly27" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jul" ItemStyle-Width="200px" SortExpression="Monthly28">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly28" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Aug" ItemStyle-Width="200px" SortExpression="Monthly29">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly29" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sep" ItemStyle-Width="200px" SortExpression="Monthly30">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly30" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oct" ItemStyle-Width="200px" SortExpression="Monthly31">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly31" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nov" ItemStyle-Width="200px" SortExpression="Monthly32">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly32" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Dec" ItemStyle-Width="200px" SortExpression="Monthly33">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly33" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Jan" ItemStyle-Width="200px" SortExpression="Monthly34">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly34" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Feb" ItemStyle-Width="200px" SortExpression="Monthly35">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly35" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mar" ItemStyle-Width="200px" SortExpression="Monthly36">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkMonthly36" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlphase1" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdphase1" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdphase1_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlphase2" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdphase2" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdphase2_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase2_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase4" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase5" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase6" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlphase3" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdphase3" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdphase3_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase3_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase4" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase5" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase6" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase7" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase7" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase8" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase8" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase9" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase9" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlphase4" ScrollBars="Auto" Height="362px" Visible="false">
                                    <asp:GridView runat="server" ID="grdphase4" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdphase4_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase4_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase4" ItemStyle-Width="200px" SortExpression="Phase4" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase5" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase6" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase7" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase7" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase4" ItemStyle-Width="200px" SortExpression="Phase8" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase8" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase9" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase9" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase10" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase10" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase11" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase11" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase4" ItemStyle-Width="200px" SortExpression="Phase12" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase12" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlphase5" ScrollBars="Auto" Height="362px">
                                    <asp:GridView runat="server" ID="grdphase5" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%" OnRowDataBound="grdphase5_RowDataBound"
                                        Font-Size="12px" ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase5_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                        <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase1" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase2" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase2" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase3" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase4" ItemStyle-Width="200px" SortExpression="Phase4" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase4" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase5" ItemStyle-Width="200px" SortExpression="Phase5" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase5" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase6" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase7" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase7" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase8" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase8" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Phase4" ItemStyle-Width="200px" SortExpression="Phase9" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase9" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase5" ItemStyle-Width="200px" SortExpression="Phase10" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase10" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase1" ItemStyle-Width="200px" SortExpression="Phase11" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase11" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase2" ItemStyle-Width="200px" SortExpression="Phase12" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase12" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase3" ItemStyle-Width="200px" SortExpression="Phase13" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase13" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase4" ItemStyle-Width="200px" SortExpression="Phase14" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase14" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phase5" ItemStyle-Width="200px" SortExpression="Phase15" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPhase15" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                    </asp:GridView>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divAuditSchedulingDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divComplianceScheduleDialog">
        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="border: solid 1px red; background-color: #ffe8eb;"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" style="margin-bottom: 7px; padding-left: 50px">

                                <asp:GridView runat="server" ID="grdAuditScheduleStartEndDate" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="40" Width="100%" Font-Size="12px" ShowFooter="false"
                                    DataKeyNames="Id">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Process" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="ProcessName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                    <asp:Label runat="server" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Branch Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="BranchName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                    <asp:Label runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="For" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="BranchName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px">
                                                    <asp:Label runat="server" Text='<%# Eval("TermName") %>' ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Financial Year" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="BranchName">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px">
                                                    <asp:Label runat="server" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                

                                                <asp:TextBox ID="txtExpectedStartDate" runat="server"
                                                    Style="height: 16px; width: 100px;"  CssClass="textbox"
                                                    Text='<%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MM-yyyy"):""%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtExpectedEndDate" runat="server" Style="height: 16px; width: 100px;"
                                                    CssClass="textbox" Text='<%# Eval("EndDate")!= null?((DateTime)Eval("EndDate")).ToString("dd-MM-yyyy"):""%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                </asp:GridView>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 142px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click"
                            CssClass="button" />
                        <%-- <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click"
                            CssClass="button" />--%>
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>



    
</asp:Content>
