﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditMainUI_IMP_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditMainUI_IMP_New" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');

            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace / Implementation / ' + filterbytype);
                $('#pagetype').css('font-size', '20px');
            }
        });

        function ShowDialogHistory(ResultID, BID, FinYear, ForMonth, SID, VID, scheduledonid, AuditID, ProcessID, ATBDID, ActionCHK) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditStatusTransaction.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ResultID=" + ResultID + "&SID=" + SID + "&VID=" + VID + "&scheduledonid=" + scheduledonid + "&ActionCHK=" + ActionCHK + "&AuditID=" + AuditID + "&PID=" + ProcessID + "&ATBDID=" + ATBDID + "&tag=Implement");
        };

        function ShowReviewerDialogHistory(ResultID, BID, FinYear, ForMonth, SID, VID, scheduledonid, AuditID, ProcessID, ATBDID, ActionCHK) {

            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditReviewerStatus.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ResultID=" + ResultID + "&SID=" + SID + "&VID=" + VID + "&scheduledonid=" + scheduledonid + "&ActionCHK=" + ActionCHK + "&AuditID=" + AuditID + "&PID=" + ProcessID + "&ATBDID=" + ATBDID + "&tag=Implement");
        };

        function ShowDialog(ResultID, BID, FinYear, ForMonth, SID, VID, scheduledonid, AuditID, ActionCHK) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/IMPStatusAuditPerformer.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ResultID=" + ResultID + "&SID=" + SID + "&VID=" + VID + "&scheduledonid=" + scheduledonid + "&ActionCHK=" + ActionCHK + "&AuditID=" + AuditID);
        };

        function ShowReviewerDialog(ResultID, BID, FinYear, ForMonth, SID, VID, scheduledonid, AuditID, ActionCHK) {

            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/IMPStatusAuditReviewer.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ResultID=" + ResultID + "&SID=" + SID + "&VID=" + VID + "&scheduledonid=" + scheduledonid + "&ActionCHK=" + ActionCHK + "&AuditID=" + AuditID);
        };
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.
            var now = new Date();
            var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var diffDays = now.getDate() - firstDayPrevMonth.getDate();
            if (diffDays > 15) {
                var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth(), 1);
            }

            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    minDate: firstDayPrevMonth,
                });
            });

        }
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                                                           
                             <div class="panel-body">
                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="AuditValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="AuditValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                        <div class="col-md-2 colpadding0">
                            <p style="color: #999; margin-top: 5px;">Show </p>
                        </div>

                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                            <asp:ListItem Text="5" Selected="True" />
                            <asp:ListItem Text="10" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                     </div>  
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                        <asp:DropDownList runat="server" ID="ddlFilterStatus" AutoPostBack="true"
                        CssClass="form-control m-bot15" Visible="false">
                        <asp:ListItem Value="-1">Select Status</asp:ListItem>
                        <asp:ListItem Value="1">Open</asp:ListItem>
                        <asp:ListItem Value="2">Submited</asp:ListItem>
                        <asp:ListItem Value="4">Team Review</asp:ListItem>
                        <asp:ListItem Value="5">Final Review</asp:ListItem>
                        <asp:ListItem Value="3">Closed</asp:ListItem>
                        <asp:ListItem Value="6">Auditee Review</asp:ListItem> 
                        </asp:DropDownList>
                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;text-align: right;">
                        <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />    
                    </div>                                                                                              
                                </div>                             

                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                  
                                <div style="margin-bottom: 4px">
                                    &nbsp;&nbsp;
                    <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" AllowSorting="true"
                    OnRowDataBound="grdSummaryDetailsAuditCoverage_RowDataBound" ShowHeaderWhenEmpty="true"
                    CssClass="table" GridLines="None" AllowPaging="true" PageSize="5" Width="100%" DataKeyNames="ATBDID">                      
                    <Columns>
                        <%--<asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; width: 150px;">
                                                <asp:Label ID="lblPeriod" runat="server"
                                                     Text='<%# Eval("AuditName") %>' data-toggle="tooltip" 
                                                    data-placement="top" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> --%>   
                        <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                                <div class="text_NlinesusingCSS" style="width: 100px;">
                                    <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top"
                                    ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField HeaderText="SubProcess">
                            <ItemTemplate>
                                <div class="text_NlinesusingCSS" style="width: 130px;">
                                    <asp:Label ID="lblSubProcess" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top"
                                    ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                                                                                                                                 
                        <asp:TemplateField HeaderText="Observation">
                            <ItemTemplate>
                                <div class="text_NlinesusingCSS" style="width: 150px;">
                                    <asp:Label ID="lblObservation" runat="server" Text='<%# Eval("Observation") %>' 
                                        data-toggle="tooltip" data-placement="top"
                                    ToolTip='<%# Eval("Observation") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Old Management Response">
                            <ItemTemplate>
                                <div class="text_NlinesusingCSS" style="width: 170px;">
                                    <asp:Label ID="lblManagementResponse" runat="server" Text='<%# Eval("ManagementResponse") %>' data-placement="top" 
                                    data-toggle="tooltip" ToolTip='<%# Eval("ManagementResponse") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Old Timeline">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblTimeline" runat="server" 
                                        Text='<%# Eval("Timeline") != null ? Convert.ToDateTime(Eval("Timeline")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField HeaderText="Current Management Response">
                            <ItemTemplate>           
                                <div class="text_NlinesusingCSS" style="width: 170px;">                  
                                <asp:Label ID="tbxMgmResponce" runat="server"  Style="word-wrap:break-word;" data-placement="top" 
                                    data-toggle="tooltip" /> 
                                    </div>                         
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text="" Width="25px" Height="23px"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                                                                   
                        <asp:TemplateField ItemStyle-Width="7%" ItemStyle-Height="22px" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="bntAuditHistory" runat="server" OnClick="bntAuditHistory_Click" data-toggle="tooltip" data-placement="top" ToolTip="Audit History"
                                CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ResultID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth") + "," + Eval("AuditStatusID")+","+Eval("VerticalId")+","+Eval("ScheduledOnID")+","+Eval("AuditID")+","+Eval("ProcessID")+","+Eval("ATBDId") %>'>
                                <img src='<%# ResolveUrl("~/Images/Icon_viewA.png")%>' alt="Audit History"/></asp:LinkButton>
                                <asp:LinkButton ID="btnChangeStatus" runat="server" OnClick="btnChangeStatus_Click" data-toggle="tooltip" data-placement="top" ToolTip="Change Status"
                                CommandName="CHANGE_STATUS"  CommandArgument='<%# Eval("ResultID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth") + "," + Eval("AuditStatusID")+","+Eval("VerticalId")+","+Eval("ScheduledOnID")+","+Eval("AuditID") %>'>
                                <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>'/></asp:LinkButton>
                            </ItemTemplate> 
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>

                        <asp:TemplateField Visible="false">
                            <HeaderTemplate>
                                <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" onclick = "Check_Click(this)" />
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Person Responsible" Visible="false">
                            <ItemTemplate>                                                                  
                                <asp:DropDownList runat="server" ID="ddlPersonResponsible" class="form-control m-bot15" Style="width: 150px;">                                                                               
                                </asp:DropDownList>                                
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="TimeLine"  Visible="false">
                            <ItemTemplate>                                
                                <asp:TextBox ID="txtStartDate" class="form-control" style="width: 120px; text-align: center;" runat="server" Text=""></asp:TextBox>                                
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Status" Visible="false">
                            <ItemTemplate>                                                                 
                                <asp:DropDownList runat="server" ID="ddlIMPStatus" class="form-control m-bot15" Style="width: 200px;">    
                                <asp:ListItem Value="-1">Select Status</asp:ListItem>
                                <asp:ListItem Value="2">Due & Partial Implemented</asp:ListItem>
                                <asp:ListItem Value="3">Due But Not Implemented</asp:ListItem>
                                <asp:ListItem Value="4">Not Feasible</asp:ListItem>
                                <asp:ListItem Value="5">Due & Closed</asp:ListItem>                                                                           
                                </asp:DropDownList>                                
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField  Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblResultID" runat="server" Text='<%# Eval("ResultID") %>'></asp:Label>
                                <asp:Label ID="lblVerticalId" runat="server" Text='<%# Eval("VerticalId") %>'></asp:Label>
                                <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>'></asp:Label>
                                <asp:Label ID="lblCustomerBranchID" runat="server" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>            
                             
                       </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" /> 
                        <HeaderStyle BackColor="#ECF0F1" /> 
                        <PagerSettings Visible="false" />                   
                        <PagerTemplate>                     
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate> 
                    </asp:GridView>
                    <div style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                        </asp:DropDownListChosen>  
                    </div>
                 </div>
                                <div class="col-md-12 colpadding0">

                                    <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                                    <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                                </div> 
                                <div>
                                 <asp:LinkButton runat="server" ID="lbkSave" Text="Save" CssClass="btn btn-primary" style="float: left; display:none;" OnClick="lbkSave_Click"/>
                                     <asp:DropDownList runat="server" ID="DropDownListPersonResponsible" AutoPostBack="true"
                                        CssClass="form-control m-bot15" Visible="false"  Style="width: 160px;margin-top: -33px;margin-left: 69px; display:none" >                                                                           
                                     </asp:DropDownList>
                             </div> 
                                 <div class="clearfix"></div>
                                            <div class="clearfix"></div>
                                 <div class="col-md-12 colpadding0">
                                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 40%">
                                                </div>
                                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">
                                                </div>
                                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                </div>
                                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%; float:right">
                                                    <div style="background-color: orange; border: 1px solid #4a8407; width: 50px; height: 20px; float: left" data-toggle="tooltip" data-placement="bottom" title="Resubmision of rejected "></div>
                                                    <div style="background-color: red; border: 1px solid #ba8b00; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="Rejected"></div>                                                   
                                                </div>
                                            </div>
                            </div>               
                       </section>
                    </div>

                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divWorkToBeDoneDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional">
                        <%--OnLoad="upComplianceDetails_Load"--%>
                        <ContentTemplate>
                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="true"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                </div>
                            </div>

                            <table style="width: 100%; margin-top: 5px;">
                                <tr>
                                    <td colspan="4" style="width: 100%;" runat="server" id="TDTab">

                                        <header> <%--class="panel-heading tab-bg-primary"--%>

                            <ul id="rblRole1" class="nav nav-tabs">        
                                <li class="active" id="liAuditCoverage" runat="server">
                                    <asp:LinkButton ID="lnkAuditCoverage" OnClick="Tab1_Click" runat="server">Audit Coverage</asp:LinkButton>                                           
                                </li> 
                                                                                    
                                <li class="" id="liActualTesting" runat="server">
                                    <asp:LinkButton ID="lnkActualTesting" OnClick="Tab2_Click" runat="server">Actual Testing/ Work Done</asp:LinkButton>                                        
                                </li>   
                                          
                                <li class="" id="liObservation" runat="server">
                                    <asp:LinkButton ID="lnkObservation" OnClick="Tab3_Click" runat="server">Observations</asp:LinkButton>                                        
                                </li> 
                                          
                                <li class="" id="liReviewLog" runat="server">
                                    <asp:LinkButton ID="lnkReviewLog" OnClick="Tab4_Click" runat="server">Review History & Log</asp:LinkButton>                                        
                                </li>                                           
                            </ul>
                        </header>



                                        <asp:MultiView ID="MainView" runat="server">
                                            <asp:View ID="FirstView" runat="server">
                                                <div style="width: 100%; float: left; margin-bottom: 15px">

                                                    <table style="width: 100%; margin-top: 0px;">
                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333; margin-bottom: 10px;">
                                                                    Audit Objective</label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtAuditObjective" TextMode="MultiLine" Style="height: 100px; width: 688px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                <label style="width: 185px; display: block; float: left; font-size: 13px; color: #333; margin-bottom: 10px;">
                                                                    Audit Steps</label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtAuditSteps" TextMode="MultiLine" Style="height: 100px; width: 688px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Analyis To Be Performed</label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtAnalysisToBePerformed" TextMode="MultiLine" Style="height: 100px; width: 688px;" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="float: right">
                                                                <asp:Button Text="Save" runat="server" ID="btnNext1" CssClass="button"
                                                                    ValidationGroup="FirstValidationGroup" />
                                                                <%--OnClick=""btnNext1_Click--%>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="SecondView" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                            <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Process  Walkthrough 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtWalkthrough" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                            <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Actual Work Done 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtActualWorkDone" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                            <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Population 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtpopulation" Style="height: 22px; width: 688px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                            <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Sample 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtSample" Style="height: 22px; width: 688px;" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <label style="width: 10px; display: block; float: left; font-size: 15px; color: red; margin-bottom: 10px;"></label>
                                                            <label style="width: 150px; display: block; float: left; font-size: 15px; color: #333;">
                                                                Remarks</label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="width: 10px; display: block; float: left; font-size: 15px; color: red;">&nbsp;</label>
                                                            <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                Working File Upload</label>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                <div>
                                                                    <asp:FileUpload ID="fuSampleFile" runat="server" />
                                                                </div>
                                                                <div>
                                                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button" />
                                                                    <%--OnClick="btnUpload_Click"--%>
                                                                </div>

                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <div runat="server" id="divDeleteDocument" style="margin-left: 175px;">
                                                                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td style="width: 50%">
                                                                                <asp:UpdatePanel runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:GridView runat="server" ID="rptComplianceDocumnets" AutoGenerateColumns="false" AllowSorting="true"
                                                                                            AllowPaging="true" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                                                                                            BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%">
                                                                                            <%--OnRowCommand="rptComplianceDocumnets_RowCommand" OnRowDataBound="rptComplianceDocumnets_RowDataBound"--%>
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="Document" ItemStyle-HorizontalAlign="Center">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                                            <ContentTemplate>
                                                                                                                <asp:LinkButton
                                                                                                                    CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                                </asp:LinkButton>
                                                                                                            </ContentTemplate>
                                                                                                            <Triggers>
                                                                                                                <asp:PostBackTrigger ControlID="btnComplianceDocumnets" />
                                                                                                            </Triggers>
                                                                                                        </asp:UpdatePanel>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                            </Columns>
                                                                                            <FooterStyle BackColor="#CCCC99" />
                                                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                                            <PagerSettings Position="Top" />
                                                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                            <AlternatingRowStyle BackColor="#e1e1e1" />
                                                                                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                                                            <EmptyDataTemplate>
                                                                                                No Records Found.
                                                                                            </EmptyDataTemplate>
                                                                                        </asp:GridView>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <asp:Button Text="Save" runat="server" ID="btnNext2" CssClass="button"
                                                                ValidationGroup="FirstValidationGroup" />
                                                            <%--OnClick="btnNext2_Click"--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:View>
                                            <asp:View ID="ThirdView" runat="server">
                                                <div style="width: 100%; float: left; margin-bottom: 15px">

                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Observation Number
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtObservationNumber" TextMode="MultiLine" Style="height: 22px; width: 688px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Observation Title
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtObservationTitle" TextMode="MultiLine" Style="height: 22px; width: 688px;" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Observation 
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtObservation" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Observation can not be empty." ControlToValidate="txtObservation"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Risk 
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtRisk" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Risk can not be Empty." ControlToValidate="txtRisk"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Root Cost 
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtRootcost" Style="height: 22px; width: 688px;" />
                                                                <asp:CompareValidator ID="cvRootCost" runat="server" ControlToValidate="txtRootcost" ErrorMessage="Only Numbers in Root Cost"
                                                                    ValidationGroup="ThirdValidationGroup" Display="None" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Financial Impact 
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtfinancialImpact" Style="height: 22px; width: 688px;" />
                                                                <asp:CompareValidator ID="cvFImpact" runat="server" ControlToValidate="txtfinancialImpact" ErrorMessage="Only Numbers in Financial Imapct"
                                                                    ValidationGroup="ThirdValidationGroup" Display="None" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Recommendation
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtRecommendation" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Recommendation can not be empty."
                                                                    ControlToValidate="txtRecommendation"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Management Response 
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtMgtResponse" TextMode="MultiLine" Style="height: 50px; width: 688px;" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Time Line
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtTimeLine" Style="height: 20px; width: 115px; text-align: center;" DataFormatString="{dd-MM-yyyy}" />

                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Person Responsible
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlPersonresponsible" runat="server" Style="height: 24px; width: 254px;"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RFVPerRes" ErrorMessage="Select Person Responsible."
                                                                    ControlToValidate="ddlPersonresponsible" InitialValue="-1"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;"></label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Observation Rating
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlobservationRating" runat="server" Style="height: 24px; width: 254px;">
                                                                    <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                                                    <asp:ListItem Value="1">High</asp:ListItem>
                                                                    <asp:ListItem Value="2">Medium</asp:ListItem>
                                                                    <asp:ListItem Value="3">Low</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RFVObRating" ErrorMessage="Select Observation Rating."
                                                                    ControlToValidate="ddlobservationRating" InitialValue="-1"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;"></label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Observation Category
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlObservationCategory" runat="server" Style="height: 24px; width: 254px;"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RFVObCat" ErrorMessage="Select Observation Category."
                                                                    ControlToValidate="ddlObservationCategory" InitialValue="-1"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Observation SubCategory
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlObservationSubCategory" runat="server" Style="height: 24px; width: 254px;"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RFVObSubCat" ErrorMessage="Select Observation SubCategory."
                                                                    ControlToValidate="ddlObservationSubCategory" InitialValue="-1"
                                                                    runat="server" ValidationGroup="ThirdValidationGroup" Display="None" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <label style="width: 10px; display: block; float: left; font-size: 15px; color: red;">*</label>
                                                                <label style="width: 100px; display: block; float: left; font-size: 15px; color: #333;">
                                                                    Date</label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="tbxDate" Style="height: 20px; width: 115px; text-align: center;" Enabled="false" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" align="center" style="margin-top: 50px;">
                                                                <asp:Button Text="Save" runat="server" ID="btnSubmit" CssClass="button"
                                                                    ValidationGroup="ThirdValidationGroup" />
                                                                <%--OnClick="btnNext3_Click"--%>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="FourthView" runat="server">
                                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Panel ID="divReviewHistory" runat="server" Style="width: 100%">
                                                                    <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                                                        <legend>Review History</legend>
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                                                        AllowPaging="true" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                                                                                        BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black" Font-Size="12px"
                                                                                        Width="100%" PageSize="50">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="ID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="riskID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("InternalAuditInstance") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Scheduleon" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                                        <ContentTemplate>
                                                                                                            <asp:LinkButton ID="lblDownLoadfile" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>'></asp:LinkButton>
                                                                                                            <%--OnClick="DownLoadClick"--%>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                                        <PagerSettings Position="Top" />
                                                                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                        <AlternatingRowStyle BackColor="#e1e1e1" />
                                                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                                                        <EmptyDataTemplate>
                                                                                            No Records Found.
                                                                                        </EmptyDataTemplate>
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td class="tdAudit1">
                                                                                    <label style="width: 225px; display: block; float: left; font-size: 15px; color: #333;">
                                                                                        Review Remark</label>
                                                                                </td>
                                                                                <td class="tdAudit2">
                                                                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="height: 50px; width: 488px;"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ErrorMessage="Remark Required" ControlToValidate="txtRemark" ForeColor="Red"
                                                                                        runat="server" ID="RequiredFieldValidator9" ValidationGroup="oplValidationGroup" />
                                                                                </td>
                                                                                <td class="tdAudit3"></td>
                                                                                <td class="tdAudit4">
                                                                                    <asp:FileUpload ID="FileuploadAdditionalFile" runat="server" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" style="text-align: center;">
                                                                                    <asp:Button Text="Submit" runat="server" ID="btnSave" CssClass="button"
                                                                                        ValidationGroup="oplValidationGroup" />
                                                                                    <%--OnClick="btnSave_Click"--%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional">
                                                                    <%--OnLoad="upComplianceDetails_Load"--%>
                                                                    <ContentTemplate>
                                                                        <div style="margin: 5px; width: 100%;">
                                                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                                                                    <legend style="font-weight: bold">Audit Log</legend>
                                                                                    <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                                                        AllowPaging="true" PageSize="50" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                                                                                        BorderStyle="Solid"
                                                                                        BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px"
                                                                                        DataKeyNames="AuditTransactionID">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                                        <PagerSettings Position="Top" />
                                                                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                        <AlternatingRowStyle BackColor="#e1e1e1" />
                                                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                                                        <EmptyDataTemplate>
                                                                                            No Records Found.
                                                                                        </EmptyDataTemplate>
                                                                                    </asp:GridView>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:View>
                                        </asp:MultiView>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="about:blank" width="1100px" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

