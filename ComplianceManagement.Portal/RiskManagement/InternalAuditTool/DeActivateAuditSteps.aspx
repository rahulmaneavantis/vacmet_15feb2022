﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="DeActivateAuditSteps.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.DeActivateAuditSteps" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .chkBoxDropDownSelectedBox {
            border: 1px solid #c7c7cc !important;
            border-radius: 5px;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
            color: #999;
        }

        .chkBoxDropDown {
            border: 1px solid #c7c7cc !important;
            border-radius: 5px;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
            color: #999;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            fhead('Active/De-Active Audit Steps');
        });

        function checkAll(chkHeader) {
            
            var selectedRowCount = 0;
            var grdAuditStepList = document.getElementById("<%=grdAuditStepList.ClientID %>");
            if (grdAuditStepList != null) {
                if (chkHeader != null) {
                    if (chkHeader.checked) {
                        for (i = 1; i < grdAuditStepList.rows.length; i++) {
                            if (grdAuditStepList.rows[i].cells[1] != undefined) {
                                if (grdAuditStepList.rows[i].cells[1].getElementsByTagName("INPUT")[0] != undefined) {
                                    grdAuditStepList.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                                    selectedRowCount++;
                                }
                            }
                        }
                    }
                    else {
                        for (i = 1; i < grdAuditStepList.rows.length; i++) {
                            if (grdAuditStepList.rows[i].cells[1] != undefined) {
                                if (grdAuditStepList.rows[i].cells[1].getElementsByTagName("INPUT")[0] != undefined) {
                                    grdAuditStepList.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                                }
                            }
                        }
                    }
                }
            }

            var btnSave = document.getElementById("<%=btnSave.ClientID %>");
            var lblTotalSelected = document.getElementById("<%=lblTotalSelected.ClientID %>");
            if ((btnSave != null || btnSave != undefined) && (lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divBtnSave.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divBtnSave.style.display = "none";
                }
            }
        }


        function checkUncheckRow(header) {
            
            var ck = header;
            var selectedRowCount = 0;
            var grdAuditStepList = document.getElementById("<%=grdAuditStepList.ClientID %>");
            var headerchk = document.getElementById(header);
            var rowcount = grdAuditStepList.rows.length;
            //By using this for loop we will count how many checkboxes has checked
            for (i = 1; i < grdAuditStepList.rows.length; i++) {
                var inputs = grdAuditStepList.rows[i].getElementsByTagName('input');
                if (grdAuditStepList.rows[i].cells[1] != undefined) {
                    if (inputs[0].checked) {
                        selectedRowCount++;
                    }
                }
            }
            //Condition to check all the checkboxes selected or not
            if (selectedRowCount == rowcount - 1) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }

            var btnSave = document.getElementById("<%=btnSave.ClientID %>");
            var lblTotalSelected = document.getElementById("<%=lblTotalSelected.ClientID %>");
            if ((btnSave != null || btnSave != undefined) && (lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divBtnSave.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divBtnSave.style.display = "none";
                }
            }
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <asp:UpdatePanel ID="upAuditStepList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <div class="col-md-12 colpadding0" style="margin: 5px">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                             </div>

                            <div class="col-md-12 colpadding0">
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                 
                                     <div class="col-md-2 colpadding0" >
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">                                        
                                        <asp:ListItem Text="50" Selected="True" />
                                        <asp:ListItem Text="100" />
                                        <asp:ListItem Text="200" />
                                        <asp:ListItem Text="500" />
                                    </asp:DropDownList>                              
                                </div> 

                                 <div class="col-md-3 colpadding0" style="margin-top: 5px; color:#999; display:none;"> 
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                    <asp:ListItem Text="Process"  Value="Process" Selected="True" />                                    
                                    <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                    </asp:RadioButtonList>
                                 </div> 

                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>                                
                            </div>  
                                                         
                            <div class="clearfix"></div>  
                              
                            <div class="col-md-12 colpadding0">
                                
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                 </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Process"
                                 AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                   
                                 </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Sub Process"  
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                 </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 colpadding0">                                

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownCheckBoxes ID="ddlVertical" runat="server" AutoPostBack="true" Visible="true"
                                    AddJQueryReference="false" UseSelectAllNode="false" >
                                    <Style SelectBoxWidth="90%" DropDownBoxBoxWidth="90%" DropDownBoxBoxHeight="250" SelectBoxCssClass="chkBoxDropDown" />
                                    <Texts SelectBoxCaption="Select Vertical" />
                                </asp:DropDownCheckBoxes>                                    
                                    
                                    <asp:ExtendedRequiredFieldValidator ID = "ExtendedRequiredFieldValidator1" runat = "server" 
                                        ControlToValidate = "ddlVertical" ErrorMessage = "Required" ForeColor = "Red"></asp:ExtendedRequiredFieldValidator>     
                                                                       
                                    </div>

                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">     
                                    <asp:DropDownListChosen runat="server" ID="ddlStatus" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  CssClass="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Vertical">
                                    <asp:ListItem Text="Active" Value="1" Selected="True" />
                                    <asp:ListItem Text="De-Active" Value="0" />                                        
                                    </asp:DropDownListChosen>                                     
                                    </div> 
                               </div>

                            <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;"> 
                                <asp:DropDownCheckBoxes ID="ddlAuditStepList" runat="server" AutoPostBack="true" 
                                    Visible="true" Style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:350px;"  
                                    AddJQueryReference="false" UseSelectAllNode="false">
                                    <Style SelectBoxWidth="95%" DropDownBoxBoxWidth="95%" DropDownBoxBoxHeight="250" SelectBoxCssClass ="chkBoxDropDown" />
                                    <Texts SelectBoxCaption="Select Audit Step" />
                                </asp:DropDownCheckBoxes>
                                </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 colpadding0" style="text-align: right;">
                        <asp:Button ID="btnShow" Text="Show" class="btn btn-primary" CausesValidation="false" runat="server" OnClick="btnShow_Click" Style="margin-right: 4%;" />
                    </div>

                    <div class="clearfix"></div>

                    <div id="grdPageNumber" runat="server">

                        <div style="margin-bottom: 4px">
                            <asp:GridView ID="grdAuditStepList" runat="server" Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="grdAuditStepList_RowDataBound"
                                CssClass="table" GridLines="none" BorderWidth="0px" AutoGenerateColumns="false" PageSize="50" AllowPaging="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAll(this)" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRow" CssClass="chkActivate" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ATBDId" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblATBDId" runat="server" Text='<%# Eval("ATBDId") %>'
                                                data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ATBDId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="RiskCreationId" HeaderText="RiskID" Visible="false" />
                                    <asp:BoundField DataField="RiskActivityID" HeaderText="RiskActivityID" Visible="false" />

                                    <asp:BoundField DataField="ControlNo" HeaderText="Control No" />

                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                <asp:Label ID="BranchName" runat="server" Text='<%# Eval("BranchName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Vertical">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                <asp:Label ID="VerticalName" runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                <asp:Label ID="ProcessName" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Sub Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                                <asp:Label ID="SubProcessName" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="ActivityTobeDone" HeaderText="Audit Step" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />

                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerStyle HorizontalAlign="Right" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0" style="text-align: left;">
                                <div id="divBtnSave" style="display: none;">
                                    <asp:Button ID="btnSave" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" CausesValidation="false" runat="server" />
                                    <div class="table-Selecteddownload-text" style="width: 20%;">
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="text-align: Right;">
                                <div style="margin-right: 8%;">
                                    <asp:Label runat="server" ID="lblPage" Text="Page" Style="color: #999; margin-right: 10px;"></asp:Label>
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No"
                                        class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>

                    </div>

                    </section>
                </div>
            </div>

        </ContentTemplate>

    </asp:UpdatePanel>


</asp:Content>
