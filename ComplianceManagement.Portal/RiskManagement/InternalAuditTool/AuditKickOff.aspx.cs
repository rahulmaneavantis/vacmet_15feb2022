﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditKickOff : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                BindCustomerLocation();
                BindLocationFilter();
                GetPageDisplaySummary();
                BindData(-1,-1,"");
            }
        }       
        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            string FinancialYear = string.Empty;            
            int customerbranchid = -1;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            BindData(customerID, customerbranchid, FinancialYear);
        }    
        public void BindData(int customerid, int customerbranchid,string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (customerid != -1 && customerbranchid != -1  && !string.IsNullOrEmpty(FinancialYear))
                {                        
                    var RiskActivityToBeDoneMappingGetByName =
                       (from row in entities.InternalAuditSchedulings
                        join row1 in entities.Internal_AuditAssignment
                        on row.CustomerBranchId equals row1.CustomerBranchid
                        join MCB in entities.mst_CustomerBranch
                         on row.CustomerBranchId equals MCB.ID
                        where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
                        && row.CustomerBranchId == customerbranchid && row.FinancialYear==FinancialYear
                        select new AuditKickOffDetails()
                        {
                            //ProcessID = row.Process,
                            TermName = row.TermName,
                            CustomerBranchId = row.CustomerBranchId,
                            ISAHQMP = row.ISAHQMP,
                            FinancialYear = row.FinancialYear,
                            AssignedTo = row1.AssignedTo,
                            ExternalAuditorId = row1.ExternalAuditorId,
                            PhaseCount = row.PhaseCount
                        }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();
                }                
                else if (customerid != -1 && customerbranchid != -1)
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.CustomerBranchId == customerbranchid 
                            && MCB.CustomerID == customerid                                      
                            select new AuditKickOffDetails()
                            {
                                //ProcessID = row.Process,
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();
                
                }
                else if (customerid != -1 && !string.IsNullOrEmpty(FinancialYear))
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.FinancialYear == FinancialYear
                            && MCB.CustomerID == customerid
                            select new AuditKickOffDetails()
                            {
                                //ProcessID = row.Process,
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                }
                else if (customerbranchid != -1 && !string.IsNullOrEmpty(FinancialYear))
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.CustomerBranchId == customerbranchid
                            && row.FinancialYear == FinancialYear
                            select new AuditKickOffDetails()
                            {
                                //ProcessID = row.Process,
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                }
                else if (customerid != -1)
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && MCB.CustomerID == customerid
                            select new AuditKickOffDetails()
                            {
                                //ProcessID = row.Process,
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                }
                else if (customerbranchid != -1)
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false 
                            && row.CustomerBranchId == customerbranchid
                            select new AuditKickOffDetails()
                            {
                                //ProcessID = row.Process,
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                }
                else
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    var RiskActivityToBeDoneMappingGetByName =
                              (from row in entities.InternalAuditSchedulings
                               join row1 in entities.Internal_AuditAssignment
                               on row.CustomerBranchId equals row1.CustomerBranchid
                               join MCB in entities.mst_CustomerBranch
                                on row.CustomerBranchId equals MCB.ID
                               where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerID
                               select new AuditKickOffDetails()
                               {
                                   //ProcessID = row.Process,
                                   TermName = row.TermName,
                                   CustomerBranchId = row.CustomerBranchId,
                                   ISAHQMP = row.ISAHQMP,
                                   FinancialYear = row.FinancialYear,
                                   AssignedTo = row1.AssignedTo,
                                   ExternalAuditorId = row1.ExternalAuditorId,
                                   PhaseCount = row.PhaseCount
                               }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();                                
                }                         
            }
        }
        public string ShowCustomerBranchName(long id)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.Status == 1
                             && row.IsDeleted == false
                             && row.ID == id
                             select row).FirstOrDefault();

                processnonprocess = query.Name;
            }
            return processnonprocess.Trim(',');
        }
        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        public string ShowProcessNameMAIN(long Processid)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.IsDeleted == false
                             && row.Id == Processid
                             select row).FirstOrDefault();

                processnonprocess = query.Name;
            }
            return processnonprocess.Trim(',');
        }       
       
        //public void BindCustomerEntityFilter()
        //{
        //    int customerID = -1;
        //    customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
        //    ddlEntityFilter.DataTextField = "Name";
        //    ddlEntityFilter.DataValueField = "ID";
        //    //ddlEntityFilter.Items.Clear();
        //    ddlEntityFilter.DataSource = UserManagementRisk.FillCustomerName(customerID);
        //    ddlEntityFilter.DataBind();
        //    ddlEntityFilter.Items.Insert(0, new ListItem("< All >", "-1"));
        //    ddlFilterLocation.Items.Clear();
        //    ddlFilterLocation.Items.Insert(0, new ListItem("< All >", "-1"));

        //    ddlFilterFinancialYear.Items.Clear();
        //    ddlFilterFinancialYear.Items.Insert(0, new ListItem("< All >", "-1"));
        //}        
        public void BindCustomerLocation()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLocation.DataTextField = "Name";
            ddlLocation.DataValueField = "ID";
            //ddlLocation.Items.Clear();
            ddlLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("< Select Location >", "-1"));
        }
        //public void BindCustomerLocationFilter(int parentid)
        //{
        //    int customerID = -1;
        //    customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
        //    ddlFilterLocation.Items.Clear();
        //    ddlFilterLocation.DataTextField = "Name";
        //    ddlFilterLocation.DataValueField = "ID";
        //    ddlFilterLocation.Items.Clear();
        //    ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerEntytywise(customerID, parentid);
        //    ddlFilterLocation.DataBind();
        //    ddlFilterLocation.Items.Insert(0, new ListItem("< All >", "-1"));
        //}
        
        public void BindFinancialYear()
        {
            //int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            //ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("< Select Fnancial Year >", "-1"));
        }
        public void BindFinancialYearFilter()
        {
            //int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancialYear.DataBind();
            ddlFilterFinancialYear.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        public void BindSchedulingType()
        {
            //int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            //ddlSchedulingType.Items.Clear();
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingType();
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("< Select Scheduling Type >", "-1"));
        }
        private void BindAuditor()
        {
            try
            {
                ddlAuditor.Items.Clear();
                ddlAuditor.DataTextField = "Name";
                ddlAuditor.DataValueField = "ID";
                ddlAuditor.DataSource = ProcessManagement.GetAllList();
                ddlAuditor.DataBind();
                ddlAuditor.Items.Insert(0, new ListItem("< Select Auditor>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

                string FinancialYear = string.Empty;
                int customerbranchid = -1;
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                BindData(customerID, customerbranchid, FinancialYear);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {

        }
        protected void upCompliancesList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlInternalExternal_SelectedIndexChanged(object sender, EventArgs e)
        {           
            if (ddlInternalExternal.SelectedItem.Text == "Internal")
            {
                ddlAuditor.Items.Clear();
                ddlAuditor.Items.Insert(0, new ListItem("< Select Auditor>", "-1"));
                DivIsExternal.Visible = false;
            }
            else
            {
                DivIsExternal.Visible = true;
                BindAuditor();
            }
        }
       
        
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");                                                      
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar"); 
                    
                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");                                               
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");                           
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");                          
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");  
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");                           
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "< Select Fnancial Year >")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    DivIsPeriod.Visible = true;
                    BindAuditSchedule("A", 0);
                    //ddlSchedulingType_SelectedIndexChanged(null, null);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    DivIsPeriod.Visible = true;
                    BindAuditSchedule("H", 0);
                    //ddlSchedulingType_SelectedIndexChanged(null, null);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    DivIsPeriod.Visible = true;
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    DivIsPeriod.Visible = true;
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    DivIsPeriod.Visible = true;                                   
                }
            }
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int branchid = -1;
            //string FinancialYear=string.Empty;
            //string Formonth = string.Empty;
            //branchid=Convert.ToInt32(ddlLocation.SelectedValue);
            //FinancialYear=ddlFinancialYear.SelectedItem.Text;
           
            //if (ddlSchedulingType.SelectedItem.Text == "Annually")
            //{
            //    Formonth = "Annually";               
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            //{

            //    Formonth = ddlPeriod.SelectedItem.Text;
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            //{
            //    Formonth = ddlPeriod.SelectedItem.Text;
              
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            //{

            //    Formonth = ddlPeriod.SelectedItem.Text;
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            //{
            //    Formonth = ddlPeriod.SelectedItem.Text;
            //}
            //DIVdisplaygrid.Visible = true;
            //lblErrorMassage.Text = string.Empty;
            //List<SP_FillInternalAuditAssignment_Result> a = new List<SP_FillInternalAuditAssignment_Result>();
            //a = ProcessManagement.GetSP_FillInternalAuditAssignment_ResultProcedure(Convert.ToInt32(ViewState["StartEndDateProcessid"]), branchid, FinancialYear, Formonth).ToList();           
            //var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
            //grdRiskActivityMatrix.DataSource = null;
            //grdRiskActivityMatrix.DataBind();
            //grdRiskActivityMatrix.DataSource = remindersummary;
            //grdRiskActivityMatrix.DataBind();      
        }
        protected void ddlprocess_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;

            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                Formonth = "Annually";
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                Formonth = ddlPeriod.SelectedItem.Text;

            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                Formonth = ddlPeriod.SelectedItem.Text;
            }
            DIVdisplaygrid.Visible = true;
            lblErrorMassage.Text = string.Empty;
            List<SP_FillInternalAuditAssignment_Result> a = new List<SP_FillInternalAuditAssignment_Result>();
            if (!string.IsNullOrEmpty(ddlprocess.SelectedValue))
            {
                a = ProcessManagement.GetSP_FillInternalAuditAssignment_ResultProcedure(Convert.ToInt32(ddlprocess.SelectedValue), branchid, FinancialYear, Formonth).ToList();
            }
            var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
            grdRiskActivityMatrix.DataSource = null;
            grdRiskActivityMatrix.DataBind();
            grdRiskActivityMatrix.DataSource = remindersummary;
            grdRiskActivityMatrix.DataBind(); 
        }
        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPeriod.SelectedValue != "Select Period")
            {
                int branchid = -1;
                string FinancialYear = string.Empty;
                string Formonth = string.Empty;
                branchid = Convert.ToInt32(ddlLocation.SelectedValue);
                FinancialYear = ddlFinancialYear.SelectedItem.Text;

                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    Formonth = "Annually";
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {

                    Formonth = ddlPeriod.SelectedItem.Text;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    Formonth = ddlPeriod.SelectedItem.Text;

                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {

                    Formonth = ddlPeriod.SelectedItem.Text;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    Formonth = ddlPeriod.SelectedItem.Text;
                }
                DIVdisplaygrid.Visible = true;
                lblErrorMassage.Text = string.Empty;
                List<SP_FillInternalAuditAssignment_Result> a = new List<SP_FillInternalAuditAssignment_Result>();
                a = ProcessManagement.GetSP_FillInternalAuditAssignment_ResultProcedure(Convert.ToInt32(ViewState["StartEndDateProcessid"]), branchid, FinancialYear, Formonth).ToList();
                var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                grdRiskActivityMatrix.DataSource = remindersummary;
                grdRiskActivityMatrix.DataBind(); 
            }
           
        }
        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {                
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;                         
                upComplianceDetails.Update();                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
        }
        public string ShowProcessName(int ProcessId)
        {
            string processname = "";
            return processname;
        }
        public string ShowPerformerName(long ProcessId, long branchid, int roleid, long Userid)
        {
            string performername = string.Empty;
            string AssignedTo = string.Empty;
            int AuditorID = -1;
            if (ddlInternalExternal.SelectedItem.Text == "Internal" )
            {
                AssignedTo = "I";
                //List<SP_GETPerformerAndReveiwerNameInternal_Result> a = new List<SP_GETPerformerAndReveiwerNameInternal_Result>();
                //a = ProcessManagement.GETPerformerAndReveiwerNameInternalProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), roleid, AuditorID, Convert.ToInt32(Userid)).ToList();                 
                // var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                // if (remindersummary !=null)
                // {
                //     performername = remindersummary.Name;
                // }
                
            }
            else
            {
                AssignedTo = "E";
                //if (!string.IsNullOrEmpty(ddlAuditor.SelectedValue))
                //{
                //    AuditorID = Convert.ToInt32(ddlAuditor.SelectedValue);
                //}
                //List<SP_GETPerformerAndReveiwerNameInternal_Result> a = new List<SP_GETPerformerAndReveiwerNameInternal_Result>();
                ////a = ProcessManagement.GETPerformerAndReveiwerNameExternalProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), roleid, AuditorID, Convert.ToInt32(Userid)).ToList();
                //a = ProcessManagement.GETPerformerAndReveiwerNameInternalProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), roleid, AuditorID, Convert.ToInt32(Userid)).ToList();                 
                //var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                //if (remindersummary != null)
                //{
                //    performername = remindersummary.Name;
                //}
               
            }      
            return performername;
        }
        public string ShowReviewerName(long ProcessId, long branchid, int roleid, long Userid)
        {
            string reviewername = string.Empty;            
            string AssignedTo = string.Empty;
            int AuditorID = -1;
            if (ddlInternalExternal.SelectedItem.Text == "Internal")
            {
                AssignedTo = "I";
                //List<SP_GETPerformerAndReveiwerNameInternal_Result> a = new List<SP_GETPerformerAndReveiwerNameInternal_Result>();
                //a = ProcessManagement.GETPerformerAndReveiwerNameInternalProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), roleid, AuditorID, Convert.ToInt32(Userid)).ToList();
                //var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                //if (remindersummary !=null)
                //{
                //    reviewername = remindersummary.Name;
                //}
               
            }
            else
            {
                AssignedTo = "E";
                if (!string.IsNullOrEmpty(ddlAuditor.SelectedValue))
                {
                    AuditorID = Convert.ToInt32(ddlAuditor.SelectedValue);
                }
               // List<SP_GETPerformerAndReveiwerNameExternal_Result> a = new List<SP_GETPerformerAndReveiwerNameExternal_Result>();
                //List<SP_GETPerformerAndReveiwerNameInternal_Result> a = new List<SP_GETPerformerAndReveiwerNameInternal_Result>();
                ////a = ProcessManagement.GETPerformerAndReveiwerNameExternalProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), roleid, AuditorID, Convert.ToInt32(Userid)).ToList();
                //a = ProcessManagement.GETPerformerAndReveiwerNameInternalProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), roleid, AuditorID, Convert.ToInt32(Userid)).ToList();
                //var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                //if (remindersummary != null)
                //{
                //    reviewername = remindersummary.Name;
                //}
            }
           
            return reviewername;

        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
            //this.BindData("P");
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
               
                DropDownList ddlProcess = (e.Row.FindControl("ddlProcess") as DropDownList);
                if (ddlProcess != null)
                {
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ID";
                    ddlProcess.DataSource = ProcessManagement.FillProcess(customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("< Select Process >", "-1"));
                }
                if (ddlInternalExternal.SelectedItem.Text == "Internal")
                {
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                    if (ddlPerformer != null)
                    {
                       
                        ddlPerformer.Items.Clear();
                        ddlPerformer.DataTextField = "Name";
                        ddlPerformer.DataValueField = "ID";
                        ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                        ddlPerformer.DataBind();
                        ddlPerformer.Items.Insert(0, new ListItem("< Select Performer >", "-1"));
                    }
                    DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                    if (ddlReviewer != null)
                    {
                       
                        ddlReviewer.Items.Clear();
                        ddlReviewer.DataTextField = "Name";
                        ddlReviewer.DataValueField = "ID";
                        ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                        ddlReviewer.DataBind();
                        ddlReviewer.Items.Insert(0, new ListItem("< Select Reviewer >", "-1"));
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlAuditor.SelectedValue) != -1)
                    {
                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(ddlAuditor.SelectedValue));
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem("< Select Performer >", "-1"));
                        }
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(ddlAuditor.SelectedValue));
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("< Select Reviewer >", "-1"));
                        }
                    }
                }

                Label lblFooterTemplateProcess = (e.Row.FindControl("lblFooterTemplateProcess") as Label);
                if (lblFooterTemplateProcess !=null)
                {
                    if (!string.IsNullOrEmpty(ddlprocess.SelectedValue))
                    {
                        if (ddlprocess.SelectedValue != "-1")
                        {
                            lblFooterTemplateProcess.Text = ShowProcessNameMAIN(Convert.ToInt32(ddlprocess.SelectedValue));
                        }
                    }
                    
                    //lblFooterTemplateProcess.Text = ShowProcessNameMAIN(Convert.ToInt32(ViewState["StartEndDateProcessid"]));
                }
                Label lblFooterTemplateID = (e.Row.FindControl("lblFooterTemplateID") as Label);
                if (lblFooterTemplateID != null)
                {
                    //lblFooterTemplateID.Text = Convert.ToString(ViewState["StartEndDateProcessid"]);
                    if (!string.IsNullOrEmpty(ddlprocess.SelectedValue))
                    {
                        if (ddlprocess.SelectedValue != "-1")
                        {
                            lblFooterTemplateID.Text = Convert.ToString(ddlprocess.SelectedValue);
                        }
                    }
                } 
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }
        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindDropDownProcess(int branchid, string FinancialYear,string ForMonth)
        {
            //int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            ddlprocess.DataTextField = "Name";
            ddlprocess.DataValueField = "ID";
            ddlprocess.Items.Clear();
            ddlprocess.DataSource = UserManagementRisk.FillInternalAuditSchedulingProcess(branchid, FinancialYear, ForMonth);
            ddlprocess.DataBind();
            ddlprocess.Items.Insert(0, new ListItem("< Select Process >", "-1"));
        }
        private void OpenScheduleInformation(string ISAHQMP, int branchid, string FinancialYear,string AssignedTo,string ExternalAuditorId,string PhaseCount,string ForMonth)
        {
            try
            {
                BindSchedulingType();
                BindFinancialYear();             
                string Formonth = string.Empty;
                if (branchid != -1)
                {
                    ddlLocation.SelectedValue = Convert.ToString(branchid);
                }
                ddlFinancialYear.SelectedItem.Text = Convert.ToString(FinancialYear);                
                if (AssignedTo == "I")
                {
                    ddlInternalExternal.SelectedItem.Text = "Internal";
                }
                else
                {
                    ddlInternalExternal.SelectedItem.Text = "External";
                    ddlInternalExternal_SelectedIndexChanged(null, null);
                    ddlAuditor.SelectedValue = Convert.ToString(ExternalAuditorId);
                }
                if (Convert.ToString(ISAHQMP) == "A")
                {
                    Formonth = "Annually";
                    ddlSchedulingType.SelectedItem.Text = "Annually";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "H")
                {
                    ddlSchedulingType.SelectedItem.Text = "Half Yearly";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "Q")
                {
                    ddlSchedulingType.SelectedItem.Text = "Quarterly";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "M")
                {
                    ddlSchedulingType.SelectedItem.Text = "Monthly";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "P")
                {
                    DivIsPeriod.Visible = true;
                    ddlSchedulingType.SelectedItem.Text = "Phase";
                    BindAuditSchedule("P", Convert.ToInt32(PhaseCount));
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                BindDropDownProcess(branchid, FinancialYear, ForMonth);
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;                        
                DIVdisplaygrid.Visible = false;
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();                         
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string isAHQMP = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string Termname = string.Empty;
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    tbxLoanStartDate.Text =string.Empty;
                    tbxLoanEndDate.Text = string.Empty;

                    GridViewRow gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    Int32 rowind = gvRow.RowIndex;

                    Label lblLocation = (Label)gvRow.FindControl("lblLocation");

                    ViewState["Location"] = lblLocation.Text;

                    Label lblFinancialYear = (Label)gvRow.FindControl("lblFinancialYear");
                    Label lblAssignedTo = (Label)gvRow.FindControl("lblAssignedTo");
                    Label lblExternalAuditorId = (Label)gvRow.FindControl("lblExternalAuditorId");
                    Label lblPhaseCount = (Label)gvRow.FindControl("lblPhaseCount");                  
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                        }                       
                        ViewState["BranchID"] = CustomerBranchId;
                        OpenScheduleInformation(isAHQMP, CustomerBranchId, lblFinancialYear.Text, lblAssignedTo.Text, lblExternalAuditorId.Text, lblPhaseCount.Text, Termname);
                    }                    
                }
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }                   
        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {                              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateTransaction(List<InternalAuditTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.InternalAuditTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }
        public static void CreateScheduleOn(long InternalAuditInstance, long createdByID, string creatdByName, string FinancialYear, long branchid,string Period,long Processid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DateTime curruntDate = DateTime.UtcNow;

                    List<InternalAuditTransaction> transactionlist = new List<InternalAuditTransaction>();
                   
                        InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                        auditScheduleon.InternalAuditInstance = InternalAuditInstance;
                        auditScheduleon.ForMonth = Period;
                        auditScheduleon.FinancialYear = FinancialYear;
                        auditScheduleon.ProcessId = Processid;
                        entities.InternalAuditScheduleOns.Add(auditScheduleon);
                        entities.SaveChanges();

                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                        {
                            InternalAuditInstance = InternalAuditInstance,
                            AuditScheduleOnID = auditScheduleon.ID,
                            CreatedBy = createdByID,
                            CreatedByText = creatdByName,
                            ProcessId = Processid,
                            StatusId = 1,
                            CustomerBranchId = branchid,
                            Remarks = "New Audit Steps assigned."

                        };

                        transactionlist.Add(transaction);
                    
                    CreateTransaction(transactionlist);

                }
            }
            catch (Exception ex)
            {
                //List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("rahulmane3192@gmail.com");
                //TO.Add("manishajalnekar07@gmail.com");


                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void Add(object sender, EventArgs e)
        {
            try
            {
                if (ddlPeriod.SelectedValue == "Select Period")
                {
                    cvDuplicateEntry.ErrorMessage = "Select Period";
                    return;
                }
                Control control = null;
                if (grdRiskActivityMatrix.FooterRow != null)
                {
                    control = grdRiskActivityMatrix.FooterRow;
                }
                else
                {
                    control = grdRiskActivityMatrix.Controls[0].Controls[0];
                }
                string lblFooterTemplateProcessID = (control.FindControl("lblFooterTemplateID") as Label).Text;                
                string ddlPerformer = (control.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                string ddlReviewer = (control.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;

                string AssignedTo = string.Empty;
                if ( ddlInternalExternal.SelectedItem.Text == "Internal")
                {
                    AssignedTo = "I";
                }
                else
                {
                    AssignedTo = "E";
                }

                InternalAuditInstance riskinstance = new InternalAuditInstance();
                riskinstance.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                riskinstance.IsDeleted = false;
                riskinstance.CreatedOn = DateTime.Now;
                riskinstance.ProcessId = Convert.ToInt32(lblFooterTemplateProcessID);
                riskinstance.SubProcessId = -1;
                UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                List<InternalControlAuditAssignment> Tempassignments = new List<InternalControlAuditAssignment>();

                DateTime a = DateTime.ParseExact(tbxLoanStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime b = DateTime.ParseExact(tbxLoanEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
               

                if (ddlPerformer != null)
                {
                    InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();                   
                    TempAssP.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                    TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                    TempAssP.UserID = Convert.ToInt32(ddlPerformer);
                    TempAssP.IsActive = true;
                    TempAssP.CreatedOn = DateTime.Now;
                    TempAssP.ProcessId = Convert.ToInt32(lblFooterTemplateProcessID);
                    TempAssP.SubProcessId = -1;
                    TempAssP.ISInternalExternal = AssignedTo;
                    TempAssP.InternalAuditInstance = riskinstance.ID;
                    TempAssP.ExpectedStartDate = GetDate(a.ToString("dd-MM-yyyy"));
                    TempAssP.ExpectedEndDate = GetDate(b.ToString("dd-MM-yyyy"));
                    
                    Tempassignments.Add(TempAssP);
                }
                if (ddlReviewer != null)
                {
                    InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                    TempAssR.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                    TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                    TempAssR.UserID = Convert.ToInt32(ddlReviewer);
                    TempAssR.IsActive = true;
                    TempAssR.CreatedOn = DateTime.Now;
                    TempAssR.ProcessId = Convert.ToInt32(lblFooterTemplateProcessID);
                    TempAssR.SubProcessId = -1;
                    TempAssR.ISInternalExternal = AssignedTo;
                    TempAssR.InternalAuditInstance = riskinstance.ID;
                    TempAssR.ExpectedStartDate = GetDate(a.ToString("dd-MM-yyyy"));
                    TempAssR.ExpectedEndDate = GetDate(b.ToString("dd-MM-yyyy"));
                    Tempassignments.Add(TempAssR);
                }
                if (Tempassignments.Count != 0)
                {
                    UserManagementRisk.AddDetailsInternalControlAuditAssignment(Tempassignments);
                }
                CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlLocation.SelectedValue), ddlPeriod.SelectedItem.Text, Convert.ToInt32(lblFooterTemplateProcessID));
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceDetailsDialog\").dialog('close')", true);
                DIVdisplaygrid.Visible = false;
                ddlPeriod.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void Delete(object sender, EventArgs e)
        {
            //Button btnRemove = (Button)sender;
            //GridViewRow objGvRow = (GridViewRow)btnRemove.NamingContainer;
            //string RequestId = Convert.ToString(grdRiskActivityMatrix.DataKeys[objGvRow.RowIndex].Value);
            //RiskCategoryManagement.Delete(Convert.ToInt32(RequestId));
            //this.BindData("P");
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCompliances.PageIndex = e.NewPageIndex;
            string FinancialYear = string.Empty;            
            int customerbranchid = -1;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            BindData(customerID, customerbranchid, FinancialYear);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                //BindProcessSubType();
                BindData(-1, -1, "");
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                //BindProcessSubType();
                BindData(-1, -1, "");
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid
                //BindProcessSubType();
                BindData(-1, -1, "");
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }



    }
}