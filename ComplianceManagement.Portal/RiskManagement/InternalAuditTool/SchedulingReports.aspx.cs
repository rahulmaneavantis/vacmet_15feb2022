﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class SchedulingReports : System.Web.UI.Page
    {
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public List<long> Branchlist = new List<long>();
        public List<long> RegionBranchlist = new List<long>();
        public string linkclick;
        protected string AuditHeadOrManagerReport = null;
        protected int CustomerId = 0;
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFinancialYear();
                BindVertical();
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
                BindData();
                bindPageNumber();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (DepartmentHead)
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
             
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        public void BindSchedulingType()
        {
            int branchid = -1;
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }
        public void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            /*
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            */
            int customerId = Convert.ToInt32(customerid);
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerId, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerId, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerId, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerId, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                RegionBranchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindData()
        {
            try
            {
                int UserID = -1;
                int branchid = -1;
                int VerticalID = -1;
                string FinancialYear = string.Empty;
                string ForMonth = string.Empty;
                int RoleID = -1;
                string departmentheadFlag = string.Empty;
                if (DepartmentHead)
                {
                    departmentheadFlag = "DH";
                }
                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        ForMonth = Convert.ToString(ddlPeriod.SelectedValue);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }

                UserID = Portal.Common.AuthenticationHelper.UserID;
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(CustomerId, branchid);
                List<SP_SchedulingReportsView_Result> transactionquery = new List<SP_SchedulingReportsView_Result>();
                if (AuditHeadOrManagerReport == null)
                {
                    AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;

                    if (departmentheadFlag == "DH")
                    {

                        transactionquery = (from row in entities.SP_SchedulingReportsView(UserID, 4, "DH")
                                            select row).ToList();

                    }
                    else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
                    {

                        transactionquery = (from row in entities.SP_SchedulingReportsView(UserID, 4, "MA")
                                            select row).ToList();


                    }
                    else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {

                        transactionquery = (from row in entities.SP_SchedulingReportsView(UserID, 4, "AM")
                                            select row).ToList();

                    }
                    else
                    {
                        //PR Means As Performer or reviewer
                        transactionquery = (from row in entities.SP_SchedulingReportsView(UserID, RoleID, "PR")
                                            select row).ToList();

                    }

                    if (transactionquery.Count > 0)
                        transactionquery = transactionquery.GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();

                    if (Branchlist.Count > 0)
                    {
                        transactionquery = transactionquery.Where(entity => Branchlist.Contains(entity.CustomerBranchId)).ToList();
                    }
                    if (!string.IsNullOrEmpty(FinancialYear))
                    {
                        transactionquery = transactionquery.Where(entity => entity.FinancialYear == FinancialYear).ToList();
                    }
                    if (!string.IsNullOrEmpty(ForMonth))
                    {
                        transactionquery = transactionquery.Where(entity => entity.ForMonth == ForMonth).ToList();
                    }
                    if (VerticalID != -1)
                    {
                        transactionquery = transactionquery.Where(entity => entity.VerticalID == VerticalID).ToList();
                    }
                    grdSummaryDetailsAuditCoverage.DataSource = transactionquery;
                    Session["TotalRows"] = transactionquery.Count();
                    grdSummaryDetailsAuditCoverage.DataBind();
                    Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<SP_SchedulingReportsView_Result>).ToDataTable();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";

                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }

                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }

        private void BindDataExport(string Flag)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (AuditHeadOrManagerReport == null)
                {
                    AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (Flag == "R")
                    {
                        List<RegionView> masterRegiondetailView = new List<RegionView>();
                        List<RegionView> RegiondetailView = new List<RegionView>();
                        RegiondetailView = UserManagementRisk.GetAllRegionData(CustomerId);
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            masterRegiondetailView = (from row in RegiondetailView
                                                      join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                                      on row.ID equals (int)EAAR.BranchID
                                                      where EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                      && EAAR.ISACTIVE == true
                                                      select row).Distinct().ToList();
                        }
                        else
                        {
                            masterRegiondetailView = (from row in RegiondetailView
                                                      join ICAA in entities.InternalControlAuditAssignments
                                                      on row.ID equals (int)ICAA.CustomerBranchID
                                                      where ICAA.UserID == Portal.Common.AuthenticationHelper.UserID
                                                      select row).Distinct().ToList();
                        }
                        grdRegionReport.DataSource = masterRegiondetailView;
                        grdRegionReport.DataBind();
                        Session["grdRegionData"] = (grdRegionReport.DataSource as List<RegionView>).ToDataTable();
                    }
                    else if (Flag == "S")
                    {
                        List<StateView> masterStatedetailView = new List<StateView>();
                        List<StateView> StatedetailView = new List<StateView>();
                        StatedetailView = UserManagementRisk.GetAllStateData(CustomerId);
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            masterStatedetailView = (from row in StatedetailView
                                                     join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                                     on row.ID equals (int)EAAR.BranchID
                                                     where EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                     && EAAR.ISACTIVE == true
                                                     select row).Distinct().ToList();
                        }
                        else
                        {
                            masterStatedetailView = (from row in StatedetailView
                                                     join ICAA in entities.InternalControlAuditAssignments
                                                     on row.ID equals (int)ICAA.CustomerBranchID
                                                     where ICAA.UserID == Portal.Common.AuthenticationHelper.UserID
                                                     select row).Distinct().ToList();
                        }
                        grdStateReport.DataSource = masterStatedetailView;
                        grdStateReport.DataBind();
                        Session["grdStateData"] = (grdStateReport.DataSource as List<StateView>).ToDataTable();
                    }
                    else if (Flag == "H")
                    {
                        List<HubView> masterHubdetailView = new List<HubView>();
                        List<HubView> HubdetailView = new List<HubView>();
                        HubdetailView = UserManagementRisk.GetAllHubData(CustomerId);
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            masterHubdetailView = (from row in HubdetailView
                                                   join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                                   on row.ID equals (int)EAAR.BranchID
                                                   where EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                   && EAAR.ISACTIVE == true
                                                   select row).Distinct().ToList();
                        }
                        else
                        {
                            masterHubdetailView = (from row in HubdetailView
                                                   join ICAA in entities.InternalControlAuditAssignments
                                                   on row.ID equals (int)ICAA.CustomerBranchID
                                                   where ICAA.UserID == Portal.Common.AuthenticationHelper.UserID
                                                   select row).Distinct().ToList();
                        }
                        grdHubReport.DataSource = masterHubdetailView;
                        grdHubReport.DataBind();
                        Session["grdHubData"] = (grdHubReport.DataSource as List<HubView>).ToDataTable();
                    }
                    else if (Flag == "B")
                    {
                        List<BranchView> masterBranchdetailView = new List<BranchView>();
                        List<BranchView> BranchdetailView = new List<BranchView>();
                        BranchdetailView = UserManagementRisk.GetAllBranchData(CustomerId);
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            masterBranchdetailView = (from row in BranchdetailView
                                                      join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                                      on row.ID equals (int)EAAR.BranchID
                                                      where EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                      && EAAR.ISACTIVE == true
                                                      select row).Distinct().ToList();
                        }
                        else
                        {
                            masterBranchdetailView = (from row in BranchdetailView
                                                      join ICAA in entities.InternalControlAuditAssignments
                                                      on row.ID equals (int)ICAA.CustomerBranchID
                                                      where ICAA.UserID == Portal.Common.AuthenticationHelper.UserID
                                                      select row).Distinct().ToList();
                        }
                        grdBranchReport.DataSource = masterBranchdetailView;
                        grdBranchReport.DataBind();
                        Session["grdBranchData"] = (grdBranchReport.DataSource as List<BranchView>).ToDataTable();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected string GetBranchNameRegion(int Customerid, long Branchid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    long pid = (from row in entities.mst_CustomerBranch
                                where row.CustomerID == Customerid && row.ID == Branchid
                                && row.IsDeleted == false
                                select (long)row.ParentID).FirstOrDefault();
                    if (pid != null)
                    {
                        long innerpid = (from row in entities.mst_CustomerBranch
                                         where row.CustomerID == Customerid && row.ID == pid
                                         && row.IsDeleted == false
                                         select (long)row.ParentID).FirstOrDefault();

                        if (innerpid != null)
                        {
                            var transactionsQuery = (from row in entities.mst_CustomerBranch
                                                     where row.CustomerID == Customerid && row.ID == innerpid
                                                       && row.IsDeleted == false
                                                     select row.Name).FirstOrDefault();
                            return transactionsQuery;
                        }
                        else
                        {
                            return "";
                        }

                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected string GetBranchNameStateORRegion(int Customerid, long Branchid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    long pid = (from row in entities.mst_CustomerBranch
                                where row.CustomerID == Customerid && row.ID == Branchid
                                && row.IsDeleted == false
                                select (long)row.ParentID).FirstOrDefault();
                    if (pid != null)
                    {
                        var transactionsQuery = (from row in entities.mst_CustomerBranch
                                                 where row.CustomerID == Customerid && row.ID == pid
                                                   && row.IsDeleted == false
                                                 select row.Name).FirstOrDefault();
                        return transactionsQuery;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected string GetBranchName(int Customerid, long Branchid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_CustomerBranch
                                             where row.CustomerID == Customerid && row.ID == Branchid
                                             && row.IsDeleted == false
                                             select row.Name).FirstOrDefault();
                    return transactionsQuery;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected long GetPlannedANDACtualCount(int Customerid, List<long> CustomerBranch, int Userid, int RoleID, string FinancialYear, string Period, string Flag)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<SchedulingReportsView> masterquery = new List<SchedulingReportsView>();
                    List<SchedulingReportsView> transactionquery = new List<SchedulingReportsView>();
                    if (AuditHeadOrManagerReport == null)
                    {
                        AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    }
                    long Count = -1;
                    masterquery = (from row in entities.SchedulingReportsViews
                                   where row.CustomerID == Customerid
                                   select row).ToList();

                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        transactionquery = (from row in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on row.CustomerBranchId equals ICAA.CustomerBranchID
                                            join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                            on row.CustomerBranchId equals (int)EAAR.BranchID
                                            where row.AuditID == ICAA.AuditID
                                            && EAAR.ProcessId == ICAA.ProcessId
                                            && EAAR.UserID == Userid
                                            && EAAR.ISACTIVE == true
                                            && row.RoleID == 4
                                            select row).Distinct().ToList();
                    }
                    else
                    {
                        transactionquery = (from row in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on row.CustomerBranchId equals (int)ICAA.CustomerBranchID
                                            where ICAA.AuditID == row.AuditID
                                            && ICAA.UserID == Userid
                                            && row.RoleID == RoleID
                                            select row).Distinct().ToList();
                    }

                    //if (Userid !=-1)
                    //{
                    //    transactionquery = transactionquery.Where(entry => entry.UserID == Userid).ToList();
                    //}
                    //if (RoleID != -1)
                    //{
                    //    transactionquery = transactionquery.Where(entry => entry.RoleID == RoleID).ToList();
                    //}
                    if (CustomerBranch.Count > 0)
                    {
                        transactionquery = transactionquery.Where(entry => CustomerBranch.Contains(entry.CustomerBranchId)).ToList();
                    }
                    if (!string.IsNullOrEmpty(FinancialYear))
                    {
                        transactionquery = transactionquery.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                    }
                    if (!string.IsNullOrEmpty(Period))
                    {
                        transactionquery = transactionquery.Where(entry => entry.ForMonth == Period).ToList();
                    }
                    if (Flag == "Planned")
                    {
                        Count = transactionquery.Where(en => en.StartDate != null).Select(en => en.StartDate).Count();
                    }
                    else if (Flag == "Actual")
                    {
                        Count = transactionquery.Where(en => en.ExpectedStartDate != null).Select(en => en.ExpectedStartDate).Count();
                    }
                    return Count;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                }
                int UserID = -1;
                string FinancialYear = string.Empty;
                string ForMonth = string.Empty;
                int RoleID = -1;
                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        ForMonth = Convert.ToString(ddlPeriod.SelectedValue);
                    }
                }
                if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    if (RoleID == -1)
                    {
                        RoleID = 4;
                    }
                }
                UserID = Portal.Common.AuthenticationHelper.UserID;
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;
                    FileName = "Audit Scheduling Report";
                    string CustomerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(CustomerId));

                    #region Sheet 1
                    //DataTable ExcelDataRegion = null;
                    //BindDataExport("R");
                    //DataView viewRegion = new System.Data.DataView((DataTable)Session["grdRegionData"]);
                    //ExcelDataRegion = viewRegion.ToTable("Selected", false, "Region", "Planned", "Actual", "ID");
                    //if (ExcelDataRegion.Rows.Count > 0)
                    //{
                    //    foreach (DataRow item in ExcelDataRegion.Rows)
                    //    {
                    //        Branchlist.Clear();
                    //        var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(item["ID"].ToString()));
                    //        var Branchlistloop = Branchlist.ToList();

                    //        if (item["Planned"].ToString() == null || item["Planned"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Planned"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Planned");
                    //            }
                    //            else
                    //            {
                    //                item["Planned"] = 0;
                    //            }
                    //        }
                    //        if (item["Actual"].ToString() == null || item["Actual"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Actual"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Actual");
                    //            }
                    //            else
                    //            {
                    //                item["Actual"] = 0;
                    //            }
                    //        }
                    //    }
                    //    ExcelDataRegion.Columns.Remove("ID");

                    //    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Region");
                    //    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    //    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    //    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    //    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                    //    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    //    exWorkSheet.Cells["A2"].Value = CustomerName;
                    //    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    //    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    //    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelDataRegion, true);

                    //    exWorkSheet.Cells["A3"].Value = FileName;
                    //    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    //    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    //    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    //    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    //    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    //    exWorkSheet.Cells["A5"].Value = "Region";
                    //    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    //    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    //    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    //    exWorkSheet.Cells["B5"].Value = "Planned";
                    //    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    //    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    //    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    //    exWorkSheet.Cells["C5"].Value = "Actual";
                    //    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    //    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelDataRegion.Rows.Count, 3])
                    //    {
                    //        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                    //        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    //        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    //        col.Style.WrapText = true;
                    //        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    //    }
                    //}
                    #endregion

                    #region Sheet 2
                    //DataTable ExcelDataState = null;
                    //BindDataExport("S");
                    //DataView viewStates = new System.Data.DataView((DataTable)Session["grdStateData"]);
                    //ExcelDataState = viewStates.ToTable("Selected", false, "Region", "State", "Planned", "Actual", "ID", "Parentid");
                    //if (ExcelDataState.Rows.Count > 0)
                    //{
                    //    foreach (DataRow item in ExcelDataState.Rows)
                    //    {
                    //        Branchlist.Clear();
                    //        var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(item["ID"].ToString()));
                    //        var Branchlistloop = Branchlist.ToList();

                    //        if (item["Region"].ToString() == null || item["Region"].ToString() == "")
                    //        {
                    //            item["Region"] = GetBranchName(CustomerId, Convert.ToInt32(item["Parentid"].ToString()));
                    //        }
                    //        if (item["Planned"].ToString() == null || item["Planned"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Planned"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Planned");
                    //            }
                    //            else
                    //            {
                    //                item["Planned"] = 0;
                    //            }

                    //        }
                    //        if (item["Actual"].ToString() == null || item["Actual"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Actual"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Actual");
                    //            }
                    //            else
                    //            {
                    //                item["Actual"] = 0;
                    //            }
                    //        }
                    //    }
                    //    ExcelDataState.Columns.Remove("ID");
                    //    ExcelDataState.Columns.Remove("Parentid");
                    //    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("State");
                    //    exWorkSheet1.Cells["A1"].Value = "Report Generated On:";
                    //    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["A1"].Style.Font.Size = 12;

                    //    exWorkSheet1.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                    //    exWorkSheet1.Cells["B1"].Style.Font.Size = 12;

                    //    exWorkSheet1.Cells["A2"].Value = CustomerName;
                    //    exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["A2"].Style.Font.Size = 12;

                    //    exWorkSheet1.Cells["A5"].LoadFromDataTable(ExcelDataState, true);

                    //    exWorkSheet1.Cells["A3"].Value = FileName;
                    //    exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                    //    exWorkSheet1.Cells["A3"].AutoFitColumns(50);

                    //    exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["A5"].Style.Font.Size = 12;
                    //    exWorkSheet1.Cells["A5"].Value = "Region";
                    //    exWorkSheet1.Cells["A5"].AutoFitColumns(50);

                    //    exWorkSheet1.Cells["B5"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["B5"].Style.Font.Size = 12;
                    //    exWorkSheet1.Cells["B5"].Value = "State";
                    //    exWorkSheet1.Cells["B5"].AutoFitColumns(50);

                    //    exWorkSheet1.Cells["C5"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["C5"].Style.Font.Size = 12;
                    //    exWorkSheet1.Cells["C5"].Value = "Planned";
                    //    exWorkSheet1.Cells["C5"].AutoFitColumns(15);

                    //    exWorkSheet1.Cells["D5"].Style.Font.Bold = true;
                    //    exWorkSheet1.Cells["D5"].Style.Font.Size = 12;
                    //    exWorkSheet1.Cells["D5"].Value = "Actual";
                    //    exWorkSheet1.Cells["D5"].AutoFitColumns(15);

                    //    using (ExcelRange col = exWorkSheet1.Cells[5, 1, 5 + ExcelDataState.Rows.Count, 4])
                    //    {
                    //        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                    //        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    //        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    //        col.Style.WrapText = true;
                    //        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //    }
                    //}
                    #endregion

                    #region Sheet 3
                    //DataTable ExcelDataHUB = null;
                    //BindDataExport("H");
                    //DataView viewHUB = new System.Data.DataView((DataTable)Session["grdHubData"]);
                    //ExcelDataHUB = viewHUB.ToTable("Selected", false, "Region", "State", "Hub", "Planned", "Actual", "ID", "Parentid");
                    //if (ExcelDataHUB.Rows.Count > 0)
                    //{
                    //    foreach (DataRow item in ExcelDataHUB.Rows)
                    //    {
                    //        Branchlist.Clear();
                    //        var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(item["ID"].ToString()));
                    //        var Branchlistloop = Branchlist.ToList();

                    //        if (item["Region"].ToString() == null || item["Region"].ToString() == "")
                    //        {
                    //            item["Region"] = GetBranchNameStateORRegion(CustomerId, Convert.ToInt32(item["Parentid"].ToString()));
                    //        }
                    //        if (item["State"].ToString() == null || item["State"].ToString() == "")
                    //        {
                    //            item["State"] = GetBranchName(CustomerId, Convert.ToInt32(item["Parentid"].ToString()));
                    //        }
                    //        if (item["Planned"].ToString() == null || item["Planned"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Planned"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Planned");
                    //            }
                    //            else
                    //            {
                    //                item["Planned"] = 0;
                    //            }

                    //        }
                    //        if (item["Actual"].ToString() == null || item["Actual"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Actual"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Actual");
                    //            }
                    //            else
                    //            {
                    //                item["Actual"] = 0;
                    //            }
                    //        }
                    //    }
                    //    ExcelDataHUB.Columns.Remove("ID");
                    //    ExcelDataHUB.Columns.Remove("Parentid");

                    //    ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Hub");
                    //    exWorkSheet2.Cells["A1"].Value = "Report Generated On:";
                    //    exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["A1"].Style.Font.Size = 12;

                    //    exWorkSheet2.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                    //    exWorkSheet2.Cells["B1"].Style.Font.Size = 12;

                    //    exWorkSheet2.Cells["A2"].Value = CustomerName;
                    //    exWorkSheet2.Cells["A2"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["A2"].Style.Font.Size = 12;

                    //    exWorkSheet2.Cells["A5"].LoadFromDataTable(ExcelDataHUB, true);

                    //    exWorkSheet2.Cells["A3"].Value = FileName;
                    //    exWorkSheet2.Cells["A3"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["A3"].Style.Font.Size = 12;
                    //    exWorkSheet2.Cells["A3"].AutoFitColumns(50);

                    //    exWorkSheet2.Cells["A5"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["A5"].Style.Font.Size = 12;
                    //    exWorkSheet2.Cells["A5"].Value = "Region";
                    //    exWorkSheet2.Cells["A5"].AutoFitColumns(50);

                    //    exWorkSheet2.Cells["B5"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["B5"].Style.Font.Size = 12;
                    //    exWorkSheet2.Cells["B5"].Value = "State";
                    //    exWorkSheet2.Cells["B5"].AutoFitColumns(50);

                    //    exWorkSheet2.Cells["C5"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["C5"].Style.Font.Size = 12;
                    //    exWorkSheet2.Cells["C5"].Value = "Hub";
                    //    exWorkSheet2.Cells["C5"].AutoFitColumns(50);

                    //    exWorkSheet2.Cells["D5"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["D5"].Style.Font.Size = 12;
                    //    exWorkSheet2.Cells["D5"].Value = "Planned";
                    //    exWorkSheet2.Cells["D5"].AutoFitColumns(15);

                    //    exWorkSheet2.Cells["E5"].Style.Font.Bold = true;
                    //    exWorkSheet2.Cells["E5"].Style.Font.Size = 12;
                    //    exWorkSheet2.Cells["E5"].Value = "Actual";
                    //    exWorkSheet2.Cells["E5"].AutoFitColumns(15);

                    //    using (ExcelRange col = exWorkSheet2.Cells[5, 1, 5 + ExcelDataHUB.Rows.Count, 5])
                    //    {
                    //        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                    //        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    //        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    //        col.Style.WrapText = true;
                    //        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //    }
                    //}
                    #endregion

                    #region Sheet 4

                    //DataTable ExcelDataBranch = null;
                    //BindDataExport("B");
                    //DataView viewBranch = new System.Data.DataView((DataTable)Session["grdBranchData"]);
                    //ExcelDataBranch = viewBranch.ToTable("Selected", false, "Region", "State", "Hub", "Branch", "Planned", "Actual", "ID", "Parentid");
                    //if (ExcelDataBranch.Rows.Count > 0)
                    //{
                    //    foreach (DataRow item in ExcelDataBranch.Rows)
                    //    {
                    //        Branchlist.Clear();
                    //        var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(item["ID"].ToString()));
                    //        var Branchlistloop = Branchlist.ToList();

                    //        if (item["Region"].ToString() == null || item["Region"].ToString() == "")
                    //        {
                    //            item["Region"] = GetBranchNameRegion(CustomerId, Convert.ToInt32(item["Parentid"].ToString()));
                    //        }
                    //        if (item["State"].ToString() == null || item["State"].ToString() == "")
                    //        {
                    //            item["State"] = GetBranchNameStateORRegion(CustomerId, Convert.ToInt32(item["Parentid"].ToString()));
                    //        }
                    //        if (item["Hub"].ToString() == null || item["Hub"].ToString() == "")
                    //        {
                    //            item["Hub"] = GetBranchName(CustomerId, Convert.ToInt32(item["Parentid"].ToString()));
                    //        }
                    //        if (item["Planned"].ToString() == null || item["Planned"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Planned"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Planned");
                    //            }
                    //            else
                    //            {
                    //                item["Planned"] = 0;
                    //            }

                    //        }
                    //        if (item["Actual"].ToString() == null || item["Actual"].ToString() == "")
                    //        {
                    //            if (Branchlist.Count > 0)
                    //            {
                    //                item["Actual"] = GetPlannedANDACtualCount(CustomerId, Branchlist, UserID, RoleID, FinancialYear, ForMonth, "Actual");
                    //            }
                    //            else
                    //            {
                    //                item["Actual"] = 0;
                    //            }
                    //        }
                    //    }
                    //    ExcelDataBranch.Columns.Remove("ID");
                    //    ExcelDataBranch.Columns.Remove("Parentid");
                    //    ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Location");
                    //    exWorkSheet3.Cells["A1"].Value = "Report Generated On:";
                    //    exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["A1"].Style.Font.Size = 12;

                    //    exWorkSheet3.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                    //    exWorkSheet3.Cells["B1"].Style.Font.Size = 12;

                    //    exWorkSheet3.Cells["A2"].Value = CustomerName;
                    //    exWorkSheet3.Cells["A2"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["A2"].Style.Font.Size = 12;

                    //    exWorkSheet3.Cells["A5"].LoadFromDataTable(ExcelDataBranch, true);

                    //    exWorkSheet3.Cells["A3"].Value = FileName;
                    //    exWorkSheet3.Cells["A3"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["A3"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["A3"].AutoFitColumns(50);

                    //    exWorkSheet3.Cells["A5"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["A5"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["A5"].Value = "Region";
                    //    exWorkSheet3.Cells["A5"].AutoFitColumns(50);

                    //    exWorkSheet3.Cells["B5"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["B5"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["B5"].Value = "State";
                    //    exWorkSheet3.Cells["B5"].AutoFitColumns(50);

                    //    exWorkSheet3.Cells["C5"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["C5"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["C5"].Value = "Hub";
                    //    exWorkSheet3.Cells["C5"].AutoFitColumns(50);

                    //    exWorkSheet3.Cells["D5"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["D5"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["D5"].Value = "Branch";
                    //    exWorkSheet3.Cells["D5"].AutoFitColumns(50);

                    //    exWorkSheet3.Cells["E5"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["E5"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["E5"].Value = "Planned";
                    //    exWorkSheet3.Cells["E5"].AutoFitColumns(15);

                    //    exWorkSheet3.Cells["F5"].Style.Font.Bold = true;
                    //    exWorkSheet3.Cells["F5"].Style.Font.Size = 12;
                    //    exWorkSheet3.Cells["F5"].Value = "Actual";
                    //    exWorkSheet3.Cells["F5"].AutoFitColumns(15);

                    //    using (ExcelRange col = exWorkSheet3.Cells[5, 1, 5 + ExcelDataBranch.Rows.Count, 6])
                    //    {
                    //        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                    //        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    //        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    //        col.Style.WrapText = true;
                    //        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //    }
                    //}
                    #endregion

                    #region Sheet 5


                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    ExcelData = view.ToTable("Selected", false, "Branch", "VerticalName", "FinancialYear", "ForMonth", "StartDate", "ExpectedStartDate", "EndDate", "ExpectedEndDate");
                    if (ExcelData.Rows.Count > 0)
                    {
                        ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Audit Scheduling Report");
                        exWorkSheet4.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A1"].Style.Font.Size = 12;

                        exWorkSheet4.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                        exWorkSheet4.Cells["B1"].Style.Font.Size = 12;

                        exWorkSheet4.Cells["A2"].Value = CustomerName;
                        exWorkSheet4.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A2"].Style.Font.Size = 12;

                        exWorkSheet4.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet4.Cells["A3"].Value = FileName;
                        exWorkSheet4.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["A3"].AutoFitColumns(50);

                        exWorkSheet4.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["A5"].Value = "Branch";
                        exWorkSheet4.Cells["A5"].AutoFitColumns(50);

                        exWorkSheet4.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["B5"].Value = "Vertical";
                        exWorkSheet4.Cells["B5"].AutoFitColumns(30);

                        exWorkSheet4.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["C5"].Value = "Financial Year";
                        exWorkSheet4.Cells["C5"].AutoFitColumns(15);

                        exWorkSheet4.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["D5"].Value = "Period";
                        exWorkSheet4.Cells["D5"].AutoFitColumns(15);

                        exWorkSheet4.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["E5"].Value = "Planned Start Date";
                        exWorkSheet4.Cells["E5"].AutoFitColumns(20);

                        exWorkSheet4.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["F5"].Value = "Actual Start Date";
                        exWorkSheet4.Cells["F5"].AutoFitColumns(20);

                        exWorkSheet4.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["G5"].Value = "Planned End Date";
                        exWorkSheet4.Cells["G5"].AutoFitColumns(20);

                        exWorkSheet4.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["H5"].Value = "Actual End Date";
                        exWorkSheet4.Cells["H5"].AutoFitColumns(20);

                        using (ExcelRange col = exWorkSheet4.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                    }

                    #endregion

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    FileName = FileName.Replace(',', ' ');
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.BinaryWrite(fileBytes);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity4.Items.Count > 0)
                {
                    ddlSubEntity4.Items.Clear();
                    ddlSubEntity4.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindVertical();
            BindData();
            BindSchedulingType();
            bindPageNumber();
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity4.Items.Count > 0)
                {
                    ddlSubEntity4.Items.Clear();
                    ddlSubEntity4.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindVertical();
            BindData();
            BindSchedulingType();
            bindPageNumber();
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity4.Items.Count > 0)
                {
                    ddlSubEntity4.Items.Clear();
                    ddlSubEntity4.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindVertical();
            BindData();
            BindSchedulingType();
            bindPageNumber();
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlSubEntity4.Items.Count > 0)
                {
                    ddlSubEntity4.Items.Clear();
                    ddlSubEntity4.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindVertical();
            BindData();
            BindSchedulingType();
            bindPageNumber();
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVertical();
            BindData();
            BindSchedulingType();
            bindPageNumber();
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;
                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }
                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count); BindData();
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}