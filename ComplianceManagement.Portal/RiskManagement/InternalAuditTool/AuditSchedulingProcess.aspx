﻿<%@ Page Title="Audit Scheduling" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditSchedulingProcess.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditSchedulingProcess" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
            border-style: ridge;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .alert {
            padding: 15px 15px 15px 40px;
            margin-bottom: 20px;
            border: 1px solid transparent;
        }

        .alignCenter {
            text-align: center;
        }

        div#ContentPlaceHolder1_ddlProcessList_dv {
            margin-top: 11px;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .dd_chk_select {
            height: 30px !important;
            text-align: center;
            border-radius: 5px;
        }

        div.dd_chk_select div#caption {
            margin-top: 5px;
            top: -5px !important;
        }

        td.locked, th.locked {
            position: relative;
            left: expression((this.parentElement.parentElement.parentElement.parentElement.scrollLeft-2)+'px');
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#ContentPlaceHolder1_ddlProcessList_sll").click(function () {
                if (this.checked) {
                    $('.ContentPlaceHolder1_ddlProcessList').each(function () {
                        $(".checkboxall").prop('checked', true);
                    })
                } else {
                    $('.ContentPlaceHolder1_ddlProcessList').each(function () {
                        $(".checkboxall").prop('checked', false);
                    })
                }
            });
        });

        function fopenpopupAudit() {
            $('#divAuditSchedulingDialog').modal('show');
        }

        function fopenpopupCompl() {
            $('#divComplianceScheduleDialog').modal('show');
        }

        function fopenpopupEditaudit() {
            $('#divEDITAuditSchedulingDialog').modal('show');
        }



        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function OpenApplytoPopUp() {
            $('#DivApplyTo').modal('show');
        }

        function ApplyToconfirm() {
            var confirmed = confirm('Are you sure you want to Apply this item?', 'Are you sure you want to Apply this item?');

            if (confirmed) {
                $('#btnapply').show();
            }
            else {
                caller();
            }
        }

        function CloseEvent() {
            $('#divAuditSchedulingDialog').modal('hide');
            window.location.reload();
        }

        function CloseWin() {
            $('#divAuditSchedulingDialog').modal('hide');
            window.location.reload();
        };

        //This is used for Close Popup after button click.
        function caller() {
            interval_A = setInterval(CloseWin, 3000);
        };

        function CloseWin1() {
            $('#DivApplyTo').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller1() {
            interval_A = setInterval(CloseWin1, 3000);
        };
    </script>
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        function func() {
            var gvcheck = document.getElementById("<%=grdAnnually.ClientID %>");
            if (ContentPlaceHolder1_grdAnnually_Annually1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for Annulay Cheakbox
        function funHalf1() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalf2() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for Quarterly

        function funcQuarterly1() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterly_Quarterly1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcQuarterly2() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterly_Quarterly2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcQuarterly3() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterly_Quarterly3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcQuarterly4() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterly_Quarterly4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }



        //for monthly 
        function funMonthly1() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly2() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly3() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly4() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly5() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly6() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly7() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly7_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly8() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly8_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly9() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly9_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly10() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly10_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly11() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly11_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly12() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly12_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }



        //for edit annuly

        function Editfunc() {
            var gvcheck = document.getElementById("<%=grdAuditSchedulingEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdAuditSchedulingEDIT_EditAnnually1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function CheckEditSchedule() {
            var gvcheck = document.getElementById("<%=grdAuditSchedulingEDIT.ClientID %>");
            var rowcount = 0;
            for (i = 0; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked) {
                    rowcount++;
                }
            }
            if (gvcheck.rows.length - 2 == rowcount) {
                ContentPlaceHolder1_grdAuditSchedulingEDIT_EditAnnually1_Id_0.checked = true;
            }
            else {
                ContentPlaceHolder1_grdAuditSchedulingEDIT_EditAnnually1_Id_0.checked = false;
            }
        }

        //for phase1
        function funcphase1() {
            var gvcheck = document.getElementById("<%=grdphase1.ClientID %>");
            if (ContentPlaceHolder1_grdphase1_Phase1_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for pahse 2

        function funcphase21() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcphase22() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        //for phase 3
        function funcphase31() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcphase32() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase33() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        //for phase 4

        function funcphase41() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
            if (ContentPlaceHolder1_grdphase4_Phase4_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcphase42() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
            if (ContentPlaceHolder1_grdphase4_Phase4_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcphase43() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
            if (ContentPlaceHolder1_grdphase4_Phase4_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase44() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
            if (ContentPlaceHolder1_grdphase4_Phase4_Phase4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        //for pahse 5
        function funcphase51() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase52() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase53() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase54() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase55() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }
        function BindControls() {

            $(function () {
                $('input[id*=txtExpectedStartDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });

            $(function () {
                $('input[id*=txtExpectedEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });


            $(function () {
                $('input[id*=txtStartDatePop]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
            $(function () {
                $('input[id*=txtEndDatePop]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Audit Scheduling Process');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                            <div class="col-md-12 colpadding0">
                                 <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                                      ValidationGroup="ComplianceInstanceValidationGroup" />                                  
                                       <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                          ValidationGroup="ComplianceInstanceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>

                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                      </div> 

                                     <div class="col-md-5 colpadding0 entrycount" style="margin-top: 5px;">
                                      </div> 

                                  <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; float:right">
                                       <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" CssClass="btn btn-primary" 
                                           OnClientClick="fopenpopupAudit()"  OnClick="btnAddCompliance_Click" Visible="true" />
                                   </div>  
                              </div>
                             <div class="clearfix"></div> 
                             <div class="col-md-12 colpadding0"> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15"
                                         Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                     DataPlaceHolder="Unit"   OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator1"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                                    
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                     DataPlaceHolder="Sub Unit"    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                         OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlSubEntity1" ID="rfvProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                      DataPlaceHolder="Sub Unit"   AllowSingleDeselect="false" DisableSearchThreshold="3"
                                         OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubEntity2" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                       DataPlaceHolder="Sub Unit"   AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                          OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>
                            </div>                  
                            <div class="col-md-12 colpadding0"> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true"
                                         class="form-control m-bot15" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>                                  
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; display:none; ">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;</label>
                                                <asp1:DropDownCheckBoxes ID="ddlProcessListMain" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlProcessList_SelectedIndexChanged"
                                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Select Process" />
                                                </asp1:DropDownCheckBoxes>
                                            </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                  DataPlaceHolder="Financial Year"  AutoPostBack="true" 
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>   
                                <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                <%{%>  
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true" 
                                            OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged"
                                             AllowSingleDeselect="false" DisableSearchThreshold="3"
                                         class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>
                                 <%}%>
                            </div>                                                                                                                              
                                <div class="clearfix"></div>                                                           
                        <div class="clearfix"></div>
                        <%--<div style="margin-bottom: 4px">--%>
                            &nbsp
                            <asp:GridView runat="server" ID="grdAuditScheduling" AutoGenerateColumns="false" OnRowDataBound="grdAuditScheduling_RowDataBound" ShowHeaderWhenEmpty="true"
                                OnSorting="grdAuditScheduling_Sorting" GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                OnPageIndexChanging="grdAuditScheduling_PageIndexChanging" OnRowCommand="grdAuditScheduling_RowCommand1">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label runat="server" Text='<%# Eval("CustomerBranchName") %>' ></asp:Label>                                              
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField  HeaderText="Vertical">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssigned" runat="server" Text='<%# Eval("VerticalName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Financial Year">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">                                                
                                                <asp:Label runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Scheduling">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label runat="server" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label runat="server" Text='<%# Eval("TermName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="Action">
                                        <ItemTemplate>                                        
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" OnClientClick="fopenpopupCompl()" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName")+","+ Eval("VerticalID")+","+ Eval("AuditID") %>'><img src="../../Images/view-icon-new.png" data-toggle="tooltip" data-placement="top"  title="Display Schedule Information" /></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_SCHEDULE" OnClientClick="fopenpopupEditaudit()" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") +","+ Eval("PhaseCount")+","+ Eval("VerticalID")+","+ Eval("AuditID") %>'><img src="../../Images/edit_icon_new.png" data-toggle="tooltip" data-placement="top"  title="Edit Schedule Information" /></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SCHEDULE" ValidationGroup="ComplianceValidationGroup1" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName")+","+ Eval("VerticalID")+","+ Eval("AuditID") %>' OnClientClick="return confirm('Are you certain you want to delete this Scheduling Details?');"><img data-toggle="tooltip" data-placement="top"  src="../../Images/delete_icon_new.png" alt="Delete Scheduling Details" title="Delete Scheduling Details" /></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                   <PagerSettings Visible="false" />       
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                    </div>
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-5 colpadding0">
                            <div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 colpadding0" style="float: right;">
                            <div class="table-paging" style="margin-bottom: 10px;">
                                <div class="table-paging-text" style="float: right;">
                                    <p>
                                        Page
                                    </p>
                                </div>
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                    </div>
                    </section>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <div class="modal fade" id="divAuditSchedulingDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 98%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="divAuditorDialog">
                            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
                                <ContentTemplate>
                                    <div>
                                        <div style="margin-bottom: 4px; margin-left: 10px;">
                                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="PopupValidationSummary" />
                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False" Style="margin-left: 10px;"
                                                ValidationGroup="PopupValidationSummary" Display="none" class="alert alert-block alert-danger fade in" />
                                            <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                        </div>

                                        <div class="col-md-12 colpadding0">
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen ID="ddlLegalEntityPop" runat="server" AutoPostBack="true" DataPlaceHolder="Unit"
                                                    class="form-control m-bot15" Width="90%" Height="32px"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    OnSelectedIndexChanged="ddlLegalEntityPop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator2"
                                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen ID="ddlSubEntity1Pop" runat="server" AutoPostBack="true"
                                                    class="form-control m-bot15" DataPlaceHolder="Sub Unit"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    Width="90%" Height="32px" OnSelectedIndexChanged="ddlSubEntity1Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlSubEntity1" ID="RequiredFieldValidator3"
                                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen ID="ddlSubEntity2Pop" runat="server" AutoPostBack="true" class="form-control m-bot15"
                                                    Width="90%" Height="32px" DataPlaceHolder="Sub Unit"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    OnSelectedIndexChanged="ddlSubEntity2Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubEntity2" ID="RequiredFieldValidator4"
                                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Pop" AutoPostBack="true"
                                                    class="form-control m-bot15" DataPlaceHolder="Sub Unit"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    Width="90%" Height="32px" OnSelectedIndexChanged="ddlSubEntity3Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:ValidationSummary ID="ValidationSummary5" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                                <asp:CustomValidator ID="CustomValidator5" runat="server" EnableClientScript="true"
                                                    ValidationGroup="ComplianceInstanceValidationGroup" />
                                                <asp:Label ID="Label3" runat="server" Style="color: Red"></asp:Label>
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity4Pop" AutoPostBack="true"
                                                    DataPlaceHolder="Sub Unit"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    class="form-control m-bot15" Width="90%" Height="32px" OnSelectedIndexChanged="ddlSubEntity4Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                            <%{%>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlVerticalListPopup" DataPlaceHolder="Verticals"
                                                    AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <%}%>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp1:DropDownCheckBoxes ID="ddlProcessList" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlProcessList_SelectedIndexChanged"
                                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="230" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Select Process" />
                                                </asp1:DropDownCheckBoxes>
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                                    OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" AutoPostBack="true"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    DataPlaceHolder="Financial Year">
                                                </asp:DropDownListChosen>
                                                <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Financial Year."
                                                    ControlToValidate="ddlFinancialYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" DataPlaceHolder="Scheduling Type"
                                                    OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                    class="form-control m-bot15" Width="90%" Height="32px">
                                                </asp:DropDownListChosen>
                                                <asp:CompareValidator ID="CompareValidator16" ErrorMessage="Please Select Scheduling Type."
                                                    ControlToValidate="ddlSchedulingType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *
                                                </label>
                                                <asp:TextBox runat="server" ID="txtStartDatePop" AutoComplete="Off" class="form-control" Style="width: 90%;" placeholder="Start Date" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Actual Start Date can not be empty."
                                                    ControlToValidate="txtStartDatePop"
                                                    runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *
                                                </label>
                                                <asp:TextBox runat="server" ID="txtEndDatePop" AutoComplete="Off" class="form-control" Style="width: 90%" placeholder="End Date" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Expected End Date can not be empty."
                                                    ControlToValidate="txtEndDatePop"
                                                    runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                                <%--<asp:CompareValidator ID="CompareValidator1" ControlToCompare="txtStartDatePop" ControlToValidate="txtEndDatePop" 
                                                    Type="Date" Operator="GreaterThanEqual" ValidationGroup="PopupValidationSummary"
                                                     ErrorMessage="ToDate should be greater than FromDate" runat="server"></asp:CompareValidator>--%>
                                            </div>
                                            <div id="Divnophase" class="col-md-3 colpadding0 entrycount" runat="server" visible="false" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:TextBox runat="server" ID="txtNoOfPhases" PlaceHolder="Nos. of Phases" class="form-control" Style="width: 90%;" AutoPostBack="true" OnTextChanged="txtNoOfPhases_TextChanged" />
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <div style="margin-bottom: 7px">
                                            <span style="color: #333; display: none">Schedule Details</span>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div style="margin-left: 20px; background: white; border: 1px solid gray; width: 95%; min-height: 200px !important; margin-bottom: 7px;">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="a1">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlAnnually" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdAnnually" AutoGenerateColumns="false" OnRowDataBound="grdAnnually_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            DataKeyNames="ProcessID" OnPageIndexChanging="grdAnnually_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>' Width="120px" ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' Width="180px" data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Annually">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkAnnualy1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlHalfYearly" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdHalfYearly" AutoGenerateColumns="false"
                                                            GridLines="None" AutoPostBack="true" CssClass="table"
                                                            Width="100%" AllowSorting="true" OnRowDataBound="grdHalfYearly_RowDataBound"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdHalfYearly_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 350px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Sep">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel3">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlQuarterly" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdQuarterly" AutoGenerateColumns="false" OnRowDataBound="grdQuarterly_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdQuarterly_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Jun">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkQuarter1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul-Sep">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkQuarter2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Dec">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkQuarter3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan-Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkQuarter4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel4">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlMonthly" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdMonthly" AutoGenerateColumns="false" OnRowDataBound="grdMonthly_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdMonthly_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="May">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jun">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aug">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sep">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Nov">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Dec">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly10" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feb">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly11" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkMonthly12" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel5">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlphase1" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase1" AutoGenerateColumns="false" OnRowDataBound="grdphase1_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdphase1_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel6">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlphase2" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase2" AutoGenerateColumns="false" OnRowDataBound="grdphase2_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdphase2_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 350px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel7">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlphase3" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase3" AutoGenerateColumns="false" OnRowDataBound="grdphase3_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdphase3_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel8">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlphase4" ScrollBars="Auto" Visible="false" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase4" AutoGenerateColumns="false" OnRowDataBound="grdphase4_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdphase4_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel9">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlphase5" ScrollBars="Auto" Style="overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase5" AutoGenerateColumns="false" OnRowDataBound="grdphase5_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ProcessID" OnPageIndexChanging="grdphase5_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcess">
                                                                    <ItemTemplate>
                                                                        <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase5">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkPhase5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="AuditID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <HeaderStyle BackColor="#ECF0F1" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div style="margin-bottom: 10px; text-align: center; margin-top: 25px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PopupValidationSummary" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        <asp:Button Text="Apply To" Visible="false" runat="server" ID="btnapply" CssClass="btn btn-primary" OnClick="btnapply_Click" OnClientClick="OpenApplytoPopUp();" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="modal fade" id="divComplianceScheduleDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 98%;">
                <div class="modal-content" style="min-height: 400px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
                            <ContentTemplate>
                                <div style="margin: 5px">
                                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 4px">

                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="ComplianceValidationGroupView" />
                                                <asp:CustomValidator ID="cvDuplicateEntry23" runat="server" EnableClientScript="False"
                                                    ValidationGroup="ComplianceValidationGroupView" Display="None" class="alert alert-block alert-danger fade in" />
                                                <asp:Label ID="Label2" runat="server" Style="color: Red"></asp:Label>

                                            </div>
                                            <div runat="server" style="margin-bottom: 5px; padding-left: 10px; height: 275px; overflow-y: auto;">
                                                <asp:GridView runat="server" ID="grdAuditScheduleStartEndDate" AutoGenerateColumns="false" GridLines="None"
                                                    AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" DataKeyNames="Id">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                                <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location"  ItemStyle-Width="150px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                                    <asp:Label runat="server" Text='<%# Eval("BranchName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vertical"  ItemStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                                    <asp:Label runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Period"  ItemStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px">
                                                                    <asp:Label runat="server" Text='<%# Eval("TermName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Financial Year"  ItemStyle-Width="120px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                    <asp:Label runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Process">
                                                            <ItemTemplate>
                                                                <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                    <asp:Label ID="lblProcess"  runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SubProcess">
                                                            <ItemTemplate>
                                                                <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                    <asp:Label ID="lblSubProcess"  runat="server" Text='<%# Eval("SubProcess") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcess") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-Width="120px">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtExpectedStartDate" runat="server" CssClass="form-control" Style="text-align: center;"
                                                                    Text='<%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MM-yyyy"):""%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="End Date" ItemStyle-Width="120px">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtExpectedEndDate" runat="server" CssClass="form-control" Style="text-align: center;"
                                                                    Text='<%# Eval("EndDate")!= null?((DateTime)Eval("EndDate")).ToString("dd-MM-yyyy"):""%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <HeaderStyle BackColor="#ECF0F1" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </div>

                                            <div style="margin-bottom: 7px; text-align: center;">
                                                <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click" CssClass="btn btn-primary" />
                                                <asp:Button Text="Close" runat="server" ID="Button2" CssClass="btn btn-primary" data-dismiss="modal" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="modal fade" id="divEDITAuditSchedulingDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 830px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title">Edit Audit Schedule</h4>
                    </div>
                    <div class="modal-body">
                        <div id="divAuditorDialog1">
                            <asp:UpdatePanel ID="upEDITComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
                                <ContentTemplate>
                                    <div>
                                        <div style="margin-bottom: 4px">
                                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="ComplianceValidationGroup" />
                                            <asp:CustomValidator ID="cvDuplicateEntry50" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            <asp:Label runat="server" ID="lblErrorMassage1" ForeColor="Red"></asp:Label>
                                        </div>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 15%">
                                                    <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Location</label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblEDITLocation" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                <td style="width: 15%">
                                                    <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Financial Year</label></td>
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblEDITFinancialyear" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%">
                                                    <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Scheduling Type</label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblEDITSchedulingType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Vertical</label>
                                                    <asp:Label ID="lblEDITTermName" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblVerticalName" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblEDITLocationID" runat="server" Text="" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblEDITVerticalID" runat="server" Text="" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblEDITPhaseCount" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Start Date</label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEDITStartDate" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        End Date</label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEDITEndDate" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                        <div style="background: white; border: 1px solid gray; height: 355px; margin-bottom: 7px;">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel2">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlAuditSchedulingEDIT" ScrollBars="Auto" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdAuditSchedulingEDIT" AutoGenerateColumns="false"
                                                            OnRowDataBound="grdAuditSchedulingEDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" ShowHeader="true" DataKeyNames="ProcessID" OnPageIndexChanging="grdAuditSchedulingEDIT_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ProcessID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ProcessName">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcessName">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Annually">
                                                                    <ItemTemplate>

                                                                        <asp:CheckBox ID="chkAnnualy1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SubProcessID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubProcessID" runat="server" Text='<%# Eval("SubProcessID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="margin-bottom: 10px; text-align: center; margin-top: 10px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <asp:Button Text="Save" runat="server" ID="btnEDITSave" OnClick="btnEDITSave_Click"
                                                CssClass="btn btn-primary" ValidationGroup="ComplianceValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="Button3" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                        <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                        </div>
                                        <div class="clearfix" style="height: 20px">
                                        </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnEDITSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="DivApplyTo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 500px; margin-top: 70px;">
            <div class="modal-content">
                <div>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                        ValidationGroup="ComplianceInstanceValidationGroup1" />
                    <asp:CustomValidator ID="cvDuplicateEntryApplyPop" runat="server" EnableClientScript="true"
                        ValidationGroup="ComplianceInstanceValidationGroup1" />
                    <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                </div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body" style="height: 200px;">
                    <div style="margin-bottom: 7px">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateApplyToPopUp">
                            <ContentTemplate>
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 110px; display: block; float: left; font-size: 13px; color: #333;">
                                    Select Location</label>
                                <asp1:DropDownCheckBoxes ID="ddlBranchList" runat="server" AutoPostBack="true" Visible="true"
                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True" Style="padding: 0px; margin: 0px; width: 300px; height: 80px;">
                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="250" />
                                    <Texts SelectBoxCaption="Select Branch" />
                                </asp1:DropDownCheckBoxes>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                <div align="center" style="margin-bottom: 7px; margin-left: 20px; margin-top: 10px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Button Text="Save" runat="server" ID="btnSaveApplyto" OnClick="btnSaveApplyto_Click" CssClass="btn btn-primary" />
                                    <asp:Button Text="Close" runat="server" ID="Button4" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
