﻿<%@ Page Title="Audit Upload" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="PastAuditUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.PastAuditUpload" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
    </style>


    <script type="text/javascript">
        //function fopenpopup() {
        //    $('#divAuditFrequencyDialog').modal('show');
        //};
        function CloseEvent() {
            $('#divreports').modal('hide');
            window.location.reload();
        }
        function fopenVersionpopup() {
            $('#divVersionDialog').modal('show');
        };
        function CloseVersionEvent() {
            $('#divVersionDialog').modal('hide');
            window.location.reload();
        }

        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        });
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 15);
        window.onunload = function () { null };

        $(document).ready(function () {
            //setactivemenu('Observation Upload');
            fhead('Past Audit Reports');
        });

        function PopulateVerticalMappingdata(PastAuditDocID,Type) {
            $('#divreports').modal('show');
            $('#showdetails').attr('width', '95%');
            $('#showdetails').attr('height', '555px');
            $('.modal-dialog').css('width', '90%');
            $('#showdetails').attr('src', "../../RiskManagement/InternalAuditTool/UploadingPastAuditDocument.aspx?PastAuditDocID=" + PastAuditDocID + "&Type=" + Type);
        }
    </script>

    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 20%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 20%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
         #ContentPlaceHolder1_ValidationSummary1 ul li {
            margin-left:10px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">   
                             <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary alert alert-block alert-danger fade in" ForeColor="Red" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red" ></asp:Label> 
                                <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="true"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                              </div>           
                                 <div class="col-md-12 colpadding0">
                                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0" style="width: 30%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                                   <% if (AuditHeadOrManagerReport == null)%>
                                         <%{%>
                                               <%if (!DepartmentHead)%>
                                               <%{%>
                                                  <%if (!ManagementFlag)%>
                                                  <%{%>           
                                                        <%--<ul>   --%>                            
                                                        <li style="float: right;">   
                                                            <div style="margin-right: 25px; margin-top:-21px; padding-left: 15px;">                                                        
                                                                    <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                                    <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                    <ul class="dropdown-menu" style="margin-left: 85%;">                                                                        
                                                                        <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation-Excel</a></li>
                                                                        <li><a href="../InternalAuditTool/SchedulingReports.aspx">Audit Scheduling</a></li>
                                                                        <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li> 
                                                                        <li><a href="../AuditTool/ManagementResponseLogReport.aspx">Management Response Log Report</a></li>                                                                                                                                                                        
                                                                    </ul>                                                       
                                                            </div>
                                                        </li> 
                                                            <%--</ul> --%>
                                                    <%}%> 
                                              <%}%> 
                                        <%}%> 
                                <div style="text-align:right;margin-right: 2.5%;">
                                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddPromotor" CssClass="btn btn-primary"
                                         OnClientClick="PopulateVerticalMappingdata(0,1)" OnClick="btnAddPromotor_Click" /></td>
                                </div>
                              </div>
                            <div class="clearfix"></div>   
                              <div class="col-md-12 colpadding0">                                  
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                  <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                  <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div> 
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>
                              </div>
                           <div class="clearfix"></div>    
                            <div class="col-md-12 colpadding0">                                  
                                 <div class="col-md-3 colpadding0" style="margin-top: 5px;">                        
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" class="form-control m-bot15" Width="90%" Height="32px"
                                    AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" DataPlaceHolder="Locations">
                                    </asp:DropDownListChosen>
                                </div> 
                                 <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                 <%{%> 
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">                                                            
                                        <asp:DropDownListChosen runat="server" ID="ddlVerticalID"  AutoPostBack="true" Width="90%" Height="32px"
                                      DataPlaceHolder="Verticals"  class="form-control m-bot15"   
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged"></asp:DropDownListChosen>                      
                                    </div>
                                   <%}%>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">  
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true"  Width="90%" Height="32px" 
                                    DataPlaceHolder="Financial Year"   class="form-control m-bot15" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged"></asp:DropDownListChosen>

                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  
                                            ErrorMessage="Select Financial Year" ForeColor="Red" InitialValue=""
                                            Font-Size="0.9em" ControlToValidate="ddlFilterFinancial" 
                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None"></asp:RequiredFieldValidator>

                                            <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                                                      
                                 </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; "> 
                                      <asp:DropDownListChosen ID="ddlAuditBackground" runat="server" class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlAuditBackground_SelectedIndexChanged"
                                         DataPlaceHolder="Audit Background"></asp:DropDownListChosen>
                                  </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; "> 
                                    <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50" PlaceHolder="Type to Search" AutoPostBack="true" Width="90%" OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                            </div>
                            <div class="clearfix"></div>                              
                            <div class="clearfix"></div>                                                     
                            <div class="clearfix"></div>                                   
                            <div style="margin-bottom: 4px">   
                                &nbsp;                
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" PageSize="20" AllowPaging="true" 
                                    CssClass="table" GridLines="none" Width="100%"  AllowSorting="true" ShowHeaderWhenEmpty="true"
                                     OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging" OnRowCommand="grdComplianceRoleMatrix_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.no">
                                        <ItemTemplate>
                                           <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField  Visible="false">
                                            <ItemTemplate>                                                                                                
                                                <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranch")%>'></asp:Label>                                                 
                                                  <asp:Label ID="lblVerticalID" runat="server" Text='<%# Eval("VerticalID")%>'></asp:Label>                                      
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>                                     
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("CustomerBrachName")%>' ToolTip='<%# Eval("CustomerBrachName") %>'></asp:Label>                                            
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vertical" Visible="false">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("VerticalName")%>'  ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Financial&nbsp;Year">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblFinancialYear" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear")%>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Audit Background">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblAuditBackground" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditBackground")%>' ToolTip='<%# Eval("AuditBackground") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <asp:TemplateField  HeaderText="Action">
                                            <ItemTemplate>     
                                                   <asp:LinkButton ID="lblDownLoadfile"  runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("AuditDocumentID") %>' 
                                                       data-toggle="tooltip" data-placement="top" ToolTip="View Documents"
                                                    CausesValidation="false"><img src="../../Images/View-icon-new.png" /></asp:LinkButton> 
                                                    <asp:LinkButton ID="lblEditFile"  runat="server" CommandName="EditAuditStatusSummary" CommandArgument='<%# Eval("AuditDocumentID") %>' 
                                                       data-toggle="tooltip" data-placement="top" ToolTip="Edit Documents"
                                                    CausesValidation="false"><img src="../../Images/edit_icon_new.png" /></asp:LinkButton> 
                                                 <% if (RoleId == 8)%>
                                                        <% { %>
                                                    <asp:LinkButton ID="lblDeletefile"  runat="server" CommandName="DeleteAuditStatusSummary" OnClientClick="return confirm('Are you certain you want to delete this Audit Details?');" CommandArgument='<%# Eval("AuditDocumentID") %>' 
                                                       data-toggle="tooltip" data-placement="top" ToolTip="Delete Document" Visible="true"
                                                    CausesValidation="false">  <img src="../../Images/delete_icon_new.png" /></asp:LinkButton> 
                                                        <% } %> 
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <HeaderStyle BackColor="#ECF0F1" />
                                     <PagerSettings Visible="false" />       
                                    <PagerTemplate>
                                            <%--<table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>--%>
                                        </PagerTemplate>
                                    <EmptyDataTemplate>
                                    No Records Found.
                                 </EmptyDataTemplate>                               
                                </asp:GridView>
                                        <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                        
                                        <div class="table-paging-text" style="float: right;">
                                            <p>
                                                Page                                              
                                            </p>
                                        </div>                                        
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                       
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlFilterFinancial" />
            <asp:PostBackTrigger ControlID="ddlFilterLocation" />
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseEvent()" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="showdetails" src="about:blank" width="1150px" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
