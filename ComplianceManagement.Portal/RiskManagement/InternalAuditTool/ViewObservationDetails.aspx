﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AuditTool.Master" CodeBehind="ViewObservationDetails.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.ViewObservationDetails" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width: 95% !important;
        }

        div.dd_chk_drop {
            background-color: white;
            border: 1px solid #CCCCCC;
            text-align: left;
            z-index: 1000;
            left: -1px;
            min-width: 100%;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .MarginTop {
            margin-top: 13px;
        }

        .header_alignment {
            text-align: center !important;
        }
    </style>

    <style type="text/css">
        ul.multiselect-container.dropdown-menu {
            width: 100%;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }
    </style>


    <style type="text/css">
        .three-inline-buttons .button {
            margin-left: 2px;
            margin-right: 12px;
        }

        .three-inline-buttons {
            display: table;
            margin: 0 auto;
        }

        @media only screen and (max-width: 960px) {
            .three-inline-buttons .button {
                width: 100%;
                margin: 20px;
                text-align: center;
            }
        }
    </style>



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upViewObservation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div style="margin-bottom: 7px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="EntitiesValidationGroup" />
                        <asp:CustomValidator ID="CvViewObservation" runat="server" EnableClientScript="False"
                            ValidationGroup="ViewObservationValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        <asp:Label runat="server" ID="lblerrormsgMainscreen" Style="color: Red"></asp:Label>
                    </div>
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                               <div class="col-md-12 colpadding0">               
                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-1 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" Selected="True" />
                                        </asp:DropDownList>
                                    </div>                                                                                                        
                               </div>
                             <div class="clearfix"></div>                                          
                            <div class="col-md-12 colpadding0">
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;">
                                   <asp:DropDownCheckBoxes ID="ddlProcess" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged"
                                                 AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 95%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                   </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                          <asp:DropDownCheckBoxes ID="ddlSubProcess" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer" DropDownBoxCssClass="MarginTop"/>
                                            <Texts SelectBoxCaption="Select Sub Process" />
                                        </asp:DropDownCheckBoxes>                                                                 
                                </div>

                                <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                 <%{%>
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                          <asp:DropDownCheckBoxes ID="ddlVertical" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer" DropDownBoxCssClass="MarginTop"/>
                                            <Texts SelectBoxCaption="Select Vertical" />
                                        </asp:DropDownCheckBoxes>                                                                 
                                </div>
                                <%}%>

                                 <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                      <asp:TextBox ID="txtAuditStepFilter" runat="server" CssClass="form-control" MaxLength="50" autocomplete="off"
                                          PlaceHolder="Type to Search" AutoPostBack="true" Width="200px" OnTextChanged="txtAuditStepFilter_TextChanged"></asp:TextBox>                                                  
                                </div>
                            </div>          
                                                              
                             <div style="margin-top:50px">                                     
                                <asp:GridView runat="server" ID="grdViewObservations" AutoGenerateColumns="false" GridLines="None"
                                PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table"  Width="100%" ShowHeaderWhenEmpty="true" OnRowCommand="grdViewObservations_RowCommand"
                                AllowSorting="true">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AuditID" Visible="false">
                                        <ItemTemplate>                                        
                                            <asp:Label runat="server" ID="lblProcessId"  Style="display:none;" Text='<%#Eval("ProcessId") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblSubProcessId" data-toggle="tooltip" data-placement="top"  Text='<%# Eval("SubProcessId") %>' ToolTip='<%# Eval("SubProcessId") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblAuditStepId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditStepId") %>' ToolTip='<%# Eval("AuditStepId") %>'></asp:Label>
                                            <%--<asp:Label runat="server" ID="lblAuditID"  Style="display:none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblATBDId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ATBDId") %>' ToolTip='<%# Eval("ATBDId") %>'></asp:Label>
                                            <asp:Label  runat="server" ID="lblAuditStepId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditStepId") %>' ToolTip='<%# Eval("AuditStepId") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("Process") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Process") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>                           
                                    <asp:TemplateField HeaderText="Sub Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width: 100px;">
                                                <asp:Label ID="lblSubProcess" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SubProcess") %>' ToolTip='<%# Eval("SubProcess") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    
                                     <asp:TemplateField HeaderText="Audit Step">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 500px;"> 
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width: 460px;float:left;">
                                                <asp:Label ID="lblAuditStep" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditStep") %>' ToolTip='<%# Eval("AuditStep") %>' Width="460px"></asp:Label>
                                                
                                            </div>
                                            <div style="float:right;">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="View"
                                                    CommandArgument='<%# Eval("AuditStepId") %>'>
                                                    <img src="../../Images/Info_icon_obs.png" alt="View Audit Step" title="View Audit Step" data-toggle="tooltip" style="float:right;" />
                                                </asp:LinkButton>
                                                </div>
                                                </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>       
                                    
                                    <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="header_alignment" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 50px;">
                                
                                </div>
                            </ItemTemplate>  
                                    </asp:TemplateField> 
                                    
                                    <asp:TemplateField HeaderText="Observation Count" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_alignment">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <%--<asp:Label  ID="lblObservationCount" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ObservationCount") %>' ToolTip='<%# Eval("ObservationCount") %>'></asp:Label>--%>
                                                <%--<asp:LinkButton ID="lnkObservationCount" runat="server" CommandName="ObservationPopUp" 
                                                    CommandArgument='<%# Eval("ProcessId") +"," +Eval("SubProcessId")+"," + Eval("AuditID")+"," +  Eval("ATBDId")+","+ Eval("AuditStepId") %>' Text='<%# Eval("ObservationCount") %>' ></asp:LinkButton>--%>
                                                <asp:LinkButton ID="lnkObservationCount" runat="server" CommandName="ObservationPopUp" 
                                                    CommandArgument='<%# Eval("ProcessId") +"," +Eval("SubProcessId")+"," + Eval("AuditStepId") %>' Text='<%# Eval("obscount") %>' ></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>  
                                    
                                  <asp:TemplateField HeaderText="Action" ItemStyle-Width="7%" Visible="false">
                                    <%--<ItemTemplate>                                                               
                                        
                                        <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Entities"  CommandArgument='<%# Eval("BranchID") + "," + Eval("DepartmentID")+ "," + Eval("ProcessId")+ "," + Eval("ISDepartmentOrAuditHead") +","+ Eval("userID") %>'
                                            OnClientClick="return confirm('Are you certain you want to delete?');"><img src="../../Images/delete_icon_new.png" alt="Delete Process" title="Delete Process" data-toggle="tooltip"/></asp:LinkButton>                                
                                    </ItemTemplate>--%>                            
                                </asp:TemplateField>                                                               
                                </Columns>
                                    <RowStyle CssClass="clsROWgrid"   />
                                    <HeaderStyle CssClass="clsheadergrid" />  
                                    <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerSettings Visible="false" />                  
                                    <PagerTemplate>                                       
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found.
                                    </EmptyDataTemplate>   
                                </asp:GridView>
                                <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
<%--                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                            </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            fhead('View Observations');
        });
    </script>

    <div class="modal fade" id="ViewAuditStepOb" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 43%;">
            <div class="modal-content" style="height: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size: 15px;">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div id="viewAudit">
                        <asp:UpdatePanel ID="upViewAuditStep" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-md-12">
                                    <label style="width: 100%; display: block; float: left; font-size: 15px; color: black;">Audit Step</label>
                                </div>
                                <div class="col-md-12" style="margin-top:8%;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Label ID="lblviewAuditStep" runat="server" Text="" Style="width: 100%; display: block; float: left; font-size: 15px; color: #666; margin-top: -7%;"></asp:Label>
                                </div>
                                <div style="margin-bottom: 7px; margin-top: 8%;text-align: center;">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnClose" aria-hidden="true">Close</button>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divPREDOCShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"  data-dismiss="modal" aria-hidden="true" style="font-size:26px">&times;</button>
                </div>
                <div class="modal-body" style="background:white !important;">
                    <iframe id="DetObsshowdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 25px;background:white;"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ViewObservationDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 43%;">
            <div class="modal-content" style="height: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div id="viewObservationDetails">
                        <asp:UpdatePanel ID="upObservationDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: black;">Audit Step: </label>
                                <asp:Label ID="Label1" runat="server" Text="" Style="width: 100%; display: block; float: left; font-size: 15px; color: black; margin-top: -7%;"></asp:Label>
                                <div style="margin-bottom: 7px; margin-left: 228px; margin-top: 20px">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnClose" aria-hidden="true">Close</button>
                                </div>

                                <div style="margin-top: 50px">
                                    <asp:GridView runat="server" ID="grdDetailedObs" AutoGenerateColumns="false" GridLines="None"
                                        PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowHeaderWhenEmpty="true"
                                        AllowSorting="true">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AuditID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAuditID" Style="display: none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblATBDId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ATBDId") %>' ToolTip='<%# Eval("ATBDId") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblAuditStepId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditStepId") %>' ToolTip='<%# Eval("AuditStepId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observation Title">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                            <asp:Label ID="lblObservationTitle" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ObservationTitle") %>' ToolTip='<%# Eval("ObservationTitle") %>' Width="200px"></asp:Label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detailed Observation">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                            <asp:Label ID="lblDetailedObservation" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Observation") %>' ToolTip='<%# Eval("Observation") %>' Width="200px"></asp:Label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="header_alignment" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <div class="text_NlinesusingCSS" style="width: 50px;">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="View"
                                                            CommandArgument='<%# Eval("AuditStep") %>'><img src="../../Images/view-icon-new.png" alt="View Audit Step" title="View Audit Step" data-toggle="tooltip" /></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Repeated Counts" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_alignment">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <%--<asp:Label  ID="lblObservationCount" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ObservationCount") %>' ToolTip='<%# Eval("ObservationCount") %>'></asp:Label>--%>
                                                        <%--<asp:LinkButton ID="lnkObservationCount" runat="server" CommandName="ObservationPopUp" 
                                                    CommandArgument='<%# Eval("ProcessId") +"," +Eval("SubProcessId")+"," + Eval("AuditID")+"," +  Eval("ATBDId")+","+ Eval("AuditStepId") %>' Text='<%# Eval("ObservationCount") %>' ></asp:LinkButton>--%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="7%" Visible="false">
                                                <%--<ItemTemplate>                                                               
                                        
                                        <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Entities"  CommandArgument='<%# Eval("BranchID") + "," + Eval("DepartmentID")+ "," + Eval("ProcessId")+ "," + Eval("ISDepartmentOrAuditHead") +","+ Eval("userID") %>'
                                            OnClientClick="return confirm('Are you certain you want to delete?');"><img src="../../Images/delete_icon_new.png" alt="Delete Process" title="Delete Process" data-toggle="tooltip"/></asp:LinkButton>                                
                                    </ItemTemplate>--%>                            
                                </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <HeaderStyle BackColor="#ECF0F1" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found.
                                   
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <div style="float: right;">
                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Page </p>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                                        <asp:DropDownListChosen runat="server" ID="DropDownListChosen1" AutoPostBack="true"
                                            class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function fopenpopup() {
            $('#ViewAuditStepOb').modal('show');
            $('.modal-dialog').css('width', '43%');

        }

        function fnopenObservationdetails() {
            $('#ViewObservationDetails').modal('show');
        }

        function ShowDetObsDialog(ProcessId, AuditStepid, SubProcessId) {
            $('#divPREDOCShowDialog').modal('show');
            $('#DetObsshowdetails').attr('width', '98%');
            $('#DetObsshowdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#DetObsshowdetails').attr('src', "../InternalAuditTool/DetailedObservationList.aspx?ProcessId=" + ProcessId + "&AuditStepid=" + AuditStepid + "&SubProcessId=" + SubProcessId + "&Redirect=VO");
        };
    </script>

</asp:Content>


