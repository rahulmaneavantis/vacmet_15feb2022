﻿<%@ Page Title="Audit Frequency" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="frmAuditFrequency.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.frmAuditFrequency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>


    <script type="text/javascript">
        function fopenpopup() {
            $('#divAuditFrequencyDialog').modal('show');
        }
    </script>


    <script type="text/javascript">
        //$(function () {
        //    $('#divAuditFrequencyDialog').dialog({
        //        height: 450,
        //        width: 550,
        //        autoOpen: false,
        //        draggable: true,
        //        title: "Audit Frequency Details",
        //        open: function (type, data) {
        //            $(this).parent().appendTo("form");
        //        }
        //    });
        //});

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    
         $(document).ready(function () {
             setactivemenu('leftremindersmenu');
             fhead('Audit Frequency');
         });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                 <div  class="col-md-4 colpadding0 entrycount" style="color: #999; padding-top: 5px;">                   
                    <div class="col-md-3 colpadding0">
                    <p style="color: #999; margin-top: 5px; width:280px;">Select  Location :</p>
                        </div>            
                        <asp:DropDownList runat="server" ID="ddlFilterLocation" class="form-control m-bot15" Style="width: 40%; float: left"
                             AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>                        
                    <div style="text-align:right">                    
                     <asp:LinkButton Text="Add New" runat="server" ID="btnAddPromotor" CssClass="btn btn-primary" OnClientClick="fopenpopup()" OnClick="btnAddPromotor_Click" />
                </div>
                 <div style="margin-bottom: 4px"> 
            <%--<asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                <asp:GridView runat="server" ID="grdAuditor" AutoGenerateColumns="false" OnRowDataBound="grdAuditor_RowDataBound"
                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table"  Width="100%"  AllowSorting="true"
                    OnSorting="grdAuditor_Sorting"  DataKeyNames="ID" OnRowCommand="grdAuditor_RowCommand"
                    OnPageIndexChanging="grdAuditor_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                        <ItemTemplate>
                                               <%#Container.DataItemIndex+1 %>
                        </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField  HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="lblClient" runat="server" Text='<%# GetCustomerBranchName((long)Eval("CustomerBranchid")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField  HeaderText="Rating">
                            <ItemTemplate>
                                <asp:Label ID="lblrating" runat="server" Text='<%# GetStatusName((Int32)Eval("Rating")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Frequency">
                            <ItemTemplate>
                                <asp:Label ID="lblFrequency" runat="server" Text='<%#Eval("Frequency") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_PROMOTER" OnClientClick="fopenpopup()"
                                    CommandArgument='<%# Eval("ID") +"," +Eval("CustomerBranchid") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Observation" title="Edit Audit Frequency Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_PROMOTER"
                                    CommandArgument='<%# Eval("ID") +"," +Eval("CustomerBranchid") %>' OnClientClick="return confirm('Are you certain you want to delete this Audit Frequency Details?');"><img src="../../Images/delete_icon_new.png" alt="Delete Audit Frequency Details" title="Delete Location Details" /></asp:LinkButton>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid"   />
                    <HeaderStyle CssClass="clsheadergrid"    />            
                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                      </PagerTemplate> 
                </asp:GridView>
                </div>
                <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="margin-left: 750px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>                
                        <%--</asp:Panel>--%>
                      </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>    
    <div class="modal fade" id="divAuditFrequencyDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 430px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAuditorDialog">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional" OnLoad="upPromotor_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>                                    
                                    <div  style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Location</label>
                                        <asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15" Style="width: 200px;"
                                             AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" />
                                    <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidatorloc" ErrorMessage="Please select Location."
                                            ControlToValidate="ddlLocation" runat="server" ValidationGroup="PromotorValidationGroup"
                                            Display="None" />--%>
                                          <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Location" ControlToValidate="ddlLocation"
                                       runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                       Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Rating</label>
                                        <asp:DropDownList runat="server" ID="ddlRating" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlRating_SelectedIndexChanged" class="form-control m-bot15" Style="width: 200px;">
                                            <asp:ListItem Value="-1">< Select Rating ></asp:ListItem>
                                            <asp:ListItem Value="1">High</asp:ListItem>
                                            <asp:ListItem Value="2">Medium</asp:ListItem>
                                            <asp:ListItem Value="3">Low</asp:ListItem>
                                        </asp:DropDownList>
                                         <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Rating" ControlToValidate="ddlRating"
                                       runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                       Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Audit Frequency</label>
                                        <asp:DropDownList runat="server" ID="ddlAuditFrequency" class="form-control m-bot15" Style="width: 200px;">
                                            <asp:ListItem Value="-1">< Select Frequency ></asp:ListItem>
                                            <asp:ListItem Value="1">Annually</asp:ListItem>
                                            <asp:ListItem Value="2">2 in Year</asp:ListItem>
                                            <asp:ListItem Value="3">4 in Year</asp:ListItem>
                                            <asp:ListItem Value="4">12 in Year</asp:ListItem>
                                            <%--<asp:ListItem Value="2">1 in 2 Years</asp:ListItem>
                            <asp:ListItem Value="3">1 in 3 Years</asp:ListItem>--%>
                                        </asp:DropDownList>
                                          <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Frequency" ControlToValidate="ddlAuditFrequency"
                                       runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                       Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server"  ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>                                    
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
