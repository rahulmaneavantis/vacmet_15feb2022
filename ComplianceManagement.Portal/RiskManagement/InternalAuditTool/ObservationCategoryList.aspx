﻿<%@ Page Title="Observation SubCategoryList" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ObservationCategoryList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.ObservationCategoryList" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        #catlist:hover {
            color: blue;
            text-decoration: underline;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">
        function fopenpopup() {
            $('#dvObservationList').modal('show');
            return true;
        }
        //This is used for Close Popup after save or update data.
        function CloseWin() {
            $('#dvObservationList').modal('hide');
        };

        //This is used for Close Popup after button click.
        //function caller() {
        //    setInterval(CloseWin, 7000);
        //};
    </script>

    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>

    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlObservation.ClientID %>").combobox();
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        $(document).ready(function () {
            setactivemenu('Observation Subcategory');
            fhead('Observation Subcategory');
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <%--<div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>--%>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upProcessList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                               <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; margin-bottom:10px; width: 100%;float: left;">
                                   <table>
                                       <tr>
                                           <td>
                                               <a href="ObservationCategory.aspx" id="catlist" style="font-size:16px; float: left; color:#666">Catogories</a>
                                           </td>
                                           <td>></td>
                                           <td> 
                                               <asp:Label ID="catname" runat="server" style="font-size:16px; float: left; color:#666;"></asp:Label>
                                           </td>
                                       </tr>
                                   </table>                                    
                               </div>                                         
                             <div class="clearfix"></div>
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width:23%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div style="text-align:right">
                     <asp:LinkButton Text="Back" runat="server" ID="lnkBackToProcess" CssClass="btn btn-primary" OnClick="lnkBackToProcess_Click"/>
                     <asp:LinkButton Text="Add New" runat="server" ID="btnSubcategory" OnClientClick="fopenpopup()" CssClass="btn btn-primary" OnClick="btnSubcategory_Click"  />
                </div>
             <div style="margin-bottom: 4px">           
                <asp:GridView runat="server" ID="grdObservationList" AutoGenerateColumns="false"  OnRowDataBound="grdObservationList_RowDataBound"
                        AllowSorting="true"  PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                        DataKeyNames="ID" OnRowCommand="grdObservationList_RowCommand" ShowHeaderWhenEmpty="true">
                    <Columns>
                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                               <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observation Category">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label ID="lblObsCat" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub Category">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label ID="lblObsSubCat" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("obsName") %>' ToolTip='<%# Eval("obsName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                                                                  
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="7%">
                            <ItemTemplate>                                
                                <asp:LinkButton ID="lbtEdit" CausesValidation="false"  OnClientClick="fopenpopup()" runat="server" CommandName="EDIT_Observation" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Observation" data-toggle="tooltip" title="Edit Observation Subcategory"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Observation" CommandArgument='<%# Eval("ID") %>' Visible="false"
                                    OnClientClick="return confirm('Are you certain you want to delete this Observation List?');"><img src="../../Images/delete_icon_new.png" alt="Delete Observation" title="Delete Observation" /></asp:LinkButton>                                
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>        
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />            
                                             <PagerSettings Visible="false" /> 
                    <HeaderStyle BackColor="#ECF0F1" />        
                    <PagerTemplate>
                        <%--<table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>--%>
                      </PagerTemplate>
                    <EmptyDataTemplate>
                        No Sub-Category Record Found
                    </EmptyDataTemplate>
                </asp:GridView>               
                </div>
                              <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                           <%-- <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="dvObservationList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 90%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divProcessDialog">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateName" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 140px; display: block; float: left; font-size: 13px; color: #333;">
                                            Observation Category</label>
                                        <asp:DropDownList runat="server" ID="ddlObservation" class="form-control m-bot15" Style="width: 300px;" />
                                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Select Observation Category." ControlToValidate="ddlObservation"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EventValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 140px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sub Category</label>
                                        <asp:TextBox runat="server" ID="tbxName" Style="width: 300px;" ToolTip="Provide Sub-Category Name" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage=" Sub Category can not be empty." ControlToValidate="tbxName"
                                            runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 5px; margin-left: 150px; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
