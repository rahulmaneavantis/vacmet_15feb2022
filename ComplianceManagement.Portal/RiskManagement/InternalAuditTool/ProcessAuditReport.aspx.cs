﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
//using Spire.Presentation.Drawing.Animation;
using System.Globalization;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ProcessAuditReport : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                //BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();

                ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));                
               
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
     
  

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;           
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }



        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }


        private List<ProcessAuditReportView> BindDataExport()
        {

            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int CustomerBranchId = -1;
            int VerticalID = -1;
            string FinancialYear = String.Empty;
            string Period = String.Empty;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
            {
                int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                if (vid != -1)
                {
                    VerticalID = vid;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }
            }

            if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    FinancialYear = ddlFinancialYear.SelectedItem.Text;
                }
            }
            Branchlist.Clear();
            GetAllHierarchy(customerID, CustomerBranchId);
            Branchlist.ToList();

            string chk = txtfromMonthDate.Text;
            int Year = Convert.ToInt32(chk.Substring(chk.Length - 4));
            string YourString = chk.Remove(chk.Length - 5);
            int monthnumber = DateTime.ParseExact(YourString, "MMMM", CultureInfo.InvariantCulture).Month;
            //int days = DateTime.DaysInMonth(Year, monthnumber);
            DateTime fromDatePeroid = new DateTime(Year, monthnumber, 1);




            string chk2 = txttoMonthDate.Text;
            int Yearto = Convert.ToInt32(chk2.Substring(chk2.Length - 4));
            string YourString2 = chk2.Remove(chk2.Length - 5);
            int monthnumber2 = DateTime.ParseExact(YourString2, "MMMM", CultureInfo.InvariantCulture).Month;
            int days = DateTime.DaysInMonth(Yearto, monthnumber2);
            DateTime toDatePeroid = new DateTime(Yearto, monthnumber2, days);
            List<ProcessAuditReportView> RCMExportList = new List<ProcessAuditReportView>();


            RCMExportList = ProcessManagement.GetProcessAuditView(customerID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, VerticalID, FinancialYear, Branchlist.ToList(), fromDatePeroid, toDatePeroid);
            RCMExportList = RCMExportList.OrderBy(a => a.ProcessOrder).ToList();

            return RCMExportList;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void lbtnExportExcelTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfromMonthDate.Text != null)
                {
                    if (txttoMonthDate.Text != null)
                    {
                        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                using (ExcelPackage exportPackge = new ExcelPackage())
                                {
                                    var data = BindDataExport();
                                    DateTime fromDatePeroid = new DateTime();
                                    DateTime toDatePeroid = new DateTime();
                                    if (txtfromMonthDate.Text != null)
                                    {
                                        fromDatePeroid = Convert.ToDateTime(txtfromMonthDate.Text);
                                    }
                                    if (txttoMonthDate.Text != null)
                                    {
                                        toDatePeroid = Convert.ToDateTime(txttoMonthDate.Text);
                                    }

                                    #region Sheet1
                                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Normal");
                                    DataTable ExcelData = null;
                                    DataView view = new System.Data.DataView(data.ToDataTable());                                   
                                    ExcelData = view.ToTable("Selected", false, "SrNo", "Company", "CompanyName", "ProcessName", "LocationName",
                                        "TypeOfReport", "FromPeriod", "EndPeriod", "ForPeriod", "CreatedDate", "ObservationTitle", "Observation", "RootCost", "Risk", "FinancialImpact", "Recomendation", "ManagementResponse",
                                         "PersonResponsible", "Owner", "TimeLine", "ObservationRating", "Currency");
                                    if (ExcelData.Rows.Count > 0)
                                    {
                                        int count = 1;
                                        foreach (DataRow item in ExcelData.Rows)
                                        {
                                            if (item["SrNo"].ToString() != null)
                                            {
                                                item["SrNo"] = count;
                                                count++;
                                            }
                                            if (item["CompanyName"].ToString() != null)
                                            {
                                                item["CompanyName"] = ddlLegalEntity.SelectedItem.Text.Trim();
                                            }
                                            if (item["TypeOfReport"].ToString() != null)
                                            {
                                                item["TypeOfReport"] = "Internal Audit";
                                            }
                                            if (item["Currency"].ToString() != null)
                                            {
                                                item["Currency"] = "INR";
                                            }
                                            if (item["FromPeriod"].ToString() != null)
                                            {
                                                item["FromPeriod"] = fromDatePeroid.Month + "" + fromDatePeroid.Year;
                                            }
                                            if (item["EndPeriod"].ToString() != null)
                                            {
                                                item["EndPeriod"] = toDatePeroid.Month + "" + toDatePeroid.Year;
                                            }
                                            if (Convert.ToInt32(item["ObservationRating"]) == 1)
                                            {
                                                item["ObservationRating"] = "High";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 3)
                                            {
                                                item["ObservationRating"] = "Low";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 2)
                                            {
                                                item["ObservationRating"] = "Medium";
                                            }
                                        }
                                        view = null;
                                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["A1"].Value = "Serial No";
                                        exWorkSheet.Cells["A1"].AutoFitColumns(20);

                                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["B1"].Value = "Company";
                                        exWorkSheet.Cells["B1"].AutoFitColumns(20);

                                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["C1"].Value = "Company Name";
                                        exWorkSheet.Cells["C1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["D1"].Value = "Department";
                                        exWorkSheet.Cells["D1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["E1"].Value = "Location";
                                        exWorkSheet.Cells["E1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["F1"].Value = "Type of report";
                                        exWorkSheet.Cells["F1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["G1"].Value = "From Period";
                                        exWorkSheet.Cells["G1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["H1"].Value = "To Period";
                                        exWorkSheet.Cells["H1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["I1"].Value = "Audit Period";
                                        exWorkSheet.Cells["I1"].AutoFitColumns(20);

                                        exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["J1"].Value = "Report released date";
                                        exWorkSheet.Cells["J1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["K1"].Value = "Audit Issue Heading";
                                        exWorkSheet.Cells["K1"].AutoFitColumns(20);

                                        exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["L1"].Value = "Audit Observation / Findings";
                                        exWorkSheet.Cells["L1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["M1"].Value = "Root Cause";
                                        exWorkSheet.Cells["M1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["N1"].Value = "Business Implication";
                                        exWorkSheet.Cells["N1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["O1"].Value = "Issue Value";
                                        exWorkSheet.Cells["O1"].AutoFitColumns(10);

                                        exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["P1"].Value = "Recommendation";
                                        exWorkSheet.Cells["P1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Q1"].Value = "Agreed Action";
                                        exWorkSheet.Cells["Q1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["R1"].Value = "Person Responsible";
                                        exWorkSheet.Cells["R1"].AutoFitColumns(20);

                                        exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["S1"].Value = "Owner";
                                        exWorkSheet.Cells["S1"].AutoFitColumns(20);

                                        exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["T1"].Value = "Time line";
                                        exWorkSheet.Cells["T1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["U1"].Value = "Risk";
                                        exWorkSheet.Cells["U1"].AutoFitColumns(10);

                                        exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["V1"].Value = "Currency";
                                        exWorkSheet.Cells["V1"].AutoFitColumns(5);

                                        using (ExcelRange col = exWorkSheet.Cells[1, 7, 1 + ExcelData.Rows.Count, 22])
                                        {
                                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                        }

                                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 22])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                            //Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }
                                    }
                                    #endregion

                                    #region Sheet 2
                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("250 Characters");
                                    DataTable ExcelData1 = null;
                                    DataView view1 = new System.Data.DataView(data.ToDataTable());
                                    ExcelData1 = view1.ToTable("Selected", false, "SrNo", "Company", "CompanyName", "ProcessName", "LocationName",
                                        "TypeOfReport", "FromPeriod", "EndPeriod", "ForPeriod", "CreatedDate", "ObservationTitle", "Observation", "RootCost", "Risk", "FinancialImpact", "Recomendation", "ManagementResponse",
                                         "PersonResponsible", "Owner", "TimeLine", "ObservationRating", "Currency");
                                    if (ExcelData1.Rows.Count > 0)
                                    {
                                        int count = 1;
                                        foreach (DataRow item in ExcelData1.Rows)
                                        {
                                            if (item["SrNo"].ToString() != null)
                                            {
                                                item["SrNo"] = count;
                                                count++;
                                            }
                                            if (item["CompanyName"].ToString() != null)
                                            {
                                                item["CompanyName"] = ddlLegalEntity.SelectedItem.Text.Trim();
                                            }
                                            if (item["TypeOfReport"].ToString() != null)
                                            {
                                                item["TypeOfReport"] = "Internal Audit";
                                            }
                                            if (item["Currency"].ToString() != null)
                                            {
                                                item["Currency"] = "INR";
                                            }
                                            if (item["FromPeriod"].ToString() != null)
                                            {
                                                item["FromPeriod"] = fromDatePeroid.Month + "" + fromDatePeroid.Year;
                                            }
                                            if (item["EndPeriod"].ToString() != null)
                                            {
                                                item["EndPeriod"] = toDatePeroid.Month + "" + toDatePeroid.Year;
                                            }
                                            if (Convert.ToInt32(item["ObservationRating"]) == 1)
                                            {
                                                item["ObservationRating"] = "High";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 3)
                                            {
                                                item["ObservationRating"] = "Low";
                                            }
                                            else if (Convert.ToInt32(item["ObservationRating"]) == 2)
                                            {
                                                item["ObservationRating"] = "Mediuam";
                                            }
                                            if (item["Observation"].ToString() != null)
                                            {                                               
                                                if (item["Observation"].ToString().Length > 250)
                                                {
                                                    var str = item["Observation"].ToString().Substring(0, 250);
                                                   // var result = str.Substring(0, str.LastIndexOf(' '));
                                                 
                                                    item["Observation"] = str;
                                                }
                                                else
                                                {
                                                    item["Observation"] = item["Observation"].ToString();
                                                }
                                            }
                                            
                                        }
                                        view1 = null;
                                        exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData1, true);

                                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A1"].Value = "Serial No";
                                        exWorkSheet1.Cells["A1"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B1"].Value = "Company";
                                        exWorkSheet1.Cells["B1"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["C1"].Value = "Company Name";
                                        exWorkSheet1.Cells["C1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["D1"].Value = "Department";
                                        exWorkSheet1.Cells["D1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["E1"].Value = "Location";
                                        exWorkSheet1.Cells["E1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["F1"].Value = "Type of report";
                                        exWorkSheet1.Cells["F1"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["G1"].Value = "From Period";
                                        exWorkSheet1.Cells["G1"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["H1"].Value = "To Period";
                                        exWorkSheet1.Cells["H1"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["I1"].Value = "Audit Period";
                                        exWorkSheet1.Cells["I1"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["J1"].Value = "Report released date";
                                        exWorkSheet1.Cells["J1"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["K1"].Value = "Audit Issue Heading";
                                        exWorkSheet1.Cells["K1"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["L1"].Value = "Audit Observation / Findings";
                                        exWorkSheet1.Cells["L1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["M1"].Value = "Root Cause";
                                        exWorkSheet1.Cells["M1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["N1"].Value = "Business Implication";
                                        exWorkSheet1.Cells["N1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["O1"].Value = "Issue Value";
                                        exWorkSheet1.Cells["O1"].AutoFitColumns(10);

                                        exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["P1"].Value = "Recommendation";
                                        exWorkSheet1.Cells["P1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["Q1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["Q1"].Value = "Agreed Action";
                                        exWorkSheet1.Cells["Q1"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["R1"].Value = "Person Responsible";
                                        exWorkSheet1.Cells["R1"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["S1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["S1"].Value = "Owner";
                                        exWorkSheet1.Cells["S1"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["T1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["T1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["T1"].Value = "Time line";
                                        exWorkSheet1.Cells["T1"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["U1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["U1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["U1"].Value = "Risk";
                                        exWorkSheet1.Cells["U1"].AutoFitColumns(10);

                                        exWorkSheet1.Cells["V1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["V1"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["V1"].Value = "Currency";
                                        exWorkSheet1.Cells["V1"].AutoFitColumns(5);

                                        using (ExcelRange col = exWorkSheet1.Cells[1, 7, 1 + ExcelData1.Rows.Count, 22])
                                        {
                                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                        }

                                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, 1 + ExcelData1.Rows.Count, 22])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                            //Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }
                                    }
                                    #endregion

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();                                    
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/vnd.ms-excel";
                                    Response.AddHeader("content-disposition", "attachment;filename=ProcessAuditReport.xlsx");//locatioName                                                                             
                                    Response.BinaryWrite(fileBytes);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   

                                }//Using End
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }

       
        
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            //BindProcess("P");
            BindVertical();           
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            //BindProcess("P");
            BindVertical();          
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            //BindProcess("P");
            BindVertical();           
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            //BindProcess("P");
            BindVertical();         
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindProcess("P");
            BindVertical();         
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}