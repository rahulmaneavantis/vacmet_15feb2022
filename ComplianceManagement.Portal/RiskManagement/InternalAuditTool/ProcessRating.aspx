﻿<%@ Page Title="Process Rating" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ProcessRating.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.ProcessRating" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //$(function () {
        //    $('#divAuditorDialog').dialog({
        //        height: 450,
        //        width: 550,
        //        autoOpen: false,
        //        draggable: true,
        //        title: "Audit Assignment Details",
        //        open: function (type, data) {
        //            $(this).parent().appendTo("form");
        //        }
        //    });
        //});

        function fopenpopup() {
            $('#RiskCategory').modal('show');
        }

        function CloseWin() {
            $('#RiskCategory').modal('hide');
            $(' #ddlLocation').val('Select Location');
            $(' #ddlProcess').val('Select Process');
            $(' #ddlRating').val('Select Rating');
        };
        function caller() {
            setInterval(CloseWin, 3000);
        };

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">

                <div class="clearfix"></div>

<div class="col-md-2 colpadding0 entrycount" style="margin-top:5px;">
        <div class="col-md-3 colpadding0" >
            <p style="color: #999; margin-top: 5px;">Show </p>
        </div>

        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" ><%-- OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" --%>
            <asp:ListItem Text="5" Selected="True"/>
            <asp:ListItem Text="10" />
            <asp:ListItem Text="20" />
            <asp:ListItem Text="50" />
        </asp:DropDownList> 
     <div class="col-md-3 colpadding0" >
       
          </div>  
    </div>
                            <div class="col-md-5 colpadding0 entrycount" style="margin-top: 5px;"> 
                          <div class="col-md-2 colpadding0 entrycount">                   
                        <p style="color: #999; margin-top: 5px;"> Location</p>
                              </div>
                        <asp:DropDownList runat="server" ID="ddlFilterLocation" class="form-control m-bot15" Style="width: 200px; float: left"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please select Location."
                            ControlToValidate="ddlFilterLocation" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />                    
                         </div> 

            <div style="text-align:right">
                <asp:LinkButton Text="Add New" CssClass="btn btn-primary" OnClientClick="fopenpopup()" runat="server" ID="btnAddAuditAssignment" OnClick="btnAddAuditAssignment_Click" />
            </div>
        
      

<%--////////////////////////////////////////////////////////////////////////////////////////////////////////--%>           
                             <div style="margin-bottom: 4px">
            <asp:GridView runat="server" ID="grdAuditor" AutoGenerateColumns="false" GridLines="Horizontal" CssClass="table"  BorderWidth="0px"
                  OnRowCommand="grdAuditor_RowCommand" OnRowDataBound="grdAuditor_RowDataBound"
                   AllowPaging="True" PageSize="5" Width="100%"  DataKeyNames="Id" OnPageIndexChanging="grdAuditor_PageIndexChanging"> <%--OnRowCreated="grdAuditor_RowCreated"--%>

                <Columns>
                            <asp:TemplateField HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>    
                    <asp:TemplateField HeaderText ="Location"><%--ItemStyle-Width="300px"--%>
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:130px;">
                                <asp:Label ID="lblClient" runat="server" Text='<%# ShowCustomerBranchName((long)Eval("CustomerBranchid")) %>'></asp:Label>
                            </div>
                        </ItemTemplate>
	                </asp:TemplateField>

                       <asp:TemplateField HeaderText ="Process Name">
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:130px;">
                               <asp:Label ID="ProcessName" runat="server" Text='<%# ShowProcessName((long)Eval("ProcessID")) %>'></asp:Label>
                            </div>
                        </ItemTemplate>
	                </asp:TemplateField>

                    <asp:TemplateField HeaderText="Rating">
                        <ItemTemplate>
                            <asp:Label ID="lblAssigned" runat="server" Text='<%# ShowRating((int)Eval("Rating")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                           <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_PROMOTER"  OnClientClick="fopenpopup()"
                                    CommandArgument='<%# Eval("CustomerBranchid") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Promotors" title="Edit Promoters Details" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 
                  <PagerTemplate>
                      <table style="display: none">
                          <tr>
                             <td>
                                 <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                               </td>
                            </tr>
                       </table>
                  </PagerTemplate>
               
            </asp:GridView>
</div>

                    <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click"/> <%--OnClick ="Previous_Click"--%>
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click"/>   <%-- OnClick ="Next_Click" --%>           
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                      </div>

</section>
                    </div>
                </div>
            </div>

            <%--///////////////////////////////////////////////--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="RiskCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width:450px; height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divRiskCategoryDialog">
                        <asp:UpdatePanel ID="upProcess" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" CssClass="vdsummary"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>                               
                                    <%--<div style="margin-bottom: 5px; align-items: center;width:100%;">
                                        <label style="display: block; font-size: 20px; float: right; color: black">Audit Assignment Details</label>
                                    </div>--%>
                                    <div style="margin-bottom: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Location:</label>
                                        <asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15" Style="width: 200px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Location can not be empty." ControlToValidate="ddlLocation"
                                           InitialValue="-1" runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Process:</label>
                                    <asp:DropDownList runat="server" ID="ddlProcess" class="form-control m-bot15" Style="width: 200px;"
                                            AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Process can not be empty." ControlToValidate="ddlProcess"
                                           InitialValue="-1" runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Rating:</label>
                                        <asp:DropDownList runat="server" ID="ddlRating" class="form-control m-bot15" Style="width: 200px;"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                            <asp:ListItem Value="1">High</asp:ListItem>
                                            <asp:ListItem Value="2">Medium</asp:ListItem>
                                            <asp:ListItem Value="3">Low</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Process can not be empty." ControlToValidate="ddlRating"
                                         InitialValue="-1"   runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                     
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>







    <%--<div id="divAuditorDialog">
        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional" OnLoad="upPromotor_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="PromotorValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="PromotorValidationGroup" Display="None" />
                    </div>
                    <div runat="server" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <asp:DropDownList runat="server" ID="ddlLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 254px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Select Location."
                            ControlToValidate="ddlLocation" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Process</label>
                        <asp:DropDownList runat="server" ID="ddlProcess" Style="padding: 0px; margin: 0px; height: 22px; width: 254px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Process."
                            ControlToValidate="ddlProcess" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Rating</label>
                        <asp:DropDownList runat="server" ID="ddlRating" Style="padding: 0px; margin: 0px; height: 22px; width: 254px;"
                            CssClass="txtbox">
                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                            <asp:ListItem Value="1">High</asp:ListItem>
                            <asp:ListItem Value="2">Medium</asp:ListItem>
                            <asp:ListItem Value="3">Low</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Rating."
                            ControlToValidate="ddlRating" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px; float: right; margin-right: 105px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="PromotorValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divAuditorDialog').dialog('close');" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>
</asp:Content>
