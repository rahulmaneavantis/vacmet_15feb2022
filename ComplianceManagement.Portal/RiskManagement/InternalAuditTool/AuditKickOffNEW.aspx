﻿<%@ Page Title="Audit Kickoff New" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditKickOffNEW.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditKickOffNEW" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <style type="text/css">
        .GridView1 {
            width: 200px;
            margin-left: auto;
            margin-right: auto;
        }

        .accordionContent1 {
            background-color: #D3DEEF;
            border-color: -moz-use-text-color #2F4F4F #2F4F4F;
            border-right: 1px dashed #2F4F4F;
            border-style: none dashed dashed;
            border-width: medium 1px 1px;
            padding: 10px 5px 5px;
            width: 92%;
        }

        .accordionHeaderSelected1 {
            background-color: #5078B3;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .accordionHeader1 {
            background-color: #2E4D7B;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .href1 {
            color: White;
            font-weight: bold;
            text-decoration: none;
        }

        .textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }

        
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.            
            $(function () {
                $('input[id*=tbxLoanStartDate]').datepicker(
                    { dateFormat: 'dd-mm-yy' });
            });
            $(function () {
                $('input[id*=tbxLoanEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

        function LblClear() {
            $('#cvDuplicateEntry').ErrorMessage('');
        };

        function LblClearValue() {
            setInterval(LblClear, 3000);
        };
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>


    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).ready(function () {
            setactivemenu('Audit Kickoff New');
            fhead('Audit Kickoff New');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ForeColor="Red"
                            ValidationGroup="ComplianceValidationGroup1" />
                            <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                            <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                        </div>                           
                            <header class="panel-heading tab-bg-primary ">
                                    <ul id="rblRole1" class="nav nav-tabs">                                              
                                               
                                         <li class="dummyval">                                                      
                                                    <asp:LinkButton ID="liAudtdetl"  OnClick="liAudtdetl_Click"  runat="server">Auditkickoff</asp:LinkButton>
                                            </li>                                           
                                            <li class="dummyval">                                                      
                                                    <asp:LinkButton ID="liprocess"  OnClick="liprocess_Click"  runat="server">Process</asp:LinkButton>
                                            </li>
                                            <li class="dummyval">                                                      
                                                    <asp:LinkButton ID="liimplSts"  OnClick="liimplSts_Click"  runat="server">ImplementationStatus</asp:LinkButton>
                                            </li>                                                                                                                                                  
                                </ul>
                                </header>
                                <div class="clearfix"></div>    
                                  <div style="float:left;width:100%">
                       <div style="float:left;width:13%">                                                                
                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 12px;width:100%">
                    <div class="col-md-3 colpadding0" style="width:30%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                    </div> </div>      <div style="float:left;width:87%">
                        <div id="FilterLocationdiv" runat="server" class="col-md-4 colpadding0 entrycount" style="color: #999; padding-top: 9px;width: 48%;">                         
                            <div class="col-md-3 colpadding0" style="width: 15%;">
                                  <p style="color: #999; margin-top: 5px; width:100px;">Location</p>
                            </div>
                             <asp:TextBox runat="server" ID="tbxFilterLocation" Width="325px" CssClass="form-control" />
                    <div style="margin-left: 66px; position: absolute; z-index: 10;display: inherit;"  id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Location."
                                    ControlToValidate="tbxFilterLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" />
                            </div>
                        </div>
                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 12px;">
                        <div class="col-md-2 colpadding0">
                        <p style="color: #999; margin-top: 5px; width:100px; ">Financial Year</p>
                    </div>
                     <asp:DropDownList runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" style="width:60%; float:right;"
                          AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                        </asp:DropDownList>
                         
                        </div>    </div></div>
                        <div class="tab-content ">                        
                            <div runat="server" id="dvAuditDetails"  class="tab-pane active">
                                            <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false"
                                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                                DataKeyNames="ProcessID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Branch Name" ItemStyle-Width="200px" HeaderStyle-VerticalAlign="Middle">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                <asp:Label runat="server" ID="lblLocation" Text='<%# ShowCustomerBranchName((int)Eval("CustomerBranchId")) %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Financial Year">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label runat="server" ID="lblFinancialYear" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label runat="server" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Term Name">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label runat="server" Text='<%# Eval("TermName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Assigned To"  SortExpression="AssignedTo" Visible="false">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                                                <asp:Label runat="server" ID="lblAssignedTo" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="EAId" Visible="false">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                                                <asp:Label runat="server" ID="lblExternalAuditorId" Text='<%# Eval("ExternalAuditorId") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PhaseCount" Visible="false">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                                                <asp:Label runat="server" ID="lblPhaseCount" Text='<%# Eval("PhaseCount") %>' CssClass="label"></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" OnClick="lbtEdit_Click" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") %>' CausesValidation="false"><img src="../../Images/edit_icon_new.png" alt="Edit Compliance" title="Edit Compliance" /></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                            </asp:GridView>                                            
                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-5 colpadding0">
                                                    <div class="table-Selecteddownload">
                                                        <div class="table-Selecteddownload-text">
                                                            <p>
                                                                <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 colpadding0" style="margin-left: 750px;">
                                                    <div class="table-paging" style="margin-bottom: 10px;">
                                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />
                                                        <div class="table-paging-text">
                                                            <p>
                                                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                            </p>
                                                        </div>
                                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />
                                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                                    </div>
                                                </div>
                                            </div>                                       
                                </div>
                            <div runat="server" id="dvProcessDetails"  class="tab-pane">
                            <asp:Panel ID="Panel2" runat="server">
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary runat="server" CssClass="vdsummary" ForeColor="Red"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                </div>
                                         
                                                      <div class="clearfix"></div><div class="clearfix"></div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">
                                              <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Location</label>
                                                <asp:DropDownList Enabled="false" runat="server" ID="ddlLocation" Width="60%" CssClass="form-control m-bot15"/>
                                                <asp:CompareValidator ID="CompareValidator14" ErrorMessage="Please Select Location."
                                                    ControlToValidate="ddlLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                                        </div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">                                                                                  
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Internal /External
                                                </label>                                            
                                                <asp:DropDownList runat="server" ID="ddlInternalExternal" Width="60%" class="form-control m-bot15" 
                                                     AutoPostBack="true" OnSelectedIndexChanged="ddlInternalExternal_SelectedIndexChanged">
                                                    <asp:ListItem Text="Select">Select Internal/External</asp:ListItem>
                                                    <asp:ListItem Text="Internal">Internal</asp:ListItem>
                                                    <asp:ListItem Text="Internal">External</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="CompareValidator17" ErrorMessage="Please Select Internal /External."
                                                    ControlToValidate="ddlInternalExternal" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                                       
                                        </div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Financial Year</label>                                          
                                                <asp:DropDownList runat="server" ID="ddlFinancialYear" class="form-control m-bot15" width="60%"/>
                                         </div>
                                <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;" runat="server" id="DivIsExternal" visible="false">
                                                                                
                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                    <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                        External Auditor</label>                                                
                                                    <asp:DropDownList runat="server" ID="ddlAuditor" class="form-control m-bot15" Style="width: 60%;" 
                                                       AutoPostBack="true" OnSelectedIndexChanged="ddlAuditor_SelectedIndexChanged">
                                                    </asp:DropDownList></td>
                                           
                                        </div><div class="clearfix"></div><div class="clearfix"></div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Scheduling Type</label>                                            
                                                <asp:DropDownList runat="server" ID="ddlSchedulingType" class="form-control m-bot15"  Style="width: 60%;" 
                                                     OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"/>
                                            </div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">                                         
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Select For Period</label>                                            
                                                <asp:DropDownList runat="server" ID="ddlPeriod" class="form-control m-bot15" Style="width: 60%;"/>
                                                <asp:RequiredFieldValidator ID="RFVPeriod" ErrorMessage="Select Period." ControlToValidate="ddlPeriod"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="Select Period" Display="None" />
                                       </div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Actual Start Date</label>                                          
                                                <asp:TextBox runat="server" ID="tbxLoanStartDate" class="form-control" style="width:60%" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Actual Start Date can not be empty."
                                                    ControlToValidate="tbxLoanStartDate"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                        </div><div class="clearfix"></div><div class="clearfix"></div>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">                                     
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 31%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Expected End Date
                                                </label>                                            
                                                <asp:TextBox runat="server" ID="tbxLoanEndDate" class="form-control" style="width:60%" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ErrorMessage="Expected End Date can not be empty."
                                                    ControlToValidate="tbxLoanEndDate"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                        </div>                                       
                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top:10px;">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                <label style="width: 41%; display: block; float: left; font-size: 14px; color: #999;">
                                                    Audit Planning Document Upload</label> 
                                                <asp:Label runat="server" ID="lblSampleForm"/>
                                                <asp:FileUpload runat="server" ID="fuSampleFile" style="color: #999;"   />                                            
                                        </div>
                                   <div class="clearfix"></div><div class="clearfix" style="height:50px"></div>
                                       <div style="margin-bottom: 7px">
                                            <span style="color:#999 ;font-size: 14px;">Auditor Details</span>                                           
                                                <asp:Label runat="server" ID="Label3" Font-Bold="true" ForeColor="Red"></asp:Label>
                                           </div>                                                                           
                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false"  OnRowDataBound="grdRiskActivityMatrix_RowDataBound"
                                                            PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                                            DataKeyNames="ProcessId" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">
                                                            <Columns>
                                                                 <asp:TemplateField HeaderText="Sr">
                                                                     <ItemTemplate>
                                                                     <%#Container.DataItemIndex+1 %>
                                                                     </ItemTemplate>
                                                                </asp:TemplateField> 
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateID" runat="server" Text='<%# Eval("ProcessId") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Performer">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlPerformer" class="form-control m-bot15" runat="server"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Reviewer">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlReviewer" runat="server" class="form-control m-bot15" ></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Add" Height="35px" Width="60px" CssClass="btn btn-primary"
                                                                            ValidationGroup="ComplianceValidationGroup" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>   
                                                                       
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdActualAssignmentSave" AutoGenerateColumns="false" 
                                                            PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                                             DataKeyNames="ProcessId">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Sr">
                                                                     <ItemTemplate>
                                                                     <%#Container.DataItemIndex+1 %>
                                                                     </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateID" runat="server" Text='<%# Eval("ProcessId") %>'></asp:Label>
                                                                        <asp:Label ID="lblPerformerUserID" runat="server" Text='<%# Eval("PerformerUserID") %>'></asp:Label>
                                                                        <asp:Label ID="lblReviewerUserID" runat="server" Text='<%# Eval("ReviewerUserID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Performer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowPerformerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Reviewer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowReviewerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                           <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph2" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                 <div style="margin-bottom: 7px; margin-left: 300px; margin-top: 10px">
                                                <asp:Button Text="Save" runat="server" ID="btnSave"  OnClick="btnSave_Click" CssClass="btn btn-primary" />
                                            </div> 
                                <div class="clearfix"></div><div class="clearfix" style="height:50px"></div>  
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdActualAssignmentSaved" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true" 
                                                            CssClass="table" GridLines="None" Width="100%" AllowSorting="true" DataKeyNames="ProcessId">
                                                            <Columns>
                                                                 <asp:TemplateField HeaderText="Sr">
                                                                     <ItemTemplate>
                                                                     <%#Container.DataItemIndex+1 %>
                                                                     </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Performer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowPerformerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Reviewer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowReviewerName((long)Eval("ProcessId"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph3" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>                               
                            </asp:Panel>
                            </div>
                            <div runat="server" id="dvImplstatus"  class="tab-pane">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table style="width: 100%">
                                        <tr style="display: none;">
                                            <td style="width: 25%">
                                                <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">
                                                    *</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 14px; color: #999;">
                                                    Financial Year</label>

                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList runat="server" ID="ddlFinancialYearImplementation" class="form-control m-bot15" Style="width: 70px; float: left"
                                                    OnSelectedIndexChanged="ddlFinancialYearImplementation_SelectedIndexChanged" />
                                            </td>
                                            <td style="width: 25%"></td>
                                            <td style="width: 25%"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" style="margin-top: 20px;">
                                                <asp:Label runat="server" ID="Label4" Font-Bold="true" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdImplementationStatus" AutoGenerateColumns="false" 
                                                            PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                                            OnRowDataBound="grdImplementationStatus_RowDataBound" OnRowCommand="grdImplementationStatus_RowCommand"
                                                            OnPageIndexChanging="grdImplementationStatus_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField  HeaderText="Financial Year">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Period">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateForPeriod" runat="server" Text='<%# Eval("ForPeriod") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Performer">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlPerformer" runat="server" Width="250px" Height="30px"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Reviewer">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlReviewer" runat="server" Width="250px" Height="30px"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="btn btn-primary"
                                                                            CommandName="ADDNEW" Height="25px" Width="60px" CausesValidation="false"
                                                                            ValidationGroup="ComplianceValidationGroup" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph4" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdImplementationStatusSave" AutoGenerateColumns="false" PageSize="5" 
                                                            AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true">
                                                            <Columns>
                                                                <asp:TemplateField  HeaderText="Financial Year">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Period">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateForPeriod" runat="server" Text='<%# Eval("ForPeriod") %>'></asp:Label>
                                                                        <asp:Label ID="lblPerformerUserID" runat="server" Text='<%# Eval("PerformerUserID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblReviewerUserID" runat="server" Text='<%# Eval("ReviewerUserID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Performer" >
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowIMPlementationPerformerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Reviewer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowIMPlementationReviewerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph5" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:Button Text="Save" runat="server" ID="btnSaveNew" OnClick="btnSaveNew_Click" CssClass="btn btn-primary" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView runat="server" ID="grdImplementationStatusSaved" AutoGenerateColumns="false" PageSize="5" 
                                                            AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Financial Year">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Period">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblItemTemplateForPeriod" runat="server" Text='<%# Eval("ForPeriod") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField  HeaderText="Performer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowIMPlementationPerformerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("PerformerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Reviewer">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowIMPlementationReviewerName((string)Eval("ForPeriod"),(int)Eval("CustomerBranchID"),(int)Eval("ReviewerUserID"),(string)Eval("FinancialYear")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph6" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                </div>
                        </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
