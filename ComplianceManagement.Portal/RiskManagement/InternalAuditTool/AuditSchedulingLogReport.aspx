﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditSchedulingLogReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditSchedulingLogReport" %>


<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width: 80% !important;
        }

        .dd_chk_drop {
            width: 90% !important;
            margin-top: 17px !important;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            fhead('Scheduling Deviation Report');
        });
        
    </script>
    <style type="text/css">
        .td1 {
            width: 5%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 5%;
        }

        .td4 {
            width: 25%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                               <div class="col-md-12 colpadding0">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                        ValidationGroup="ComplianceInstanceValidationGroup" class="alert alert-block alert-danger fade in"  ForeColor="Red" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" class="alert alert-block alert-danger fade in"  ForeColor="Red" />
                                    <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                                </div>
                                <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                        </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                        <li class=""  id="liReviewer" runat="server">
                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                        </li>
                                          <%}%>                                    
                                    </ul>
                                </header>
                                <div class="clearfix"></div>                                      
                                  
                                <div class="clearfix"></div>                                                                                                 
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                       <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                        AutoPostBack="true" Style="background:none;" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                               DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="90%" Height="32px"
                                        AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                              DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="90%" Height="32px"
                                        AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                              DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                        AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                              DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                </div> 
                                <div class="clearfix"></div>                                                               
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" 
                                            class="form-control m-bot15" Width="90%" Height="32px"
                                            DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                             OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                  
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;">                    
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial"  AutoPostBack="true"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"  
                                            OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged"
                                             class="form-control m-bot15" Width="90%" DataPlaceHolder="Financial year">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Select Financial Year." ControlToValidate="ddlFilterFinancial"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                        Display="None" />
                                    </div>
                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true"
                                            DataPlaceHolder="Scheduling Type" 
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                            class="form-control m-bot15" Width="90%" Height="32px">                                                                               
                                        </asp:DropDownListChosen>
                                      <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Select Scheduling Type." ControlToValidate="ddlSchedulingType"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                        Display="None" />
                                    </div>
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true"
                                          DataPlaceHolder="Period" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                           class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                         <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Select Period." ControlToValidate="ddlPeriod"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                        Display="None" />
                                    </div> 
                                    
                                </div>                                                                                           
                                <div class="clearfix"></div>   
                            <div class="col-md-12 colpadding0">
                                  <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                    <%{%>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true"
                                           AllowSingleDeselect="false" DisableSearchThreshold="3"   class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Vertical">
                                        </asp:DropDownListChosen>
                                    </div> 
                                  <%}%>    
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;"> 
                                        <%-- <asp:TextBox runat="server" ID="tbxStartDate" class="form-control" Style="width: 80%;" placeholder="Start Date" AutoPostBack="true"/>--%>
                                    </div>
                                    <div class="col-md-6 colpadding0" style="margin-top: 5px;"> 
                                         <%--<asp:TextBox runat="server" ID="tbxEndDate" class="form-control" Style="width: 80%" placeholder="End Date" AutoPostBack="true"/>--%>
                                    </div>
                                    
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px;float: right; margin-right: -183px;"> 
                                         <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" ValidationGroup="ComplianceInstanceValidationGroup" style="margin-top: 5px;  width: 126px;" OnClick="lbtnExportExcel_Click" runat="server"/>
                                    </div>
                            </div>     
                            <div class="clearfix"></div>                                               
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

