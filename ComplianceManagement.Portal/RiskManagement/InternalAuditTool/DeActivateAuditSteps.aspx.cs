﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Collections;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class DeActivateAuditSteps : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindProcess("P");
                BindVertical();
                //BindData("P");
                grdPageNumber.Attributes.Add("style", "display:none;");
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindVertical()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlVertical.DataTextField = "VerticalName";
            ddlVertical.DataValueField = "ID";
            ddlVertical.Items.Clear();
            ddlVertical.DataSource = UserManagementRisk.FillVerticalList(customerID); //FillVerticalList(int customerID)
            ddlVertical.DataBind();

            foreach (ListItem item in ddlVertical.Items)
            {
                item.Selected = true;
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindAuditStepList()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int processid = -1;
            int subprocessid = -1;
            int CustomerBranchId = -1;
            int verticalid = -1;

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
            {
                if (ddlSubProcess.SelectedValue != "-1")
                {
                    subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                }
            }

            //if (ddlVertical.SelectedValue != "")
            //{
            //    if (ddlVertical.SelectedValue != "-1")
            //    {
            //        verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
            //    }
            //}

            List<int> verticalList = new List<int>();
            for (int i = 0; i < ddlVertical.Items.Count; i++)
            {
                if (ddlVertical.Items[i].Selected)
                    verticalList.Add(Convert.ToInt32(ddlVertical.Items[i].Value));
            }

            bool auditStepStatus = false;
            if (ddlStatus.SelectedValue == "0")
                auditStepStatus = false;
            else
                auditStepStatus = true;

            Branchlist.Clear();
            GetAllHierarchy(customerID, CustomerBranchId);
            Branchlist.ToList();

            ddlAuditStepList.DataTextField = "AuditStep";
            ddlAuditStepList.DataValueField = "ID";
            ddlAuditStepList.Items.Clear();
            ddlAuditStepList.DataSource = Business.AdditionalRiskCreation.FillAuditStepList(customerID, Branchlist, processid, subprocessid, verticalList, auditStepStatus);
            ddlAuditStepList.DataBind();           
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        private void BindProcess(string flag)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustomerBranchId = -1;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("P", customerID, CustomerBranchId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("N", customerID, CustomerBranchId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.Text != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

                grdAuditStepList.PageIndex = chkSelectedPage - 1;
                grdAuditStepList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindData();
            }       
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();

                    BindProcess("P");
                }
                else
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();

                    BindProcess("N");
                }

                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));               
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }

            //ddlPageSize_SelectedIndexChanged(sender, e);

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");

            BindAuditStepList();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("P");
                }
                else
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("N");
                }

                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            //ddlPageSize_SelectedIndexChanged(sender, e);

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");

            BindAuditStepList();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("P");
                }
                else
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("N");
                }

                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
               
            }
            else
            {

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            //ddlPageSize_SelectedIndexChanged(sender, e);

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");

            BindAuditStepList();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("P");
                }
                else
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("N");
                }

                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            //ddlPageSize_SelectedIndexChanged(sender, e);

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");

            BindAuditStepList();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("P");
                }
                else
                {
                    //grdAuditStepList.DataSource = null;
                    //grdAuditStepList.DataBind();
                    BindProcess("N");
                }
            }

            //ddlPageSize_SelectedIndexChanged(sender, e);

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");

            BindAuditStepList();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditStepList();

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            ViewState["selectedAuditSteps"] = null;
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdAuditStepList.DataSource = null;
                grdAuditStepList.DataBind();
                BindProcess("P");
            }
            else
            {
                grdAuditStepList.DataSource = null;
                grdAuditStepList.DataBind();
                BindProcess("N");
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        

        //public string ShowActivityToBeDone(long RiskActivityId, long RiskCreationId)
        //{

        //    List<SPRiskActivityToBeDoneMapping_Result> a = new List<SPRiskActivityToBeDoneMapping_Result>();
        //    string processnonprocess = "";
        //    a = ProcessManagement.Getsp_RiskActivityToBeDoneMappingProcedure(Convert.ToInt32(RiskActivityId), Convert.ToInt32(RiskCreationId)).ToList();
        //    var remindersummary = a.OrderBy(entry => entry.ActivityTobeDone).ToList();
        //    if (remindersummary.Count > 0)
        //    {
        //        foreach (var row in remindersummary)
        //        {
        //            processnonprocess += row.ActivityTobeDone + " ,";
        //        }
        //    }
        //    return processnonprocess.Trim(',');
        //}

        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        protected void txtFilter_TextChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData();
            }
            else
            {
                BindData();
            }
        }

        private void BindData()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int processid = -1;
                int subprocessid = -1;
                int CustomerBranchId = -1;
                int verticalid = -1;
                String FilterText = String.Empty;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }

                if (ddlVertical.SelectedValue != "")
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        verticalid = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }

                List<int> verticalList = new List<int>();
                for (int i = 0; i < ddlVertical.Items.Count; i++)
                {
                    if (ddlVertical.Items[i].Selected)
                        verticalList.Add(Convert.ToInt32(ddlVertical.Items[i].Value));
                }

                List<long> auditStepMasterIDList = new List<long>();
                for (int i = 0; i < ddlAuditStepList.Items.Count; i++)
                {
                    if (ddlAuditStepList.Items[i].Selected)
                        auditStepMasterIDList.Add(Convert.ToInt32(ddlAuditStepList.Items[i].Value));
                }

                Branchlist.Clear();
                GetAllHierarchy(customerID, CustomerBranchId);
                Branchlist.ToList();

                if(ddlStatus.SelectedValue=="1")               
                {
                    var Riskcategorymanagementlist = Business.AdditionalRiskCreation.GetAuditStepExportForDeActivate(customerID, Branchlist, processid, verticalList, auditStepMasterIDList);
                    grdAuditStepList.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdAuditStepList.DataBind();

                    grdPageNumber.Attributes.Add("style", "display:block");

                    //grdAuditStepList.Visible = true;
                }
                else if (ddlStatus.SelectedValue == "0")
                {
                    var Riskcategorymanagementlist = Business.AdditionalRiskCreation.GetAuditStepExportForActivate(customerID, Branchlist, processid, verticalList, auditStepMasterIDList);
                    grdAuditStepList.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdAuditStepList.DataBind();

                    grdPageNumber.Attributes.Add("style", "display:block");

                    //grdAuditStepList.Visible = true;
                }

                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditStepExportForUpdate");
                    DataTable ExcelData = null;

                    if (Session["grdFilterAuditSteps"] != null)
                    {
                        DataView view = new System.Data.DataView((DataTable)Session["grdFilterAuditSteps"]);
                        ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "ControlNo", "ActivityDescription", "ControlDescription", "RiskRating", "AuditObjective", "ActivityTobeDone", "AuditStepRating", "ATBDId");

                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "Location";
                        exWorkSheet.Cells["A1"].AutoFitColumns(20);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "Vertical";
                        exWorkSheet.Cells["B1"].AutoFitColumns(20);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Process";
                        exWorkSheet.Cells["C1"].AutoFitColumns(20);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "Sub Process";
                        exWorkSheet.Cells["D1"].AutoFitColumns(20);

                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "Control No";
                        exWorkSheet.Cells["E1"].AutoFitColumns(15);

                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F1"].Value = "Risk Description";
                        exWorkSheet.Cells["F1"].AutoFitColumns(50);

                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G1"].Value = "Control Description";
                        exWorkSheet.Cells["G1"].AutoFitColumns(50);

                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H1"].Value = "Risk Rating";
                        exWorkSheet.Cells["H1"].AutoFitColumns(15);

                        exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I1"].Value = "Audit Objective";
                        exWorkSheet.Cells["I1"].AutoFitColumns(50);

                        exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J1"].Value = "Audit Steps";
                        exWorkSheet.Cells["J1"].AutoFitColumns(50);

                        exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K1"].Value = "Audit Step Rating";
                        exWorkSheet.Cells["K1"].AutoFitColumns(15);

                        exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L1"].Value = "ID";
                        exWorkSheet.Cells["L1"].AutoFitColumns(10);

                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 12])
                        {
                            col.Style.WrapText = true;
                            //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            //Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        //exWorkSheet.Protection.IsProtected = true;
                        //exWorkSheet.Protection.AllowSort = true;
                        //exWorkSheet.Protection.AllowAutoFilter = true;
                        //exWorkSheet.Protection.AllowDeleteRows = true;

                        //exWorkSheet.Cells["A:G"].Style.Locked = false;
                        //exWorkSheet.Cells["A:G"].AutoFilter = true;
                        //exWorkSheet.Cells["K:L"].Style.Locked = false;
                        //exWorkSheet.Cells["K:L"].AutoFilter = true;

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AuditStepExportForUpdate.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<SP_AdditionalRiskCreationFillView_Result> GetAllRiskControlMatrix(int Customerid, List<long> BranchID, int ProcessId, int SubprocessId, string Flag, string filter)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_AdditionalRiskCreationFillView_Result> riskcategorycreations = new List<SP_AdditionalRiskCreationFillView_Result>();

                if (Flag == "P")
                {
                    if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)
                        riskcategorycreations = (List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    else
                    {
                        entities.Database.CommandTimeout = 300;
                        riskcategorycreations = (from row in entities.SP_AdditionalRiskCreationFillView()
                                                 where row.CustomerID == Customerid
                                                 && !string.IsNullOrEmpty(row.ActivityTobeDone)
                                                 //&& row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();

                        HttpContext.Current.Cache.Insert("MasterListAuditSteps", riskcategorycreations, null, DateTime.Now.AddMinutes(1), System.Web.Caching.Cache.NoSlidingExpiration);

                    }

                    if (BranchID.Count > 0)
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();

                    if (ProcessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();

                    if (SubprocessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                    if (filter != "")
                        riskcategorycreations = riskcategorycreations
                                                 .Where(entry => entry.ControlNo.Contains(filter)
                                                 || entry.BranchName.Contains(filter)
                                                 || entry.VerticalName.Contains(filter)
                                                 || entry.ActivityTobeDone.Contains(filter)).ToList();
                }
                else
                {
                    if (HttpContext.Current.Cache["MasterListAuditSteps"] != null)
                        riskcategorycreations = (List<SP_AdditionalRiskCreationFillView_Result>)HttpContext.Current.Cache["MasterListAuditSteps"];
                    else
                    {
                        entities.Database.CommandTimeout = 300;
                        riskcategorycreations = (from row in entities.SP_AdditionalRiskCreationFillView()
                                                 where row.CustomerID == Customerid
                                                 && !string.IsNullOrEmpty(row.ActivityTobeDone)
                                                 //&& row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();

                        HttpContext.Current.Cache["MasterListAuditSteps"] = riskcategorycreations;
                    }

                    if (BranchID.Count > 0)
                        riskcategorycreations = riskcategorycreations.Where(entry => BranchID.Contains((long)entry.BranchId)).ToList();

                    if (ProcessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.ProcessId == ProcessId).ToList();

                    if (SubprocessId != -1)
                        riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == SubprocessId).ToList();

                    if (filter != "")
                        riskcategorycreations = riskcategorycreations
                                                   .Where(entry => entry.ControlNo.Contains(filter)
                                                   || entry.BranchName.Contains(filter)
                                                   || entry.VerticalName.Contains(filter)
                                                   || entry.ActivityTobeDone.Contains(filter)).ToList();
                }

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ControlNo)
                                                                 .ThenBy(entry => entry.ProcessId)
                                                                 .ThenBy(entry => entry.SubProcessId)
                                                                .ThenBy(entry => entry.ActivityTobeDone).ToList();
                }

                return riskcategorycreations;
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
            }
            else
            {
                if (ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                }
            }

            BindAuditStepList();

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");


            // ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditStepList();

            grdAuditStepList.DataSource = null;
            grdAuditStepList.DataBind();
           grdPageNumber.Attributes.Add("style", "display:none;");

            //if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            //{
            //    ddlPageSize_SelectedIndexChanged(sender, e);
            //}
            //else
            //{
            //    ddlPageSize_SelectedIndexChanged(sender, e);
            //}
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                List<int> ATBDIDList = new List<int>();

                for (int i = 0; i < grdAuditStepList.Rows.Count; i++)
                {
                    CheckBox chkRow = (CheckBox)grdAuditStepList.Rows[i].FindControl("chkRow");

                    if (chkRow != null)
                    {
                        if (chkRow.Checked)
                        {
                            string lblATBDId = (grdAuditStepList.Rows[i].FindControl("lblATBDId") as Label).Text;

                            if (lblATBDId != "")
                            {
                                ATBDIDList.Add(Convert.ToInt32(lblATBDId));
                            }
                        }
                    }
                }

                if (ATBDIDList.Count > 0)
                {
                    if(ddlStatus.SelectedValue=="0") //Status - In Active
                        saveSuccess = Business.AdditionalRiskCreation.ActiveDeactiveAuditSteps(ATBDIDList, true);/*Active*/
                    else if (ddlStatus.SelectedValue == "1")//Status - Active
                        saveSuccess = Business.AdditionalRiskCreation.ActiveDeactiveAuditSteps(ATBDIDList, false);/*DeActive*/
                }                   

                if (saveSuccess)
                {
                    cvDuplicateEntry.IsValid = false;

                    if (ddlStatus.SelectedValue == "0")
                        cvDuplicateEntry.ErrorMessage = "Selected Audit Steps Activated Successfully.";
                    else if (ddlStatus.SelectedValue == "1")
                        cvDuplicateEntry.ErrorMessage = "Selected Audit Step De-Activated Successfully.";

                    ddlVertical.ClearSelection();

                    BindData();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateSelectedValues()
        {
            try
            {
                ArrayList AuditStepsList = (ArrayList)ViewState["selectedAuditSteps"];

                if (AuditStepsList != null && AuditStepsList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdAuditStepList.Rows)
                    {
                        Label lblATBDId = (Label)gvrow.FindControl("lblATBDId");

                        if (lblATBDId != null && lblATBDId.Text != "")
                        {
                            int auditStepID = Convert.ToInt32(lblATBDId.Text);

                            if (AuditStepsList.Contains(auditStepID))
                            {
                                CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkRow");
                                myCheckBox.Checked = true;
                            }

                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SaveSelectedValues()
        {
            try
            {
                ArrayList AuditStepsList = new ArrayList();

                if (ViewState["selectedAuditSteps"] != null)
                {
                    AuditStepsList = (ArrayList)ViewState["selectedAuditSteps"];

                    foreach (GridViewRow gvrow in grdAuditStepList.Rows)
                    {
                        Label lblATBDId = (Label)gvrow.FindControl("lblATBDId");

                        if (lblATBDId != null && lblATBDId.Text!="")
                        {
                            bool result = ((CheckBox)gvrow.FindControl("chkRow")).Checked;
                            int auditStepID = Convert.ToInt32(lblATBDId.Text);

                            //Check in the Session
                            if (result)
                            {
                                if (!AuditStepsList.Contains(auditStepID))
                                    AuditStepsList.Add(auditStepID);
                            }
                            else
                                AuditStepsList.Remove(auditStepID);
                        }
                    }
                }

                if (AuditStepsList != null && AuditStepsList.Count > 0)
                    ViewState["selectedAuditSteps"] = AuditStepsList;
            }            
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdAuditStepList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox headerchk = (CheckBox)grdAuditStepList.HeaderRow.FindControl("chkHeader");
                CheckBox childchk = (CheckBox)e.Row.FindControl("chkRow");
                childchk.Attributes.Add("onclick", "javascript:checkUncheckRow('" + headerchk.ClientID + "')");
            }
        }

        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")                
                    BindData();                
                else                
                   BindData();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditStepList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditStepList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}

                //if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                //    BindData("P");
                //else
                //    BindData("N");

                grdAuditStepList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                SaveSelectedValues();
                BindData();
                PopulateSelectedValues();

                int count = Convert.ToInt32(GetTotalPagesCount());

                if (count > 0)
                {
                    int gridindex = grdAuditStepList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                if (count > 0)
                {
                    DropDownListPageNo.DataTextField = "ID";
                    DropDownListPageNo.DataValueField = "ID";

                    for (int i = 1; i <= count; i++)
                    {
                        string chkPageID = i.ToString();
                        DropDownListPageNo.Items.Add(chkPageID);
                    }

                    DropDownListPageNo.DataBind();

                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

    
       
    }
}