﻿<%@ Page Title="Audit Steps" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AdditionalRiskCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AdditionalRiskCreation" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        ul {
            margin-left:0% !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        #ContentPlaceHolder1_rdCD label {
            margin-left: 5px;
            margin-right: 5px;
        }
    </style>
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
        .topnav-right {
  float: right;
}
        .MarginTop {
            margin-top: 13px;
        }
    </style> 

    <script type="text/javascript">
        function fopenpopup() {
            $('#txtATBD').val('');
            $('#txtCDEC').val('');            
            $('#ddlRatingItemTemplate').val = -1;
            $('#divAuditorDialog').modal('show');
            return true;
        }

        function CloseWin() {
            $('#divAuditorDialog').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller() {
            setInterval(CloseWin, 30000);
        };

        function OpenModelPopupAddDetails() {
            $('#DivAddNewDetials').modal('show');
        }
    </script>



    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeCombobox() {
            $("#<%= ddlProcess.ClientID %>").combobox();
            $("#<%= ddlSubProcess.ClientID %>").combobox();
            $("#<%= ddlFilterLocation.ClientID %>").combobox();
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        $(document).ready(function () {
           // setactivemenu('Audit Steps');
            fhead('Additional Risk Creation');
        });

    </script>
    <style type="text/css">
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <asp:UpdatePanel ID="upComplianceTypeList" runat="server">  <%--UpdateMode="Conditional"--%>
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <div class="col-md-12 colpadding0" style="margin: 5px">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                             </div>
                            <div class="col-md-12 colpadding0">
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px; width:20%;">                                 
                                     <div class="col-md-2 colpadding0" style="margin-right: 5px;" >
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" Selected="True"/>
                                         <asp:ListItem Text="100" />
                                    </asp:DropDownList>                              
                                </div> 
                                 <div class="col-md-3 colpadding0" style="margin-top: 5px; width:20%; color:#999; display:none;"> 
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                    <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                    <%--<asp:ListItem Text="Non Process" Value="Non Process" />--%>
                                    <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                    </asp:RadioButtonList>
                                 </div> 

                                <div class="col-md-3 colpadding0" style="margin-top: 5px;width:20%; color:#999;"> 
                                    </div>
                                 <div class="col-md-3 colpadding0" style="margin-top: 5px;width:20%; color:#999;"> 
                                    </div>
                                 <div class="col-md-1 colpadding0" style="margin-top: 5px;width:30%; color:#999;">
                                     <div class="topnav-right"style="width:62%;"> 
                                         <asp:Button ID="btnAddNew" Text="Add New" class="btn btn-primary" CausesValidation="false" OnClientClick ="OpenModelPopupAddDetails()"
                                         runat="server" style="float: left;" /> 
                                     <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-primary" CausesValidation="false" 
                                         runat="server" OnClick="lbtnExportExcel_Click" style="float: right;" /> 
                                      </div> 
                                     </div>
                                <div class="col-md-2 colpadding0" style="margin-top: 5px;width:10%; color:#999;"> 
                                    <div style="width:90%;">                                          
                                    <asp:Button ID="btnDeActivate" Text="De-Activate" class="btn btn-primary"  runat="server" style="float:right;"
                                        PostBackUrl="../InternalAuditTool/DeActivateAuditSteps.aspx" CausesValidation="false"  />                                
                                    </div>
                                </div>
                            </div>                               
                            <div class="clearfix"></div>    
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%; ">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="95%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15" Width="95%" Height="32px">
                                    </asp:DropDownListChosen>
                                 </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0">                               
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                    <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px" DataPlaceHolder="Process"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                    
                                 </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                    <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px" DataPlaceHolder="Sub Process"  
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                   
                                 </div>
                                 <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                 <%{%> 
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                      <asp:DropDownListChosen ID="ddlVertical" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px" DataPlaceHolder="Vertical"  
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                                                     
                                 </div> 
                                <%}%>                         
                                 <div class="col-md-3 colpadding0 " style="margin-top: 5px;width:40%;">                                
                                   <asp:TextBox runat="server" ID="txtFilter" PlaceHolder="Type to Search" AutoPostBack="true" OnTextChanged="txtFilter_TextChanged" CssClass="form-control" Style="margin-bottom: 10px; width: 95%;" />                                 
                                </div>      
                               </div>
                    </div>
                    <div class="clearfix"></div>
                    <div style="margin-bottom: 4px">
                        &nbsp;
                        <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false"
                            OnSorting="grdRiskActivityMatrix_Sorting" OnRowDataBound="grdRiskActivityMatrix_RowDataBound" PageSize="50" AllowPaging="true" AutoPostBack="true"
                            CssClass="table" GridLines="None" Width="100%" AllowSorting="true" ShowHeaderWhenEmpty="true"
                            DataKeyNames="RiskActivityID" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true"
                            OnRowCommand="grdRiskActivityMatrix_RowCommand">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Control No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Branch">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("BranchName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Vertical">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="lblVerticalName" runat="server" Text='<%# Eval("VerticalName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                

                                <asp:TemplateField HeaderText="Risk Description">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 150px;"> 
                                            <asp:Label ID="LabelD" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Control Description">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 150px;"> 
                                            <asp:Label ID="LabelCD" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ControlDescription") %>' ToolTip='<%# Eval("ControlDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Audit Steps">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 150px;"> 
                                        <asp:Label ID="lblActivityToBeDone" runat="server" Text='<%# Eval("ActivityToBeDone") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ActivityToBeDone") %>'></asp:Label>
                                            </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Rating" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActivityRating" runat="server" Text='<%# Eval("RiskRating") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="VerticalId" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVerticalIdItemTemplate" runat="server" Text='<%# Eval("VerticalId") %>'></asp:Label>
                                        <asp:Label ID="lblBranchIdItemTemplate" runat="server" Text='<%# Eval("BranchId") %>'></asp:Label>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="RATId" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRATIDItemTemplate" runat="server" Text='<%# Eval("RiskActivityID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Task Id" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lbltaskidItemTemplate" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                        <asp:Label ID="lblATBDIdItemTemplate" runat="server" Text='<%# Eval("ATBDId") %>'></asp:Label>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="New_AdditionalRiskCreation" OnClientClick="fopenpopup()" data-toggle="tooltip" data-placement="top" ToolTip="Add New Risk Details"
                                            CommandArgument='<%# Eval("RiskCreationId") + "," + Eval("RiskActivityID") %>' CausesValidation="false"><img src="../../Images/add_icon_new.png"/></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_AdditionalRiskCreation" OnClientClick="fopenpopup()" data-toggle="tooltip" data-placement="top" ToolTip="Edit Risk Details"
                                            CommandArgument='<%# Eval("RiskCreationId") + "," + Eval("RiskActivityID") %>' CausesValidation="false"><img src="../../Images/edit_icon_new.png"/></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <HeaderStyle BackColor="#ECF0F1" />

                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div style="float: right;">
                      <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                     </asp:DropDownListChosen>  
                </div>
                    <div class="col-md-12 colpadding0">

                        <div class="col-md-5 colpadding0">
                            <div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                    </p>
                                </div>
                            </div>
                        </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                   <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page
                                                                 
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>                                  <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                    </div>
                    </section>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="divAuditorDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 95%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAuditorDialog1">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk Description</label>
                                        <asp:TextBox runat="server" ID="txtFName" Width="350px" Style="margin-left: 130px;" TextMode="MultiLine" CssClass="form-control" Height="100px" Enabled="false" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Location can not be empty." ControlToValidate="txtFName"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:RadioButtonList runat="server" ID="rdCD" RepeatDirection="Horizontal" AutoPostBack="true" Style="color: #999; margin-left: 165px;"
                                            RepeatLayout="Flow" OnSelectedIndexChanged="rdCD_SelectedIndexChanged">
                                            <asp:ListItem Text="Same" Value="Same" Selected="True" />
                                            <asp:ListItem Text="New" Value="New" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Control Description</label>
                                        <asp:TextBox runat="server" ID="txtCDEC" Width="350px" TextMode="MultiLine" CssClass="form-control" Height="100px" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Control Description can not be empty." ControlToValidate="txtCDEC"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                    </div>
                                    <br />
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Audit Step</label>
                                        <asp:TextBox runat="server" ID="txtATBD" Width="350px" Height="100px" TextMode="MultiLine" CssClass="form-control" Style="margin-left: 12px;" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Required Activity to be Done" ControlToValidate="txtATBD"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                    </div>
                                    <br />
                                    <div style="margin-bottom: 7px; display:none;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Rating</label>
                                        <asp:DropDownList ID="ddlRatingItemTemplate" runat="server" class="form-control m-bot15" Style="width: 220px; margin-left: 127px;">
                                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="True">Major</asp:ListItem>
                                            <asp:ListItem Value="2">Moderate</asp:ListItem>
                                            <asp:ListItem Value="3">Minor</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:CompareValidator ErrorMessage="Please select Rating." ControlToValidate="ddlRatingItemTemplate"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnPopupSave" OnClick="btnPopupSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="DivAddNewDetials" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 98%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="upAddNewDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 4px; margin-left: 10px;">
                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="PopupValidationSummary" />
                            <asp:CustomValidator ID="CvPoupvalidator" runat="server" EnableClientScript="False" Style="margin-left: 10px;"
                                ValidationGroup="PopupValidationSummary" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                        </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownListChosen ID="ddlLegalEntityPop" runat="server" AutoPostBack="true" DataPlaceHolder="Unit" 
                                                class="form-control m-bot15" Width="90%" Height="32px" OnSelectedIndexChanged="ddlLegalEntityPop_SelectedIndexChanged"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Unit." ControlToValidate="ddlLegalEntityPop" ID="RequiredFieldValidator7"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen ID="ddlSubEntity1Pop" runat="server" AutoPostBack="true"
                                                class="form-control m-bot15" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1Pop_SelectedIndexChanged"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Branch." ControlToValidate="ddlSubEntity1Pop" ID="RequiredFieldValidator5"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen ID="ddlSubEntity2Pop" runat="server" AutoPostBack="true" class="form-control m-bot15"
                                                Width="90%" Height="32px" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2Pop_SelectedIndexChanged"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Branch." ControlToValidate="ddlSubEntity2Pop" ID="RequiredFieldValidator6"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Pop" AutoPostBack="true"
                                                class="form-control m-bot15" DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3Pop_SelectedIndexChanged"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                            <asp:CustomValidator ID="CustomValidator5" runat="server" EnableClientScript="true"
                                                ValidationGroup="PopupValidationSummary" />
                                            <asp:Label ID="Label3" runat="server" Style="color: Red"></asp:Label>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity4Pop" AutoPostBack="true"
                                                DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity4Pop_SelectedIndexChanged"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                class="form-control m-bot15" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                        <%{%>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlVerticalPopup" DataPlaceHolder="Verticals"
                                                AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Vertical." ControlToValidate="ddlVerticalPopup" ID="RequiredFieldValidator16"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <%}%>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownCheckBoxes ID="ddlProcessPopup" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlProcessPopup_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 100%; height: 50px;">
                                                <Style SelectBoxWidth="230" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlProcessPopup" ID="RequiredFieldValidator7"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownCheckBoxes ID="ddlSubProcessPopup" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 100%; height: 50px;">
                                                <Style SelectBoxWidth="230" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select SubProcess" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:RequiredFieldValidator ErrorMessage="Please Select SubProcess." ControlToValidate="ddlSubProcessPopup" ID="RequiredFieldValidator8"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlAuditee" DataPlaceHolder="Auditee"
                                                AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Person Responsible." ControlToValidate="ddlAuditee" ID="RequiredFieldValidator9"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlRCMType" DataPlaceHolder="RCM Type"
                                                AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlRCMType_SelectedIndexChanged"                                                
                                                class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                                <asp:ListItem Text="Select RCM Type" Value="-1" Selected="True"/>
                                                <asp:ListItem Text="Both" Value="1"/>
                                                <asp:ListItem Text="ARS" Value="2"/> 
                                                <asp:ListItem Text="IFC" Value="3"/>   
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select RCMType." ControlToValidate="ddlRCMType" ID="RequiredFieldValidator14"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;" id="divKey" runat="server">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlKeyControl" DataPlaceHolder="Key Control"
                                                AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                                <asp:ListItem Text="Select Key Type" Value="-1" Selected="True"/> 
                                                <asp:ListItem Text="Yes" Value="1"/> 
                                                <asp:ListItem Text="No" Value="2"/>
                                            </asp:DropDownListChosen>
                                             <asp:RequiredFieldValidator ErrorMessage="Please Select Key Type." ControlToValidate="ddlKeyControl" ID="RequiredFieldValidator15"
                                                runat="server" InitialValue="-1" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-6 colpadding0">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #999;">
                                                Objective Ref./Control No.</label>
                                            <asp:TextBox runat="server" ID="tbxControlNo" Width="90%" AutoComplete="off" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Control No." ControlToValidate="tbxControlNo" ID="RequiredFieldValidator8"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-6 colpadding0">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #999;">
                                                Risk</label>
                                            <asp:TextBox runat="server" ID="tbxRisk" Width="90%" TextMode="MultiLine" AutoComplete="off" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Risk." ControlToValidate="tbxRisk" ID="RequiredFieldValidator2"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-6 colpadding0">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #999;">
                                                Control Objective</label>
                                            <asp:TextBox runat="server" ID="tbxConrolObj" Width="90%" AutoComplete="off" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Control Objective." ControlToValidate="tbxConrolObj" ID="RequiredFieldValidator10"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-6 colpadding0">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #999;">
                                                Control Description</label>
                                            <asp:TextBox runat="server" ID="tbxControlDesc" Width="90%" AutoComplete="off" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Control Description." ControlToValidate="tbxControlDesc" ID="RequiredFieldValidator11"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-6 colpadding0">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 400px; display: block; float: left; font-size: 13px; color: #999;">
                                                Audit Methodology</label>
                                            <asp:TextBox runat="server" ID="tbxAuditObjective" Width="90%" AutoComplete="off" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Audit Methodology." ControlToValidate="tbxAuditObjective" ID="RequiredFieldValidator12"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                        <div class="col-md-6 colpadding0">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #999;">
                                                Audit Steps</label>
                                            <asp:TextBox runat="server" ID="tbxAuditSteps" Width="90%" AutoComplete="off" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Audit Steps." ControlToValidate="tbxAuditSteps" ID="RequiredFieldValidator13"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-6 colpadding0"> 
                                            <label style="width: 400px; display: block; float: left; font-size: 13px; color: #999;">
                                               Pre-Requisite List</label><%--Process Description--%>
                                            <asp:TextBox runat="server" ID="tbxPreRequisite" Width="90%" AutoComplete="off" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div style="margin-bottom: 10px; text-align: center; margin-top: 25px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                        ValidationGroup="PopupValidationSummary" />
                                    <asp:Button Text="Close" runat="server" ID="Button1" CssClass="btn btn-primary" OnClientClick="javascript:window.location.reload()" data-dismiss="modal" />
                                </div>
                                <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
