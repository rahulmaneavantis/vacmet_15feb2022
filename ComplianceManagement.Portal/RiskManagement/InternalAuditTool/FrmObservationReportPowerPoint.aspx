﻿<%@ Page Title="Observation Report Power Point" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true"
     CodeBehind="FrmObservationReportPowerPoint.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.FrmObservationReportPowerPoint" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">

        $(document).ready(function () {
            //setactivemenu('Power Point Report');
            fhead('Power Point Report');
        });

        function ShowProcessOrderPopUp() {
            $('#divProcessOrder').modal('show');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 ">
                        <section class="panel">
               
                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>  
                   
                    <div class="col-md-12 colpadding0">
                        
                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 40px; margin-right: 5px;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    </div> 
                         
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" 
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   class="form-control m-bot15" Width="95%" Height="32px"
                                     DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>

                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" 
                                        class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>

                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true"
                                         class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>

                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" 
                                         class="form-control m-bot15" Width="95%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>

                                </div>

                        <div class="clearfix"></div>
                        
                            <div class="col-md-12 colpadding0"> 
                             
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true"
                                         class="form-control m-bot15" Width="95%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                          DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>

                                   <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                   <%-- <asp:DropDownList runat="server" ID="ddlVertical" AutoPostBack="true" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged"
                                        class="form-control m-bot15" Width="80%" Height="32px" Style="float: left; width:95%;">
                                    </asp:DropDownList>--%> 

                                         <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true"
                                         class="form-control m-bot15" Width="95%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                          DataPlaceHolder="Select Vertical" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                    </asp:DropDownListChosen> 
                                 </div> 

                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="95%" Height="32px"
                                        AutoPostBack="true" DataPlaceHolder="Financial Year" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">
                                    <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15" DataPlaceHolder="Scheduling Type"  
                                        Width="95%" Height="32px" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged">                                      
                                    </asp:DropDownListChosen>
                                </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width:20%">                     
                                <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" DataPlaceHolder="Select Period"  
                               AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" Width="95%" Height="32px" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged">
                                </asp:DropDownListChosen>

                                        </div>                
                             </div>     
                        
                         <div class="clearfix"></div>      
                        <div class="col-md-12 colpadding0"> 
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">  </div>                                                                      

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">  </div>

                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">
                            <asp:DropDownListChosen runat="server" ID="ddlMultiColor" class="form-control m-bot15" Width="95%" Height="32px"
                         AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                  AutoPostBack="true" OnSelectedIndexChanged="ddlMultiColor_OnSelectedIndexChanged" DataPlaceHolder="Select Color"/>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">  </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">  </div>
                            </div> 
                         <div class="clearfix"></div>       
                                                                                          
                                <div style="margin-bottom: 4px">                                       
                                      <asp:GridView runat="server" ID="grdAudits" AutoGenerateColumns="false"  ShowHeaderWhenEmpty="true"
                                    PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                   ShowFooter="true" > <%--OnRowCommand="grdRiskActivityMatrix_RowCommand"--%>
                                    <Columns>
                                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                          <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                    <asp:Label runat="server" ID="lblBranchname" Text='<%# Eval("BranchName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Vertical">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                    <asp:Label runat="server" ID="lblVerticalName" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Financial Year">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Period">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75;">
                                                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("TermName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>        

                                                        <asp:TemplateField ItemStyle-Width="5%" HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                 <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                                              <ContentTemplate>
                                                                <asp:Button ID="btnExportDoc" runat="server" CssClass="btn btn-search" Text="Export to PowerPoint" OnClick="btnExportDoc_Click" 
                                                                    Enabled='<%#CheckEnable(Convert.ToInt32(Eval("CustomerBranchId")),Convert.ToString(Eval("TermName")),Convert.ToString(Eval("FinancialYear")),Convert.ToInt32(Eval("VerticalID"))) %>' 
                                                                    CommandArgument='<%# Eval("TermName") + "," + Eval("CustomerBranchId")+ "," + Eval("FinancialYear") +","+ Eval("VerticalID")+","+ Eval("BranchName") %>'></asp:Button>
                                                             </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnExportDoc" />                                                       
                                                    </Triggers>
                                                                       </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-Width="15%" HeaderText="Process Order" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnProcessOrder" runat="server" OnClientClick="ShowProcessOrderPopUp()" OnClick="btnProcessOrder_Click"                                                             
                                                                    CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("TermName") + "," + Eval("CustomerBranchId")+ "," + Eval("FinancialYear") +","+ Eval("VerticalID") %>'>
                                                                    <img src="../../Images/View-icon-new.png" alt="Specify Process Order" data-toggle="tooltip" data-placement="top" title="Specify Process Order" /> </asp:LinkButton>
                                                            </ItemTemplate>                                                            
                                                        </asp:TemplateField> 

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />   
                                           <PagerSettings Visible="false" />                      
                                    <PagerTemplate>
                                      <%--  <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                                        <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                    AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                                </div>
                              
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                        <%--    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                      </section>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <%-- <asp:PostBackTrigger ControlID="btnExportDoc" />--%>
        </Triggers>
    </asp:UpdatePanel>


     <div class="modal fade" id="divProcessOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel" >Specify Process Order for Power Point Presentation</h4>
                </div>

                <div class="modal-body">

                    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ProcessOrderPopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvProcessOrderPopUp" runat="server" EnableClientScript="true"
                                        ValidationGroup="ProcessOrderPopUpValidationGroup" Display="None" />
                                </div>
                            </div>

                            <div style="height: 400px; overflow-y: auto;">
                                <table width="100%" style="text-align: left;">
                                    <tr>
                                        <td valign="top">
                                            <asp:GridView runat="server" ID="grdProcessOrder" AutoGenerateColumns="false"
                                                 CssClass="table" GridLines="None" Width="100%"
                                                AllowSorting="true">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Process">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top"
                                                                ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ProcessID" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessId") %>' data-toggle="tooltip" data-placement="top"
                                                                ToolTip='<%# Eval("ProcessId") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Order">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtProcessOrder" runat="server" CssClass="form-control" type="number" data-toggle="tooltip" data-placement="top" Style="text-align: center; width: 40%;"
                                                                Text='<%# Eval("ProcessOrder") != null ? Convert.ToString(Eval("ProcessOrder")) : "" %>' ToolTip="Specify the Process Order for Power Point Presentation"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtProcessOrder" runat="server" ErrorMessage="Only Numbers allowed"
                                                                ValidationExpression="\d+" ValidationGroup="ProcessOrderPopUpValidationGroup"></asp:RegularExpressionValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                          
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>   
                            <div style="text-align:center">
                                  <asp:Button ID="btnProcessOrderSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="btnProcessOrderSave_Click"
                                                ValidationGroup="ProcessOrderPopUpValidationGroup" />
                                </div>
                        </ContentTemplate>
                        <%-- <Triggers>
                                <asp:PostBackTrigger ControlID="btnProcessOrderSave" />
                            </Triggers>--%>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>


</asp:Content>
