﻿<%@ Page Title="Assign Entities To Audit Manager" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AssignEntitiesToAuditManager.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AssignEntitiesToAuditManager" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width: 95% !important;
        }

        div.dd_chk_drop {
            background-color: white;
            border: 1px solid #CCCCCC;
            text-align: left;
            z-index: 1000;
            left: -1px;
            top: 32px !important;
            min-width: 100%;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <style type="text/css">
        ul.multiselect-container.dropdown-menu {
            width: 100%;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }
    </style>


    <script type="text/javascript">
        function fopenpopup() {
            $('#AssignEntitiesDialog').modal('show');
        }
        function CloseEvent() {
            $('#AssignEntitiesDialog').modal('hide');
            window.location.reload();
        };

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("Are you sure you want reassign selected compliances to ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
        function fopenpopupEdit() {
            $('#AssignEntitiesDialogEdit').modal('show');
        }
        function CloseEventEdit() {
            $('#AssignEntitiesDialogEdit').modal('hide');
            window.location.reload();
        };
    </script>


    <script type="text/javascript">


        function openModal() {
            $('#divReAssign').modal('show');
            return true;
        }

        function closeModal() {
            $('#divReAssign').modal('hide');
            window.location.reload();
        };

        function ConfirmEvent() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_Event_value";
            var oldUser = $("#<%=ddlUser.ClientID%> option:selected").text();
            var newUser = $("#<%=ddlREAssignUser.ClientID%> option:selected").text();

            if (newUser != "Select User") {
                if (confirm("Are you sure you want re-assign selected Process of " + oldUser + "?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <style type="text/css">
        .three-inline-buttons .button {
            margin-left: 2px;
            margin-right: 12px;
        }

        .three-inline-buttons {
            display: table;
            margin: 0 auto;
        }

        @media only screen and (max-width: 960px) {
            .three-inline-buttons .button {
                width: 100%;
                margin: 20px;
                text-align: center;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div style="margin-bottom: 7px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="EntitiesValidationGroup" />
                        <asp:CustomValidator ID="CvEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="EntitiesValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        <asp:Label runat="server" ID="lblerrormsgMainscreen" Style="color: Red"></asp:Label>
                    </div>
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                               <div class="col-md-12 colpadding0">               
                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-1 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                                                        
                                    <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="three-inline-buttons" style="float:right">
                                       
                                            <asp:LinkButton Text="Add New"  runat="server" OnClientClick="fopenpopup()" ID="btnAddComplianceType" CssClass="btn btn-primary button" OnClick="btnAddComplianceType_Click" />
                                       
                                            <asp:LinkButton Text="Edit Entities Assignment" runat="server" OnClientClick="fopenpopupEdit()" ID="btnAddComplianceTypeEdit" CssClass="btn btn-primary button" OnClick="btnAddComplianceTypeEdit_Click" />
                                     
                                             <asp:Button Text="Re-Assign" runat="server" ID="btnReAssign" OnClick="btnReAssign_Click" CssClass="btn btn-advanceSearch button"/>
                                         </div>
                                   </div>                                 
                               </div>
                             <div class="clearfix"></div>                                          
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15"  Width="95%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>                               
                            <div class="clearfix"></div>
                              <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0" style="margin-top:5px;">                                 
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Sub Unit"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" Width="95%" Height="32px" class="form-control m-bot15">
                                    </asp:DropDownListChosen>                          
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top:5px;">                                 
                                     <asp:DropDownListChosen runat="server" ID="ddlFilterUsers" class="form-control m-bot15"  Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="User" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                                     </asp:DropDownListChosen>                     
                                </div>
                            </div>                            
                           
                            <div style="margin-top:50px"> 
                                    
                                <asp:GridView runat="server" ID="grdAssignEntities" AutoGenerateColumns="false" GridLines="None"
                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table"  Width="100%"
                                AllowSorting="true" 
                                DataKeyNames="ID" OnPageIndexChanging="grdAssignEntities_PageIndexChanging"
                                OnRowCommand="grdAssignEntities_RowCommand">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>                           
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="Label12" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    
                                     <asp:TemplateField HeaderText="Role">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="LblRole" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ISDepartmentOrAuditHead") %>' ToolTip='<%# Eval("ISDepartmentOrAuditHead") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>       
                                    
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lbldepartment" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("DepartmentName") %>' ToolTip='<%# Eval("DepartmentName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    
                                    <asp:TemplateField HeaderText="Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblProcess" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>  
                                    
                                  <asp:TemplateField HeaderText="Action" ItemStyle-Width="7%">
                                    <ItemTemplate>                                                               
                                        
                                        <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Entities"  CommandArgument='<%# Eval("BranchID") + "," + Eval("DepartmentID")+ "," + Eval("ProcessId")+ "," + Eval("ISDepartmentOrAuditHead") +","+ Eval("userID") %>'
                                            OnClientClick="return confirm('Are you certain you want to delete?');"><img src="../../Images/delete_icon_new.png" alt="Delete Process" title="Delete Process" data-toggle="tooltip"/></asp:LinkButton>                                
                                    </ItemTemplate>                            
                                </asp:TemplateField>                                                               
                                </Columns>
                                    <RowStyle CssClass="clsROWgrid"   />
                                    <HeaderStyle CssClass="clsheadergrid" />  
                                    <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerSettings Visible="false" />                  
                                    <PagerTemplate>                                       
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found.
                                    </EmptyDataTemplate>   
                                </asp:GridView>
                                <div style="float: right;">
                                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                  </asp:DropDownListChosen>  
                                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page</p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="AssignEntitiesDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content" style="height: 300px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAssignEntitiesDialog">
                        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
                            <ContentTemplate>
                                <div class="row Dashboard-white-widget">
                                    <div class="dashboard">
                                        <div class="col-lg-12 col-md-12 ">
                                            <section class="panel">     
                                                <header class="panel-heading tab-bg-primary ">
                                                  <ul id="rbl" class="nav nav-tabs"> 
                                                    <li class="active" id="liAuditHead" runat="server">
                                                        <asp:LinkButton Text="Audit Head" runat="server" ID="btnTabAuditHead" class=""  OnClick="btnTabAuditHead_Click"/>
                                                    </li>
                                                    <li class="" id="liManagement" runat="server">
                                                        <asp:LinkButton Text="Management" runat="server" ID="btnTabManagement" class="" OnClick="btnTabManagement_Click"/>  
                                                    </li>                                                  
                                                    <li class="" id="liDepartmentHead" runat="server">
                                                        <asp:LinkButton Text="Department Head" runat="server" ID="btnTabDepartmentHead" class=""  OnClick="btnTabDepartmentHead_Click"/>  
                                                    </li>                                                   
                                                    </ul>                        
                                                    </header>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div runat="server" id="divAuditHead" class="tab-pane active">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="AuditHeadValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="AuditHeadValidationGroup" />
                                        <asp:CustomValidator ID="AuditHeadDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="AuditHeadValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassageAuditHead" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlLegalEntityAuditHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                                OnSelectedIndexChanged="ddlLegalEntityAuditHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity1AuditHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1AuditHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity2AuditHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2AuditHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3AuditHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3AuditHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterLocationAuditHeadPopPup" AutoPostBack="true" DataPlaceHolder="Location"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFilterLocationAuditHeadPopPup_SelectedIndexChanged" Width="95%" Height="32px" class="form-control m-bot15">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlAuditHeadUsers"
                                                AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                class="form-control m-bot15" DataPlaceHolder="User" Width="95%" Height="32px">
                                            </asp:DropDownListChosen>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlAuditHeadUsers" runat="server" ValidationGroup="AuditHeadValidationGroup"
                                                Display="None" />

                                            <asp:CompareValidator ID="CVDept" ErrorMessage="Please Select User." ControlToValidate="ddlAuditHeadUsers"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AuditHeadValidationGroup"
                                                Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlProcessAuditHead" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:Button Text="Save" runat="server" ID="btnAuditHeadSave" OnClick="btnAuditHeadSave_Click" CssClass="btn btn-primary"
                                                ValidationGroup="AuditHeadValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="btnClose" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divManagement" class="tab-pane">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ManagementValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="ManagementValidationGroup" />
                                        <asp:CustomValidator ID="ManagementDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="ManagementValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassageManagement" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlLegalEntityManagementPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                                OnSelectedIndexChanged="ddlLegalEntityManagementPopPup_SelectedIndexChanged" DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity1ManagementPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity1ManagementPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity2ManagementPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity2ManagementPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3ManagementPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity3ManagementPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterLocationManagementPopPup" AutoPostBack="true" DataPlaceHolder="Location"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFilterLocationManagementPopPup_SelectedIndexChanged" Width="95%" Height="32px" class="form-control m-bot15">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlManagementUsers" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                class="form-control m-bot15" DataPlaceHolder="User" Width="95%" Height="32px">
                                            </asp:DropDownListChosen>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlManagementUsers" runat="server" ValidationGroup="ManagementValidationGroup"
                                                Display="None" />

                                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlManagementUsers"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ManagementValidationGroup"
                                                Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlProcessManagement" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:Button Text="Save" runat="server" ID="btnManagementSave" OnClick="btnManagementSave_Click" CssClass="btn btn-primary"
                                                ValidationGroup="ManagementValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="btnCloseManagement" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divDepartmentHead" class="tab-pane">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="DepartmentHeadValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="DepartmentHeadValidationGroup" />
                                        <asp:CustomValidator ID="DepartmentHeadDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="DepartmentHeadValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassageDepartmentHead" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlLegalEntityDepartmentHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                                OnSelectedIndexChanged="ddlLegalEntityDepartmentHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity1DepartmentHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity1DepartmentHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity2DepartmentHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity2DepartmentHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3DepartmentHeadPopPup" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity3DepartmentHeadPopPup_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:DropDownListChosen runat="server" ID="ddlFilterLocationDepartmentHeadPopPup" AutoPostBack="true" DataPlaceHolder="Location"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFilterLocationDepartmentHeadPopPup_SelectedIndexChanged" Width="95%" Height="32px" class="form-control m-bot15">
                                            </asp:DropDownListChosen>
                                        </div>
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlDepartmentHeadUsers" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                class="form-control m-bot15" DataPlaceHolder="User" Width="95%" Height="32px">
                                            </asp:DropDownListChosen>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlDepartmentHeadUsers" runat="server" ValidationGroup="DepartmentHeadValidationGroup"
                                                Display="None" />

                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlDepartmentHeadUsers"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="DepartmentHeadValidationGroup"
                                                Display="None" />

                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <asp:DropDownCheckBoxes ID="ddlDepartment" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:Button Text="Save" runat="server" ID="btnDepartmentHeadSave" OnClick="btnDepartmentHeadSave_Click" CssClass="btn btn-primary"
                                                ValidationGroup="DepartmentHeadValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="btnCloseDepartmentHead" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                </div>
                                <div class="clearfix" style="height: 50px"></div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AssignEntitiesDialogEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content" style="height: 300px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAssignEntitiesDialogEdit">
                        <asp:UpdatePanel ID="upComplianceEdit" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
                            <ContentTemplate>
                                <div class="row Dashboard-white-widget">
                                    <div class="dashboard">
                                        <div class="col-lg-12 col-md-12 ">
                                            <section class="panel">     
                                                <header class="panel-heading tab-bg-primary ">
                                                  <ul id="rblEdit" class="nav nav-tabs"> 
                                                    <li class="active" id="liAuditHeadEdit" runat="server">
                                                        <asp:LinkButton Text="Audit Head" runat="server" ID="btnTabAuditHeadEdit" class=""  OnClick="btnTabAuditHeadEdit_Click"/>
                                                    </li>
                                                    <li class="" id="liManagementEdit" runat="server">
                                                        <asp:LinkButton Text="Management" runat="server" ID="btnTabManagementEdit" class="" OnClick="btnTabManagementEdit_Click"/>  
                                                    </li>                                                  
                                                    <li class="" id="liDepartmentHeadEdit" runat="server">
                                                        <asp:LinkButton Text="Department Head" runat="server" ID="btnTabDepartmentHeadEdit" class=""  OnClick="btnTabDepartmentHeadEdit_Click"/>  
                                                    </li>                                                   
                                                    </ul>                        
                                                    </header>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div runat="server" id="divAuditHeadEdit" class="tab-pane active">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="AuditHeadValidationSummaryEdit" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="AuditHeadValidationGroupEdit" />
                                        <asp:CustomValidator ID="AuditHeadDuplicateEntryEdit" runat="server" EnableClientScript="False"
                                            ValidationGroup="AuditHeadValidationGroupEdit" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassageAuditHeadEdit" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlLegalEntityAuditHeadPopPupEdit" runat="server" AutoPostBack="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID=""
                                                class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                                OnSelectedIndexChanged="ddlLegalEntityAuditHeadPopPupEdit_SelectedIndexChanged"
                                                DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity1AuditHeadPopPupEdit" runat="server" AutoPostBack="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity1AuditHeadPopPupEdit"
                                                class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity1AuditHeadPopPupEdit_SelectedIndexChanged"
                                                DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity2AuditHeadPopPupEdit" runat="server" AutoPostBack="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity2AuditHeadPopPupEdit" class="form-control m-bot15"
                                                Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity2AuditHeadPopPupEdit_SelectedIndexChanged"
                                                DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity3AuditHeadPopPupEdit" runat="server" AutoPostBack="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity3AuditHeadPopPupEdit"
                                                class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity3AuditHeadPopPupEdit_SelectedIndexChanged"
                                                DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlFilterLocationAuditHeadPopPupEdit" runat="server" AutoPostBack="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlFilterLocationAuditHeadPopPupEdit"
                                                AutoPostBack="true" DataPlaceHolder="Location"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFilterLocationAuditHeadPopPupEdit_SelectedIndexChanged"
                                                Width="95%" Height="32px" class="form-control m-bot15">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlAuditHeadUsersEdit" runat="server" AutoPostBack="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlAuditHeadUsersEdit" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlAuditHeadUsersEdit_SelectedIndexChanged"
                                                AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                class="form-control m-bot15" DataPlaceHolder="User" Width="95%" Height="32px">
                                            </asp:DropDownListChosen>--%>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlAuditHeadUsersEdit" runat="server" ValidationGroup="AuditHeadValidationGroupEdit"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlAuditHeadUsersEdit"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AuditHeadValidationGroupEdit"
                                                Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlProcessAuditHeadEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:Button Text="Save" runat="server" ID="btnAuditHeadSaveEdit"
                                                OnClick="btnAuditHeadSaveEdit_Click" CssClass="btn btn-primary"
                                                ValidationGroup="AuditHeadValidationGroupEdit" />
                                            <asp:Button Text="Close" runat="server" ID="btnCloseEdit" OnClientClick="CloseEventEdit()"
                                                CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divManagementEdit" class="tab-pane">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ManagementValidationSummaryEdit" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="ManagementValidationGroupEdit" />
                                        <asp:CustomValidator ID="ManagementDuplicateEntryEdit" runat="server" EnableClientScript="False"
                                            ValidationGroup="ManagementValidationGroupEdit" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassageManagementEdit" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlLegalEntityManagementPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlLegalEntityManagementPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                                OnSelectedIndexChanged="ddlLegalEntityManagementPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity1ManagementPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity1ManagementPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity1ManagementPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity2ManagementPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity2ManagementPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity2ManagementPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity3ManagementPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity3ManagementPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity3ManagementPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="DropDownCheckBoxes1" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlFilterLocationManagementPopPupEdit" AutoPostBack="true" DataPlaceHolder="Location"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFilterLocationManagementPopPupEdit_SelectedIndexChanged" Width="95%" Height="32px" class="form-control m-bot15">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlManagementUsersEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlManagementUsersEdit" AllowSingleDeselect="false"
                                                AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlManagementUsersEdit_SelectedIndexChanged"
                                                DisableSearchThreshold="5"
                                                class="form-control m-bot15" DataPlaceHolder="User" Width="95%" Height="32px">
                                            </asp:DropDownListChosen>--%>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlManagementUsersEdit" runat="server" ValidationGroup="ManagementValidationGroupEdit"
                                                Display="None" />

                                            <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlManagementUsersEdit"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ManagementValidationGroupEdit"
                                                Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlProcessManagementEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Process" />
                                            </asp:DropDownCheckBoxes>
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:Button Text="Save" runat="server" ID="btnManagementSaveEdit" OnClick="btnManagementSaveEdit_Click" CssClass="btn btn-primary"
                                                ValidationGroup="ManagementValidationGroupEdit" />
                                            <asp:Button Text="Close" runat="server" ID="btnCloseManagementEdit" OnClientClick="CloseEventEdit()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divDepartmentHeadEdit" class="tab-pane">
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="DepartmentHeadValidationSummaryEdit" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="DepartmentHeadValidationGroupEdit" />
                                        <asp:CustomValidator ID="DepartmentHeadDuplicateEntryEdit" runat="server" EnableClientScript="False"
                                            ValidationGroup="DepartmentHeadValidationGroupEdit" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassageDepartmentHeadEdit" Style="color: Red"></asp:Label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlLegalEntityDepartmentHeadPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlLegalEntityDepartmentHeadPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                                OnSelectedIndexChanged="ddlLegalEntityDepartmentHeadPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity1DepartmentHeadPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity1DepartmentHeadPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity1DepartmentHeadPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity2DepartmentHeadPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity2DepartmentHeadPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity2DepartmentHeadPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlSubEntity3DepartmentHeadPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlSubEntity3DepartmentHeadPopPupEdit" class="form-control m-bot15" Width="95%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSubEntity3DepartmentHeadPopPupEdit_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:DropDownCheckBoxes ID="ddlFilterLocationDepartmentHeadPopPupEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlFilterLocationDepartmentHeadPopPupEdit" AutoPostBack="true" DataPlaceHolder="Location"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFilterLocationDepartmentHeadPopPupEdit_SelectedIndexChanged" Width="95%" Height="32px" class="form-control m-bot15">
                                            </asp:DropDownListChosen>--%>
                                        </div>
                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:DropDownCheckBoxes ID="ddlDepartmentHeadUsersEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                            
                                            <%--<asp:DropDownListChosen runat="server" ID="ddlDepartmentHeadUsersEdit" AutoPostBack="true"
                                                AllowSingleDeselect="false"
                                                OnSelectedIndexChanged="ddlDepartmentHeadUsersEdit_SelectedIndexChanged"
                                                DisableSearchThreshold="5"
                                                class="form-control m-bot15" DataPlaceHolder="User" Width="95%" Height="32px">
                                            </asp:DropDownListChosen>--%>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlDepartmentHeadUsersEdit" runat="server"
                                                ValidationGroup="DepartmentHeadValidationGroupEdit"
                                                Display="None" />

                                            <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please Select User."
                                                ControlToValidate="ddlDepartmentHeadUsersEdit"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="DepartmentHeadValidationGroupEdit"
                                                Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <asp:DropDownCheckBoxes ID="ddlDepartmentEdit" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Department" />
                                            </asp:DropDownCheckBoxes>
                                        </div>

                                        <div class="col-md-3 colpadding0" style="margin-top: 5px;">
                                            <asp:Button Text="Save" runat="server" ID="btnDepartmentHeadSaveEdit" OnClick="btnDepartmentHeadSaveEdit_Click" CssClass="btn btn-primary"
                                                ValidationGroup="DepartmentHeadValidationGroupEdit" />
                                            <asp:Button Text="Close" runat="server" ID="btnCloseDepartmentHeadEdit" OnClientClick="CloseEventEdit()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                </div>
                                <div class="clearfix" style="height: 50px"></div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="divReAssign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px; height: 800px;">
            <div class="modal-content"  style="width: 1150px; height: 600px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">×</button>
                </div>

                <div class="modal-body">
                    <asp:UpdatePanel ID="ReAssign" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-lg-12 col-md-12 ">
                             <section class="panel">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="VSReassignSummary" runat="server" Display="None" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ReAssignValidationGroup" />
                                    <asp:CustomValidator ID="cvReAssignAudit" runat="server" EnableClientScript="False"
                                        ValidationGroup="ReAssignValidationGroup" Display="None" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 20%;">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSizeReAssign" class="form-control m-bot15" Style="width: 70px; float: left"
                                            OnSelectedIndexChanged="ddlPageSizeReAssign_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="5"  Selected="True"  />
                                            <asp:ListItem Text="10"/>
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlUser" Width="90%" AutoPostBack="true"
                                            DataPlaceHolder="User" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" />
                                        <asp:Label ID="lblReAssign" Visible="false" runat="server" Style="color: #333;" />
                                    </div>
                                    
                                </div>
                                <div class="clearfix"></div>

                                 <div class="col-md-12 colpadding0">   
                                                                        
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen ID="ddlLegalEntityPopup" runat="server" AutoPostBack="true"
                                            class="form-control m-bot15" Width="90%" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            Height="32px" DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntityPopUp_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator7"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen ID="ddlSubEntity1Popup" runat="server" AutoPostBack="true"
                                            class="form-control m-bot15" Width="90%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1Popup_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlSubEntity1" ID="RequiredFieldValidator8"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />
                                    </div>
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen ID="ddlSubEntity2Popup" runat="server" AutoPostBack="true"
                                            class="form-control m-bot15" Width="90%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2Popup_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Unit." ControlToValidate="ddlSubEntity2" ID="RequiredFieldValidator9"
                                            runat="server" InitialValue="-1" ValidationGroup="ReAssignValidationGroup" Display="None" />
                                    </div>

                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Popup" AutoPostBack="true"
                                            class="form-control m-bot15" Width="90%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3Popup_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLocationPopup" AutoPostBack="true"
                                            class="form-control m-bot15" Width="90%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlLocationPopup_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                 </div>
                                 <div class="clearfix" ></div>
                                <div class="col-md-12 colpadding0">                                                                                                          
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                          <asp:DropDownListChosen runat="server" ID="ddlProcess" AutoPostBack="true"
                                            class="form-control m-bot15" Width="90%"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Process" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" >
                                        </asp:DropDownListChosen>
                                     </div>
                                    <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                     </div>
                                    <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                     </div>
                                    <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                     </div>
                                     <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlREAssignUser" Width="90%" AutoPostBack="true"
                                            CssClass="form-control m-bot15" DataPlaceHolder="New Process Owner to Assign"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlREAssignUser_SelectedIndexChanged" />

                                        <asp:CompareValidator ErrorMessage="Please Select New User to Assign." ControlToValidate="ddlREAssignUser"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ReAssignValidationGroup"
                                            Display="None" />
                                    </div>

                                    
                                </div>
                                <div class="clearfix" ></div>
                                </div>   
                           
                            <div runat="server" id="div2" style="margin-bottom: 7px; width: 100%; height: 350px; overflow-y: auto;">
                                <asp:GridView runat="server" ID="grdReassign" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" ShowHeaderWhenEmpty="true"
                                    GridLines="None" AllowSorting="true" Width="100%" CssClass="table" ShowFooter="false" OnRowDataBound="grdReassign_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </div>
                                                <asp:Label ID="lblBranchID" runat="server" Style="display: none;" Text='<%# Eval("BranchID") %>'></asp:Label>                                                                                                   
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                    <asp:Label ID="lblprocessname" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top"
                                                        ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblprocessId" Style="display: none;" Text='<%#Eval("ProcessID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                        
                                        <asp:TemplateField HeaderText="New User">
                                            <ItemTemplate>
                                                <asp:DropDownListChosen runat="server" ID="ddlGridReAssignUser" Width="61%" AutoPostBack="false"
                                                    CssClass="form-control m-bot15" DataPlaceHolder="New User to Assign" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                     <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <div class="clearfix"></div>
                            <div id="divSavePageSummary" runat="server" style="display: none;">
                                <div class="col-md-12 colpadding0" style="margin: 5px; margin-bottom: 40px;">
                                    <div class="col-md-6 colpadding0" style="margin: auto; text-align: right;">
                                        <asp:Button Text="Save" runat="server" ID="btnReassignEvent" OnClick="btnReassignAuditSave_Click"
                                            CssClass="btn btn-primary" ValidationGroup="ReAssignValidationSummary" OnClientClick="ConfirmEvent();" />
                                        <asp:Button Text="Close" runat="server" ID="btnReassignClose" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="closeModal()" />
                                    </div>

                                    <div class="col-md-6 colpadding0">
                                        <div class="table-paging" style="margin-bottom: 10px;">
                                            <asp:ImageButton ID="lBPreviousReAssign" CssClass="table-paging-left" runat="server"
                                                ImageUrl="~/img/paging-left-active.png" OnClick="lBPreviousReAssign_Click" />
                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNoReAssign" runat="server" Text=""></asp:Label>/
                                                     <asp:Label ID="lTotalCountReAssign" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                            <asp:ImageButton ID="lBNextReAssign" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png"
                                                OnClick="lBNextReAssign_Click" />
                                            <asp:HiddenField ID="TotalRowsReAssign" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </section>   
                                               
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            fhead('Unit Assignment');
        });
    </script>

</asp:Content>

