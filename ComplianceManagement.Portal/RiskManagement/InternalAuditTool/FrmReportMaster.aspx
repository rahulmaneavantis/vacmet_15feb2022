﻿<%@ Page Title="ReportMaster" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="FrmReportMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.FrmReportMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                 <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" CssClass="vdsummary"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                
                            <div class="col-md-4 colpadding0 entrycount" style="margin-top:5px;">
        <div class="col-md-4 colpadding0" >
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <p style="color: #999; margin-top: 5px;">Select Location </p>
        </div>

     <div class="col-md-8" >
         <asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15" AutoPostBack="true">
                    <%--OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"--%>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please select Location."
                  InitialValue="-1"  ControlToValidate="ddlLocation" runat="server" ValidationGroup="EventValidationGroup"
                    Display="None" />
          </div>  
    </div><div class="clearfix"></div>
       <div class="col-md-4" style="margin-top:5px;">
                    <asp:CheckBoxList class="BodyTxt" ID="ChkServicestopitch" runat="server">
                        <asp:ListItem Text="Observation Number" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Observation Title" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Observation" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Risk" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Root Cost" Value="5"></asp:ListItem>
                        <asp:ListItem Text="Financial Impact" Value="6"></asp:ListItem>
                        <asp:ListItem Text="Recommendation" Value="7"></asp:ListItem>
                        <asp:ListItem Text="Management Response" Value="8"></asp:ListItem>
                        <asp:ListItem Text="TimeLine" Value="9"></asp:ListItem>
                        <asp:ListItem Text="Person Responsible" Value="10"></asp:ListItem>
                        <asp:ListItem Text="Observation Rating" Value="11"></asp:ListItem>
                        <asp:ListItem Text="Observation Category" Value="12"></asp:ListItem>
                    </asp:CheckBoxList>
                </div><div class="clearfix"></div>
<div>
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"  ValidationGroup="EventValidationGroup" />
                </div>

      
</section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>






  


</asp:Content>
