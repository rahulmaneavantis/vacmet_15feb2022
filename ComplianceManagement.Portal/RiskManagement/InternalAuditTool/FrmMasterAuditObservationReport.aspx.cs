﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Spire.Presentation.Drawing.Animation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmMasterAuditObservationReport : System.Web.UI.Page
    {
      
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();

                ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        private void BindProcess(string flag)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                if (flag == "P")
                {
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("P", customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                else
                {
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("N", customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindData()
        {
            try
            {
                ApplyFilter = false;
                int userID = -1;
                int RoleID = -1;
               
                RoleID = -1;
                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                int customerID = -1;
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustBranchID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                String FinancialYear = String.Empty;
                String Period = String.Empty;


                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }

                if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    if (RoleID == -1)
                    {
                        RoleID = 4;
                    }
                    //Branchlist.Clear();
                    //List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    //Branchlist = branchids.ToList();
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                    var Branchlistloop = Branchlist.ToList();
                }
                else
                {

                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                    var Branchlistloop = Branchlist.ToList();
                }


               
                var detailView = InternalControlManagementDashboardRisk.getDataMasterReport(RoleID, customerID, userID, VerticalID, ProcessID, FinancialYear, Period, Branchlist);
              
                Session["grdDetailData"] = detailView;
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void lbtnExportExcelTest_Click(object sender, EventArgs e)
        {
            try
            {

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {

                        String FileName = String.Empty;
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Master Audit Observation");
                        DataTable ExcelData = null;
                                                
                        BindData();

                        DataView view = new System.Data.DataView(Session["grdDetailData"] as DataTable);

                        ExcelData = view.ToTable("Selected", false, "IssueNumber", "AuditArea", "Grouping", "StateShort", "State_Branch", "S_No", "Report_No_Date", "Business_Vertical", "State", "Hub", "Location", "Category", "SubCategory",
                                                                    "Observation", "Impact", "Root_Cause", "Audit_Recommendation", "Action_Plan", "Owner", "Owner_Name_detail", "Due_Date", "Response", "Closure_Status", "Remarks",
                                                                    "Reporting_AC_period", "AC_Closure", "Due_Date1", "Audit_area", "Report_no_State_Hub", "Business_Vertical1", "Observation_Root_Cause", "Action_Plan1", "Risk_Level", "Staff");
                        
                        var customer = UserManagementRisk.GetCustomer(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            if (item["Due_Date"] != null && item["Due_Date"] != DBNull.Value)
                                item["Due_Date"] = Convert.ToDateTime(item["Due_Date"]).ToString("dd-MMM-yyyy");

                            if (item["Due_Date1"] != null && item["Due_Date1"] != DBNull.Value)
                                item["Due_Date1"] = Convert.ToDateTime(item["Due_Date1"]).ToString("dd-MMM-yyyy");


                        }


                        FileName = "Master Audit Observation Report";

                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].AutoFitColumns(50);
                        exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));


                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].AutoFitColumns(50);
                        exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["A2"].Value = "Name of Group:";
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A2"].AutoFitColumns(50);
                        exWorkSheet.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));

                        exWorkSheet.Cells["B2"].Value = customer.Name;
                        //exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B2"].AutoFitColumns(50);
                        exWorkSheet.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));


                        exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));


                        exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));
                        exWorkSheet.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(216, 216, 216));




                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A3"].Value = "Type of Report:";
                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A3"].AutoFitColumns(50);

                        exWorkSheet.Cells["B3"].Value = FileName;
                        //exWorkSheet.Cells["B3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B3"].AutoFitColumns(50);
                        
                      


                        exWorkSheet.Cells["A6"].Style.Font.Bold = true;                        
                        exWorkSheet.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["A6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171,227,196));
                        exWorkSheet.Cells["A6"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["A6"].Style.Font.
                        exWorkSheet.Cells["A6"].Value = "Issue#";                        
                        exWorkSheet.Cells["A6"].AutoFitColumns(25);

                        exWorkSheet.Cells["B6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["B6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["B6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B6"].Value = "Audit Area";
                        exWorkSheet.Cells["B6"].AutoFitColumns(25);

                        exWorkSheet.Cells["C6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["C6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["C6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C6"].Value = "Grouping";
                        exWorkSheet.Cells["C6"].AutoFitColumns(25);


                        exWorkSheet.Cells["D6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["D6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["D6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D6"].Value = "State Short";
                        exWorkSheet.Cells["D6"].AutoFitColumns(25);


                        exWorkSheet.Cells["E6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["E6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171,227,196));
                        exWorkSheet.Cells["E6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E6"].Value = "State/Branch";
                        exWorkSheet.Cells["E6"].AutoFitColumns(25);

                        exWorkSheet.Cells["F6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["F6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["F6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F6"].Value = "#";
                        exWorkSheet.Cells["F6"].AutoFitColumns(25);

                        exWorkSheet.Cells["G6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["G6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["G6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G6"].Value = "2016 -17 Report#";
                        exWorkSheet.Cells["G6"].AutoFitColumns(25);

                        exWorkSheet.Cells["H6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["H6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["H6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H6"].Value = "Business Vertical";
                        exWorkSheet.Cells["H6"].AutoFitColumns(25);

                        exWorkSheet.Cells["I6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["I6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["I6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I6"].Value = "State";
                        exWorkSheet.Cells["I6"].AutoFitColumns(25);

                        exWorkSheet.Cells["J6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["J6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["J6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J6"].Value = "Hub";
                        exWorkSheet.Cells["J6"].AutoFitColumns(25);

                        exWorkSheet.Cells["K6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["K6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["K6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["K6"].Value = "Location";
                        exWorkSheet.Cells["K6"].AutoFitColumns(25);

                        exWorkSheet.Cells["L6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["L6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["L6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["L6"].Value = "Category";
                        exWorkSheet.Cells["L6"].AutoFitColumns(25);

                        exWorkSheet.Cells["M6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["M6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["M6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["M6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["M6"].Value = "Sub-Category";
                        exWorkSheet.Cells["M6"].AutoFitColumns(25);

                        exWorkSheet.Cells["N6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["N6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["N6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["N6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["N6"].Value = "Observation";
                        exWorkSheet.Cells["N6"].AutoFitColumns(25);

                        exWorkSheet.Cells["O6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["O6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["O6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["O6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["O6"].Value = "Impact";
                        exWorkSheet.Cells["O6"].AutoFitColumns(25);

                        exWorkSheet.Cells["P6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["P6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["P6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["P6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["P6"].Value = "Root Cause";
                        exWorkSheet.Cells["P6"].AutoFitColumns(25);

                        exWorkSheet.Cells["Q6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["Q6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["Q6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["Q6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["Q6"].Value = "Audit Recommendation";
                        exWorkSheet.Cells["Q6"].AutoFitColumns(25);

                        exWorkSheet.Cells["R6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["R6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["R6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["R6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["R6"].Value = "Action Plan";
                        exWorkSheet.Cells["R6"].AutoFitColumns(25);

                        exWorkSheet.Cells["S6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["S6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["S6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["S6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["S6"].Value = "Owner";
                        exWorkSheet.Cells["S6"].AutoFitColumns(25);

                        exWorkSheet.Cells["T6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["T6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["T6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["T6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["T6"].Value = "Owner Name(Where ever possible for now)";
                        exWorkSheet.Cells["T6"].AutoFitColumns(25);

                        exWorkSheet.Cells["U6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["U6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["U6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["U6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["U6"].Value = "Due Date";
                        exWorkSheet.Cells["U6"].AutoFitColumns(25);

                        exWorkSheet.Cells["V6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["V6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["V6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["V6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["V6"].Value = "Response";
                        exWorkSheet.Cells["V6"].AutoFitColumns(25);

                        exWorkSheet.Cells["W6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["W6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["W6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["W6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["W6"].Value = "Closure Status";
                        exWorkSheet.Cells["W6"].AutoFitColumns(25);

                        exWorkSheet.Cells["X6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["X6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["X6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LemonChiffon);
                        exWorkSheet.Cells["X6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["X6"].Value = "Remarks";
                        exWorkSheet.Cells["X6"].AutoFitColumns(25);

                        exWorkSheet.Cells["Y6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["Y6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["Y6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        exWorkSheet.Cells["Y6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["Y6"].Value = "Reporting AC period";
                        exWorkSheet.Cells["Y6"].AutoFitColumns(25);

                        exWorkSheet.Cells["Z6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["Z6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["Z6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        exWorkSheet.Cells["Z6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["Z6"].Value = "AC Closure";
                        exWorkSheet.Cells["Z6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AA6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AA6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AA6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        exWorkSheet.Cells["AA6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AA6"].Value = "Due Date";
                        exWorkSheet.Cells["AA6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AB6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AB6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AB6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["AB6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AB6"].Value = "Audit area";
                        exWorkSheet.Cells["AB6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AC6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AC6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AC6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["AC6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AC6"].Value = "Report no/State/Hub";
                        exWorkSheet.Cells["AC6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AD6"].Style.Font.Bold = true;                        
                        exWorkSheet.Cells["AD6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AD6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["AD6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AD6"].Value = "Business Vertical";
                        exWorkSheet.Cells["AD6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AE6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AE6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AE6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["AE6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AE6"].Value = "Observation/Root Cause";
                        exWorkSheet.Cells["AE6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AF6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AF6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AF6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(171, 227, 196));
                        exWorkSheet.Cells["AF6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AF6"].Value = "Action Plan";
                        exWorkSheet.Cells["AF6"].AutoFitColumns(25);

                        exWorkSheet.Cells["AG6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AG6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AG6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        exWorkSheet.Cells["AG6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AG6"].Value = "Risk Level";
                        exWorkSheet.Cells["AG6"].AutoFitColumns(25);

                       // exWorkSheet.Cells["AH6"].Style.Border = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red); 
                        exWorkSheet.Cells["AH6"].Style.Font.Bold = true;
                        exWorkSheet.Cells["AH6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet.Cells["AH6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        exWorkSheet.Cells["AH6"].Style.Font.Size = 12;
                        exWorkSheet.Cells["AH6"].Value = "Staff";
                        exWorkSheet.Cells["AH6"].AutoFitColumns(25);

                        using (ExcelRange col = exWorkSheet.Cells[6, 1, 6 + ExcelData.Rows.Count, 34])
                        {
                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            
                            // Assign borders
                            //col.Style.TextRotation = "center";
                           
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            col.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(199, 199, 204));
                            col.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(199, 199, 204));
                            col.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(199, 199, 204));
                            col.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(199, 199, 204));
                          
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=MasterAuditObservation.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }





        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcess.SelectedValue != "-1")
            {
                // BindData();
            }
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlVertical.SelectedValue != "-1")
            {
                // BindData();
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            //BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                // BindData();
            }

        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }

    }
}