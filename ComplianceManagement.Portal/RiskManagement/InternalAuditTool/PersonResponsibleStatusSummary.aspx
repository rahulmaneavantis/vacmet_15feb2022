﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="PersonResponsibleStatusSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.PersonResponsibleStatusSummary" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>  
    <style type="text/css">              
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }   
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }
        .clsheadergrid > th{text-align:center;}
        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
           font-family: 'Roboto',sans-serif; 
            color: #8e8e93;
        }
    </style>
   <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('My Workspace/ Audit Status Summary');
        });       
   </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>           
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                                  <div class="clearfix"></div> 
                             <header class="panel-heading tab-bg-primary" style="margin-left: 75%;">  
                                                                                                                                                                                                             
                                    </header> 
                            <%--<header class="panel-heading tab-bg-primary" style="margin-left: 95%;">  
                                        <a href="#" class="hover" id="aBackChk" onClick="history.go(-1)">Back</a>                                                                                                    
                                    </header> --%>
                             <div class="panel-body">
                                <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="AuditValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="AuditValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div> 
                                     
                                    <div class="col-md-3 colpadding0" style="color: #999; padding-top: 9px;width: 24%; display:none;">
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                    RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                    <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                    <asp:ListItem Text="Non Process" Value="Non Process" />
                                    </asp:RadioButtonList>
                                    </div>   
                                    
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                       <%-- <asp:DropDownList ID="ddlProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                        OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                        </asp:DropDownList>  --%>   
                                         <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true" Width="90%" Height="32px" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                        </asp:DropDownListChosen>               
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                      <%--  <asp:DropDownList ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                        OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                        </asp:DropDownList> --%>   
                                           <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>                                      
                                    </div> 

                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  
                                         <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" OnClick="lbtnExportExcel_Click" style="width: 126px;"  runat="server"/> 
                                        &nbsp;     &nbsp; 
                                    <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-search"  OnClick="btnBack_Click" style="float: right;" />     
                                          </div>                    
                                </div>   
                                                           
                                <div class="clearfix"></div>                               
                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">                                                            
                                                                                           
                                </div>   
                                                                    
                                <div class="clearfix"></div>                                
                                <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdAuditStatus" AutoGenerateColumns="false" 
                                    PageSize="5" AllowPaging="true" AutoPostBack="true" OnRowCommand="grdAuditStatus_RowCommand"
                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true" onrowdatabound="grdAuditStatus_RowDataBound"
                                    OnPageIndexChanging="grdAuditStatus_PageIndexChanging" ShowFooter="true">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                         <ItemTemplate>
                                             <%#Container.DataItemIndex+1 %>
                                         </ItemTemplate>
                                         </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Process" ItemStyle-Width="150px" FooterStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%#Eval("Process") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Process") %>' />
                                                     </div>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Sub Process" ItemStyle-Width="150px" FooterStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblSubProcess" runat="server" Text='<%#Eval("SubProcess") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("SubProcess") %>' />
                                                     </div>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTotal" runat="server"
                                                    Text='<%# Eval("Total") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                       <%-- <asp:TemplateField HeaderText="Not Done" ItemStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskNotDone" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "NotDone;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")  %>'
                                                    Text='<%# Eval("NotDone") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Submitted" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Submitted;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")  %>'
                                                    Text='<%# Eval("Submited") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Team Review" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTeamReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "TeamReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")  %>'
                                                    Text='<%# Eval("TeamReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Auditee Review" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'
                                                    Text='<%# Eval("AuditeeReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Final Review" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblFinalReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "FinalReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear") %>'
                                                    Text='<%# Eval("FinalReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Closed" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskClosed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Closed;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("VerticalID")+";"+Eval("ForMonth")+";"+Eval("FinancialYear")  %>'
                                                    Text='<%# Eval("Closed") %>' ></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>   
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />     
                                      <PagerSettings Visible="false" />                     
                                    <PagerTemplate>
                                       <%-- <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                                       <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                                 </div>
                                <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>           
                       </section>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
           <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
