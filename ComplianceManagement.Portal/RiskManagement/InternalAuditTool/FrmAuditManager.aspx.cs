﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmAuditManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFinancialYear();
                BindLegalEntityData();

                ViewState["ActiveFlag"] = "P";  /* P For Process and I for Implementation Status */

                BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                GetPageDisplaySummary();

                ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));

                //udcReviewStatusTranscatopn.OpenTransactionPage(1, 1, "1", 1);
            }   
        }

        public void BindData(int Userid)
        {            
            int branchid = -1;            
            int Statusid = -1;            

            string FinYear = String.Empty;
            string Period = String.Empty;

            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
            {
                if (ddlFilterStatus.SelectedValue != "-1")
                {
                    Statusid = Convert.ToInt32(ddlFilterStatus.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    FinYear = ddlFinancialYear.SelectedItem.Text;
                }
            }

            if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
            {
                if (ddlPeriod.SelectedValue != "-1")
                {
                    Period = ddlPeriod.SelectedItem.Text;
                }
            }
            if (ViewState["ActiveFlag"]!=null)
            {
                if(ViewState["ActiveFlag"].ToString()=="P")
                {
                    var records = Business.ProcessManagement.GetAuditsForAuditManager(Userid, branchid, FinYear, Statusid); /*, period,*/
                    grdSummaryDetailsAuditCoverage.DataSource = records;
                    Session["TotalRows"] = records.Count;
                    grdSummaryDetailsAuditCoverage.DataBind();
                }
                else if (ViewState["ActiveFlag"].ToString() == "I")
                {
                    var records = Business.ProcessManagement.GetAuditsForAuditManagerIMP(Userid, branchid, FinYear, Statusid, Period, customerID); /*, ,*/
                    grdSummaryDetailsAuditCoverageIMP.DataSource = records;
                    Session["TotalRows"] = records.Count;
                    grdSummaryDetailsAuditCoverageIMP.DataBind();
                }
            }                   
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";                    
                        
                 ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + ");", true);
                       
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnChangeStatusIMP_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIMPDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + ");", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)                
                    ddlSubEntity1.Items.Clear();  
                else
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));

                if (ddlSubEntity2.Items.Count > 0)                
                    ddlSubEntity2.Items.Clear();  
                else
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();
                else
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
                else
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
            }

            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            GetPageDisplaySummary();
        }        

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection(); 
                else
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection(); 
                else
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection(); 
                else
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));

                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }

                BindSchedulingType(branchid);
            }

            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            GetPageDisplaySummary();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;

            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();  
                else
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
                else
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));

                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }

                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);                    
                }

                BindSchedulingType(branchid);
            }

            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            GetPageDisplaySummary();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindSchedulingType(Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)                
                    ddlFilterLocation.ClearSelection();                
                else
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));

                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }

                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }

                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }

                BindSchedulingType(branchid);
            }

            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            GetPageDisplaySummary();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;

            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedItem.Text))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                }

                else
                {
                    if (ddlLegalEntity.SelectedValue != "-1" && ddlLegalEntity.SelectedValue != "")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }

                    if (ddlSubEntity1.SelectedValue != "-1" && ddlSubEntity1.SelectedValue != "")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }

                    if (ddlSubEntity2.SelectedValue != "-1" && ddlSubEntity2.SelectedValue != "")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }

                    if (ddlSubEntity3.SelectedValue != "-1" && ddlSubEntity3.SelectedValue != "")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }

                    BindSchedulingType(branchid);
                }

                //Reload the Grid
                BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                GetPageDisplaySummary();
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ActiveFlag"] != null)
                {
                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo <= GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();

                        if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                        {
                            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                        else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                        {
                            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                    }

                    //Reload the Grid
                    BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["ActiveFlag"] = "I";

            ProcessGrid.Visible = false;
            ImplementationGrid.Visible = true;

            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");           
            ViewState["ActiveFlag"] = "P";

            ProcessGrid.Visible = true;
            ImplementationGrid.Visible = false;

            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Reload the Grid
            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            GetPageDisplaySummary();
        }

        protected void ddlFilterStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Reload the Grid
            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            GetPageDisplaySummary();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Reload the Grid
            BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            GetPageDisplaySummary();
        }                

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            //{
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            //}
        }

        public static String GetSchedulingType(int CustBranchID, int processid, String FinYear, String Period)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Record = (from row in entities.InternalAuditSchedulings
                              where row.CustomerBranchId == CustBranchID
                              && row.Process == processid
                              && row.FinancialYear == FinYear
                              && row.TermName == Period
                              select row.ISAHQMP).FirstOrDefault();

                if (Record != "")
                    return Record;
                else
                    return "";
            }
        }       

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    if (ViewState["ActiveFlag"] != null)
                    {
                        SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();

                        if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                        {
                            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                        else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                        {
                            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                    }
                }

                //Reload the Grid
                BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (ViewState["ActiveFlag"] != null)
                {
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo + 1).ToString();

                        if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                        {
                            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }

                        else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                        {
                            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                    }

                    //Reload the Grid
                    BindData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

    }
}