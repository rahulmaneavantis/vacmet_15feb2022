﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="SchedulingReports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.SchedulingReports" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('Audit Scheduling Report');
            fhead('Audit Scheduling Report');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>  
            
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                            <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs" style="margin: -10px -24px -11px 0;">
                                           <%if (roles.Contains(3) || roles.Contains(4))%>
                                           <%{%>
                                                  <%if (roles.Contains(3))%>
                                                    <%{%>      
                                                        <li class="active" id="liPerformer" runat="server">
                                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                                        </li>
                                                    <%}%>
                                                    <%if (roles.Contains(4))%>
                                                    <%{%>      
                                                        <li class=""  id="liReviewer" runat="server">
                                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                                        </li> 
                                                    <%}%>
                                             
                                                <% if (AuditHeadOrManagerReport == null)%>
                                                <%{%>
                                                    <li style="float: right;">   
                                                        <div>                                                        
                                                                <button class="form-control m-bot15" type="button" style="background-color:none;" data-toggle="dropdown">More Reports
                                                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                <ul class="dropdown-menu">                                                                                                                       
                                                                    <li><a href="../AuditTool/MyReportAudit.aspx">Final Audit</a></li>
                                                                    <li><a href="../InternalAuditTool/AuditObservationDraft.aspx">Draft Observation</a></li>                                                                    
                                                                    <li><a href="../InternalAuditTool/FrmObservationReportWord.aspx">Observation-Word</a></li>
                                                                    <li><a href="../InternalAuditTool/OpenObervationReport.aspx">Open Observation-Excel</a></li>                                                                    
                                                                    <li><a href="../AuditTool/ARSReports.aspx">Audit Status</a></li> 
                                                                    <li><a href="../AuditTool/PreRequsiteLogReport.aspx">Pre-Requsite Log Report</a></li>
                                                                    <li><a href="../AuditTool/ManagementResponseLogReport.aspx">Management Response Log Report</a></li>
                                                                    <li><a href="../InternalAuditTool/PastAuditUpload.aspx">Past Audit Reports</a></li>
                                                                    <%--<li><a href="../InternalAuditTool/FrmMasterAuditObservationReport.aspx">Audit Observation</a></li>                                                    
                                                                    <li><a href="../InternalAuditTool/FrmInternalAuditReportInWord.aspx">Internal Observation Word</a></li>                                                                                                      
                                                                    <li><a href="../InternalAuditTool/InternalAuditReportInDocument.aspx">Internal Observation Revised</a></li> --%>                                                     
                                                                </ul>                                                       
                                                        </div>
                                                    </li>  
                                                <%}%>
                                           <%}%>                                                 
                                    </ul>
                                </header>  
                            <div class="clearfix"></div>                  
                        <div style="margin-bottom: 4px"/> 
                         <div class="col-md-12 colpadding0">
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-2 colpadding0" style="width:30%">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                <asp:ListItem Text="5" Selected="True" />
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                                 </div>
                             <div style="text-align:right;" >
                                <asp:LinkButton Text="Export to Excel" CssClass="btn btn-primary" runat="server" ID="lbtnExportExcel" OnClick="lbtnExportExcel_Click" />                     
                            </div>  
                          </div>                                      
                            <div class="clearfix"></div>                    
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"  class="form-control m-bot15 select_location" Width="90%" Height="32px" Style="float: left; width:90%;"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                              AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                    
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                   
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                      
                            </div>                                                                                                 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                    
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                              AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                    
                            </div>
                            </div><div class="clearfix">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" DataPlaceHolder="Sub Unit" AutoPostBack="true"
                                 AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                </div> 
                               <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                               <%{%> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15 select_location" Width="90%" Height="32px" OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                                  <%}%>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" DataPlaceHolder="Financial Year">
                                        </asp:DropDownListChosen>                    
                                    </div> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15 select_location" 
                                            Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>  
                            </div>
                            <div class="clearfix"><div class="clearfix">
                            <div class="col-md-12 colpadding0">                                                                    
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen runat="server" ID="ddlPeriod" DataPlaceHolder="Period" AutoPostBack="true"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                    class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                </div>                                                                    
                            </div>
                            <div class="clearfix">
                            <div class="clearfix">                          
                            <div class="clearfix" style="margin-top:50px"></div>
                               
                          <%--  <div style="margin-bottom: 4px">--%>
                                     &nbsp;
                                <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                                Font-Size="12px">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField  HeaderText="Location">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 100px;"> 
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>' ></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField  HeaderText="Vertical">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 90px;"> 
                                                <asp:Label ID="lblVertical" runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("VerticalName") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField  HeaderText="Financial Year">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;"> 
                                                <asp:Label ID="lblFyear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                         <asp:TemplateField  HeaderText="Period">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 80px;"> 
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ForMonth") %>'></asp:Label> 
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                 <%--   <asp:BoundField DataField="Branch" HeaderText="Location" />
                                    <asp:BoundField DataField="VerticalName" HeaderText="Vertical" />
                                    <asp:BoundField DataField="FinancialYear" HeaderText="Financial Year" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="ForMonth" HeaderText="Period" ItemStyle-HorizontalAlign="Center" />--%>

                                    <asp:TemplateField HeaderText="Planned (From)" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPlannedFrom" runat="server" Text='<%# Eval("StartDate")!=null?Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual (From)" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFrom" runat="server" Text='<%# Eval("ExpectedStartDate")!=null?Convert.ToDateTime(Eval("ExpectedStartDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned (To)" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPlannedTo" runat="server" Text='<%# Eval("EndDate")!=null?Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual (To)" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualTo" runat="server" Text='<%# Eval("ExpectedEndDate")!=null?Convert.ToDateTime(Eval("ExpectedEndDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                    <HeaderStyle BackColor="#ECF0F1" />
                                     <PagerSettings Visible="false" />
                                <PagerTemplate>
                                 <%--   <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                         <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right">
                                <div class="table-paging" style="margin-top: -34px;margin-right: 11%;">
                                   <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>  
            
            
            <div style="display:none;">

                    <asp:GridView runat="server" ID="grdRegionReport" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                        CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                        Font-Size="12px">
                        <Columns>                           
                            <asp:BoundField DataField="Region" HeaderText="Location" />
                            <asp:BoundField DataField="Planned" HeaderText="Location" />  
                            <asp:BoundField DataField="Actual" HeaderText="Actual" />                       
                        </Columns>                               
                    </asp:GridView>                                         
                <br />
                <br />
                <asp:GridView runat="server" ID="grdStateReport" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                        CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                        Font-Size="12px">
                        <Columns>                           
                            <asp:BoundField DataField="State" HeaderText="Location" />    
                            <asp:BoundField DataField="Planned" HeaderText="Location" />  
                            <asp:BoundField DataField="Actual" HeaderText="Actual" />                                                                  
                        </Columns>                               
                    </asp:GridView>   
                 <br />
                 <br />   

                 <asp:GridView runat="server" ID="grdHubReport" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                        CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                        Font-Size="12px">
                        <Columns>                           
                            <asp:BoundField DataField="Hub" HeaderText="Location" />                            
                             <asp:BoundField DataField="Planned" HeaderText="Location" />  
                            <asp:BoundField DataField="Actual" HeaderText="Actual" />                       
                        </Columns>                               
                    </asp:GridView>  
                
                 <br />
                 <br />
                  <asp:GridView runat="server" ID="grdBranchReport" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                        CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                        Font-Size="12px">
                        <Columns>                           
                            <asp:BoundField DataField="Branch" HeaderText="Location" />                            
                            <asp:BoundField DataField="Planned" HeaderText="Location" />  
                            <asp:BoundField DataField="Actual" HeaderText="Actual" />                       
                        </Columns>                               
                    </asp:GridView>  
            </div>                 
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
