﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class DetailedObservationList : System.Web.UI.Page
    {
        protected string flag = string.Empty;
        protected long BranchId = -1;
        protected long StatusId = -1;
        protected static string StoreSessionvalue = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int ProcessId = -1;
                int AuditStepid = -1;
                int SubProcessId = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["ProcessId"]))
                {
                    ProcessId = Convert.ToInt32(Request.QueryString["ProcessId"]);
                    Session["ProcessId"] = ProcessId;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditStepid"]))
                {
                    AuditStepid = Convert.ToInt32(Request.QueryString["AuditStepid"]);
                    Session["AuditStepid"] = AuditStepid;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SubProcessId"]))
                {
                    SubProcessId = Convert.ToInt32(Request.QueryString["SubProcessId"]);
                    Session["SubProcessid"] = SubProcessId;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Redirect"]))
                {
                    if (Request.QueryString["Redirect"] == "VO")
                    {
                        ViewState["Redirect"] = "VO";
                        BindGrid(ProcessId, AuditStepid, SubProcessId);
                        int chknumber = Convert.ToInt32(grdDetailedObs.PageIndex);
                        bindPageNumber();
                        if (chknumber > 0)
                        {
                            chknumber = chknumber + 1;
                            DropDownListPageNo.SelectedValue = (chknumber).ToString();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["BranchId"]))
                        {
                            BranchId = Convert.ToInt64(Request.QueryString["BranchId"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["StatusId"]))
                        {
                            StatusId = Convert.ToInt64(Request.QueryString["StatusId"]);
                        }
                        bool checkPRFlag = false;
                        checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID), Convert.ToInt32(BranchId));
                        var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                        var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Convert.ToInt32(BranchId));
                        if (checkPRFlag == true)
                        {
                            #region checkPRFlag true
                            bool checkPRRiskActivationFlag = false;
                            checkPRRiskActivationFlag = CustomerManagementRisk.CheckPersonResponsibleFlowApplicable(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Convert.ToInt32(BranchId));
                            #endregion
                        }
                        if ((AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH"))
                        {
                            flag = "Disable";
                        }
                        BindGrid(ProcessId, AuditStepid, SubProcessId);
                        int chknumber = Convert.ToInt32(grdDetailedObs.PageIndex);
                        bindPageNumber();
                        if (chknumber > 0)
                        {
                            chknumber = chknumber + 1;
                            DropDownListPageNo.SelectedValue = (chknumber).ToString();
                        }

                    }
                }
                //BindAuditStep(AuditStepid);

            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                    ListItem removeItem = DropDownListPageNo.Items.FindByText("0");
                    DropDownListPageNo.Items.Remove(removeItem);
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdDetailedObs.PageIndex = chkSelectedPage - 1;
            grdDetailedObs.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid(Convert.ToInt32(Session["ProcessId"]), Convert.ToInt32(Session["AuditStepid"]), Convert.ToInt32(Session["SubProcessId"]));
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdDetailedObs.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid(Convert.ToInt32(Session["ProcessId"]), Convert.ToInt32(Session["AuditStepid"]), Convert.ToInt32(Session["SubProcessId"]));
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdDetailedObs.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public void BindGrid(int ProcessId, int AuditStepid, int SubProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var observationList = (from row in entities.usp_detailedObservation(ProcessId, AuditStepid, SubProcessId)
                                       select row).ToList();

                if (observationList.Count > 0)
                {
                    lblAuditStep.Text = (from row in entities.AuditStepMasterViews
                                         where row.ID == AuditStepid
                                         select row.AuditStep).FirstOrDefault();
                    lblAuditStep.ToolTip = lblAuditStep.Text;
                    //lblAuditStep.Attributes.Add("tooltip", lblAuditStep.Text);
                    grdDetailedObs.DataSource = observationList;
                    grdDetailedObs.DataBind();
                    Session["TotalRows"] = observationList.Count;
                    upObservationDetails.Update();
                    if (Session["Observation"] != null)
                    {
                        //StoreSessionvalue = Convert.ToString(Session["Observation"]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "check_uncheck();", true);
                    }
                }
                else
                {
                    grdDetailedObs.DataSource = null;
                    grdDetailedObs.DataBind();

                }
            }
        }

        protected void grdDetailedObs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (e.CommandName.Equals("ViewObservation"))
                    {
                        String Args = e.CommandArgument.ToString();
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int AuditId = Convert.ToInt32(commandArgs[0]);
                        int ATBDId = Convert.ToInt32(commandArgs[1]);
                        lblObservation.Text = (from row in entities.AuditClosures
                                                where row.AuditID == AuditId && row.ATBDId == ATBDId
                                                select row.Observation).FirstOrDefault();

                        //lblObservationtitle.Text = Convert.ToString(commandArgs[0]);
                        //lblObservation.Text = Convert.ToString(commandArgs[1]);
                        upViewobservation.Update();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "viewObs();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "check_uncheck();", true);
                    }
                    else if (e.CommandName.Equals("Select_Observation"))
                    {
                        try
                        {
                            String Args = e.CommandArgument.ToString();
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                            int AuditId = Convert.ToInt32(commandArgs[0]);
                            int ATBDId = Convert.ToInt32(commandArgs[1]);
                            Session["AuditATBDID"] = Convert.ToString(AuditId) + "," + Convert.ToString(ATBDId);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "closePop();", true);
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string auditStep = lblAuditStep.Text;
            lblviewAuditStep.Text = auditStep;
            upViewAuditStep.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Popup", "fopenpopup();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "check_uncheck();", true);
        }

        protected void btnToggle_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.CommandName == "ToggleBtn")
            {
                string Ids = btn.CommandArgument.ToString();
                string[] commandArgs = Ids.Split(new char[] { ',' });
                int AuditId = Convert.ToInt32(commandArgs[0]);
                int ATBDId = Convert.ToInt32(commandArgs[1]);
                Session["AuditATBDID"] = null;
                Session["AuditATBDID"] = Convert.ToString(AuditId) + "," + Convert.ToString(ATBDId);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "closePop();", true);
            }
        }

        [System.Web.Services.WebMethod]
        public static string SetSession(string id)
        {
            string idFull = id;
            //string[] ids = idFull.Split('_');
            HttpContext.Current.Session["AuditATBDID"] = id;
            return "Done";
        }

        protected void grdDetailedObs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (Convert.ToString(ViewState["Redirect"]) == "VO")
            {
                grdDetailedObs.Columns[6].Visible = false;
            }
            else
            {
                grdDetailedObs.Columns[6].Visible = true;
            }
        }

        [System.Web.Services.WebMethod]
        public static string SetSessionObj(string name)
        {
            StoreSessionvalue = name;
            string Observation = name;
            HttpContext.Current.Session["Observation"] = Observation;            
            return "Done";
        }
    }
}