﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditMainUI_IMP_New : System.Web.UI.Page
    {
        public string mgmtResponse = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["SID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["RoleID"]))
                                            if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                                if (!String.IsNullOrEmpty(Request.QueryString["returnUrl2"]))
                                                {
                                                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                                    {
                                                        int chkRole = Convert.ToInt32(Request.QueryString["RoleID"]);
                                                        int CBranchID = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                                                        ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["Period"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["AuditID"];
                                                        ViewState["roleid"] = Convert.ToInt32(Request.QueryString["RoleID"]);
                                                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                        ViewState["returnUrl2"] = Request.QueryString["returnUrl2"];
                                                        ViewState["rStatus"] = Request.QueryString["Status"];
                                                        ViewState["VerticalID"] = Request.QueryString["VID"];
                                                        ViewState["ProcessID"] = null;
                                                        ViewState["scheduledonid"] = Request.QueryString["SID"];
                                                        bindPageNumber();

                                                        int customerID = -1;
                                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                        if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                                                        {
                                                            string statusidasd = Request.QueryString["Status"].ToString();
                                                            int statusFlag = 0;
                                                            if (statusidasd.Equals("NotDone"))
                                                            {
                                                                statusFlag = 1;
                                                            }
                                                            else if (statusidasd.Equals("Submitted"))
                                                            {
                                                                statusFlag = 2;
                                                            }
                                                            else if (statusidasd.Equals("TeamReview"))
                                                            {
                                                                statusFlag = 4;
                                                            }
                                                            else if (statusidasd.Equals("AuditeeReview"))
                                                            {
                                                                statusFlag = 6;
                                                            }
                                                            else if (statusidasd.Equals("FinalReview"))
                                                            {
                                                                statusFlag = 5;
                                                            }
                                                            else if (statusidasd.Equals("Closed"))
                                                            {
                                                                statusFlag = 3;
                                                            }

                                                            if (statusFlag != 0)
                                                            {
                                                                ddlFilterStatus.ClearSelection();
                                                                ddlFilterStatus.Items.FindByValue(statusFlag.ToString()).Selected = true;
                                                            }

                                                            if (chkRole == 3)
                                                            {

                                                                if (statusidasd.Equals("Submitted") || statusidasd.Equals("Closed") || statusidasd.Equals("FinalReview") || statusidasd.Equals("AuditeeReview"))
                                                                {
                                                                    lbkSave.Enabled = false;
                                                                    DropDownListPersonResponsible.Visible = false;
                                                                }
                                                                else
                                                                {
                                                                    lbkSave.Enabled = true;
                                                                    DropDownListPersonResponsible.Visible = false;
                                                                }

                                                                //if (statusidasd.Equals("Submitted") || statusidasd.Equals("Closed") || statusidasd.Equals("FinalReview") || statusidasd.Equals("AuditeeReview"))
                                                                //{
                                                                //    lbkSave.Enabled = false;
                                                                //    DropDownListPersonResponsible.Visible = false;
                                                                //}
                                                                //else
                                                                //{
                                                                //    lbkSave.Enabled = true;
                                                                //    DropDownListPersonResponsible.Visible = false;
                                                                //}
                                                            }
                                                            if (chkRole == 4)
                                                            {
                                                                bool flagStatus = false;
                                                                if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                                                                {
                                                                    lbkSave.Enabled = false;
                                                                    DropDownListPersonResponsible.Visible = false;
                                                                    flagStatus = false;
                                                                }
                                                                else
                                                                {
                                                                    lbkSave.Enabled = true;
                                                                    DropDownListPersonResponsible.Visible = true;
                                                                    flagStatus = true;
                                                                }
                                                                if (flagStatus == true)
                                                                {
                                                                    bool checkPRFlag = false;
                                                                    checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, CBranchID);

                                                                    var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                    var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                    if (checkPRFlag == true)
                                                                    {
                                                                        if (!(statusidasd.Equals("Closed")))
                                                                        {
                                                                            BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (!(statusidasd.Equals("Closed")))
                                                                        {
                                                                            BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                                                                        }
                                                                    }
                                                                }

                                                                //bool flagStatus = false;
                                                                //if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                                                                //{
                                                                //    lbkSave.Enabled = false;
                                                                //    DropDownListPersonResponsible.Visible = false;
                                                                //    flagStatus = false;
                                                                //}
                                                                //else
                                                                //{
                                                                //    lbkSave.Enabled = true;
                                                                //    DropDownListPersonResponsible.Visible = true;
                                                                //    flagStatus = true;
                                                                //}

                                                                ////int customerID = -1;
                                                                //if (flagStatus == true)
                                                                //{
                                                                //    bool checkPRFlag = false;
                                                                //    checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, CBranchID);

                                                                //    var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                //    var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                                //    if (checkPRFlag == true)
                                                                //    {
                                                                //        if (!(statusidasd.Equals("Closed")))
                                                                //        {
                                                                //            BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                                //        }
                                                                //    }
                                                                //    else
                                                                //    {
                                                                //        if (!(statusidasd.Equals("Closed")))
                                                                //        {
                                                                //            BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                                                                //        }
                                                                //    }
                                                                //}
                                                            }
                                                        }
                                                    }
                                                }
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());


                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                DropDownListPersonResponsible.Items.Clear();
                DropDownListPersonResponsible.Visible = true;
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 3)) //|| (st.ID == 6)
                            {
                            }
                            else
                            {
                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));

                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 6) || (st.ID == 3))
                            {
                            }
                            else
                            {
                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "4")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 5) || (st.ID == 3))
                            {
                            }
                            else
                            {
                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "2")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 5) || (st.ID == 3))
                            {
                            }
                            else
                            {
                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                            else
                            {
                                if (!(st.ID == 5))
                                {
                                    DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                }//Personresponsible End
                else
                {
                    if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 4))
                            {
                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                if ((st.ID == 4) || (st.ID == 7))
                                {
                                }
                                else
                                {
                                    DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                            else
                            {
                                if (!(st.ID == 7))
                                {
                                    DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        //{
        //    try
        //    {
        //        DropDownListPersonResponsible.Items.Clear();
        //        DropDownListPersonResponsible.Visible = true;
        //        // DropDownListPersonResponsible.Items.Insert(-1, "Select status");
        //        var statusList = AuditStatusManagement.GetInternalStatusList();
        //        List<InternalAuditStatu> allowedStatusList = null;
        //        List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
        //        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
        //        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
        //        if (statusID == 7)
        //        {
        //            if (ddlFilterStatus.SelectedValue == "6")
        //            {
        //                foreach (InternalAuditStatu st in allowedStatusList)
        //                {
        //                    //if ((st.ID == 4) || (st.ID == 6))
        //                    if (st.ID == 4) //|| (st.ID == 6)
        //                    {
        //                    }
        //                    else
        //                    {
        //                        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));

        //                    }
        //                }
        //            }
        //            else if (ddlFilterStatus.SelectedValue == "5")
        //            {
        //                foreach (InternalAuditStatu st in allowedStatusList)
        //                {
        //                    if ((st.ID == 4) || (st.ID == 6))
        //                    {
        //                    }
        //                    else
        //                    {
        //                        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                    }
        //                }
        //            }
        //            else if (ddlFilterStatus.SelectedValue == "4")
        //            {
        //                foreach (InternalAuditStatu st in allowedStatusList)
        //                {
        //                    if (!(st.ID == 5))
        //                    {
        //                        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                foreach (InternalAuditStatu st in allowedStatusList)
        //                {
        //                    if (isAuditmanager == "AM" && rolelist.Contains(4))
        //                    {
        //                        //if (!(st.ID == 4))
        //                        //{
        //                            //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                       // }
        //                    }
        //                    else
        //                    {
        //                        //if (string.IsNullOrEmpty(txtObservation.Text))
        //                        //{
        //                        //    if (!(st.ID == 6))
        //                        //    {
        //                        //        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                        //        //DropDownListPersonResponsible.Items.Insert(1, st.Name);
        //                        //        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                        //    }
        //                        //}
        //                        //else
        //                        //{
        //                            if (!(st.ID == 5))
        //                            {
        //                                //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                                DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                            }
        //                        //}
        //                    }
        //                }
        //            }
        //        }//Personresponsible End
        //        else
        //        {
        //            if (ddlFilterStatus.SelectedValue == "5")
        //            {
        //                foreach (InternalAuditStatu st in allowedStatusList)
        //                {
        //                    if (!(st.ID == 4))
        //                    {
        //                        // rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                        DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                foreach (InternalAuditStatu st in allowedStatusList)
        //                {
        //                    if (isAuditmanager == "AM" && rolelist.Contains(4))
        //                    {
        //                        if ((st.ID == 4) || (st.ID == 7))
        //                        {
        //                        }
        //                        else
        //                        {
        //                            //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                            // ddlPeriod.Items.Insert(1, "April");
        //                        }

        //                    }
        //                    else
        //                    {
        //                        if (!(st.ID == 7))
        //                        {
        //                            //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
        //                            DropDownListPersonResponsible.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        // lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;

            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindDetailView(ViewState["Arguments"].ToString(), "P");

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url3 = "";
            string url4 = "";
            string auditID = "";
            string FinYear = string.Empty;
            if (!String.IsNullOrEmpty(Request.QueryString["returnUrl2"]))
            {
                url3 = Request.QueryString["returnUrl2"];

                if (!String.IsNullOrEmpty(Request.QueryString["returnUrl1"]))
                {
                    url4 = "&returnUrl1=" + Request.QueryString["returnUrl1"];
                }
                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    FinYear = Convert.ToString(Request.QueryString["FY"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Request.QueryString["AuditID"];
                }

                Response.Redirect("~/RiskManagement/AuditTool/AuditStatusUI_IMP_New.aspx?" + url3.Replace("@", "=") + "&FY=" + FinYear + url4);
            }
        }


        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                string ActionCHK = Request.QueryString["Status"];

                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";

                    if (ViewState["roleid"] != null)
                    {
                        if (Convert.ToInt32(ViewState["roleid"]) == 3)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ",'" + ActionCHK + "');", true);
                        else if (Convert.ToInt32(ViewState["roleid"]) == 4)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "ShowReviewerDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ",'" + ActionCHK + "');", true);
                        //else if (Convert.ToInt32(ViewState["roleid"]) == 5)
                        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "ShowReviewerDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ",'" + ActionCHK + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }

        public bool ATBDVisibleorNot(int? StatusID)
        {
            if (ViewState["roleid"] != null)
            {
                if (StatusID == null && Convert.ToInt32(ViewState["roleid"]) == 4)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        private void BindDetailView(string Arguments, String IFlag)
        {
            try
            {
                int BranchID = -1;
                List<int> roleid = new List<int>();
                string financialyear = String.Empty;
                string period = String.Empty;
                string filter = String.Empty;
                long AuditID = 0;
                int ScheduledonID = -1;

                int VerticalID = -1;
                int userid = -1;
                userid = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string[] arg = Arguments.ToString().Split(';');

                if (arg.Length > 0)
                {
                    ScheduledonID = Convert.ToInt32(arg[2]);
                    BranchID = Convert.ToInt32(arg[3]);
                    financialyear = Convert.ToString(arg[4]);
                    period = Convert.ToString(arg[5]);
                    VerticalID = Convert.ToInt32(arg[6]);
                    AuditID = Convert.ToInt64(arg[7]);
                    List<int> statusIds = new List<int>();
                    List<int?> statusNullableIds = new List<int?>();

                    filter = arg[0].Trim();

                    if (arg[0].Trim().Equals("NotDone"))
                    {
                        statusIds.Add(-1);
                    }
                    else if (arg[0].Trim().Equals("Submitted"))
                    {
                        statusIds.Add(2);
                    }
                    else if (arg[0].Trim().Equals("TeamReview"))
                    {
                        statusIds.Add(4);
                    }
                    else if (arg[0].Trim().Equals("Closed"))
                    {
                        statusIds.Add(3);
                    }
                    else if (arg[0].Trim().Equals("AuditeeReview"))
                    {
                        statusIds.Add(6);
                    }
                    else if (arg[0].Trim().Equals("FinalReview"))
                    {
                        statusIds.Add(5);
                    }
                    else
                    {
                        statusIds.Add(1);
                        statusIds.Add(4);
                        statusIds.Add(2);
                        statusIds.Add(3);
                        statusIds.Add(5);
                        statusIds.Add(6);
                    }

                    if (ViewState["roleid"] != null)
                    {
                        if (Convert.ToInt32(ViewState["roleid"]) == 3)
                            roleid.Add(3);
                        else if (Convert.ToInt32(ViewState["roleid"]) == 4)
                        {
                            roleid.Add(4);
                            roleid.Add(5);
                        }
                    }
                    string AudResponse = string.Empty;
                    if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["Audittee"])))
                    {
                        AudResponse = Convert.ToString(Request.QueryString["Audittee"]);
                    }
                    else
                    {
                        AudResponse = null;
                    }

                    if (IFlag == "P")
                    {
                        var detailView = InternalControlManagementDashboardRisk.GetAuditDetailUserWiseIMP(customerID, BranchID, VerticalID, financialyear, period, userid, roleid, ScheduledonID, statusIds, statusNullableIds, filter, AuditID);
                        Session["TotalRows"] = detailView.Count;
                        grdSummaryDetailsAuditCoverage.DataSource = detailView;
                        grdSummaryDetailsAuditCoverage.DataBind();
                    }
                    else
                    {
                        var detailView = InternalControlManagementDashboardRisk.GetAuditDetailUserWiseIMP(customerID, BranchID, VerticalID, financialyear, period, userid, roleid, ScheduledonID, statusIds, statusNullableIds, filter, AuditID);
                        Session["TotalRows"] = detailView.Count;
                        grdSummaryDetailsAuditCoverage.DataSource = detailView;
                        grdSummaryDetailsAuditCoverage.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}

                //Reload the Grid
                BindDetailView(ViewState["Arguments"].ToString(), "P");

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {               
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindDetailView(ViewState["Arguments"].ToString(), "P");

        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindDetailView(ViewState["Arguments"].ToString(), "P");

        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected void Tab1_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "active");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;
            //upComplianceDetails_Load(sender, e);
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "active");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;
            //upComplianceDetails_Load(sender, e);
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "active");
            liReviewLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;
            //upComplianceDetails_Load(sender, e);
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            liAuditCoverage.Attributes.Add("class", "");
            liActualTesting.Attributes.Add("class", "");
            liObservation.Attributes.Add("class", "");
            liReviewLog.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 3;
            //upComplianceDetails_Load(sender, e);
        }
        public static ImplementationAuditResult GetImplementationAuditResultbyID(long AuditScheduleOnID, int ResultID, long CustomerBranchId, int Verticalid, long AuditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.ImplementationAuditResults
                                     where row.AuditScheduleOnID == AuditScheduleOnID && row.ResultID == ResultID
                                     && row.CustomerBranchId == CustomerBranchId && row.VerticalID == Verticalid
                                     && row.AuditID == AuditId
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        protected void grdSummaryDetailsAuditCoverage_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            int roleId = Convert.ToInt32(ViewState["roleid"]);
            string statusidasd = Request.QueryString["Status"].ToString();
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            var details = RiskCategoryManagement.FillUsers(customerID);
            if (roleId == 3)
            {
                if (statusidasd.Equals("Submitted") || statusidasd.Equals("Closed") || statusidasd.Equals("FinalReview") || statusidasd.Equals("AuditeeReview"))
                {

                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = false;
                        Label lblResultID = (Label)rw.FindControl("lblResultID");
                        Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                        Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                        Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                        Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                        Label lblStatus = (Label)rw.FindControl("lblStatus");

                        var MstRiskResult = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text));

                        if (MstRiskResult != null)
                        {
                            if (MstRiskResult.AuditeeResponse == "R2")
                            {
                                lblStatus.BackColor = Color.Red;
                            }
                            else
                            {

                            }
                        }
                        var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                        if (implementationdetails != null)
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            if (implementationdetails.ManagementResponse != "")
                            {
                                tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                                //mgmtResponse = implementationdetails.ManagementResponse;
                            }
                            tbxMgmResponce.Enabled = false;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = false;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                            {
                                ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                            }

                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = false;
                            txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = false;
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                            {
                                ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                            }
                        }
                        else
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            tbxMgmResponce.Enabled = false;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = false;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = false;

                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = false;
                        }
                    }
                }
                else
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        //CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        //chkBx.Enabled = true;

                        Label lblResultID = (Label)rw.FindControl("lblResultID");
                        Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                        Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                        Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                        Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                        Label lblStatus = (Label)rw.FindControl("lblStatus");
                        var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                        if (implementationdetails != null)
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            if (implementationdetails.ManagementResponse != "")
                            {
                                tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                                // mgmtResponse = implementationdetails.ManagementResponse;
                            }
                            tbxMgmResponce.Enabled = true;

                            var MstRiskResult = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text));

                            if (MstRiskResult != null)
                            {
                                if (MstRiskResult.AuditeeResponse == "R2")
                                {
                                    lblStatus.BackColor = Color.Red;
                                }
                                else
                                {

                                }
                            }
                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = true;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                            {
                                ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                            }

                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = true;
                            txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = true;
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                            {
                                ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                            }
                        }
                        else
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            tbxMgmResponce.Enabled = true;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = true;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = true;

                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = true;
                        }
                    }
                }
            }
            else if (roleId == 4 || roleId == 5)
            {
                if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        int ResultID = 0;
                        //CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        //chkBx.Enabled = false;
                        Label lblResultID = (Label)rw.FindControl("lblResultID");
                        Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                        Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                        Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                        Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                        Label lblStatus = (Label)rw.FindControl("lblStatus");
                        if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                        {
                            ResultID = Convert.ToInt32(lblResultID.Text);
                        }
                        else if (!string.IsNullOrEmpty(lblResultID.Text))
                        {
                            ResultID = Convert.ToInt32(lblResultID.Text);
                        }
                        long ProcessID = ProcessManagement.GetProcessIDByResultID(ResultID);
                        var MstRiskResult = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text));
                        var RoleListInAudit = CustomerManagementRisk.GetAuditListRoleIMP(Convert.ToInt32(lblAuditID.Text), ProcessID);
                        var RoleListOfUser = CustomerManagementRisk.GetListOfRoleInAuditIMp(Convert.ToInt32(lblAuditID.Text), Portal.Common.AuthenticationHelper.UserID, ProcessID);

                        if (RoleListInAudit.Contains(5))
                        {
                            if (RoleListOfUser.Contains(5))
                            {
                                if (MstRiskResult != null)
                                {
                                    if (MstRiskResult.AuditeeResponse == "3R")
                                    {
                                        lblStatus.BackColor = Color.Orange;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            {
                                if (MstRiskResult != null)
                                {
                                    if (MstRiskResult.AuditeeResponse == "R3")
                                    {
                                        lblStatus.BackColor = Color.Red;
                                    }
                                    else if (MstRiskResult.AuditeeResponse == "2R")
                                    {
                                        lblStatus.BackColor = Color.Orange;
                                    }
                                }
                            }
                        }
                        else if (RoleListInAudit.Contains(4))
                        {
                            if (MstRiskResult != null)
                            {
                                if (MstRiskResult.AuditeeResponse == "R3")
                                {
                                    lblStatus.BackColor = Color.Red;
                                }
                                else if (MstRiskResult.AuditeeResponse == "2R")
                                {
                                    lblStatus.BackColor = Color.Orange;
                                }
                            }
                        }

                        var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                        if (implementationdetails != null)
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            if (implementationdetails.ManagementResponse != "")
                            {
                                tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                                //mgmtResponse = implementationdetails.ManagementResponse;
                            }
                            tbxMgmResponce.Enabled = false;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = false;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                            {
                                ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                            }

                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = false;
                            txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = false;
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                            {
                                ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                            }
                        }
                        else
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            tbxMgmResponce.Enabled = false;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = false;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = false;

                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = false;
                        }
                    }
                }
                else
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                    {
                        int ResultID = 0;
                        //CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        //chkBx.Enabled = true;
                        Label lblResultID = (Label)rw.FindControl("lblResultID");
                        Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                        Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                        Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                        Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                        Label lblStatus = (Label)rw.FindControl("lblStatus");
                        if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                        {
                            ResultID = Convert.ToInt32(lblResultID.Text);
                        }
                        else if (!string.IsNullOrEmpty(lblResultID.Text))
                        {
                            ResultID = Convert.ToInt32(lblResultID.Text);
                        }
                        long ProcessID = ProcessManagement.GetProcessIDByResultID(ResultID);
                        var MstRiskResult = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text));
                        var RoleListInAudit = CustomerManagementRisk.GetAuditListRoleIMP(Convert.ToInt32(lblAuditID.Text), ProcessID);
                        var RoleListOfUser = CustomerManagementRisk.GetListOfRoleInAuditIMp(Convert.ToInt32(lblAuditID.Text), Portal.Common.AuthenticationHelper.UserID, ProcessID);

                        if (RoleListInAudit.Contains(5))
                        {
                            if (RoleListOfUser.Contains(5))
                            {
                                if (MstRiskResult != null)
                                {
                                    if (MstRiskResult.AuditeeResponse == "3R")
                                    {
                                        lblStatus.BackColor = Color.Orange;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            {
                                if (MstRiskResult != null)
                                {
                                    if (MstRiskResult.AuditeeResponse == "R3")
                                    {
                                        lblStatus.BackColor = Color.Red;
                                    }
                                    else if (MstRiskResult.AuditeeResponse == "2R")
                                    {
                                        lblStatus.BackColor = Color.Orange;
                                    }
                                }
                            }
                        }
                        else if (RoleListInAudit.Contains(4))
                        {
                            if (MstRiskResult != null)
                            {
                                if (MstRiskResult.AuditeeResponse == "R3")
                                {
                                    lblStatus.BackColor = Color.Red;
                                }
                                else if (MstRiskResult.AuditeeResponse == "2R")
                                {
                                    lblStatus.BackColor = Color.Orange;
                                }
                            }
                        }

                        var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                        if (implementationdetails != null)
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            if (implementationdetails.ManagementResponse != "")
                            {
                                tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                                // mgmtResponse = implementationdetails.ManagementResponse;
                            }
                            tbxMgmResponce.Enabled = true;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = true;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                            {
                                ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                            }

                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = true;
                            txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = true;
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                            {
                                ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                            }
                        }
                        else
                        {
                            Label tbxMgmResponce = (Label)rw.FindControl("tbxMgmResponce");
                            tbxMgmResponce.Enabled = true;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = true;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = true;

                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = true;
                        }
                    }
                }
            }
        }

        protected void lbkSave_Click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                    }
                }
                if (chkValidationflag > 0)
                {
                    string ManagementResponse = string.Empty;
                    string TimeLineValue = null;
                    string PersonResponsible = "-1";
                    string FinalStatusIMP = "-1";
                    if (Convert.ToInt32(ViewState["roleid"]) == 3)
                    {
                        #region Performer 
                        foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                        {
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                            if (chkBx != null && chkBx.Checked)
                            {
                                long roleid = 3;
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                ManagementResponse = ((TextBox)rw.FindControl("tbxMgmResponce")).Text;
                                TimeLineValue = ((TextBox)rw.FindControl("txtStartDate")).Text;
                                PersonResponsible = ((DropDownList)rw.FindControl("ddlPersonResponsible")).SelectedValue;
                                FinalStatusIMP = ((DropDownList)rw.FindControl("ddlIMPStatus")).SelectedValue;

                                if (FinalStatusIMP != "" && FinalStatusIMP != "-1")
                                {
                                    LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                                    String Args = chklnkbutton.CommandArgument.ToString();
                                    string[] arg = Args.ToString().Split(',');

                                    long ResultID = Convert.ToInt32(arg[0]);
                                    long CustomerbranchID = Convert.ToInt32(arg[1]);
                                    string FinancialYear = arg[2].ToString();
                                    string ForMonth = arg[3].ToString();
                                    long VerticalID = Convert.ToInt32(arg[5]);
                                    long Scheduledonid = Convert.ToInt32(arg[6]);
                                    long AuditID = Convert.ToInt32(arg[7]);
                                    var getImpAuditScheduleOnByDetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinancialYear, ForMonth, Convert.ToInt32(CustomerbranchID), Convert.ToInt32(VerticalID), Convert.ToInt32(Scheduledonid), AuditID);
                                    if (getImpAuditScheduleOnByDetails != null)
                                    {
                                        #region
                                        ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                                        {
                                            AuditScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                            FinancialYear = getImpAuditScheduleOnByDetails.FinancialYear,
                                            ForPeriod = getImpAuditScheduleOnByDetails.ForMonth,
                                            CustomerBranchId = CustomerbranchID,
                                            IsDeleted = false,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            ResultID = ResultID,
                                            RoleID = roleid,
                                            ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                            VerticalID = (int?)VerticalID,
                                            ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                            AuditID = AuditID,
                                        };
                                        AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                                        {
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                            ImplementationScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                            FinancialYear = FinancialYear,
                                            CustomerBranchId = CustomerbranchID,
                                            ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                            ForPeriod = ForMonth,
                                            ResultID = ResultID,
                                            RoleID = roleid,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            VerticalID = (int?)VerticalID,
                                            ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                            AuditID = AuditID,
                                        };

                                        if (ManagementResponse.Trim() == "")
                                            MstRiskResult.ManagementResponse = "";
                                        else
                                            MstRiskResult.ManagementResponse = ManagementResponse.Trim();

                                        if (PersonResponsible == "-1")
                                        {
                                            MstRiskResult.PersonResponsible = null;
                                            transaction.PersonResponsible = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                            transaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                        }
                                        if (FinalStatusIMP == "-1")
                                        {
                                            MstRiskResult.ImplementationStatus = null;
                                            transaction.ImplementationStatusId = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.ImplementationStatus = Convert.ToInt32(FinalStatusIMP);
                                            transaction.ImplementationStatusId = Convert.ToInt32(FinalStatusIMP);
                                        }
                                        DateTime dt = new DateTime();
                                        if (!string.IsNullOrEmpty(TimeLineValue.Trim()))
                                        {
                                            dt = DateTime.ParseExact(TimeLineValue.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            MstRiskResult.TimeLine = dt.Date;
                                        }
                                        else
                                            MstRiskResult.TimeLine = null;

                                        #endregion

                                        var MstRiskResultchk = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(Convert.ToInt32(ResultID), Convert.ToInt32(VerticalID), Convert.ToInt32(getImpAuditScheduleOnByDetails.Id), AuditID);
                                        if (MstRiskResultchk != null)
                                        {
                                            #region
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                            {
                                                MstRiskResult.ProcessWalkthrough = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ActualWorkDone))
                                            {
                                                MstRiskResult.ActualWorkDone = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ActualWorkDone = MstRiskResultchk.ActualWorkDone;
                                            }

                                            if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                            {
                                                MstRiskResult.Population = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Population = MstRiskResultchk.Population;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                            {
                                                MstRiskResult.Sample = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Sample = MstRiskResultchk.Sample;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                            {
                                                MstRiskResult.FixRemark = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;
                                            }
                                            // transaction.StatusId = MstRiskResultchk.Status;
                                            if (ViewState["rStatus"].ToString() == "TeamReview")
                                            {
                                                MstRiskResult.Status = 4;
                                                transaction.StatusId = 4;
                                                transaction.Remarks = "Implementation Steps Under Team Review.";
                                            }
                                            else if (ViewState["rStatus"].ToString() == "NotDone")
                                            {
                                                transaction.Remarks = "Implementation Steps Submitted.";
                                                transaction.StatusId = 2;
                                                MstRiskResult.Status = 2;
                                            }
                                            else if (ViewState["rStatus"].ToString() == "Submitted")
                                            {
                                                transaction.Remarks = "Implementation Steps Submitted.";
                                                transaction.StatusId = 2;
                                                MstRiskResult.Status = 2;
                                            }

                                            #endregion
                                        }
                                        else if (MstRiskResultchk == null)
                                        {
                                            transaction.Remarks = "Implementation Steps Submited.";
                                            transaction.StatusId = 2;
                                            MstRiskResult.Status = 2;
                                        }
                                        bool Success1 = false;
                                        bool Success2 = false;

                                        bool isvalidate = true;
                                        string msg = string.Empty;
                                        if (MstRiskResult.ImplementationStatus == null)
                                        {
                                            isvalidate = false;
                                            msg += "Implementation Status,";
                                        }
                                        if (MstRiskResult.PersonResponsible == null)
                                        {
                                            isvalidate = false;
                                            msg += "Person Responsible";
                                        }
                                        if (isvalidate)
                                        {
                                            if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                                            {
                                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                            }
                                            else
                                            {
                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                            }

                                            if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                                            {
                                                transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.UpdatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.UpdateAuditImplementationTransactionStatus(transaction);
                                            }
                                            else
                                            {
                                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.CreatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                            }

                                            if (Success1 == true && Success2 == true)
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Implementation Step Submitted Successfully to the Reviewer";
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please Provide " + msg.Trim(',') + " Before Save";
                                            //cvDuplicateEntry.ErrorMessage = "Please Provide Implementation Status and Person Responsible Before Save";
                                        }
                                    }//getImpAuditScheduleOnByDetails
                                }
                            }
                            else
                            {
                                //cvDuplicateEntry.IsValid = false;
                                //cvDuplicateEntry.ErrorMessage = "Select Implementation Status";
                                break;
                            }
                        }

                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                        #endregion
                    }
                    else if (Convert.ToInt32(ViewState["roleid"]) == 4)
                    {
                        #region Reviewer

                        foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                        {
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                            if (chkBx != null && chkBx.Checked)
                            {
                                long roleid = 4;
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                ManagementResponse = ((TextBox)rw.FindControl("tbxMgmResponce")).Text;
                                TimeLineValue = ((TextBox)rw.FindControl("txtStartDate")).Text;
                                PersonResponsible = ((DropDownList)rw.FindControl("ddlPersonResponsible")).SelectedValue;
                                FinalStatusIMP = ((DropDownList)rw.FindControl("ddlIMPStatus")).SelectedValue;

                                if (FinalStatusIMP != "" && FinalStatusIMP != "-1")
                                {
                                    LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                                    String Args = chklnkbutton.CommandArgument.ToString();
                                    string[] arg = Args.ToString().Split(',');

                                    long ResultID = Convert.ToInt32(arg[0]);
                                    long CustomerbranchID = Convert.ToInt32(arg[1]);
                                    string FinancialYear = arg[2].ToString();
                                    string ForMonth = arg[3].ToString();
                                    long VerticalID = Convert.ToInt32(arg[5]);
                                    long Scheduledonid = Convert.ToInt32(arg[6]);
                                    long AuditID = Convert.ToInt32(arg[7]);
                                    var getImpAuditScheduleOnByDetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinancialYear, ForMonth, Convert.ToInt32(CustomerbranchID), Convert.ToInt32(VerticalID), Convert.ToInt32(Scheduledonid), AuditID);
                                    if (getImpAuditScheduleOnByDetails != null)
                                    {
                                        if (DropDownListPersonResponsible.SelectedValue != "")
                                        {
                                            int StatusID = 0;
                                            StatusID = Convert.ToInt32(DropDownListPersonResponsible.SelectedValue);
                                            #region
                                            ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                                            {
                                                AuditScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                                FinancialYear = getImpAuditScheduleOnByDetails.FinancialYear,
                                                ForPeriod = getImpAuditScheduleOnByDetails.ForMonth,
                                                CustomerBranchId = CustomerbranchID,
                                                IsDeleted = false,
                                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                ResultID = ResultID,
                                                RoleID = roleid,
                                                ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                                VerticalID = (int?)VerticalID,
                                                ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                                AuditID = AuditID,
                                            };
                                            AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                                            {
                                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                                StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                                ImplementationScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                                FinancialYear = FinancialYear,
                                                CustomerBranchId = CustomerbranchID,
                                                ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                                ForPeriod = ForMonth,
                                                ResultID = ResultID,
                                                RoleID = roleid,
                                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                VerticalID = (int?)VerticalID,
                                                ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                                AuditID = AuditID,
                                            };

                                            if (ManagementResponse.Trim() == "")
                                                MstRiskResult.ManagementResponse = "";
                                            else
                                                MstRiskResult.ManagementResponse = ManagementResponse.Trim();

                                            if (PersonResponsible == "-1")
                                            {
                                                MstRiskResult.PersonResponsible = null;
                                                transaction.PersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                                transaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                            }
                                            if (FinalStatusIMP == "-1")
                                            {
                                                MstRiskResult.ImplementationStatus = null;
                                                transaction.ImplementationStatusId = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.ImplementationStatus = Convert.ToInt32(FinalStatusIMP);
                                                transaction.ImplementationStatusId = Convert.ToInt32(FinalStatusIMP);
                                            }
                                            DateTime dt = new DateTime();
                                            if (!string.IsNullOrEmpty(TimeLineValue.Trim()))
                                            {
                                                dt = DateTime.ParseExact(TimeLineValue.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                MstRiskResult.TimeLine = dt.Date;
                                            }
                                            else
                                                MstRiskResult.TimeLine = null;

                                            string remark = string.Empty;

                                            if (DropDownListPersonResponsible.SelectedItem.Text == "Closed")
                                            {
                                                transaction.StatusId = 3;
                                                MstRiskResult.Status = 3;
                                                remark = "Implementation Steps Closed.";
                                            }
                                            else if (DropDownListPersonResponsible.SelectedItem.Text == "Team Review")
                                            {
                                                transaction.StatusId = 4;
                                                MstRiskResult.Status = 4;
                                                remark = "Implementation Steps Under Team Review.";
                                            }
                                            else if (DropDownListPersonResponsible.SelectedItem.Text == "Final Review")
                                            {
                                                transaction.StatusId = 5;
                                                MstRiskResult.Status = 5;
                                                remark = "Implementation Steps Under Final Review.";
                                            }
                                            else if (DropDownListPersonResponsible.SelectedItem.Text == "Auditee Review")
                                            {
                                                transaction.StatusId = 6;
                                                MstRiskResult.Status = 6;
                                                remark = "Implementation Steps Under Auditee Review.";
                                            }
                                            transaction.Remarks = remark.Trim();
                                            #endregion
                                            var MstRiskResultchk = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(Convert.ToInt32(ResultID), Convert.ToInt32(VerticalID), Convert.ToInt32(getImpAuditScheduleOnByDetails.Id), AuditID);
                                            if (MstRiskResultchk != null)
                                            {
                                                if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                                {
                                                    MstRiskResult.ProcessWalkthrough = null;
                                                }
                                                else
                                                {
                                                    MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.ActualWorkDone))
                                                {
                                                    MstRiskResult.ActualWorkDone = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.ActualWorkDone = MstRiskResultchk.ActualWorkDone;
                                                }

                                                if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                                {
                                                    MstRiskResult.Population = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.Population = MstRiskResultchk.Population;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                                {
                                                    MstRiskResult.Sample = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.Sample = MstRiskResultchk.Sample;
                                                }
                                                if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                                {
                                                    MstRiskResult.FixRemark = "";
                                                }
                                                else
                                                {
                                                    MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;
                                                }
                                            }//risk result code


                                            bool Success1 = false;
                                            bool Success2 = false;

                                            bool isvalidate = true;
                                            string msg = string.Empty;
                                            if (MstRiskResult.ImplementationStatus == null)
                                            {
                                                isvalidate = false;
                                                msg += "Implementation Status,";
                                            }
                                            if (MstRiskResult.PersonResponsible == null)
                                            {
                                                isvalidate = false;
                                                msg += "Person Responsible";
                                            }
                                            if (isvalidate)
                                            {
                                                if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                                                {
                                                    if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                                                    {
                                                        if (ddlFilterStatus.SelectedValue == "5")
                                                        {
                                                            if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                                            {
                                                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResultResult(MstRiskResult, 5);
                                                            }
                                                            else
                                                            {
                                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                                            {
                                                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                                            }
                                                            else
                                                            {
                                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                                    }
                                                }
                                                else
                                                {
                                                    MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    MstRiskResult.CreatedOn = DateTime.Now;
                                                    Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                                }
                                                if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                                                {
                                                    if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                                                    {
                                                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        transaction.CreatedOn = DateTime.Now;
                                                        Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                                    }
                                                    else
                                                    {
                                                        transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        transaction.UpdatedOn = DateTime.Now;
                                                        Success2 = RiskCategoryManagement.UpdateAuditImplementationTransactionTxnStatusReviewer(transaction);
                                                    }
                                                }
                                                else
                                                {
                                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.CreatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Please Provide " + msg.Trim(',') + " Before Save";
                                                //cvDuplicateEntry.ErrorMessage = "Please Provide Implementation Status,Person Responsible,Time Line and Management Reponse Before Save";
                                            }
                                        }
                                    }//getImpAuditScheduleOnByDetails
                                }
                                else
                                {
                                    //cvDuplicateEntry.IsValid = false;
                                    //cvDuplicateEntry.ErrorMessage = "Select Implementation Status.";
                                }
                            }//if (chkBx != null && chkBx.Checked)

                        }//foreach      
                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                        #endregion
                    }//elseif end
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void bntAuditHistory_Click(object sender, EventArgs e)
        {
            try
            {
                string ActionCHK = Request.QueryString["Status"];

                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";

                    if (ViewState["roleid"] != null)
                    {
                        if (Convert.ToInt32(ViewState["roleid"]) == 3)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogHistory(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + "," + arg[8] + "," + arg[9] + ",'" + ActionCHK + "');", true);
                        else if (Convert.ToInt32(ViewState["roleid"]) == 4)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "ShowReviewerDialogHistory(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + "," + arg[8] + "," + arg[9] + ",'" + ActionCHK + "');", true);
                        else if (Convert.ToInt32(ViewState["roleid"]) == 5)
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "ShowReviewerDialogHistory(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + "," + arg[8] + "," + arg[9] + ",'" + ActionCHK + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}