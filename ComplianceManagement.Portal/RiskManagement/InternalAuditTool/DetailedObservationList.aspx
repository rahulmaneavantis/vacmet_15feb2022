﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailedObservationList.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.DetailedObservationList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->

    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <style type="text/css">
        .body {
            background: white !important;
        }

        /*ToggleSwitch Start*/
        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 48px;
            height: 19px;
        }

            /* Hide default HTML checkbox */
            .switch input {
                opacity: 0;
                width: 0;
                height: 0;
            }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 10px;
                width: 15px;
                left: 4px;
                bottom: 3px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            background-color: green;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

            .slider.round:before {
                border-radius: 50%;
            }
        /*End*/
        .modal-header {
            border-bottom: white !important;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .table-paging-text p {
            color: #555;
            text-align: center;
            font-size: 14px;
            padding-top: 7px;
        }

        .auto-style1 {
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0;
            right: 0;
            left: 0;
            z-index: 9999999;
        }

        .auto-style2 {
            position: fixed;
            top: 30%;
            left: 40%;
        }

        .text_NlinesusingCSS {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            line-height: 18px;
            max-height: 50px;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }

        .header_alignment {
            text-align: center !important;
        }

        .search-choice-close {
            display: none !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {

        });

        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        });

        function viewObs() {
            $('#Viewobservation').modal('show');
        }

        function closePop() {
            
            //$('#divPREDOCShowDialog').modal('hide');
            window.parent.HistoryMethod();
        }

        function removeData() {
            window.parent.RemoveData();
        }

        function fopenpopup() {
            $('#ViewAuditStep').modal('show');
        }

        function check_uncheck() {
            debugger;
            var sessionValue = '<%=Convert.ToString(Session["Observation"]) %>';
            if (sessionValue == '') {
                sessionValue= '<%=StoreSessionvalue%>';
            }
            sessionValue = sessionValue.replace(/^\s+|\s+$|\s+(?=\s)/g, "");
            var ddlSelectedCol = sessionValue;
            var colVAL = "";
            var finalValuenew = "";
            var finalValue = "";
            var newFinal = "";

            var count = $('#grdDetailedObs tr').length;

            for (var i = 1; i <= count; i++) {
                var currVAL = $("#grdDetailedObs tr:eq(" + i + ")").find("td:eq(1)").html();
                if (currVAL != null) {
                    var cutrrVal = currVAL.split('top">');
                    if (cutrrVal[1] != null) {
                        finalValuenew = cutrrVal[1].split('</span>');
                        newFinal = finalValuenew[0];
                        newFinal = newFinal.replace("'", "`");
                        newFinal = newFinal
                        finalValue = "";
                        for (var z = 0; z < newFinal.length; z++) {
                            if (!(newFinal[z] == '\n' || newFinal[z] == '\r'))
                                finalValue += newFinal[z];
                        }
                        if (finalValue.includes('&nbsp;')) {
                            finalValue = finalValue.replace('&nbsp;', '');
                        }
                        if (finalValue.includes('&amp;')) {
                            finalValue = finalValue.replace('&amp;', '&');
                        }
                    }
                }
                debugger;
                if (finalValue == sessionValue) {
                    var value = $("#grdDetailedObs tr:eq(" + i + ")").find("td:eq(4) input[type='checkbox']").val();
                    $('input[id*="togBtn"][value="' + value.toString() + '"]').prop("checked", true);
                    sessionStorage.setItem('va', i);
                    sessionStorage.setItem('ft', 0);
                    break;
                }
                else {
                    sessionStorage.setItem('ft', null);
                }

                //if (finalValue.match(sessionValue)) {
                //    var value = $("#grdDetailedObs tr:eq(" + i + ")").find("td:eq(4) input[type='checkbox']").val();
                //    $('input[id*="togBtn"][value="' + value.toString() + '"]').prop("checked", true);
                //    sessionStorage.setItem('va', i);
                //    break;
                //}
            }

            <%--$("#<%=grdDetailedObs.ClientID %> tr").each(function () {   
                var currVAL = $(this).find("td:eq(1)").html();
                if (currVAL != null) {
                    var cutrrVal = currVAL.split('top">');
                    if (cutrrVal[1] != null) {
                        finalValuenew = cutrrVal[1].split('</span>');
                        finalValue = finalValuenew[0];
                    }
                }

                if (finalValue == sessionValue) {
                    var value = $('#grdDetailedObs').find("td:eq(4) input[type='checkbox']").val();
                    $('input[id*="togBtn"][value="' + value.toString() + '"]').prop("checked", true);
                    sessionStorage.removeItem('auditIdatbdId');
                }
            });--%>
        }

    </script>
    <script type="text/javascript">

        function check() {
            debugger;
            //if ($('#togBtn').prop("checked")) {
            if ($("[name='disableYXLogo']:checked")) {
                
                var tcount = $('#grdDetailedObs tr').length;
                var oldValue = sessionStorage.getItem('va');
                var firstTime = sessionStorage.getItem('ft');
                var id = null;
                var a = false;
                for (var i = 1; i < tcount; i++) {
                    if (oldValue != null) {
                        $("#grdDetailedObs tr:eq(" + oldValue + ")").find("td:eq(4) input[type='checkbox']").removeAttr('checked');
                        sessionStorage.removeItem('va');
                    }
                    if ($("#grdDetailedObs tr:eq(" + i + ")").find("td:eq(4) input[type='checkbox']").is(':checked')) {
                        for (var j = 1; j < tcount; j++) {
                            if (j != i) {
                                $("#grdDetailedObs tr:eq(" + j + ")").find("td:eq(4) input[type='checkbox']").removeAttr('checked');
                            }
                        }
                        id = $("#grdDetailedObs tr:eq(" + i + ")").find("td:eq(4) input[type='checkbox']").val();

                        sessionStorage.removeItem('auditIdatbdId');
                        sessionStorage.setItem('auditIdatbdId', id);
                        debugger;
                        if (oldValue != null && id != null && firstTime == "0") {
                            a = confirm('Are you certain to change the observation selection, as due to the same, all data will be reset at the observation screen ?');
                        }
                        else if (oldValue == null && id != null && (firstTime == "null" || firstTime == null)) {
                            a = confirm('Do you want to select this observation?');
                        }
                        else if (oldValue == null && id != null && (firstTime == "0" || firstTime == null)) {
                            a = confirm('Do you want to select this observation?');
                        }

                        if (a == true) {
                            //$('#togBtn').prop('checked', true);

                            $.ajax({
                                type: "POST",
                                url: "DetailedObservationList.aspx/SetSession",
                                data: '{id: "' + id + '" }',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    
                                    closePop();
                                },
                            });

                        }
                        else {
                            if (oldValue != null && id != null) {
                                var valueCheck = $("#grdDetailedObs tr:eq(" + oldValue + ")").find("td:eq(4) input[type='checkbox']").val();
                                $('input[id*="togBtn"][value="' + valueCheck.toString() + '"]').prop("checked", true); //Commented by Sushant
                                $('input[id*="togBtn"][value="' + id.toString() + '"]').removeAttr("checked");
                                id = null;
                                oldValue = oldValue;
                                sessionStorage.setItem('va', oldValue);
                                return;

                            }
                            //else if (oldValue == null && id != null) {
                            //    $('input[id*="togBtn"][value="' + id.toString() + '"]').prop("checked", true);
                            //    id = id;
                            //    break;
                            //}
                            else {
                                $("#grdDetailedObs tr:eq(" + i + ")").find("td:eq(4) input[type='checkbox']").removeAttr('checked');
                                sessionStorage.removeItem('va');
                            }
                        }
                    }
                }

                if (oldValue != null && id == null && firstTime == "0") {
                    deselectId = $("#grdDetailedObs tr:eq(" + oldValue + ")").find("td:eq(4) input[type='checkbox']").val();
                    var b = confirm('Are you certain to deselect the below observation as due to the same, all data will be reset at the observation screen?');
                    if (b == true) {
                        
                        $('input[type=checkbox]').prop('checked', false);
                        var id = null;
                        $.ajax({
                            type: "POST",
                            url: "DetailedObservationList.aspx/SetSession",
                            data: '{id: "' + id + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                removeData();
                                sessionStorage.removeItem('auditIdatbdId');
                            },
                        });
                    }
                    else {
                        //$("#grdDetailedObs tr:eq(" + oldValue + ")").find("td:eq(4) input[type='checkbox']").val();
                        var valueCheck = $("#grdDetailedObs tr:eq(" + oldValue + ")").find("td:eq(4) input[type='checkbox']").val();
                        $('input[id*="togBtn"][value="' + valueCheck.toString() + '"]').prop("checked", true);
                        oldValue = oldValue;
                        sessionStorage.setItem('va', oldValue);
                    }
                }
            }
            else {
                confirm('do you want to deselect this observation?');
                if (confirm) {
                    
                    $('input[type=checkbox]').prop('checked', false);
                    var id = null;
                    $.ajax({
                        type: "POST",
                        url: "DetailedObservationList.aspx/SetSession",
                        data: '{id: "' + id + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            removeData();
                            sessionStorage.removeItem('auditIdatbdId');
                        },
                    });
                }
                else {
                    $('input[type=checkbox]').prop('checked', true);
                }
            }
        }
        //});
    </script>
</head>
<body style="background-color: white;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upObservationDetails">
                <ProgressTemplate>
                    <div style="text-align: center; background-color: #000000; opacity: 0.3;" class="auto-style1">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;" CssClass="auto-style2" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <asp:UpdatePanel ID="upObservationDetails" runat="server" UpdateMode="Conditional" style="background: white;">
                <ContentTemplate>
                    <div>
                        <div class="text_NlinesusingCSS" style="float: left;">
                            <asp:Label runat="server" Style="width: 100%; display: block; float: left; font-size: 15px; color: #666;">Audit Step: </asp:Label>
                        </div>
                        <div>
                            <div class="text_NlinesusingCSS" style="width: 80%; float: left;">
                                <div>
                                    <asp:Label ID="lblAuditStep" runat="server" Text="" Style="width: 100%; display: block; font-size: 14px; color: #666; margin-left: 2%;"></asp:Label>
                                </div>
                            </div>
                            <div style="float: right; margin-right: 4%;">
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">
                                   <img src="../../Images/Info_icon_obs.png" title="View Audit Step" alt="View Audit Step" data-toggle="tooltip" data-placement="bottom" />
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0 entrycount" style="margin-top: 22px;">
                            <div class="col-md-1 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="5" />
                                <asp:ListItem Text="10" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" Selected="True" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="margin-top: 90px">
                        <asp:GridView runat="server" ID="grdDetailedObs" AutoGenerateColumns="false" GridLines="None" OnRowCommand="grdDetailedObs_RowCommand"
                            PageSize="50" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="95%" ShowHeaderWhenEmpty="true" OnRowDataBound="grdDetailedObs_RowDataBound"
                            AllowSorting="true">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="AuditID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAuditID" Style="display: none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                        <asp:Label runat="server" ID="lblATBDId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ATBDId") %>' ToolTip='<%# Eval("ATBDId") %>'></asp:Label>
                                        <%--<asp:Label runat="server" ID="lblAuditStepId" data-toggle="tooltip" data-placement="top" Text='<%# Eval("AuditStepId") %>' ToolTip='<%# Eval("AuditStepId") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Observation Title" Visible="false">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 350px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                <asp:Label ID="lblObservationTitle" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ObservationTitle") %>' ToolTip='<%# Eval("ObservationTitle") %>' Width="200px"></asp:Label>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Detailed Observation">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 500px;">
                                            <asp:Label ID="lblDetailedObservation" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Observation") %>' ToolTip='<%# Eval("Observation") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="View" HeaderStyle-CssClass="header_alignment" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div class="text_NlinesusingCSS" style="width: 50px;">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="ViewObservation" OnClientClick="check_uncheck()"
                                                CommandArgument='<%# Eval("AuditID") +"," +Eval("ATBDId") %>'><img src="../../Images/view-icon-new.png" alt="View Observation" title="View Observation" data-toggle="tooltip" /></asp:LinkButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Repeated Counts" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_alignment">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                            <asp:Label ID="lblObservationCount" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("RepeatedCount") %>' ToolTip='<%# Eval("RepeatedCount") %>'></asp:Label>
                                            <%--<asp:LinkButton ID="lnkObservationCount" runat="server" CommandName="ObservationPopUp" 
                                                    CommandArgument='<%# Eval("ProcessId") +"," +Eval("SubProcessId")+"," + Eval("AuditID")+"," +  Eval("ATBDId")+","+ Eval("AuditStepId") %>' Text='<%# Eval("ObservationCount") %>' ></asp:LinkButton>--%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Select Observation">
                                    <ItemTemplate>
                                        <%--<asp:LinkButton ID="lbtSelect" runat="server" CommandName="Select_Observation" CommandArgument='<%# Eval("AuditID")+","+ Eval("ATBDId") %>'
                                            OnClientClick="return confirm('Are you certain you want to select this observation?');">
                                            <img src="../../Images/delete_icon_new.png" alt="Select Observation" title="Select Observation" data-toggle="tooltip"/>
                                        </asp:LinkButton>--%>
                                        <label class="switch">
                                            <%--<input type="checkbox" id="togBtn_<%# Eval("AuditID")+","+ Eval("ATBDId") %>" value="<%# Eval("AuditID")+","+ Eval("ATBDId") %>" name="disableYXLogo">--%>
                                            <input type="checkbox" id="togBtn" value="<%# Eval("AuditID")+","+ Eval("ATBDId") %>" name="disableYXLogo" onclick="check();">
                                            <%--<asp:CheckBox >--%>
                                            <%--<asp:CheckBox runat="server" ID="togBtn_<%# Eval("AuditID")+","+ Eval("ATBDId") %>" Checked='<%# CheckObservation(Convert.ToString(Eval("Observation"))).ToString().Equals("1") %>' name="disableYXLogo"  />--%>
                                            <%--<asp:CheckBox ID="togBtn" runat="server" name="disableYXLogo"  />--%>
                                            <span class="slider round"></span>
                                            <asp:Button ID="btnToggle" CommandArgument='<%# Eval("AuditID")+","+ Eval("ATBDId") %>' CommandName="ToggleBtn" runat="server" Style="display: none;" OnClick="btnToggle_Click" />
                                        </label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <HeaderStyle BackColor="#ECF0F1" />
                            <PagerSettings Visible="false" />
                            <PagerTemplate>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found.
                                   
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <%--<div style="float: right; background: white;">
                            <p style="color: #999; margin-top: 5px; margin-right: 5px;">Page </p>
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" Style="margin-left: -29%;"
                                class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </div>--%>
                        <div style="float: right; margin-right: 3%; margin-top: 0.1%;">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                class="form-control m-bot15" Width="110%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </div>
                        <div class="col-md-12 colpadding0" style="background: white;">
                            <div class="col-md-5 colpadding0" style="background: white;">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 colpadding0" style="float: right; background: white;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                            <%--                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="modal fade" id="Viewobservation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 43%;">
                <div class="modal-content" style="height: auto; width: auto;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size: 15px;">x</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <div id="Viewobs">
                            <asp:UpdatePanel ID="upViewobservation" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-md-12" style="margin-top:-2%;">
                                        <label style="width: 100%; display: block; float: left; font-size: 15px; color: black;">Observation</label>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 8%;">
                                        <asp:Label ID="lblObservation" runat="server" Text="" Style="width: 100%; display: block; float: left; font-size: 15px; color: #666;"></asp:Label>
                                    </div>
                                    <div style="margin-bottom: 7px; text-align: center;margin-top: 8%;">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnClose" aria-hidden="true" style="margin-top: 5%;">Close</button>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ViewAuditStep" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 43%;">
                <div class="modal-content" style="height: auto;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size: 15px;">x</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <div id="viewAudit">
                            <asp:UpdatePanel ID="upViewAuditStep" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-md-12" style="margin-top:-2%;">
                                        <label style="width: 100%; display: block; float: left; font-size: 15px; color: black;">Audit Step</label>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 8%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Label ID="lblviewAuditStep" runat="server" Text="" Style="width: 100%; display: block; float: left; font-size: 15px; color: #666; margin-top: -7%;"></asp:Label>
                                    </div>
                                    <div style="margin-bottom: 7px; margin-top: 8%; text-align: center;">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnClose" aria-hidden="true" style="margin-top: 3%;">Close</button>  <%--OnClientClick="check_uncheck()"--%>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>


