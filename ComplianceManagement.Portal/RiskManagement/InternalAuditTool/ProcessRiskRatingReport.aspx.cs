﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ProcessRiskRatingReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;                
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlLegalEntity_SelectedIndexChanged(sender, e);
                BindLegalEntityData();

                BindData(-1);
                bindPageNumber1();                             
                bindPageNumber();
                lblProcesswisePlotring.Visible = false;
                Label10.Visible = false;
                Label3.Visible = false;
                BindVertical();
            }
        }
        private void bindPageNumber1()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo1.Items.Count > 0)
                {
                    DropDownListPageNo1.Items.Clear();
                }

                DropDownListPageNo1.DataTextField = "ID";
                DropDownListPageNo1.DataValueField = "ID";

                DropDownListPageNo1.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo1.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo1.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo1.Items.Add("0");
                    DropDownListPageNo1.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCompliances.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindData(-1);

        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount1());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCompliances.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindData(-1);

        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }
                
                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public string GetRatingName(int Rating)
        {
            string processnonprocess = "";
            if (Rating != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    mst_Risk_ControlRating mstcustomerbranch = (from row in entities.mst_Risk_ControlRating
                                                                where row.Value == Rating && row.IsActive == false && row.IsRiskControl == "R"
                                                                select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }
        

        public int GetBranchID()
        {
            int CustomerBranchId = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            return CustomerBranchId;
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    lblCustomername.Text = string.Empty;
                    lblRiskwisePlotring.Text = string.Empty;
                    Label4.Visible = false;
                    Label6.Visible = false;
                    dvshowpagesize.Visible = true;
                    dvShowPageNumber.Visible = true;
                    lblProcesswisePlotring.Visible = true;
                    Label10.Visible = true;
                    Label3.Visible = true;

                    BindData(Convert.ToInt32(ddlLegalEntity.SelectedValue));

                    BindMatrixProcessWiseData();
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    lblCustomername.Text = string.Empty;
                    lblRiskwisePlotring.Text = string.Empty;
                    Label4.Visible = false;
                    Label6.Visible = false;
                    dvshowpagesize.Visible = true;
                    dvShowPageNumber.Visible = true;
                    lblProcesswisePlotring.Visible = true;
                    Label10.Visible = true;
                    Label3.Visible = true;

                    BindData(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindMatrixProcessWiseData();
                    BindVertical();

                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    lblCustomername.Text = string.Empty;
                    lblRiskwisePlotring.Text = string.Empty;
                    Label4.Visible = false;
                    Label6.Visible = false;
                    dvshowpagesize.Visible = true;
                    dvShowPageNumber.Visible = true;
                    lblProcesswisePlotring.Visible = true;
                    Label10.Visible = true;
                    Label3.Visible = true;

                    BindData(Convert.ToInt32(ddlSubEntity2.SelectedValue));

                    BindMatrixProcessWiseData();
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    lblCustomername.Text = string.Empty;
                    lblRiskwisePlotring.Text = string.Empty;
                    Label4.Visible = false;
                    Label6.Visible = false;
                    dvshowpagesize.Visible = true;
                    dvShowPageNumber.Visible = true;
                    lblProcesswisePlotring.Visible = true;
                    Label10.Visible = true;
                    Label3.Visible = true;

                    BindData(Convert.ToInt32(ddlSubEntity3.SelectedValue));

                    BindMatrixProcessWiseData();
                    BindVertical();
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }


        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCustomername.Text = string.Empty;
            lblRiskwisePlotring.Text = string.Empty;
            Label4.Visible = false;
            Label6.Visible = false;
            dvshowpagesize.Visible = true;
            dvShowPageNumber.Visible = true;
            lblProcesswisePlotring.Visible = true;
            Label10.Visible = true;
            Label3.Visible = true;

            BindData(Convert.ToInt32(ddlSubEntity4.SelectedValue));

            BindMatrixProcessWiseData();
            BindVertical();
        }

        public void BindDataRatingDetails(int customerbranchid, int ProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (customerbranchid != -1)
                {
                    int customerID = -1;                    
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);                   
                    var ProcessRiskRatingReportViewDetails = ProcessManagement.GetSPProcessRiskRatingDetail(customerID, customerbranchid, ProcessID).ToList();

                    grdRiskRatingMatrix.DataSource = null;
                    grdRiskRatingMatrix.DataBind();
                    grdProcessRatingDetails.DataSource = null;
                    grdProcessRatingDetails.DataBind();

                    grdProcessRatingDetails.DataSource = ProcessRiskRatingReportViewDetails.ToList();
                    Session["TotalRows"] = ProcessRiskRatingReportViewDetails.Count;
                    grdProcessRatingDetails.DataBind();
                   // GetPageDisplaySummary();
                }
            }
        }
        public void BindData(int customerbranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int customerID = -1;                
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int verticalID = -1;
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        verticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            verticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                List<ProcessRiskRatingView> ProcessRiskRatingReportDetails = new List<ProcessRiskRatingView>();
                if (customerbranchid != -1 && verticalID != -1)
                {
                    ProcessRiskRatingReportDetails =
                               (from row in entities.ProcessRiskRatingViews
                                where row.CustomerBranchId == customerbranchid
                                && row.CustomerID == customerID && row.VerticalID == verticalID
                                select row).Distinct().ToList();
                }
                else if (customerbranchid != -1)
                {
                      ProcessRiskRatingReportDetails =
                               (from row in entities.ProcessRiskRatingViews
                                where row.CustomerBranchId == customerbranchid
                                && row.CustomerID == customerID 
                                select row).Distinct().ToList();
                }               
                grdCompliances.DataSource = null;
                grdCompliances.DataBind();

                grdRiskRatingMatrix.DataSource = null;
                grdRiskRatingMatrix.DataBind();
                grdProcessRatingDetails.DataSource = null;
                grdProcessRatingDetails.DataBind();
                lblCustomername.Text = string.Empty;
                lblRiskwisePlotring.Text = string.Empty;
                grdCompliances.DataSource = ProcessRiskRatingReportDetails.ToList();
                Session["TotalRows1"] = ProcessRiskRatingReportDetails.Count;
                grdCompliances.DataBind();
                

                grdCompliances.Visible = true;
                DivProcess.Visible = true;
                DivRisk.Visible = false;


                liPerformer.Attributes.Add("class", "active");
                liReviewer.Attributes.Add("class", "");


                DivProcess.Attributes.Remove("class");
                DivProcess.Attributes.Add("class", "tab-pane active");

                DivRisk.Attributes.Remove("class");
                DivRisk.Attributes.Add("class", "tab-pane");
            }
        }
        public void BindMatrixProcessWiseData()
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DataTable dt = new DataTable();

                    List<mst_Risk_ControlRating> mstRiskControlRatinglistRisk = (from row in entities.mst_Risk_ControlRating
                                                                                 where row.IsActive == false && row.IsRiskControl == "R"
                                                                                 orderby row.Value descending
                                                                                 select row).ToList();

                    if (mstRiskControlRatinglistRisk != null)
                    {
                        List<string> a = new List<string>();
                        int count = mstRiskControlRatinglistRisk.Count;
                        if (count == 3)
                        {
                            DataColumn dcol = new DataColumn("A", typeof(System.String));
                            dt.Columns.Add(dcol);
                            mstRiskControlRatinglistRisk.ForEach(entry =>
                            {
                                dcol = new DataColumn(entry.Name, typeof(System.String));
                                dt.Columns.Add(dcol);
                                a.Add(entry.Name.ToUpper());
                            });

                            mstRiskControlRatinglistRisk.ForEach(entry =>
                            {
                                var aaa = a[0].ToString();

                                var aaaa = a[1].ToString();

                                var aaaaa = a[2].ToString();

                                var high = (from row in entities.mst_Risk_ControlRating
                                            where row.IsActive == false && row.IsRiskControl == "C" && row.Name == aaa
                                            select row.Value).FirstOrDefault();

                                var medium = (from row in entities.mst_Risk_ControlRating
                                              where row.IsActive == false && row.IsRiskControl == "C" && row.Name == aaaa
                                              select row.Value).FirstOrDefault();


                                var low = (from row in entities.mst_Risk_ControlRating
                                           where row.IsActive == false && row.IsRiskControl == "C" && row.Name == aaaaa
                                           select row.Value).FirstOrDefault();

                                if (entry.Name.ToUpper() == "LOW")
                                {
                                    dt.Rows.Add("HIGH", entry.Value * low, entry.Value * medium, entry.Value * high);
                                    //avalue.Add(entry.Value * low);
                                }
                                if (entry.Name.ToUpper() == "MEDIUM")
                                {
                                    dt.Rows.Add("MEDIUM", entry.Value * low, entry.Value * medium, entry.Value * high);
                                }
                                if (entry.Name.ToUpper() == "HIGH")
                                {
                                    dt.Rows.Add("LOW", entry.Value * low, entry.Value * medium, entry.Value * high);
                                }
                            });
                            dt.Rows.Add("", "LOW", "MEDIUM", "HIGH");

                            #region Working Code
                            //dt.Columns.AddRange(new DataColumn[4] 
                            //{ 
                            //    new DataColumn("-", typeof(string)),
                            //    new DataColumn("Low", typeof(int)),
                            //    new DataColumn("Medium",typeof(int)), 
                            //    new DataColumn("High",typeof(int))
                            //});

                            //mstRiskControlRatinglistRisk.ForEach(entry =>
                            //{                               
                            //    var high = (from row in entities.mst_Risk_ControlRating
                            //                where row.IsActive == false && row.IsRiskControl == "C" && row.Name == "High"
                            //                select row.Value).FirstOrDefault();

                            //    var medium = (from row in entities.mst_Risk_ControlRating
                            //                  where row.IsActive == false && row.IsRiskControl == "C" && row.Name == "Medium"
                            //                  select row.Value).FirstOrDefault();


                            //    var low = (from row in entities.mst_Risk_ControlRating
                            //               where row.IsActive == false && row.IsRiskControl == "C" && row.Name == "Low"
                            //               select row.Value).FirstOrDefault();

                            //    if (entry.Name == "Low")
                            //    {
                            //        dt.Rows.Add("High", entry.Value * high, entry.Value * medium, entry.Value * low);
                            //    }
                            //    if (entry.Name == "Medium")
                            //    {
                            //        dt.Rows.Add("Medium", entry.Value * high, entry.Value * medium, entry.Value * low);
                            //    }
                            //    if (entry.Name == "High")
                            //    {
                            //        dt.Rows.Add("Low", entry.Value * high, entry.Value * medium, entry.Value * low);
                            //    }
                            //});
                            #endregion
                        }
                        grdRiskRatingMatrixProcess.DataSource = null;
                        grdRiskRatingMatrixProcess.DataBind();

                        grdRiskRatingMatrixProcess.DataSource = dt;
                        grdRiskRatingMatrixProcess.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindMatrixData()
        {
            try
            {

                Label4.Visible = true;
                Label6.Visible = true;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DataTable dt = new DataTable();

                    List<mst_Risk_ControlRating> mstRiskControlRatinglistRisk = (from row in entities.mst_Risk_ControlRating
                                                                                 where row.IsActive == false && row.IsRiskControl == "R"
                                                                                 orderby row.Value descending
                                                                                 select row).ToList();

                    if (mstRiskControlRatinglistRisk != null)
                    {
                        List<string> a = new List<string>();
                        int count = mstRiskControlRatinglistRisk.Count;
                        if (count == 3)
                        {
                            DataColumn dcol = new DataColumn("A", typeof(System.String));
                            dt.Columns.Add(dcol);
                            mstRiskControlRatinglistRisk.ForEach(entry =>
                            {
                                dcol = new DataColumn(entry.Name, typeof(System.String));
                                dt.Columns.Add(dcol);
                                a.Add(entry.Name.ToUpper());
                            });

                            mstRiskControlRatinglistRisk.ForEach(entry =>
                            {
                                var aaa = a[0].ToString();

                                var aaaa = a[1].ToString();

                                var aaaaa = a[2].ToString();

                                var high = (from row in entities.mst_Risk_ControlRating
                                            where row.IsActive == false && row.IsRiskControl == "C" && row.Name == aaa
                                            select row.Value).FirstOrDefault();

                                var medium = (from row in entities.mst_Risk_ControlRating
                                              where row.IsActive == false && row.IsRiskControl == "C" && row.Name == aaaa
                                              select row.Value).FirstOrDefault();


                                var low = (from row in entities.mst_Risk_ControlRating
                                           where row.IsActive == false && row.IsRiskControl == "C" && row.Name == aaaaa
                                           select row.Value).FirstOrDefault();

                                if (entry.Name.ToUpper() == "LOW")
                                {
                                    dt.Rows.Add("HIGH", entry.Value * low, entry.Value * medium, entry.Value * high);
                                    //avalue.Add(entry.Value * low);
                                }
                                if (entry.Name.ToUpper() == "MEDIUM")
                                {
                                    dt.Rows.Add("MEDIUM", entry.Value * low, entry.Value * medium, entry.Value * high);
                                }
                                if (entry.Name.ToUpper() == "HIGH")
                                {
                                    dt.Rows.Add("LOW", entry.Value * low, entry.Value * medium, entry.Value * high);
                                }
                            });
                            dt.Rows.Add("", "LOW", "MEDIUM", "HIGH");

                            #region Working Code
                            //dt.Columns.AddRange(new DataColumn[4] 
                            //{ 
                            //    new DataColumn("-", typeof(string)),
                            //    new DataColumn("Low", typeof(int)),
                            //    new DataColumn("Medium",typeof(int)), 
                            //    new DataColumn("High",typeof(int))
                            //});

                            //mstRiskControlRatinglistRisk.ForEach(entry =>
                            //{                               
                            //    var high = (from row in entities.mst_Risk_ControlRating
                            //                where row.IsActive == false && row.IsRiskControl == "C" && row.Name == "High"
                            //                select row.Value).FirstOrDefault();

                            //    var medium = (from row in entities.mst_Risk_ControlRating
                            //                  where row.IsActive == false && row.IsRiskControl == "C" && row.Name == "Medium"
                            //                  select row.Value).FirstOrDefault();


                            //    var low = (from row in entities.mst_Risk_ControlRating
                            //               where row.IsActive == false && row.IsRiskControl == "C" && row.Name == "Low"
                            //               select row.Value).FirstOrDefault();

                            //    if (entry.Name == "Low")
                            //    {
                            //        dt.Rows.Add("High", entry.Value * high, entry.Value * medium, entry.Value * low);
                            //    }
                            //    if (entry.Name == "Medium")
                            //    {
                            //        dt.Rows.Add("Medium", entry.Value * high, entry.Value * medium, entry.Value * low);
                            //    }
                            //    if (entry.Name == "High")
                            //    {
                            //        dt.Rows.Add("Low", entry.Value * high, entry.Value * medium, entry.Value * low);
                            //    }
                            //});
                            #endregion
                        }


                        grdRiskRatingMatrix.DataSource = null;
                        grdRiskRatingMatrix.DataBind();

                        grdRiskRatingMatrix.DataSource = dt;
                        grdRiskRatingMatrix.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
        {
            DataTable dtUniqRecords = new DataTable();
            dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
            return dtUniqRecords;
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ProcessID = -1;
                int CustomerBranchID = -1;
                int TotalAVG = -1;
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    GridViewRow gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    Int32 rowind = gvRow.RowIndex;


                    Label lblProcessName = (Label)gvRow.FindControl("lblProcessName");

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            ProcessID = Convert.ToInt32(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            CustomerBranchID = Convert.ToInt32(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            TotalAVG = Convert.ToInt32(commandArgs[2]);
                        }
                        lblCustomername.Text = string.Empty;
                        lblCustomername.Text = GetCustomerBranchName(CustomerBranchID) + "/" + lblProcessName.Text.Trim();

                        lblRiskwisePlotring.Text = string.Empty;

                        lblRiskwisePlotring.Text = "Risk Ploting" + "/" + GetCustomerBranchName(CustomerBranchID) + "/" + lblProcessName.Text.Trim();
                        ViewState["ProcessID"] = ProcessID;
                        ViewState["BranchID"] = CustomerBranchID;
                        ViewState["TotalAVG"] = TotalAVG;
                        BindDataRatingDetails(CustomerBranchID, ProcessID);
                        BindMatrixData();
                        //UserAccordion1.SelectedIndex = 1;
                        BindMatrixProcessWiseData();
                        Label7.Text = "Risk Ploting" + "/" + GetCustomerBranchName(CustomerBranchID) + "/" + lblProcessName.Text.Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCompliances.PageIndex = e.NewPageIndex;
            int branchID = -1;
            int VerticalID = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }                      
            BindData(branchID);
        }
        protected void grdProcessRatingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }
        protected void grdProcessRatingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProcessRatingDetails.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["BranchID"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ProcessID"])))
                {
                    if (Convert.ToString(ViewState["BranchID"]) != "-1")
                    {
                        if (Convert.ToString(ViewState["ProcessID"]) != "-1")
                        {
                            BindDataRatingDetails(Convert.ToInt32(ViewState["BranchID"]), Convert.ToInt32(ViewState["ProcessID"]));
                        }
                    }
                }
            }
        }

        public static Sp_GetratingBeteewn_Result GetratingBeteewnDisplay(int rating, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Sp_GetratingBeteewn_Result auditdsplay = new Sp_GetratingBeteewn_Result();

                auditdsplay = entities.Sp_GetratingBeteewn(rating, Branchid).FirstOrDefault();

                return auditdsplay;
            }
        }
        public static List<Sp_GetratingBeteewnProcess_Result> GetratingBeteewnDisplayLIST(int lessrating, int highrating, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_GetratingBeteewnProcess_Result> auditdsplay = new List<Sp_GetratingBeteewnProcess_Result>();

                auditdsplay = entities.Sp_GetratingBeteewnProcess(lessrating, highrating, Branchid).ToList();

                return auditdsplay;
            }
        }

        public static List<ProcessRiskRatingView> GetProcessRiskRatingViewDisplay(int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ProcessRiskRatingView> auditdsplay = new List<ProcessRiskRatingView>();

                auditdsplay =
                             (from row in entities.ProcessRiskRatingViews
                              where row.CustomerBranchId == Branchid //&& row.ProcessId == ProcessId
                              //&& (row.Totalavg >= lessthan && row.Totalavg <= greaterthan) //(row.Totalavg == rating
                              select row).ToList();
                return auditdsplay;

                //  where a.Id == p.OID && (a.Start.Date >= startDate.Date && a.Start.Date <= endDate)
            }
        }

        public static List<ProcessRiskRating_PlotingView> GetProcessRiskRating_PlotingDisplay(int rating, int Branchid, int ProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ProcessRiskRating_PlotingView> auditdsplay = new List<ProcessRiskRating_PlotingView>();

                auditdsplay =
                             (from row in entities.ProcessRiskRating_PlotingView
                              where row.CustomerBranchId == Branchid && row.ProcessId == ProcessId
                              && row.Total == rating
                              select row).ToList();
                return auditdsplay;
            }
        }
        List<int> avalue = new List<int>();
        protected void grdRiskRatingMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvRow = e.Row;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["TotalAVG"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["BranchID"])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ProcessID"])))
                        {
                            if (Convert.ToString(ViewState["BranchID"]) != "-1")
                            {
                                if (Convert.ToString(ViewState["ProcessID"]) != "-1")
                                {
                                    Label lblControlRatingName = (e.Row.FindControl("lblControlRatingName") as Label);
                                    Label lblLOW = (e.Row.FindControl("lblLOW") as Label);
                                    Label lblMEDIUM = (e.Row.FindControl("lblMEDIUM") as Label);
                                    Label lblHIGH = (e.Row.FindControl("lblHIGH") as Label);

                                    //if (lblControlRatingName.Text.Trim().ToUpper()=="HIGH")
                                    //{
                                    e.Row.Cells[0].BackColor = System.Drawing.Color.White;
                                    e.Row.Cells[0].Font.Bold = true;
                                    e.Row.Cells[0].ForeColor = System.Drawing.Color.Black;
                                    //}
                                    //if (lblControlRatingName.Text.Trim().ToUpper() == "MEDIUM")
                                    //{
                                    //    e.Row.Cells[0].BackColor = System.Drawing.Color.Orange;
                                    //    e.Row.Cells[0].Font.Bold = true;
                                    //    e.Row.Cells[0].ForeColor = System.Drawing.Color.Black;
                                    //}
                                    //if (lblControlRatingName.Text.Trim().ToUpper() == "LOW")
                                    //{
                                    //    e.Row.Cells[0].BackColor = System.Drawing.Color.Lime;
                                    //    e.Row.Cells[0].Font.Bold = true;
                                    //    e.Row.Cells[0].ForeColor = System.Drawing.Color.Black;
                                    //}


                                    if (!string.IsNullOrEmpty(lblLOW.Text))
                                    {
                                        if (lblLOW.Text.Trim() != "LOW")
                                        {
                                            if (lblLOW.Text.Trim() != "HIGH")
                                            {
                                                if (lblLOW.Text.Trim() != "MEDIUM")
                                                {
                                                    var a = GetratingBeteewnDisplay(Convert.ToInt32(lblLOW.Text), Convert.ToInt32(ViewState["BranchID"]));
                                                    if (a != null)
                                                    {
                                                        if (a.Name.ToUpper() == "LOW")
                                                        {
                                                            e.Row.Cells[1].BackColor = System.Drawing.Color.LimeGreen;
                                                        }
                                                        if (a.Name.ToUpper() == "HIGH")
                                                        {
                                                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                                                        }
                                                        if (a.Name.ToUpper() == "MEDIUM")
                                                        {
                                                            e.Row.Cells[1].BackColor = System.Drawing.Color.Orange;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(lblMEDIUM.Text))
                                    {
                                        if (lblMEDIUM.Text.Trim() != "LOW")
                                        {
                                            if (lblMEDIUM.Text.Trim() != "HIGH")
                                            {
                                                if (lblMEDIUM.Text.Trim() != "MEDIUM")
                                                {
                                                    var a = GetratingBeteewnDisplay(Convert.ToInt32(lblMEDIUM.Text), Convert.ToInt32(ViewState["BranchID"]));
                                                    if (a != null)
                                                    {
                                                        if (a.Name.ToUpper() == "LOW")
                                                        {
                                                            e.Row.Cells[2].BackColor = System.Drawing.Color.LimeGreen;
                                                        }
                                                        if (a.Name.ToUpper() == "HIGH")
                                                        {
                                                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                                                        }
                                                        if (a.Name.ToUpper() == "MEDIUM")
                                                        {
                                                            e.Row.Cells[2].BackColor = System.Drawing.Color.Orange;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(lblHIGH.Text))
                                    {
                                        if (lblHIGH.Text.Trim() != "LOW")
                                        {
                                            if (lblHIGH.Text.Trim() != "HIGH")
                                            {
                                                if (lblHIGH.Text.Trim() != "MEDIUM")
                                                {
                                                    var a = GetratingBeteewnDisplay(Convert.ToInt32(lblHIGH.Text), Convert.ToInt32(ViewState["BranchID"]));
                                                    if (a != null)
                                                    {
                                                        if (a.Name.ToUpper() == "LOW")
                                                        {
                                                            e.Row.Cells[3].BackColor = System.Drawing.Color.LimeGreen;
                                                        }
                                                        if (a.Name.ToUpper() == "HIGH")
                                                        {
                                                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                                                        }
                                                        if (a.Name.ToUpper() == "MEDIUM")
                                                        {
                                                            e.Row.Cells[3].BackColor = System.Drawing.Color.Orange;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #region Previous Code
                                    //var a = GetratingBeteewnDisplay(Convert.ToInt32(ViewState["TotalAVG"]), Convert.ToInt32(ViewState["BranchID"]));
                                    //if (Convert.ToInt32(a.Lessthan) <= Convert.ToInt32(lblLOW.Text) && Convert.ToInt32(lblLOW.Text) <= Convert.ToInt32(a.Greaterthan))
                                    //{
                                    //    if (a.Name.ToUpper() == "LOW")
                                    //    {
                                    //        e.Row.Cells[1].BackColor = System.Drawing.Color.Lime;
                                    //    }
                                    //    if (a.Name.ToUpper() == "HIGH")
                                    //    {
                                    //        e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                                    //    }
                                    //    if (a.Name.ToUpper() == "MEDIUM")
                                    //    {
                                    //        e.Row.Cells[1].BackColor = System.Drawing.Color.Orange;
                                    //    }
                                    //}
                                    //if (Convert.ToInt32(a.Lessthan) <= Convert.ToInt32(lblMEDIUM.Text) && Convert.ToInt32(lblMEDIUM.Text) <= Convert.ToInt32(a.Greaterthan))
                                    //{
                                    //    if (a.Name.ToUpper() == "LOW")
                                    //    {
                                    //        e.Row.Cells[2].BackColor = System.Drawing.Color.Lime;
                                    //    }
                                    //    if (a.Name.ToUpper() == "HIGH")
                                    //    {
                                    //        e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                                    //    }
                                    //    if (a.Name.ToUpper() == "MEDIUM")
                                    //    {
                                    //        e.Row.Cells[2].BackColor = System.Drawing.Color.Orange;
                                    //    }
                                    //}
                                    //if (Convert.ToInt32(a.Lessthan) <= Convert.ToInt32(lblHIGH.Text) && Convert.ToInt32(lblHIGH.Text) <= Convert.ToInt32(a.Greaterthan))
                                    //{
                                    //if (a.Name.ToUpper() == "LOW")
                                    //{
                                    //    e.Row.Cells[3].BackColor = System.Drawing.Color.Lime;
                                    //}
                                    //else if (a.Name.ToUpper() == "HIGH")
                                    //{
                                    //    e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                                    //}
                                    //else if (a.Name.ToUpper() == "MEDIUM")
                                    //{
                                    //    e.Row.Cells[3].BackColor = System.Drawing.Color.Orange;
                                    //}
                                    //}
                                    #endregion
                                    List<ProcessRiskRating_PlotingView> valuea = new List<ProcessRiskRating_PlotingView>();
                                    if (!string.IsNullOrEmpty(lblLOW.Text))
                                    {
                                        if (lblLOW.Text.Trim() != "LOW")
                                        {
                                            if (lblLOW.Text.Trim() != "HIGH")
                                            {
                                                if (lblLOW.Text.Trim() != "MEDIUM")
                                                {
                                                    valuea = GetProcessRiskRating_PlotingDisplay(Convert.ToInt32(lblLOW.Text), Convert.ToInt32(ViewState["BranchID"]), Convert.ToInt32(ViewState["ProcessID"]));
                                                    if (valuea != null)
                                                    {
                                                        if (valuea.Count != 0)
                                                        {
                                                            bool checkfirst = avalue.Exists(element => element == Convert.ToInt32(lblLOW.Text));
                                                            if (checkfirst == false)
                                                            {
                                                                int getcount = 0;
                                                                valuea.ForEach(entry =>
                                                                    {
                                                                        if (getcount == 5)
                                                                        {
                                                                            e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                        }
                                                                        else if (getcount == 10)
                                                                        {
                                                                            e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                        }
                                                                        else if (getcount == 15)
                                                                        {
                                                                            e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                        }
                                                                        else
                                                                        {
                                                                            e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                        }
                                                                        //e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                        avalue.Add(Convert.ToInt32(entry.Total));
                                                                        getcount++;
                                                                    });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(lblMEDIUM.Text))
                                    {
                                        if (lblMEDIUM.Text.Trim() != "LOW")
                                        {
                                            if (lblMEDIUM.Text.Trim() != "HIGH")
                                            {
                                                if (lblMEDIUM.Text.Trim() != "MEDIUM")
                                                {
                                                    valuea = GetProcessRiskRating_PlotingDisplay(Convert.ToInt32(lblMEDIUM.Text), Convert.ToInt32(ViewState["BranchID"]), Convert.ToInt32(ViewState["ProcessID"]));
                                                    if (valuea != null)
                                                    {
                                                        if (valuea.Count != 0)
                                                        {
                                                            bool checkfirst = avalue.Exists(element => element == Convert.ToInt32(lblMEDIUM.Text));
                                                            if (checkfirst == false)
                                                            {
                                                                int getcount = 0;
                                                                valuea.ForEach(entry =>
                                                                {
                                                                    if (getcount == 5)
                                                                    {
                                                                        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                    }
                                                                    else if (getcount == 10)
                                                                    {
                                                                        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                    }
                                                                    else if (getcount == 15)
                                                                    {
                                                                        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                    }
                                                                    else
                                                                    {
                                                                        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                    }
                                                                    //e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));                                                       
                                                                    avalue.Add(Convert.ToInt32(entry.Total));
                                                                    getcount++;
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(lblHIGH.Text))
                                    {
                                        if (lblHIGH.Text.Trim() != "LOW")
                                        {
                                            if (lblHIGH.Text.Trim() != "HIGH")
                                            {
                                                if (lblHIGH.Text.Trim() != "MEDIUM")
                                                {
                                                    valuea = GetProcessRiskRating_PlotingDisplay(Convert.ToInt32(lblHIGH.Text), Convert.ToInt32(ViewState["BranchID"]), Convert.ToInt32(ViewState["ProcessID"]));
                                                    if (valuea != null)
                                                    {
                                                        if (valuea.Count != 0)
                                                        {
                                                            bool checkfirst = avalue.Exists(element => element == Convert.ToInt32(lblHIGH.Text));
                                                            if (checkfirst == false)
                                                            {
                                                                int getcount = 0;
                                                                valuea.ForEach(entry =>
                                                                {
                                                                    if (getcount == 5)
                                                                    {
                                                                        e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                    }
                                                                    else if (getcount == 10)
                                                                    {
                                                                        e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                    }
                                                                    else if (getcount == 15)
                                                                    {
                                                                        e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                    }
                                                                    else
                                                                    {
                                                                        e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                    }
                                                                    avalue.Add(Convert.ToInt32(entry.Total));
                                                                    getcount++;
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void grdRiskRatingMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }
        public string GetUserName(int userid)
        {
            string processnonprocess = "";
            if (userid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(userid);
                    mst_User mstuser = (from row in entities.mst_User
                                        where row.ID == userid && row.IsDeleted == false && row.IsActive == true
                                        select row).FirstOrDefault();

                    processnonprocess = mstuser.FirstName + " " + mstuser.LastName;
                }
            }
            return processnonprocess;
        }
        protected void btnExportDoc_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        List<int> Newvalue = new List<int>();
        protected void grdRiskRatingMatrixProcess_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvRow = e.Row;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(GetBranchID())))
                {
                    if (Convert.ToString(GetBranchID()) != "-1")
                    {
                        Label lblControlRatingName = (e.Row.FindControl("lblControlRatingName") as Label);
                        Label lblLOW = (e.Row.FindControl("lblLOW") as Label);
                        Label lblMEDIUM = (e.Row.FindControl("lblMEDIUM") as Label);
                        Label lblHIGH = (e.Row.FindControl("lblHIGH") as Label);

                        #region Matrxi Color code
                        e.Row.Cells[0].BackColor = System.Drawing.Color.White;
                        e.Row.Cells[0].Font.Bold = true;
                        e.Row.Cells[0].ForeColor = System.Drawing.Color.Black;
                        if (!string.IsNullOrEmpty(lblLOW.Text))
                        {
                            if (lblLOW.Text.Trim() != "LOW")
                            {
                                if (lblLOW.Text.Trim() != "HIGH")
                                {
                                    if (lblLOW.Text.Trim() != "MEDIUM")
                                    {
                                        var a = GetratingBeteewnDisplay(Convert.ToInt32(lblLOW.Text), Convert.ToInt32(GetBranchID()));
                                        if (a != null)
                                        {
                                            if (a.Name.ToUpper() == "LOW")
                                            {
                                                e.Row.Cells[1].BackColor = System.Drawing.Color.LimeGreen;
                                            }
                                            if (a.Name.ToUpper() == "HIGH")
                                            {
                                                e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                                            }
                                            if (a.Name.ToUpper() == "MEDIUM")
                                            {
                                                e.Row.Cells[1].BackColor = System.Drawing.Color.Orange;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(lblMEDIUM.Text))
                        {
                            if (lblMEDIUM.Text.Trim() != "LOW")
                            {
                                if (lblMEDIUM.Text.Trim() != "HIGH")
                                {
                                    if (lblMEDIUM.Text.Trim() != "MEDIUM")
                                    {
                                        var a = GetratingBeteewnDisplay(Convert.ToInt32(lblMEDIUM.Text), Convert.ToInt32(GetBranchID()));
                                        if (a != null)
                                        {
                                            if (a.Name.ToUpper() == "LOW")
                                            {
                                                e.Row.Cells[2].BackColor = System.Drawing.Color.LimeGreen;
                                            }
                                            if (a.Name.ToUpper() == "HIGH")
                                            {
                                                e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                                            }
                                            if (a.Name.ToUpper() == "MEDIUM")
                                            {
                                                e.Row.Cells[2].BackColor = System.Drawing.Color.Orange;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(lblHIGH.Text))
                        {
                            if (lblHIGH.Text.Trim() != "LOW")
                            {
                                if (lblHIGH.Text.Trim() != "HIGH")
                                {
                                    if (lblHIGH.Text.Trim() != "MEDIUM")
                                    {
                                        var a = GetratingBeteewnDisplay(Convert.ToInt32(lblHIGH.Text), Convert.ToInt32(GetBranchID()));
                                        if (a != null)
                                        {
                                            if (a.Name.ToUpper() == "LOW")
                                            {
                                                e.Row.Cells[3].BackColor = System.Drawing.Color.LimeGreen;
                                            }
                                            if (a.Name.ToUpper() == "HIGH")
                                            {
                                                e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                                            }
                                            if (a.Name.ToUpper() == "MEDIUM")
                                            {
                                                e.Row.Cells[3].BackColor = System.Drawing.Color.Orange;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        List<ProcessRiskRatingView> valueb = new List<ProcessRiskRatingView>();
                        List<Sp_GetratingBeteewnProcess_Result> valuea = new List<Sp_GetratingBeteewnProcess_Result>();
                        if (!string.IsNullOrEmpty(lblLOW.Text))
                        {
                            if (lblLOW.Text.Trim() != "LOW")
                            {
                                if (lblLOW.Text.Trim() != "HIGH")
                                {
                                    if (lblLOW.Text.Trim() != "MEDIUM")
                                    {
                                        var getratingbetweenvalue = GetratingBeteewnDisplay(Convert.ToInt32(lblLOW.Text), Convert.ToInt32(GetBranchID()));
                                        if (getratingbetweenvalue != null)
                                        {
                                            valuea = GetratingBeteewnDisplayLIST(Convert.ToInt32(getratingbetweenvalue.Lessthan), Convert.ToInt32(getratingbetweenvalue.Greaterthan), Convert.ToInt32(GetBranchID()));
                                            if (valuea != null)
                                            {
                                                if (valuea.Count != 0)
                                                {
                                                    bool checkfirst = Newvalue.Exists(element => element == Convert.ToInt32(lblLOW.Text));
                                                    if (checkfirst == false)
                                                    {
                                                        int getcount = 0;
                                                        valuea.ForEach(entry =>
                                                        {
                                                            // if (Convert.ToInt32(entry.Totalavg) <= Convert.ToInt32(lblLOW.Text) && Convert.ToInt32(lblLOW.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan))                                                         
                                                            if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblLOW.Text))
                                                            {
                                                                if (getcount == 5)
                                                                {
                                                                    e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else if (getcount == 10)
                                                                {
                                                                    e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else if (getcount == 15)
                                                                {
                                                                    e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else
                                                                {
                                                                    e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                }
                                                                Newvalue.Add(Convert.ToInt32(lblLOW.Text));
                                                                Newvalue.Add(Convert.ToInt32(4));
                                                                Newvalue.Add(Convert.ToInt32(entry.Totalavg));
                                                                getcount++;
                                                            }
                                                            //else if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblLOW.Text) && Convert.ToInt32(lblLOW.Text) <= Convert.ToInt32(entry.Totalavg))                                                         
                                                            //{

                                                            //    //var getratingbetweenvalue1 = GetratingBeteewnDisplay(Convert.ToInt32(entry.Totalavg), Convert.ToInt32(ddlFilterLocation.SelectedValue));

                                                            //     if (getcount == 5)
                                                            //    {
                                                            //        e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 10)
                                                            //    {
                                                            //        e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 15)
                                                            //    {
                                                            //        e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                            //    }
                                                            //    avalue.Add(Convert.ToInt32(lblLOW.Text));
                                                            //    getcount++;
                                                            //}
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(lblMEDIUM.Text))
                        {
                            if (lblMEDIUM.Text.Trim() != "LOW")
                            {
                                if (lblMEDIUM.Text.Trim() != "HIGH")
                                {
                                    if (lblMEDIUM.Text.Trim() != "MEDIUM")
                                    {
                                        var getratingbetweenvalue = GetratingBeteewnDisplay(Convert.ToInt32(lblMEDIUM.Text), Convert.ToInt32(GetBranchID()));
                                        if (getratingbetweenvalue != null)
                                        {
                                            valuea = GetratingBeteewnDisplayLIST(Convert.ToInt32(getratingbetweenvalue.Lessthan), Convert.ToInt32(getratingbetweenvalue.Greaterthan), Convert.ToInt32(GetBranchID()));
                                            if (valuea != null)
                                            {
                                                if (valuea.Count != 0)
                                                {
                                                    bool checkfirst = Newvalue.Exists(element => element == Convert.ToInt32(lblMEDIUM.Text));
                                                    if (checkfirst == false)
                                                    {

                                                        int getcount = 0;
                                                        valuea.ForEach(entry =>
                                                        {
                                                            // if (Convert.ToInt32(entry.Totalavg) <= Convert.ToInt32(lblMEDIUM.Text) && Convert.ToInt32(lblMEDIUM.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan))                                                           
                                                            if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblMEDIUM.Text))
                                                            //if (Convert.ToInt32(entry.Totalavg) <= Convert.ToInt32(lblMEDIUM.Text) && Convert.ToInt32(lblMEDIUM.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan))                                                            
                                                            {
                                                                if (getcount == 5)
                                                                {
                                                                    e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else if (getcount == 10)
                                                                {
                                                                    e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else if (getcount == 15)
                                                                {
                                                                    e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else
                                                                {
                                                                    e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                }
                                                                Newvalue.Add(Convert.ToInt32(lblMEDIUM.Text));
                                                                Newvalue.Add(Convert.ToInt32(entry.Totalavg));
                                                                getcount++;
                                                            }
                                                            //else if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblMEDIUM.Text))
                                                            //{
                                                            //    if (getcount == 5)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 10)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 15)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                            //    }
                                                            //    Newvalue.Add(Convert.ToInt32(lblMEDIUM.Text));
                                                            //    //avalue.Add(Convert.ToInt32(entry.Totalavg));
                                                            //    getcount++;
                                                            //}  
                                                            //else if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblMEDIUM.Text) && Convert.ToInt32(lblMEDIUM.Text) <= Convert.ToInt32(entry.Totalavg)) 
                                                            //{                                                              
                                                            //    if (getcount == 5)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 10)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 15)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        e.Row.Cells[1].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                            //    }
                                                            //    avalue.Add(Convert.ToInt32(lblMEDIUM.Text));
                                                            //    getcount++;
                                                            //}                                                            
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(lblHIGH.Text))
                        {
                            if (lblHIGH.Text.Trim() != "LOW")
                            {
                                if (lblHIGH.Text.Trim() != "HIGH")
                                {
                                    if (lblHIGH.Text.Trim() != "MEDIUM")
                                    {
                                        var getratingbetweenvalue = GetratingBeteewnDisplay(Convert.ToInt32(lblHIGH.Text), Convert.ToInt32(GetBranchID()));
                                        if (getratingbetweenvalue != null)
                                        {
                                            valuea = GetratingBeteewnDisplayLIST(Convert.ToInt32(getratingbetweenvalue.Lessthan), Convert.ToInt32(getratingbetweenvalue.Greaterthan), Convert.ToInt32(GetBranchID()));
                                            if (valuea != null)
                                            {
                                                if (valuea.Count != 0)
                                                {
                                                    bool checkfirst = Newvalue.Exists(element => element == Convert.ToInt32(lblHIGH.Text));
                                                    if (checkfirst == false)
                                                    {
                                                        int getcount = 0;
                                                        valuea.ForEach(entry =>
                                                        {
                                                            //if (Convert.ToInt32(entry.Totalavg) <= Convert.ToInt32(lblHIGH.Text) && Convert.ToInt32(lblHIGH.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan))                                                            
                                                            //if (Convert.ToInt32(entry.Totalavg) <= Convert.ToInt32(lblHIGH.Text) && Convert.ToInt32(lblHIGH.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan))                                                            
                                                            if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblHIGH.Text))
                                                            {
                                                                if (getcount == 5)
                                                                {
                                                                    e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else if (getcount == 10)
                                                                {
                                                                    e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else if (getcount == 15)
                                                                {
                                                                    e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                                }
                                                                else
                                                                {
                                                                    e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                                }
                                                                Newvalue.Add(Convert.ToInt32(lblHIGH.Text));
                                                                Newvalue.Add(Convert.ToInt32(entry.Totalavg));
                                                                getcount++;
                                                            }
                                                            //else if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(lblHIGH.Text))
                                                            //{
                                                            //    if (getcount == 5)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 10)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else if (getcount == 15)
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        e.Row.Cells[2].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                            //    }
                                                            //    avalue.Add(Convert.ToInt32(lblHIGH.Text));
                                                            //    //avalue.Add(Convert.ToInt32(entry.Totalavg));
                                                            //    getcount++;
                                                            //}
                                                            // //else if (Convert.ToInt32(entry.Totalavg) >= Convert.ToInt32(getratingbetweenvalue.Greaterthan))
                                                            // //else if (Convert.ToInt32(getratingbetweenvalue.Greaterthan) >= Convert.ToInt32(entry.Totalavg))
                                                            ////else  if (Convert.ToInt32(lblMEDIUM.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan) && Convert.ToInt32(lblMEDIUM.Text) >= Convert.ToInt32(getratingbetweenvalue.Greaterthan))   
                                                            // else if (Convert.ToInt32(getratingbetweenvalue.Lessthan) >= Convert.ToInt32(lblHIGH.Text) && Convert.ToInt32(lblHIGH.Text) <= Convert.ToInt32(getratingbetweenvalue.Greaterthan))                                                         
                                                            // {
                                                            //     if (getcount == 5)
                                                            //     {
                                                            //         e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //     }
                                                            //     else if (getcount == 10)
                                                            //     {
                                                            //         e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //     }
                                                            //     else if (getcount == 15)
                                                            //     {
                                                            //         e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'><br>&nbsp;&nbsp;&nbsp;&#9734;</font>"));
                                                            //     }
                                                            //     else
                                                            //     {
                                                            //         e.Row.Cells[3].Controls.Add(new LiteralControl("<font style='color : white;'>&#9734;</font>"));
                                                            //     }
                                                            //     avalue.Add(Convert.ToInt32(lblHIGH.Text));
                                                            //     getcount++;
                                                            // }
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        protected void grdRiskRatingMatrixProcess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void liPerformer_Click(object sender, EventArgs e)
        {
            try
            {
                DivProcess.Visible = true;
                DivRisk.Visible = false;

                liPerformer.Attributes.Add("class", "active");
                liReviewer.Attributes.Add("class", "");

                DivProcess.Attributes.Remove("class");
                DivProcess.Attributes.Add("class", "tab-pane active");

                DivRisk.Attributes.Remove("class");
                DivRisk.Attributes.Add("class", "tab-pane");
            }
            catch
            {

            }
        }

        protected void liReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                DivProcess.Visible = false;
                DivRisk.Visible = true;

                liPerformer.Attributes.Add("class", "");
                liReviewer.Attributes.Add("class", "active");

                DivRisk.Attributes.Remove("class");
                DivRisk.Attributes.Add("class", "tab-pane active");

                DivProcess.Attributes.Remove("class");
                DivProcess.Attributes.Add("class", "tab-pane");
                //BindProcess();
            }
            catch
            {

            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdProcessRatingDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //SelectedPageNo.Text = "1";
            //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

            //if (currentPageNo <= GetTotalPagesCount())
            //{
            //    SelectedPageNo.Text = (currentPageNo).ToString();
            //    grdProcessRatingDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //    grdProcessRatingDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            //}
            //else
            //{

            //}
            //Reload the Grid
            //BindProcess();
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["BranchID"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ProcessID"])))
                {
                    if (Convert.ToString(ViewState["BranchID"]) != "-1")
                    {
                        if (Convert.ToString(ViewState["ProcessID"]) != "-1")
                        {
                            BindDataRatingDetails(Convert.ToInt32(ViewState["BranchID"]), Convert.ToInt32(ViewState["ProcessID"]));
                            bindPageNumber1();
                            int count = Convert.ToInt32(GetTotalPagesCount());
                            if (count > 0)
                            {
                                int gridindex = grdProcessRatingDetails.PageIndex;
                                string chkcindition = (gridindex + 1).ToString();
                                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                            }
                        }
                    }
                }
            }
            //GetPageDisplaySummary();
            
        }


        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void BindProcess()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var ProcessList = ProcessManagement.GetAllProcess(customerID);

                grdProcessRatingDetails.DataSource = ProcessList;
                Session["TotalRows"] = ProcessList.Count;
                grdProcessRatingDetails.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        

        protected void ddlPageSize1_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.PageSize = Convert.ToInt32(ddlPageSize1.SelectedValue);            
            //Reload the Grid
            if (Convert.ToString(GetBranchID()) != "-1")
            {                
                BindData(Convert.ToInt32(GetBranchID()));
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount1());
                if (count > 0)
                {
                    int gridindex = grdCompliances.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }           
        }        
        private int GetTotalPagesCount1()
        {
            try
            {
                TotalRows1.Value = Session["TotalRows1"].ToString();

                int totalPages = Convert.ToInt32(TotalRows1.Value) / Convert.ToInt32(ddlPageSize1.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows1.Value) % Convert.ToInt32(ddlPageSize1.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                lblCustomername.Text = string.Empty;
                lblRiskwisePlotring.Text = string.Empty;
                Label4.Visible = false;
                Label6.Visible = false;
                dvshowpagesize.Visible = true;
                dvShowPageNumber.Visible = true;
                lblProcesswisePlotring.Visible = true;
                Label10.Visible = true;
                Label3.Visible = true;
                BindData(Convert.ToInt32(GetBranchID()));
                BindMatrixProcessWiseData();
                    
        }

    }
}