﻿<%@ Page Title="Process RiskRating Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ProcessRiskRatingReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.ProcessRiskRatingReport" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .dispnone {
            display: none;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .accordionContent1 {
            background-color: #D3DEEF;
            border-color: -moz-use-text-color #2F4F4F #2F4F4F;
            border-right: 1px dashed #2F4F4F;
            border-style: none dashed dashed;
            border-width: medium 1px 1px;
            padding: 10px 5px 5px;
            width: 92%;
        }

        .accordionHeaderSelected1 {
            background-color: #5078B3;
            border: 0px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .accordionHeader1 {
            background-color: #2E4D7B;
            border: 0px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .href1 {
            color: White;
            font-weight: bold;
            text-decoration: none;
        }

        /*.textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }*/
    </style>

    <style type="text/css">
        .clsVertBT {
            writing-mode: tb-rl;
            /*-webkit-transform:rotate(90deg);
   -moz-transform:rotate(90deg);
   -o-transform: rotate(90deg);*/
            white-space: nowrap;
            display: block;
            bottom: 0;
            /*width:20px;
   height:20px;*/
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        function setTabActive(id) {
            
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };
        $(document).ready(function () {
            //setactivemenu('Rating Report');
            fhead('Rating Report');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <section class="panel">      
                    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                    ValidationGroup="ComplianceValidationGroup1" />
                                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lblCustomername" runat="server" Text="" Visible="false"></asp:Label>
                            </div> 
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="clearfix"></div>
                    <div class="panel-body">
                       <div class="col-md-12 colpadding0">     
                            <div runat="server" id="performerdocuments"  class="tab-pane active">
                              <div class="clearfix"></div>
                                <header class="panel-heading tab-bg-primary ">
                                              <ul id="rblRole1" class="nav nav-tabs">
                                                       <li class="dummyval">                                                     
                                                            <asp:LinkButton ID="liPerformer" OnClick="liPerformer_Click"  runat="server">Process Rating</asp:LinkButton>
                                                        </li>
                                                      <li class="dummyval">                                                      
                                                               <asp:LinkButton ID="liReviewer" OnClick="liReviewer_Click" runat="server">Risk Rating Details</asp:LinkButton>
                                                        </li>                           
                                            </ul>
                                    </header>
                             </div>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <div class="tab-content ">
                                <div runat="server" id="DivProcess"  class="tab-pane active">
                                    <asp:Panel ID="LoanEntry" runat="server">       
                                          <Header><a href="#" class="href1">Process Rating </a></Header>                          
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td colspan="4" style="vertical-align: top; width: 50%;">
                                                        <div style="margin-top: 10px">
                                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                                <ContentTemplate>
                                                                 
                                                                    <div class="col-md-12 colpadding0">  
                                                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;" id="dvshowpagesize" runat="server">
                                                                             <div class="col-md-2 colpadding0">
                                                                                <p style="color: #999; margin-top: 5px;">Show </p>
                                                                            </div>
                                                                            <asp:DropDownList runat="server" ID="ddlPageSize1" class="form-control m-bot15" Style="width: 70px;float: left;"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize1_SelectedIndexChanged" >                        
                                                                            <asp:ListItem Text="5" Selected="True" />
                                                                            <asp:ListItem Text="10" />
                                                                            <asp:ListItem Text="20" />
                                                                            <asp:ListItem Text="50" />
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                                                                <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" 
                                                                                     AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" Width="95%" Height="32px"
                                                                                 DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                                                                </asp:DropDownListChosen>                                
                                                                            </div>
                                                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                                                                <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px"
                                                                                 AllowSingleDeselect="false" DisableSearchThreshold="3" DataPlaceHolder="Sub Unit 1" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                                                                </asp:DropDownListChosen>
                                                                            </div>
                                                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                                                                <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px"
                                                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit 2" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                                                                </asp:DropDownListChosen>
                                                                            </div>    
                                                                     </div>
                                                                     <div class="clearfix"></div>
                                                                    <div class="col-md-12 colpadding0"> 
                                                                          <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                                                                 <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px"
                                                                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Unit 3" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                                                                </asp:DropDownListChosen>
                                                                             </div>
                                                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                                                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px"
                                                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged"  DataPlaceHolder="Sub Unit 4" >
                                                                                </asp:DropDownListChosen>                                  
                                                                            </div>
                                                                         <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                                                         <%{%>
                                                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">                                                                                
                                                                                 <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" class="form-control m-bot15" Width="95%" Height="32px"
                                                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged" 
                                                                                      DataPlaceHolder="Select Vertical" >
                                                                                </asp:DropDownListChosen> 
                                                                            </div>   
                                                                          <%}%>                             
                                                                    </div>                                                                                                                                                                                                                                                                                                                    
                                                                    <div class="clearfix"></div>
                                                                    <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="none" PageSize="5" AllowPaging="true" AutoPostBack="true" 
                                                                        CssClass="table" Width="100%" DataKeyNames="ProcessId" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                                                                        <Columns>
                                                                         <asp:TemplateField  HeaderText="Sr">
                                                                        <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Process"  SortExpression="Name">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; font-size: 15px; font-family: Calibri;">
                                                                                        <asp:Label runat="server" ID="lblProcessName" Text='<%# Eval("Name") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Vertical"  SortExpression="Name">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; font-size: 15px; font-family: Calibri;">
                                                                                        <asp:Label runat="server" ID="lblVerticalName" Text='<%# Eval("VerticalName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Risk Total">
                                                                                <ItemTemplate>
                                                                                   <div style="overflow: hidden; text-overflow: ellipsis; font-size: 15px; font-family: Calibri;">
                                                                                        <asp:Label runat="server" ID="lblRisk" Text='<%# Eval("RCM") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Rating Total">
                                                                                <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; font-size: 15px; font-family: Calibri;">
                                                                                        <asp:Label runat="server" ID="lblTotal" Text='<%# Eval("Total") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Average">
                                                                                <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; font-size: 15px; font-family: Calibri;">
                                                                                        <asp:Label runat="server" ID="lblAverage" Text='<%# Eval("Totalavg") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Rating">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; font-size: 15px; font-family: Calibri;">
                                                                                        <asp:Label runat="server" ID="lblAverage1"  Text='<%# GetRatingName((int)Eval("Rating")) %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField ItemStyle-Width="5%"  HeaderText="Action">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ProcessId")+"," + Eval("CustomerBranchId")+"," + Eval("Totalavg") %>' CausesValidation="false">
                                                                                        <img src="../../Images/edit_icon_new.png" data-toggle="tooltip" data-placement="top" alt="Display Rating" title="Display Rating" /></asp:LinkButton>
                                                                                </ItemTemplate>                                                                    
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                         <PagerTemplate>                                                        
                                                                        </PagerTemplate>
                                                                        <EmptyDataTemplate>
                                                                            No Record Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                             <div style="float: right;">
                                                                              <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                                                                  class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                                                              </asp:DropDownListChosen>  
                                                                            </div>
                                                                            <div id="dvShowPageNumber" runat="server">
                                                                             <div class="col-md-5 colpadding0">
                                                                                <div class="table-Selecteddownload">
                                                                                    <div class="table-Selecteddownload-text">
                                                                                        <p><asp:Label runat="server" ID="lblTotalSelected1" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                                                                    </div>                                   
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 colpadding0" style="float:right">
                                                                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                                                                    <div class="table-paging-text" style="float:right">
                                                                                        <p>Page
                                           
                                                                                        </p>
                                                                                    </div>                                    
                                                                                    <asp:HiddenField ID="TotalRows1" runat="server" Value="0" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <div style="float: left; margin-left: 10%;">                                               
                                                        </div>
                                                    </td>
                                                    <td colspan="2" align="center">                                           
                                                    </td>
                                                </tr>
                                                <tr>                                                                        
                                                    <td colspan="2" align="center">                                           
                                                         <div style="margin-top: 10px; float:left;">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                                <ContentTemplate>  
                                                                     <table><tr>
                                                                    <td><div style="float: left; margin-left: 14%;">
                                                                   <asp:Label ID="lblProcesswisePlotring" runat="server" Font-Bold="true"  Text="Process Ploting"></asp:Label>
                                                  
                                                                    </div></td>
                                                                        </tr>
                                                                    <tr><td>
                                                                        <div class="clsVertBT" style="float:left; margin-left:10%;">
                                                                            <asp:Label ID="Label10" runat="server" Font-Bold="true"     Text="<---------------- Control Rating ---------------->"></asp:Label>
                                                                    </div>                                                     
                                                                    <asp:GridView runat="server" ID="grdRiskRatingMatrixProcess" AutoGenerateColumns="false"  OnRowDataBound="grdRiskRatingMatrixProcess_RowDataBound"
                                                                        OnPageIndexChanging="grdRiskRatingMatrixProcess_PageIndexChanging">
                                                                        <Columns>
                                                                             <asp:TemplateField HeaderText="" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left"  HeaderStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblControlRatingName" Font-Bold="true" Text='<%# Eval("A") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                             <asp:TemplateField HeaderText="LOW" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left"  HeaderStyle-BackColor="White" HeaderStyle-HorizontalAlign="Center">                                                                     
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblLOW" Font-Bold="true" Text='<%# Eval("Low") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                             <asp:TemplateField HeaderText="MEDIUM" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="White"  HeaderStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblMEDIUM" Font-Bold="true" Text='<%# Eval("Medium") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                             <asp:TemplateField HeaderText="HIGH" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="White" HeaderStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblHIGH" Font-Bold="true" Text='<%# Eval("High") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>                                                               
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                        <PagerSettings Position="Top" />
                                                                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#603311" />
                                                                        <HeaderStyle  ForeColor="Black"  CssClass="dispnone"   ></HeaderStyle> <%-- --%>
                                                                        <AlternatingRowStyle BackColor="White" />
                                                            
                                                                    </asp:GridView>
                                                                        </div> 
                                                                         </td>  
                                                                </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div style="float: left; margin-left: 10%;">
                                                                          <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="<---------------- Risk Rating ---------------->"></asp:Label>
                                                               </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>   
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>                                        
                                                    </td>
                                                    <td colspan="2" align="center">
                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                            <ContentTemplate>  
                                                                <table><tr>
                                                                    <td><div style="float: left; margin-left: 14%;">
                                                                        <asp:Label ID="lblRiskwisePlotring" Font-Bold="true"  runat="server" Text=""></asp:Label>
                                                                        <asp:Label ID="Label7" runat="server" Font-Bold="true" style="display:none" Text=""></asp:Label>
                                                                    </div></td>
                                                                        </tr>
                                                                    <tr><td>
                                                                        <div class="clsVertBT" style="float:left;">
                                                                            <asp:Label ID="Label4" runat="server" Font-Bold="true"   Visible="false" Text="<---------------- Control Rating ---------------->"></asp:Label>
                                                                    </div>
                                                           
                                                    
                                                        
                                                                    <div style="margin-top: 10px; float:left; ">
                                                                                                   
                                                                            <asp:GridView runat="server" ID="grdRiskRatingMatrix" AutoGenerateColumns="false" OnRowDataBound="grdRiskRatingMatrix_RowDataBound"
                                                                                OnPageIndexChanging="grdRiskRatingMatrix_PageIndexChanging">
                                                                                <Columns>
                                                                                        <asp:TemplateField HeaderText="" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left"  HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="lblControlRatingName" Font-Bold="true" Text='<%# Eval("A") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="LOW" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left"  HeaderStyle-BackColor="White" HeaderStyle-HorizontalAlign="Center">                                                                     
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="lblLOW" Font-Bold="true" Text='<%# Eval("Low") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="MEDIUM" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="White"  HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="lblMEDIUM" Font-Bold="true" Text='<%# Eval("Medium") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="HIGH" ItemStyle-Width="80px" ItemStyle-Height="60px" HeaderStyle-Height="50px" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="White" HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="lblHIGH" Font-Bold="true" Text='<%# Eval("High") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>                                                               
                                                                                </Columns>
                                                                                <FooterStyle BackColor="#CCCC99" />
                                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                                <PagerSettings Position="Top" />
                                                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#603311" />
                                                                                <HeaderStyle  ForeColor="Black"  CssClass="dispnone"  ></HeaderStyle> <%-- --%>
                                                                                <AlternatingRowStyle BackColor="White" />
                                                                   
                                                        
                                                                    </asp:GridView>
               
                                                   
                                                                      </div> 
                                                                         </td>  
                                                                </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div style="float: left; margin-left: 10%;">
                                                                                <asp:Label ID="Label6" runat="server" Font-Bold="true" Visible="false" Text="<---------------- Risk Rating ---------------->"></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>   
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <div style="float: left; margin-left: 10%;">
                                                         </div>
                                                    </td>                                         
                                                </tr>                                         
                                            </table>
                                    </asp:Panel>
                                 </div>
                                <div runat="server" id="DivRisk" class="tab-pane">  
                                    <Header><a href="#" class="href1">Risk Rating Details</a> </Header>
                                        <Content>
                                            <asp:Panel ID="Panel3" runat="server">            
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-md-2 colpadding0 entrycount" style="margin-top:5px;">
                                                        <div class="col-md-3 colpadding0" >
                    <p style="color: #999; margin-top: 5px;">Show </p>
                </div>
                                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: right"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                            <asp:ListItem Text="5" Selected="True" />
                            <asp:ListItem Text="10" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>                                        
                                                        </div>
                                                        <section class="panel">    
                                                                <div style="margin-top: 10px;">
                                                                    <asp:GridView runat="server" ID="grdProcessRatingDetails" AutoGenerateColumns="false" CssClass="table" GridLines="none"
                                                                        AllowPaging="True" PageSize="5" Width="100%" DataKeyNames="ProcessId" OnRowCommand="grdProcessRatingDetails_RowCommand"
                                                                        OnPageIndexChanging="grdProcessRatingDetails_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                            <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                             </ItemTemplate>
                                                                             </asp:TemplateField>   
                                                                            <asp:TemplateField HeaderText="ControlNo">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                        <asp:Label runat="server" ID="lblRISK11" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Risk">
                                                                                <ItemTemplate>
                                                                                    <div class="text_NlinesusingCSS" style="width: 200px;"> 
                                                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="top" ID="lblRISK" ToolTip='<%# Eval("RISK") %>' Text='<%# Eval("RISK") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Risk Rating">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                        <asp:Label runat="server" ID="lblRiskRatingName" Text='<%# Eval("RiskRatingName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Control Rating">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                        <asp:Label runat="server" ID="lblControlRatingName" Text='<%# Eval("ControlRatingName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Print">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                        <asp:Label runat="server" ID="lblControlRatingName" Text='<%# Eval("Printploted") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                    
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid"   />
                                                                        <HeaderStyle CssClass="clsheadergrid"    />
                                                                        <PagerTemplate>
                      
                                            </PagerTemplate>
                                                                        <EmptyDataTemplate>
                                                                            No Record Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                    <div style="float: right;">
                                                              <asp:DropDownListChosen runat="server" ID="DropDownListPageNo1" AutoPostBack="true"
                                                                 AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo1_SelectedIndexChanged">                                   
                                                              </asp:DropDownListChosen>  
                                                            </div>                            
                                                                    <div class="col-md-5 colpadding0">
                                                                <div class="table-Selecteddownload">
                                                                    <div class="table-Selecteddownload-text">
                                                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                                                    </div>                                   
                                                                </div>
                                                            </div>
                                                                    <div class="col-md-6 colpadding0" style="float:right">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                    
                                        <div class="table-paging-text" style="float:right">
                                            <p>Page
                                          
                                            </p>
                                        </div>                                    
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                                
                                                        </section>   
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </Content>  
                                </div>
                            </div>
                        </div>
                    </div>
                  </section>    
            </div>            
        </div>
    </div>
</asp:Content>
