﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
//using Spire.Presentation.Drawing.Animation;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class MISReportNew : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {                
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();

                ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                //ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }   
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        
        private string ToRoman(int number)
        {

            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            //throw new ArgumentOutOfRangeException("something bad happened");
            throw new NotImplementedException();
        }


        protected void lbtnExportExcelTest_Click(object sender, EventArgs e)
        {

            #region Sheet 1
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                string chk = txtMonthDate.Text;
                int Year = Convert.ToInt32(chk.Substring(chk.Length - 4));
                string YourString = chk.Remove(chk.Length - 5);
                int monthnumber = DateTime.ParseExact(YourString, "MMMM", CultureInfo.InvariantCulture).Month;
                int days = DateTime.DaysInMonth(Year, monthnumber);
                DateTime startDatePeroid = new DateTime(Year, monthnumber, 1);
                DateTime EndDatePeroid = new DateTime(Year, monthnumber, days);

                if (monthnumber > 1)
                {
                    monthnumber = monthnumber - 1;
                    days = DateTime.DaysInMonth(Year, monthnumber);
                }
                else
                {
                    monthnumber = 12;
                    Year = Year - 1;
                    days = DateTime.DaysInMonth(Year, monthnumber);
                }

                DateTime PendingDate = new DateTime(Year, monthnumber, days);

                try
                {
                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Major Issue-Audit");
                    ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Pending Issue-Audit");

                    String FileName = String.Empty;
                    DataTable ExcelData = null;
                    DataTable ExcelDataPending = null;
                    ApplyFilter = false;
                    int userID = -1;
                    int RoleID = -1;

                    RoleID = -1;
                    if (PerformerFlag)
                        RoleID = 3;
                    if (ReviewerFlag)
                        RoleID = 4;
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int CustBranchID = -1;
                    int VerticalID = -1;
                    int ProcessID = -1;
                    string FinancialYear = string.Empty;
                    string Period = string.Empty;
                    string PeriodValueYear = string.Empty;
                    string Year1 = string.Empty;
                    string Year2 = string.Empty;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        if (ddlFinancialYear.SelectedValue != "-1")
                        {
                            FinancialYear = ddlFinancialYear.SelectedItem.Text;
                            if (!string.IsNullOrEmpty(FinancialYear))
                            {
                                Year1 = FinancialYear.Substring(0, FinancialYear.IndexOf("-")).Trim();
                                Year2 = FinancialYear.Substring(5, FinancialYear.IndexOf("-")).Trim();
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlPeriod.SelectedValue != "-1")
                        {
                            Period = ddlPeriod.SelectedItem.Text;
                            if (Period.Equals("Jan-Mar") || Period.Equals("Jan") || Period.Equals("Feb") || Period.Equals("Mar"))
                            {
                                PeriodValueYear = Year2;
                            }
                            else if (Period.Equals("Oct-Mar"))
                            {
                                PeriodValueYear = FinancialYear;
                            }
                            else if (Period.Equals("Apr-Sep") || Period.Equals("Apr-Jun") || Period.Equals("Jul-Sep") || Period.Equals("Oct-Dec") || Period.Equals("Apr") || Period.Equals("May") || Period.Equals("Jun") || Period.Equals("Jul") || Period.Equals("Aug") || Period.Equals("Sep") || Period.Equals("Oct") || Period.Equals("Nov") || Period.Equals("Dec"))
                            {
                                PeriodValueYear = Year1;
                            }

                        }
                    }                   
                    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                    {
                        int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                        if (vid != -1)
                        {
                            VerticalID = vid;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                        {
                            if (ddlVertical.SelectedValue != "-1")
                            {
                                VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                            }
                        }
                    }


                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                    var Branchlistloop = Branchlist.ToList();

                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    #region Sheet Major

                    List<CoalReviewKPUnitView> assignmentList = null;
                    assignmentList = ProcessManagement.GetMISViewMajor(RoleID, customerID, userID, VerticalID, ProcessID, FinancialYear, Period, Branchlist, startDatePeroid, EndDatePeroid);
                    string locatioName = null;
                    string ClientName = null;
                    if (CustBranchID != 1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            locatioName = (from row in entities.mst_CustomerBranch
                                           where row.ID == CustBranchID
                                           select row.Name).FirstOrDefault();

                            ClientName = (from row in entities.mst_Customer
                                          where row.ID == customerID
                                          select row.Name).FirstOrDefault();
                        }
                    }

                    DataTable table = new DataTable();
                    table.Columns.Add("Id", typeof(string));
                    table.Columns.Add("ProcessId", typeof(long));
                    table.Columns.Add("CustomerbranchId", typeof(long));
                    table.Columns.Add("ForPeriod", typeof(string));
                    table.Columns.Add("FinancialYear", typeof(string));
                    table.Columns.Add("Rs", typeof(string));
                    table.Columns.Add("Name", typeof(string));
                    table.Columns.Add("Observation", typeof(string));
                    table.Columns.Add("ManagementResponse", typeof(string));
                    table.Columns.Add("PersonResponsible", typeof(string));
                    table.Columns.Add("Owner", typeof(string));
                    table.Columns.Add("TimeLine", typeof(string));
                    table.Columns.Add("ObservationRating", typeof(string));

                    int i = 0;

                    int pIDchk = 0;
                    int chkprcoessId = 1;

                    int observaioncategoryIdchk = 0;
                    int chkincrementobservaioncategoryId = 1;

                    int locationidchk = 0;
                    int checklocationid = 1;

                    string observationtitlechk = string.Empty;
                    int chkobservationtitleId = 1;
                    string strAlpha = string.Empty;

                    int AtbtIDCheck = 0;

                 
                   assignmentList = assignmentList.OrderBy(a => a.ProcessOrder).ToList();
                    foreach (var cc in assignmentList)
                    {
                        string checktestATBTID = cc.ATBDId.ToString();

                        if (observaioncategoryIdchk != cc.ObservationCategory)
                        {
                            checklocationid = 1;
                            chkprcoessId = 1;
                            chkobservationtitleId = 1;
                            strAlpha = null;
                            for (int f = 65; f <= 90; f++) //
                            {
                                if ((chkincrementobservaioncategoryId + 64) == f)
                                {
                                    strAlpha += ((char)f).ToString() + " ";
                                }
                            }

                            observaioncategoryIdchk = Convert.ToInt16(cc.ObservationCategory);
                            table.Rows.Add(strAlpha, null, null, "", "", "", "", cc.ObsevationCategoryName, "", "", "", null, "0");

                            string FetchRoman = ToRoman(checklocationid);
                            locationidchk = Convert.ToInt16(cc.CustomerbranchId);
                            table.Rows.Add(FetchRoman, null, null, "", "", "", "", cc.LocationName, "", "", "", null, "4");

                            pIDchk = Convert.ToInt32(cc.ProcessId);
                            table.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");

                            string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;

                            //observationtitlechk = Convert.ToString(cc.ObservationTitle);
                            observationtitlechk = Convert.ToString(cc.ATBDId);
                            table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                            chkprcoessId++;
                            chkincrementobservaioncategoryId += 1;
                            checklocationid++;
                            chkobservationtitleId++;
                            i = 0;
                        }
                        else if (locationidchk != cc.CustomerbranchId)
                        {
                            //checklocationid = 1;
                            chkprcoessId = 1;
                            chkobservationtitleId = 1;

                            string FetchRoman = ToRoman(checklocationid);
                            locationidchk = Convert.ToInt16(cc.CustomerbranchId);
                            table.Rows.Add(FetchRoman, null, null, "", "", "", "", cc.LocationName, "", "", "", null, "4");


                            pIDchk = Convert.ToInt32(cc.ProcessId);
                            table.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");


                            string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;
                            observationtitlechk = Convert.ToString(cc.ATBDId);
                            table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                            chkobservationtitleId++;
                            checklocationid++;
                            chkprcoessId++;
                            i = 0;
                        }
                        else if (pIDchk != cc.ProcessId)
                        {
                            //chkprcoessId = 1;
                            chkobservationtitleId = 1;

                            pIDchk = Convert.ToInt32(cc.ProcessId);
                            table.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");


                            string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;
                            observationtitlechk = Convert.ToString(cc.ATBDId);
                            table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                            chkobservationtitleId++;
                            chkprcoessId++;

                            i = 0;
                        }
                        else if (!observationtitlechk.Equals(checktestATBTID))
                        {
                            string fetchTitleId = (chkprcoessId - 1) + "." + chkobservationtitleId;
                            observationtitlechk = Convert.ToString(cc.ObservationTitle);
                            table.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);
                            chkobservationtitleId++;

                            i = 0;
                        }

                        i++;
                        //string setIDforTitle = string.Empty;
                        //for (int f = 65; f <= 90; f++) //
                        //{
                        //    if ((i + 64) == f)
                        //    {
                        //        setIDforTitle += ((char)f).ToString() + " ";
                        //        setIDforTitle = setIDforTitle.ToLower();
                        //    }
                        //}

                        table.Rows.Add("", cc.ProcessId, cc.CustomerbranchId, cc.ForPeriod, cc.FinancialYear, cc.Rs, cc.Name, cc.Observation, cc.ManagementResponse,
                            cc.PersonResponsible, cc.Owner, Convert.ToString(cc.TimeLine), "9");

                    }

                    DataView view = new System.Data.DataView(table as DataTable);
                    ExcelData = view.ToTable("Selected", false, "Id", "Observation", "Rs", "ManagementResponse", "PersonResponsible", "Owner", "TimeLine", "ObservationRating");


                    exWorkSheet1.Cells["A1"].Value = "MAJOR  ISSUES";//ClientName;
                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                    //exWorkSheet1.Cells["A1"].Style.Font
                    exWorkSheet1.Cells["A1:H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["A1:H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["A1:H1"].Merge = true;
                    exWorkSheet1.Cells["A1:H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["A1:H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663

                    exWorkSheet1.Cells["A2"].Value = txtMonthDate.Text;//"Audit Period - " + Period + " " + PeriodValueYear;
                    exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A2"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["A2:H2"].Merge = true;
                    exWorkSheet1.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["A2:H2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    exWorkSheet1.Cells["A3"].Value = ClientName; //"Location : " + locatioName;
                    exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["A3:H3"].Merge = true;
                    exWorkSheet1.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["A3:H3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    //exWorkSheet1.Cells["A4"].Value = "Audit Period - " + Period;
                    //exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                    //exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                    //exWorkSheet1.Cells["A4:H4"].Merge = true;

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (Convert.ToInt32(item["ObservationRating"]) == 1)
                        {
                            item["ObservationRating"] = "High";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 3)
                        {
                            item["ObservationRating"] = "Low";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 2)
                        {
                            item["ObservationRating"] = "Mediuam";

                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 0)
                        {
                            item["ObservationRating"] = "ObservationCategory";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 4)
                        {
                            item["ObservationRating"] = "Location";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 5)
                        {
                            item["ObservationRating"] = "Process";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 6)
                        {
                            item["ObservationRating"] = "Title";
                        }
                        else
                        {
                            item["ObservationRating"] = "";
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(item["TimeLine"])))
                        {
                            item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                        }
                        //if (item["TimeLine"] != null && item["TimeLine"] != DBNull.Value && item["TimeLine"] != "")
                        //    item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                    }


                    exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData, true);
                    exWorkSheet1.Cells["A4"].Value = "Sl. No.";
                    exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["A4"].AutoFitColumns(10);
                    exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet1.Cells["B4"].Value = "Observation";
                    exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["B4"].AutoFitColumns(60);
                    exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet1.Cells["C4"].Value = "Rs.(Lacs)";
                    exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["C4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["C4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet1.Cells["D4"].Value = "Agreed Action";//"Auditee's Response"
                    exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["D4"].AutoFitColumns(25);
                    exWorkSheet1.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet1.Cells["E4"].Value = "Person Responsible";//Person Responsible
                    exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["E4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["E4"].AutoFitColumns(25);
                    exWorkSheet1.Cells["E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["E4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet1.Cells["F4"].Value = "Owner";//Owner
                    exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["F4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["F4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["F4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet1.Cells["F4"].AutoFitColumns(25);

                    exWorkSheet1.Cells["G4"].Value = "Time Line";
                    exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["G4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["G4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["G4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["G4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["G4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    //exWorkSheet1.Cells[4G5"].AutoFitColumns(10);
                    exWorkSheet1.Cells["G4"].AutoFitColumns(15);

                    exWorkSheet1.Cells["H4"].Value = "Risk";//Observation Rating
                    exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["H4"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["H4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["H4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["H4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    //exWorkSheet1.Cells["H5"].AutoFitColumns(10);

                    int lengthchk = Convert.ToInt32(ExcelData.Rows.Count) + 5;
                    for (int k = 5; k < lengthchk; k++)
                    {
                        string chkCondition = "H" + k;
                        string chkConditionobservation = "B" + k;



                        string checkallcond = exWorkSheet1.Cells[chkCondition].Value.ToString();

                        string nameprocess = exWorkSheet1.Cells[chkConditionobservation].Value.ToString();

                        string checkforRs = "c" + k;
                        string fetchvalueforRs = exWorkSheet1.Cells[checkforRs].Value.ToString();
                        if (!string.IsNullOrEmpty(fetchvalueforRs))
                        {
                            exWorkSheet1.Cells[checkforRs].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        string checkTimeLine = "G" + k;
                        string fetchTimeLine = exWorkSheet1.Cells[checkTimeLine].Value.ToString();
                        if (!string.IsNullOrEmpty(fetchTimeLine))
                        {
                            exWorkSheet1.Cells[checkTimeLine].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }

                        string ConditionoSlnoallighment = "A" + k;

                        exWorkSheet1.Cells[ConditionoSlnoallighment].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        if (checkallcond.Equals("Location") || checkallcond.Equals("Process"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet1.Cells[final].Merge = true;
                            exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BCD6EE"));//#8696B8
                            exWorkSheet1.Cells[final].Value = nameprocess;
                            exWorkSheet1.Cells[final].Style.Font.Bold = true;
                            //  exWorkSheet1.Cells[final].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BCD6EE"));//#1F5663 

                            if (checkallcond.Equals("Location"))
                            {
                                exWorkSheet1.Cells[chkConditionoSlno].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[chkConditionoSlno].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }

                        }
                        if (checkallcond.Equals("Title"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet1.Cells[final].Merge = true;
                            exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#F8CBAC"));//#1F5663
                            exWorkSheet1.Cells[final].Value = nameprocess;
                            exWorkSheet1.Cells[final].Style.Font.Bold = true;
                            //  exWorkSheet1.Cells[final].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#F8CBAC"));//#1F5663 

                        }

                        if (checkallcond.Equals("ObservationCategory"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet1.Cells[final].Merge = true;
                            exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFC000"));//#1F5663
                            exWorkSheet1.Cells[final].Value = nameprocess;
                            exWorkSheet1.Cells[final].Style.Font.Bold = true;
                            //  exWorkSheet1.Cells[final].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFC000"));//#1F5663 

                            exWorkSheet1.Cells[chkConditionoSlno].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        }

                        if (checkallcond.Equals("High"))
                        {


                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + checkTimeLine;

                            exWorkSheet1.Cells[final].Merge = true;
                            exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                            exWorkSheet1.Cells[final].Value = nameprocess;
                            exWorkSheet1.Cells[final].Style.Font.Bold = true;

                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663


                            //string chkConditionoSlno = "A" + k;
                            //string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);//#1F5663
                            exWorkSheet1.Cells[chkCondition].Value = "";
                        }
                        else if (checkallcond.Equals("Mediuam"))
                        {

                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + checkTimeLine;

                            exWorkSheet1.Cells[final].Merge = true;
                            exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                            exWorkSheet1.Cells[final].Value = nameprocess;
                            exWorkSheet1.Cells[final].Style.Font.Bold = true;

                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663


                            exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663
                            exWorkSheet1.Cells[chkCondition].Value = "";
                        }
                        else if (checkallcond.Equals("Low"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + checkTimeLine;

                            exWorkSheet1.Cells[final].Merge = true;
                            exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                            exWorkSheet1.Cells[final].Value = nameprocess;
                            exWorkSheet1.Cells[final].Style.Font.Bold = true;

                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663




                            exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);//#1F5663
                            exWorkSheet1.Cells[chkCondition].Value = "";
                        }

                    }


                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 4;
                    using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 8])
                    {
                        col.Style.WrapText = true;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    }

                    #endregion

                    //exWorkSheet2
                    #region Pending

                    List<MISReportForPending> assignmentListPending = null;
                    assignmentListPending = ProcessManagement.GetMISPendingView(RoleID, customerID, userID, VerticalID, ProcessID, FinancialYear, Period, Branchlist, PendingDate);
                    string locatioNamePending = null;
                    string ClientNamePending = null;
                    if (CustBranchID != 1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            locatioName = (from row in entities.mst_CustomerBranch
                                           where row.ID == CustBranchID
                                           select row.Name).FirstOrDefault();

                            ClientName = (from row in entities.mst_Customer
                                          where row.ID == customerID
                                          select row.Name).FirstOrDefault();
                        }
                    }

                    DataTable tablepending = new DataTable();
                    tablepending.Columns.Add("Id", typeof(string));
                    tablepending.Columns.Add("ProcessId", typeof(long));
                    tablepending.Columns.Add("CustomerbranchId", typeof(long));
                    tablepending.Columns.Add("ForPeriod", typeof(string));
                    tablepending.Columns.Add("FinancialYear", typeof(string));
                    tablepending.Columns.Add("Rs", typeof(string));
                    tablepending.Columns.Add("Name", typeof(string));
                    tablepending.Columns.Add("Observation", typeof(string));
                    tablepending.Columns.Add("ManagementResponse", typeof(string));
                    tablepending.Columns.Add("PersonResponsible", typeof(string));
                    tablepending.Columns.Add("Owner", typeof(string));
                    tablepending.Columns.Add("TimeLine", typeof(string));
                    tablepending.Columns.Add("ObservationRating", typeof(string));

                    i = 0;

                    pIDchk = 0;
                    chkprcoessId = 1;

                    observaioncategoryIdchk = 0;
                    chkincrementobservaioncategoryId = 1;

                    locationidchk = 0;
                    checklocationid = 1;

                    observationtitlechk = string.Empty;
                    chkobservationtitleId = 1;
                    strAlpha = string.Empty;

                    AtbtIDCheck = 0;
                    assignmentListPending = assignmentListPending.OrderBy(a => a.ProcessOrder).ToList();
                    foreach (var cc in assignmentListPending)
                    {
                        string checktestATBTID = cc.ATBDId.ToString();

                        if (observaioncategoryIdchk != cc.ObservationCategory)
                        {
                            checklocationid = 1;
                            chkprcoessId = 1;
                            chkobservationtitleId = 1;
                            strAlpha = null;
                            for (int f = 65; f <= 90; f++) //
                            {
                                if ((chkincrementobservaioncategoryId + 64) == f)
                                {
                                    strAlpha += ((char)f).ToString() + " ";
                                }
                            }

                            observaioncategoryIdchk = Convert.ToInt16(cc.ObservationCategory);
                            tablepending.Rows.Add(strAlpha, null, null, "", "", "", "", cc.ObsevationCategoryName, "", "", "", null, "0");

                            string FetchRoman = ToRoman(checklocationid);
                            locationidchk = Convert.ToInt16(cc.CustomerbranchId);
                            tablepending.Rows.Add(FetchRoman, null, null, "", "", "", "", cc.LocationName, "", "", "", null, "4");

                            pIDchk = Convert.ToInt32(cc.ProcessId);
                            tablepending.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");

                            string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;

                            //observationtitlechk = Convert.ToString(cc.ObservationTitle);
                            observationtitlechk = Convert.ToString(cc.ATBDId);
                            tablepending.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                            chkprcoessId++;
                            chkincrementobservaioncategoryId += 1;
                            checklocationid++;
                            chkobservationtitleId++;
                            i = 0;
                        }
                        else if (locationidchk != cc.CustomerbranchId)
                        {
                            //checklocationid = 1;
                            chkprcoessId = 1;
                            chkobservationtitleId = 1;

                            string FetchRoman = ToRoman(checklocationid);
                            locationidchk = Convert.ToInt16(cc.CustomerbranchId);
                            tablepending.Rows.Add(FetchRoman, null, null, "", "", "", "", cc.LocationName, "", "", "", null, "4");


                            pIDchk = Convert.ToInt32(cc.ProcessId);
                            tablepending.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");


                            string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;
                            observationtitlechk = Convert.ToString(cc.ATBDId);
                            tablepending.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                            chkobservationtitleId++;
                            checklocationid++;
                            chkprcoessId++;
                            i = 0;
                        }
                        else if (pIDchk != cc.ProcessId)
                        {
                            //chkprcoessId = 1;
                            chkobservationtitleId = 1;

                            pIDchk = Convert.ToInt32(cc.ProcessId);
                            tablepending.Rows.Add(chkprcoessId, null, null, "", "", "", "", cc.Name, "", "", "", null, "5");


                            string fetchTitleId = chkprcoessId + "." + chkobservationtitleId;
                            observationtitlechk = Convert.ToString(cc.ATBDId);
                            tablepending.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                            chkobservationtitleId++;
                            chkprcoessId++;

                            i = 0;
                        }
                        else if (!observationtitlechk.Equals(checktestATBTID))
                        {
                            string fetchTitleId = (chkprcoessId - 1) + "." + chkobservationtitleId;
                            observationtitlechk = Convert.ToString(cc.ObservationTitle);
                            tablepending.Rows.Add(fetchTitleId, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);
                            chkobservationtitleId++;

                            i = 0;
                        }

                        i++;
                       

                        tablepending.Rows.Add("", cc.ProcessId, cc.CustomerbranchId, cc.ForPeriod, cc.FinancialYear, cc.Rs, cc.Name, cc.Observation, cc.ManagementResponse,
                            cc.PersonResponsible, cc.Owner, Convert.ToString(cc.TimeLine), "9");

                    }

                    DataView view1 = new System.Data.DataView(tablepending as DataTable);
                    ExcelDataPending = view1.ToTable("Selected", false, "Id", "Observation", "Rs", "ManagementResponse", "PersonResponsible", "Owner", "TimeLine", "ObservationRating");


                    exWorkSheet2.Cells["A1"].Value = "PENDING  ISSUES";//ClientName;
                    exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A1"].Style.Font.Size = 12;
                    //exWorkSheet1.Cells["A1"].Style.Font
                    exWorkSheet2.Cells["A1:H1"].Merge = true;
                    exWorkSheet2.Cells["A1:H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["A1:H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["A1:H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["A1:H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663

                    exWorkSheet2.Cells["A2"].Value = txtMonthDate.Text;//"Audit Period - " + Period + " " + PeriodValueYear;
                    exWorkSheet2.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A2"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["A2:H2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["A2:H2"].Merge = true;

                    exWorkSheet2.Cells["A3"].Value = ClientName; //"Location : " + locatioName;
                    exWorkSheet2.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["A3:H3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["A3:H3"].Merge = true;

                    //exWorkSheet1.Cells["A4"].Value = "Audit Period - " + Period;
                    //exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                    //exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                    //exWorkSheet1.Cells["A4:H4"].Merge = true;

                    foreach (DataRow item in ExcelDataPending.Rows)
                    {
                        if (Convert.ToInt32(item["ObservationRating"]) == 1)
                        {
                            item["ObservationRating"] = "High";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 3)
                        {
                            item["ObservationRating"] = "Low";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 2)
                        {
                            item["ObservationRating"] = "Mediuam";

                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 0)
                        {
                            item["ObservationRating"] = "ObservationCategory";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 4)
                        {
                            item["ObservationRating"] = "Location";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 5)
                        {
                            item["ObservationRating"] = "Process";
                        }
                        else if (Convert.ToInt32(item["ObservationRating"]) == 6)
                        {
                            item["ObservationRating"] = "Title";
                        }
                        else
                        {
                            item["ObservationRating"] = "";
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(item["TimeLine"])))
                        {
                            item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                        }
                    }


                    exWorkSheet2.Cells["A4"].LoadFromDataTable(ExcelDataPending, true);
                    exWorkSheet2.Cells["A4"].Value = "Sl. No.";
                    exWorkSheet2.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["A4"].AutoFitColumns(10);
                    exWorkSheet2.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["A4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet2.Cells["B4"].Value = "Observation";
                    exWorkSheet2.Cells["B4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["B4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["B4"].AutoFitColumns(60);
                    exWorkSheet2.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["B4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet2.Cells["C4"].Value = "Rs.(Lacs)";
                    exWorkSheet2.Cells["C4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["C4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["C4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["C4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["C4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet2.Cells["D4"].Value = "Agreed Action";//"Auditee's Response"
                    exWorkSheet2.Cells["D4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["D4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["D4"].AutoFitColumns(25);
                    exWorkSheet2.Cells["D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["D4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet2.Cells["E4"].Value = "Person Responsible";//Person Responsible
                    exWorkSheet2.Cells["E4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["E4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["E4"].AutoFitColumns(25);
                    exWorkSheet2.Cells["E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["E4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet2.Cells["F4"].Value = "Owner";//Owner
                    exWorkSheet2.Cells["F4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["F4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["F4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["F4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Cells["F4"].AutoFitColumns(25);

                    exWorkSheet2.Cells["G4"].Value = "Time Line";
                    exWorkSheet2.Cells["G4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["G4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["G4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["G4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["G4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["G4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    //exWorkSheet1.Cells[4G5"].AutoFitColumns(10);
                    exWorkSheet2.Cells["G4"].AutoFitColumns(15);

                    exWorkSheet2.Cells["H4"].Value = "Risk";//Observation Rating
                    exWorkSheet2.Cells["H4"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["H4"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["H4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["H4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["H4"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    //exWorkSheet1.Cells["H5"].AutoFitColumns(10);

                    lengthchk = Convert.ToInt32(ExcelDataPending.Rows.Count) + 5;
                    for (int k = 5; k < lengthchk; k++)
                    {

                        string chkCondition = "H" + k;
                        string chkConditionobservation = "B" + k;



                        string checkallcond = exWorkSheet2.Cells[chkCondition].Value.ToString();

                        string nameprocess = exWorkSheet2.Cells[chkConditionobservation].Value.ToString();

                        string checkforRs = "c" + k;
                        string fetchvalueforRs = exWorkSheet2.Cells[checkforRs].Value.ToString();
                        if (!string.IsNullOrEmpty(fetchvalueforRs))
                        {
                            exWorkSheet2.Cells[checkforRs].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        string checkTimeLine = "G" + k;
                        string fetchTimeLine = exWorkSheet2.Cells[checkTimeLine].Value.ToString();
                        if (!string.IsNullOrEmpty(fetchTimeLine))
                        {
                            exWorkSheet2.Cells[checkTimeLine].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }

                        string ConditionoSlnoallighment = "A" + k;

                        exWorkSheet2.Cells[ConditionoSlnoallighment].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        if (checkallcond.Equals("Location") || checkallcond.Equals("Process"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet2.Cells[final].Merge = true;
                            exWorkSheet2.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BCD6EE"));//#8696B8
                            exWorkSheet2.Cells[final].Value = nameprocess;
                            exWorkSheet2.Cells[final].Style.Font.Bold = true;
                            //  exWorkSheet2.Cells[final].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#BCD6EE"));//#1F5663 

                            if (checkallcond.Equals("Location"))
                            {
                                exWorkSheet2.Cells[chkConditionoSlno].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet2.Cells[chkConditionoSlno].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }
                        }
                        if (checkallcond.Equals("Title"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet2.Cells[final].Merge = true;
                            exWorkSheet2.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#F8CBAC"));//#1F5663
                            exWorkSheet2.Cells[final].Value = nameprocess;
                            exWorkSheet2.Cells[final].Style.Font.Bold = true;
                            //  exWorkSheet1.Cells[final].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#F8CBAC"));//#1F5663 

                        }

                        if (checkallcond.Equals("ObservationCategory"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet2.Cells[final].Merge = true;
                            exWorkSheet2.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFC000"));//#1F5663
                            exWorkSheet2.Cells[final].Value = nameprocess;
                            exWorkSheet2.Cells[final].Style.Font.Bold = true;
                            //  exWorkSheet1.Cells[final].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFC000"));//#1F5663 

                            exWorkSheet2.Cells[chkConditionoSlno].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        }

                        if (checkallcond.Equals("High"))
                        {


                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + checkTimeLine;

                            exWorkSheet2.Cells[final].Merge = true;
                            exWorkSheet2.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                            exWorkSheet2.Cells[final].Value = nameprocess;
                            exWorkSheet2.Cells[final].Style.Font.Bold = true;

                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663


                            //string chkConditionoSlno = "A" + k;
                            //string final = chkConditionobservation + ":" + chkCondition;

                            exWorkSheet2.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);//#1F5663
                            exWorkSheet2.Cells[chkCondition].Value = "";
                        }
                        else if (checkallcond.Equals("Mediuam"))
                        {

                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + checkTimeLine;

                            exWorkSheet2.Cells[final].Merge = true;
                            exWorkSheet2.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                            exWorkSheet2.Cells[final].Value = nameprocess;
                            exWorkSheet2.Cells[final].Style.Font.Bold = true;

                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663


                            exWorkSheet2.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663
                            exWorkSheet2.Cells[chkCondition].Value = "";
                        }
                        else if (checkallcond.Equals("Low"))
                        {
                            string chkConditionoSlno = "A" + k;
                            string final = chkConditionobservation + ":" + checkTimeLine;

                            exWorkSheet2.Cells[final].Merge = true;
                            exWorkSheet2.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663
                            exWorkSheet2.Cells[final].Value = nameprocess;
                            exWorkSheet2.Cells[final].Style.Font.Bold = true;

                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ffb08f"));//#1F5663




                            exWorkSheet2.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet2.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);//#1F5663
                            exWorkSheet2.Cells[chkCondition].Value = "";
                        }

                    }




                    count = Convert.ToInt32(ExcelDataPending.Rows.Count) + 4;
                    using (ExcelRange col = exWorkSheet2.Cells[1, 1, count, 8])
                    {
                        col.Style.WrapText = true;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    }

                    #endregion

                    Byte[] fileBytes = exportPackge.GetAsByteArray();                    
                    locatioName = locatioName + "_" + txtMonthDate.Text + ".xlsx";
                    locatioName = locatioName.Replace(',', ' ');
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();                    
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment;filename=MISReport_" + locatioName);//locatioName                                                                             
                    Response.BinaryWrite(fileBytes);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
        }        
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }            
            BindVertical();
            BindSchedulingType();            
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }            
            BindVertical();
            BindSchedulingType();            
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }            
            BindVertical();
            BindSchedulingType();            
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }            
            BindVertical();
            BindSchedulingType();            
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVertical();
            BindSchedulingType();            
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }

    }
}