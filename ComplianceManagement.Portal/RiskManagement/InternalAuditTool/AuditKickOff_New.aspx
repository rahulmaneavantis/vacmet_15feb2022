﻿<%@ Page Title="Audit KickOff" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditKickOff_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditKickOff_New" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .table > tbody > tr > th, .table > tbody > tr > th > a {
            border-bottom: 6px solid #ddd;
        }
        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
            padding-bottom: 8px;
            padding-left: unset;
            padding-right: unset;
            padding-top: 8px;
        }
        .MarginTop {
            margin-top: 0px !important;
        }
        .Check {
            width: 144px !important;
            height: 34px !important;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }
        div#ContentPlaceHolder1_grdProcess_ddlPerformerAuditHeader_dv {
            margin-top: 15px;
        }

        div#ContentPlaceHolder1_grdProcess_ddlPerformerImplemetationHeader_dv {
            margin-top: 15px;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .GridView1 {
            width: 200px;
            margin-left: auto;
            margin-right: auto;
        }

        .accordionContent1 {
            background-color: #D3DEEF;
            border-color: -moz-use-text-color #2F4F4F #2F4F4F;
            border-right: 1px dashed #2F4F4F;
            border-style: none dashed dashed;
            border-width: medium 1px 1px;
            padding: 10px 5px 5px;
            width: 92%;
        }

        .accordionHeaderSelected1 {
            background-color: #5078B3;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .accordionHeader1 {
            background-color: #2E4D7B;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .href1 {
            color: White;
            font-weight: bold;
            text-decoration: none;
        }

        .textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            //JQUERY DATE PICKER.            
            $(function () {
                $('input[id*=tbxLoanStartDate]').datepicker(
                    { dateFormat: 'dd-mm-yy' });
            });
            $(function () {
                $('input[id*=tbxLoanEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });

            $(function () {
                $('input[id*=txtStartDateKickoff]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
            $(function () {
                $('input[id*=txtEndDateKickoff]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

        function LblClear() {
            $('#cvDuplicateEntry1').ErrorMessage('');
        };

        function LblClearValue() {
            setInterval(LblClear, 3000);
        };
    </script>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).ready(function () {
            //setactivemenu('Audit Kickoff');
            fhead('Audit Kickoff');
        });

        function CloseModelDelete() {
            $('#ShowDeleteModel').modal('hide');
            location.reload();
        }

        function OpenModelDeleteAssignment() {
            $('#ShowDeleteModel').modal('show');
            $('#ContentPlaceHolder1_IframeDeleteAssignment').attr('src', "../../RiskManagement/InternalAuditTool/DeleteKickOff.aspx");
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                      <section class="panel"> 
                        <div class="panel-body">
                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="ComplianceValidationGroup1" />
                                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                                <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0 entrycount" style="width: 15%">
                                    <div class="col-md-2 colpadding0" style="margin-right: 15%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left; margin-bottom: 0px;"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <%--<asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" />--%>
                                        <asp:ListItem Text="20" Selected="True"/>
                                        <asp:ListItem Text="50" />
                                        <asp:ListItem Text="100" />
                                    </asp:DropDownList>
                                </div>
                                 <div class="col-md-4 colpadding0 entrycount" style="width: 68%;">
                                     </div>
                                <div class="col-md-2 colpadding0 entrycount" style="display:none; ">
                                    <asp:DropDownListChosen runat="server" ID="ddlPerformerFilter" class="form-control m-bot15" Width="90%" Height="32px"
                                        AutoPostBack="true" DataPlaceHolder="Performer"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlPerformerFilter_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount " style="display:none;  ">
                                    <asp:DropDownListChosen runat="server" ID="ddlReviewerFilter" class="form-control m-bot15" Width="90%" Height="32px"
                                        AutoPostBack="true" DataPlaceHolder="Reviewer"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlReviewerFilter_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0 entrycount" style="display:none; ">
                                    <asp:DropDownListChosen runat="server" ID="ddlReviewerFilter2" class="form-control m-bot15" Width="90%" Height="32px"
                                        AutoPostBack="true" DataPlaceHolder="Reviewer2"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlReviewerFilter2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-2 colpadding0 entrycount">
                                    <asp:Button Text="Send Email" CssClass="btn btn-primary" runat="server"
                                        ID="btnSendEmail" OnClick="btnSendEmail_Click" style="margin-left: 10px;" />
                                    <asp:Button Text="Back" CssClass="btn btn-primary" runat="server" Style="float:right;"
                                                   ID="btnBack" OnClick="btnBack_Click" />
                                    <asp:Button ID="btnDeleteAssigned" runat="server" Text="Delete Assigment" Visible="false" Style="float:right; margin-right: 6px;" CssClass="btn btn-primary" OnClientClick="OpenModelDeleteAssignment();" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 colpadding0" style="margin-top: 15px;">
                               <div class="col-md-2 colpadding0 entrycount" style="width: 100%">
                                    <div class="col-md-2 colpadding0" >
                                       <label style="display: block; float: left; font-size: 14px; color: #999;">
                                            Upload Audit Assignment Excel File</label>
                                    </div>
                                    <asp:FileUpload ID="fileUploadKickOff" runat="server" style="width: 300px; margin-left: 1%;float: left;color: black;"/>
                                    <div style="margin-left: 35%;">
                                        <asp:Button ID="btnUploadDocument" runat="server" CssClass="btn btn-primary" Width="13%" Text="Upload" OnClick="btnUploadDocument_Click"/>
                                        <div style="float: right;"> 
                                            <asp:LinkButton ID="lbtnDownloadSampleFormat" runat="server" Font-Underline="True" Text="Download the Audit Assignment Sample Excel File" OnClick="lbtnDownloadSampleFormat_Click" ></asp:LinkButton>
                                        </div>
                                    </div> 
                                </div>
                           </div>
                            <div class="clearfix"></div>
                            <div runat="server" id="dvProcessDetails" class="tab-pane active">
                                <div style="margin-top: 10px">
                                    <asp:GridView runat="server" ID="grdProcess" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                        PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table"
                                        GridLines="None" Width="100%" AllowSorting="true"
                                        DataKeyNames="ProcessID" OnRowDataBound="grdCompliances_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="AuditID" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAuditID" Style="display: none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblLocation" data-toggle="tooltip" data-placement="top" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblLocationId" Style="display: none;" Text='<%#Eval("CustomerBranchId") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblVerticalName" data-toggle="tooltip" data-placement="top" Text='<%# Eval("VerticalName") %>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblVerticalId" Style="display: none;" Text='<%#Eval("VerticalId") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblFinancialYear" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblISAHQMP" data-toggle="tooltip" data-placement="top" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblTermName" data-toggle="tooltip" data-placement="top" Text='<%# Eval("TermName") %>' ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblExternalAuditorId" Text='<%# Eval("ExternalAuditorId") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblPhaseCount" Text='<%# Eval("PhaseCount") %>' CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                   <div class="text_NlinesusingCSS" style="width: 100px;">
                                                        <asp:Label runat="server" ID="lblProcessName" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblProcessId" Style="display: none;" Text='<%#Eval("ProcessId") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:TemplateField HeaderText="SubProcess">
                                                <ItemTemplate>
                                                   <div class="text_NlinesusingCSS" style="width: 100px;">
                                                        <asp:Label runat="server" ID="lblSubProcessName" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblSubProcessId" Style="display: none;" Text='<%#Eval("SubProcessId") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                           
                                            <asp:TemplateField HeaderText="Audit" HeaderStyle-CssClass="text-center">
                                                 <HeaderTemplate>
                                             <div>Audit</div>
                                              <div style="clear:both;height:5px;"></div>
                                             <div style="text-overflow: ellipsis; white-space: nowrap; width: 460px;">
                                                <div class="col-md-12" style="padding-left: 0px !important">
                                                    <div class="col-md-4">
                                                        <asp1:DropDownCheckBoxes ID="ddlPerformerAuditHeader" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15 Check" OnSelectedIndexChanged="ddlPerformerAuditHeader_SelectedIndexChanged"
                                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130"/>
                                                    <Texts SelectBoxCaption="Performer" />
                                                </asp1:DropDownCheckBoxes>
                                                </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlReviewerAuditHeader" runat="server" Width="140px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer" AutoPostBack="true" OnSelectedIndexChanged="ddlReviewerAuditHeader_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:Label ID="lblReviewerNameAuditHeader"  runat="server" Visible="false"></asp:Label>
                                               </div>
                                                    <div class="col-md-4"> <asp:DropDownList ID="ddlReviewer2AuditHeader" runat="server" Width="140px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer2" AutoPostBack="true" OnSelectedIndexChanged="ddlReviewer2AuditHeader_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:Label ID="lblReviewerName2AuditHeader" runat="server" Visible="false" ></asp:Label>
                                                </div>
                                                </div>
                                                </div>
  
                                         </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div style="text-overflow: ellipsis; white-space: nowrap; width: 460px;">
                                                        <div class="col-md-12" style="padding-left: 0px !important">
                                                            <div class="col-md-4">
                                                                <asp1:DropDownCheckBoxes ID="ddlPerformer" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%;">
                                                    <Style SelectBoxWidth="146" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130" SelectBoxCssClass="MarginTop"/>
                                                    <Texts SelectBoxCaption="Performer" />
                                                </asp1:DropDownCheckBoxes>
                                                                <asp:Label ID="lblPerformerName" runat="server" Visible="false"></asp:Label>                                                                
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlReviewer" runat="server" Width="145px"
                                                                    class="form-control m-bot15" DataPlaceHolder="Reviewer">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblReviewerName" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlReviewer2" runat="server" Width="145px"
                                                                    class="form-control m-bot15" DataPlaceHolder="Reviewer2">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblReviewerName2" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Implementation Review" HeaderStyle-CssClass="text-center">
                                                   <HeaderTemplate >
                                              <div>Implementation Review</div>
                                              <div style="clear:both;height:5px;"></div>
                                             <div style="text-overflow: ellipsis; white-space: nowrap; width: 460px;">
                                                <div class="col-md-12" style="padding-left: 0px !important">
                                                    <div class="col-md-4">
                                                        <asp1:DropDownCheckBoxes ID="ddlPerformerImplemetationHeader" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15 Check" OnSelectedIndexChanged="ddlPerformerImplemetationHeader_SelectedIndexChanged"
                                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="146" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130"  SelectBoxCssClass="MarginTop" />
                                                    <Texts SelectBoxCaption="Performer" />
                                                </asp1:DropDownCheckBoxes> 
                                                <asp:Label ID="lblPerformerNameImplemetationHeader" runat="server" Visible="false"></asp:Label>
                                                </div>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlReviewerImplemetationHeader" runat="server" Width="140px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer" AutoPostBack="true"  OnSelectedIndexChanged="ddlReviewerImplemetationHeader_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:Label ID="lblReviewerNameImplemetationHeader" runat="server" Visible="false"></asp:Label>
                                               </div>
                                                    <div class="col-md-4"> <asp:DropDownList ID="ddlReviewer2ImplemetationHeader" runat="server" Width="140px"                                                                                                
                                                class="form-control m-bot15" DataPlaceHolder="Reviewer2" AutoPostBack="true"  OnSelectedIndexChanged="ddlReviewer2ImplemetationHeader_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:Label ID="lblReviewerName2ImplemetationHeader" runat="server" Visible="false"></asp:Label>
                                                </div>
                                                </div>
                                                </div>
  
                                         </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div style="text-overflow: ellipsis; white-space: nowrap; width: 460px;">
                                                        <div class="col-md-12" style="padding-left: 0px !important">
                                                            <div class="col-md-4">
                                                                <%--<asp:DropDownList ID="ddlPerformerIMP" class="form-control m-bot15" Width="145px"
                                                                    DataPlaceHolder="Performer" runat="server">
                                                                </asp:DropDownList>--%>
                                                                <asp1:DropDownCheckBoxes ID="ddlPerformerIMP" runat="server" AutoPostBack="true" Visible="true"
                                                    CssClass="form-control m-bot15 Check" AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                    Style="padding: 0px; margin: 0px; width: 85%; height: 50px;">
                                                    <Style SelectBoxWidth="184" DropDownBoxBoxWidth="163" DropDownBoxBoxHeight="130" />
                                                    <Texts SelectBoxCaption="Performer" />
                                                </asp1:DropDownCheckBoxes>
                                                                <asp:Label ID="lblPerformerNameIMP" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlReviewerIMP" runat="server" Width="145px"
                                                                    class="form-control m-bot15" DataPlaceHolder="Reviewer">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblReviewerNameIMP" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlReviewerIMP2" runat="server" Width="145px"
                                                                    class="form-control m-bot15" DataPlaceHolder="Reviewer2">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblReviewerNameIMP2" runat="server" Visible="false"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>

                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                    class="form-control m-bot15" Width="120%" Height="30px"
                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0" style="text-align: right;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" ValidationGroup="ComplianceValidationGroup1" CssClass="btn btn-primary" />
                                </div>
                                <div class="col-md-5 colpadding0" style="float: right">
                                    <div class="table-paging" style="margin-bottom: 10px;">
                                        <div class="table-paging-text" style="float: right;">
                                            <p>
                                                Page                                          
                                            </p>
                                        </div>
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </section>
                </div>
            </div>            
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnDownloadSampleFormat" />
            <asp:PostBackTrigger ControlID="btnUploadDocument" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="modal fade" id="ShowDeleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 85%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 260px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Delete KickOff</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDeleteAssignment" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
