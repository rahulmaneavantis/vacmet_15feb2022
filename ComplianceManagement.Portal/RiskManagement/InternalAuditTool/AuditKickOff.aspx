﻿<%@ Page Title="Audit Kickoff" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="AuditKickOff.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditKickOff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>


    <style type="text/css">
        .textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.            
            $(function () {
                $('input[id*=tbxLoanStartDate]').datepicker(
                    { dateFormat: 'dd-mm-yy' });
            });
            $(function () {
                $('input[id*=tbxLoanEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('#divComplianceDetailsDialog').dialog({
                height: 670,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Audit Kickoff Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Kickoff');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                       <%-- <div style="margin-bottom: 4px"/>--%> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>                    
                    <%--<p style="color: #999; margin-top: 10px; margin-left: 5px;">Entries</p>--%>
                </div>  
                <div id="FilterLocationdiv" runat="server" class="col-md-4 colpadding0 entrycount" style="color: #999; padding-top: 9px;">
                   <%-- <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>--%>
                   <%-- <p style="color: #999; margin-top: 5px; float:left;"> Select Location:</p>--%>
                    <%--<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                    *</label>
                                <label style="display: block;  font-size: 13px; color: #999; margin-top: 5px;">
                                    Select  Location :</label>--%>
                    <div class="col-md-3 colpadding0">
                    <p style="color: #999; margin-top: 5px; width:280px;">Select  Location :</p>
                        </div>
                    <asp:TextBox runat="server" ID="tbxFilterLocation" Width="250px" CssClass="form-control" />
                    <div style="margin-left: 100px; z-index: 10" runat="server"  id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="180px" Width="250px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual"  Display="None" />
                    </div>                    
                </div>
                    <%-- <div id="FilterLocationdiv" class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                    *</label>
                                <label style="width: 200px; display: block;  font-size: 13px; color: #999; margin-top: 5px;">
                                    Select  Location</label>
                                <asp:TextBox runat="server" ID="tbxFilterLocation" CssClass="form-control" />
                                <div style="float: left; z-index: 10" id="divFilterLocation">
                                    <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                        Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Location."
                                        ControlToValidate="tbxFilterLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                </div>
                            </div>--%>
                    <div class="col-md-5 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px; width:250px; ">Select Financial Year :</p>
                    </div>
                       <asp:DropDownList runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" style="width:40%"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                        </asp:DropDownList>
                            <asp:ValidationSummary runat="server" CssClass="vdsummary" ForeColor="Red"
                        ValidationGroup="ComplianceValidationGroup1" />
                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>                    
            <div style="margin-bottom: 4px">
            <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" OnRowDataBound="grdCompliances_RowDataBound"
                 OnRowCreated="grdCompliances_RowCreated" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                 OnSorting="grdCompliances_Sorting" DataKeyNames="ProcessID" OnRowCommand="grdCompliances_RowCommand1" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                    <ItemTemplate>
                                  <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch Name" ItemStyle-Width="200px" SortExpression="CustomerBranchId" HeaderStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                <asp:Label runat="server" ID="lblLocation" Text='<%# ShowCustomerBranchName((int)Eval("CustomerBranchId")) %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FinancialYear" ItemStyle-Width="100px" SortExpression="FinancialYear" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                <asp:Label runat="server" ID="lblFinancialYear" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                <asp:Label runat="server" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Term Name" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                <asp:Label runat="server" Text='<%# Eval("TermName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AssignedTo" ItemStyle-Width="10px" SortExpression="AssignedTo" Visible="false">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                <asp:Label runat="server" ID="lblAssignedTo" Text='<%# Eval("AssignedTo") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EAId" ItemStyle-Width="10px" Visible="false">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                <asp:Label runat="server" ID="lblExternalAuditorId" Text='<%# Eval("ExternalAuditorId") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PhaseCount" ItemStyle-Width="10px" Visible="false">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 5px;">
                                <asp:Label runat="server" ID="lblPhaseCount" Text='<%# Eval("PhaseCount") %>' CssClass="label"></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") %>' CausesValidation="false"><img src="../../Images/edit_icon.png" alt="Edit Compliance" title="Edit Compliance" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
               <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />            
                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                </PagerTemplate> 
                </asp:GridView>
                 </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="margin-left: 750px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceDetailsDialog" style="display: none">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <%--<asp:Label ID="lblLocation" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server"></asp:Label>--%>
                        <asp:DropDownList Enabled="false" runat="server" ID="ddlLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator14" ErrorMessage="Please Select Location."
                            ControlToValidate="ddlLocation" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Internal /External
                        </label>

                        <asp:DropDownList runat="server" ID="ddlInternalExternal" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlInternalExternal_SelectedIndexChanged" Enabled="false">
                            <asp:ListItem Text="Select">Select Internal/External</asp:ListItem>
                            <asp:ListItem Text="Internal">Internal</asp:ListItem>
                            <asp:ListItem Text="Internal">External</asp:ListItem>
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator17" ErrorMessage="Please Select Internal /External."
                            ControlToValidate="ddlInternalExternal" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" runat="server" id="DivIsExternal" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            External Auditor</label>
                        <asp:DropDownList runat="server" ID="ddlAuditor" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" Enabled="false"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlAuditor_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Financial Year</label>
                        <asp:DropDownList runat="server" ID="ddlFinancialYear" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" />

                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Scheduling Type</label>
                        <asp:DropDownList runat="server" ID="ddlSchedulingType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" Enabled="false" />

                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;" runat="server" id="DivIsPeriod" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select For Period</label>
                        <asp:DropDownList runat="server" ID="ddlPeriod" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" Enabled="false" />
                        <asp:RequiredFieldValidator ID="RFVPeriod" ErrorMessage="Select Period." ControlToValidate="ddlPeriod"
                            runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="Select Period" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Process</label>
                        <asp:DropDownList runat="server" ID="ddlprocess" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlprocess_SelectedIndexChanged" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Select Period." ControlToValidate="ddlprocess"
                            runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="Select Process" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Actual Start Date</label>
                        <asp:TextBox runat="server" ID="tbxLoanStartDate" Style="height: 16px; width: 100px;" DataFormatString="{dd-MM-yyyy}" CssClass="textbox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Actual Start Date can not be empty." ControlToValidate="tbxLoanStartDate"
                            runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator runat="server" ValidationGroup="ComplianceValidationGroup" ControlToValidate="tbxLoanStartDate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"
                            ErrorMessage="Invalid Date Format, Should be in dd-MM-yyyy Format" ForeColor="Red" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Actual End Date</label>
                        <asp:TextBox runat="server" ID="tbxLoanEndDate" Style="height: 16px; width: 100px;" DataFormatString="{dd-MM-yyyy}" CssClass="textbox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ErrorMessage="Actual End Date can not be empty." ControlToValidate="tbxLoanEndDate"
                            runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator runat="server" ValidationGroup="ComplianceValidationGroup" ControlToValidate="tbxLoanEndDate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"
                            ErrorMessage="Invalid Date Format, Should be in dd-MM-yyyy Format" ForeColor="Red" />
                        <%--<asp:CompareValidator ID="CVStartEndDate" ErrorMessage="End Date Should be Greater than Start Date"
                            Display="None"  ControlToValidate="tbxLoanEndDate" ControlToCompare="tbxLoanStartDate" runat="server" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" Type="Date" />--%>
                    </div>
                    <div style="margin-bottom: 7px" id="dvSampleForm" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 210px; display: block; float: left; font-size: 13px; color: #333;">
                            Audit Planning Document Upload</label>
                        <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                        <asp:FileUpload runat="server" ID="fuSampleFile" />

                    </div>

                    <div style="margin-bottom: 7px">
                        Auditor Details               
                    </div>
                    <div style="margin-left: 20px; background: white; border: 1px solid gray; height: 200px; margin-bottom: 7px;" runat="server" id="DIVdisplaygrid" visible="false">

                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="9" Width="100%" OnRowDataBound="grdRiskActivityMatrix_RowDataBound"
                                    Font-Size="12px" DataKeyNames="ProcessId" ShowFooter="true" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemTemplateID" runat="server" Text='<%# Eval("ProcessId") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblFooterTemplateID" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Process">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemTemplateProcess" runat="server" Text='<%# ShowProcessNameMAIN((long)Eval("ProcessId")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblFooterTemplateProcess" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Performer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPerformer" runat="server" Text='<%# ShowPerformerName((long)Eval("ProcessId"),(long)Eval("CustomerBranchID"),3,(long)Eval("PerformerUserID")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlPerformer" runat="server" Width="200px" Height="30px"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ErrorMessage="Select Performer." ControlToValidate="ddlPerformer"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="-1" Display="None" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Reviewer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReviewer" runat="server" Text='<%# ShowReviewerName((long)Eval("ProcessId"),(long)Eval("CustomerBranchID"),4,(long)Eval("ReviewerUserID")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlReviewer" runat="server" Width="200px" Height="30px"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Select Reviewer." ControlToValidate="ddlReviewer"
                                                    runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="-1" Display="None" />

                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%--<asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="Delete" Height="25px" Width="60px" />--%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Add" Height="25px" Width="60px" CommandName="Footer"
                                                    ValidationGroup="ComplianceValidationGroup" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataTemplate>
                                        <table>
                                            <tr style="background-color: #5c9ccc">
                                                <th scope="col" style="display: none;">ID</th>
                                                <th scope="col">Process</th>
                                                <th scope="col">Performer</th>
                                                <th scope="col">Reviewer</th>
                                            </tr>
                                            <tr>
                                                <td style="display: none;">
                                                    <asp:Label ID="lblFooterTemplateID" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 20%;">
                                                    <asp:Label ID="lblFooterTemplateProcess" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 20%;">
                                                    <asp:DropDownList ID="ddlPerformer" runat="server" Height="30px" Width="200px"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ErrorMessage="Select Performer." ControlToValidate="ddlPerformer"
                                                        runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="-1" Display="None" />
                                                </td>
                                                <td style="width: 20%;">
                                                    <asp:DropDownList ID="ddlReviewer" runat="server" Height="30px" Width="200px"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Select Reviewer." ControlToValidate="ddlReviewer"
                                                        runat="server" ValidationGroup="ComplianceValidationGroup" InitialValue="-1" Display="None" />
                                                </td>
                                                <td style="width: 9%;">
                                                    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Add" CommandName="EmptyDataTemplate" Height="25px"
                                                        Width="60px" ValidationGroup="ComplianceValidationGroup" />
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                    <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button" Visible="false"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" Visible="false" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
