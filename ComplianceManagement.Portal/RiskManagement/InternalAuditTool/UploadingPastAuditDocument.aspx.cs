﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class UploadingPastAuditDocument : System.Web.UI.Page
    {
        public List<long> Branchlist = new List<long>();
        //protected static string AuditHeadOrManagerReport;
        protected static int RoleId;
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            //AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            //RoleId = CustomerManagementRisk.GetRoleid(Portal.Common.AuthenticationHelper.UserID);

            if (!IsPostBack)
            {
                ddlLegalEntityPopPup.ClearSelection();
                ddlSubEntity1PopPup.ClearSelection();
                ddlSubEntity2PopPup.ClearSelection();
                ddlSubEntity3PopPup.ClearSelection();
                ddlFilterLocationPopPup.ClearSelection();
                ddlFilterFinancialPopPup.ClearSelection();
                ddlVertical.ClearSelection();
                BindLegalEntityData(ddlLegalEntityPopPup);
                BindFnancialYear(ddlFilterFinancialPopPup);
                BindAuditBackground();
                BindVersionDetails();
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Convert.ToString(Request.QueryString["Type"]) != "undefined")
                    {
                        string Type = Convert.ToString(Request.QueryString["Type"]);
                        if (Type == "0")//Edit
                        {
                            DivTopFilter.Visible = true;
                            divuploadlbl.Visible = false;
                            FileUploadObservation.Visible = false;
                            //grdVersionDisplayDownload.Visible = false;
                            lnkAddNewAB.Visible = false;
                            BindFilters();
                        }
                        else if (Type == "1") //ADD
                        {
                            DivTopFilter.Visible = true;
                            divuploadlbl.Visible = true;
                            FileUploadObservation.Visible = true;
                        }
                        else if (Type == "2")//View
                        {
                            DivTopFilter.Visible = false;
                        }
                    }
                    else
                    {
                        DivTopFilter.Visible = false;
                        grdVersionDisplayDownload.Visible = false;
                    }
                }
                else
                {
                    DivTopFilter.Visible = false;
                    grdVersionDisplayDownload.Visible = false;
                }
            }
        }

        private void BindFilters()
        {
            string Type = string.Empty;
            int PastAuditDocumentID = -1;
            if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
            {
                if (Convert.ToString(Request.QueryString["Type"]) != "undefined")
                {
                    Type = Convert.ToString(Request.QueryString["Type"]);
                    if (Type == "0")//Edit
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString["PastAuditDocID"]))
                        {
                            if (Convert.ToString(Request.QueryString["PastAuditDocID"]) != "undefined")
                            {
                                PastAuditDocumentID = Convert.ToInt32(Request.QueryString["PastAuditDocID"]);
                                if (PastAuditDocumentID > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        List<SP_BindPastAuditDocumentFileMapping_Result> riskassignmentexport = new List<SP_BindPastAuditDocumentFileMapping_Result>();

                                        var ResultID = (from row in entities.Past_Audit_Documents
                                                        where row.ID == PastAuditDocumentID
                                                        select row).FirstOrDefault();

                                        if (ResultID != null)
                                        {
                                            ddlLegalEntityPopPup.ClearSelection();
                                            ddlSubEntity1PopPup.ClearSelection();
                                            ddlSubEntity2PopPup.ClearSelection();
                                            ddlSubEntity3PopPup.ClearSelection();
                                            ddlFilterLocationPopPup.ClearSelection();
                                            ddlFilterFinancialPopPup.ClearSelection();
                                            ddlAuditBackground.ClearSelection();
                                            ddlVertical.ClearSelection();
                                            tbxReportDate.Text = string.Empty;
                                            tbxIssueBy.Text = string.Empty;
                                            tbxRemark.Text = string.Empty;
                                            tbxTitle.Text = string.Empty;
                                            SetValuesToLocationFilters((int)ResultID.CustomerBranch);
                                            rfvFile.Enabled = false;
                                            //GetAllHierarchy((int)ResultID.CustomerID, (int)ResultID.CustomerBranch);
                                            //if (ddlLegalEntityPopPup.Items.Count > 0)
                                            //{
                                            //    Branchlist.ForEach(entry =>
                                            //   {
                                            //       string BranchID = Convert.ToString(entry);
                                            //       if (ddlLegalEntityPopPup.Items.FindByValue(BranchID) != null)
                                            //       {
                                            //           ddlLegalEntityPopPup.Items.FindByValue(BranchID).Selected = true;
                                            //       }                                                                                                     
                                            //   });
                                            //}
                                            //ddlLegalEntityPopPup_SelectedIndexChanged(null, null);
                                            //if (ddlSubEntity1PopPup.Items.Count > 0)
                                            //{
                                            //    Branchlist.ForEach(entry =>
                                            //    {
                                            //        string BranchID = Convert.ToString(entry);
                                            //        if (ddlSubEntity1PopPup.Items.FindByValue(BranchID) != null)
                                            //        {
                                            //            ddlSubEntity1PopPup.Items.FindByValue(BranchID).Selected = true;
                                            //        }
                                            //    });
                                            //}
                                            //ddlSubEntity1PopPup_SelectedIndexChanged(null, null);
                                            //if (ddlSubEntity2PopPup.Items.Count > 0)
                                            //{
                                            //    Branchlist.ForEach(entry =>
                                            //    {
                                            //        string BranchID = Convert.ToString(entry);
                                            //        if (ddlSubEntity2PopPup.Items.FindByValue(BranchID) != null)
                                            //        {
                                            //            ddlSubEntity2PopPup.Items.FindByValue(BranchID).Selected = true;
                                            //        }
                                            //    });
                                            //}
                                            //ddlSubEntity2PopPup_SelectedIndexChanged(null, null);
                                            //if (ddlSubEntity3PopPup.Items.Count > 0)
                                            //{
                                            //    Branchlist.ForEach(entry =>
                                            //    {
                                            //        string BranchID = Convert.ToString(entry);
                                            //        if (ddlSubEntity3PopPup.Items.FindByValue(BranchID) != null)
                                            //        {
                                            //            ddlSubEntity3PopPup.Items.FindByValue(BranchID).Selected = true;
                                            //        }
                                            //    });
                                            //}
                                            //ddlSubEntity3PopPup_SelectedIndexChanged(null, null);
                                            //if (ddlFilterLocationPopPup.Items.Count > 0)
                                            //{
                                            //    Branchlist.ForEach(entry =>
                                            //    {
                                            //        string BranchID = Convert.ToString(entry);
                                            //        if (ddlFilterLocationPopPup.Items.FindByValue(BranchID) != null)
                                            //        {
                                            //            ddlFilterLocationPopPup.Items.FindByValue(BranchID).Selected = true;
                                            //        }
                                            //    });
                                            //}
                                            //ddlFilterLocationPopPup_SelectedIndexChanged(null, null);
                                            if (ddlVertical.Items.Count > 0)
                                            {
                                                string VID = Convert.ToString(ResultID.VerticalID);
                                                if (ddlVertical.Items.FindByValue(VID) != null)
                                                {
                                                    ddlVertical.Items.FindByValue(VID).Selected = true;
                                                }
                                            }
                                            if (ddlFilterFinancialPopPup.Items.Count > 0)
                                            {
                                                string FY = Convert.ToString(ResultID.FinancialYear);
                                                if (ddlFilterFinancialPopPup.Items.FindByText(FY) != null)
                                                {
                                                    ddlFilterFinancialPopPup.Items.FindByText(FY).Selected = true;
                                                }
                                            }
                                            if (ddlAuditBackground.Items.Count > 0)
                                            {
                                                string AuditBackID = Convert.ToString(ResultID.AuditBackgroundID);
                                                if (ddlAuditBackground.Items.FindByValue(AuditBackID) != null)
                                                {
                                                    ddlAuditBackground.Items.FindByValue(AuditBackID).Selected = true;
                                                }
                                            }
                                            tbxReportDate.Text = ResultID.DateOfIssueOfReport != null ? ResultID.DateOfIssueOfReport.Value.ToString("dd-MM-yyyy") : "";
                                            //tbxReportDate.Text = Convert.ToString(ResultID.DateOfIssueOfReport).ToString("dd-MM-yyyy");
                                            tbxIssueBy.Text = ResultID.IssuedByVendor;
                                            tbxRemark.Text = ResultID.Remark;
                                            tbxTitle.Text = ResultID.TitleOfReport;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }

        public void SetValuesToLocationFilters(int BranchID)
        {
            try
            {
                var LocationList = CustomerBranchManagementRisk.GetLocationHierarchy(BranchID);

                int FwdPTR = 0;
                int RevPTR = LocationList.Count;

                if (LocationList.Count > 0)
                {
                    while (hasIndex(FwdPTR, LocationList))
                    {
                        if (FwdPTR == 0)
                        {
                            ddlLegalEntityPopPup.ClearSelection();
                            ddlLegalEntityPopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                            ddlLegalEntityPopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                            // ddlLegalEntityPopPup.Enabled = false;
                            BindSubEntityData(ddlSubEntity1PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                        }
                        else if (FwdPTR == 1)
                        {
                            ddlSubEntity1PopPup.ClearSelection();
                            ddlSubEntity1PopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                            ddlSubEntity1PopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                            //ddlSubEntity1PopPup.Enabled = false;
                            BindSubEntityData(ddlSubEntity2PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                        }
                        else if (FwdPTR == 2)
                        {
                            ddlSubEntity2PopPup.ClearSelection();
                            ddlSubEntity2PopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                            ddlSubEntity2PopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                            // ddlSubEntity2PopPup.Enabled = false;
                            BindSubEntityData(ddlSubEntity3PopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                        }
                        else if (FwdPTR == 3)
                        {
                            ddlSubEntity3PopPup.ClearSelection();
                            ddlSubEntity3PopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                            ddlSubEntity3PopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                            // ddlSubEntity3PopPup.Enabled = false;
                            BindSubEntityData(ddlFilterLocationPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                        }
                        else if (FwdPTR == 4)
                        {
                            ddlFilterLocationPopPup.ClearSelection();
                            ddlFilterLocationPopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                            ddlFilterLocationPopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                            // ddlFilterLocationPopPup.Enabled = false;
                        }

                        FwdPTR++;
                        RevPTR--;
                    };
                }
            }
            catch (Exception ex)
            {
                Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public Boolean hasIndex(int index, List<int> list)
        {
            if (index < list.Count)
                return true;
            return false;
        }
        public void BindAuditBackground()
        {
            int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Result = (from row in entities.AuditBackgrounds
                              where row.CustomerID == CustomerID
                              && row.IsDeleted == false
                              select row).ToList();

                var query = (from row in Result
                             select new { ID = row.ID, AuditBackground = row.AuditBackground1 }).OrderBy(entry => entry.AuditBackground).ToList<object>();

                ddlAuditBackground.DataTextField = "AuditBackground";
                ddlAuditBackground.DataValueField = "ID";
                ddlAuditBackground.DataSource = query;
                ddlAuditBackground.DataBind();
            }
        }
        private void bindVertical(Saplin.Controls.DropDownCheckBoxes ddlVertical, int? BranchId)
        {
            ddlVertical.DataTextField = "VerticalName";
            ddlVertical.DataValueField = "VerticalsId";
            ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
            ddlVertical.DataBind();
        }
        public void BindLegalEntityData(DropDownList PopFDRP)
        {
            int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            PopFDRP.DataTextField = "Name";
            PopFDRP.DataValueField = "ID";
            PopFDRP.Items.Clear();
            if (DepartmentHead)
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }

            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(customerID, userID);
            }
            else
            {
                PopFDRP.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            }
            PopFDRP.DataBind();
        }
        public void BindFnancialYear(DropDownList PopFDRP)
        {
            var details = UserManagementRisk.FillFnancialYear();
            if (details != null)
            {
                PopFDRP.DataTextField = "Name";
                PopFDRP.DataValueField = "ID";
                PopFDRP.Items.Clear();
                PopFDRP.DataSource = details;
                PopFDRP.DataBind();
                PopFDRP.Items.Insert(0, new ListItem("Financial Year", "-1"));
            }

        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            int customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(userID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(userID, customerID, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(userID, customerID, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        public static void CreateAuditObservation(PastAuditDocumentMapping auditsampleform)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.PastAuditDocumentMappings.Add(auditsampleform);
                entities.SaveChanges();
            }
        }
        public static int GetAuditObservationCount(long CustomerBranchID, long VerticalID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditSampleFormToUpdate = (from row in entities.AuditObservations
                                               where row.CustomerBranchID == CustomerBranchID &&
                                               row.VerticalID == VerticalID
                                               && row.FinancialYear == FinancialYear
                                               select row.Id).ToList().Count;
                if (AuditSampleFormToUpdate != null)
                {
                    return AuditSampleFormToUpdate;
                }
                else
                {
                    return 0;
                }
            }
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }
        protected void ddlLegalEntityPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
            {
                if (ddlLegalEntityPopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    this.bindVertical(ddlVertical, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlSubEntity1PopPup.Items.Count > 0)
                    ddlSubEntity1PopPup.Items.Clear();

                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.Items.Clear();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.Items.Clear();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.Items.Clear();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        protected void ddlSubEntity1PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
            {
                if (ddlSubEntity1PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2PopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    this.bindVertical(ddlVertical, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.ClearSelection();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        protected void ddlSubEntity2PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
            {
                if (ddlSubEntity2PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3PopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    this.bindVertical(ddlVertical, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        protected void ddlSubEntity3PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
            {
                if (ddlSubEntity3PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocationPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    this.bindVertical(ddlVertical, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        protected void ddlFilterLocationPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
            {
                //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue));
                this.bindVertical(ddlVertical, Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue));
                BindVersionDetails();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        protected void ddlFilterFinancialPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVersionDetails();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        protected void ddlVerticalIDPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVersionDetails();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
        private void BindVersionDetails()
        {
            try
            {
                List<int> VerticalIdS = new List<int>();
                int PastAuditDocumentID = -1;
                int AuditBackgroundID = -1;
                string FinancialYear = string.Empty;
                long VerticalId = -1;
                int CustomerBranchId = -1;
                long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
                VerticalIdS.Clear();
                if (!String.IsNullOrEmpty(Request.QueryString["PastAuditDocID"]))
                {
                    if (Convert.ToString(Request.QueryString["PastAuditDocID"]) != "undefined")
                    {
                        PastAuditDocumentID = Convert.ToInt32(Request.QueryString["PastAuditDocID"]);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(CustomerID);
                    if (vid != -1)
                    {
                        VerticalIdS.Add(vid);
                    }
                }
                else
                {
                    for (int i = 0; i < ddlVertical.Items.Count; i++)
                    {
                        if (ddlVertical.Items[i].Selected)
                        {
                            VerticalIdS.Add(Convert.ToInt32(ddlVertical.Items[i].Value));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                {
                    if (ddlLegalEntityPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                {
                    if (ddlSubEntity1PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                {
                    if (ddlSubEntity2PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                {
                    if (ddlSubEntity3PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                {
                    if (ddlFilterLocationPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterFinancialPopPup.SelectedValue))
                {
                    if (ddlFilterFinancialPopPup.SelectedValue != "-1")
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialPopPup.SelectedItem.Text);
                    }
                }
                if (!String.IsNullOrEmpty(ddlAuditBackground.SelectedValue))
                {
                    if (ddlAuditBackground.SelectedValue != "-1")
                    {
                        AuditBackgroundID = Convert.ToInt32(ddlAuditBackground.SelectedValue);
                    }
                }
                if (CustomerBranchId != -1 && VerticalIdS.Count > 0 && !string.IsNullOrEmpty(FinancialYear) && AuditBackgroundID != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        List<SP_BindPastAuditDocumentFileMapping_Result> riskassignmentexport = new List<SP_BindPastAuditDocumentFileMapping_Result>();

                        var ResultID = (from row in entities.Past_Audit_Documents
                                        where row.CustomerBranch == CustomerBranchId
                                        && VerticalIdS.Contains(row.VerticalID)
                                        && row.FinancialYear == FinancialYear
                                        && row.AuditBackgroundID == AuditBackgroundID
                                        select row.ID).FirstOrDefault();

                        riskassignmentexport = (from C in entities.SP_BindPastAuditDocumentFileMapping(CustomerID, ResultID)
                                                select C).ToList();

                        if (riskassignmentexport.Count > 0)
                        {
                            grdVersionDisplayDownload.DataSource = riskassignmentexport;
                            grdVersionDisplayDownload.DataBind();
                            upCompliance.Update();
                        }
                        else
                        {
                            grdVersionDisplayDownload.DataSource = null;
                            grdVersionDisplayDownload.DataBind();
                            upCompliance.Update();
                        }
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        if (PastAuditDocumentID != -1)
                        {
                            var riskassignmentexport = (from C in entities.SP_BindPastAuditDocumentFileMapping(CustomerID, PastAuditDocumentID)
                                                        select C).ToList();

                            grdVersionDisplayDownload.DataSource = riskassignmentexport;
                            grdVersionDisplayDownload.DataBind();
                            upCompliance.Update();

                            grdVersionDisplayDownload.DataSource = riskassignmentexport;
                            grdVersionDisplayDownload.DataBind();
                            upCompliance.Update();
                        }
                        else
                        {
                            grdVersionDisplayDownload.DataSource = null;
                            grdVersionDisplayDownload.DataBind();
                            upCompliance.Update();
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<PastAuditDocumentMapping> GetAuditGridVersionDisplay(long BranchID, long VerticalID, string FinancialYear, int AuditBackgroundID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<PastAuditDocumentMapping> riskassignmentexport = new List<PastAuditDocumentMapping>();

                var ResultID = (from row in entities.Past_Audit_Documents
                                where row.CustomerBranch == BranchID
                                && row.VerticalID == VerticalID
                                && row.FinancialYear == FinancialYear
                                && row.AuditBackgroundID == AuditBackgroundID
                                select row.ID).FirstOrDefault();

                riskassignmentexport = (from C in entities.PastAuditDocumentMappings
                                        where C.PastAuditDocID == ResultID
                                        select C).ToList();

                return riskassignmentexport;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string Type = string.Empty;
                int PastAuditDocIDEdit = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Convert.ToString(Request.QueryString["Type"]) != "undefined")
                    {
                        Type = Convert.ToString(Request.QueryString["Type"]);
                    }
                }
                if (!String.IsNullOrEmpty(Request.QueryString["PastAuditDocID"]))
                {
                    if (Convert.ToString(Request.QueryString["PastAuditDocID"]) != "undefined")
                    {
                        PastAuditDocIDEdit = Convert.ToInt32(Request.QueryString["PastAuditDocID"]);
                    }
                }
                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                List<int> VerticalIdSave = new List<int>();
                VerticalIdSave.Clear();
                string FinancialYear = "";
                int CustomerBranchId = -1;
                int versioncount = 0;
                int AuditBackgroundID = -1;
                int PastAuditDocID = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                {
                    if (ddlLegalEntityPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                {
                    if (ddlSubEntity1PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                {
                    if (ddlSubEntity2PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                {
                    if (ddlSubEntity3PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                {
                    if (ddlFilterLocationPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterFinancialPopPup.SelectedItem.Text))
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialPopPup.SelectedItem.Text);
                }
                if (!string.IsNullOrEmpty(ddlAuditBackground.SelectedValue))
                {
                    if (ddlAuditBackground.SelectedValue != "-1")
                    {
                        AuditBackgroundID = Convert.ToInt32(ddlAuditBackground.SelectedValue);
                    }
                }

                Past_Audit_Documents objPast_Audit_Documents = new Past_Audit_Documents();
                objPast_Audit_Documents.CustomerID = CustomerID;
                objPast_Audit_Documents.CustomerBranch = CustomerBranchId;
                objPast_Audit_Documents.FinancialYear = FinancialYear;
                objPast_Audit_Documents.Period = null;
                objPast_Audit_Documents.AuditBackgroundID = AuditBackgroundID;
                objPast_Audit_Documents.Isdeleted = false;
                objPast_Audit_Documents.CreatedBy = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                objPast_Audit_Documents.CreatedOn = DateTime.Now;
                objPast_Audit_Documents.TitleOfReport = tbxTitle.Text.Trim();
                if (!string.IsNullOrEmpty(tbxReportDate.Text.Trim()))
                {
                    if (tbxReportDate.Text.Trim() != "")
                    {
                        objPast_Audit_Documents.DateOfIssueOfReport = Convert.ToDateTime(tbxReportDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                }
                objPast_Audit_Documents.IssuedByVendor = tbxIssueBy.Text;
                objPast_Audit_Documents.Remark = tbxRemark.Text;

                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(CustomerID);
                    if (vid != -1)
                    {
                        VerticalIdSave.Add(vid);
                    }
                }
                else
                {
                    for (int i = 0; i < ddlVertical.Items.Count; i++)
                    {
                        if (ddlVertical.Items[i].Selected)
                        {
                            VerticalIdSave.Add(Convert.ToInt32(ddlVertical.Items[i].Value));
                        }
                    }
                }
                if (VerticalIdSave.Count > 0)
                {
                    foreach (var VerticalIditem in VerticalIdSave)
                    {
                        int VerticalId = Convert.ToInt32(VerticalIditem);
                        objPast_Audit_Documents.VerticalID = VerticalId;
                        if (Type != "0")
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            if (FileUploadObservation.HasFile)
                            {
                                string directoryPath = "";
                                if (!string.IsNullOrEmpty(FinancialYear) && CustomerBranchId != -1 && VerticalId != -1 && AuditBackgroundID != -1)
                                {
                                    int count = GetAuditObservationCount(CustomerBranchId, VerticalId, FinancialYear);//Agaist that field unique id created
                                    versioncount = count + 1;
                                    directoryPath = Server.MapPath("~/PastAuditDocuments/" + CustomerBranchId.ToString() + "/" + FinancialYear.ToString());
                                    AuditControlEntities entities = new AuditControlEntities();
                                    var ResultID = (from row in entities.Past_Audit_Documents
                                                    where row.CustomerBranch == CustomerBranchId
                                                    && row.VerticalID == VerticalId
                                                    && row.FinancialYear == FinancialYear
                                                    && row.AuditBackgroundID == AuditBackgroundID
                                                    && row.CustomerID == CustomerID
                                                    select row.ID).FirstOrDefault();
                                    if (ResultID > 0)
                                    {
                                        if (ResultID == PastAuditDocIDEdit)
                                        {
                                            if (Type == "0")
                                            {
                                                var UpdateEdited = (from row in entities.Past_Audit_Documents
                                                                    where row.ID == PastAuditDocIDEdit
                                                                    select row).FirstOrDefault();

                                                UpdateEdited.TitleOfReport = objPast_Audit_Documents.TitleOfReport;
                                                UpdateEdited.DateOfIssueOfReport = objPast_Audit_Documents.DateOfIssueOfReport;
                                                UpdateEdited.IssuedByVendor = objPast_Audit_Documents.IssuedByVendor;
                                                UpdateEdited.Remark = objPast_Audit_Documents.Remark;
                                                entities.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "This selection is already exists, please select any other fields to proceed";
                                        }
                                        PastAuditDocID = ResultID;
                                    }
                                    else
                                    {
                                        if (Type == "1")
                                        {
                                            //Unique ID Created against CustomerBranchId,VerticalId,FinancialYear,AuditBackgroundID                                                                      
                                            entities.Past_Audit_Documents.Add(objPast_Audit_Documents);
                                            entities.SaveChanges();
                                            PastAuditDocID = objPast_Audit_Documents.ID;
                                        }
                                        else
                                        {
                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "This selection is already exists, please select any other fields to proceed";
                                        }
                                    }
                                }
                                if (PastAuditDocID != -1)
                                {
                                    if (directoryPath != "")
                                    {
                                        if (FileUploadObservation.HasFile)
                                        {
                                            HttpFileCollection fileCollection1 = Request.Files;
                                            if (fileCollection1.Count > 0)
                                            {
                                                DocumentManagement.CreateDirectory(directoryPath);
                                                for (int i = 0; i < fileCollection1.Count; i++)
                                                {
                                                    Guid fileKey = Guid.NewGuid();
                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(fileCollection1[i].FileName));
                                                    HttpPostedFile uploadfile = fileCollection1[i];
                                                    Stream fs = uploadfile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                                    if (uploadfile.ContentLength > 0)
                                                    {
                                                        using (AuditControlEntities entities = new AuditControlEntities())
                                                        {
                                                            PastAuditDocumentMapping file1 = new PastAuditDocumentMapping()
                                                            {
                                                                PastAuditDocID = PastAuditDocID,
                                                                FileName = uploadfile.FileName,
                                                                FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                                FileKey = fileKey.ToString(),
                                                                FileData = null,//FileUploadObservation.FileBytes,
                                                                VersionDate = DateTime.Now,
                                                                EnType = "A",
                                                                IsDeleted = false,
                                                            };
                                                            CreateAuditObservation(file1);
                                                            SaveDocFiles(Filelist);
                                                        }
                                                        AuditControlEntities entitie = new AuditControlEntities();
                                                        var file = (from row in entitie.Past_Audit_Documents
                                                                    where row.ID == PastAuditDocID
                                                                    select row).FirstOrDefault();
                                                        file.Isdeleted = false;
                                                        entitie.SaveChanges();
                                                    }
                                                }
                                                BindVersionDetails();
                                                CustomValidator1.IsValid = false;
                                                CustomValidator1.ErrorMessage = "Record Uploaded Successfully.";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(FinancialYear) && CustomerBranchId != -1 && VerticalId != -1 && AuditBackgroundID != -1)
                            {
                                AuditControlEntities entities = new AuditControlEntities();
                                var ResultID = (from row in entities.Past_Audit_Documents
                                                where row.CustomerBranch == CustomerBranchId
                                                && row.VerticalID == VerticalId
                                                && row.FinancialYear == FinancialYear
                                                && row.AuditBackgroundID == AuditBackgroundID
                                                && row.CustomerID == CustomerID
                                                select row.ID).FirstOrDefault();
                                if (ResultID > 0)
                                {
                                    if (ResultID == PastAuditDocIDEdit)
                                    {
                                        if (Type == "0")
                                        {
                                            var UpdateEdited = (from row in entities.Past_Audit_Documents
                                                                where row.ID == PastAuditDocIDEdit
                                                                select row).FirstOrDefault();

                                            UpdateEdited.TitleOfReport = objPast_Audit_Documents.TitleOfReport;
                                            UpdateEdited.DateOfIssueOfReport = objPast_Audit_Documents.DateOfIssueOfReport;
                                            UpdateEdited.IssuedByVendor = objPast_Audit_Documents.IssuedByVendor;
                                            UpdateEdited.Remark = objPast_Audit_Documents.Remark;
                                            entities.SaveChanges();

                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "Record Updated Successfully";
                                        }
                                    }
                                    else
                                    {
                                        CustomValidator1.IsValid = false;
                                        CustomValidator1.ErrorMessage = "This selection is already exists, please select any other fields to proceed";
                                    }
                                    PastAuditDocID = ResultID;
                                }
                                else
                                {
                                    var UpdateEdited = (from row in entities.Past_Audit_Documents
                                                        where row.ID == PastAuditDocIDEdit
                                                        select row).FirstOrDefault();

                                    UpdateEdited.CustomerBranch = objPast_Audit_Documents.CustomerBranch;
                                    UpdateEdited.FinancialYear = objPast_Audit_Documents.FinancialYear;
                                    UpdateEdited.VerticalID = objPast_Audit_Documents.VerticalID;
                                    UpdateEdited.AuditBackgroundID = objPast_Audit_Documents.AuditBackgroundID;
                                    UpdateEdited.TitleOfReport = objPast_Audit_Documents.TitleOfReport;
                                    UpdateEdited.DateOfIssueOfReport = objPast_Audit_Documents.DateOfIssueOfReport;
                                    UpdateEdited.IssuedByVendor = objPast_Audit_Documents.IssuedByVendor;
                                    UpdateEdited.Remark = objPast_Audit_Documents.Remark;
                                    entities.SaveChanges();

                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Record Updated Successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    CustomValidator1.ErrorMessage = "Plase select Vertical.";
                    CustomValidator1.IsValid = false;
                    return;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
            }
            catch (Exception ex)
            {
                Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }
        protected void grdVersionDisplayDownload_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("grdVersionDisplayDelete"))
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int FileID = Convert.ToInt32(commandArgs[0]);
                DeleteClick(FileID);
                BindVersionDetails();
            }
            if (e.CommandName.Equals("grdVersionDisplayView"))
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int FileID = Convert.ToInt32(commandArgs[0]);
                ViewClick(FileID);
            }
            if (e.CommandName.Equals("grdVersionDisplayDownload"))
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int FileID = Convert.ToInt32(commandArgs[0]);
                DownLoadClick(FileID);
            }
        }
        protected void DeleteClick(int FileID)
        {
            try
            {
                AuditControlEntities entities = new AuditControlEntities();
                var file = (from row in entities.PastAuditDocumentMappings
                            where row.ID == FileID
                            select row).FirstOrDefault();
                file.IsDeleted = true;

                entities.SaveChanges();
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "File Deleted Successfully.";
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void ViewClick(int FileID)
        {
            try
            {
                AuditControlEntities entities = new AuditControlEntities();
                var file = (from row in entities.PastAuditDocumentMappings
                            where row.ID == FileID
                            select row).FirstOrDefault();

                if (file.FilePath != null)
                {

                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                    if (filePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + FileData;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (file.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();

                        string CompDocReviewPath = FileName;
                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewREV('" + CompDocReviewPath + "');", true);

                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void DownLoadClick(int FileID)
        {
            try
            {
                AuditControlEntities entities = new AuditControlEntities();
                var file = (from row in entities.PastAuditDocumentMappings
                            where row.ID == FileID
                            select row).FirstOrDefault();

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void btnAddAuditBackground_Click(object sender, EventArgs e)
        {
            try
            {
                AuditControlEntities entities = new AuditControlEntities();
                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(tbxAuditBackground.Text.Trim()))
                {
                    var checkExist = (from row in entities.AuditBackgrounds
                                      where row.CustomerID == CustomerID
                                      && row.AuditBackground1.ToLower() == tbxAuditBackground.Text.Trim().ToLower()
                                      select row.ID).FirstOrDefault();
                    if (checkExist > 0)
                    {
                        cvAuditBackground.IsValid = false;
                        cvAuditBackground.ErrorMessage = "Audit Background Already exist.";
                    }
                    else
                    {
                        AuditBackground objAuditBackground = new AuditBackground();
                        objAuditBackground.AuditBackground1 = tbxAuditBackground.Text.Trim();
                        objAuditBackground.CustomerID = CustomerID;
                        objAuditBackground.IsDeleted = false;
                        objAuditBackground.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                        objAuditBackground.CreatedOn = DateTime.Now;
                        entities.AuditBackgrounds.Add(objAuditBackground);
                        entities.SaveChanges();
                        cvAuditBackground.IsValid = false;
                        cvAuditBackground.ErrorMessage = "Audit Background Added Successfully.";
                        tbxAuditBackground.Text = string.Empty;
                        BindAuditBackground();
                    }
                }
                else
                {
                    cvAuditBackground.IsValid = false;
                    cvAuditBackground.ErrorMessage = "Please Enter Audit Background.";
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
            }
            catch (Exception ex)
            {
                Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAuditBackground.IsValid = false;
                cvAuditBackground.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(int customerid, Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(UserID));
            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(UserID));
            List<AuditManagerClass> query;
            if (DepartmentHead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            var subEntities = query.ToList().Select(entry => new Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void ddlAuditBackground_SelectedIndexChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "applyclass", "BindControls();", true);
        }
    }
}