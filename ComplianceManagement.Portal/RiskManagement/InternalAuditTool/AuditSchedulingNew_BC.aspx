﻿<%@ Page Title="Audit Scheduling" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditSchedulingNew_BC.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditSchedulingNew_BC" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

        div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }

         td.locked, th.locked {
            position:relative;    
            left:expression((this.parentElement.parentElement.parentElement.parentElement.scrollLeft-2)+'px');
            }   
    </style>

      <%-- <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width:80% !important;
        }
       </style>--%>
    <script type="text/javascript">

        function fopenpopupAudit() {
            $('#divAuditSchedulingDialog').modal('show');
        }

        function fopenpopupCompl() {
            $('#divComplianceScheduleDialog').modal('show');
        }

        function fopenpopupEditaudit() {
            $('#divEDITAuditSchedulingDialog').modal('show');
        }



        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function OpenApplytoPopUp() {
            $('#DivApplyTo').modal('show');
        }

        function ApplyToconfirm() {
            var confirmed = confirm('Are you sure you want to Apply this item?', 'Are you sure you want to Apply this item?');

            if (confirmed) {
                $('#btnapply').show();
            }
            else {
                caller();
            }
        }

        function CloseEvent() {
            $('#divAuditSchedulingDialog').modal('hide');
            //window.location.reload();
        }

        function CloseWin() {
            $('#divAuditSchedulingDialog').modal('hide');
            window.location.reload();
        };

        //This is used for Close Popup after button click.
        function caller() {
            interval_A = setInterval(CloseWin, 3000);
        };

        function CloseWin1() {
            $('#DivApplyTo').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller1() {
            interval_A = setInterval(CloseWin1, 3000);
        };
    </script>
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        function func() {
            var gvcheck = document.getElementById("<%=grdAnnually.ClientID %>");
            if (ContentPlaceHolder1_grdAnnually_Annually1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function func1() {
            var gvcheck = document.getElementById("<%=grdAnnually.ClientID %>");
              if (ContentPlaceHolder1_grdAnnually_Annually2_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }
          function func2() {
              var gvcheck = document.getElementById("<%=grdAnnually.ClientID %>");
            if (ContentPlaceHolder1_grdAnnually_Annually3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for Annulay Cheakbox
        function funHalf1() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalf2() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalf3() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalf4() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalf5() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalf6() {
            var gvcheck = document.getElementById("<%=grdHalfYearly.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearly_Half6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        //for Quarterly

        function funcQuarterly1() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterly_Quarterly1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcQuarterly2() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
               if (ContentPlaceHolder1_grdQuarterly_Quarterly2_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcQuarterly3() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly3_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly4() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly4_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
         }
        function funcQuarterly5() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly5_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
         }
        function funcQuarterly6() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly6_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly7() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly7_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly8() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly8_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly9() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly9_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly10() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly10_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly11() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly11_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcQuarterly12() {
            var gvcheck = document.getElementById("<%=grdQuarterly.ClientID %>");
             if (ContentPlaceHolder1_grdQuarterly_Quarterly12_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }

        //for monthly 
        function funMonthly1() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
              if (ContentPlaceHolder1_grdMonthly_grdMonthly1_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
        }
        function funMonthly2() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly2_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly3() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly3_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly4() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly4_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly5() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly5_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly6() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly6_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly7() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly7_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funMonthly8() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly8_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funMonthly9() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly9_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly10() {           
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly10_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly11() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly11_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funMonthly12() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly12_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }

        function funMonthly13() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly13_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funMonthly14() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly14_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funMonthly15() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly15_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funMonthly16() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly16_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[16].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[16].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funMonthly17() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly17_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[17].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[17].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funMonthly18() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly18_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[18].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[18].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funMonthly19() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly19_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[19].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[19].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funMonthly20() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
               if (ContentPlaceHolder1_grdMonthly_grdMonthly20_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[20].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[20].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funMonthly21() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly21_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[21].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[21].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly22() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly22_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[22].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[22].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly23() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly23_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[23].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[23].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly24() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly24_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[24].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[24].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly25() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly25_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[25].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[25].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly26() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly26_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[26].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[26].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly27() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly27_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[27].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[27].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly28() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly28_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[28].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[28].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly29() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly29_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[29].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[29].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly30() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly30_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[30].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[30].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly31() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly31_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[31].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[31].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly32() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly32_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[32].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[32].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthly33() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly33_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[33].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[33].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        function funMonthly34() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly34_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[34].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[34].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly35() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly35_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[35].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[35].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthly36() {
            var gvcheck = document.getElementById("<%=grdMonthly.ClientID %>");
            if (ContentPlaceHolder1_grdMonthly_grdMonthly36_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[36].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[36].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }





        //
        //for edit Annulay Cheakbox
        function funHalfEdit1() {
            var gvcheck = document.getElementById("<%=grdHalfYearlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearlyEDIT_EditHalf1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalfEdit2() {
            var gvcheck = document.getElementById("<%=grdHalfYearlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearlyEDIT_EditHalf2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalfEdit3() {
            var gvcheck = document.getElementById("<%=grdHalfYearlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearlyEDIT_EditHalf3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalfEdit4() {
            var gvcheck = document.getElementById("<%=grdHalfYearlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearlyEDIT_EditHalf4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalfEdit5() {
            var gvcheck = document.getElementById("<%=grdHalfYearlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearlyEDIT_EditHalf5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funHalfEdit6() {
            var gvcheck = document.getElementById("<%=grdHalfYearlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdHalfYearlyEDIT_EditHalf6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for edit annuly

        function Editfunc() {
            var gvcheck = document.getElementById("<%=grdAnnuallyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdAnnuallyEDIT_EditAnnually1_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }

        function Editfunc1() {
              var gvcheck = document.getElementById("<%=grdAnnuallyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdAnnuallyEDIT_EditAnnually2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfunc2() {
            var gvcheck = document.getElementById("<%=grdAnnuallyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdAnnuallyEDIT_EditAnnually3_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }




        //for month edit        
        function funMonthlyEdit1() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit2() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit3() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit4() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit5() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit6() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit7() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly7_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit8() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly8_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit9() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly9_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit10() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly10_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit11() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly11_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit12() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly12_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit13() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly13_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit14() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly14_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit15() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly15_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit16() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly16_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[16].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[16].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit17() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly17_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[17].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[17].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit18() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly18_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[18].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[18].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit19() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly19_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[19].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[19].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit20() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly20_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[20].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[20].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit21() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly21_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[21].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[21].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit22() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly22_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[22].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[22].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit23() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly23_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[23].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[23].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit24() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly24_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[24].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[24].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit25() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly25_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[25].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[25].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit26() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly26_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[26].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[26].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit27() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly27_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[27].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[27].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit28() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly28_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[28].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[28].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit29() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly29_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[29].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[29].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit30() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly30_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[30].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[30].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit31() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly31_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[31].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[31].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit32() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly32_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[32].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[32].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funMonthlyEdit33() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly33_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[33].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[33].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }


        function funMonthlyEdit34() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly34_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[34].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[34].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit35() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly35_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[35].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[35].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funMonthlyEdit36() {
            var gvcheck = document.getElementById("<%=grdMonthlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdMonthlyEDIT_EditgrdMonthly36_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[36].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[36].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for  edit Quarterly

        function EditfuncQuarterly1() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function EditfuncQuarterly2() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly3() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly4() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly5() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly6() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly7() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly7_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly8() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly8_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly9() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly9_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly10() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly10_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly11() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly11_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function EditfuncQuarterly12() {
            var gvcheck = document.getElementById("<%=grdQuarterlyEDIT.ClientID %>");
            if (ContentPlaceHolder1_grdQuarterlyEDIT_EditQuarterly12_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }



        //for phase1
        function funcphase1() {
            var gvcheck = document.getElementById("<%=grdphase1.ClientID %>");
            if (ContentPlaceHolder1_grdphase1_Phase1_Phase1_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }

        function funcphase2() {
            var gvcheck = document.getElementById("<%=grdphase1.ClientID %>");
            if (ContentPlaceHolder1_grdphase1_Phase1_Phase2_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }

        function funcphase3() {
            var gvcheck = document.getElementById("<%=grdphase1.ClientID %>");
            if (ContentPlaceHolder1_grdphase1_Phase1_Phase3_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
        }

        //for pahse 2

        function funcphase21() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase1_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }

        function funcphase22() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase2_Id_0.checked) {
                      for (i = 0; i < gvcheck.rows.length; i++) {
                          gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                      }
                  }
                  else {
                      for (i = 0; i < gvcheck.rows.length; i++) {
                          gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                      }
                  }
        }

        function funcphase23() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase3_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }

        function funcphase24() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase4_Id_0.checked) {
                       for (i = 0; i < gvcheck.rows.length; i++) {
                           gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                       }
                   }
                   else {
                       for (i = 0; i < gvcheck.rows.length; i++) {
                           gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                       }
                   }
               }
        function funcphase25() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase5_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
         }
        function funcphase26() {
            var gvcheck = document.getElementById("<%=grdphase2.ClientID %>");
            if (ContentPlaceHolder1_grdphase2_Phase2_Phase6_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        //for phase 3
        function funcphase31() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase1_Id_0.checked) {
                    for (i = 0; i < gvcheck.rows.length; i++) {
                        gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                    }
                }
                else {
                    for (i = 0; i < gvcheck.rows.length; i++) {
                        gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                    }
                }
        }

        function funcphase32() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase2_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase33() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase3_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase34() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase4_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase35() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
             if (ContentPlaceHolder1_grdphase3_Phase3_Phase5_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
        }
        function funcphase36() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
             if (ContentPlaceHolder1_grdphase3_Phase3_Phase6_Id_0.checked) {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                 }
             }
             else {
                 for (i = 0; i < gvcheck.rows.length; i++) {
                     gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                 }
             }
         }

        function funcphase37() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
            if (ContentPlaceHolder1_grdphase3_Phase3_Phase7_Id_0.checked) {
                    for (i = 0; i < gvcheck.rows.length; i++) {
                        gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                    }
                }
                else {
                    for (i = 0; i < gvcheck.rows.length; i++) {
                        gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                    }
                }
        }

        function funcphase38() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
              if (ContentPlaceHolder1_grdphase3_Phase3_Phase8_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
        }

        function funcphase39() {
            var gvcheck = document.getElementById("<%=grdphase3.ClientID %>");
              if (ContentPlaceHolder1_grdphase3_Phase3_Phase9_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }

        //for phase 4

        function funcphase41() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
            if (ContentPlaceHolder1_grdphase4_Phase4_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function funcphase42() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase2_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funcphase43() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase3_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase44() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase4_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funcphase45() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase5_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase46() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase6_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase47() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase7_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase48() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase8_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase49() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase9_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }

        function funcphase410() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase10_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase411() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase11_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }

        function funcphase412() {
            var gvcheck = document.getElementById("<%=grdphase4.ClientID %>");
               if (ContentPlaceHolder1_grdphase4_Phase4_Phase12_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }

        //for pahse 5
        function funcphase51() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase1_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }
        function funcphase52() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase2_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase53() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase3_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funcphase54() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase4_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funcphase55() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase5_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase56() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase6_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase57() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase7_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase58() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase8_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase59() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase9_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
        }
        function funcphase510() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
               if (ContentPlaceHolder1_grdphase5_Phase5_Phase10_Id_0.checked) {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                   }
               }
               else {
                   for (i = 0; i < gvcheck.rows.length; i++) {
                       gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                   }
               }
           }
        function funcphase511() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase11_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase512() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase12_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase513() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase13_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase514() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase14_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function funcphase515() {
            var gvcheck = document.getElementById("<%=grdphase5.ClientID %>");
            if (ContentPlaceHolder1_grdphase5_Phase5_Phase15_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for pahse 1 edit
        function Editfuncphase1() {
            var gvcheck = document.getElementById("<%=grdphase1EDIT.ClientID %>");
              if (ContentPlaceHolder1_grdphase1EDIT_EditPhase1_Phase1_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }

          function Editfuncphase2() {
              var gvcheck = document.getElementById("<%=grdphase1EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase1EDIT_EditPhase1_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase3() {
            var gvcheck = document.getElementById("<%=grdphase1EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase1EDIT_EditPhase1_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for ohase 2 edit
        function Editfuncphase21() {
            var gvcheck = document.getElementById("<%=grdphase2EDIT.ClientID %>");
              if (ContentPlaceHolder1_grdphase2EDIT_EditPhase2_Phase1_Id_0.checked) {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                  }
              }
              else {
                  for (i = 0; i < gvcheck.rows.length; i++) {
                      gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                  }
              }
          }

          function Editfuncphase22() {
              var gvcheck = document.getElementById("<%=grdphase2EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase2EDIT_EditPhase2_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase23() {
            var gvcheck = document.getElementById("<%=grdphase2EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase2EDIT_EditPhase2_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase24() {
            var gvcheck = document.getElementById("<%=grdphase2EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase2EDIT_EditPhase2_Phase4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase25() {
            var gvcheck = document.getElementById("<%=grdphase2EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase2EDIT_EditPhase2_Phase5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase26() {
            var gvcheck = document.getElementById("<%=grdphase2EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase2EDIT_EditPhase2_Phase6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        //for phase 3 Edit
        function Editfuncphase31() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase32() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase33() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase34() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase35() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase36() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase37() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase7_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase38() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase8_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase39() {
            var gvcheck = document.getElementById("<%=grdphase3EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase3EDIT_EditPhase3_Phase9_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        //for phase 4 edit

        function Editfuncphase41Eidt() {
          
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase42Edit() {

            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase43() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase44Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase45Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase46Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase47Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase7_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase48Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase8_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase49Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase9_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase410Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase10_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase411Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase11_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        function Editfuncphase412Edit() {
            var gvcheck = document.getElementById("<%=grdphase4EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase4EDIT_EditPhase4_Phase12_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }

        //for pahse 5 Edit
        function Editfuncphase51() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase1_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[1].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase52() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase2_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[2].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase53() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase3_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[3].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase54() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase4_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[4].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase55() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase5_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[5].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase56() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase6_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[6].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase57() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase7_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[7].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase58() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase8_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[8].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase59() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase9_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[9].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase510() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase10_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[10].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase511() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase11_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[11].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase512() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase12_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[12].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase513() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase13_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[13].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase514() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase14_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[14].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }
        function Editfuncphase515() {
            var gvcheck = document.getElementById("<%=grdphase5EDIT.ClientID %>");
            if (ContentPlaceHolder1_grdphase5EDIT_EditPhase5_Phase15_Id_0.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = true;
                }
            }
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[15].getElementsByTagName("INPUT")[0].checked = false;
                }
            }
        }



        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }
        function BindControls() {

            $(function () {
                $('input[id*=txtExpectedStartDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });

            $(function () {
                $('input[id*=txtExpectedEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });


            $(function () {
                $('input[id*=txtStartDatePop]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
            $(function () {
                $('input[id*=txtEndDatePop]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('Audit Scheduling');
            fhead('Audit Scheduling');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upCompliancesList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                            <div class="col-md-12 colpadding0">
                                 <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                                      ValidationGroup="ComplianceInstanceValidationGroup" />                                  

                                       <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />

                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" Selected="True" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>

                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                      </div> 

                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                      </div> 

                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float:right">
                                       <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" CssClass="btn btn-primary" 
                                           OnClientClick="fopenpopupAudit()"  OnClick="btnAddCompliance_Click" Visible="true" />
                                   </div>  
                              </div>
                             <div class="clearfix"></div> 
                             <div class="col-md-12 colpadding0"> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                     DataPlaceHolder="Unit"   OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator1"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                                    
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                     DataPlaceHolder="Sub Unit"    OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select ." ControlToValidate="ddlSubEntity1" ID="rfvProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                      DataPlaceHolder="Sub Unit"   OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubEntity2" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>
                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                       DataPlaceHolder="Sub Unit"   OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>
                            </div>                  
                            <div class="col-md-12 colpadding0"> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                        OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged" DataPlaceHolder="Sub Unit">
                                    </asp:DropDownListChosen>                                  
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                  DataPlaceHolder="Financial Year"  AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFinancialYear_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true" 
                                    OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged"
                                 class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                </asp:DropDownListChosen>
                            </div>
                            </div>                                                                                                                              
                                <div class="clearfix"></div>                                                           
                        <div class="clearfix"></div>
                        <div style="margin-bottom: 4px">
                            <asp:GridView runat="server" ID="grdAuditScheduling" AutoGenerateColumns="false" OnRowDataBound="grdAuditScheduling_RowDataBound" ShowHeaderWhenEmpty="true"
                                OnSorting="grdAuditScheduling_Sorting" GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                OnPageIndexChanging="grdAuditScheduling_PageIndexChanging" OnRowCommand="grdAuditScheduling_RowCommand1">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                <asp:Label runat="server" Text='<%# ShowCustomerBranchName((long)Eval("CustomerBranchId")) %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField  HeaderText="Vertical">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAssigned" runat="server" Text='<%# Eval("VerticalName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Financial Year">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                <%--<asp:Label runat="server" Text='<%# ShowProcessName((long)Eval("ProcessId")) %>'  ToolTip='<%# Eval("ProcessId") %>'></asp:Label>--%>
                                                <asp:Label runat="server" Text='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Scheduling">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label runat="server" Text='<%# ShowStatus((string)Eval("ISAHQMP")) %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label runat="server" Text='<%# Eval("TermName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="Action">
                                        <ItemTemplate>
                                            <%--  <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ProcessId") + "," + Eval("ISAHQMP") +"," + Eval("CustomerBranchId") %>'><img src="../../Images/package_icon.png"  title="Display Schedule Information" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SCHEDULE" CommandArgument='<%# Eval("ProcessId") + "," + Eval("ISAHQMP") +"," + Eval("CustomerBranchId") %>' OnClientClick="return confirm('Are you certain you want to delete this Scheduling Details?');"><img src="../../Images/delete_icon.png" alt="Delete Scheduling Details" title="Delete Scheduling Details" /></asp:LinkButton>--%>

                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" OnClientClick="fopenpopupCompl()" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName")+","+ Eval("VerticalID") %>'><img src="../../Images/view-icon-new.png" data-toggle="tooltip" data-placement="bottom"  title="Display Schedule Information" /></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_SCHEDULE" OnClientClick="fopenpopupEditaudit()" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName") +","+ Eval("PhaseCount")+","+ Eval("VerticalID") %>'><img src="../../Images/edit_icon_new.png" data-toggle="tooltip" data-placement="bottom"  title="Edit Schedule Information" /></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SCHEDULE" ValidationGroup="ComplianceValidationGroup1" CommandArgument='<%# Eval("ISAHQMP") +"," + Eval("FinancialYear") +"," + Eval("CustomerBranchId") +"," + Eval("TermName")+","+ Eval("VerticalID") %>' OnClientClick="return confirm('Are you certain you want to delete this Scheduling Details?');"><img data-toggle="tooltip" data-placement="bottom"  src="../../Images/delete_icon_new.png" alt="Delete Scheduling Details" title="Delete Scheduling Details" /></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="GetStartDateAndEnddate" Visible="false">
                                        <ItemTemplate>        
                                            <asp:Label runat="server" ID="lblEDITGridStartDate"  Text='<%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MM-yyyy"):""%>'></asp:Label>
                                            <asp:Label runat="server" ID="lblEDITGridEndDate"   Text='<%# Eval("EndDate")!= null?((DateTime)Eval("EndDate")).ToString("dd-MM-yyyy"):""%>'></asp:Label>                                           
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                   <PagerSettings Visible="false" />       
                                <PagerTemplate>
                               <%--     <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                           <%-- <asp:Button Text="Save" runat="server" ID="btnSaveMainGrid" CssClass="btn btn-primary" OnClick="btnSaveMainGrid_Click" />--%>
                                            <%--<asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>--%>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                          <%--  <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                    </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <div class="modal fade" id="divAuditSchedulingDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 80%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="divAuditorDialog">
                            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
                                <ContentTemplate>
                                    <div>
                                        <div style="margin-bottom: 4px">
                                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="PopupValidationSummary" />
                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                ValidationGroup="PopupValidationSummary" Display="none" class="alert alert-block alert-danger fade in" />
                                            <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                        </div>

                                        <div class="col-md-12 colpadding0">
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen ID="ddlLegalEntityPop" runat="server" AutoPostBack="true" DataPlaceHolder="Unit"
                                                    class="form-control m-bot15" Width="90%" Height="32px" OnSelectedIndexChanged="ddlLegalEntityPop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator2"
                                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen ID="ddlSubEntity1Pop" runat="server" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Sub Unit"
                                                    Width="90%" Height="32px" OnSelectedIndexChanged="ddlSubEntity1Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlSubEntity1" ID="RequiredFieldValidator3"
                                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen ID="ddlSubEntity2Pop" runat="server" AutoPostBack="true" class="form-control m-bot15"
                                                     Width="90%" Height="32px" DataPlaceHolder="Sub Unit"
                                                    OnSelectedIndexChanged="ddlSubEntity2Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubEntity2" ID="RequiredFieldValidator4"
                                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Pop" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Sub Unit"
                                                    Width="90%" Height="32px" OnSelectedIndexChanged="ddlSubEntity3Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                                <asp:ValidationSummary ID="ValidationSummary5" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                                <asp:CustomValidator ID="CustomValidator5" runat="server" EnableClientScript="true"
                                                    ValidationGroup="ComplianceInstanceValidationGroup" />
                                                <asp:Label ID="Label3" runat="server" Style="color: Red"></asp:Label>
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    &nbsp;
                                                </label>
                                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity4Pop" AutoPostBack="true" DataPlaceHolder="Sub Unit"
                                                    class="form-control m-bot15" Width="90%" Height="32px" OnSelectedIndexChanged="ddlSubEntity4Pop_SelectedIndexChanged">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *
                                                </label>
                                                <asp:TextBox runat="server" ID="txtStartDatePop" class="form-control" Style="width: 90%;" placeholder="Start Date" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Actual Start Date can not be empty."
                                                    ControlToValidate="txtStartDatePop"
                                                    runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *
                                                </label>
                                                <asp:TextBox runat="server" ID="txtEndDatePop" class="form-control" Style="width: 90%" placeholder="End Date" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Expected End Date can not be empty."
                                                    ControlToValidate="txtEndDatePop"
                                                    runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                         
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlVerticalListPopup" DataPlaceHolder="Verticals" AutoPostBack="true"
                                                    class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                                </asp:DropDownListChosen>

                                              <%--  <asp:DropDownCheckBoxes ID="ddlVerticalBranch" runat="server" AutoPostBack="true" Visible="true" CssClass="form-control m-bot15"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True" Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                                <Style SelectBoxWidth="320" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="130" />
                                                <Texts SelectBoxCaption="Select Vertical" />
                                                </asp:DropDownCheckBoxes>--%>

                                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Vertical ID."
                                                    ControlToValidate="ddlVerticalListPopup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="PopupValidationSummary" Display="None" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Vertical ID."
                                                    ControlToValidate="ddlVerticalListPopup"
                                                    runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>

                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                                    OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" AutoPostBack="true"
                                                    DataPlaceHolder="Financial Year">
                                                </asp:DropDownListChosen>
                                                <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Financial Year."
                                                    ControlToValidate="ddlFinancialYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" DataPlaceHolder="Scheduling Type"
                                                    OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                                    class="form-control m-bot15" Width="90%" Height="32px">
                                                </asp:DropDownListChosen>
                                                <asp:CompareValidator ID="CompareValidator16" ErrorMessage="Please Select Scheduling Type."
                                                    ControlToValidate="ddlSchedulingType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                    ValidationGroup="PopupValidationSummary" Display="None" />
                                            </div>
                                            <div id="Divnophase" class="col-md-3 colpadding0 entrycount" runat="server" visible="false" style="margin-top: 5px; width: 20%;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                    *</label>
                                                <asp:TextBox runat="server" ID="txtNoOfPhases" PlaceHolder="Nos. of Phases" class="form-control" Style="width: 90%;" AutoPostBack="true" OnTextChanged="txtNoOfPhases_TextChanged" />
                                            </div>                                            
                                        </div>
                                        <div class="clearfix"></div>                                            

                                        <div style="margin-bottom: 7px">
                                            <span style="color: #333;">Schedule Details</span>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div style="margin-left: 20px; background: white; border: 1px solid gray; height: 263px; width: 95%; margin-bottom: 7px;">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="a1">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlAnnually" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdAnnually" AutoGenerateColumns="false" OnRowDataBound="grdAnnually_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            DataKeyNames="ID" OnPageIndexChanging="grdAnnually_PageIndexChanging">
                                                            <%--OnRowCreated="grdAnnually_RowCreated"--%>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Annually">
                                                                    <ItemTemplate>
                                                                        <%--   <asp:Label runat="server" Text='<%# Eval("Annualy1") %>' ToolTip='<%# Eval("Annualy1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkAnnualy1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Annually">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Annualy2") %>' ToolTip='<%# Eval("Annualy2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkAnnualy2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Annually">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Annualy3") %>' ToolTip='<%# Eval("Annualy3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkAnnualy3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
                                                <ContentTemplate>
                                                    <asp:Panel runat="server" ID="pnlHalfYearly" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <%--GridLines="Vertical" PageSize="5" AllowPaging="true"--%>
                                                        <asp:GridView runat="server" ID="grdHalfYearly" AutoGenerateColumns="false"
                                                            GridLines="None" AutoPostBack="true" CssClass="table"
                                                            Width="100%" AllowSorting="true" OnRowDataBound="grdHalfYearly_RowDataBound"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdHalfYearly_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Sep">
                                                                    <ItemTemplate>
                                                                        <%-- <asp:Label runat="server" Text='<%# Eval("Halfyearly1") %>' ToolTip='<%# Eval("Halfyearly1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkHalfyearly1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly2" runat="server" />
                                                                        <%-- <asp:Label runat="server" Text='<%# Eval("Halfyearly2") %>' ToolTip='<%# Eval("Halfyearly2") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Sep">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly3" runat="server" />
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Halfyearly3") %>' ToolTip='<%# Eval("Halfyearly3") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly4" runat="server" />
                                                                        <%--   <asp:Label runat="server" Text='<%# Eval("Halfyearly4") %>' ToolTip='<%# Eval("Halfyearly4") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Sep">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly5" runat="server" />
                                                                        <%-- <asp:Label runat="server" Text='<%# Eval("Halfyearly5") %>' ToolTip='<%# Eval("Halfyearly5") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly6" runat="server" />
                                                                        <%--   <asp:Label runat="server" Text='<%# Eval("Halfyearly6") %>' ToolTip='<%# Eval("Halfyearly6") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel3">
                                                <ContentTemplate>
                                                    <%--    PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlQuarterly" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdQuarterly" AutoGenerateColumns="false" OnRowDataBound="grdQuarterly_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdQuarterly_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Jun">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter1") %>' ToolTip='<%# Eval("Quarter1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul-Sep">
                                                                    <ItemTemplate>
                                                                        <%--       <asp:Label runat="server" Text='<%# Eval("Quarter2") %>' ToolTip='<%# Eval("Quarter2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Dec">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter3") %>' ToolTip='<%# Eval("Quarter3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan-Mar">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter4") %>' ToolTip='<%# Eval("Quarter4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Apr-Jun">
                                                                    <ItemTemplate>
                                                                        <%--    <asp:Label runat="server" Text='<%# Eval("Quarter5") %>' ToolTip='<%# Eval("Quarter5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul-Sep">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter6") %>' ToolTip='<%# Eval("Quarter6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Dec">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter7") %>' ToolTip='<%# Eval("Quarter7") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan-Mar">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter8") %>' ToolTip='<%# Eval("Quarter8") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Apr-Jun">
                                                                    <ItemTemplate>
                                                                        <%--    <asp:Label runat="server" Text='<%# Eval("Quarter9") %>' ToolTip='<%# Eval("Quarter9") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul-Sep">
                                                                    <ItemTemplate>
                                                                        <%--   <asp:Label runat="server" Text='<%# Eval("Quarter10") %>' ToolTip='<%# Eval("Quarter10") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter10" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Dec">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter11") %>' ToolTip='<%# Eval("Quarter11") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter11" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan-Mar">
                                                                    <ItemTemplate>
                                                                        <%--  <asp:Label runat="server" Text='<%# Eval("Quarter12") %>' ToolTip='<%# Eval("Quarter12") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter12" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel4">
                                                <ContentTemplate>
                                                    <%-- PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlMonthly" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdMonthly" AutoGenerateColumns="false" OnRowDataBound="grdMonthly_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdMonthly_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr">
                                                                    <ItemTemplate>
                                                                      <%--  <asp:Label runat="server" Text='<%# Eval("Monthly1") %>' ToolTip='<%# Eval("Monthly1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="May">
                                                                    <ItemTemplate>
                                                                    <%--    <asp:Label runat="server" Text='<%# Eval("Monthly2") %>' ToolTip='<%# Eval("Monthly2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jun">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly3") %>' ToolTip='<%# Eval("Monthly3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly4") %>' ToolTip='<%# Eval("Monthly4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aug">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly5") %>' ToolTip='<%# Eval("Monthly5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sep">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly6") %>' ToolTip='<%# Eval("Monthly6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly7") %>' ToolTip='<%# Eval("Monthly7") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Nov">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly8") %>' ToolTip='<%# Eval("Monthly8") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Dec">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly9") %>' ToolTip='<%# Eval("Monthly9") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly10") %>' ToolTip='<%# Eval("Monthly10") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly10" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feb">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly11") %>' ToolTip='<%# Eval("Monthly11") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly11" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mar">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly12") %>' ToolTip='<%# Eval("Monthly12") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly12" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Apr">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly13") %>' ToolTip='<%# Eval("Monthly13") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly13" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="May">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly14") %>' ToolTip='<%# Eval("Monthly14") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly14" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jun">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly15") %>' ToolTip='<%# Eval("Monthly15") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly15" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly16") %>' ToolTip='<%# Eval("Monthly16") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly16" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aug">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly17") %>' ToolTip='<%# Eval("Monthly17") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly17" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sep">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly18") %>' ToolTip='<%# Eval("Monthly18") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly18" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly19") %>' ToolTip='<%# Eval("Monthly19") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly19" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Nov">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly20") %>' ToolTip='<%# Eval("Monthly20") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly20" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Dec">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly21") %>' ToolTip='<%# Eval("Monthly21") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly21" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly22") %>' ToolTip='<%# Eval("Monthly22") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly22" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feb">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly23") %>' ToolTip='<%# Eval("Monthly23") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly23" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mar">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly24") %>' ToolTip='<%# Eval("Monthly24") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly24" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly25") %>' ToolTip='<%# Eval("Monthly25") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly25" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="May">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly26") %>' ToolTip='<%# Eval("Monthly26") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly26" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jun">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly27") %>' ToolTip='<%# Eval("Monthly27") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly27" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly28") %>' ToolTip='<%# Eval("Monthly28") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly28" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aug">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly29") %>' ToolTip='<%# Eval("Monthly29") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly29" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sep">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly30") %>' ToolTip='<%# Eval("Monthly30") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly30" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly31") %>' ToolTip='<%# Eval("Monthly31") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly31" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Nov">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly32") %>' ToolTip='<%# Eval("Monthly32") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly32" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Dec">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly33") %>' ToolTip='<%# Eval("Monthly33") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly33" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly34") %>' ToolTip='<%# Eval("Monthly34") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly34" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feb">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly35") %>' ToolTip='<%# Eval("Monthly35") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly35" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mar">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly36") %>' ToolTip='<%# Eval("Monthly36") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly36" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel5">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase1" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase1" AutoGenerateColumns="false" OnRowDataBound="grdphase1_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase1_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel6">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase2" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase2" AutoGenerateColumns="false" OnRowDataBound="grdphase2_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase2_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel7">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase3" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase3" AutoGenerateColumns="false" OnRowDataBound="grdphase3_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase3_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel8">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase4" ScrollBars="Auto" Visible="false" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase4" AutoGenerateColumns="false" OnRowDataBound="grdphase4_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase4_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase10") %>' ToolTip='<%# Eval("Phase10") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase10" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase11") %>' ToolTip='<%# Eval("Phase11") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase11" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase12") %>' ToolTip='<%# Eval("Phase12") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase12" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel9">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase5" ScrollBars="Auto" Style="height: 260px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase5" AutoGenerateColumns="false" OnRowDataBound="grdphase5_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase5_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase5">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase5">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase10") %>' ToolTip='<%# Eval("Phase10") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase10" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase11") %>' ToolTip='<%# Eval("Phase11") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase11" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase12") %>' ToolTip='<%# Eval("Phase12") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase12" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase13") %>' ToolTip='<%# Eval("Phase13") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase13" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase14") %>' ToolTip='<%# Eval("Phase14") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase14" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase5">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase15") %>' ToolTip='<%# Eval("Phase15") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase15" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div style="margin-bottom: 10px; text-align:center; margin-top: 25px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PopupValidationSummary" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                        <asp:Button Text="Apply To" runat="server" ID="btnapply"  CssClass="btn btn-primary" OnClick="btnapply_Click" OnClientClick="OpenApplytoPopUp();" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>

                                </ContentTemplate>
                                <%--<Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>--%>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="modal fade" id="divComplianceScheduleDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 960px;">
                <div class="modal-content" style="height:400px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
                            <ContentTemplate>
                                <div style="margin: 5px">
                                    <div style="margin-bottom: 4px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="border: solid 1px red; background-color: #ffe8eb;"
                                            ValidationGroup="ComplianceValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry23" runat="server" EnableClientScript="False"
                                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                                    </div>
                                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div runat="server" style="margin-bottom: 7px; padding-left: 10px; height:300px;overflow-y:auto;">
                                                <asp:GridView runat="server" ID="grdAuditScheduleStartEndDate" AutoGenerateColumns="false" GridLines="None"
                                                    AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" DataKeyNames="Id">
                                                    <Columns>
                                                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       
                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                                    <asp:Label runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                           <asp:TemplateField HeaderText="Vertical">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                                    <asp:Label runat="server" Text='<%# Eval("VerticalName") %>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Period">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px">
                                                                    <asp:Label runat="server" Text='<%# Eval("TermName") %>' ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Financial Year">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                    <asp:Label runat="server" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Process">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                                    <asp:Label runat="server" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Start Date">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtExpectedStartDate" runat="server" CssClass="form-control" style="text-align: center;"
                                                                    Text='<%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MM-yyyy"):""%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="End Date">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtExpectedEndDate" runat="server" CssClass="form-control" style="text-align: center;"
                                                                    Text='<%# Eval("EndDate")!= null?((DateTime)Eval("EndDate")).ToString("dd-MM-yyyy"):""%>'></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </div>

                                            <div style="margin-bottom: 7px; text-align: center; ">
                                                <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click" CssClass="btn btn-primary" />
                                                <%-- <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click"
                                                    CssClass="button" />--%>
                                                <asp:Button Text="Close" runat="server" ID="Button2" CssClass="btn btn-primary" data-dismiss="modal" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="modal fade" id="divEDITAuditSchedulingDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 830px;">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title">Edit Audit Schedule</h4>
                    </div>
                    <div class="modal-body">
                        <div id="divAuditorDialog1">
                            <asp:UpdatePanel ID="upEDITComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
                                <ContentTemplate>
                                    <div>
                                        <div style="margin-bottom: 4px">
                                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="ComplianceValidationGroup" />
                                            <asp:CustomValidator ID="cvDuplicateEntry50" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            <asp:Label runat="server" ID="lblErrorMassage1" ForeColor="Red"></asp:Label>
                                        </div>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Location</label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEDITLocation" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                                <td>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Financial Year</label></td>
                                                <td>
                                                    <asp:Label ID="lblEDITFinancialyear" runat="server" Text="" Font-Bold="true"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Scheduling Type</label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEDITSchedulingType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td>
                                                     <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Vertical</label>
                                                    <asp:Label ID="lblEDITTermName" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVerticalName" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblEDITLocationID" runat="server" Text="" Visible="false"></asp:Label>
                                                     <asp:Label ID="lblEDITVerticalID" runat="server" Text="" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblEDITPhaseCount" runat="server" Text="" Visible="false"></asp:Label>                                                    
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Start Date</label>
                                                </td>
                                                 <td>
                                                     <asp:Label ID="lblEDITStartDate" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                 </td>
                                                 <td>
                                                      <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        End Date</label>
                                                 </td>
                                                 <td>
                                                     <asp:Label ID="lblEDITEndDate" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                 </td>
                                             </tr>
                                        </table>

                                        <div style="background: white; border: 1px solid gray; height: 355px; margin-bottom: 7px;">
                                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel2">
                                                <ContentTemplate>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlAnnuallyEDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdAnnuallyEDIT" AutoGenerateColumns="false" OnRowDataBound="grdAnnuallyEDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" ShowHeader="true" DataKeyNames="ID" OnPageIndexChanging="grdAnnuallyEDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Annually">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Annualy1") %>' ToolTip='<%# Eval("Annualy1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkAnnualy1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                                                                              
                                                         </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlHalfYearlyEDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdHalfYearlyEDIT" AutoGenerateColumns="false" OnRowDataBound="grdHalfYearlyEDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdHalfYearlyEDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Sep">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Halfyearly1") %>' ToolTip='<%# Eval("Halfyearly1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkHalfyearly1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Mar">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkHalfyearly2" runat="server" />
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Halfyearly2") %>' ToolTip='<%# Eval("Halfyearly2") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>                                                               
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlQuarterlyEDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdQuarterlyEDIT" AutoGenerateColumns="false" OnRowDataBound="grdQuarterlyEDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdQuarterlyEDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr-Jun">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Quarter1") %>' ToolTip='<%# Eval("Quarter1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul-Sep">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Quarter2") %>' ToolTip='<%# Eval("Quarter2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct-Dec">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Quarter3") %>' ToolTip='<%# Eval("Quarter3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan-Mar">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Quarter4") %>' ToolTip='<%# Eval("Quarter4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkQuarter4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                               
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlMonthlyEDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdMonthlyEDIT" AutoGenerateColumns="false" OnRowDataBound="grdMonthlyEDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdMonthlyEDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Apr">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly1") %>' ToolTip='<%# Eval("Monthly1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="May">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly2") %>' ToolTip='<%# Eval("Monthly2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jun">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly3") %>' ToolTip='<%# Eval("Monthly3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jul">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly4") %>' ToolTip='<%# Eval("Monthly4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aug">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly5") %>' ToolTip='<%# Eval("Monthly5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sep">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly6") %>' ToolTip='<%# Eval("Monthly6") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly6" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oct">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly7") %>' ToolTip='<%# Eval("Monthly7") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly7" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Nov">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly8") %>' ToolTip='<%# Eval("Monthly8") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly8" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Dec">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly9") %>' ToolTip='<%# Eval("Monthly9") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly9" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jan">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly10") %>' ToolTip='<%# Eval("Monthly10") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly10" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Feb">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly11") %>' ToolTip='<%# Eval("Monthly11") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly11" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mar">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Monthly12") %>' ToolTip='<%# Eval("Monthly12") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkMonthly12" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                              
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase1EDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase1EDIT" AutoGenerateColumns="false" OnRowDataBound="grdphase1EDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase1EDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                               
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase2EDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase2EDIT" AutoGenerateColumns="false" OnRowDataBound="grdphase2EDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase2EDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase3EDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase3EDIT" AutoGenerateColumns="false" OnRowDataBound="grdphase3EDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase3EDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                               
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase4EDIT" ScrollBars="Auto" Visible="false" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase4EDIT" AutoGenerateColumns="false" OnRowDataBound="grdphase4EDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase4EDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <%--PageSize="5" AllowPaging="true"--%>
                                                    <asp:Panel runat="server" ID="pnlphase5EDIT" ScrollBars="Auto" Style="height: 350px; overflow-y: auto;">
                                                        <asp:GridView runat="server" ID="grdphase5EDIT" AutoGenerateColumns="false" OnRowDataBound="grdphase5EDIT_RowDataBound"
                                                            GridLines="None" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase5EDIT_PageIndexChanging">
                                                            <Columns>
                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="ID" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Process">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase1">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase2">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase2" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase3">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase3" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phase4">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase4" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Phase5">
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>--%>
                                                                        <asp:CheckBox ID="chkPhase5" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                               
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="margin-bottom: 10px; text-align: center; margin-top: 10px;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                            <asp:Button Text="Save" runat="server" ID="btnEDITSave" OnClick="btnEDITSave_Click"
                                                CssClass="btn btn-primary" ValidationGroup="ComplianceValidationGroup" />
                                            <asp:Button Text="Close" runat="server" ID="Button3" CssClass="btn btn-primary" data-dismiss="modal" />
                                        </div>
                                        <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                        </div>
                                        <div class="clearfix" style="height: 20px">
                                        </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnEDITSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="DivApplyTo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 500px; margin-top: 70px;">
            <div class="modal-content" >
                <div>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                        ValidationGroup="ComplianceInstanceValidationGroup1" />
                    <asp:CustomValidator ID="cvDuplicateEntryApplyPop" runat="server" EnableClientScript="true"
                        ValidationGroup="ComplianceInstanceValidationGroup1" />
                    <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                </div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body" style="height:200px;">
                    <div style="margin-bottom: 7px">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateApplyToPopUp">
                            <ContentTemplate>
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 110px; display: block; float: left; font-size: 13px; color: #333;">
                                    Select Location</label>
                                <asp:DropDownCheckBoxes ID="ddlBranchList" runat="server" AutoPostBack="true" Visible="true"
                                    AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True" Style="padding: 0px; margin: 0px; width: 300px; height: 80px;">
                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="250" />
                                    <Texts SelectBoxCaption="Select Branch" />
                                </asp:DropDownCheckBoxes>
                                 <div class="clearfix"></div> 
                                 <div class="clearfix"></div> 
                                <div align="center" style="margin-bottom: 7px; margin-left: 20px; margin-top: 10px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Button Text="Save" runat="server" ID="btnSaveApplyto" OnClick="btnSaveApplyto_Click" CssClass="btn btn-primary" />
                                    <asp:Button Text="Close" runat="server" ID="Button4" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>
                            </ContentTemplate>                           
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
