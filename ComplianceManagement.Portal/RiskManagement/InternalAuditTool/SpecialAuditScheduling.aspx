﻿<%@ Page Title="Special Audit Scheduling" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="SpecialAuditScheduling.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.SpecialAuditScheduling" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            //setactivemenu('Special Audit Scheduling');
            fhead('Special Audit Scheduling');
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();

        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }
        function BindControls() {

            $(function () {
                $('input[id*=txtStartDatePop]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
            $(function () {
                $('input[id*=txtEndDatePop]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy'
                    });
            });
        }
        function fopenpopupAudit() {
            $('#divAuditSchedulingDialog').modal('show');
        }

        function CloseEvent() {
            $('#divAuditSchedulingDialog').modal('hide');
            window.location.reload();
        }
    </script>
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                     <section class="panel">
                        <div class="col-md-12 colpadding0">
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" />
                                <asp:CustomValidator ID="cvSpecialAuditError" runat="server" EnableClientScript="False"
                                    Display="none" class="alert alert-block alert-danger fade in" />
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0" style="margin-top: 5px; width:25%;">
                            </div>

                            <div class="col-md-5 colpadding0" style="margin-top: 5px; width:25%;">
                            </div>
                            <div class="col-md-1 colpadding0" style="margin-top: 5px; float: right !important">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" CssClass="btn btn-primary" OnClientClick="fopenpopupAudit()" OnClick="btnAddCompliance_Click" Visible="true" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true"
                                    class="form-control m-bot15" Width="90%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                    DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                    DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                    DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px"
                                    DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlFilterFinancialYear" class="form-control m-bot15" Width="90%" Height="32px"
                                    DataPlaceHolder="Financial Year" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                            <%{%> 
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Vertical" AutoPostBack="true"
                                        class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                        OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                              <%}%>
                        </div>
                        <div class="clearfix"></div>
                        <div style="margin-bottom: 4px">
                            &nbsp;
                            <asp:GridView runat="server" ID="grdAuditScheduling" AutoGenerateColumns="false" GridLines="None"
                                OnRowCommand="grdAuditScheduling_RowCommand"
                                OnRowDataBound="grdAuditScheduling_RowDataBound"
                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true" ShowHeaderWhenEmpty="true">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                                                        

                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                                <asp:Label runat="server" id="lblProcessId" Style="display:none;"  Text='<%# Eval("ProcessId") %>'></asp:Label>                                            

                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90%;">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                   <%-- <asp:TemplateField HeaderText="Vertical">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssigned" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("VerticalName") %>' ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="FY">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90%">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Period">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90%;">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("TermName") %>' ToolTip='<%# Eval("TermName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90%">
                                                <asp:Label ID="lblProcessname" data-toggle="tooltip" data-placement="top" runat="server" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDownLoadfile" runat="server" Text=""></asp:Label>
                                            <%-- <asp:Label ID="lblDownLoadfile" runat="server" 
                                            Text='<%# ShowStepsUploadOrNot(Convert.ToInt32(Eval("ProcessId"))) %>' ></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Upload Audit Steps">
                                        <ItemTemplate>
                                            <asp:FileUpload ID="fuSpecialAuditSteps" runat="server"></asp:FileUpload>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:UpdatePanel runat="server" ID="aaa" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button Text="Upload" runat="server" ID="btnAuditStepsSave" CommandName="UploadAuditSteps" CommandArgument='<%# Eval("ProcessId") + "," + Eval("VerticalID") +"," + Eval("CustomerBranchId") %>' CssClass="btn btn-primary" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAuditStepsSave" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />

                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text"></div></div>
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px; display:none;"></asp:Label>
                                            <div style="width: 100%; text-align:right;margin-right: 2%;" runat="server"></div>
                                           <u> <a href="../../AuditSampleDocument/SpecialStepSample.xlsx">Sample Format - Special Audit Step</a></u>                                                                                     
                                        </p>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page</p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                      </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="divAuditSchedulingDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAuditorDialog">
                        <asp:UpdatePanel ID="upComplianceDetails" runat="server">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 4px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="PopupValidationSummary" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PopupValidationSummary" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                    </div>

                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen ID="ddlLegalEntityPop" runat="server" AutoPostBack="true" DataPlaceHolder="Unit"
                                                class="form-control m-bot15" Width="80%" Height="32px"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlLegalEntityPop_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen ID="ddlSubEntity1Pop" runat="server" AutoPostBack="true" class="form-control m-bot15" DataPlaceHolder="Sub Unit"
                                                Width="80%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlSubEntity1Pop_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlSubEntity1" ID="RequiredFieldValidator3"
                                                runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen ID="ddlSubEntity2Pop" runat="server" AutoPostBack="true"
                                                class="form-control m-bot15" Width="80%" Height="32px" DataPlaceHolder="Sub Unit"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlSubEntity2Pop_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubEntity2" ID="RequiredFieldValidator4"
                                                runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity3Pop" AutoPostBack="true"
                                                class="form-control m-bot15" DataPlaceHolder="Sub Unit"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlSubEntity3Pop_SelectedIndexChanged"
                                                Width="80%" Height="32px">
                                            </asp:DropDownListChosen>
                                            <asp:ValidationSummary ID="ValidationSummary5" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceInstanceValidationGroup" />
                                            <asp:CustomValidator ID="CustomValidator5" runat="server" EnableClientScript="true"
                                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                            <asp:Label ID="Label3" runat="server" Style="color: Red"></asp:Label>
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                &nbsp;
                                            </label>
                                            <asp:DropDownListChosen runat="server" ID="ddlSubEntity4Pop" AutoPostBack="true"
                                                DataPlaceHolder="Sub Unit" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlSubEntity4Pop_SelectedIndexChanged"
                                                class="form-control m-bot15" Width="80%" Height="32px">
                                            </asp:DropDownListChosen>
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="80%" Height="32px"
                                                DataPlaceHolder="Financial Year" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                OnSelectedIndexChanged="ddlFinancialYearpop_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select Financial Year."
                                                ControlToValidate="ddlFinancialYear" runat="server" ValidationGroup="PopupValidationSummary"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Financial Year."
                                                ControlToValidate="ddlFinancialYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *</label>

                                            <asp:DropDownListChosen runat="server" ID="ddlVertical" class="form-control m-bot15"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                Width="80%" Height="32px"
                                                DataPlaceHolder="Vertical">
                                            </asp:DropDownListChosen>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select Vertical."
                                                ControlToValidate="ddlVertical" runat="server" ValidationGroup="PopupValidationSummary"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select Vertical."
                                                ControlToValidate="ddlVertical" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <asp:TextBox runat="server" ID="txtStartDatePop" class="form-control" AutoComplete="Off" Style="width: 80%;" placeholder="Start Date" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Expected Start Date can not be empty."
                                                ControlToValidate="txtStartDatePop"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>

                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                                *
                                            </label>
                                            <asp:TextBox runat="server" ID="txtEndDatePop" class="form-control" AutoComplete="Off" Style="width: 80%" placeholder="End Date" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Expected End Date can not be empty."
                                                ControlToValidate="txtEndDatePop"
                                                runat="server" ValidationGroup="PopupValidationSummary" Display="None" />
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div style="margin-bottom: 7px">
                                        <span style="color: #333; font-weight: bold;display:none">Schedule Details</span>
                                    </div>

                                    <div style="margin-left: 20px; background: white; border: 1px solid gray; height: 120px; width: 95%; margin-bottom: 7px;">


                                        <asp:Panel runat="server" ID="pnlAnnually" ScrollBars="Auto" Visible="false">
                                            <asp:GridView runat="server" ID="grdAnnually" AutoGenerateColumns="false" OnRowDataBound="grdAnnually_RowDataBound"
                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                DataKeyNames="ID">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Process">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtNewProcess" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SubProcess">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtNewSubProcess" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Special Audit">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("OneTime") %>' ToolTip='<%# Eval("OneTime") %>'></asp:Label>
                                                            <asp:CheckBox ID="chkOneTime" runat="server" Enabled="false" Checked="true" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div style="margin-bottom: 7px; margin-left: 260px; margin-top: 25px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                        ValidationGroup="PopupValidationSummary" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" OnClientClick="CloseEvent()" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>
                                <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
