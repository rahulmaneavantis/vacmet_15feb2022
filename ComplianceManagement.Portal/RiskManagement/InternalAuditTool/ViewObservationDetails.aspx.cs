﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ViewObservationDetails : System.Web.UI.Page
    {
        int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProcess();
                BindGrid();
                int chknumber = Convert.ToInt32(grdViewObservations.PageIndex);
                bindPageNumber();
                if (chknumber > 0)
                {
                    chknumber = chknumber + 1;
                    DropDownListPageNo.SelectedValue = (chknumber).ToString();
                }
            }
            
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                    ListItem removeItem = DropDownListPageNo.Items.FindByText("0");
                    DropDownListPageNo.Items.Remove(removeItem);
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindProcess()
        {
            
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.Mst_Process
                                   where row.CustomerID == CustomerId
                                   && row.IsDeleted == false
                                   select row).ToList();
                if (processList.Count > 0)
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ID";
                    ddlProcess.Items.Clear();
                    ddlProcess.DataSource = processList;
                    ddlProcess.DataBind();
                }
            }
        }

        public void BindGrid()
        {
            try
            {
                List<long?> ProcessIdList = new List<long?>();
                List<long?> SubProcessIdList = new List<long?>();

                for (int i = 0; i < ddlProcess.Items.Count; i++)
                {
                    if (ddlProcess.Items[i].Selected)
                    {
                        ProcessIdList.Add(Convert.ToInt64(ddlProcess.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlSubProcess.Items.Count; i++)
                {
                    if (ddlSubProcess.Items[i].Selected)
                    {
                        SubProcessIdList.Add(Convert.ToInt64(ddlSubProcess.Items[i].Value));
                    }
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var viewObservationList = (from row in entities.usp_ViewObservations(CustomerId)
                                               select row).ToList();

                    if (ProcessIdList.Count > 0)
                    {
                        viewObservationList = viewObservationList.Where(entry => ProcessIdList.Contains(entry.ProcessId)).ToList();
                    }
                    if (SubProcessIdList.Count > 0)
                    {
                        viewObservationList = viewObservationList.Where(entry => SubProcessIdList.Contains(entry.SubProcessId)).ToList();
                    }

                    if (viewObservationList.Count > 0)
                    {
                        grdViewObservations.DataSource = viewObservationList;
                        grdViewObservations.DataBind();
                        Session["TotalRows"] = viewObservationList.Count;
                    }
                    else
                    {
                        grdViewObservations.DataSource = null;
                        grdViewObservations.DataBind();
                        
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdViewObservations.PageIndex = chkSelectedPage - 1;
            grdViewObservations.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdViewObservations.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdViewObservations.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcess.SelectedValue != "-1")
            {
                if (ddlProcess.SelectedValue != "")
                {

                    List<long?> ProcessIdList = new List<long?>();

                    for (int i = 0; i < ddlProcess.Items.Count; i++)
                    {
                        if (ddlProcess.Items[i].Selected)
                        {
                            ProcessIdList.Add(Convert.ToInt64(ddlProcess.Items[i].Value));
                        }
                    }

                    //int processId = Convert.ToInt32(ddlProcess.SelectedValue);

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var subProcessList = (from row in entities.mst_Subprocess
                                              where ProcessIdList.Contains(row.ProcessId)
                                              && row.IsProcessNonProcess == "P"
                                              && row.IsDeleted == false
                                              select row).ToList();

                        if (subProcessList.Count > 0)
                        {
                            ddlSubProcess.DataTextField = "Name";
                            ddlSubProcess.DataValueField = "ID";
                            ddlSubProcess.Items.Clear();
                            ddlSubProcess.DataSource = subProcessList;
                            ddlSubProcess.DataBind();
                        }
                    }
                }
            }
            BindGrid();
            bindPageNumber();
        }

        protected void grdViewObservations_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (e.CommandName.Equals("View"))
                    {
                        String Args = e.CommandArgument.ToString();
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        //string AuditName = Convert.ToString(commandArgs[0]);
                        int AuditStepId = Convert.ToInt32(commandArgs[0]);

                        //lblviewAuditStep.Text = AuditName;
                        lblviewAuditStep.Text = (from row in entities.AuditStepMasters
                                                 where row.ID == AuditStepId
                                                 select row.AuditStep).FirstOrDefault();
                        upViewAuditStep.Update();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Popup", "fopenpopup();", true);
                    }
                    else if (e.CommandName.Equals("ObservationPopUp"))
                    {
                        String Args = e.CommandArgument.ToString();
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ProcessId = Convert.ToInt32(commandArgs[0]);
                        int SubProcessId = Convert.ToInt32(commandArgs[1]);
                        int AuditStepid = Convert.ToInt32(commandArgs[2]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDetObsDialog(" + ProcessId + "," + AuditStepid + "," + SubProcessId + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Text Filter
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        public static List<usp_ViewObservations_Result> GetfilteredAuditStep(string filter = null)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int cust = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                    var users = (from row in entities.usp_ViewObservations(cust)
                                 select row);

                    if (!string.IsNullOrEmpty(filter))
                    {
                        users = users.Where(entry => entry.AuditStep.Contains(filter));
                    }
                    return users.OrderBy(entry => entry.AuditStep).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
        public void BindfilterData()
        {
            try
            {
                List<long?> ProcessIdList = new List<long?>();
                List<long?> SubProcessIdList = new List<long?>();

                for (int i = 0; i < ddlProcess.Items.Count; i++)
                {
                    if (ddlProcess.Items[i].Selected)
                    {
                        ProcessIdList.Add(Convert.ToInt64(ddlProcess.Items[i].Value));
                    }
                }
                for (int i = 0; i < ddlSubProcess.Items.Count; i++)
                {
                    if (ddlSubProcess.Items[i].Selected)
                    {
                        SubProcessIdList.Add(Convert.ToInt64(ddlSubProcess.Items[i].Value));
                    }
                }
                var uselist = GetfilteredAuditStep(txtAuditStepFilter.Text);

                if (ProcessIdList.Count > 0)
                {
                    uselist = uselist.Where(entry => ProcessIdList.Contains(entry.ProcessId)).ToList();
                }
                if (SubProcessIdList.Count > 0)
                {
                    uselist = uselist.Where(entry => SubProcessIdList.Contains(entry.SubProcessId)).ToList();
                }

                if (uselist.Count > 0)
                {
                    grdViewObservations.DataSource = uselist;
                    Session["TotalRows"] = uselist.Count;
                    grdViewObservations.DataBind();
                    bindPageNumber();
                }
                else
                {
                    grdViewObservations.DataSource = null;
                    grdViewObservations.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void txtAuditStepFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdViewObservations.PageIndex = 0;
                BindfilterData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}