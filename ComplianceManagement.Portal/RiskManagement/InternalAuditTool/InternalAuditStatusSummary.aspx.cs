﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class InternalAuditStatusSummary : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                {
                    ViewState["RoleID"] = Request.QueryString["Role"];
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {                       
                        ViewState["returnUrl1"] = Request.QueryString["ReturnUrl1"];
                    }                   
                }
                else
                {
                    ViewState["RoleID"] = "3";
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    ViewState["Period"] = Convert.ToString(Request.QueryString["Period"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["PageSize"]) && !String.IsNullOrEmpty(Request.QueryString["gridpagesize"]) && !String.IsNullOrEmpty(Request.QueryString["BoPSize"]))
                {
                    ddlPageSize.SelectedIndex = Convert.ToInt32(Request.QueryString["PageSize"]);

                    int chkSelectedPage = Convert.ToInt32(Request.QueryString["BoPSize"]);
                    grdAuditStatus.PageIndex = chkSelectedPage - 1;
                    grdAuditStatus.PageSize = Convert.ToInt32(Request.QueryString["gridpagesize"]);
                    BindProcess("P", customerID, -1);
                    BindData();
                    DropDownListPageNo.SelectedValue = Request.QueryString["BoPSize"];
                }
                else
                {
                    BindProcess("P", customerID, -1);
                    BindData();
                }            
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditStatus.PageIndex = chkSelectedPage - 1;            
            grdAuditStatus.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);            
            BindData();
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url = "";
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            {
                url = Request.QueryString["ReturnUrl1"];
                Response.Redirect("~/RiskManagement/InternalAuditTool/InternalAuditStatusUI.aspx?" + url.Replace("@", "=") + "&RoleID=" + Request.QueryString["Role"] + "");
            }
            else
            {                
                Response.Redirect("~/RiskManagement/InternalAuditTool/InternalAuditStatusUI.aspx?Status=Open");
            }
        }
        private void BindProcess(string flag, int Customerid,int Branchid)
        {
            try
            {
                if (Session["ListOfAuditInstance"] != null)
                {
                    var InternalAuditInstancelist = (List<long>)Session["ListOfAuditInstance"];

                    var processids = InternalControlManagementDashboardRisk.GetDistinctProcessIDsInternalAudit(InternalAuditInstancelist);

                    if (flag == "P")
                    {
                        ddlSubProcess.Items.Clear();
                        ddlProcess.Items.Clear();

                        ddlProcess.DataTextField = "Name";
                        ddlProcess.DataValueField = "Id";
                        ddlProcess.DataSource = ProcessManagement.FillProcess("P", Customerid, Branchid, processids);
                        ddlProcess.DataBind();
                        ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));

                        if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                        }
                    }
                    else
                    {
                        ddlSubProcess.Items.Clear();
                        ddlProcess.Items.Clear();

                        ddlProcess.DataTextField = "Name";
                        ddlProcess.DataValueField = "Id";
                        ddlProcess.DataSource = ProcessManagement.FillProcess("N", Customerid, Branchid, processids);
                        ddlProcess.DataBind();
                        ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));

                        if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem(" Select Sub Process ", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem(" Select Sub Process ", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindData()
        {
            try
            {
                int roleid = -1;
                int Processid = -1;
                int SubProcessID = -1;
                string Period = string.Empty;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (Session["ListOfAuditInstance"] != null)
                {
                    var InternalAuditInstancelist = (List<long>)Session["ListOfAuditInstance"];

                    if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                    {
                        Period = Convert.ToString(Request.QueryString["Period"].Trim());
                    }
                    else
                    {
                        Period = Convert.ToString(ViewState["Period"]);
                    }
                    if (!String.IsNullOrEmpty(Request.QueryString["Role"]))
                    {
                        roleid = Convert.ToInt32(Request.QueryString["Role"].Trim());
                    }
                    else
                    {
                        roleid = Convert.ToInt32(ViewState["RoleID"]);
                    }
                    
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        if (ddlProcess.SelectedValue != "-1")
                        {
                            Processid = Convert.ToInt32(ddlProcess.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                    {
                        if (ddlSubProcess.SelectedValue != "-1")
                        {
                            SubProcessID = Convert.ToInt32(ddlSubProcess.SelectedValue);
                        }
                    }
                    var AuditLists = InternalControlManagementDashboardRisk.GetInternalAuditStatusProcessSubProcessUserWise(InternalAuditInstancelist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, roleid, customerID, Processid, SubProcessID, Convert.ToString(Period.Trim()));
                    grdAuditStatus.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Rows.Count;
                    grdAuditStatus.DataBind();

                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAuditStatus.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcess.SelectedItem.Text != " Select Process " || ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != "")
            {
                BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
            }
            BindData();
        }

        protected void ddlProcessNew_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlProcess.SelectedItem.Text != " Select Process " || ddlProcess.SelectedValue != "-1" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != "")
            {
                BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
            }
        }

        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditStatus.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);               
                BindData();              
            }
            catch (Exception ex)
            {               
            }
        }        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

       
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void grdAuditStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    int PageSize = Convert.ToInt32(ddlPageSize.SelectedIndex);
                    int gridpagesize = Convert.ToInt32(grdAuditStatus.PageSize);
                    int BoPSize = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                    String Args = e.CommandArgument.ToString();
                    String URLStr = "";
                    string[] arg = Args.ToString().Split(';');
                    string url = "";
                    if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
                    {
                        url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
                    }
                    string url2 = HttpContext.Current.Request.Url.AbsolutePath;
                    if ((!String.IsNullOrEmpty(Request.QueryString["Period"])))
                    {                        
                        URLStr = "~/RiskManagement/AuditTool/TestingUI.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&PID=" + arg[3] + "&SPID=" + arg[4] + "&CustBranchID=" + arg[5] + "&RoleID=" + arg[6] + "&Period= " + Request.QueryString["Period"].ToString() + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize + "&returnUrl2=Role@" + Request.QueryString["Role"].ToString() + url;
                    }
                    else
                    {
                        URLStr = "~/RiskManagement/AuditTool/TestingUI.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&PID=" + arg[3] + "&SPID=" + arg[4] + "&CustBranchID=" + arg[5] + "&RoleID=" + arg[6] + "&Period= " + Request.QueryString["Period"].ToString() + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize;
                    }

                    if (Args != "")
                        Response.Redirect(URLStr, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}