﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
//using Spire.Presentation.Drawing.Animation;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class CoalReviewReport : System.Web.UI.Page
    {
      
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();

                ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                //ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        
        private void BindProcess(string flag)
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                if (flag == "P")
                {
                    //ddlProcess.Items.Clear();
                    //ddlProcess.DataTextField = "Name";
                    //ddlProcess.DataValueField = "Id";
                    //ddlProcess.DataSource = ProcessManagement.FillProcess("P", branchid);
                    //ddlProcess.DataBind();
                    //ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                else
                {
                    //ddlProcess.Items.Clear();
                    //ddlProcess.DataTextField = "Name";
                    //ddlProcess.DataValueField = "Id";
                    //ddlProcess.DataSource = ProcessManagement.FillProcess("N", branchid);
                    //ddlProcess.DataBind();
                    //ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        

        protected void lbtnExportExcelTest_Click(object sender, EventArgs e)
        {
            #region Sheet 1
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    string chk = txtfromMonthDate.Text;
                    int Year = Convert.ToInt32(chk.Substring(chk.Length - 4));
                    string YourString = chk.Remove(chk.Length - 5);
                    int monthnumber = DateTime.ParseExact(YourString, "MMMM", CultureInfo.InvariantCulture).Month;                 
                    DateTime fromDatePeroid = new DateTime(Year, monthnumber, 1);

                    string chk2 = txttoMonthDate.Text;
                    int Yearto = Convert.ToInt32(chk2.Substring(chk2.Length - 4));
                    string YourString2 = chk2.Remove(chk2.Length - 5);
                    int monthnumber2 = DateTime.ParseExact(YourString2, "MMMM", CultureInfo.InvariantCulture).Month;
                    int days = DateTime.DaysInMonth(Yearto, monthnumber2);
                    DateTime toDatePeroid = new DateTime(Yearto, monthnumber2, days);

                    if (fromDatePeroid <= toDatePeroid)
                    {
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Summary Of Test Results");
                        String FileName = String.Empty;
                        DataTable ExcelData = null;

                        ApplyFilter = false;                        
                        int RoleID = -1;

                        RoleID = -1;
                        if (PerformerFlag)
                            RoleID = 3;
                        if (ReviewerFlag)
                            RoleID = 4;
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        int CustBranchID = -1;
                        int VerticalID = -1;
                        int ProcessID = -1;
                        String FinancialYear = String.Empty;
                        String Period = String.Empty;
                        string PeriodValueYear = string.Empty;
                        string Year1 = string.Empty;
                        string Year2 = string.Empty;
                        if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                        {
                            if (ddlSubEntity1.SelectedValue != "-1")
                            {
                                CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                        {
                            if (ddlSubEntity2.SelectedValue != "-1")
                            {
                                CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                        {
                            if (ddlSubEntity3.SelectedValue != "-1")
                            {
                                CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                        {
                            if (ddlFilterLocation.SelectedValue != "-1")
                            {
                                CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                FinancialYear = ddlFinancialYear.SelectedItem.Text;
                                if (!string.IsNullOrEmpty(FinancialYear))
                                {
                                    Year1 = FinancialYear.Substring(0, FinancialYear.IndexOf("-")).Trim();
                                    Year2 = FinancialYear.Substring(5, FinancialYear.IndexOf("-")).Trim();
                                }
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlPeriod.SelectedValue != "-1")
                            {
                                Period = ddlPeriod.SelectedItem.Text;
                                if (Period.Equals("Jan-Mar") || Period.Equals("Jan") || Period.Equals("Feb") || Period.Equals("Mar"))
                                {
                                    PeriodValueYear = Year2;
                                }
                                else if (Period.Equals("Oct-Mar"))
                                {
                                    PeriodValueYear = FinancialYear;
                                }
                                else if (Period.Equals("Apr-Sep") || Period.Equals("Apr-Jun") || Period.Equals("Jul-Sep") || Period.Equals("Oct-Dec") || Period.Equals("Apr") || Period.Equals("May") || Period.Equals("Jun") || Period.Equals("Jul") || Period.Equals("Aug") || Period.Equals("Sep") || Period.Equals("Oct") || Period.Equals("Nov") || Period.Equals("Dec"))
                                {
                                    PeriodValueYear = Year1;
                                }
                            }
                        }
                        if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                        {
                            int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                            if (vid != -1)
                            {
                                VerticalID = vid;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                            {
                                if (ddlVertical.SelectedValue != "-1")
                                {
                                    VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                                }
                            }
                        }

                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            if (RoleID == -1)
                            {
                                RoleID = 4;
                            }
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                            var Branchlistloop = Branchlist.ToList();
                        }
                        else
                        {                            
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                            var Branchlistloop = Branchlist.ToList();
                        }
                        List<CoalReviewKPUnitView> assignmentList = null;
                        assignmentList = ProcessManagement.GetCoalReviewKPUnitView(RoleID, customerID, Portal.Common.AuthenticationHelper.UserID, VerticalID, ProcessID, FinancialYear, Period, Branchlist, fromDatePeroid, toDatePeroid);
                        string locatioName = null;
                        string ClientName = null;
                        if (CustBranchID != 1)
                        {
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                locatioName = (from row in entities.mst_CustomerBranch
                                               where row.ID == CustBranchID
                                               select row.Name).FirstOrDefault();

                                ClientName = (from row in entities.mst_Customer
                                              where row.ID == customerID
                                              select row.Name).FirstOrDefault();
                            }
                        }

                        DataTable table = new DataTable();
                        table.Columns.Add("Id", typeof(int));
                        table.Columns.Add("ProcessId", typeof(long));
                        table.Columns.Add("CustomerbranchId", typeof(long));
                        table.Columns.Add("ForPeriod", typeof(string));
                        table.Columns.Add("FinancialYear", typeof(string));
                        table.Columns.Add("Rs", typeof(string));
                        table.Columns.Add("Name", typeof(string));
                        table.Columns.Add("Observation", typeof(string));
                        table.Columns.Add("ManagementResponse", typeof(string));
                        table.Columns.Add("PersonResponsible", typeof(string));
                        table.Columns.Add("Owner", typeof(string));
                        table.Columns.Add("TimeLine", typeof(string));
                        table.Columns.Add("ObservationRating", typeof(string));
                        
                        string observationTitle = string.Empty;
                        int AtbtID = 0;
                        int i = 0;
                        foreach (var cc in assignmentList)
                        {
                            i++;
                            if (AtbtID != cc.ATBDId)
                            {
                                AtbtID = Convert.ToInt32(cc.ATBDId);
                                table.Rows.Add(i, null, null, "", "", "", "", cc.ObservationTitle, "", "", "", null, cc.ObservationRating);

                                table.Rows.Add(null, cc.ProcessId, cc.CustomerbranchId, cc.ForPeriod, cc.FinancialYear, cc.Rs, cc.Name, cc.Observation, cc.ManagementResponse,
                                   cc.PersonResponsible, cc.Owner, Convert.ToString(cc.TimeLine), "0");

                                table.Rows.Add(null, null, null, "", "", "", "", "Risk", "", "", "", null, "5");
                                table.Rows.Add(null, null, null, "", "", "", "", cc.Risk, "", "", "", null, "0");

                                table.Rows.Add(null, null, null, "", "", "", "", "Recomendation", "", "", "", null, "6");
                                table.Rows.Add(null, null, null, "", "", "", "", cc.Recomendation, "", "", "", null, "0");

                            }                         
                        }

                        DataView view = new System.Data.DataView(table as DataTable);
                        ExcelData = view.ToTable("Selected", false, "Id", "Observation", "Rs", "ManagementResponse", "PersonResponsible", "Owner", "TimeLine", "ObservationRating");
                        
                        exWorkSheet1.Cells["A1"].Value = "MANAGEMENT AUDIT REPORT";
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;                        
                        exWorkSheet1.Cells["A1:H1"].Merge = true;
                        exWorkSheet1.Cells["A1:H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A1:H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["A2"].Value = ClientName;//"Emami Agrotech Ltd";
                        exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A2"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A2:H2"].Merge = true;
                        exWorkSheet1.Cells["A2:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A2:H2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["A3"].Value = "Location : " + locatioName;
                        exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A3:H3"].Merge = true;
                        exWorkSheet1.Cells["A3:H3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A3:H3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["A4"].Value = "Audit Period - " + txtfromMonthDate.Text + " to " + txttoMonthDate.Text;   //"Audit Period - " + Period + " " + PeriodValueYear;
                        exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A4:H4"].Merge = true;
                        exWorkSheet1.Cells["A4:H4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A4:H4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            if (Convert.ToInt32(item["ObservationRating"]) == 1)
                            {
                                item["ObservationRating"] = "High";                               
                            }
                            else if (Convert.ToInt32(item["ObservationRating"]) == 3)
                            {
                                item["ObservationRating"] = "Low";
                            }
                            else if (Convert.ToInt32(item["ObservationRating"]) == 2)
                            {
                                item["ObservationRating"] = "Mediuam";

                            }
                            else if (Convert.ToInt32(item["ObservationRating"]) == 5)
                            {
                                item["ObservationRating"] = "Risk";

                            }
                            else if (Convert.ToInt32(item["ObservationRating"]) == 6)
                            {
                                item["ObservationRating"] = "Recomendation";

                            }
                            else
                            {
                                item["ObservationRating"] = "";
                            }                          
                            if (!string.IsNullOrEmpty(Convert.ToString(item["TimeLine"])))
                            {
                                item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                            }
                        }


                        exWorkSheet1.Cells["A5"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet1.Cells["A5"].Value = "Sl. No.";
                        exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A5"].AutoFitColumns(5);
                        exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663

                        exWorkSheet1.Cells["B5"].Value = "Observation";
                        exWorkSheet1.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["B5"].AutoFitColumns(60);
                        exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663

                        exWorkSheet1.Cells["C5"].Value = "Rs.(Lacs)";
                        exWorkSheet1.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663

                        exWorkSheet1.Cells["D5"].Value = "Auditee's Response";//"Auditee's Response"
                        exWorkSheet1.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663

                        exWorkSheet1.Cells["E5"].Value = "Person Responsible";//Person Responsible
                        exWorkSheet1.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet1.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663

                        exWorkSheet1.Cells["F5"].Value = "Owner";//Owner
                        exWorkSheet1.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663
                        exWorkSheet1.Cells["F5"].AutoFitColumns(25);

                        exWorkSheet1.Cells["G5"].Value = "Time Line";
                        exWorkSheet1.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["G5"].AutoFitColumns(15);
                        exWorkSheet1.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["G5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663
                       
                        exWorkSheet1.Cells["H5"].Value = "Risk";//Observation Rating
                        exWorkSheet1.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["H5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#B7DEE8"));//#1F5663
                        
                        int lengthchk = Convert.ToInt32(ExcelData.Rows.Count) + 6;
                        for (int k = 6; k < lengthchk; k++)
                        {
                            string chkCondition = "H" + k;                            
                            string chkConditionobservation = "B" + k;
                            string GchkCondition = "G" + k;
                            string checkallcond = exWorkSheet1.Cells[chkCondition].Value.ToString();

                            if (checkallcond.Equals("High"))
                            {
                                exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);//#1F5663
                                exWorkSheet1.Cells[chkCondition].Value = "";

                                string chkConditionoSlno = "A" + k;
                                string final = chkConditionobservation + ":" + GchkCondition;
                                exWorkSheet1.Cells[final].Merge = true;
                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#8696B8"));//#1F5663                                
                                exWorkSheet1.Cells[final].Style.Font.Bold = true;
                               
                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#8696B8"));//#1F5663 

                            }

                            else if (checkallcond.Equals("Mediuam"))
                            {
                                exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);//#1F5663
                                exWorkSheet1.Cells[chkCondition].Value = "";

                                string chkConditionoSlno = "A" + k;
                                string final = chkConditionobservation + ":" + GchkCondition;
                                exWorkSheet1.Cells[final].Merge = true;
                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#8696B8"));//#1F5663                               
                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#8696B8"));//#1F5663 
                            }
                            else if (checkallcond.Equals("Low"))
                            {
                                exWorkSheet1.Cells[chkCondition].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[chkCondition].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);//#1F5663
                                exWorkSheet1.Cells[chkCondition].Value = "";

                                string chkConditionoSlno = "A" + k;
                                string final = chkConditionobservation + ":" + GchkCondition;
                                exWorkSheet1.Cells[final].Merge = true;
                                exWorkSheet1.Cells[final].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[final].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#8696B8"));//#1F5663                               
                                exWorkSheet1.Cells[final].Style.Font.Bold = true;

                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[chkConditionoSlno].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#8696B8"));//#1F5663 
                            }
                            else if (checkallcond.Equals("Recomendation") || checkallcond.Equals("Risk"))
                            {
                                exWorkSheet1.Cells[chkCondition].Value = "";
                                exWorkSheet1.Cells[chkConditionobservation].Style.Font.Bold = true;
                                exWorkSheet1.Cells[chkConditionobservation].Style.Font.Size = 12;
                            }
                        }
                      
                        for (int k = 7; k < lengthchk; k++)
                        {                          
                            exWorkSheet1.Cells["C" + k + ":" + "C" + (k + 4)].Merge = true;
                            exWorkSheet1.Cells["D" + k + ":" + "D" + (k + 4)].Merge = true;
                            exWorkSheet1.Cells["E" + k + ":" + "E" + (k + 4)].Merge = true;
                            exWorkSheet1.Cells["F" + k + ":" + "F" + (k + 4)].Merge = true;
                            exWorkSheet1.Cells["G" + k + ":" + "G" + (k + 4)].Merge = true;
                            exWorkSheet1.Cells["H" + k + ":" + "H" + (k + 4)].Merge = true;
                            
                            k = k + 4 + 1;
                        }

                        int count = Convert.ToInt32(ExcelData.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, 8])
                        {
                            col.Style.WrapText = true;
                                                        
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            Byte[] fileBytes = exportPackge.GetAsByteArray();                                                        
                            locatioName = locatioName + "_" + txtfromMonthDate.Text + " to " + txttoMonthDate.Text + ".xlsx";
                            locatioName = locatioName.Replace(',', ' ');
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("content-disposition", "attachment;filename=AuditReport_" + locatioName);//locatioName                                                                             
                            Response.BinaryWrite(fileBytes);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   

                        }
                    }
                    else
                    {                       
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "From Date should be less than To Date.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion

           
        }

        //protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlProcess.SelectedValue != "-1")
        //    {
        //        // BindData();
        //    }
        //}

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {            
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            //BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
            // BindData();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                // BindData();
            }

        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }

    }
}