﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditManagerMainUI : System.Web.UI.Page
    {
        protected List<Int32> roles;
        protected static int CustomerId = 0;
        public List<InternalAuditTransactionView> MasterInternalAuditTransactionView = new List<InternalAuditTransactionView>();
        public List<InternalControlAuditAssignment> MasterInternalControlAuditAssignment = new List<InternalControlAuditAssignment>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["SID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["PID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["SPID"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                            if (!String.IsNullOrEmpty(Request.QueryString["peroid"]))
                                                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                                {
                                                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                                    {
                                                        int CBranchID = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                                                        roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                        if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                                                        {
                                                            #region SID
                                                            string statusidasd = Request.QueryString["Status"].ToString();
                                                            int statusFlag = 0;
                                                            if (statusidasd.Equals("NotDone"))
                                                            {
                                                                statusFlag = 1;
                                                            }
                                                            else if (statusidasd.Equals("Submitted"))
                                                            {
                                                                statusFlag = 2;
                                                            }
                                                            else if (statusidasd.Equals("TeamReview"))
                                                            {
                                                                statusFlag = 4;
                                                            }
                                                            else if (statusidasd.Equals("AuditeeReview"))
                                                            {
                                                                statusFlag = 6;
                                                            }
                                                            else if (statusidasd.Equals("FinalReview"))
                                                            {
                                                                statusFlag = 5;
                                                            }
                                                            else if (statusidasd.Equals("Closed"))
                                                            {
                                                                statusFlag = 3;
                                                            }

                                                            if (statusFlag != 0)
                                                            {
                                                                ddlFilterStatus.ClearSelection();
                                                                ddlFilterStatus.Items.FindByValue(statusFlag.ToString()).Selected = true;
                                                            }
                                                            bool checkPRFlag = false;
                                                            checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(Convert.ToInt32(CustomerId), Convert.ToInt32(CBranchID));
                                                            var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                            var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CBranchID);
                                                            if (checkPRFlag == true)
                                                            {
                                                                #region checkPRFlag true
                                                                bool checkPRRiskActivationFlag = false;
                                                                checkPRRiskActivationFlag = CustomerManagementRisk.CheckPersonResponsibleFlowApplicable(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CBranchID);

                                                                if (litrole.Contains(4)) // If Audit Manager IS Reviewer
                                                                {
                                                                    if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                                                                    {
                                                                        btnAllSavechk.Enabled = false;
                                                                        ddlStatus.Visible = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        btnAllSavechk.Enabled = true;
                                                                        ddlStatus.Visible = true;
                                                                    }
                                                                }
                                                                //if Audit Manager Is PersonResponsible
                                                                else if ((AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH") && checkPRRiskActivationFlag)
                                                                {
                                                                    if (statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                                                                    {
                                                                        btnAllSavechk.Enabled = false;
                                                                        ddlStatus.Visible = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        btnAllSavechk.Enabled = true;
                                                                        ddlStatus.Visible = true;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                                                                    {
                                                                        btnAllSavechk.Enabled = false;
                                                                        ddlStatus.Visible = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        btnAllSavechk.Enabled = true;
                                                                        ddlStatus.Visible = true;
                                                                    }
                                                                }
                                                                BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                #region checkPRFlag false
                                                                if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                                                                {
                                                                    btnAllSavechk.Enabled = false;
                                                                    ddlStatus.Visible = false;
                                                                }
                                                                else
                                                                {
                                                                    btnAllSavechk.Enabled = true;
                                                                    ddlStatus.Visible = true;
                                                                }

                                                                BindStatusList(5, litrole.ToList(), AuditHeadOrManager);
                                                                #endregion
                                                            }
                                                            #endregion
                                                        }

                                                        ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + Request.QueryString["PID"] + ";" + Request.QueryString["SPID"] + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["AuditID"];


                                                        BindProcess("P", CustomerId, Convert.ToInt32(Request.QueryString["AuditID"]));
                                                        if (Convert.ToInt32(Request.QueryString["PID"]) != -1)
                                                        {
                                                            ddlProcess.ClearSelection();
                                                            ddlProcess.SelectedValue = Request.QueryString["PID"].Trim();
                                                            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                                                            {
                                                                if (ddlProcess.SelectedValue != "-1")
                                                                {
                                                                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                                                                }
                                                            }
                                                            if (Convert.ToInt32(Request.QueryString["SPID"]) != -1)
                                                            {
                                                                ddlSubProcess.ClearSelection();
                                                                ddlSubProcess.SelectedValue = Request.QueryString["SPID"].Trim();
                                                            }
                                                        }
                                                        //PageTitle
                                                        var BrachName = CustomerBranchManagement.GetByID(CBranchID).Name;
                                                        string Pname = string.Empty;
                                                        string SPname = string.Empty;
                                                        string VName = string.Empty;
                                                        string FY = string.Empty;
                                                        string Period1 = string.Empty;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FY"])))
                                                        {
                                                            FY = "/" + Convert.ToString(Request.QueryString["FY"]);
                                                        }
                                                        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["peroid"])))
                                                        {
                                                            Period1 = "/" + Convert.ToString(Request.QueryString["peroid"]);
                                                        }
                                                        if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                                                        {
                                                            if (ddlProcess.SelectedValue != "-1")
                                                            {
                                                                Pname = "/" + ddlProcess.SelectedItem.Text;
                                                            }
                                                        }
                                                        if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                                                        {
                                                            if (ddlSubProcess.SelectedValue != "-1")
                                                            {
                                                                SPname = "/" + ddlSubProcess.SelectedItem.Text;
                                                            }
                                                        }
                                                        var VerticalName = UserManagementRisk.GetVerticalName(CustomerId, Convert.ToInt32((Request.QueryString["VID"])));
                                                        if (string.IsNullOrEmpty(VerticalName))
                                                        {
                                                            VName = "/" + VerticalName;
                                                        }
                                                        LblPageDetails.InnerText = BrachName + FY + Period1 + Pname + SPname + VName;
                                                        //End
                                                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                        bindPageNumber();
                                                    }
                                                }
            }
        }
        private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                ddlStatus.Items.Clear();
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();

                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        if (isAuditmanager == "AM" || isAuditmanager == "AH")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || st.ID == 2)
                                {
                                }
                                else
                                {
                                    ddlStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || st.ID == 6 || st.ID == 2)
                                {
                                }
                                else
                                {
                                    ddlStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 6) || st.ID == 2)
                            {
                            }
                            else
                            {
                                ddlStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "4")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 5))
                            {
                                if (!(st.ID == 2))
                                {
                                    ddlStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {

                                ddlStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));

                            }
                            else
                            {

                                if (!(st.ID == 5))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        ddlStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    }
                                }
                            }
                        }
                    }
                }//Personresponsible End
                else
                {
                    foreach (InternalAuditStatu st in allowedStatusList)
                    {
                        if (!(st.ID == 6))
                        {
                            if (!(st.ID == 2))
                            {
                                ddlStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + ddlProcess.SelectedValue + ";" + ddlSubProcess.SelectedValue + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["AuditID"];
            BindDetailView(ViewState["Arguments"].ToString(), "P");
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + ddlProcess.SelectedValue + ";" + ddlSubProcess.SelectedValue + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["peroid"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["AuditID"];
            BindDetailView(ViewState["Arguments"].ToString(), "P");
            bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindProcess(string flag, int Customerid, int AuditID)
        {
            try
            {
                int PID = 0;
                if (ViewState["ProcessId"] != null)
                {
                    PID = Convert.ToInt32(ViewState["ProcessId"]);
                }
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManagerSummary("P", Customerid, AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                    if (PID > 0)
                    {
                        ddlProcess.SelectedValue = PID.ToString();
                    }
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManagerSummary("N", Customerid, AuditID, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;

            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindDetailView(ViewState["Arguments"].ToString(), "P");

        }
        protected void btnAllsave_click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                    }
                }
                if (chkValidationflag > 0)
                {
                    string observatiofinal = string.Empty;
                    string workDone = string.Empty;
                    string remarkreview = string.Empty;
                    roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    if (ddlStatus.SelectedValue != "")
                    {
                        foreach (GridViewRow rw in grdSummaryDetailsAuditCoverage.Rows)
                        {
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                            if (chkBx != null && chkBx.Checked)
                            {
                                #region
                                workDone = ((TextBox)rw.FindControl("txtWorkDone")).Text;
                                remarkreview = ((TextBox)rw.FindControl("txtRemark")).Text;
                                observatiofinal = ((Label)rw.FindControl("LblObservation")).Text;

                                LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                                String Args = chklnkbutton.CommandArgument.ToString();
                                string[] arg = Args.ToString().Split(',');
                                int ATBDid = Convert.ToInt32(arg[0]);
                                int customerbranchid = Convert.ToInt32(arg[1]);
                                string FinancialYear = arg[2].ToString();
                                string period = arg[3].ToString();
                                int verticalId = Convert.ToInt32(arg[6]);
                                int AuditID = Convert.ToInt32(arg[7]);
                                #endregion
                                long roleid = 4;
                                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinancialYear, period, Convert.ToInt32(customerbranchid), Convert.ToInt32(ATBDid), Convert.ToInt32(verticalId), AuditID);
                                if (getscheduleondetails != null)
                                {
                                    if (ddlStatus.SelectedValue != "")
                                    {
                                        int StatusID = 0;
                                        StatusID = Convert.ToInt32(ddlStatus.SelectedValue);

                                        #region InternalControlAuditResult
                                        InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                        {
                                            AuditScheduleOnID = getscheduleondetails.ID,
                                            ProcessId = getscheduleondetails.ProcessId,
                                            FinancialYear = FinancialYear,
                                            ForPerid = period,
                                            CustomerBranchId = Convert.ToInt32(customerbranchid),
                                            IsDeleted = false,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            ATBDId = Convert.ToInt32(ATBDid),
                                            RoleID = roleid,
                                            InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                            VerticalID = verticalId,
                                            AuditID = AuditID,
                                        };
                                        #endregion

                                        #region InternalAuditTransaction
                                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                                        {
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                                            AuditScheduleOnID = getscheduleondetails.ID,
                                            FinancialYear = FinancialYear,
                                            CustomerBranchId = Convert.ToInt32(customerbranchid),
                                            InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                            ProcessId = getscheduleondetails.ProcessId,
                                            ForPeriod = period,
                                            ATBDId = Convert.ToInt32(ATBDid),
                                            RoleID = roleid,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            VerticalID = Convert.ToInt32(verticalId),
                                            AuditID = AuditID,
                                        };
                                        #endregion

                                        #region Added by rahul on 23 MAY 2017

                                        ComplianceManagement.Business.DataRisk.AuditClosure AuditClosureResult = new ComplianceManagement.Business.DataRisk.AuditClosure()
                                        {
                                            ProcessId = getscheduleondetails.ProcessId,
                                            FinancialYear = FinancialYear,
                                            ForPeriod = period,
                                            CustomerbranchId = Convert.ToInt32(customerbranchid),
                                            ACStatus = 1,
                                            AuditCommiteRemark = "",
                                            ATBDId = Convert.ToInt32(ATBDid),
                                            VerticalID = Convert.ToInt32(verticalId),
                                            AuditCommiteFlag = 0,
                                            AuditID = AuditID,
                                        };
                                        #endregion

                                        #region remark
                                        string remark = string.Empty;
                                        if (ddlStatus.SelectedItem.Text == "Closed")
                                        {
                                            transaction.StatusId = 3;
                                            MstRiskResult.AStatusId = 3;
                                            remark = "Audit Steps Closed.";
                                        }
                                        else if (ddlStatus.SelectedItem.Text == "Team Review")
                                        {
                                            transaction.StatusId = 4;
                                            MstRiskResult.AStatusId = 4;
                                            remark = "Audit Steps Under Team Review.";
                                            MstRiskResult.AuditeeResponse = "HT";
                                        }
                                        else if (ddlStatus.SelectedItem.Text == "Final Review")
                                        {
                                            transaction.StatusId = 5;
                                            MstRiskResult.AStatusId = 5;
                                            remark = "Audit Steps Under Final Review.";
                                            MstRiskResult.AuditeeResponse = "HF";
                                        }
                                        else if (ddlStatus.SelectedItem.Text == "Auditee Review")
                                        {
                                            transaction.StatusId = 6;
                                            MstRiskResult.AStatusId = 6;
                                            remark = "Audit Steps Under Auditee Review.";
                                            MstRiskResult.AuditeeResponse = "RS";
                                        }
                                        #endregion


                                        if (workDone.Trim() == "")
                                        {
                                            MstRiskResult.ActivityToBeDone = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.ActivityToBeDone = workDone.ToString();
                                        }
                                        if (remarkreview.Trim() == "")
                                        {
                                            MstRiskResult.FixRemark = "";
                                        }
                                        else
                                        {
                                            MstRiskResult.FixRemark = remarkreview.ToString();
                                        }
                                        #region
                                        string Observation = string.Empty;
                                        var MstRiskResultchk = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(getscheduleondetails.ID), Convert.ToInt32(ATBDid), Convert.ToInt32(verticalId), AuditID);
                                        if (MstRiskResultchk != null)
                                        {

                                            if (string.IsNullOrEmpty(MstRiskResultchk.AuditObjective))
                                                MstRiskResult.AuditObjective = null;
                                            else
                                                MstRiskResult.AuditObjective = MstRiskResultchk.AuditObjective;

                                            if (string.IsNullOrEmpty(MstRiskResultchk.AuditSteps))
                                                MstRiskResult.AuditSteps = "";
                                            else
                                                MstRiskResult.AuditSteps = MstRiskResultchk.AuditSteps;

                                            if (string.IsNullOrEmpty(MstRiskResultchk.AnalysisToBePerofrmed))

                                                MstRiskResult.AnalysisToBePerofrmed = "";
                                            else
                                                MstRiskResult.AnalysisToBePerofrmed = MstRiskResultchk.AnalysisToBePerofrmed;


                                            if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                                MstRiskResult.ProcessWalkthrough = "";
                                            else
                                                MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;

                                            if (string.IsNullOrEmpty(MstRiskResultchk.ActivityToBeDone))

                                                MstRiskResult.ActivityToBeDone = "";
                                            else
                                                MstRiskResult.ActivityToBeDone = MstRiskResultchk.ActivityToBeDone;
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                                MstRiskResult.Population = "";
                                            else
                                                MstRiskResult.Population = MstRiskResultchk.Population;
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                                MstRiskResult.Sample = "";
                                            else
                                                MstRiskResult.Sample = MstRiskResultchk.Sample;

                                            if (string.IsNullOrEmpty(MstRiskResultchk.ObservationNumber))
                                            {
                                                MstRiskResult.ObservationNumber = "";
                                                AuditClosureResult.ObservationNumber = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ObservationNumber = MstRiskResultchk.ObservationNumber;
                                                AuditClosureResult.ObservationNumber = MstRiskResultchk.ObservationNumber;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ObservationTitle))
                                            {
                                                MstRiskResult.ObservationTitle = "";
                                                AuditClosureResult.ObservationTitle = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ObservationTitle = MstRiskResultchk.ObservationTitle;
                                                AuditClosureResult.ObservationTitle = MstRiskResultchk.ObservationTitle;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Observation))
                                            {
                                                MstRiskResult.Observation = "";
                                                AuditClosureResult.Observation = "";
                                            }
                                            else
                                            {
                                                Observation = MstRiskResultchk.Observation;
                                                MstRiskResult.Observation = MstRiskResultchk.Observation;
                                                AuditClosureResult.Observation = MstRiskResultchk.Observation;
                                            }
                                            if (MstRiskResultchk.ISACPORMIS != null)
                                            {
                                                MstRiskResult.ISACPORMIS = MstRiskResultchk.ISACPORMIS;
                                                AuditClosureResult.ISACPORMIS = MstRiskResultchk.ISACPORMIS;
                                            }
                                            else
                                            {
                                                MstRiskResult.ISACPORMIS = null;
                                                AuditClosureResult.ISACPORMIS = null;
                                            }
                                            if (!string.IsNullOrEmpty(MstRiskResultchk.BriefObservation))
                                            {
                                                MstRiskResult.BriefObservation = MstRiskResultchk.BriefObservation;
                                                transaction.BriefObservation = MstRiskResultchk.BriefObservation;
                                                AuditClosureResult.BriefObservation = MstRiskResultchk.BriefObservation;
                                            }
                                            else
                                            {
                                                MstRiskResult.BriefObservation = null;
                                                transaction.BriefObservation = null;
                                                AuditClosureResult.BriefObservation = null;
                                            }
                                            if (!string.IsNullOrEmpty(MstRiskResultchk.ObjBackground))
                                            {
                                                MstRiskResult.ObjBackground = MstRiskResultchk.ObjBackground;
                                                transaction.ObjBackground = MstRiskResultchk.ObjBackground;
                                                AuditClosureResult.ObjBackground = MstRiskResultchk.ObjBackground;
                                            }
                                            else
                                            {
                                                MstRiskResult.ObjBackground = null;
                                                transaction.ObjBackground = null;
                                                AuditClosureResult.ObjBackground = null;
                                            }
                                            if (MstRiskResultchk.DefeciencyType != null)
                                            {
                                                MstRiskResult.DefeciencyType = MstRiskResultchk.DefeciencyType;
                                                transaction.DefeciencyType = MstRiskResultchk.DefeciencyType;
                                                AuditClosureResult.DefeciencyType = MstRiskResultchk.DefeciencyType;
                                            }
                                            else
                                            {
                                                MstRiskResult.DefeciencyType = null;
                                                transaction.DefeciencyType = null;
                                                AuditClosureResult.DefeciencyType = null;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Risk))
                                            {
                                                MstRiskResult.Risk = "";
                                                AuditClosureResult.Risk = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Risk = MstRiskResultchk.Risk;
                                                AuditClosureResult.Risk = MstRiskResultchk.Risk;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.RootCost))
                                            {
                                                MstRiskResult.RootCost = null;
                                                AuditClosureResult.RootCost = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.RootCost = MstRiskResultchk.RootCost;
                                                AuditClosureResult.RootCost = MstRiskResultchk.RootCost;
                                            }

                                            if (MstRiskResultchk.AuditScores == null)
                                                MstRiskResult.AuditScores = null;
                                            else
                                                MstRiskResult.AuditScores = Convert.ToDecimal(MstRiskResultchk.AuditScores);

                                            if (MstRiskResultchk.FinancialImpact == null)
                                            {
                                                MstRiskResult.FinancialImpact = null;
                                                AuditClosureResult.FinancialImpact = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.FinancialImpact = MstRiskResultchk.FinancialImpact;
                                                AuditClosureResult.FinancialImpact = MstRiskResultchk.FinancialImpact;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Recomendation))
                                            {
                                                MstRiskResult.Recomendation = "";
                                                AuditClosureResult.Recomendation = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Recomendation = MstRiskResultchk.Recomendation;
                                                AuditClosureResult.Recomendation = MstRiskResultchk.Recomendation;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ManagementResponse))
                                            {
                                                MstRiskResult.ManagementResponse = "";
                                                AuditClosureResult.ManagementResponse = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ManagementResponse = MstRiskResultchk.ManagementResponse;
                                                AuditClosureResult.ManagementResponse = MstRiskResultchk.ManagementResponse;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                                MstRiskResult.FixRemark = "";
                                            else
                                                MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;

                                            //Code added by Sushant
                                            if (MstRiskResultchk.BodyContent == "")
                                            {
                                                MstRiskResult.BodyContent = "";
                                                transaction.BodyContent = "";
                                                AuditClosureResult.BodyContent = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.BodyContent = MstRiskResultchk.BodyContent;
                                                transaction.BodyContent = MstRiskResultchk.BodyContent;
                                                AuditClosureResult.BodyContent = MstRiskResultchk.BodyContent;
                                            }
                                            if (MstRiskResultchk.UHComment == "")
                                            {
                                                MstRiskResult.UHComment = "";
                                                transaction.UHComment = "";
                                                AuditClosureResult.UHComment = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.UHComment = MstRiskResultchk.UHComment;
                                                transaction.UHComment = MstRiskResultchk.UHComment;
                                                AuditClosureResult.UHComment = MstRiskResultchk.UHComment;
                                            }

                                            if (MstRiskResultchk.PRESIDENTComment == "")
                                            {
                                                MstRiskResult.PRESIDENTComment = "";
                                                transaction.PRESIDENTComment = "";
                                                AuditClosureResult.PRESIDENTComment = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                                transaction.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                                AuditClosureResult.PRESIDENTComment = MstRiskResultchk.PRESIDENTComment;
                                            }

                                            if (MstRiskResultchk.UHPersonResponsible != null)
                                            {
                                                MstRiskResult.UHPersonResponsible = null;
                                                transaction.UHPersonResponsible = null;
                                                AuditClosureResult.UHPersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                                transaction.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                                AuditClosureResult.UHPersonResponsible = MstRiskResultchk.UHPersonResponsible;
                                            }

                                            if (MstRiskResultchk.UHPersonResponsible != null)
                                            {
                                                MstRiskResult.PRESIDENTPersonResponsible = null;
                                                transaction.PRESIDENTPersonResponsible = null;
                                                AuditClosureResult.PRESIDENTPersonResponsible = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                                transaction.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                                AuditClosureResult.PRESIDENTPersonResponsible = MstRiskResultchk.PRESIDENTPersonResponsible;
                                            }
                                            //End


                                            DateTime dt = new DateTime();
                                            if (MstRiskResultchk.TimeLine != null)
                                            {
                                                string s = MstRiskResultchk.TimeLine != null ? MstRiskResultchk.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                                                dt = DateTime.ParseExact(s, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                MstRiskResult.TimeLine = dt.Date;
                                                AuditClosureResult.TimeLine = dt.Date;
                                            }
                                            else
                                            {
                                                MstRiskResult.TimeLine = null;
                                                AuditClosureResult.TimeLine = null;
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.PersonResponsible)))
                                            {
                                                if (MstRiskResultchk.PersonResponsible == -1)
                                                {
                                                    MstRiskResult.PersonResponsible = null;
                                                    transaction.PersonResponsible = null;
                                                    AuditClosureResult.PersonResponsible = null;
                                                }
                                                else
                                                {
                                                    MstRiskResult.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                    transaction.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                    AuditClosureResult.PersonResponsible = Convert.ToInt32(MstRiskResultchk.PersonResponsible);
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.Owner)))
                                            {
                                                if (MstRiskResultchk.Owner == -1)
                                                {
                                                    MstRiskResult.Owner = null;
                                                    transaction.Owner = null;
                                                    AuditClosureResult.Owner = null;
                                                }
                                                else
                                                {
                                                    MstRiskResult.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                    transaction.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                    AuditClosureResult.Owner = Convert.ToInt32(MstRiskResultchk.Owner);
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationRating)))
                                            {
                                                if (MstRiskResultchk.ObservationRating == -1)
                                                {
                                                    transaction.ObservatioRating = null;
                                                    MstRiskResult.ObservationRating = null;
                                                    AuditClosureResult.ObservationRating = null;
                                                }
                                                else
                                                {
                                                    transaction.ObservatioRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                    MstRiskResult.ObservationRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                    AuditClosureResult.ObservationRating = Convert.ToInt32(MstRiskResultchk.ObservationRating);
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationCategory)))
                                            {
                                                if (MstRiskResultchk.ObservationCategory == -1)
                                                {
                                                    transaction.ObservationCategory = null;
                                                    MstRiskResult.ObservationCategory = null;
                                                    AuditClosureResult.ObservationCategory = null;
                                                }
                                                else
                                                {
                                                    transaction.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                    MstRiskResult.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                    AuditClosureResult.ObservationCategory = Convert.ToInt32(MstRiskResultchk.ObservationCategory);
                                                }
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.ObservationSubCategory)))
                                            {
                                                if (MstRiskResultchk.ObservationSubCategory == -1)
                                                {
                                                    transaction.ObservationSubCategory = null;
                                                    MstRiskResult.ObservationSubCategory = null;
                                                    AuditClosureResult.ObservationSubCategory = null;
                                                }
                                                else
                                                {
                                                    transaction.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                    MstRiskResult.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                    AuditClosureResult.ObservationSubCategory = Convert.ToInt32(MstRiskResultchk.ObservationSubCategory);
                                                }
                                            }

                                            // added by sagar more on 10-01-2020
                                            if (string.IsNullOrEmpty(MstRiskResultchk.AnnexueTitle))
                                            {
                                                MstRiskResult.AnnexueTitle = "";
                                                AuditClosureResult.AnnexueTitle = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.AnnexueTitle = MstRiskResultchk.AnnexueTitle;
                                                AuditClosureResult.AnnexueTitle = MstRiskResultchk.AnnexueTitle;
                                            }

                                        }

                                        transaction.Remarks = remark.Trim();
                                        #endregion
                                        bool Success1 = false;
                                        bool Success2 = false;
                                        bool Success3 = false;
                                        bool Success4 = false;

                                        if (ddlStatus.SelectedItem.Text == "Closed")
                                        {
                                            MstRiskResult.AuditeeResponse = "C";
                                        }
                                        if (!string.IsNullOrEmpty(observatiofinal))
                                        {

                                            if (!string.IsNullOrEmpty(MstRiskResult.ManagementResponse)
                                            && MstRiskResult.TimeLine != null
                                            && !string.IsNullOrEmpty(Convert.ToString(MstRiskResultchk.PersonResponsible)))
                                            {
                                                #region If Observation Code
                                                if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                                {
                                                    MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    MstRiskResult.UpdatedOn = DateTime.Now;
                                                    Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                                }
                                                else
                                                {
                                                    MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    MstRiskResult.CreatedOn = DateTime.Now;
                                                    Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                                }
                                                if (ddlStatus.SelectedItem.Text == "Closed")
                                                {
                                                    #region  AuditClosure 
                                                    int customerID = -1;
                                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                    Success4 = RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                                        Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                                        MstRiskResult.ForPerid, MstRiskResult.FinancialYear, AuditID);

                                                    if (!string.IsNullOrEmpty(Observation))
                                                    {
                                                        using (AuditControlEntities entities = new AuditControlEntities())
                                                        {

                                                            var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                                  where row.ProcessId == MstRiskResult.ProcessId
                                                                                  && row.FinancialYear == MstRiskResult.FinancialYear
                                                                                  && row.ForPerid == MstRiskResult.ForPerid
                                                                                  && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                                  && row.UserID == MstRiskResult.UserID
                                                                                  && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                                  && row.VerticalID == MstRiskResult.VerticalID
                                                                                  && row.AuditID == MstRiskResult.AuditID
                                                                                  select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                            if (RecordtoUpdate != null)
                                                            {
                                                                AuditClosureResult.ResultID = RecordtoUpdate;
                                                                AuditClosureResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                AuditClosureResult.CreatedOn = DateTime.Now;
                                                                Success3 = RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }

                                                if (RiskCategoryManagement.InternalAuditTxnExistsAuditManager(transaction) && Convert.ToInt32(ddlFilterStatus.SelectedValue) != 5)
                                                {
                                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.UpdatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewerAuditmanager(transaction);
                                                }
                                                else
                                                {
                                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.CreatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                                }
                                                if (Success1 == true && Success2 == true)
                                                {
                                                    #region
                                                    if (ddlStatus.SelectedItem.Text == "Closed")
                                                    {
                                                        if (!string.IsNullOrEmpty(Observation))
                                                        {
                                                            if (Success3 == true)
                                                            {
                                                                if (ddlStatus.SelectedItem.Text == "Closed")
                                                                {
                                                                    cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                cvDuplicateEntry.IsValid = false;
                                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (ddlStatus.SelectedItem.Text == "Closed")
                                                            {
                                                                cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {

                                                        if (ddlStatus.SelectedItem.Text == "Closed")
                                                        {
                                                            cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                                        }
                                                        else if (ddlStatus.SelectedItem.Text == "Team Review")
                                                        {
                                                            cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                                                        }
                                                        else if (ddlStatus.SelectedItem.Text == "Final Review")
                                                        {
                                                            cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                                                        }
                                                        else if (ddlStatus.SelectedItem.Text == "Auditee Review")
                                                        {
                                                            cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Please Provide Management Response ,Timeline and Person Responsible  Before Save";
                                            }
                                        }
                                        else
                                        {
                                            #region If No Observation Code
                                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                            {
                                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                            }
                                            else
                                            {
                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                            }
                                            if (ddlStatus.SelectedItem.Text == "Closed")
                                            {
                                                #region  AuditClosure 
                                                int customerID = -1;
                                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                Success4 = RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                                    Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                                    MstRiskResult.ForPerid, MstRiskResult.FinancialYear, AuditID);

                                                if (!string.IsNullOrEmpty(Observation))
                                                {
                                                    using (AuditControlEntities entities = new AuditControlEntities())
                                                    {

                                                        var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                              where row.ProcessId == MstRiskResult.ProcessId
                                                                              && row.FinancialYear == MstRiskResult.FinancialYear
                                                                              && row.ForPerid == MstRiskResult.ForPerid
                                                                              && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                              && row.UserID == MstRiskResult.UserID
                                                                              && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                              && row.VerticalID == MstRiskResult.VerticalID
                                                                              && row.AuditID == MstRiskResult.AuditID
                                                                              select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                        if (RecordtoUpdate != null)
                                                        {
                                                            AuditClosureResult.ResultID = RecordtoUpdate;
                                                            AuditClosureResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            AuditClosureResult.CreatedOn = DateTime.Now;
                                                            Success3 = RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }

                                            if (RiskCategoryManagement.InternalAuditTxnExistsAuditManager(transaction) && Convert.ToInt32(ddlFilterStatus.SelectedValue) != 5)
                                            {
                                                transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.UpdatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewerAuditmanager(transaction);
                                            }
                                            else
                                            {
                                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.CreatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                            }
                                            if (Success1 == true && Success2 == true)
                                            {
                                                #region
                                                if (ddlStatus.SelectedItem.Text == "Closed")
                                                {
                                                    if (!string.IsNullOrEmpty(Observation))
                                                    {
                                                        if (Success3 == true)
                                                        {
                                                            if (ddlStatus.SelectedItem.Text == "Closed")
                                                            {
                                                                cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (ddlStatus.SelectedItem.Text == "Closed")
                                                        {
                                                            cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                                        }
                                                    }
                                                }
                                                else
                                                {

                                                    if (ddlStatus.SelectedItem.Text == "Closed")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Audit Steps Closed Successfully";
                                                    }
                                                    else if (ddlStatus.SelectedItem.Text == "Team Review")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                                                    }
                                                    else if (ddlStatus.SelectedItem.Text == "Final Review")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                                                    }
                                                    else if (ddlStatus.SelectedItem.Text == "Auditee Review")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Select Status";
                                    }
                                }
                            }
                        }//END FOR EACH

                        //Code added by Sushant
                        #region Audit Close 
                        if (ddlStatus.SelectedValue == "3")
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["peroid"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                            {
                                                if (RiskCategoryManagement.CheckAllStepClosed(Convert.ToInt64(Request.QueryString["AuditID"])))
                                                {
                                                    AuditClosureClose auditclosureclose = new AuditClosureClose();
                                                    auditclosureclose.CustomerID = CustomerId;
                                                    auditclosureclose.CustomerBranchId = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                                                    auditclosureclose.FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                                                    auditclosureclose.ForPeriod = Convert.ToString(Request.QueryString["peroid"]);
                                                    auditclosureclose.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    auditclosureclose.CreatedDate = DateTime.Today.Date;
                                                    auditclosureclose.ACCStatus = 1;
                                                    auditclosureclose.VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                                                    auditclosureclose.AuditId = Convert.ToInt64(Request.QueryString["AuditID"]);
                                                    if (!RiskCategoryManagement.AuditClosureCloseExists(auditclosureclose.CustomerBranchId, (int)auditclosureclose.VerticalID, auditclosureclose.FinancialYear, auditclosureclose.ForPeriod, Convert.ToInt32(Request.QueryString["AuditID"])))
                                                    {
                                                        RiskCategoryManagement.CreateAuditClosureClosed(auditclosureclose);
                                                    }
                                                }
                                            }
                        }
                        #endregion
                        BindDetailView(ViewState["Arguments"].ToString(), "P");
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Status";
                    }
                }
                else
                {
                    //cvDuplicateEntry
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one step.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //private void ProcessReminderOnAuditManagerAuditReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid)
        //{
        //    try
        //    {
        //        long userid = GetReviewer(ForPeriod, FinancialYear, ScheduledOnID, processid);
        //        var user = UserManagement.GetByID(Convert.ToInt32(userid));
        //        if (user != null)
        //        {
        //            int customerID = -1;
        //            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
        //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

        //            string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;

        //            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //            string message = Properties.Settings.Default.EmailTemplate_AuditManagerAuditReviewer
        //                 .Replace("@User", username)
        //                 .Replace("@PeriodName", ForPeriod)
        //                 .Replace("@FinancialYear", FinancialYear)
        //                 .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                 .Replace("@From", ReplyEmailAddressName);

        //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Audit Reminder on Final review", message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        //public static long GetReviewer(string ForPeriod, string FinancialYear, int ScheduledOnID, int processid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var statusList = (from row in entities.InternalControlAuditAssignments
        //                          join row1 in entities.InternalAuditScheduleOns
        //                          on row.ProcessId equals row1.ProcessId
        //                          where row.InternalAuditInstance == row1.InternalAuditInstance && row.ProcessId == processid &&
        //                          row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
        //                          && row1.ID == ScheduledOnID && row.RoleID == 4
        //                          select row.UserID).FirstOrDefault();

        //        return (long)statusList;
        //    }
        //}

        public string ShowObservation(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string Observation = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(ATBDID), VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                Observation = MstRiskResult.Observation;
            }
            return Observation;
        }

        public string ShowWorkDone(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string workdone = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(ATBDID), VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                workdone = MstRiskResult.ActivityToBeDone;
            }
            return workdone;
        }

        public string ShowRemark(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string remarks = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(ATBDID), VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                remarks = MstRiskResult.FixRemark;
            }
            return remarks;
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url3 = "";
            string url4 = "";
            int PID = -1;
            int SPID = -1;
            int PageSize = -1;
            int gridpagesize = -1;
            int AuditID = 0;
            int BoPSize = 0;
            string financialyear = string.Empty; ;
            if (!String.IsNullOrEmpty(Request.QueryString["chkPID"]))
            {
                PID = Convert.ToInt32(Request.QueryString["chkPID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["chkSPID"]))
            {
                SPID = Convert.ToInt32(Request.QueryString["chkSPID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["PageSize"]))
            {
                PageSize = Convert.ToInt32(Request.QueryString["PageSize"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["gridpagesize"]))
            {
                gridpagesize = Convert.ToInt32(Request.QueryString["gridpagesize"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["BoPSize"]))
            {
                BoPSize = Convert.ToInt32(Request.QueryString["BoPSize"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
            {
                financialyear = Convert.ToString(Request.QueryString["FY"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["returnUrl1"]))
            {
                url4 = "&returnUrl1=" + Request.QueryString["returnUrl1"] + "&PID=" + PID + "&SPID=" + SPID + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize + "&AuditID=" + AuditID + "&FY=" + financialyear;
            }

            if (!String.IsNullOrEmpty(Request.QueryString["AH"]))
            {
                Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Type=Process&Status=Open");
            }
            else
            {
                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?" + url4);
            }
        }


        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ");", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }
        public bool ATBDVisibleorNot(int? StatusID)
        {

            if (StatusID == null || StatusID == 2)
                return false;
            else
                return true;

        }


        private void BindDetailView(string Arguments, String IFlag)
        {
            try
            {
                int ProcessId = -1;
                int Subprocessid = -1;
                int BranchID = -1;
                string financialyear = String.Empty;
                string period = string.Empty;
                string filter = String.Empty;
                int VerticalID = -1;
                int InstanceID = -1;
                long AuditID = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string[] arg = Arguments.ToString().Split(';');

                if (arg.Length > 0)
                {
                    InstanceID = Convert.ToInt32(arg[1]);
                    ProcessId = Convert.ToInt32(arg[3]);
                    Subprocessid = Convert.ToInt32(arg[4]);
                    BranchID = Convert.ToInt32(arg[5]);
                    VerticalID = Convert.ToInt32(arg[6]);
                    period = arg[7].ToString();
                    financialyear = arg[8].ToString();

                    int userid = -1;
                    userid = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                    List<int> statusIds = new List<int>();
                    List<int?> statusNullableIds = new List<int?>();

                    filter = arg[0].Trim();

                    if (arg[0].Trim().Equals("NotDone"))
                    {
                        statusIds.Add(-1);
                    }
                    else if (arg[0].Trim().Equals("Submited"))
                    {
                        statusIds.Add(2);
                    }
                    else if (arg[0].Trim().Equals("TeamReview"))
                    {
                        statusIds.Add(4);
                    }
                    else if (arg[0].Trim().Equals("Closed"))
                    {
                        statusIds.Add(3);
                    }
                    else if (arg[0].Trim().Equals("AuditeeReview"))
                    {
                        statusIds.Add(6);
                    }
                    else if (arg[0].Trim().Equals("FinalReview"))
                    {
                        statusIds.Add(5);
                    }
                    else
                    {
                        statusIds.Add(1);
                        statusIds.Add(4);
                        statusIds.Add(2);
                        statusIds.Add(3);
                        statusIds.Add(5);
                        statusIds.Add(6);
                    }
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        AuditID = Convert.ToInt64(Request.QueryString["AuditID"]);
                    }
                    if (AuditID != -1 && AuditID != 0)
                    {
                        if (IFlag == "P")
                        {
                            var detailView = InternalControlManagementDashboardRisk.GetAuditManagerDetailUserWise(customerID, BranchID, VerticalID, financialyear, ProcessId, Subprocessid, InstanceID, statusIds, statusNullableIds, filter, period, userid, AuditID);
                            grdSummaryDetailsAuditCoverage.DataSource = detailView;
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetailsAuditCoverage.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //Reload the Grid
                BindDetailView(ViewState["Arguments"].ToString(), "P");
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSummaryDetailsAuditCoverage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int CBranchID = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                bool checkPRFlag = false;
                checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(Convert.ToInt32(customerID), Convert.ToInt32(CBranchID));
                var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CBranchID);
                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                string statusidasd = Request.QueryString["Status"].ToString();
                if (checkPRFlag == true)
                {
                    bool checkPRRiskActivationFlag = false;
                    checkPRRiskActivationFlag = CustomerManagementRisk.CheckPersonResponsibleFlowApplicable(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CBranchID);

                    if (litrole.Contains(4)) // If Audit Manager IS Reviewer
                    {
                        #region If Audit Manager IS Reviewer
                        if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                        {
                            CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                            chkBx.Enabled = false;

                            TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                            txtWorkDone.Enabled = false;

                            TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                            txtRemark.Enabled = false;
                        }
                        else
                        {
                            CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                            chkBx.Enabled = true;

                            TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                            txtWorkDone.Enabled = true;

                            TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                            txtRemark.Enabled = true;
                        }
                        #endregion
                    }
                    //if Audit Manager Is PersonResponsible
                    else if ((AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH") && checkPRRiskActivationFlag)
                    {
                        if (statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                        {
                            CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                            chkBx.Enabled = false;

                            TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                            txtWorkDone.Enabled = false;

                            TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                            txtRemark.Enabled = false;
                        }
                        else
                        {
                            if (litrole.Contains(5))
                            {
                                CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                                chkBx.Enabled = true;

                                TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                                txtWorkDone.Enabled = true;

                                TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                                txtRemark.Enabled = true;
                            }
                            else
                            {
                                CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                                chkBx.Enabled = false;

                                TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                                txtWorkDone.Enabled = false;

                                TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                                txtRemark.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                        {
                            CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                            chkBx.Enabled = false;

                            TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                            txtWorkDone.Enabled = false;

                            TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                            txtRemark.Enabled = false;
                        }
                        else
                        {
                            CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                            chkBx.Enabled = true;

                            TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                            txtWorkDone.Enabled = true;

                            TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                            txtRemark.Enabled = true;
                        }
                    }
                }
                else
                {
                    if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                    {
                        CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                        chkBx.Enabled = false;

                        TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = false;

                        TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                        txtRemark.Enabled = false;
                    }
                    else
                    {
                        CheckBox chkBx = (CheckBox)e.Row.FindControl("CheckBox1");
                        chkBx.Enabled = true;

                        TextBox txtWorkDone = (TextBox)e.Row.FindControl("txtWorkDone");
                        txtWorkDone.Enabled = true;

                        TextBox txtRemark = (TextBox)e.Row.FindControl("txtRemark");
                        txtRemark.Enabled = true;
                    }
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (statusidasd == "TeamReview")
                    {
                        int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        Label txtstepId = (Label)e.Row.FindControl("lblATBDID");
                        Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                        Label lblcolor = (Label)e.Row.FindControl("LActivityTobeDone");
                        int ScheduledOnID = Convert.ToInt32(lblScheduledOnID.Text);

                        var User = (from row in MasterInternalAuditTransactionView
                                    where row.AuditScheduleOnID == ScheduledOnID
                                    && row.AuditID == AuditID
                                    && row.ATBDId == Convert.ToInt32(txtstepId.Text)
                                    select row).OrderByDescending(el => el.Dated).FirstOrDefault();

                        var ListPerformer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 3 select row.UserID).FirstOrDefault();
                        var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                        if (User != null)
                        {
                            if (ListPerformer == User.CreatedBy)
                            {
                                lblcolor.Style.Add("color", "black");
                            }
                            else if (ListReviewer == User.CreatedBy)
                            {
                                lblcolor.Style.Add("color", "red");
                            }
                        }
                    }
                    else if (statusidasd == "AuditeeReview")
                    {
                        int AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                        Label txtstepId = (Label)e.Row.FindControl("lblATBDID");
                        Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                        Label lblcolor = (Label)e.Row.FindControl("LActivityTobeDone");
                        int ScheduledOnID = Convert.ToInt32(lblScheduledOnID.Text);

                        var User = (from row in MasterInternalAuditTransactionView
                                    where row.AuditScheduleOnID == ScheduledOnID
                                    && row.AuditID == AuditID
                                    && row.ATBDId == Convert.ToInt32(txtstepId.Text)
                                    select row).OrderByDescending(el => el.Dated).FirstOrDefault();
                        var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                        if (User != null)
                        {
                            if (ListReviewer == User.CreatedBy)
                            {
                                lblcolor.Style.Add("color", "blue");
                            }
                            else
                            {
                                lblcolor.Style.Add("color", "black");
                            }
                        }
                    }
                }
            }
        }

        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        //BindData("P");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        //BindData("P");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}       
    }
}