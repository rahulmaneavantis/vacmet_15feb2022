﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditPlanning : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomer();
                BindData("P");
                GetPageDisplaySummary();
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem(" Select Location ", "-1"));
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("P");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("N");
            }
        }
        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("P");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("N");
            }
        }
        

     
        private void BindData(string Flag)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (Flag == "P")
                {
                    var Riskcategorymanagementlist = ProcessManagement.GetAllProcess("P", customerID);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();

                }
                else
                {
                    var Riskcategorymanagementlist = ProcessManagement.GetAllProcess("N", customerID);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void Add(object sender, EventArgs e)
        {
            try
            {
                //Get the button that raised the event
                Button btn = (Button)sender;

                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                //Control control = null;
                //if (grdRiskActivityMatrix.FooterRow != null)
                //{
                //    control = grdRiskActivityMatrix.FooterRow;
                //}
                //else
                //{
                //    control = grdRiskActivityMatrix.Controls[0].Controls[0];
                //}
                string lblProcessID = (gvr.FindControl("lblProcessID") as Label).Text;
                string lblProcess = (gvr.FindControl("lblProcess") as Label).Text;
                string ddlResponsiblePerson = (gvr.FindControl("ddlResponsiblePerson") as DropDownList).SelectedItem.Value;
                string ddlPeriod = (gvr.FindControl("ddlPeriod") as DropDownList).SelectedItem.Value;                  
                if (!String.IsNullOrEmpty(ddlResponsiblePerson))
                {
                    if (!String.IsNullOrEmpty(ddlPeriod))
                    {
                        if (ddlResponsiblePerson != " Select Responsible Person ")
                        {
                            if (ddlResponsiblePerson != " Select Period ")
                            {
                                Mst_AuditPlanning MstAuditPlanning = new Mst_AuditPlanning()
                                {             
                                    ProcessId =Convert.ToInt32(lblProcessID),
                                    Personresponsible = Convert.ToInt32(ddlResponsiblePerson),
                                    Period = Convert.ToInt32(ddlPeriod),  
                                    IsActive=false,                              
                                };
                                if (RiskCategoryManagement.Exists(MstAuditPlanning))
                                {
                                    cvDuplicateEntry.ErrorMessage = "Audit Planning already exists.";
                                    cvDuplicateEntry.IsValid = false;
                                    return;
                                }
                                using (AuditControlEntities entities = new AuditControlEntities())
                                {
                                    entities.Mst_AuditPlanning.Add(MstAuditPlanning);
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                }
                this.BindData("P");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationProcess\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                DropDownList ddlResponsiblePerson = (e.Row.FindControl("ddlResponsiblePerson") as DropDownList);
                if (ddlResponsiblePerson != null)
                {
                    ddlResponsiblePerson.DataTextField = "Name";
                    ddlResponsiblePerson.DataValueField = "ID";
                    ddlResponsiblePerson.DataSource = RiskCategoryManagement.FillResponsiblePerson();
                    ddlResponsiblePerson.DataBind();
                    ddlResponsiblePerson.Items.Insert(0, new ListItem(" Select Responsible Person ", "-1"));
                }

                DropDownList ddlPeriod = (e.Row.FindControl("ddlPeriod") as DropDownList);
                if (ddlPeriod != null)
                {
                    ddlPeriod.DataTextField = "Name";
                    ddlPeriod.DataValueField = "ID";
                    ddlPeriod.DataSource = RiskCategoryManagement.FillPeriod();
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Insert(0, new ListItem(" Select Period ", "-1"));
                }                                              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
        //protected void grdRiskActivityMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //}
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
                BindData("P");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindData("P");
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindData("P");
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid                
                BindData("P");
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}