﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using System.Globalization;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditKickOff_New : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected string FinYear;
        protected string Period;
        protected int branchid;
        protected int atbdid;
        protected static int statusid;
        protected int processid;
        protected int VerticalID;
        protected int AuditID;
        protected string isIE;
        protected int AuditorID;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    branchid = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BID"] = branchid;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VID"] = VerticalID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    FinYear = Request.QueryString["FY"];
                    ViewState["FY"] = FinYear;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    Period = Request.QueryString["Period"];
                    ViewState["Period"] = Period;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AUID"]);
                    ViewState["AUID"] = AuditID;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                {
                    isIE = Convert.ToString(Request.QueryString["ISIE"]);
                    ViewState["ISIE"] = isIE;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                {
                    AuditorID = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                    ViewState["AUDTOR"] = AuditID;
                }
                BindPerformerFilter();
                BindReviewerFilter();
                BindReviewerFilter2();
                BindProcessAudit();
                bindPageNumber();
                if (branchid != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(isIE))
                {
                    if (InternalAuditAssignmnetExists(branchid, VerticalID, FinYear, Period, AuditID))
                    {
                        var UploadOptionDisplayOrNot = UserManagementRisk.Sp_CheckStatusIdSubmittedCountProcedure(Convert.ToInt32(branchid), Convert.ToInt32(VerticalID), FinYear, Period, AuditID);
                        btnSendEmail.Visible = true;
                        if (UploadOptionDisplayOrNot != null)
                        {
                            if (UploadOptionDisplayOrNot.TotalCount >= UploadOptionDisplayOrNot.savecount)
                            {
                                btnSendEmail.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        btnSendEmail.Visible = false;
                    }
                }
                else
                {
                    btnSendEmail.Visible = false;
                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AUID"]);
            }
        }

        protected void ddlPerformerAuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //DropDownList ddlPerformerAuditHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlPerformerAuditHeader");
                DropDownCheckBoxes ddlPerformerAuditHeader = (DropDownCheckBoxes)grdProcess.HeaderRow.FindControl("ddlPerformerAuditHeader");
                if (ddlPerformerAuditHeader.Items.Count > 0)
                {
                    for (int j = 0; j < grdProcess.Rows.Count; j++)
                    {
                        for (int i = 0; i < ddlPerformerAuditHeader.Items.Count; i++)
                        {
                            //DropDownList ddlPerformer = (DropDownList)grdProcess.Rows[j].FindControl("ddlPerformer");
                            DropDownCheckBoxes ddlPerformer = (DropDownCheckBoxes)grdProcess.Rows[j].FindControl("ddlPerformer");
                            if (ddlPerformer.Items.Count > 0)
                            {
                                if (ddlPerformer.Enabled)
                                {
                                    if (ddlPerformerAuditHeader.Items[i].Selected)
                                    {
                                        string val = ddlPerformerAuditHeader.Items[i].Value;
                                        ddlPerformer.Items.FindByValue(val).Selected = true;
                                    }
                                    else
                                    {
                                        string val = ddlPerformerAuditHeader.Items[i].Value;
                                        ddlPerformer.Items.FindByValue(val).Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlReviewerAuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewerAuditHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewerAuditHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer");


                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerAuditHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlReviewer2AuditHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewer2AuditHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewer2AuditHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer2");


                    if (ddlReviewer2.Enabled)
                    {
                        ddlReviewer2.ClearSelection();
                        ddlReviewer2.SelectedValue = ddlReviewer2AuditHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlReviewer2ImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewer2ImplemetationHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewer2ImplemetationHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewerIMP2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP2");


                    if (ddlReviewerIMP2.Enabled)
                    {
                        ddlReviewerIMP2.ClearSelection();
                        ddlReviewerIMP2.SelectedValue = ddlReviewer2ImplemetationHeader.SelectedValue;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlReviewerImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlReviewerImplemetationHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlReviewerImplemetationHeader");
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewerIMP = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP");


                    if (ddlReviewerIMP.Enabled)
                    {
                        ddlReviewerIMP.ClearSelection();
                        ddlReviewerIMP.SelectedValue = ddlReviewerImplemetationHeader.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlPerformerImplemetationHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //DropDownList ddlPerformerImplemetationHeader = (DropDownList)grdProcess.HeaderRow.FindControl("ddlPerformerImplemetationHeader");
                DropDownCheckBoxes ddlPerformerImplemetationHeader = (DropDownCheckBoxes)grdProcess.HeaderRow.FindControl("ddlPerformerImplemetationHeader");
                if (ddlPerformerImplemetationHeader.Items.Count > 0)
                {
                    for (int j = 0; j < grdProcess.Rows.Count; j++)
                    {
                        for (int i = 0; i < ddlPerformerImplemetationHeader.Items.Count; i++)
                        {
                            //DropDownList ddlPerformerIMP = (DropDownList)grdProcess.Rows[j].FindControl("ddlPerformerIMP");
                            DropDownCheckBoxes ddlPerformerIMP = (DropDownCheckBoxes)grdProcess.Rows[j].FindControl("ddlPerformerIMP");
                            if (ddlPerformerIMP.Enabled)
                            {
                                if (ddlPerformerIMP.Items.Count > 0)
                                {
                                    if (ddlPerformerImplemetationHeader.Items[i].Selected)
                                    {
                                        string val = ddlPerformerImplemetationHeader.Items[i].Value;
                                        ddlPerformerIMP.Items.FindByValue(val).Selected = true;
                                    }
                                    else
                                    {
                                        string val = ddlPerformerImplemetationHeader.Items[i].Value;
                                        ddlPerformerIMP.Items.FindByValue(val).Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static bool InternalAuditAssignmnetExists(int CustomerBranchid, int Verticalid, string FinancialYear, string ForMonth, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from ICAA in entities.InternalControlAuditAssignments
                             join IASO in entities.InternalAuditScheduleOns
                             on ICAA.InternalAuditInstance equals IASO.InternalAuditInstance
                             where ICAA.ProcessId == IASO.ProcessId
                             && IASO.FinancialYear == FinancialYear
                             && IASO.ForMonth == ForMonth
                             && ICAA.CustomerBranchID == CustomerBranchid
                             && ICAA.VerticalID == Verticalid
                             && ICAA.AuditID == AuditID
                             select ICAA).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                DropDownListPageNo.DataBind();
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileUploadKickOff.HasFile)
                {
                    string strfileName = Path.GetFileName(fileUploadKickOff.FileName);
                    string filename = Guid.NewGuid() + strfileName;
                    fileUploadKickOff.SaveAs(Server.MapPath("~/UploadedKickOffFile/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/UploadedKickOffFile/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "AuditAssignment");
                            if (flag == true)
                            {
                                string filepath = "~/UploadedKickOffFile/" + filename + "";
                                UploadKickOffExcel(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry1.IsValid = false;
                                cvDuplicateEntry1.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'AuditAssignment'.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                else
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please upload file.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("AuditAssignment"))
                    {
                        if (sheet.Name.Trim().Equals("AuditAssignment") || sheet.Name.Trim().Equals("AuditAssignment") || sheet.Name.Trim().Equals("AuditAssignment"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void UploadKickOffExcel(ExcelPackage xlWorkbook)
        {
            try
            {
                int GridviewRecordCount = 0;
                if (!string.IsNullOrEmpty(Session["TotalRows"].ToString()))
                {
                    GridviewRecordCount = Convert.ToInt32(Session["TotalRows"]);
                }
                else
                {
                    BindProcessAudit();
                    if (!string.IsNullOrEmpty(Session["TotalRows"].ToString()))
                    {
                        GridviewRecordCount = Convert.ToInt32(Session["TotalRows"]);
                    }
                }
                bool success = false;
                int CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                bool SaveFlag = false;
                string listNotSaveList = string.Empty;
                #region Get Branch, Vertical, FY, Period,auditId Values from query string

                int customerBranchId1 = -1;
                int verticalID1 = -1;
                string financialYear1 = string.Empty;
                string period1 = string.Empty;
                string auditId1 = string.Empty;

                List<string> auditPerformerList = new List<string>();
                List<string> auditReviewerOneList = new List<string>();
                List<string> auditReviewerTwoList = new List<string>();

                List<string> implementationPerformerList = new List<string>();
                List<string> implementationReviewerOneList = new List<string>();
                List<string> implementationReviewerTwoList = new List<string>();


                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    customerBranchId1 = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    customerBranchId1 = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    verticalID1 = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    verticalID1 = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    financialYear1 = Convert.ToString(Request.QueryString["FY"]);
                }
                else
                {
                    financialYear1 = Convert.ToString(ViewState["FY"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    period1 = Convert.ToString(Request.QueryString["Period"]);
                }
                else
                {
                    period1 = Convert.ToString(ViewState["Period"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                {
                    auditId1 = Convert.ToString(Request.QueryString["AUID"]);
                }
                else
                {
                    auditId1 = Convert.ToString(ViewState["AUID"]);
                }
                #endregion

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["AuditAssignment"];
                    List<string> errorMessage = new List<string>();
                    if (xlWorksheetvalidation != null)
                    {
                        if (GetDimensionRows(xlWorksheetvalidation) != 0)
                        {
                            int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                            if (GridviewRecordCount != xlrow2 - 2)
                            {
                                cvDuplicateEntry1.IsValid = false;
                                cvDuplicateEntry1.ErrorMessage = "Partial Audit Assignment is not possible.Please fill all rows.";
                                return;
                            }
                            //if (Convert.ToString(xlWorksheetvalidation.Cells[xlrow2 - 4, 1].Text.Trim()) != "" && Convert.ToString(xlWorksheetvalidation.Cells[xlrow2 - 5, 1].Text.Trim()) != "")
                            //{
                            for (int rowNum = 3; rowNum <= xlrow2; rowNum++)
                            {
                                if (String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    errorMessage.Add("Please remove the messages and empty rows.");
                                    break;
                                }
                                if (String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    errorMessage.Add("Please remove the messages and empty rows.");
                                    break;
                                }

                                #region Validation variable
                                long processId = 0;
                                long subProcessId = 0;
                                string valProcessName = string.Empty;
                                string valSubProcessName = string.Empty;
                                string valAuditPerformerEmail = string.Empty;
                                string valAuditReviewer1Email = string.Empty;
                                string valAuditReviewer2Email = string.Empty;
                                string valImplementationPerformerEmail = string.Empty;
                                string valImplementationReviewer1Email = string.Empty;
                                string valImplementationReviewer2Email = string.Empty;
                                List<string> CheckPerformerWithOther = new List<string>();
                                List<string> CheckPerformerWithOtherIMP = new List<string>();
                                #endregion

                                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                                {
                                    CustomerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                                }

                                #region Get Process Name
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    valProcessName = xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valProcessName))
                                {
                                    errorMessage.Add("Required Process Name at row number-" + rowNum);
                                }
                                #endregion

                                #region Check Process Name existance
                                if (!string.IsNullOrEmpty(valProcessName))
                                {
                                    if (!string.IsNullOrEmpty(valProcessName))
                                    {
                                        processId = ProcessManagement.IsProcessExistInSystem(valProcessName, CustomerID);
                                    }
                                    if (processId == 0)
                                    {
                                        errorMessage.Add("Process Name not defined in the system at row number-" + rowNum);
                                    }
                                    if (processId > 0)
                                    {
                                        bool isProcessAssignedToCurrentAudit = ProcessManagement.isProcessAssignedToCurrentAudit(processId, customerBranchId1, financialYear1, period1, verticalID1, Convert.ToInt32(auditId1));
                                        if (isProcessAssignedToCurrentAudit == false)
                                        {
                                            errorMessage.Add("Process Name not defined for Current Audit at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region Get Sub Process Name
                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    valSubProcessName = xlWorksheetvalidation.Cells[rowNum, 2].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valSubProcessName))
                                {
                                    errorMessage.Add("Required SubProcessName at row number-" + rowNum);
                                }
                                #endregion

                                #region Check Sub Process Name existance
                                if (!string.IsNullOrEmpty(valSubProcessName))
                                {
                                    if (!string.IsNullOrEmpty(valSubProcessName))
                                    {
                                        subProcessId = ProcessManagement.IsSubProcessExistInSystem(processId, valSubProcessName);
                                    }
                                    if (subProcessId == 0)
                                    {
                                        errorMessage.Add("Sub Process Name not defined in the system at row number-" + rowNum);
                                    }
                                    if (subProcessId > 0)
                                    {
                                        bool isSubProcessAssignedToCurrentAudit = ProcessManagement.isSubProcessAssignedToCurrentAudit(processId, Convert.ToInt32(auditId1), subProcessId);
                                        if (isSubProcessAssignedToCurrentAudit == false)
                                        {
                                            errorMessage.Add("Sub Process Name not defined for Current Audit at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region Get Audit Performer Email
                                if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 3].Text)))
                                {
                                    valAuditPerformerEmail = xlWorksheetvalidation.Cells[rowNum, 3].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditPerformerEmail))
                                {
                                    errorMessage.Add("Required Audit Performer Email at row number-" + rowNum);
                                }
                                #endregion

                                #region check valid Performer email and Performer email existance
                                if (!string.IsNullOrEmpty(valAuditPerformerEmail))
                                {
                                    string[] performerNameArray = valAuditPerformerEmail.Split(',');
                                    CheckPerformerWithOther = valAuditPerformerEmail.Split(',').ToList();
                                    for (int i = 0; i < performerNameArray.Length; i++)
                                    {
                                        bool checkUserExistanceInComplianceDB = false;
                                        bool checkUserExistanceInAuditDb = false;
                                        string email = performerNameArray[i].ToString().Trim();
                                        Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                        Match match = regex.Match(email);
                                        if (match.Success)
                                        {
                                            //checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            //checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            if (checkUserExistanceInComplianceDB == false || checkUserExistanceInAuditDb == false)
                                            {
                                                errorMessage.Add("Audit Performer email not defined in the system at row number-" + rowNum);
                                            }
                                            auditPerformerList.Add(email);
                                        }
                                        else
                                        {
                                            errorMessage.Add("Invalid Audit Performer email at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region Get Audit Reviewer 1 Email
                                if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 4].Text)))
                                {
                                    valAuditReviewer1Email = xlWorksheetvalidation.Cells[rowNum, 4].Text.Trim();
                                }
                                if (string.IsNullOrEmpty(valAuditReviewer1Email))
                                {
                                    errorMessage.Add("Required Audit Reviewer 1 at row number-" + rowNum);
                                }
                                #endregion

                                #region check valid Reviewer 1 email and Reviewer 1 email existance
                                if (!string.IsNullOrEmpty(valAuditReviewer1Email))
                                {
                                    string[] Reviewer1Array = valAuditReviewer1Email.Split(',');
                                    for (int i = 0; i < Reviewer1Array.Length; i++)
                                    {
                                        bool checkUserExistanceInComplianceDB = false;
                                        bool checkUserExistanceInAuditDb = false;
                                        string email = Reviewer1Array[i].ToString().Trim();
                                        Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                        Match match = regex.Match(email);
                                        if (match.Success)
                                        {
                                            //checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            //checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            if (checkUserExistanceInComplianceDB == false || checkUserExistanceInAuditDb == false)
                                            {
                                                errorMessage.Add("Audit Reviewer 1 email not defined in the system at row number-" + rowNum);
                                            }
                                            auditReviewerOneList.Add(email);
                                        }
                                        else
                                        {
                                            errorMessage.Add("Invalid Audit Reviewer 1 email at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region Get Audit Reviewer 2 Email
                                if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 5].Text)))
                                {
                                    valAuditReviewer2Email = xlWorksheetvalidation.Cells[rowNum, 5].Text.Trim();
                                }
                                #endregion

                                #region check valid Reviewer 2 email and Reviewer 2 email existance
                                if (!string.IsNullOrEmpty(valAuditReviewer2Email))
                                {
                                    string[] Reviewer2Array = valAuditReviewer2Email.Split(',');
                                    for (int i = 0; i < Reviewer2Array.Length; i++)
                                    {
                                        bool checkUserExistanceInComplianceDB = false;
                                        bool checkUserExistanceInAuditDb = false;
                                        string email = Reviewer2Array[i].ToString().Trim();
                                        Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                        Match match = regex.Match(email);
                                        if (match.Success)
                                        {
                                            //checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            //checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            if (checkUserExistanceInComplianceDB == false || checkUserExistanceInAuditDb == false)
                                            {
                                                errorMessage.Add("Audit Reviewer 2 email not defined in the system at row number-" + rowNum);
                                            }
                                            auditReviewerTwoList.Add(email);
                                        }
                                        else
                                        {
                                            errorMessage.Add("Invalid Audit Reviewer 2 email at row number-" + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    auditReviewerTwoList.Add("");
                                }
                                #endregion

                                #region Get Implementation Performer Email
                                if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 7].Text)))
                                {
                                    valImplementationPerformerEmail = xlWorksheetvalidation.Cells[rowNum, 7].Text.Trim();
                                }
                                #endregion

                                #region check valid Performer email and Performer email existance
                                if (!string.IsNullOrEmpty(valImplementationPerformerEmail))
                                {
                                    string[] performerNameArray = valImplementationPerformerEmail.Split(',');
                                    CheckPerformerWithOtherIMP = valImplementationPerformerEmail.Split(',').ToList();
                                    for (int i = 0; i < performerNameArray.Length; i++)
                                    {
                                        bool checkUserExistanceInComplianceDB = false;
                                        bool checkUserExistanceInAuditDb = false;
                                        string email = performerNameArray[i].ToString().Trim();
                                        Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                        Match match = regex.Match(email);
                                        if (match.Success)
                                        {
                                            //checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            //checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);

                                            if (checkUserExistanceInComplianceDB == false || checkUserExistanceInAuditDb == false)
                                            {
                                                errorMessage.Add("Implementation Performer email not defined in the system at row number-" + rowNum);
                                            }
                                            implementationPerformerList.Add(email);
                                        }
                                        else
                                        {
                                            errorMessage.Add("Invalid Implementation Performer email at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region Get Implementation Reviewer 1 Email
                                if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 8].Text)))
                                {
                                    valImplementationReviewer1Email = xlWorksheetvalidation.Cells[rowNum, 8].Text.Trim();
                                }
                                #endregion

                                #region check valid Reviewer 1 email and Reviewer 1 email existance
                                if (!string.IsNullOrEmpty(valImplementationReviewer1Email))
                                {
                                    string[] Reviewer1Array = valImplementationReviewer1Email.Split(',');
                                    for (int i = 0; i < Reviewer1Array.Length; i++)
                                    {
                                        bool checkUserExistanceInComplianceDB = false;
                                        bool checkUserExistanceInAuditDb = false;
                                        string email = Reviewer1Array[i].ToString().Trim();
                                        Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                        Match match = regex.Match(email);
                                        if (match.Success)
                                        {
                                            //checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            //checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            if (checkUserExistanceInComplianceDB == false || checkUserExistanceInAuditDb == false)
                                            {
                                                errorMessage.Add("Implementation Reviewer 1 email not defined in the system at row number-" + rowNum);
                                            }
                                            implementationReviewerOneList.Add(email);
                                        }
                                        else
                                        {
                                            errorMessage.Add("Implementation Invalid Reviewer 1 email at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region Get Implementation Reviewer 2 Email
                                if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 9].Text)))
                                {
                                    valImplementationReviewer2Email = xlWorksheetvalidation.Cells[rowNum, 9].Text.Trim();
                                }
                                #endregion

                                #region check valid Reviewer 2 email and Reviewer 2 email existance
                                if (!string.IsNullOrEmpty(valImplementationReviewer2Email))
                                {
                                    string[] Reviewer2Array = valImplementationReviewer2Email.Split(',');
                                    for (int i = 0; i < Reviewer2Array.Length; i++)
                                    {
                                        bool checkUserExistanceInComplianceDB = false;
                                        bool checkUserExistanceInAuditDb = false;
                                        string email = Reviewer2Array[i].ToString().Trim();
                                        Regex regex = new Regex(@"^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$");
                                        Match match = regex.Match(email);
                                        if (match.Success)
                                        {
                                            //checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            //checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            checkUserExistanceInComplianceDB = UserManagement.UserExistsByEmailInComplianceDB(email, CustomerID);
                                            checkUserExistanceInAuditDb = UserManagement.UserExistsByEmailInAuditDb(email, CustomerID);
                                            if (checkUserExistanceInComplianceDB == false || checkUserExistanceInAuditDb == false)
                                            {
                                                errorMessage.Add("Implementation Reviewer 2 email not defined in the system at row number-" + rowNum);
                                            }
                                            implementationReviewerTwoList.Add(email);
                                        }
                                        else
                                        {
                                            errorMessage.Add("Implementation Invalid Reviewer 2 email at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion

                                #region check performer same as Reviewer 1 and Reviewer 2 for Audit and Implementation
                                //Audit
                                if (CheckPerformerWithOther.Count > 0)
                                {
                                    if (CheckPerformerWithOther.Contains(valAuditReviewer1Email))
                                    {
                                        errorMessage.Add("Performer and Reviewer 1 should not be same at row number-" + rowNum);
                                    }
                                    if (CheckPerformerWithOther.Contains(valAuditReviewer2Email))
                                    {
                                        errorMessage.Add("Performer and Reviewer 2 should not be same at row number-" + rowNum);
                                    }
                                    if (!string.IsNullOrEmpty(valAuditReviewer1Email))
                                    {
                                        if (valAuditReviewer1Email == valAuditReviewer2Email)
                                        {
                                            errorMessage.Add("Reviewer 1 and Reviewer 2 should not be same at row number-" + rowNum);
                                        }
                                    }
                                }
                                //Implementation
                                if (CheckPerformerWithOtherIMP.Count > 0)
                                {
                                    if (CheckPerformerWithOtherIMP.Contains(valImplementationReviewer1Email))
                                    {
                                        errorMessage.Add("Implementation Performer and Reviewer 1 should not be same at row number-" + rowNum);
                                    }
                                    if (CheckPerformerWithOtherIMP.Contains(valImplementationReviewer2Email))
                                    {
                                        errorMessage.Add("Implementation Performer and Reviewer 2 should not be same at row number-" + rowNum);
                                    }
                                    if (!string.IsNullOrEmpty(valImplementationReviewer1Email))
                                    {
                                        if (valImplementationReviewer1Email == valImplementationReviewer2Email)
                                        {
                                            errorMessage.Add("Implementation Reviewer 1 and Reviewer 2 should not be same at row number-" + rowNum);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                                success = false;
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                            }
                            else
                            {
                                #region Get Branch, Vertical, FY, Period,auditId Values from query string
                                int customerBranchId = -1;
                                int verticalID = -1;
                                string financialYear = string.Empty;
                                string period = string.Empty;
                                string auditId = string.Empty;
                                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                                {
                                    customerBranchId = Convert.ToInt32(Request.QueryString["BID"]);
                                }
                                else
                                {
                                    customerBranchId = Convert.ToInt32(ViewState["BID"]);
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                                {
                                    verticalID = Convert.ToInt32(Request.QueryString["VID"]);
                                }
                                else
                                {
                                    verticalID = Convert.ToInt32(ViewState["VID"]);
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                                {
                                    financialYear = Convert.ToString(Request.QueryString["FY"]);
                                }
                                else
                                {
                                    financialYear = Convert.ToString(ViewState["FY"]);
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                                {
                                    period = Convert.ToString(Request.QueryString["Period"]);
                                }
                                else
                                {
                                    period = Convert.ToString(ViewState["Period"]);
                                }

                                if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                                {
                                    auditId = Convert.ToString(Request.QueryString["AUID"]);
                                }
                                else
                                {
                                    auditId = Convert.ToString(ViewState["AUID"]);
                                }
                                #endregion
                                #region Get branch name 
                                string customerBranchName = string.Empty;
                                //int serviceProviderId = Convert.ToInt32(Portal.Common.AuthenticationHelper.ServiceProviderID);
                                //mst_CustomerBranch branch = CustomerBranchManagement.GetCustomerBranchNameForKickOffExcel(customerBranchId, CustomerID);
                                CustomerBranch branch = CustomerBranchManagement.GetByID(customerBranchId);
                                if (branch != null)
                                {
                                    customerBranchName = branch.Name;
                                }
                                #endregion

                                List<RiskActivityToBeDoneMapping> ActiveAuditStepIDList = new List<RiskActivityToBeDoneMapping>();

                                #region code get Values from Risk Activity To be done mapping
                                ActiveAuditStepIDList = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                                         where RATBDM.IsActive == true
                                                         && RATBDM.CustomerBranchID == customerBranchId
                                                         && RATBDM.RCMType != "IFC"
                                                         select RATBDM).Distinct().ToList();
                                #endregion

                                #region Read Uploaded excelsheet to kick off Audit
                                int xlrow3 = xlWorksheetvalidation.Dimension.End.Row;
                                for (int rowNum = 3; rowNum <= xlrow3; rowNum++)
                                {
                                    if (String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                    {
                                        errorMessage.Add("Please remove the messages and empty rows.");
                                        break;
                                    }
                                    if (String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                    {
                                        errorMessage.Add("Please remove the messages and empty rows.");
                                        break;
                                    }
                                    #region variables
                                    int userID = -1;
                                    userID = Portal.Common.AuthenticationHelper.UserID;
                                    int Count = 0;

                                    long processId = 0;
                                    long subProcessId = 0;
                                    string processName = string.Empty;
                                    string subProcessName = string.Empty;

                                    string AuditPerformerEmail = string.Empty;
                                    string AuditReviewer1Email = string.Empty;
                                    string AuditReviewer2Email = string.Empty;

                                    string ImplementationPerformerEmail = string.Empty;
                                    string ImplementationReviewer1Email = string.Empty;
                                    string ImplementationReviewer2Email = string.Empty;

                                    List<int> AuditPerformerList = new List<int>();
                                    List<int> AuditReviewer1List = new List<int>();
                                    List<int> AuditReviewer2List = new List<int>();

                                    List<int> ImplementationPerformerList = new List<int>();
                                    List<int> ImplementationReviewer1List = new List<int>();
                                    List<int> ImplementationReviewer2List = new List<int>();

                                    List<int> Auditstepdetails = new List<int>();
                                    List<InternalControlAuditAssignment> Tempassignments = new List<InternalControlAuditAssignment>();

                                    InternalAuditInstance riskinstance = new InternalAuditInstance();
                                    AuditImplementationInstance riskinstanceIMP = new AuditImplementationInstance();

                                    int distinctprocess = -1;
                                    int distinctprocessImp = -1;
                                    #endregion

                                    #region Get Process Name
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                    {
                                        processName = xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim();
                                    }
                                    #endregion

                                    #region Get Process Id
                                    if (!string.IsNullOrEmpty(processName))
                                    {
                                        //processId = ProcessManagement.IsProcessExistInSystem(processName, CustomerID);
                                        processId = ProcessManagement.IsProcessExistInSystem(processName, CustomerID);
                                    }
                                    #endregion

                                    #region Get Sub Process Name
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                    {
                                        subProcessName = xlWorksheetvalidation.Cells[rowNum, 2].Text.Trim();
                                    }
                                    #endregion

                                    #region Get Sub Process Id
                                    if (!string.IsNullOrEmpty(subProcessName))
                                    {
                                        subProcessId = ProcessManagement.IsSubProcessExistInSystem(processId, subProcessName);
                                    }
                                    #endregion

                                    #region Get Audit Performer Email
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 3].Text.ToString().Trim()))
                                    {
                                        AuditPerformerEmail = xlWorksheetvalidation.Cells[rowNum, 3].Text.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(AuditPerformerEmail))
                                    {
                                        string[] performerEmailArray = AuditPerformerEmail.Split(',');
                                        for (int i = 0; i < performerEmailArray.Length; i++)
                                        {
                                            string email = performerEmailArray[i].ToString().Trim();
                                            //int performerId = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            int performerId = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            AuditPerformerList.Add(performerId);
                                        }
                                    }
                                    #endregion

                                    #region Get Audit Reviewer1  Email
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 4].Text.ToString().Trim()))
                                    {
                                        AuditReviewer1Email = xlWorksheetvalidation.Cells[rowNum, 4].Text.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(AuditReviewer1Email))
                                    {
                                        string[] reviewer1EmailArray = AuditReviewer1Email.Split(',');
                                        for (int i = 0; i < reviewer1EmailArray.Length; i++)
                                        {
                                            string email = reviewer1EmailArray[i].ToString().Trim();
                                            //int reviewer1Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            int reviewer1Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            AuditReviewer1List.Add(reviewer1Id);
                                        }
                                    }
                                    #endregion

                                    #region Get Audit Reviewer2  Email
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 5].Text.ToString().Trim()))
                                    {
                                        AuditReviewer2Email = xlWorksheetvalidation.Cells[rowNum, 5].Text.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(AuditReviewer2Email))
                                    {
                                        string[] reviewer2EmailArray = AuditReviewer2Email.Split(',');
                                        for (int i = 0; i < reviewer2EmailArray.Length; i++)
                                        {
                                            string email = reviewer2EmailArray[i].ToString().Trim();
                                            //int reviewer2Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            int reviewer2Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            AuditReviewer2List.Add(reviewer2Id);
                                        }
                                    }
                                    #endregion

                                    #region Get Implementation Performer Email
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 7].Text.ToString().Trim()))
                                    {
                                        ImplementationPerformerEmail = xlWorksheetvalidation.Cells[rowNum, 7].Text.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(ImplementationPerformerEmail))
                                    {
                                        string[] performerEmailArray = ImplementationPerformerEmail.Split(',');
                                        for (int i = 0; i < performerEmailArray.Length; i++)
                                        {
                                            string email = performerEmailArray[i].ToString().Trim();
                                            //int performerId = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            int performerId = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            ImplementationPerformerList.Add(performerId);
                                        }
                                    }
                                    #endregion

                                    #region Get Implementation Reviewer1  Email
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 8].Text.ToString().Trim()))
                                    {
                                        ImplementationReviewer1Email = xlWorksheetvalidation.Cells[rowNum, 8].Text.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(ImplementationReviewer1Email))
                                    {
                                        string[] reviewer1EmailArray = ImplementationReviewer1Email.Split(',');
                                        for (int i = 0; i < reviewer1EmailArray.Length; i++)
                                        {
                                            string email = reviewer1EmailArray[i].ToString().Trim();
                                            //int reviewer1Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            int reviewer1Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            ImplementationReviewer1List.Add(reviewer1Id);
                                        }
                                    }
                                    #endregion

                                    #region Get Implementation Reviewer2  Email
                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 9].Text.ToString().Trim()))
                                    {
                                        ImplementationReviewer2Email = xlWorksheetvalidation.Cells[rowNum, 9].Text.Trim();
                                    }

                                    if (!string.IsNullOrEmpty(ImplementationReviewer2Email))
                                    {
                                        string[] reviewer2EmailArray = ImplementationReviewer2Email.Split(',');
                                        for (int i = 0; i < reviewer2EmailArray.Length; i++)
                                        {
                                            string email = reviewer2EmailArray[i].ToString().Trim();
                                            //int reviewer2Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            int reviewer2Id = UserManagement.GetUserIdByEmail(email, CustomerID);
                                            ImplementationReviewer2List.Add(reviewer2Id);
                                        }
                                    }
                                    #endregion


                                    if (customerBranchId != -1 && verticalID != -1 && financialYear != "" && period != "" && processId != 0 && auditId != "")
                                    {
                                        Auditstepdetails = UserManagementRisk.GetAuditStepDetailsCount(ActiveAuditStepIDList, Convert.ToInt32(customerBranchId), Convert.ToInt32(verticalID), Convert.ToInt64(auditId), userID);
                                        if (Auditstepdetails.Count > 0)
                                        {
                                            if (AuditPerformerList.Count > 0 && AuditReviewer1List.Count > 0)
                                            {
                                                string AssignedTo = "";
                                                if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                                                {
                                                    AssignedTo = Convert.ToString(Request.QueryString["ISIE"]);
                                                }
                                                else
                                                {
                                                    AssignedTo = Convert.ToString(ViewState["ISIE"]);
                                                }

                                                riskinstance.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                riskinstance.VerticalID = Convert.ToInt32(verticalID);
                                                riskinstance.ProcessId = Convert.ToInt32(processId);
                                                riskinstance.SubProcessId = -1;
                                                riskinstance.IsDeleted = false;
                                                riskinstance.CreatedOn = DateTime.Now;
                                                riskinstance.CreatedBy = userID;
                                                riskinstance.AuditID = Convert.ToInt64(auditId);

                                                if (distinctprocess != Convert.ToInt32(processId))
                                                {
                                                    long ProcessInstanceID = UserManagementRisk.CheckProcessInstanceIDExist(riskinstance);
                                                    if (ProcessInstanceID == 0)
                                                    {
                                                        UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                        CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, financialYear, Convert.ToInt32(customerBranchId), period, Convert.ToInt32(processId), Convert.ToInt32(verticalID), Convert.ToInt64(auditId), -1);
                                                    }
                                                    else
                                                    {
                                                        riskinstance.ID = ProcessInstanceID;
                                                    }
                                                    distinctprocess = Convert.ToInt32(processId);
                                                }

                                                #region Assign Performer & Reviewer to Audit
                                                if (AuditPerformerList.Count > 0)
                                                {
                                                    foreach (var AuditPerformerId in AuditPerformerList)
                                                    {
                                                        InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();
                                                        TempAssP.InternalAuditInstance = riskinstance.ID;
                                                        TempAssP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                        TempAssP.VerticalID = Convert.ToInt32(verticalID);
                                                        TempAssP.ProcessId = Convert.ToInt32(processId);
                                                        TempAssP.SubProcessId = Convert.ToInt32(subProcessId);
                                                        TempAssP.RoleID = 3;
                                                        TempAssP.UserID = Convert.ToInt32(AuditPerformerId);
                                                        TempAssP.IsActive = true;
                                                        TempAssP.ISInternalExternal = AssignedTo;
                                                        TempAssP.CreatedOn = DateTime.Now;
                                                        TempAssP.CreatedBy = userID;
                                                        TempAssP.AuditID = Convert.ToInt64(auditId);
                                                        TempAssP.AuditPeriodStartdate = null;
                                                        TempAssP.AuditPeriodEnddate = null;
                                                        //TempAssP.AuditTypeID = AuditTypeID;
                                                        Tempassignments.Add(TempAssP);
                                                    }
                                                }

                                                if (AuditReviewer1List.Count > 0)
                                                {
                                                    foreach (var AuditReviewer1Id in AuditReviewer1List)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                        TempAssR.VerticalID = Convert.ToInt32(verticalID);
                                                        TempAssR.ProcessId = Convert.ToInt32(processId);
                                                        TempAssR.SubProcessId = Convert.ToInt32(subProcessId);
                                                        TempAssR.RoleID = 4;
                                                        TempAssR.UserID = Convert.ToInt32(AuditReviewer1Id);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(auditId);
                                                        //TempAssR.AuditTypeID = AuditTypeID;
                                                        Tempassignments.Add(TempAssR);
                                                    }
                                                }

                                                if (AuditReviewer2List.Count > 0)
                                                {
                                                    foreach (var AuditReviewer2Id in AuditReviewer2List)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                        TempAssR.VerticalID = Convert.ToInt32(verticalID);
                                                        TempAssR.ProcessId = Convert.ToInt32(processId);
                                                        TempAssR.SubProcessId = Convert.ToInt32(subProcessId);
                                                        TempAssR.RoleID = 5;
                                                        TempAssR.UserID = Convert.ToInt32(AuditReviewer2Id);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(auditId);
                                                        //TempAssR.AuditTypeID = AuditTypeID;
                                                        Tempassignments.Add(TempAssR);
                                                    }
                                                }

                                                if (Tempassignments.Count != 0)
                                                {
                                                    UserManagementRisk.AddDetailsInternalControlAuditAssignment(Tempassignments);
                                                }

                                                if (Count == 0)
                                                {
                                                    //SaveFlag = UserManagementRisk.SaveAuditStepsDetails(Auditstepdetails, CustomerID, customerBranchId, verticalID, financialYear, period, Convert.ToInt64(auditId));
                                                    SaveFlag = UserManagementRisk.SaveAuditStepsDetails(Auditstepdetails, CustomerID, customerBranchId, verticalID, financialYear, period, Convert.ToInt64(auditId));
                                                }
                                                Count++;
                                                #endregion

                                                #region Assign Performer & Reviewer to Implementation
                                                if (ImplementationPerformerList.Count > 0)
                                                {
                                                    #region Implementation
                                                    riskinstanceIMP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                    riskinstanceIMP.IsDeleted = false;
                                                    riskinstanceIMP.CreatedOn = DateTime.Now;
                                                    riskinstanceIMP.ForPeriod = Convert.ToString(period);
                                                    riskinstanceIMP.VerticalID = Convert.ToInt32(verticalID);
                                                    riskinstanceIMP.ProcessId = Convert.ToInt32(processId);
                                                    riskinstanceIMP.SubProcessId = -1;
                                                    riskinstanceIMP.AuditID = Convert.ToInt64(auditId);
                                                    riskinstanceIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                                                    if (distinctprocessImp != Convert.ToInt32(processId))
                                                    {
                                                        long InstanceID = UserManagementRisk.CheckInstanceIDExist(riskinstanceIMP);
                                                        if (InstanceID == 0)
                                                        {
                                                            UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstanceIMP);
                                                            CreateScheduleOnImplementation(riskinstanceIMP.Id,
                                                            Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.User,
                                                            financialYear.Trim(), Convert.ToInt32(customerBranchId), period, Convert.ToInt32(verticalID), Convert.ToInt32(processId), Convert.ToInt64(auditId), -1);
                                                        }
                                                        else
                                                        {
                                                            riskinstanceIMP.Id = InstanceID;
                                                        }
                                                        distinctprocessImp = Convert.ToInt32(processId);
                                                    }


                                                    List<AuditImplementationAssignment> TempassignmentsIMP = new List<AuditImplementationAssignment>();
                                                    if (ImplementationPerformerList.Count > 0)
                                                    {
                                                        foreach (var ImplementationPerformerId in ImplementationPerformerList)
                                                        {
                                                            AuditImplementationAssignment TempAssPIMP = new AuditImplementationAssignment();
                                                            TempAssPIMP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                            TempAssPIMP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                                            TempAssPIMP.UserID = Convert.ToInt32(ImplementationPerformerId);
                                                            TempAssPIMP.IsActive = true;
                                                            TempAssPIMP.CreatedOn = DateTime.Now;
                                                            TempAssPIMP.ForPeriod = Convert.ToString(period);
                                                            TempAssPIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                            TempAssPIMP.FinancialYear = financialYear.Trim();
                                                            TempAssPIMP.VerticalID = Convert.ToInt32(verticalID);
                                                            TempAssPIMP.ProcessId = Convert.ToInt32(processId);
                                                            TempAssPIMP.SubProcessId = Convert.ToInt32(subProcessId);
                                                            TempAssPIMP.AuditID = Convert.ToInt64(auditId);
                                                            TempAssPIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            TempassignmentsIMP.Add(TempAssPIMP);
                                                        }
                                                    }
                                                    if (ImplementationReviewer1List.Count > 0)
                                                    {
                                                        foreach (var ImplementationReviewer1Id in ImplementationReviewer1List)
                                                        {
                                                            AuditImplementationAssignment TempAssRIMP = new AuditImplementationAssignment();
                                                            TempAssRIMP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                            TempAssRIMP.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                                            TempAssRIMP.UserID = Convert.ToInt32(ImplementationReviewer1Id);
                                                            TempAssRIMP.IsActive = true;
                                                            TempAssRIMP.CreatedOn = DateTime.Now;
                                                            TempAssRIMP.ForPeriod = Convert.ToString(period);
                                                            TempAssRIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                            TempAssRIMP.FinancialYear = financialYear.Trim();
                                                            TempAssRIMP.VerticalID = Convert.ToInt32(verticalID);
                                                            TempAssRIMP.ProcessId = Convert.ToInt32(processId);
                                                            TempAssRIMP.SubProcessId = Convert.ToInt32(subProcessId);
                                                            TempAssRIMP.AuditID = Convert.ToInt64(auditId);
                                                            TempAssRIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            TempassignmentsIMP.Add(TempAssRIMP);
                                                        }
                                                    }
                                                    if (ImplementationReviewer2List.Count > 0)
                                                    {
                                                        foreach (var ImplementationReviewer2Id in ImplementationReviewer2List)
                                                        {
                                                            AuditImplementationAssignment TempAssRIMP = new AuditImplementationAssignment();
                                                            TempAssRIMP.CustomerBranchID = Convert.ToInt32(customerBranchId);
                                                            TempAssRIMP.RoleID = RoleManagement.GetByCode("RVW2").ID;
                                                            TempAssRIMP.UserID = Convert.ToInt32(ImplementationReviewer2Id);
                                                            TempAssRIMP.IsActive = true;
                                                            TempAssRIMP.CreatedOn = DateTime.Now;
                                                            TempAssRIMP.ForPeriod = Convert.ToString(period);
                                                            TempAssRIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                            TempAssRIMP.FinancialYear = financialYear.Trim();
                                                            TempAssRIMP.VerticalID = Convert.ToInt32(verticalID);
                                                            TempAssRIMP.ProcessId = Convert.ToInt32(processId);
                                                            TempAssRIMP.SubProcessId = Convert.ToInt32(subProcessId);
                                                            TempAssRIMP.AuditID = Convert.ToInt64(auditId);
                                                            TempAssRIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            TempassignmentsIMP.Add(TempAssRIMP);
                                                        }
                                                    }
                                                    if (TempassignmentsIMP.Count != 0)
                                                    {
                                                        UserManagementRisk.AddDetailsAuditImplementationAssignment(TempassignmentsIMP);
                                                    }
                                                    SaveFlag = true;
                                                    #endregion
                                                }
                                                #endregion

                                                #region send email after kick off
                                                if (InternalAuditAssignmnetExists(customerBranchId, verticalID, financialYear, period, Convert.ToInt32(auditId)))
                                                {
                                                    btnSendEmail.Visible = true;
                                                }
                                                else
                                                {
                                                    btnSendEmail.Visible = false;
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            if (!listNotSaveList.Contains(customerBranchName.Trim() + "/" + period.Trim()))
                                            {
                                                listNotSaveList += customerBranchName.Trim() + "/" + period.Trim();
                                            }
                                        }
                                    }
                                }
                                cvDuplicateEntry1.IsValid = false;
                                if (SaveFlag)
                                {
                                    if (listNotSaveList.Length <= 0)
                                    {
                                        cvDuplicateEntry1.ErrorMessage = "Audit Kickoff Successfully.";
                                    }
                                    else
                                    {
                                        cvDuplicateEntry1.ErrorMessage = "Audit Kickoff Successfully. The following audits " +
                                                                       "- " + listNotSaveList.Trim(',') + " not kickoff due to audit steps not uploaded or Reviewer & Reviewer2 cannot be same user";
                                    }
                                    #region Code added by Sushant for Process Name/SubProcess
                                    var DistinctAuditID = (from row in entities.ProcessCommaSaperates
                                                           select row.AuditID).Distinct().ToList();

                                    var SubProcessAuditList = (from row in entities.SubProcessCommaSeparates
                                                               select row.AuditID).Distinct().ToList();

                                    var CheckAuditID = (from row in entities.AuditClosureDetails
                                                        where !DistinctAuditID.Contains(row.ID)
                                                        select row.ID).Distinct().ToList();

                                    var CheckAuditIDSubProcess = (from row in entities.AuditClosureDetails
                                                                  where !SubProcessAuditList.Contains(row.ID)
                                                                  select row.ID).Distinct().ToList();

                                    foreach (var item in CheckAuditID)
                                    {
                                        var ProcessList = (from row in entities.InternalControlAuditAssignments
                                                           join row1 in entities.Mst_Process
                                                           on row.ProcessId equals row1.Id
                                                           join RATBDM in entities.RiskActivityToBeDoneMappings
                                                           on row.CustomerBranchID equals RATBDM.CustomerBranchID
                                                           where row.AuditID == item
                                                           && row.IsActive == true
                                                           && row.ProcessId == RATBDM.ProcessId
                                                           && RATBDM.RCMType != "IFC"
                                                           select row1.Name).Distinct().ToList();

                                        var ProcessNameComaSaperate = string.Empty;
                                        foreach (var item1 in ProcessList)
                                        {
                                            ProcessNameComaSaperate += item1 + ",";
                                        }
                                        ProcessNameComaSaperate = ProcessNameComaSaperate.TrimEnd(',');
                                        ProcessNameComaSaperate = ProcessNameComaSaperate.TrimStart(',');
                                        if (!string.IsNullOrEmpty(ProcessNameComaSaperate))
                                        {
                                            ProcessCommaSaperate objProcess = new ProcessCommaSaperate();
                                            objProcess.AuditID = item;
                                            objProcess.ProcessName = ProcessNameComaSaperate;
                                            objProcess.CreatedOn = DateTime.Now;
                                            entities.ProcessCommaSaperates.Add(objProcess);
                                            entities.SaveChanges();
                                        }
                                    }

                                    foreach (var item in CheckAuditIDSubProcess)
                                    {
                                        var SubProcessList = (from row in entities.InternalControlAuditAssignments
                                                              join row1 in entities.mst_Subprocess
                                                              on row.SubProcessId equals row1.Id
                                                              join RATBDM in entities.RiskActivityToBeDoneMappings
                                                              on row.CustomerBranchID equals RATBDM.CustomerBranchID
                                                              where row.AuditID == item
                                                              && row.ProcessId == RATBDM.ProcessId
                                                              && row.SubProcessId == RATBDM.SubProcessId
                                                              && row.IsActive == true
                                                              && RATBDM.RCMType != "IFC"
                                                              select row1.Name).Distinct().ToList();

                                        var SubProcessNameComaSeparate = string.Empty;
                                        foreach (var item1 in SubProcessList)
                                        {
                                            SubProcessNameComaSeparate += item1 + ",";
                                        }

                                        SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimEnd(',');
                                        SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimStart(',');
                                        if (!string.IsNullOrEmpty(SubProcessNameComaSeparate))
                                        {
                                            SubProcessCommaSeparate objSubProcess = new SubProcessCommaSeparate();
                                            objSubProcess.AuditID = item;
                                            objSubProcess.SubProcessName = SubProcessNameComaSeparate;
                                            objSubProcess.CreatedOn = DateTime.Now;
                                            entities.SubProcessCommaSeparates.Add(objSubProcess);
                                            entities.SaveChanges();
                                        }
                                    }
                                    #endregion
                                    BindProcessAudit();
                                }
                                else
                                {
                                    cvDuplicateEntry1.ErrorMessage = "No Audit Assignment due to audit steps not uploaded for following " +
                                        "- " + listNotSaveList.Trim(',') + " or Reviewer & Reviewer2 cannot be same user";
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            errorMessage.Add("Sheet cannot be empty");
                            ErrorMessages(errorMessage);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 0, function () { });", true);
                            success = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }

        protected void lbtnDownloadSampleFormat_Click(object sender, EventArgs e)
        {
            try
            {
                string GridviewRecordCount = string.Empty;
                int setDefaultValue = 0;
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AuditAssignment");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdProcessData"]);
                    if (!string.IsNullOrEmpty(Session["TotalRows"].ToString()))
                    {
                        GridviewRecordCount = Session["TotalRows"].ToString();
                        setDefaultValue = Convert.ToInt32(GridviewRecordCount) + 5;
                    }

                    ExcelData = view.ToTable("Selected", false, "ProcessName", "SubProcessName");
                    exWorkSheet.Cells["A2"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["C1:E1"].Merge = true;
                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["C1"].Style.Font.Size = 14;
                    exWorkSheet.Cells["C1"].Value = "Audit";
                    exWorkSheet.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    exWorkSheet.Cells["G1:I1"].Merge = true;
                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["G1"].Style.Font.Size = 14;
                    exWorkSheet.Cells["G1"].Value = "Implementation";
                    exWorkSheet.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["A2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["A2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["A2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["A2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(84, 130, 53));
                    exWorkSheet.Cells["A2"].Value = "Process";
                    exWorkSheet.Cells["A2"].AutoFitColumns(25);

                    exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["B2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["B2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["B2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Green);
                    exWorkSheet.Cells["B2"].Value = "SubProcess";
                    exWorkSheet.Cells["B2"].AutoFitColumns(30);

                    exWorkSheet.Cells["C2"].Value = "Performer";
                    exWorkSheet.Cells["C2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["C2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["C2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["C2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["C2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                    exWorkSheet.Cells["C2"].AutoFitColumns(30);

                    exWorkSheet.Cells["D2"].Value = "Reviewer1";
                    exWorkSheet.Cells["D2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["D2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["D2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["D2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["D2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                    exWorkSheet.Cells["D2"].AutoFitColumns(30);

                    exWorkSheet.Cells["E2"].Value = "Reviewer2";
                    exWorkSheet.Cells["E2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["E2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["E2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["E2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["E2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);
                    exWorkSheet.Cells["E2"].AutoFitColumns(30);

                    exWorkSheet.Cells["F2"].AutoFitColumns(10);

                    exWorkSheet.Cells["G2"].Value = "Performer";
                    exWorkSheet.Cells["G2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["G2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["G2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["G2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["G2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);
                    exWorkSheet.Cells["G2"].AutoFitColumns(30);

                    exWorkSheet.Cells["H2"].Value = "Reviewer1";
                    exWorkSheet.Cells["H2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["H2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["H2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["H2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["H2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);
                    exWorkSheet.Cells["H2"].AutoFitColumns(30);

                    exWorkSheet.Cells["I2"].Value = "Reviewer2";
                    exWorkSheet.Cells["I2"].Style.Font.Name = "Calibri";
                    exWorkSheet.Cells["I2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I2"].Style.Font.Size = 11;
                    exWorkSheet.Cells["I2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    exWorkSheet.Cells["I2"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                    exWorkSheet.Cells["I2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);
                    exWorkSheet.Cells["I2"].AutoFitColumns(30);

                    if (setDefaultValue > 0)
                    {
                        string setColorRed = "A" + Convert.ToString(setDefaultValue);
                        exWorkSheet.Cells[setColorRed].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        exWorkSheet.Cells[setColorRed].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                        exWorkSheet.Cells[setColorRed].AutoFitColumns(25);

                        string setFirstDefaultValue = "B" + Convert.ToString(setDefaultValue);
                        exWorkSheet.Cells[setFirstDefaultValue].Value = "Mandatory Columns";
                        exWorkSheet.Cells[setFirstDefaultValue].Style.Font.Name = "Calibri";
                        exWorkSheet.Cells[setFirstDefaultValue].Style.Font.Size = 11;
                        exWorkSheet.Cells[setFirstDefaultValue].Style.Font.Color.SetColor(System.Drawing.Color.Black);
                        exWorkSheet.Cells[setFirstDefaultValue].AutoFitColumns(30);

                        setDefaultValue = setDefaultValue + 1;
                        string setColorBlue = "A" + Convert.ToString(setDefaultValue);
                        exWorkSheet.Cells[setColorBlue].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        exWorkSheet.Cells[setColorBlue].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Blue);
                        exWorkSheet.Cells[setColorBlue].AutoFitColumns(25);

                        string setSecondDefaultValue = "B" + Convert.ToString(setDefaultValue);
                        exWorkSheet.Cells[setSecondDefaultValue].Value = "Non-Mandatory Columns";
                        exWorkSheet.Cells[setSecondDefaultValue].Style.Font.Name = "Calibri";
                        exWorkSheet.Cells[setSecondDefaultValue].Style.Font.Size = 11;
                        exWorkSheet.Cells[setSecondDefaultValue].Style.Font.Color.SetColor(System.Drawing.Color.Black);
                        exWorkSheet.Cells[setSecondDefaultValue].AutoFitColumns(30);

                        setDefaultValue = setDefaultValue + 1;
                        string setColorGreen = "A" + Convert.ToString(setDefaultValue);
                        exWorkSheet.Cells[setColorGreen].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        exWorkSheet.Cells[setColorGreen].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(84, 130, 53));
                        exWorkSheet.Cells[setColorGreen].AutoFitColumns(25);

                        string setThirdDefaultValue = "B" + Convert.ToString(setDefaultValue);
                        exWorkSheet.Cells[setThirdDefaultValue].Value = "Auto-filled by System, do not change";
                        exWorkSheet.Cells[setThirdDefaultValue].Style.Font.Name = "Calibri";
                        exWorkSheet.Cells[setThirdDefaultValue].Style.Font.Size = 11;
                        exWorkSheet.Cells[setThirdDefaultValue].Style.Font.Color.SetColor(System.Drawing.Color.Black);
                        exWorkSheet.Cells[setThirdDefaultValue].AutoFitColumns(30);

                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Audit_Assignment_Excel.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdProcess.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            if (chkSelectedPage != 0)
            {
                grdProcess.PageIndex = chkSelectedPage - 1;
            }
            BindProcessAudit();
            bindPageNumber();
        }
        public void BindPerformerFilter()
        {
            int auditorid = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
            {
                isIE = Convert.ToString(Request.QueryString["ISIE"]);
            }
            else
            {
                isIE = Convert.ToString(ViewState["ISIE"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
            {
                auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
            }
            else
            {
                auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
            }
            if (isIE == "I")
            {
                ddlPerformerFilter.Items.Clear();
                ddlPerformerFilter.DataTextField = "Name";
                ddlPerformerFilter.DataValueField = "ID";
                ddlPerformerFilter.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                ddlPerformerFilter.DataBind();
            }
            else
            {
                if (auditorid != -1)
                {
                    ddlPerformerFilter.Items.Clear();
                    ddlPerformerFilter.DataTextField = "Name";
                    ddlPerformerFilter.DataValueField = "ID";
                    ddlPerformerFilter.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(auditorid));
                    ddlPerformerFilter.DataBind();
                }
            }
        }
        public void BindReviewerFilter2()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            ddlReviewerFilter2.Items.Clear();
            ddlReviewerFilter2.DataTextField = "Name";
            ddlReviewerFilter2.DataValueField = "ID";
            ddlReviewerFilter2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
            ddlReviewerFilter2.DataBind();
        }
        public void BindReviewerFilter()
        {
            int auditorid = -1;
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
            {
                isIE = Convert.ToString(Request.QueryString["ISIE"]);
            }
            else
            {
                isIE = Convert.ToString(ViewState["ISIE"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
            {
                auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
            }
            else
            {
                auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
            }
            if (isIE == "I")
            {
                ddlReviewerFilter.Items.Clear();
                ddlReviewerFilter.DataTextField = "Name";
                ddlReviewerFilter.DataValueField = "ID";
                ddlReviewerFilter.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                ddlReviewerFilter.DataBind();
            }
            else
            {
                if (auditorid != -1)
                {
                    ddlReviewerFilter.Items.Clear();
                    ddlReviewerFilter.DataTextField = "Name";
                    ddlReviewerFilter.DataValueField = "ID";
                    ddlReviewerFilter.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(auditorid));
                    ddlReviewerFilter.DataBind();
                }
            }
        }
        protected void ddlPerformerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlPerformer = (DropDownList)grdProcess.Rows[i].FindControl("ddlPerformer");
                    DropDownList ddlPerformerIMP = (DropDownList)grdProcess.Rows[i].FindControl("ddlPerformerIMP");
                    if (ddlPerformer.Enabled)
                    {
                        ddlPerformer.ClearSelection();
                        ddlPerformer.SelectedValue = ddlPerformerFilter.SelectedValue;
                    }
                    if (ddlPerformerIMP.Enabled)
                    {
                        ddlPerformerIMP.ClearSelection();
                        ddlPerformerIMP.SelectedValue = ddlPerformerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlReviewerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer");
                    DropDownList ddlReviewerIMP = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP");

                    if (ddlReviewer.Enabled)
                    {
                        ddlReviewer.ClearSelection();
                        ddlReviewer.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                    if (ddlReviewerIMP.Enabled)
                    {
                        ddlReviewerIMP.ClearSelection();
                        ddlReviewerIMP.SelectedValue = ddlReviewerFilter.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlReviewerFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdProcess.Rows.Count; i++)
                {
                    DropDownList ddlReviewer2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewer2");
                    DropDownList ddlReviewerIMP2 = (DropDownList)grdProcess.Rows[i].FindControl("ddlReviewerIMP2");

                    if (ddlReviewer2.Enabled)
                    {
                        ddlReviewer2.ClearSelection();
                        ddlReviewer2.SelectedValue = ddlReviewerFilter2.SelectedValue;
                    }
                    if (ddlReviewerIMP2.Enabled)
                    {
                        ddlReviewerIMP2.ClearSelection();
                        ddlReviewerIMP2.SelectedValue = ddlReviewerFilter2.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdProcess.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (chkSelectedPage != 0)
                {
                    grdProcess.PageIndex = chkSelectedPage - 1;
                }
                BindProcessAudit();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdProcess.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {

            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }


        public void BindProcessAudit()
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int auditorid = -1;
                    int CustomerBranchId = -1;
                    string flag = string.Empty;
                    string FinancialYear = string.Empty;
                    string Period = string.Empty;
                    int VerticalID = -1;
                    int customerID = -1;
                    int Auid = -1;
                    customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                    {
                        CustomerBranchId = Convert.ToInt32(Request.QueryString["BID"]);
                    }
                    else
                    {
                        CustomerBranchId = Convert.ToInt32(ViewState["BID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                    {
                        VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    }
                    else
                    {
                        VerticalID = Convert.ToInt32(ViewState["VID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                    {
                        FinancialYear = Convert.ToString(Request.QueryString["FY"]);
                    }
                    else
                    {
                        FinancialYear = Convert.ToString(ViewState["FY"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                    {
                        Period = Convert.ToString(Request.QueryString["Period"]);
                    }
                    else
                    {
                        Period = Convert.ToString(ViewState["Period"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                    {
                        Auid = Convert.ToInt32(Request.QueryString["AUID"]);
                    }
                    else
                    {
                        Auid = Convert.ToInt32(ViewState["AUID"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                    {
                        flag = Convert.ToString(Request.QueryString["ISIE"]);
                    }
                    else
                    {
                        flag = Convert.ToString(ViewState["ISIE"]);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                    {
                        auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                    }
                    else
                    {
                        auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
                    }
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                    var Branchlistloop = Branchlist.ToList();

                    if (flag == "I")
                    {
                        var RiskActivityToBeDoneMappingGetByName = (from row in entities.AuditKickOffFillViews
                                                                    join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                                                    on row.ProcessId equals row1.ProcessId
                                                                    where row.CustomerBranchId == row1.BranchID
                                                                    && row.CustomerID == customerID
                                                                    && row.AssignedTo == "I"
                                                                    && row.AuditID == Auid
                                                                    && row1.UserID == Portal.Common.AuthenticationHelper.UserID
                                                                    && row1.ISACTIVE == true
                                                                    select row).Distinct().ToList();
                        if (Branchlist.Count > 0)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();

                        if (VerticalID != -1)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.Verticalid == VerticalID).ToList();

                        if (!string.IsNullOrEmpty(FinancialYear))
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                        if (!string.IsNullOrEmpty(Period))
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.TermName == Period).ToList();

                        if (RiskActivityToBeDoneMappingGetByName.Count > 0)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.OrderBy(entry => entry.ProcessName).ToList();

                        grdProcess.DataSource = null;
                        grdProcess.DataBind();

                        grdProcess.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                        Session["TotalRows"] = null;
                        Session["grdProcessData"] = (grdProcess.DataSource as List<AuditKickOffFillView>).ToDataTable();
                        Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                        Session["grdProcessDataFilter"] = (grdProcess.DataSource as List<AuditKickOffFillView>).ToList();
                        grdProcess.DataBind();
                    }
                    else if (flag == "E")
                    {
                        var RiskActivityToBeDoneMappingGetByName = (from row in entities.AuditKickOffFillViews
                                                                    join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                                                     on row.ProcessId equals row1.ProcessId
                                                                    where row.CustomerBranchId == row1.BranchID
                                                                    && row.CustomerID == customerID
                                                                    && row.AssignedTo == "E"
                                                                    && row.AuditID == Auid
                                                                     && row1.UserID == Portal.Common.AuthenticationHelper.UserID
                                                                    && row1.ISACTIVE == true
                                                                    select row).Distinct().ToList();

                        if (Branchlist.Count > 0)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();

                        if (VerticalID != -1)
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.Verticalid == VerticalID).ToList();

                        if (!string.IsNullOrEmpty(FinancialYear))
                            RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                        if (ViewState["Period"] != null)
                        {
                            Period = Convert.ToString(ViewState["Period"]);
                            if (!string.IsNullOrEmpty(Period))
                                RiskActivityToBeDoneMappingGetByName = RiskActivityToBeDoneMappingGetByName.Where(entry => entry.TermName == Period).ToList();
                        }

                        grdProcess.DataSource = null;
                        grdProcess.DataBind();

                        grdProcess.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                        Session["TotalRows"] = null;
                        Session["grdProcessData"] = (grdProcess.DataSource as List<AuditKickOffFillView>).ToDataTable();
                        Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                        Session["grdProcessDataFilter"] = (grdProcess.DataSource as List<AuditKickOffFillView>).ToList();
                        grdProcess.DataBind();
                    }
                    //CommanBranchlist.Clear();
                    //branchids.Clear();
                    Branchlistloop.Clear();
                    Branchlist.Clear();

                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int auditorid = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                {
                    isIE = Convert.ToString(Request.QueryString["ISIE"]);
                }
                else
                {
                    isIE = Convert.ToString(ViewState["ISIE"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                {
                    auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                }
                else
                {
                    auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
                }
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    var obj = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                    DropDownCheckBoxes ddlPerformerAuditHeader = (e.Row.FindControl("ddlPerformerAuditHeader") as DropDownCheckBoxes);

                    ddlPerformerAuditHeader.DataTextField = "Name";
                    ddlPerformerAuditHeader.DataValueField = "ID";
                    ddlPerformerAuditHeader.DataSource = obj;
                    ddlPerformerAuditHeader.DataBind();

                    Session["Control"] = ddlPerformerAuditHeader;

                    DropDownList ddlReviewerAuditHeader = (e.Row.FindControl("ddlReviewerAuditHeader") as DropDownList);

                    ddlReviewerAuditHeader.DataTextField = "Name";
                    ddlReviewerAuditHeader.DataValueField = "ID";
                    ddlReviewerAuditHeader.DataSource = obj;
                    ddlReviewerAuditHeader.DataBind();
                    ddlReviewerAuditHeader.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                    DropDownList ddlReviewer2AuditHeader = (e.Row.FindControl("ddlReviewer2AuditHeader") as DropDownList);

                    ddlReviewer2AuditHeader.DataTextField = "Name";
                    ddlReviewer2AuditHeader.DataValueField = "ID";
                    ddlReviewer2AuditHeader.DataSource = obj;
                    ddlReviewer2AuditHeader.DataBind();
                    ddlReviewer2AuditHeader.Items.Insert(0, new ListItem("Select Reviewer2", "-1"));

                    DropDownCheckBoxes ddlPerformerImplemetationHeader = (e.Row.FindControl("ddlPerformerImplemetationHeader") as DropDownCheckBoxes);

                    ddlPerformerImplemetationHeader.DataTextField = "Name";
                    ddlPerformerImplemetationHeader.DataValueField = "ID";
                    ddlPerformerImplemetationHeader.DataSource = obj;
                    ddlPerformerImplemetationHeader.DataBind();

                    //Session["ControlIMP"] = ddlPerformerImplemetationHeader;

                    DropDownList ddlReviewerImplemetationHeader = (e.Row.FindControl("ddlReviewerImplemetationHeader") as DropDownList);


                    ddlReviewerImplemetationHeader.DataTextField = "Name";
                    ddlReviewerImplemetationHeader.DataValueField = "ID";
                    ddlReviewerImplemetationHeader.DataSource = obj;
                    ddlReviewerImplemetationHeader.DataBind();
                    ddlReviewerImplemetationHeader.Items.Insert(0, new ListItem("Select Reviewer", "-1"));

                    DropDownList ddlReviewer2ImplemetationHeader = (e.Row.FindControl("ddlReviewer2ImplemetationHeader") as DropDownList);

                    ddlReviewer2ImplemetationHeader.DataTextField = "Name";
                    ddlReviewer2ImplemetationHeader.DataValueField = "ID";
                    ddlReviewer2ImplemetationHeader.DataSource = obj;
                    ddlReviewer2ImplemetationHeader.DataBind();
                    ddlReviewer2ImplemetationHeader.Items.Insert(0, new ListItem("Select Reviewer2", "-1"));
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblLocationId = (e.Row.FindControl("lblLocationId") as Label);
                    Label lblProcessId = (e.Row.FindControl("lblProcessId") as Label);
                    Label lblSubProcessId = (e.Row.FindControl("lblSubProcessId") as Label);
                    Label lblFinancialYear = (e.Row.FindControl("lblFinancialYear") as Label);
                    Label lblTermName = (e.Row.FindControl("lblTermName") as Label);
                    Label lblVerticalId = (e.Row.FindControl("lblVerticalId") as Label);
                    Label lblAuditID = (e.Row.FindControl("lblAuditID") as Label);
                    if (isIE == "I")
                    {
                        #region Audit
                        DropDownCheckBoxes ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownCheckBoxes);
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        DropDownList ddlReviewer2 = (e.Row.FindControl("ddlReviewer2") as DropDownList);

                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformer.DataBind();

                            if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                MultiplePerformerReviewerEnable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                //DropDownCheckBoxes ddlPerformerAuditHeader = (e.Row.FindControl("ddlPerformerAuditHeader") as DropDownCheckBoxes);
                                MultiplePerformerReviewerEnableHeader((DropDownCheckBoxes)Session["Control"], customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text));

                            }
                        }

                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                            if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                PerformerReviewerEnable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }

                        if (ddlReviewer2 != null)
                        {
                            ddlReviewer2.Items.Clear();
                            ddlReviewer2.DataTextField = "Name";
                            ddlReviewer2.DataValueField = "ID";
                            ddlReviewer2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewer2.DataBind();
                            ddlReviewer2.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                            if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                PerformerReviewerEnable(ddlReviewer2, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }
                        #endregion

                        #region Implementation
                        DropDownCheckBoxes ddlPerformerIMP = (e.Row.FindControl("ddlPerformerIMP") as DropDownCheckBoxes);
                        DropDownList ddlReviewerIMP = (e.Row.FindControl("ddlReviewerIMP") as DropDownList);
                        DropDownList ddlReviewerIMP2 = (e.Row.FindControl("ddlReviewerIMP2") as DropDownList);

                        if (ddlPerformerIMP != null)
                        {
                            ddlPerformerIMP.Items.Clear();
                            ddlPerformerIMP.DataTextField = "Name";
                            ddlPerformerIMP.DataValueField = "ID";
                            ddlPerformerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlPerformerIMP.DataBind();

                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                ImpMultiplePerformerReviewerEnable(ddlPerformerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                // MultiplePerformerReviewerEnableHeader((DropDownCheckBoxes)Session["ControlIMP"], customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text));
                            }
                        }

                        if (ddlReviewerIMP != null)
                        {
                            ddlReviewerIMP.Items.Clear();
                            ddlReviewerIMP.DataTextField = "Name";
                            ddlReviewerIMP.DataValueField = "ID";
                            ddlReviewerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                            ddlReviewerIMP.DataBind();
                            ddlReviewerIMP.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                ImpPerformerReviewerEnable(ddlReviewerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }

                        if (ddlReviewerIMP2 != null)
                        {
                            ddlReviewerIMP2.Items.Clear();
                            ddlReviewerIMP2.DataTextField = "Name";
                            ddlReviewerIMP2.DataValueField = "ID";
                            ddlReviewerIMP2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                            ddlReviewerIMP2.DataBind();
                            ddlReviewerIMP2.Items.Insert(0, new ListItem("Select Reviewer2", "-1"));
                            if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                            {
                                ImpPerformerReviewerEnable(ddlReviewerIMP2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (auditorid != -1)
                        {
                            #region Audit
                            DropDownCheckBoxes ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownCheckBoxes);
                            DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                            DropDownList ddlReviewer2 = (e.Row.FindControl("ddlReviewer2") as DropDownList);

                            if (ddlPerformer != null)
                            {
                                ddlPerformer.Items.Clear();
                                ddlPerformer.DataTextField = "Name";
                                ddlPerformer.DataValueField = "ID";
                                ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformer.DataBind();

                                if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    PerformerReviewerEnable(ddlPerformer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewer != null)
                            {
                                ddlReviewer.Items.Clear();
                                ddlReviewer.DataTextField = "Name";
                                ddlReviewer.DataValueField = "ID";
                                ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewer.DataBind();
                                ddlReviewer.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                                if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    PerformerReviewerEnable(ddlReviewer, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewer2 != null)
                            {
                                ddlReviewer2.Items.Clear();
                                ddlReviewer2.DataTextField = "Name";
                                ddlReviewer2.DataValueField = "ID";
                                ddlReviewer2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                                ddlReviewer2.DataBind();
                                ddlReviewer2.Items.Insert(0, new ListItem("Select Reviewer2", "-1"));
                                if (lblLocationId != null && lblProcessId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    PerformerReviewerEnable(ddlReviewer2, customerID, Convert.ToInt32(lblLocationId.Text), Convert.ToInt32(lblProcessId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }
                            #endregion

                            #region Implementation
                            DropDownCheckBoxes ddlPerformerIMP = (e.Row.FindControl("ddlPerformerIMP") as DropDownCheckBoxes);
                            DropDownList ddlReviewerIMP = (e.Row.FindControl("ddlReviewerIMP") as DropDownList);
                            DropDownList ddlReviewerIMP2 = (e.Row.FindControl("ddlReviewerIMP2") as DropDownList);

                            if (ddlPerformerIMP != null)
                            {
                                ddlPerformerIMP.Items.Clear();
                                ddlPerformerIMP.DataTextField = "Name";
                                ddlPerformerIMP.DataValueField = "ID";
                                ddlPerformerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlPerformerIMP.DataBind();

                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    ImpPerformerReviewerEnable(ddlPerformerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 3, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewerIMP != null)
                            {
                                ddlReviewerIMP.Items.Clear();
                                ddlReviewerIMP.DataTextField = "Name";
                                ddlReviewerIMP.DataValueField = "ID";
                                ddlReviewerIMP.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(customerID);
                                ddlReviewerIMP.DataBind();
                                ddlReviewerIMP.Items.Insert(0, new ListItem("Select Reviewer", "-1"));
                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    ImpPerformerReviewerEnable(ddlReviewerIMP, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 4, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }

                            if (ddlReviewerIMP2 != null)
                            {
                                ddlReviewerIMP2.Items.Clear();
                                ddlReviewerIMP2.DataTextField = "Name";
                                ddlReviewerIMP2.DataValueField = "ID";
                                ddlReviewerIMP2.DataSource = RiskCategoryManagement.Reviewer2Users(customerID);
                                ddlReviewerIMP2.DataBind();
                                ddlReviewerIMP2.Items.Insert(0, new ListItem("Select Reviewer2", "-1"));
                                if (lblLocationId != null && lblFinancialYear != null && lblTermName != null)
                                {
                                    ImpPerformerReviewerEnable(ddlReviewerIMP2, customerID, Convert.ToInt32(lblLocationId.Text), lblFinancialYear.Text, lblTermName.Text, 5, Convert.ToInt32(lblVerticalId.Text), Convert.ToInt32(lblProcessId.Text), Convert.ToInt32(lblAuditID.Text), Convert.ToInt32(lblSubProcessId.Text));
                                }
                            }
                            #endregion
                        }
                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        //public void PerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, long ProcessID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID, int SubProcessID)
        //{
        //    List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
        //                   select row).ToList();

        //        if (Records.Count > 0)
        //        {
        //            var Data = (from row in Records
        //                        where row.ProcessId == ProcessID
        //                        && row.CustomerBranchID == CustBranchID
        //                        && row.SubProcessId == SubProcessID
        //                        && row.FinancialYear == FinYear
        //                        && row.ForMonth == Period
        //                        && row.RoleID == RoleID && row.VerticalID == VerticalId
        //                        select row).FirstOrDefault();

        //            if (Data != null)
        //            {
        //                ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
        //                ddl.Enabled = false;
        //                ddl.Attributes.Add("class", "");
        //                ddl.CssClass = "form-control m-bot15 Check";
        //            }
        //            else
        //                ddl.Enabled = true;
        //        }
        //    }
        //}
        public void PerformerReviewerEnable(DropDownCheckBoxes ddl, int CustID, int CustBranchID, long ProcessID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID, int SubProcessID)
        {
            List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();

                if (Records.Count > 0)
                {
                    var Data = (from row in Records
                                where row.ProcessId == ProcessID
                                && row.SubProcessId == SubProcessID
                                && row.CustomerBranchID == CustBranchID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                && row.RoleID == RoleID && row.VerticalID == VerticalId
                                select row).FirstOrDefault();

                    if (Data != null)
                    {
                        ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
                        ddl.Enabled = false;
                        ddl.Attributes.Add("class", "");
                        ddl.CssClass = "form-control m-bot15";
                    }
                    else
                        ddl.Enabled = true;
                }
            }
        }

        public void PerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, long ProcessID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID, int SubProcessID)
        {
            List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();

                if (Records.Count > 0)
                {
                    var Data = (from row in Records
                                where row.ProcessId == ProcessID
                                && row.SubProcessId == SubProcessID
                                && row.CustomerBranchID == CustBranchID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                && row.RoleID == RoleID && row.VerticalID == VerticalId
                                select row).FirstOrDefault();

                    if (Data != null)
                    {
                        ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
                        ddl.Enabled = false;
                        ddl.Attributes.Add("class", "");
                        ddl.CssClass = "form-control m-bot15";
                    }
                    else
                        ddl.Enabled = true;
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            String URLStr = "~/RiskManagement/InternalAuditTool/AuditListForKickOff.aspx";
            Response.Redirect(URLStr, false);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region
                List<string> errorMessage = new List<string>();
                int rowNumber = 1;
                foreach (GridViewRow g1 in grdProcess.Rows)
                {
                    List<string> auditPerformerList = new List<string>();
                    List<string> auditReviewerOneList = new List<string>();
                    List<string> auditReviewerTwoList = new List<string>();

                    List<string> implementationPerformerList = new List<string>();
                    List<string> implementationReviewerOneList = new List<string>();
                    List<string> implementationReviewerTwoList = new List<string>();

                    DropDownCheckBoxes ddlper = new DropDownCheckBoxes();
                    DropDownList ddlrev = new DropDownList();
                    DropDownList ddlrev2 = new DropDownList();
                    DropDownCheckBoxes ddlPerImp = new DropDownCheckBoxes();
                    DropDownList ddlRevImp = new DropDownList();
                    DropDownList ddlRev2Imp = new DropDownList();

                    ddlper = (g1.FindControl("ddlPerformer") as DropDownCheckBoxes);
                    ddlrev = (g1.FindControl("ddlReviewer") as DropDownList);
                    ddlrev2 = (g1.FindControl("ddlReviewer2") as DropDownList);
                    ddlPerImp = (g1.FindControl("ddlPerformerIMP") as DropDownCheckBoxes);
                    ddlRevImp = (g1.FindControl("ddlReviewerIMP") as DropDownList);
                    ddlRev2Imp = (g1.FindControl("ddlReviewerIMP2") as DropDownList);

                    for (int i = 0; i < ddlper.Items.Count; i++)
                    {
                        if (ddlper.Items[i].Selected)
                        {
                            if (ddlper.Items[i].Value != "-1")
                            {
                                auditPerformerList.Add(ddlper.Items[i].Value);
                            }
                        }
                    }
                    if (auditPerformerList.Count == 0)
                    {
                        errorMessage.Add("Partial Audit Assignment is not possible. Please select Performer at row number - " + rowNumber);
                    }
                    for (int i = 0; i < ddlrev.Items.Count; i++)
                    {
                        if (ddlrev.Items[i].Selected)
                        {
                            if (ddlrev.Items[i].Value != "-1")
                            {
                                auditReviewerOneList.Add(ddlrev.Items[i].Value);
                            }
                        }
                    }
                    if (auditReviewerOneList.Count == 0)
                    {
                        errorMessage.Add("Please select Reviewer at row number - " + rowNumber);
                    }
                    for (int i = 0; i < ddlrev2.Items.Count; i++)
                    {
                        if (ddlrev2.Items[i].Selected)
                        {
                            if (ddlrev2.Items[i].Value != "-1")
                            {
                                auditReviewerTwoList.Add(ddlrev2.Items[i].Value);
                            }
                        }
                    }
                    for (int i = 0; i < ddlPerImp.Items.Count; i++)
                    {
                        if (ddlPerImp.Items[i].Selected)
                        {
                            if (ddlPerImp.Items[i].Value != "-1")
                            {
                                implementationPerformerList.Add(ddlPerImp.Items[i].Value);
                            }
                        }
                    }
                    for (int i = 0; i < ddlRevImp.Items.Count; i++)
                    {
                        if (ddlRevImp.Items[i].Selected)
                        {
                            if (ddlRevImp.Items[i].Value != "-1")
                            {
                                implementationReviewerOneList.Add(ddlRevImp.Items[i].Value);
                            }
                        }
                    }
                    for (int i = 0; i < ddlRev2Imp.Items.Count; i++)
                    {
                        if (ddlRev2Imp.Items[i].Selected)
                        {
                            if (ddlRev2Imp.Items[i].Value != "-1")
                            {
                                implementationReviewerTwoList.Add(ddlRev2Imp.Items[i].Value);
                            }
                        }
                    }

                    if (auditPerformerList.Count > 0)
                    {
                        if (auditReviewerOneList.Count > 0)
                        {
                            List<string> matching = auditPerformerList.Intersect(auditReviewerOneList).ToList();
                            if (matching.Count > 0)
                            {
                                errorMessage.Add("Performer and Reviewer 1 should not be same for Audit - " + rowNumber);
                            }
                        }
                        if (auditReviewerTwoList.Count > 0)
                        {
                            List<string> matching = auditPerformerList.Intersect(auditReviewerTwoList).ToList();
                            if (matching.Count > 0)
                            {
                                errorMessage.Add("Performer and Reviewer 2 should not be same for Audit - " + rowNumber);
                            }
                        }
                    }
                    if (implementationPerformerList.Count > 0)
                    {
                        if (implementationReviewerOneList.Count > 0)
                        {
                            List<string> matching = implementationPerformerList.Intersect(implementationReviewerOneList).ToList();
                            if (matching.Count > 0)
                            {
                                errorMessage.Add("Performer and Reviewer 1 should not be same for Implementation - " + rowNumber);
                            }
                        }
                        if (implementationReviewerTwoList.Count > 0)
                        {
                            List<string> matching = implementationPerformerList.Intersect(implementationReviewerTwoList).ToList();
                            if (matching.Count > 0)
                            {
                                errorMessage.Add("Performer and Reviewer 2 should not be same for Implementation - " + rowNumber);
                            }
                        }
                    }
                    rowNumber++;
                }
                if (errorMessage.Count > 0)
                {
                    ErrorMessages(errorMessage);
                    return;
                }
                #endregion

                bool SaveFlag = false;
                string listNotSaveList = string.Empty;
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int userID = -1;
                userID = Portal.Common.AuthenticationHelper.UserID;

                #region Get Branch, Vertical, FY, Period Values

                int customerBranchId = -1;
                int verticalID = -1;
                string financialYear = string.Empty;
                string period = string.Empty;
                bool CheckSameUser = false;
                int distinctprocess = -1;
                int distinctprocessImp = -1;

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    customerBranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    customerBranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    verticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    verticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    financialYear = Convert.ToString(Request.QueryString["FY"]);
                }
                else
                {
                    financialYear = Convert.ToString(ViewState["FY"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    period = Convert.ToString(Request.QueryString["Period"]);
                }
                else
                {
                    period = Convert.ToString(ViewState["Period"]);
                }
                #endregion
                

                int TotalRowCount = Convert.ToInt32(Session["TotalRows"]);
                int ActualRows = 0;
                foreach (GridViewRow g1 in grdProcess.Rows)
                {
                    int selectedCount = 0;
                    DropDownCheckBoxes ddlperval = new DropDownCheckBoxes();
                    ddlperval = (g1.FindControl("ddlPerformer") as DropDownCheckBoxes);
                    
                    for (int i = 0; i < ddlperval.Items.Count; i++)
                    {
                        if (ddlperval.Items[i].Selected)
                        {
                            if (selectedCount > 0)
                            {
                                break;
                            }
                            else
                            {
                                if (ddlperval.Items[i].Value != "-1")
                                {
                                    ActualRows++;
                                    selectedCount++;
                                }
                            }

                        }
                    }
                }
                if (TotalRowCount == ActualRows)
                {
                    CheckSameUser = true;
                }

                if (CheckSameUser)
                {
                    if (customerBranchId != -1 && verticalID != -1 && financialYear != "" && period != "")
                    {
                        List<InternalControlAuditAssignment> Tempassignments = new List<InternalControlAuditAssignment>();
                        List<RiskActivityToBeDoneMapping> ActiveAuditStepIDList = new List<RiskActivityToBeDoneMapping>();
                        InternalAuditInstance riskinstance = new InternalAuditInstance();
                        AuditImplementationInstance riskinstanceIMP = new AuditImplementationInstance();
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            int Count = 0;
                            ActiveAuditStepIDList = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                                     where RATBDM.IsActive == true
                                                     && RATBDM.RCMType != "IFC"
                                                     select RATBDM).Distinct().ToList();
                            foreach (GridViewRow g1 in grdProcess.Rows)
                            {
                                Tempassignments.Clear();
                                DropDownCheckBoxes ddlper = new DropDownCheckBoxes();
                                DropDownList ddlrev = new DropDownList();
                                DropDownList ddlrev2 = new DropDownList();
                                DropDownCheckBoxes ddlPerImp = new DropDownCheckBoxes();
                                DropDownList ddlRevImp = new DropDownList();
                                DropDownList ddlRev2Imp = new DropDownList();

                                ddlper = (g1.FindControl("ddlPerformer") as DropDownCheckBoxes);
                                ddlrev = (g1.FindControl("ddlReviewer") as DropDownList);
                                ddlrev2 = (g1.FindControl("ddlReviewer2") as DropDownList);
                                ddlPerImp = (g1.FindControl("ddlPerformerIMP") as DropDownCheckBoxes);
                                ddlRevImp = (g1.FindControl("ddlReviewerIMP") as DropDownList);
                                ddlRev2Imp = (g1.FindControl("ddlReviewerIMP2") as DropDownList);

                                List<int> Auditstepdetails = new List<int>();

                                string lblLocation = (g1.FindControl("lblLocation") as Label).Text;
                                string lblVerticalName = (g1.FindControl("lblVerticalName") as Label).Text;
                                string lblLocationId = (g1.FindControl("lblLocationId") as Label).Text;
                                string lblVerticalId = (g1.FindControl("lblVerticalId") as Label).Text;
                                string lblItemTemplateFinancialYear = (g1.FindControl("lblFinancialYear") as Label).Text;
                                string lblItemTemplateForPeriod = (g1.FindControl("lblTermName") as Label).Text;
                                string lblProcessId = (g1.FindControl("lblProcessId") as Label).Text;
                                string lblSubProcessId = (g1.FindControl("lblSubProcessId") as Label).Text;
                                string lblAuditID = (g1.FindControl("lblAuditID") as Label).Text;
                                List<string> PerformerList = new List<string>();
                                List<string> PerformerListImplem = new List<string>();
                                //string lblPerformerUserID = (g1.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserID = (g1.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserID2 = (g1.FindControl("ddlReviewer2") as DropDownList).SelectedItem.Value;

                                //string lblPerformerUserIDIMP = (g1.FindControl("ddlPerformerIMP") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserIDIMP = (g1.FindControl("ddlReviewerIMP") as DropDownList).SelectedItem.Value;
                                string lblReviewerUserIDIMP2 = (g1.FindControl("ddlReviewerIMP2") as DropDownList).SelectedItem.Value;
                                string lblPerformerUserID = string.Empty;
                                string lblPerformerUserIDIMP = string.Empty;
                                for (int i = 0; i < ddlper.Items.Count; i++)
                                {
                                    if (ddlper.Items[i].Selected)
                                    {
                                        PerformerList.Add(ddlper.Items[i].Value);
                                        lblPerformerUserID = ddlper.Items[i].Value;
                                    }
                                }
                                for (int i = 0; i < ddlPerImp.Items.Count; i++)
                                {
                                    if (ddlPerImp.Items[i].Selected)
                                    {
                                        PerformerListImplem.Add(ddlPerImp.Items[i].Value);
                                        lblPerformerUserIDIMP = ddlPerImp.Items[i].Value;
                                    }
                                }
                                if (lblReviewerUserID == lblReviewerUserID2 && lblReviewerUserID != "-1" && lblReviewerUserID2 != "-1")
                                {
                                    if (!listNotSaveList.Contains(lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim()))
                                    {
                                        listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                                    }
                                }
                                else if (lblReviewerUserIDIMP == lblReviewerUserIDIMP2 && lblReviewerUserIDIMP != "-1" && lblReviewerUserIDIMP2 != "-1")
                                {
                                    if (!listNotSaveList.Contains(lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim()))
                                    {
                                        listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim() + ", ";
                                    }
                                }
                                else
                                {
                                    if (lblLocationId != "" && lblVerticalId != "" && lblItemTemplateFinancialYear != "" && lblItemTemplateForPeriod != "" && lblProcessId != "" && lblAuditID != "")
                                    {
                                        Auditstepdetails = UserManagementRisk.GetAuditStepDetailsCount(ActiveAuditStepIDList, Convert.ToInt32(lblLocationId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), userID);

                                        if (Auditstepdetails != null)
                                        {
                                            if (Auditstepdetails.Count > 0)
                                            {
                                                var AuditId = Convert.ToInt64(lblAuditID);
                                                if (lblPerformerUserID != "-1" && lblReviewerUserID != "-1" && ddlper != null && ddlper.Enabled && ddlrev != null && ddlrev.Enabled)
                                                {
                                                    #region Audit
                                                    string AssignedTo = "";
                                                    if (!string.IsNullOrEmpty(Request.QueryString["ISIE"]))
                                                    {
                                                        AssignedTo = Convert.ToString(Request.QueryString["ISIE"]);
                                                    }
                                                    else
                                                    {
                                                        AssignedTo = Convert.ToString(ViewState["ISIE"]);
                                                    }

                                                    riskinstance.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                    riskinstance.VerticalID = Convert.ToInt32(lblVerticalId);
                                                    riskinstance.ProcessId = Convert.ToInt32(lblProcessId);
                                                    riskinstance.SubProcessId = -1;
                                                    riskinstance.IsDeleted = false;
                                                    riskinstance.CreatedOn = DateTime.Now;
                                                    riskinstance.CreatedBy = userID;
                                                    riskinstance.AuditID = Convert.ToInt64(lblAuditID);

                                                    //if (distinctprocess != Convert.ToInt32(lblProcessId))
                                                    //{
                                                    //    UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                    //    CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, lblItemTemplateFinancialYear, Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblProcessId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), -1);
                                                    //    distinctprocess = Convert.ToInt32(lblProcessId);
                                                    //}

                                                    if (distinctprocess != Convert.ToInt32(lblProcessId))
                                                    {
                                                        long ProcessInstanceID = UserManagementRisk.CheckProcessInstanceIDExist(riskinstance);
                                                        if (ProcessInstanceID == 0)
                                                        {
                                                            UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);
                                                            CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, lblItemTemplateFinancialYear, Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblProcessId), Convert.ToInt32(lblVerticalId), Convert.ToInt64(lblAuditID), -1);
                                                        }
                                                        else
                                                        {
                                                            riskinstance.ID = ProcessInstanceID;
                                                        }
                                                        distinctprocess = Convert.ToInt32(lblProcessId);
                                                    }

                                                    if (PerformerList.Count > 0)
                                                    {
                                                        foreach (var item in PerformerList)
                                                        {
                                                            if (lblPerformerUserID != null && lblPerformerUserID != "-1" && ddlper.Enabled)
                                                            {
                                                                InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();
                                                                TempAssP.InternalAuditInstance = riskinstance.ID;
                                                                TempAssP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                                TempAssP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                                TempAssP.ProcessId = Convert.ToInt32(lblProcessId);
                                                                TempAssP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                                TempAssP.RoleID = 3;   //RoleManagement.GetByCode("PERF").ID;
                                                                TempAssP.UserID = Convert.ToInt32(item);
                                                                TempAssP.IsActive = true;
                                                                TempAssP.ISInternalExternal = AssignedTo;
                                                                TempAssP.CreatedOn = DateTime.Now;
                                                                TempAssP.CreatedBy = userID;
                                                                TempAssP.AuditID = Convert.ToInt64(lblAuditID);
                                                                TempAssP.AuditPeriodStartdate = null;
                                                                TempAssP.AuditPeriodEnddate = null;
                                                                Tempassignments.Add(TempAssP);
                                                            }
                                                        }
                                                    }

                                                    if (lblReviewerUserID != null && lblReviewerUserID != "-1" && ddlrev.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssR.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssR.RoleID = 4;// RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(lblAuditID);
                                                        Tempassignments.Add(TempAssR);
                                                    }

                                                    if (lblReviewerUserID2 != null && lblReviewerUserID2 != "-1" && ddlrev2.Enabled)
                                                    {
                                                        InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                                                        TempAssR.InternalAuditInstance = riskinstance.ID;
                                                        TempAssR.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssR.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssR.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssR.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssR.RoleID = 5;// RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID2);
                                                        TempAssR.IsActive = true;
                                                        TempAssR.ISInternalExternal = AssignedTo;
                                                        TempAssR.AuditPeriodStartdate = null;
                                                        TempAssR.AuditPeriodEnddate = null;
                                                        TempAssR.CreatedOn = DateTime.Now;
                                                        TempAssR.CreatedBy = userID;
                                                        TempAssR.AuditID = Convert.ToInt64(lblAuditID);
                                                        Tempassignments.Add(TempAssR);
                                                    }

                                                    if (Tempassignments.Count != 0)
                                                    {
                                                        UserManagementRisk.AddDetailsInternalControlAuditAssignment(Tempassignments);
                                                    }

                                                    //Save Total Active Ateps and Count 
                                                    if (Count == 0)
                                                    {
                                                        SaveFlag = UserManagementRisk.SaveAuditStepsDetails(Auditstepdetails, customerID, customerBranchId, verticalID, financialYear, period, Convert.ToInt64(lblAuditID));
                                                    }
                                                    Count++;
                                                    #endregion
                                                }
                                                if (lblPerformerUserIDIMP != "-1" && ddlPerImp != null && ddlPerImp.Enabled)
                                                {
                                                    #region Implementation
                                                    riskinstanceIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                    riskinstanceIMP.IsDeleted = false;
                                                    riskinstanceIMP.CreatedOn = DateTime.Now;
                                                    riskinstanceIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                    riskinstanceIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                    riskinstanceIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                    riskinstanceIMP.SubProcessId = -1;
                                                    riskinstanceIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                    riskinstanceIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                                                    if (distinctprocessImp != Convert.ToInt32(lblProcessId))
                                                    {
                                                        long InstanceID = UserManagementRisk.CheckInstanceIDExist(riskinstanceIMP);
                                                        if (InstanceID == 0)
                                                        {
                                                            UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstanceIMP);
                                                            CreateScheduleOnImplementation(riskinstanceIMP.Id,
                                                            Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.User,
                                                            lblItemTemplateFinancialYear.Trim(), Convert.ToInt32(lblLocationId), lblItemTemplateForPeriod, Convert.ToInt32(lblVerticalId), Convert.ToInt32(lblProcessId), Convert.ToInt64(lblAuditID), -1);
                                                        }
                                                        else
                                                        {
                                                            riskinstanceIMP.Id = InstanceID;
                                                        }
                                                        distinctprocessImp = Convert.ToInt32(lblProcessId);
                                                    }


                                                    List<AuditImplementationAssignment> TempassignmentsIMP = new List<AuditImplementationAssignment>();
                                                    if (PerformerListImplem.Count > 0)
                                                    {
                                                        foreach (var item in PerformerListImplem)
                                                        {
                                                            if (lblPerformerUserIDIMP != null && lblPerformerUserIDIMP != "-1" && ddlPerImp.Enabled)
                                                            {
                                                                AuditImplementationAssignment TempAssPIMP = new AuditImplementationAssignment();
                                                                TempAssPIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                                TempAssPIMP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                                                TempAssPIMP.UserID = Convert.ToInt32(item);
                                                                TempAssPIMP.IsActive = true;
                                                                TempAssPIMP.CreatedOn = DateTime.Now;
                                                                TempAssPIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                                TempAssPIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                                TempAssPIMP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                                TempAssPIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                                TempAssPIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                                TempAssPIMP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                                TempAssPIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                                TempAssPIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                                TempassignmentsIMP.Add(TempAssPIMP);
                                                            }
                                                        }
                                                    }
                                                    if (lblReviewerUserIDIMP != null && lblReviewerUserIDIMP != "-1" && ddlRevImp.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssRIMP = new AuditImplementationAssignment();
                                                        TempAssRIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssRIMP.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                                        TempAssRIMP.UserID = Convert.ToInt32(lblReviewerUserIDIMP);
                                                        TempAssRIMP.IsActive = true;
                                                        TempAssRIMP.CreatedOn = DateTime.Now;
                                                        TempAssRIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssRIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssRIMP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssRIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssRIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssRIMP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssRIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssRIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        TempassignmentsIMP.Add(TempAssRIMP);
                                                    }
                                                    if (lblReviewerUserIDIMP2 != null && lblReviewerUserIDIMP2 != "-1" && ddlRev2Imp.Enabled)
                                                    {
                                                        AuditImplementationAssignment TempAssRIMP = new AuditImplementationAssignment();
                                                        TempAssRIMP.CustomerBranchID = Convert.ToInt32(lblLocationId);
                                                        TempAssRIMP.RoleID = RoleManagement.GetByCode("RVW2").ID;
                                                        TempAssRIMP.UserID = Convert.ToInt32(lblReviewerUserIDIMP2);
                                                        TempAssRIMP.IsActive = true;
                                                        TempAssRIMP.CreatedOn = DateTime.Now;
                                                        TempAssRIMP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                                        TempAssRIMP.ImplementationInstance = riskinstanceIMP.Id;
                                                        TempAssRIMP.FinancialYear = lblItemTemplateFinancialYear.Trim();
                                                        TempAssRIMP.VerticalID = Convert.ToInt32(lblVerticalId);
                                                        TempAssRIMP.ProcessId = Convert.ToInt32(lblProcessId);
                                                        TempAssRIMP.SubProcessId = Convert.ToInt32(lblSubProcessId);
                                                        TempAssRIMP.AuditID = Convert.ToInt64(lblAuditID);
                                                        TempAssRIMP.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        TempassignmentsIMP.Add(TempAssRIMP);
                                                    }
                                                    if (TempassignmentsIMP.Count != 0)
                                                    {
                                                        UserManagementRisk.AddDetailsAuditImplementationAssignment(TempassignmentsIMP);
                                                    }
                                                    SaveFlag = true;
                                                    #endregion
                                                }

                                                if (InternalAuditAssignmnetExists(customerBranchId, verticalID, financialYear, period, Convert.ToInt32(lblAuditID)))
                                                {
                                                    btnSendEmail.Visible = true;
                                                }
                                                else
                                                {
                                                    btnSendEmail.Visible = false;
                                                }
                                            }//Auditstepdetails.Count > 0                               
                                        }
                                        else
                                        {
                                            if (!listNotSaveList.Contains(lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim()))
                                            {
                                                listNotSaveList += lblLocation.Trim() + "/" + lblItemTemplateForPeriod.Trim();
                                            }
                                        }
                                    }// grd branch fy period vertical not blank end
                                }
                            }//Foreach End 

                            #region Code added by Sushant for Process Name/SubProcess
                            var DistinctAuditID = (from row in entities.ProcessCommaSaperates
                                                   select row.AuditID).Distinct().ToList();

                            var SubProcessAuditList = (from row in entities.SubProcessCommaSeparates
                                                       select row.AuditID).Distinct().ToList();

                            var CheckAuditID = (from row in entities.AuditClosureDetails
                                                where !DistinctAuditID.Contains(row.ID)
                                                select row.ID).Distinct().ToList();

                            var CheckAuditIDSubProcess = (from row in entities.AuditClosureDetails
                                                          where !SubProcessAuditList.Contains(row.ID)
                                                          select row.ID).Distinct().ToList();

                            foreach (var item in CheckAuditID)
                            {
                                var ProcessList = (from row in entities.InternalControlAuditAssignments
                                                   join row1 in entities.Mst_Process
                                                   on row.ProcessId equals row1.Id
                                                   join RATBDM in entities.RiskActivityToBeDoneMappings
                                                   on row.CustomerBranchID equals RATBDM.CustomerBranchID
                                                   where row.AuditID == item
                                                   && row.IsActive == true
                                                   && row.ProcessId == RATBDM.ProcessId
                                                   && RATBDM.RCMType != "IFC"
                                                   select row1.Name).Distinct().ToList();

                                var ProcessNameComaSaperate = string.Empty;
                                foreach (var item1 in ProcessList)
                                {
                                    ProcessNameComaSaperate += item1 + ",";
                                }
                                ProcessNameComaSaperate = ProcessNameComaSaperate.TrimEnd(',');
                                ProcessNameComaSaperate = ProcessNameComaSaperate.TrimStart(',');
                                if (!string.IsNullOrEmpty(ProcessNameComaSaperate))
                                {
                                    ProcessCommaSaperate objProcess = new ProcessCommaSaperate();
                                    objProcess.AuditID = item;
                                    objProcess.ProcessName = ProcessNameComaSaperate;
                                    objProcess.CreatedOn = DateTime.Now;
                                    entities.ProcessCommaSaperates.Add(objProcess);
                                    entities.SaveChanges();
                                }
                            }

                            foreach (var item in CheckAuditIDSubProcess)
                            {
                                var SubProcessList = (from row in entities.InternalControlAuditAssignments
                                                      join row1 in entities.mst_Subprocess
                                                      on row.SubProcessId equals row1.Id
                                                      join RATBDM in entities.RiskActivityToBeDoneMappings
                                                      on row.CustomerBranchID equals RATBDM.CustomerBranchID
                                                      where row.AuditID == item
                                                      && row.ProcessId == RATBDM.ProcessId
                                                      && row.SubProcessId == RATBDM.SubProcessId
                                                      && row.IsActive == true
                                                      && RATBDM.RCMType != "IFC"
                                                      select row1.Name).Distinct().ToList();

                                var SubProcessNameComaSeparate = string.Empty;
                                foreach (var item1 in SubProcessList)
                                {
                                    SubProcessNameComaSeparate += item1 + ",";
                                }

                                SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimEnd(',');
                                SubProcessNameComaSeparate = SubProcessNameComaSeparate.TrimStart(',');
                                if (!string.IsNullOrEmpty(SubProcessNameComaSeparate))
                                {
                                    SubProcessCommaSeparate objSubProcess = new SubProcessCommaSeparate();
                                    objSubProcess.AuditID = item;
                                    objSubProcess.SubProcessName = SubProcessNameComaSeparate;
                                    objSubProcess.CreatedOn = DateTime.Now;
                                    entities.SubProcessCommaSeparates.Add(objSubProcess);
                                    entities.SaveChanges();
                                }
                            }
                            #endregion
                        }//Using End
                    }//branch fy period vertical not blank end

                    cvDuplicateEntry1.IsValid = false;
                    if (SaveFlag)
                    {

                        if (listNotSaveList.Length <= 0)
                        {
                            cvDuplicateEntry1.ErrorMessage = "Audit Kickoff Successfully.";
                        }
                        else
                        {
                            cvDuplicateEntry1.ErrorMessage = "Audit Kickoff Successfully. The following audits " +
                                                           "- " + listNotSaveList.Trim(',') + " not kickoff due to audit steps not uploaded or Reviewer & Reviewer2 cannot be same user";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry1.ErrorMessage = "No Audit Kickoff due to audit steps not uploaded for following " +
                            "- " + listNotSaveList.Trim(',') + " or Reviewer & Reviewer2 cannot be same user";
                    }
                    BindProcessAudit();
                }
                else
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Please select user from drop down.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool Exists(int CustomerBranchID, int ProcessId, int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (CustomerBranchID != -1 && ProcessId != -1 && userid != -1)
                {
                    var query = (from row in entities.ProcessPerformerReminderNotifications
                                 where row.ProcessID == ProcessId
                                     && row.CustomerBranchId == CustomerBranchID
                                     && row.UserID == userid
                                 select row);
                    return query.Select(entry => true).SingleOrDefault();
                }
                else
                {
                    return false;
                }
            }
        }

        public static void AddProcessPerformerReminderNotification(List<ProcessPerformerReminderNotification> objEscalation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    objEscalation.ForEach(entry =>
                    {
                        bool chelexists = Exists(Convert.ToInt32(entry.CustomerBranchId), Convert.ToInt32(entry.ProcessID), Convert.ToInt32(entry.UserID));
                        if (chelexists == false)
                        {
                            entities.ProcessPerformerReminderNotifications.Add(entry);
                            entities.SaveChanges();
                        }

                    });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static PerformerSendMailView ProcessPerformerSendMailReminder(int ProcessId, int CustomerId)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var CheckListReminders = (from row in entities.PerformerSendMailViews

                                          where row.CustomerID == CustomerId && row.RoleID == 3
                                          && row.ProcessId == ProcessId
                                          select row).FirstOrDefault();

                return CheckListReminders;
            }
        }
        public static void CreateScheduleOn(long InternalAuditInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, long Processid, int VerticalID, long AuditID, int SubProcessID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int userID = -1;
                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    DateTime curruntDate = DateTime.UtcNow;

                    List<InternalAuditTransaction> transactionlist = new List<InternalAuditTransaction>();

                    InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                    auditScheduleon.InternalAuditInstance = InternalAuditInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = Processid;
                    auditScheduleon.CreatedOn = DateTime.Now;
                    auditScheduleon.CreatedBy = userID;
                    auditScheduleon.AuditID = AuditID;
                    entities.InternalAuditScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        InternalAuditInstance = InternalAuditInstance,
                        AuditScheduleOnID = auditScheduleon.ID,
                        CreatedByText = creatdByName,
                        ProcessId = Processid,
                        SubProcessId = -1,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        VerticalID = VerticalID,
                        CreatedOn = DateTime.Now,
                        CreatedBy = userID,
                        AuditID = AuditID,
                        Remarks = "New Audit Steps Assigned."
                    };

                    transactionlist.Add(transaction);

                    CreateTransaction(transactionlist);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void CreateTransaction(List<InternalAuditTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.InternalAuditTransactions.Add(entry);
                });

                entities.SaveChanges();
            }
        }

        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudit();
        }

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvDuplicateEntry1.IsValid = false;
            cvDuplicateEntry1.ErrorMessage = finalErrMsg;
        }
        #region Implementation Status     

        public void ImpPerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, int ProcessId, int AuditID, int SubProcessID)
        {
            List<Sp_ImplementationAuditAssignedInstancesView_Result> Records = new List<Sp_ImplementationAuditAssignedInstancesView_Result>();


            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_ImplementationAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();
            }

            if (Records.Count > 0)
            {
                var Data = (from row in Records
                            where row.CustomerBranchID == CustBranchID
                            && row.FinancialYear == FinYear
                            && row.ForMonth == Period
                            && row.ProcessId == ProcessId
                            && row.SubProcessId == SubProcessID
                            && row.RoleID == RoleID && row.VerticalID == Verticalid
                            select row).FirstOrDefault();

                if (Data != null)
                {
                    ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
                    ddl.Enabled = false;
                    ddl.Attributes.Add("class", "");
                    ddl.CssClass = "form-control m-bot15 Check";
                }
                else
                    ddl.Enabled = true;
            }
        }

        public void ImpPerformerReviewerEnable(DropDownCheckBoxes ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, int ProcessId, int AuditID, int SubProcessID)
        {
            List<Sp_ImplementationAuditAssignedInstancesView_Result> Records = new List<Sp_ImplementationAuditAssignedInstancesView_Result>();


            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_ImplementationAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();
            }

            if (Records.Count > 0)
            {
                var Data = (from row in Records
                            where row.CustomerBranchID == CustBranchID
                            && row.FinancialYear == FinYear
                            && row.ForMonth == Period
                            && row.ProcessId == ProcessId
                            && row.SubProcessId == SubProcessID
                            && row.RoleID == RoleID && row.VerticalID == Verticalid
                            select row).FirstOrDefault();

                if (Data != null)
                {
                    ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
                    ddl.Enabled = false;
                    ddl.Attributes.Add("class", "");
                    ddl.CssClass = "form-control m-bot15 Check";
                }
                else
                    ddl.Enabled = true;
            }
        }

        //public void ImpPerformerReviewerEnable(DropDownList ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, int ProcessId, int AuditID, int SubProcessID)
        //{
        //    List<ImplementationAuditAssignedInstancesView> Records = new List<ImplementationAuditAssignedInstancesView>();


        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        Records = (from row in entities.ImplementationAuditAssignedInstancesViews
        //                   where row.CustomerID == CustID && row.AuditID == AuditID
        //                   select row).ToList();

        //    }

        //    if (Records.Count > 0)
        //    {
        //        var Data = (from row in Records
        //                    where row.CustomerBranchID == CustBranchID
        //                    && row.FinancialYear == FinYear
        //                    && row.ForMonth == Period
        //                    && row.ProcessId == ProcessId
        //                    && row.SubProcessId == SubProcessID
        //                    && row.RoleID == RoleID && row.VerticalID == Verticalid
        //                    select row).FirstOrDefault();

        //        if (Data != null)
        //        {
        //            ddl.Items.FindByValue(Data.UserID.ToString()).Selected = true;
        //            ddl.Enabled = false;
        //            ddl.Attributes.Add("class", "");
        //            ddl.CssClass = "form-control m-bot15";
        //        }
        //        else
        //            ddl.Enabled = true;
        //    }
        //}
        public static void CreateScheduleOnImplementation(long ImplementationInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, int VerticalId, int ProcessId, long AuditID, int SubProcessID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DateTime curruntDate = DateTime.UtcNow;

                    List<AuditImplementationTransaction> transactionlist = new List<AuditImplementationTransaction>();

                    AuditImpementationScheduleOn auditScheduleon = new AuditImpementationScheduleOn();
                    auditScheduleon.ImplementationInstance = ImplementationInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = ProcessId;
                    //auditScheduleon.
                    auditScheduleon.AuditID = AuditID;
                    auditScheduleon.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    auditScheduleon.CreatedOn = DateTime.Now;
                    entities.AuditImpementationScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        ImplementationInstance = ImplementationInstance,
                        ImplementationScheduleOnID = auditScheduleon.Id,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        ForPeriod = Period,
                        FinancialYear = FinancialYear,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        VerticalID = VerticalId,
                        ProcessId = ProcessId,
                        SubProcessId = SubProcessID,
                        Remarks = "New Implementation Steps assigned.",
                        CreatedOn = DateTime.Now,
                        AuditID = AuditID,
                    };

                    transactionlist.Add(transaction);

                    CreateTransactionImplementation(transactionlist);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        public static void CreateTransactionImplementation(List<AuditImplementationTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }

        protected void grdProcess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdProcess.PageIndex = e.NewPageIndex;
            BindProcessAudit();
            bindPageNumber();
        }
        #endregion

        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcessAudit();
            bindPageNumber();
        }
        public void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(file.Value);
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    foreach (string file in Directory.GetFiles(path))
                    {
                        File.Delete(file);
                    }
                    Directory.Delete(path);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int auditorid = -1;
                    string financialYear = string.Empty;
                    string customerBranchName = string.Empty;
                    string verticalName = string.Empty;
                    #region
                    if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                    {
                        branchid = Convert.ToInt32(Request.QueryString["BID"]);
                        ViewState["BID"] = branchid;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                    {
                        VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                        ViewState["VID"] = VerticalID;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                    {
                        FinYear = Request.QueryString["FY"];
                        ViewState["FY"] = FinYear;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                    {
                        Period = Request.QueryString["Period"];
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AUID"]))
                    {
                        AuditID = Convert.ToInt32(Request.QueryString["AUID"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["AUDTOR"]))
                    {
                        auditorid = Convert.ToInt32(Request.QueryString["AUDTOR"]);
                    }
                    else
                    {
                        auditorid = Convert.ToInt32(ViewState["AUDTOR"]);
                    }

                    #endregion

                    #region Excel Report
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    customerBranchName = CustomerBranchManagement.GetByID(branchid).Name;
                    verticalName = UserManagementRisk.GetVerticalName(customerID, VerticalID);

                    if (branchid != -1 && VerticalID != -1 && !string.IsNullOrEmpty(FinYear) && !string.IsNullOrEmpty(Period))
                    {
                        if (AuditID != 0)
                        {
                            var itemlist = (from row in entities.Sp_ExportAuditStepForPerformer(branchid, VerticalID, FinYear, Period, Convert.ToInt32(AuditID))
                                            select row).ToList();
                            if (itemlist.Count > 0)
                            {
                                using (ExcelPackage exportPackge = new ExcelPackage())
                                {
                                    try
                                    {
                                        #region
                                        String FileName = String.Empty;
                                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ExternalAuditorUpload");
                                        DataTable ExcelData = null;
                                        DataView view = new System.Data.DataView((itemlist as List<Sp_ExportAuditStepForPerformer_Result>).ToDataTable());

                                        ExcelData = view.ToTable("Selected", false, "BranchName", "VerticalName", "ProcessName", "SubProcessName", "FinancialYear",
                                        "Period", "ActivityTobeDone", "ProcessWalkthrough", "ActualWorkDone", "Population", "Sample", "Remark",
                                        "ObservationTitle", "Observation", "BriefObservation", "ObservationBackground", "ObservationReport", "BusinessImplication",
                                        "RootCause", "FinancialImpact", "Recomendation", "ManagementResponse", "TimeLine", "PersonResponsible", "Email", "ObservationRating",
                                        "ObservationCategory", "ObservationSubCategory", "Owner", "OwnerEmail", "ATBTID", "CustomerBranchID", "VerticalID", "AuditId");

                                        foreach (DataRow item in ExcelData.Rows)
                                        {
                                            if (item["BranchName"] != null && item["BranchName"] != DBNull.Value)
                                                item["BranchName"] = customerBranchName;

                                            if (item["VerticalName"] != null && item["VerticalName"] != DBNull.Value)
                                                item["VerticalName"] = verticalName;
                                        }
                                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["A1"].Value = "Branch Name";
                                        exWorkSheet.Cells["A1"].AutoFitColumns(35);

                                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["B1"].Value = "Vertical Name";
                                        exWorkSheet.Cells["B1"].AutoFitColumns(25);

                                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["C1"].Value = "Process Name";
                                        exWorkSheet.Cells["C1"].AutoFitColumns(25);

                                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["D1"].Value = "Sub Process Name";
                                        exWorkSheet.Cells["D1"].AutoFitColumns(25);

                                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["E1"].Value = "FinancialYear";
                                        exWorkSheet.Cells["E1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["F1"].Value = "Period";
                                        exWorkSheet.Cells["F1"].AutoFitColumns(15);

                                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["G1"].Value = "Audit Step";
                                        exWorkSheet.Cells["G1"].AutoFitColumns(50);

                                        exWorkSheet.Cells["H1"].Value = "Process Walkthrough";
                                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["H1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["I1"].Value = "Actual Work Done";
                                        exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["I1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["J1"].Value = "Population";
                                        exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["J1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["K1"].Value = "Sample";
                                        exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["K1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["L1"].Value = "Remark";
                                        exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["L1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["M1"].Value = "Observation Title";
                                        exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["M1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["N1"].Value = "Observation";
                                        exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["N1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["O1"].Value = "Brief Observation";
                                        exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["O1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["P1"].Value = "Observation Background";
                                        exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["P1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Q1"].Value = "Observation Report (Audit Commitee/None of the Above)";
                                        exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Q1"].AutoFitColumns(30);
                                        exWorkSheet.Cells["Q1"].Style.WrapText = true;

                                        exWorkSheet.Cells["R1"].Value = "Business Implication";
                                        exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["R1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["S1"].Value = "Root Cause";
                                        exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["S1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["T1"].Value = "Financial Impact";
                                        exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["T1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["U1"].Value = "Recomendation";
                                        exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["U1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["V1"].Value = "Management Response";
                                        exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["V1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["W1"].Value = "TimeLine";
                                        exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["W1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["X1"].Value = "Person Responsible";
                                        exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["X1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Y1"].Value = "Person Responsible Email";
                                        exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Y1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["Z1"].Value = "Observation Rating";
                                        exWorkSheet.Cells["Z1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["Z1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["Z1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AA1"].Value = "Observation Category";
                                        exWorkSheet.Cells["AA1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AA1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AA1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AB1"].Value = "Observation Sub Category";
                                        exWorkSheet.Cells["AB1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AB1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AB1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AC1"].Value = "Owner";
                                        exWorkSheet.Cells["AC1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AC1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AC1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AD1"].Value = "Owner Email";
                                        exWorkSheet.Cells["AD1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AD1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AD1"].AutoFitColumns(30);

                                        exWorkSheet.Cells["AE1"].Value = "ATBTID";
                                        exWorkSheet.Cells["AE1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AE1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AE1"].AutoFitColumns(5);

                                        exWorkSheet.Cells["AF1"].Value = "CBID";
                                        exWorkSheet.Cells["AF1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AF1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AF1"].AutoFitColumns(5);

                                        exWorkSheet.Cells["AG1"].Value = "VID";
                                        exWorkSheet.Cells["AG1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AG1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AG1"].AutoFitColumns(5);

                                        exWorkSheet.Cells["AH1"].Value = "AUID";
                                        exWorkSheet.Cells["AH1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["AH1"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["AH1"].AutoFitColumns(5);

                                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 34])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }

                                        Byte[] fileBytes = exportPackge.GetAsByteArray();

                                        #endregion

                                        #region Save Code                                              
                                        string fileName = "AuditorUploadFile_" + customerBranchName + financialYear + ".xlsx";
                                        string CreatefileName = "AuditorUploadFile_" + customerBranchName + financialYear;
                                        CreatefileName = DocumentManagement.MakeValidFileName(CreatefileName);
                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                        string directoryPath1 = "";
                                        directoryPath1 = Server.MapPath("~/AuditCheckListPerformer/" + customerID + "/"
                                            + Convert.ToInt32(branchid) + "/" + Convert.ToInt32(VerticalID) + "/"
                                            + Convert.ToString(financialYear) + "/" + Period + "");

                                        if (!Directory.Exists(directoryPath1))
                                        {
                                            DocumentManagement.CreateDirectory(directoryPath1);
                                        }
                                        else
                                        {
                                            DeleteDirectory(directoryPath1);
                                            DocumentManagement.CreateDirectory(directoryPath1);
                                        }
                                        string finalPath1 = Path.Combine(directoryPath1, CreatefileName + Path.GetExtension(fileName));
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                        SaveDocFiles(Filelist1);
                                        #endregion

                                        if (auditorid != -1)
                                        {
                                            #region External Audittor                                            
                                            var details = (from row in entities.Mst_AuditorMaster
                                                           where row.CustomerID == customerID
                                                           && row.ID == auditorid
                                                           select row).FirstOrDefault();

                                            string emailids = string.Empty;
                                            if (details != null)
                                            {
                                                if (!string.IsNullOrEmpty(details.PartnerEmail))
                                                {
                                                    emailids += details.PartnerEmail + ",";
                                                }
                                                if (!string.IsNullOrEmpty(details.AuditMEmail))
                                                {
                                                    emailids += details.AuditMEmail + ",";
                                                }
                                                emailids = emailids.Trim(',');

                                                string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;
                                                List<Attachment> attachment = new List<Attachment>();
                                                attachment.Add(new Attachment(finalPath1));


                                                //emailids = "rahul@avantis.co.in,ankit@avantis.info,narendra@avantis.info";
                                                string[] emailIsd = emailids.Split(',');


                                                string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                                                  + "Dear Sir,<br /><br />"
                                                                  // + "You have been assigned to perform attached audit steps for audit -" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + " <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                                                  + "Kindly note that an audit has been assigned to you. The attachment below has the list of audit steps which are required to be completed during the audit.<br/><br/><br/>Kindly update the details of the audit in the attached sheet and send it back for review and system updation. <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                                                string subject = "Audit Steps-" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + "";

                                                if (subject.Length > 75)
                                                    subject = "Audit Steps for Upload";


                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "External Auditor Upload.", message, attachment);

                                                AuditKickOffMailLog AKOML = new AuditKickOffMailLog()
                                                {
                                                    AuditId = (int)AuditID,
                                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    CustomerID = customerID
                                                };

                                                if (ProcessManagement.AuditKickOffLogExists(customerID, (int)AuditID))
                                                {
                                                    return;
                                                }
                                                else
                                                {
                                                    ProcessManagement.CreateAuditKickOffLog(AKOML);
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Internal User


                                            var details = (from ICAA in entities.InternalControlAuditAssignments
                                                           join IASO in entities.InternalAuditScheduleOns
                                                           on ICAA.AuditID equals IASO.AuditID
                                                           join mu in entities.mst_User
                                                           on ICAA.UserID equals mu.ID
                                                           where ICAA.InternalAuditInstance == IASO.InternalAuditInstance
                                                           && IASO.ProcessId == ICAA.ProcessId
                                                           && IASO.FinancialYear == financialYear
                                                           && IASO.ForMonth == Period
                                                           && ICAA.CustomerBranchID == branchid
                                                           && ICAA.VerticalID == VerticalID
                                                           && mu.IsDeleted == false
                                                           && mu.IsActive == true
                                                           && ICAA.RoleID == 3
                                                           && ICAA.AuditID == AuditID
                                                           select mu.Email).Distinct().ToList();

                                            string emailids = string.Empty;
                                            if (details != null)
                                            {
                                                foreach (var item in details)
                                                {
                                                    if (!string.IsNullOrEmpty(item))
                                                    {
                                                        emailids += item + ",";
                                                    }
                                                }
                                                emailids = emailids.Trim(',');

                                                string ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerID));
                                                List<Attachment> attachment = new List<Attachment>();
                                                attachment.Add(new Attachment(finalPath1));

                                                //emailids = "rahul@avantis.co.in,ankit@avantis.info,narendra@avantis.info";
                                                string[] emailIsd = emailids.Split(',');

                                                //string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                                //+ "Dear Sir,<br /><br />"
                                                //+ "Here is Attached Audit Steps <br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                                                string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                                   + "Dear Sir,<br /><br />" +
                                                  //+ "You have been assigned to perform attached audit steps for audit -" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + " <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                                  "Kindly note that an audit has been assigned to you. The attachment below has the list of audit steps which are required to be completed during the audit.<br/><br/> Kindly update the details of the audit in the attached sheet and send it back for review and system updation. <br/><br/>Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                                                string subject = "Audit Steps-" + customerBranchName + "/" + verticalName + "/" + financialYear + "/" + Period + "";

                                                if (subject.Length > 75)
                                                    subject = "Audit Steps for Upload";

                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Internal Auditor Upload.", message, attachment);

                                                AuditKickOffMailLog AKOML = new AuditKickOffMailLog()
                                                {
                                                    AuditId = (int)AuditID,
                                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                    CustomerID = customerID
                                                };

                                                if (ProcessManagement.AuditKickOffLogExists(customerID, (int)AuditID))
                                                {
                                                    return;
                                                }
                                                else
                                                {
                                                    ProcessManagement.CreateAuditKickOffLog(AKOML);
                                                }
                                            }
                                            #endregion

                                        }
                                        #region Excel Download Code
                                        //Response.ClearContent();
                                        //Response.Buffer = true;
                                        ////string fileName = string.Empty;
                                        //fileName = "asd.xlsx";
                                        //Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                        //Response.Charset = "";
                                        //Response.ContentType = "application/vnd.ms-excel";
                                        //StringWriter sw = new StringWriter();
                                        //Response.BinaryWrite(fileBytes);
                                        //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                        #endregion

                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                            }//itemlist.Count > 0 end
                        }//auditID != 0 end
                    }//customerBranchId != -1 && verticalID != -1 && !string.IsNullOrEmpty(financialYear) && !string.IsNullOrEmpty(Period) end
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public static long GetPerformerID(string ForPeriod, string FinancialYear, int CustomerBranchid, int VerticalId, int ProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var reviewerid = (from row in entities.InternalControlAuditAssignments
                                  join row1 in entities.InternalAuditScheduleOns
                                  on row.InternalAuditInstance equals row1.InternalAuditInstance
                                  where row.ProcessId == row1.ProcessId && row.CustomerBranchID == CustomerBranchid &&
                                  row.VerticalID == VerticalId &&
                                  row1.ForMonth == ForPeriod && row1.FinancialYear == FinancialYear
                                  && row.ProcessId == ProcessID
                                  && row.RoleID == 3
                                  select row.UserID).FirstOrDefault();

                return (long)reviewerid;
            }
        }
        private void ProcessReminderPerformer(string ForPeriod, string FinancialYear, int CustomerBranchid, int VerticalId, int ProcessID)
        {
            try
            {
                long userid = GetPerformerID(ForPeriod, FinancialYear, CustomerBranchid, VerticalId, ProcessID);
                var user = UserManagement.GetByID(Convert.ToInt32(userid));
                if (user != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string ReplyEmailAddressName = CustomerManagementRisk.GetByID(Convert.ToInt32(customerID)).Name;

                    List<Attachment> attachment = new List<Attachment>();
                    attachment.Add(new Attachment("c:/textfile.txt"));

                    string emailids = "rahul@avantis.co.in,ankit@avantis.info,narendra@avantis.info";
                    string[] emailIsd = emailids.Split(',');


                    string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                     + "Dear Sir,<br /><br />"
                                     + "Here is Attached dashboard summary pdf.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";

                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Approver Dashboards.", message, attachment);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void MultiplePerformerReviewerEnable(DropDownCheckBoxes ddl, int CustID, int CustBranchID, long ProcessID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID, int SubProcessID)
        {
            List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();

                if (Records.Count > 0)
                {
                    var Data = (from row in Records
                                where row.ProcessId == ProcessID
                                && row.SubProcessId == SubProcessID
                                && row.CustomerBranchID == CustBranchID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                && row.RoleID == RoleID && row.VerticalID == VerticalId
                                select row).ToList();

                    if (Data.Count > 0)
                    {
                        foreach (var item in Data)
                        {
                            // ddl.Items.FindByValue(item.UserID.ToString()).Selected = true;
                            #region code added by sagar on 15-07-2020
                            for (int i = 0; i < ddl.Items.Count; i++)
                            {
                                if (ddl.Items[i].Value == Convert.ToString(item.UserID))
                                {
                                    ddl.Items[i].Selected = true;
                                }
                            }
                            #endregion
                            ddl.Enabled = false;
                            ddl.Attributes.Add("class", "");
                            ddl.CssClass = "form-control m-bot15 Check";
                        }
                    }
                    else
                        ddl.Enabled = true;
                }
            }
        }

        public void MultiplePerformerReviewerEnableHeader(DropDownCheckBoxes ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int VerticalId, int AuditID)
        {
            List<Sp_InternalAuditAssignedInstancesView_Result> Records = new List<Sp_InternalAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_InternalAuditAssignedInstancesView(AuditID)
                               //where row.CustomerID == CustID && row.AuditID == AuditID
                           select row).ToList();

                if (Records.Count > 0)
                {
                    var Data = (from row in Records
                                where row.CustomerBranchID == CustBranchID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                && row.RoleID == RoleID && row.VerticalID == VerticalId
                                select row).ToList();

                    if (Data.Count > 0)
                    {

                        foreach (var item in Data)
                        {
                            // ddl.Items.FindByValue(item.UserID.ToString()).Selected = true;
                            #region code added by sagar on 15-07-2020
                            for (int i = 0; i < ddl.Items.Count; i++)
                            {
                                if (ddl.Items[i].Value == Convert.ToString(item.UserID))
                                {
                                    ddl.Items[i].Selected = true;
                                }
                            }
                            #endregion
                            ddl.Enabled = false;
                            ddl.Attributes.Add("class", "");
                            ddl.CssClass = "form-control m-bot15 Check";
                        }

                    }
                    else
                        ddl.Enabled = true;
                }
            }
        }



        public void ImpMultiplePerformerReviewerEnable(DropDownCheckBoxes ddl, int CustID, int CustBranchID, String FinYear, String Period, int RoleID, int Verticalid, int ProcessId, int AuditID, int SubProcessID)
        {
            List<Sp_ImplementationAuditAssignedInstancesView_Result> Records = new List<Sp_ImplementationAuditAssignedInstancesView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Records = (from row in entities.Sp_ImplementationAuditAssignedInstancesView(AuditID)
                           select row).ToList();
            }

            if (Records.Count > 0)
            {
                var Data = (from row in Records
                            where row.CustomerBranchID == CustBranchID
                            && row.FinancialYear == FinYear
                            && row.ForMonth == Period
                            && row.ProcessId == ProcessId
                            && row.SubProcessId == SubProcessID
                            && row.RoleID == RoleID && row.VerticalID == Verticalid
                            select row).ToList();

                if (Data.Count > 0)
                {
                    foreach (var item in Data)
                    {
                        ddl.Items.FindByValue(item.UserID.ToString()).Selected = true;
                        ddl.Enabled = false;
                        ddl.Attributes.Add("class", "");
                        ddl.CssClass = "form-control m-bot15 Check";
                    }
                }
                else
                    ddl.Enabled = true;
            }
        }
    }
}