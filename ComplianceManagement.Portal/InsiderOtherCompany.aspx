﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderOtherCompany.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderOtherCompany" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">

    .k-i-arrow-60-down{
        margin-top:5px;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <div class="col-sm-12">
                     <div class="col-sm-12" style="padding:0">
                <div class="toolbar">  
                       <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Relationship Company: &nbsp; </label>
                <input id="dropdownlistCompanyType" data-placeholder="Type" style="width:172px;">
                 <button  style="float:right;"  id="button" type="button" class="btn btn-primary"  onclick="OpenAdvanceSearch(event)" >Add New</button> 
                </div>
                 
                        
            </div>
        <div class="col-sm-12" style="margin-bottom:1%;" >
            
        </div>

    <div class="col-sm-12" id="grid"></div>
  </div>    
        <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div  class="col-sm-12" style="padding:0">
                       <form  class="form-inline" method="POST" id="fcontact"  >
                  
            <div class="col-sm-12" style="padding:0;padding-top:10px;">
            <div class="form-group required col-sm-6" style="padding:0;">
                <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Name:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="name" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="form-group required col-sm-6" style="padding:0;margin-top:1px;">
                <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Association Type:</label>
                <input id="associate" data-placeholder="Type" style="width:40%;">
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="form-group required col-sm-6" style="padding:0;">
                <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Relation Company:</label>
                <input id="relationshipcompany" data-placeholder="Type" style="width:40%;">
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            
                <div class="form-group required col-sm-6" style="padding:0;">
                <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">CIN:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="cin" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="CIN" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="form-group required col-sm-6  " style="padding:0;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Email ID:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="email" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            
            <div class="col-sm-6" style="padding:0;">
                <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="firstName">Registered Address:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="address" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Address" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>

                
            </div>
            
            
          
            
            </div>
            <button id="saveBtn" style="margin-top:20px;" type="submit" onclick="saveDetails(event)" class="btn btn-primary">Save</button>
                                        
                       </form>
                      </div>
           </div>
    <input type="hidden" id="editFlag" value="0" />
        <script>

            $(document).ready(function () {
                $("#pagetype").text("Company Master");
                $("#grid").kendoGrid({

                    dataSource: {
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getcomptable/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>
                        },
                    schema: {
                        data: function (response) {
                            return response.data;
                        },
                        total: function (response) {
                            return response.total;
                        },
                        model: {
                            fields: {
                                Name: { type: "string" },
                                Relation: { type: "string" },
                                RelationCompany: { type: "string" },
                                Address: { type: "string" },
                                CIN: { type: "string" },

                            }
                        }
                    },
                    pageSize: 10
                },
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                //height: 550,
                //toolbar: ["search"],
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{

                    field: "Name",
                    title: "Name",
                    width: 150

                },  {

                        field: "CIN",
                        title: "CIN",
                        width: 100

                    },{

                    field: "Relation",
                    title: "Association Type",
                    width: 150

                    }, {

                        field: "RelationCompany",
                        title: "Relation Company",
                        width: 150

                    } , {

                        field: "Email",
                        title: "Email",
                        width: 150

                    }, {

                    field: "Address",
                    title: "Address",
                    width: 150

                }, {
                    template: "<div class='customer-photo'" +
                        "style='background-image: url(../../Images/edit_icon.png);'" + " onClick='OpenAdvanceSearch(#= JSON.stringify(data) #)' >",
                    field: "",
                    title: "Action",
                    width: 100
                }]
                });

                $("#relationshipcompany").kendoDropDownList({
                    dataTextField: "Name",
                    dataValueField: "ID",
                    dataSource: {
                        transport: {
                            read: {
                                url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompany/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                            }
                        }
                    }
                });
                $("#associate").kendoDropDownList({
                    placeholder: "",
                    dataTextField: "text",
                    dataValueField: "value",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: true,
                    dataSource: [
                        { text: "Associate", value: "Associate" },
                        { text: "Joint Venture", value: "Joint Venture" },
                        { text: "Subsidiary", value: "Subsidiary" },
                        { text: "Holding", value: "Holding" }
                    ],
                });
        });
    </script>
<script>

    function OpenAdvanceSearch(dd) {
        //alert(JSON.stringify(dd));
        if (dd.Name != undefined) {
            $('#editFlag').val(dd.ID);
        } else {
            $('#editFlag').val(0);
        }
        $('#name').val(dd.Name);
        $('#cin').val(dd.CIN);
        $('#email').val(dd.Email);
        $('#address').val(dd.Address);
        $("#associate").data("kendoDropDownList").value(dd.Relation);
       // $("#relationshipcompany").data("kendoDropDownList").value(dd.RelationCompany);
        
        var myWindowAdv = $("#divAdvanceSearchModel");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "65%",
            height: "35%",
            title: "Add New Company",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                "Maximize",
                "Close"
            ],
            close: onClose
        });
        $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
        e.preventDefault();
        return false;
    }

    function OpenAdvanceSearchFilter(e) {
        $('#divAdvanceSearchFilterModel').modal('show');
        e.preventDefault();
        return false;
    }
        </script>
    
    <script>
        $("#dropdownRole").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "DP", value: "38" },
                { text: "HDCS", value: "29" },
            ],
            index: 0,
            change: function (e) {


            }

        });
    </script>
    <script>
        function saveDetails(e) {
            e.preventDefault();
            //confirm("Are you sure want to add details?");
            var editFlag = $("#editFlag").val();
            var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
            var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
            var name = $('#name').val();
            var relation = $('#associate').val();
            var cin = $('#cin').val();
            var address = $('#address').val();
            var relationshipcompany = $('#relationshipcompany').val();
            var email = $('#email').val();
            var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";
            //var pdata = ;

            //alert(pdata);


            $.ajax({
                type: 'post',
                url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createcomp/',
                data: { "user_id": user_id, "name": name, "customer_id": customer_id, "relation": relation, "address": address, "created_by": created_by, "cin": cin, "relationshipcompany": relationshipcompany, "email": email, "editflag": editFlag },
                success: function (result) {
                    alert(result.data);
                    location.reload();
                },
                error: function (e, t) { alert('something went wrong'); }
            });

            return false;
        }
        </script>
        <script>
            $("#dropdownlistCompanyType").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompanyall/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                        }
                    }
                },
                change: function (e) {
                    //alert(e.sender.value());
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getcomptablecompanyspecific/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + this.value()
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    Name: { type: "string" },
                                    Relation: { type: "string" },
                                    RelationCompany: { type: "string" },
                                    Address: { type: "string" },
                                    CIN: { type: "string" },

                                }
                            }
                        }


                    });
                    var grid = $("#grid").data("kendoGrid");
                    grid.setDataSource(dataSource);
                    // Use the value of the widget
                }
            });
        </script>
</asp:Content>
