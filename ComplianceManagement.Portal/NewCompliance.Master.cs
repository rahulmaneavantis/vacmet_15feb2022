﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class NewCompliance : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string user_Roles;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;
        protected bool user_Audit;
        protected bool PageAutherzation;
        protected bool DeptHead = false;
        protected bool showLicenseManagement = false;
        protected bool IsCertificateVisible = false;
        protected string LogoName;
        protected bool IsOwner = false;
        protected bool IsOfficer = false;
        protected bool IsReviewer = false;
        protected bool DisplayLockingDaysReviewer = false;
        protected bool LockUnlockCustomerEnable = false;
        protected bool IsMgmtCertificate = false;
        public static bool DeptCustID;
        public static bool MGM_KEy;
        public static bool MgmtTask;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   
                customerid =Convert.ToInt32(AuthenticationHelper.CustomerID);
                userid = AuthenticationHelper.UserID;
                if (AuthenticationHelper.CustomerID != -1)
                {
                    string CustomerLogo = string.Empty;
                    Customer objCust = UserManagement.GetCustomerforLogo(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (objCust != null)
                    {
                        if (objCust.LogoPath != null)
                        {
                            CustomerLogo = objCust.LogoPath;
                            LogoName = CustomerLogo.Replace("~", "../..");
                        }
                    }
                }


                MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
                DeptCustID = CaseManagement.CheckForClient(customerid, "IsDeptHead");
                MgmtTask = CaseManagement.CheckForClient(customerid, "MgmtTask");
                user_Audit = CustomerBranchManagement.AuditNameExist(Convert.ToInt32(AuthenticationHelper.CustomerID));
                PageAutherzation = UserManagement.IsPageAutherzation(Convert.ToInt32(AuthenticationHelper.UserID),Convert.ToInt32(AuthenticationHelper.CustomerID));
                Page.Header.DataBind();
                //var UserDetails = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                string customer = ConfigurationManager.AppSettings["CertificateDisplayCustID"].ToString();
                List<string> CertificateDisplayCustomerList = customer.Split(',').ToList();
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                  IsCertificateVisible = UserManagement.GetComplianceCertificateMappingNew(customerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ReviewerIsApplicable = (from row in entities.ComplianceInstances
                                                join y in entities.ComplianceAssignments
                                                on row.ID equals y.ComplianceInstanceID
                                                where row.Cer_OfficerUserID != null
                                                && row.Cer_OwnerUserID != null
                                                && y.UserID == userid && y.RoleID == 4
                                                select row).ToList();

                    var ReviewerIsApplicableForLocking = (from row in entities.ComplianceInstances
                                                join y in entities.ComplianceAssignments
                                                on row.ID equals y.ComplianceInstanceID
                                                where y.UserID == userid && y.RoleID == 4
                                                select row).ToList();
                    if (ReviewerIsApplicable.Count > 0)
                    {
                        IsReviewer = true;
                    }
                    if (ReviewerIsApplicableForLocking.Count > 0)
                    {
                        DisplayLockingDaysReviewer = true;
                    }
                    LockUnlockCustomerEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "LockUnlock");

                }
                //if (CertificateMappingData != null)
                //{
                //    IsCertificateVisible = true;
                //}

                    if (Session["LastLoginTime"] != null)
                {
                    LastLoginDate = Session["LastLoginTime"].ToString();
                }

                if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                {
                    if (AuthenticationHelper.UserID != -1)
                    {                        
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                        if (cname != null)
                        {
                            CustomerName = cname;
                        }
                    }
                }
                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                }
                if (Session["User_dept"] != null)
                {
                    DeptHead =(bool)Session["User_dept"];
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(LoggedUser.IsHead)))
                    {
                        DeptHead = (bool)LoggedUser.IsHead;
                    }
                }
                if (roles.Contains(6))
                {
                    Approveruser_Roles = "APPR";
                }
                else
                {
                    Approveruser_Roles = "";
                }
                //Commented by Narendra 19 April 2019
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();

                //    if (GetApprover.Count > 0)
                //    {
                //        Approveruser_Roles = "APPR";
                //    }
                //    else
                //    {
                //        Approveruser_Roles = "";
                //    }
                //}
                user_Roles = AuthenticationHelper.Role;
                
                
                //License
                var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerid));
                if (ProductMappingDetails.Contains(6))
                {
                    if (LoggedUser != null)
                    {
                        if (LoggedUser.LicenseRoleID != null)
                        {
                            showLicenseManagement = true;
                        }
                    }
                }
                //End License
                if (Session["User_tasks_Roles"] != null)
                {
                    TaskRoles = Session["User_tasks_Roles"] as List<int>;
                }
                else
                {
                    TaskRoles = TaskManagment.GetAssignedTaskUserRole(AuthenticationHelper.UserID);
                }

                if (LoggedUser.CustomerID == null)
                {
                    checkTaskapplicable = 2;
                }
                else
                {
                    //var IsTaskApplicable = CustomerManagement.GetByTaskApplicableID(Convert.ToInt32(LoggedUser.CustomerID));
                    //if (IsTaskApplicable != -1)
                    //{
                    //    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    //}
                    var IsTaskApplicable = AuthenticationHelper.TaskApplicable;
                }



                if (LoggedUser.ImagePath != null)
                {
                    ProfilePic.Src = LoggedUser.ImagePath;
                    ProfilePicTop.Src = LoggedUser.ImagePath;
                    ProfilePicSide.Src = LoggedUser.ImagePath;
                }
                else
                {
                    ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicSide.Src = "~/UserPhotos/DefaultImage.png";
                }


                if (LoggedUser.Cer_OwnerRoleID == 1)
                {
                    IsOwner = true;
                }
           
                if (AuthenticationHelper.Role.Equals("MGMT"))
                {
                    if (LoggedUser.IsCertificateVisible == 1)
                    {
                        IsMgmtCertificate = true;
                    }
                }
                if (LoggedUser.Cer_OfficerRoleID == 1)
                {
                    IsOfficer = true;
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}