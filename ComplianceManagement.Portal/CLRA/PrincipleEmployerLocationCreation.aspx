﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="PrincipleEmployerLocationCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployerLocationCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<!DOCTYPE html>--%>
<%--
<html xmlns="http://www.w3.org/1999/xhtml">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--<head runat="server">--%>
    <title></title>
    <link href="../NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
	<link href="../NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
	<link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="../Newjs/3.3.1/jquery.min.js"></script>
	<script src="../Newjs/3.3.7/bootstrap.min.js"></script>
	<script src="../Newjs/bootstrap-datepicker.min.js"></script>
	<script src="../Newjs/jquery.validate.min.js"></script>
	<script src="../Newjs/jquery.validate.unobtrusive.js"></script>

	<link href="style.css" rel="stylesheet" />
	<link href="../assets/select2.min.css" rel="stylesheet" />
	<script src="../assets/select2.min.js"></script>

	<link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
	<script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
	<link href="../NewCSS/font-awesome.min.css" rel="stylesheet" />
	<link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

	<script src="../Newjs/fastselect.standalone.js"></script>
	<link href="../NewCSS/fastselect.min.css" rel="stylesheet" />





    <style type="text/css">
        /*.k-grid-content {
            min-height:  300px;
            max-height: 400px;
        }
        */
        
       .modal-header h4.modal-title{
           margin-top:-11px;
       }
       .modal-header .close {
         margin-top: -16px;

        }
       
       
        .modal-open {
    overflow: scroll;
}

       

        .btn{
            font-size:14px;
            font-weight:400;
        }

        

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .btn-primary[disabled] {
            background-color: #d7d7d7;
            border-color: #d7d7d7;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .form-control{
            height: 32px;
        }

        .k-multiselect-wrap .k-input {
            padding-top: 6px;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
        .col-md-12 {
            text-align: center;
        }
        .k-pager-sizes {
            float: left;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
             cursor:pointer;

        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .k-grid-header th.k-with-icon .k-link{
            text-align:center;
        }

        .inactiveclass {
        pointer-events:none;
        }


        .fstResultItem{
            text-align:left;
        }

        .checkbox label, .radio label{
            padding-left:0px;
        }
            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
                 cursor:pointer;
            }

        .k-grid, .k-listview {
            margin-top: 10px;
        }

            .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            }

        .k-tooltip-content {
            width: max-content;
        }

        a:focus {
        outline:none !important;
        }

      .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected{
          box-shadow:none !important; 
      }

      .pem{
            text-align:left;

      }

      input[type=checkbox], input[type=radio]{
          margin:4px 3px 2px -3px !important;
      }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-grid input.k-checkbox + label.k-checkbox-label{
            margin-right:15px;
        }

        .k-calendar-container {
            background-color: white;
            width: 217px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: 0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }


        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left: 2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y: scroll! important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            text-align:center;
            font-weight: 400;
        }
        .k-button.k-button-icon .k-icon, .k-grid-filter .k-icon, .k-header .k-icon{
            margin-right:10px;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

      

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
            text-align:left;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height: 34px;
        }

        .k-multiselect-wrap, .k-floatwrap {
            height: 34px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 1%;
            margin-bottom: 1%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
            color: #515967;
            padding-top: 5px;
        }

        .k-filter-row th, .k-grid-header th[data-index='10'] {
            text-align: center;
        }


        .fstElement {
            font-size: 0.7em;
            height: 34px;
        }

        .fstToggleBtn {
            min-width: 16.5em;
        }

        .submitBtn {
            display: none;
        }

        .fstMultipleMode {
            display: block;
        }

            .fstMultipleMode .fstControls {
                width: 100%;
            }
  
              .k-checkbox-label:hover {
                  color: #1fd9e1;
              }

              .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
                  cursor: pointer;
                  background-color: transparent !important;
                  border-color: transparent;
                  color: #1fd9e1;
              }

              .btn {
                  font-weight: 400;
              }

              .k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
                  color: #2e2e2e;
                  background-color: #d8d6d6 !important;
              }

              .k-grid-content {
                  min-height: auto !important;
              }

              .k-multiselect-wrap .k-input {
                  padding-top: 6px;
              }

              .k-grid tbody .k-button {
                  min-width: 19px;
                  min-height: 25px;
                  background-color: transparent;
                  border: none;
                  margin-left: 0px;
                  margin-right: 0px;
                  padding-left: 2px;
                  padding-right: 2px;
                  border-radius: 25px;
              }

              .k-button.k-state-active, .k-button:active {
                  color: black;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li:hover {
                  color: white;
                  background-color: #1fd9e1;
                  border-top-left-radius: 10px;
                  border-top-right-radius: 10px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li {
                  margin-left: 5px !important;
                  margin-right: 5px !important;
              }

              .panel-heading .nav {
                  background-color: #f8f8f8;
                  border: none;
                  font-size: 11px;
                  margin: 0px 0px 0px 0;
                  border-radius: 10px;
              }

            .k-grid, .k-listview {
                  margin-top: 0px;
              }

                  .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                      -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                      box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                  }

              .k-tooltip-content {
                  width: max-content;
              }

              input[type=checkbox], input[type=radio] {
                  margin: 4px -2px 0;
                  margin-top: 1px\9;
                  line-height: normal;
              }

              .k-calendar-container {
                  background-color: white;
                  width: 217px;
              }

              .div.k-grid-footer, div.k-grid-header {
                  border-top-width: 1px;
                  margin-right: 0px;
                  margin-top: 0px;
              }


              .k-grid-footer-wrap, .k-grid-header-wrap {
                  position: relative;
                  width: 100%;
                  overflow: hidden !important;
                  border-style: solid;
                  border-width: 0 1px 0 0;
                  zoom: 1;
              }


              html {
                  color: #666666;
                  font-size: 15px;
                  font-weight: normal;
                  font-family: 'Roboto',sans-serif;
              }

              .k-checkbox-label, .k-radio-label {
                  display: inline;
              }

              .myKendoCustomClass {
                  z-index: 999 !important;
              }

              .k-header .k-grid-toolbar {
                  background: white;
                  float: left;
                  width: 100%;
              }

              .k-grid td {
                  line-height: 2.0em;
                  border-bottom-width: 1px;
                  background-color: white;
                  border-width: 0 1px 1px 0px;
              }

              .k-i-more-vertical:before {
                  content: "\e006";
              }

              .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                  background-color: #1fd9e1;
                  background-image: none;
                  background-color: white;
              }

              k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                  color: #000000;
                  border-color: #1fd9e1;
                  background-color: white;
              }

              /*#tblPrincipleEmployer .k-grid-toolbar {
            background: white;
        }*/

              .k-pager-wrap > .k-link > .k-icon {
                  margin-top: 0px;
                  margin-left: 2.7px;
                  color: inherit;
              }

              .toolbar {
                  float: left;
              }

              html .k-grid tr:hover {
                  background: white;
              }

              html .k-grid tr.k-alt:hover {
                  background: white;
              }


              .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                  margin-right: 0px;
                  margin-right: 0px;
                  margin-left: 0px;
                  margin-left: 0px;
              }

             

              .k-grid-header {
                  padding-right: 0px !important;
                  margin-right: 2px;
              }

              .k-filter-menu .k-button {
                  width: 27%;
              }

              .k-label input[type="checkbox"] {
                  margin: 0px 5px 0 !important;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  font-size: 15px;
                  background: #f8f8f8;
                  font-family: 'Roboto',sans-serif;
                  color: #2b2b2b;
                  font-weight: 400;
              }

              .k-primary {
                  border-color: #1fd9e1;
                  background-color: #1fd9e1;
              }

              .k-pager-wrap {
                  background-color: white;
                  color: #2b2b2b;
              }

              td.k-command-cell {
                  border-width: 0 0px 1px 0px;
                  text-align: center;
              }

              .k-grid-pager {
                  margin-top: -1px;
              }

              span.k-icon.k-i-calendar {
                  margin-top: 6px;
              }

              .col-md-2 {
                  width: 20%;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  border-width: 0px 0px 1px 0px;
                  background: #E9EAEA;
                  font-weight: bold;
                  margin-right: 18px;
                  font-family: 'Roboto',sans-serif;
                  font-size: 15px;
                  color: rgba(0, 0, 0, 0.5);
                  height: 20px;
                  vertical-align: middle;
              }

              .k-dropdown-wrap.k-state-default {
                  background-color: white;
                  height: 34px;
              }

              .k-multiselect-wrap, .k-floatwrap {
                  height: 34px;
              }

              .k-popup.k-calendar-container, .k-popup.k-list-container {
                  background-color: white;
              }

              .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
                  -webkit-box-shadow: inset 0 0 3px 1px white;
                  box-shadow: inset 0 0 3px 1px white;
              }

              label.k-label {
                  font-family: roboto,sans-serif !important;
                  color: #515967;
                  /* font-stretch: 100%; */
                  font-style: normal;
                  font-weight: 400;
                  min-width: max-content !important;
                  white-space: pre-wrap;
              }

              .k-multicheck-wrap .k-item {
                  line-height: 1.2em;
                  font-size: 14px;
                  margin-bottom: 5px;
              }

              label {
                  display: flex;
                  margin-bottom: 0px;
              }

              .k-state-default > .k-select {
                  border-color: #ceced2;
                  margin-top: 3px;
              }

              .k-grid-norecords {
                  width: 100%;
                  height: 100%;
                  text-align: center;
                  margin-top: 2%;
              }

              .k-grouping-header {
                  font-style: italic;
                  background-color: white;
              }

              .k-grid-toolbar {
                  background: white;
                  border: none;
              }

              .k-grid table {
                  width: 100.5%;
              }

              .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
                  color: #515967;
                  padding-top: 5px;
              }

              .k-filter-row th, .k-grid-header th[data-index='11'] {
                  text-align: center;
              }

              .k-i-filter-clear {
                  margin-top: -3px;
              }

              .k-button {
                  margin-top: 2.5px;
              }

              label.k-label:hover {
                  color: #1fd9e1;
              }

              .k-tooltip {
                  margin-top: 5px;
              }

              .k-column-menu > .k-menu {
                  background-color: white;
              }

            
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

        .select2-container{
            width:100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>
        

    <script type="text/javascript">


        //var item = document.getElementById("liPELocationCreation");
        //item.classList.add("active");
            
        var EmployeeMasterListData = [];

        $(document).ready(function () {

            var activeclass = $('#liPELocationCreation');
            activeclass.addClass('active');

           // $("#divCheckPE").hide();
          //  $(".tabdata").hide();
          //  $("#divPECreation").show();
           // $("#divMsg").hide();
            $("#divMsgPELocation").hide();
            $('#btnClearLocation').hide();
           // $("#divMsgContractor").hide();
           // $("#divErrorsPE").hide();
            $("#divErrorsPELocation").hide();
           // $("#divErrorsContractor").hide();
           // $("#divErrorsEmployees").hide();

            $("#divCheckPELocation").hide();
         //   $("#divCheckEmployees").hide();
          //  $("#divCheckContractor").hide();
          //  $("#divPrincipleEmployer").hide();
           BindClientList();
         //  BindPrincipleEmployerTable("");
           BindPrincipleEmployerList();

            
          

         
            $("#btnNewPELocation").on("click", function (e) {
                $("#divMsgPELocation").hide();
                $("#hiddmode").val("A");
                $("#ModalPrincipleEmployerLocation").modal("show");
                ClrPELocation();
              
                $('#ddlWeekoff').fastselect();



                $("#txtBranch").css('pointer-events','inherit');



                $("#hybrid1").removeClass("hidden");
                
                $("#hybrid2").addClass("hidden");
               
            });

          

            $("#btnUploadLocation").on("click", function (e) {
                OpenLocationUploadPopup();
            });
            

                     
            $("#ddlPrincipleEmployer").on("change", function (e) {
                $("#divCheckPELocation").html("");
                $("#divCheckPELocation").hide();
                $("#divUploadLocation").hide();

                if ($("#ddlPrincipleEmployer").val() == "-1") {
                    $("#divNewLocation").addClass("hidden");
                    $("#tblPrincipleEmployerLocation").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployer").select2();

                    if ($("#divNewLocation").hasClass("hidden")) {
                        $("#divNewLocation").removeClass("hidden");
                    }
                    if ($("#tblPrincipleEmployerLocation").hasClass("hidden")) {
                        $("#tblPrincipleEmployerLocation").removeClass("hidden");
                    }
                    BindClientStateList();
                    BindPrincipleEmployerLocationTable();
                    debugger;
                    //var employerID = $("#ddlPrincipleEmployer").val();
                  

                   <%-- '<%Session["employerId"] = "' + employerID + '"; %>';
                     alert('<%=Session["employerId"] %>');--%>
                    
                   
                }

            });

          

            $("#ddlPrincipleEmployerState").on("change", function (e) {
                $("#divCheckPELocation").html("");
                $("#divCheckPELocation").hide();

                if ($("#ddlPrincipleEmployerState").val() == "-1")  {
                   // $("#divLocation").addClass("hidden");
                    $("#divNewLocation").addClass("hidden");
                   // $("#tblPrincipleEmployerLocation").addClass("hidden");
                }
                else {
                  //  $("#ddlPrincipleEmployerState").select2();
                   // if ($("#divLocation").hasClass("hidden"))
                   //     $("#divLocation").removeClass("hidden");

                    BindClientLocationList($(this).val());
                    //BindPrincipleEmployerLocationTable();
                   // $("#divNewLocation").removeClass("hidden");
                   // $("#tblPrincipleEmployerLocation").addClass("hidden");
                }


            });

            

            $("#btnLocationSave").click(function (e) {
                var location = document.location.href;
                var queryString = location.split('clientid=')[1];
                var queryString = queryString.replace(/[^\w\s]/gi, '')

                if ($('#txtBranch1').is(':visible')) {
                    var branchval = $("#txtBranch1").val()
                }
                else {
                    var branchval = $("#txtBranch").val()
                }
               
                if (ValidatePrincipleEmployerLocation()) {
                    debugger;
                    var id = $("#lblPELID").text();
                    //alert("id is " + id);
                    $("#divErrorsPELocation").hide();
                    var DetailsObj = {
                        PLID: $("#lblPELID").text(),
                        PEID: $("#ddlPrincipleEmployer").val(),
                        State: $("#ddlPrincipleEmployerState").val(),
                        ClientID: queryString,
                        Location: $("#ddlPrincipleEmployerLocation").val(),
                        Branch: branchval,
                        NatureOfBusiness: $("#txtbarchNatureOfBusiness").val(),
                        Mines: ($("#chkMineApplicability").is(":checked") ? "1" : "0"),
                        WeekOff: $("#ddlWeekoff").val(),
                        //,Status: ($("#chklocationStatus").is(":checked") ? "A" : "I")

                        PE_LIN: $("#txtPE_LIN").val(),
                        PE_AuthorisedPerson_EmailID: $("#txtPE_AuthorisedPerson_EmailID").val(),
                        PE_Company_PhoneNo: $("#txtPE_Company_PhoneNo").val(),
                        Client_LINNo: $("#txtClient_LINNo").val(),
                        Client_CompanyEmailID: $("#txtClient_CompanyEmailID").val(),
                        Client_Company_Phone_No: $("#txtClient_Company_Phone_No").val(),
                        Contract_Licence_No: $("#txtContract_Licence_No").val(),
                        Licence_Valid_From_date: $("#txtLicenceValidFromdate").val(),
                        Licence_Valid_To_date: $("#txtLicenceValidTodate").val(),
                        Contractor_Person_Incharge_Name: $("#txtContractor_Person_Incharge_Name").val(),
                        Contractor_Person_Incharge_LIN: $("#txtContractor_Person_Incharge_LIN").val(),
                        Contractor_Person_Incharge_PAN: $("#txtContractor_Person_Incharge_PAN").val(),
                        Contractor_Person_Incharge_EmailID: $("#txtContractor_Person_Incharge_EmailID").val(),
                        Contractor_Person_Incharge_MobileNo: $("#txtContractor_Person_Incharge_MobileNo").val(),
                        Client_Nature_of_business: $("#txtClient_Nature_of_business").val(),
                        PE_Address: $("#txtPE_Address").val(),
                        Contractor_Licensing_Officer_Designation: $("#txtContractor_Licensing_Officer_Designation").val(),
                        Licencing_officer_Head_Quarter: $("#txtLicencing_officer_Head_Quarter").val(),
                        Nature_ofwelfare_amenities_provided: $("#txtNature_ofwelfare_amenities_provided").val(),
                        Statutory_statute: $("#txtStatutory_statute").val(),
                        Address: $("#txtBranch_Address").val(),
                        NumberOfEmp: $("#txtNumber_Employee").val(),
                        ContractFrom: $("#txtContractForm").val(),
                        ContractTo: $("#txtContractTo").val()

                    }

                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerLocationCreation.aspx/SavePrincipleEmployerLocationDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                //if(id == "0")
                                //    alert("Principle Employer Location Saved Successfully.");
                                //else
                                //    alert("Principle Employer Location Updated Successfully.");


                                $("#divErrorsPELocation").hide();
                                $("#divMsgPELocation").show();
                                $('#ModalPrincipleEmployerLocation').scrollTop(0);
                                if (id == "0")
                                    $("#spanMsgPELocation").html("Principle Employer Location Saved Successfully.");
                                else
                                    $("#spanMsgPELocation").html("Principle Employer Location Updated Successfully.");

                                setTimeout(function () {

                                    $("#ModalPrincipleEmployerLocation").modal("hide");
                                    ClrPELocation();

                                }, 3000);




                                /*$("#lblPELID").text("0");
                                $("#txtBranch").val("");
                                $("#txtbarchNatureOfBusiness").val("");
                                $("#ddlWeekoff").val("");


                                $('.multipleSelect').val("");
                                $('.fstChoiceRemove').click();
                                $('#ddlWeekoff').fastselect();

                                $("#chkMineApplicability").prop("checked", false);*/

                                //$("#ModalPrincipleEmployerLocation").modal("hide");

                            }
                            else {
                            }
                            BindPrincipleEmployerLocationTable();
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
            });

           

            $(document).on("click", "#tblPrincipleEmployerLocation tbody tr .Update", function (e) {
                $("#divErrorsPELocation").hide();
                $("#divMsgPELocation").hide();
                //ClrPELocation();
                $('#hiddmode').val('E').trigger('change');
                var item = $("#tblPrincipleEmployerLocation").data("kendoGrid").dataItem($(this).closest("tr"));
              
                $("#lblPELID").text(item.PLID);
                var DetailsObj = {
                    PLID: item.PLID
                }
                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerLocationCreation.aspx/GetPrincipleEmployerLocationDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var LocationDetails = JSON.parse(data.d);
                        debugger;
                        if (LocationDetails.length > 0) {
                            $("#ModalPrincipleEmployerLocation").modal("show");
                            $("#hybrid1").addClass("hidden");
                            $("#hybrid2").removeClass("hidden");
                           
                            $('#principlestatetext').css('pointer-events', 'none');
                            $('#principlelocationtext').css('pointer-events', 'none');
                            $('#txtBranch1').css('pointer-events', 'none');
                           // $("#ddlPrincipleEmployerState").css("display", "none");
                           // $("#ddlPrincipleEmployerLocation").css("display", "none");
                            
                            
                            $("#principlestatetext").val(LocationDetails[0].State);
                            $("#principlelocationtext").val(LocationDetails[0].Location);
                            
                            
                            $("#lblPELID").text(LocationDetails[0].PLID);
                            $("#txtBranch1").val(LocationDetails[0].Branch);
                           // $("#txtbarchNatureOfBusiness").val(LocationDetails[0].NatureOfBusiness);

                            $("#txtPE_LIN").val(LocationDetails[0].PE_LIN);
                            $("#txtPE_AuthorisedPerson_EmailID").val(LocationDetails[0].PE_AuthorisedPerson_EmailID);
                            $("#txtPE_Company_PhoneNo").val(LocationDetails[0].PE_Company_PhoneNo);
                            $("#txtClient_LINNo").val(LocationDetails[0].Client_LINNo);
                            $("#txtClient_CompanyEmailID").val(LocationDetails[0].Client_CompanyEmailID);
                            $("#txtClient_Company_Phone_No").val(LocationDetails[0].Client_Company_Phone_No);
                            $("#txtContract_Licence_No").val(LocationDetails[0].Contract_Licence_No);
                            
                            const StartdateTime = LocationDetails[0].Licence_Valid_From_date;
                            if (StartdateTime===null) {
                            }
                            else if (StartdateTime != "01/01/0001") {
                                const parts = StartdateTime.split(/[// :]/);
                                const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                $("#txtLicenceValidFromdate").datepicker("setDate", FromDate);
                            }
                            const EnddateTime = LocationDetails[0].Licence_Valid_To_date;
                            if (EnddateTime === null) {
                            }
                            else if (EnddateTime != "01/01/0001") {
                                const parts1 = EnddateTime.split(/[// :]/);
                                const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                                $("#txtLicenceValidTodate").datepicker("setDate", ToDate);
                            }

                            $("#txtContractor_Person_Incharge_Name").val(LocationDetails[0].Contractor_Person_Incharge_Name);
                            $("#txtContractor_Person_Incharge_LIN").val(LocationDetails[0].Contractor_Person_Incharge_LIN);
                            $("#txtContractor_Person_Incharge_PAN").val(LocationDetails[0].Contractor_Person_Incharge_PAN);
                            $("#txtContractor_Person_Incharge_EmailID").val(LocationDetails[0].Contractor_Person_Incharge_EmailID);
                            $("#txtContractor_Person_Incharge_MobileNo").val(LocationDetails[0].Contractor_Person_Incharge_MobileNo);
                            $("#txtClient_Nature_of_business").val(LocationDetails[0].Client_Nature_of_business);
                            $("#txtPE_Address").val(LocationDetails[0].PE_Address);
                            $("#txtContractor_Licensing_Officer_Designation").val(LocationDetails[0].Contractor_Licensing_Officer_Designation);
                            $("#txtLicencing_officer_Head_Quarter").val(LocationDetails[0].Licencing_officer_Head_Quarter);
                            $("#txtNature_ofwelfare_amenities_provided").val(LocationDetails[0].Nature_ofwelfare_amenities_provided);
                            $("#txtStatutory_statute").val(LocationDetails[0].Statutory_statute);
                          
                            $('#hiddmode').val('E').trigger('change');
                            $("#ddlWeekoff").val("");

                            $('.multipleSelect').val("");
                          //  $('.fstChoiceRemove').click();
                            $('#ddlWeekoff').fastselect();

                            var array = LocationDetails[0].WeekOff;
                            for (i in array) {
                                if (array[i] != "") {
                                    array[i] = array[i].toUpperCase();
                                    $('.multipleSelect').data('fastselect').setSelectedOption($('.multipleSelect option[value=' + array[i] + ' ]').get(0));
                                    $("#ddlWeekoff option[value='" + array[i] + "']").prop("selected", true);
                                }
                            }
                            //var values = "Mon,Tue,Wed";
                            //$.each(values.split(","), function (i, e) {
                            //    $("#ddlWeekoff option[value='" + e + "']").prop("selected", true);
                            //});

                            //$('.multipleSelect').val("Mon").change();
                            //$('.multipleSelect').val("Tue").change();
                            //$('.multipleSelect').fastselect();

                            
                            if (LocationDetails[0].Mines != "False")
                                $("#chkMineApplicability").prop("checked", true);
                            else
                                $("#chkMineApplicability").prop("checked", false);
                            debugger;
                            $("#txtBranch_Address").val(LocationDetails[0].Address);
                            $("#txtNumber_Employee").val(LocationDetails[0].NumberOfEmp);
                            $("#txtContractForm").val(LocationDetails[0].ContractFrom);
                            $("#txtContractTo").val(LocationDetails[0].ContractTo);


                            const ContractFromStartdateTime = LocationDetails[0].ContractFrom;
                            if (ContractFromStartdateTime === null) {
                            }
                            else if (ContractFromStartdateTime != "01/01/0001") {
                                const parts = ContractFromStartdateTime.split(/[// :]/);
                                const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                $("#txtContractForm").datepicker("setDate", FromDate);
                            }
                            const ContractToEnddateTime = LocationDetails[0].ContractTo;
                            if (ContractToEnddateTime === null) {
                            }
                            else if (ContractToEnddateTime != "01/01/0001") {
                                const parts1 = ContractToEnddateTime.split(/[// :]/);
                                const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                                $("#txtContractTo").datepicker("setDate", ToDate);
                            }
                            //if (LocationDetails[0].Status == "A")
                            //    $("#chklocationStatus").prop("checked", true);
                            //else
                            //    $("#chklocationStatus").prop("checked", false);
                         
                          
                        }
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });


            });

            $(document).on("click", "#tblPrincipleEmployerLocation tbody tr .Delete", function (e) {
                debugger;

                var item = $("#tblPrincipleEmployerLocation").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblPELID").text(item.PLID);
                //alert("pelid is " + item.PLID);
                var DetailsObj = {
                    PLID: item.PLID
                }
                var res = confirm("Are you sure you want to delete this Principle Employer Location?");
                if (res == true) {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerLocationCreation.aspx/DeletePrincipleEmployerLocation",
                        //data: JSON.stringify({ DetailsObj }),
                        data: '{plid:' + item.PLID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var result = JSON.parse(data.d);

                            if (result == true) {
                                alert("Principle Employer Location Deleted Successfully");
                                BindPrincipleEmployerLocationTable();
                            }
                            else
                                alert("Server error occured. Please try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }


            });

          


            $('#txtSearchLocation').keyup(function () {

                if ($(this).val()) {
                    $('#btnClearLocation').show();
                }
                else {
                    $('#btnClearLocation').hide();
                }
            });

            
            $("#btnSearchLocation").click(function (e) {
                e.preventDefault();

                var filter = [];
                $x = $("#txtSearchLocation").val();
                if ($x) {
                    var gridview = $("#tblPrincipleEmployerLocation").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "Location", operator: "contains", value: $x },
                              { field: "Branch", operator: "contains", value: $x },
                              { field: "State", operator: "contains", value: $x },
                              { field: "Status", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#tblPrincipleEmployerLocation").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindGrid();
                }
                // return false;
            });


            $("#btnClearLocation").click(function (e) {
                e.preventDefault();
                $("#txtSearchLocation").val('');

                var filter = [];
                $x = $("#txtSearchLocation").val();
                if ($x) {
                    var gridview = $("#tblPrincipleEmployerLocation").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "Location", operator: "contains", value: $x },
                              { field: "Branch", operator: "contains", value: $x },
                              { field: "State", operator: "contains", value: $x },
                              { field: "Status", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#tblPrincipleEmployerLocation").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindGrid();
                }

            });




 // $('#ddlWeekoff').fastselect();
           


        });



      

        $(document).click(function (e) {
           
            if (!$(e.target).hasClass("fstControls")  && $(e.target).parents('.fstElement').length == 0) {
                $(".fstResults").hide();
            }
            else {
                $(".fstResults").show();
            }
        });

        //$(document).click(function (e) {
        //    if ($(e.target).parents("#weekoffdiv").length === 0) {
        //        $(".dropdown").hide();
        //    }
        //});


    

        //$(".fstResults").click(function (e) {
        //    e.stopPropagation();
        //});

        //$(document).click(function () {
        //    $(".fstResults").hide();
        //});

       
        
        function DownloadPedetails() {
        
            debugger;
          //var  PEID=$("#ddlPrincipleEmployer").val();
          var PEIDObj = {
              PEID: $("#ddlPrincipleEmployer").val()
          }
            
          //alert(PEIDObj)
          //var cid = "32";
          //var pid = "40";

          //    $.ajax({
          //        type: "POST",
          //        url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindPrincipleEmployeeReport",
          //        data: '{"clientid":"' + cid + '","principleEmpId":"' + pid + '"}',
         

            $.ajax({
                type: "POST",
                // url: "/CLRA/PrincipleEmployerLocationCreation.aspx/DownloadPEDetails?PEID="+PEID,
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/DownloadPEDetails",
                data: JSON.stringify({ PEIDObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                  
                },
                failure: function (data) {
                    alert(data);
                }
            });

        }




        function OpenLocationUploadPopup() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var clientId = queryString;
            $('#divUploadLocation').show();
            var myWindowAdv = $("#divUploadLocation");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '50%',
                title: "Upload Location",
                visible: false,
                actions: ["Close"],
                //open: onOpen, 
                close: onCloseLocationPopup
            });

            $('#iframeUploadLocation').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeUploadLocation').attr('src', "/CLRA/LocationUpload.aspx?clientid=" + clientId + "&employerId=" + $("#ddlPrincipleEmployer").val() + "&employerState=" + $("#ddlPrincipleEmployerState").val() + "&employerLocation=" + $("#ddlPrincipleEmployerLocation").val());
            //$('#iframeUploadContractors').attr({ 'src': "/CLRA/ContractorMasterUpload.aspx?clientid=" + clientId, "scrolling": "no", })

            var dialog = $("#divUploadLocation").data("kendoWindow");
            dialog.title("Upload Locations");

            return false;
        }


        function onCloseLocationPopup() {
            BindPrincipleEmployerLocationTable();

        }

        

        function ClrPELocation() {
             $('#ddlPrincipleEmployerState').val("-1").trigger('change.select2');
             $('#ddlPrincipleEmployerLocation').val("-1").trigger('change.select2');

           // $('#ddlPrincipleEmployerState').val(1).trigger('change.select2');
           // $("#").val("");
            $("#lblPELID").text("0");
            $("#txtBranch").val("");
            $("#txtbarchNatureOfBusiness").val("");
            $("#txtBranch_Address").val("");
            $("#txtContractForm").val("");
            $("#txtContractTo").val("");
            $("#txtNumber_Employee").val("");
            $("#txtPE_LIN").val("");
            $("#txtPE_AuthorisedPerson_EmailID").val("");
            $("#txtPE_Company_PhoneNo").val("");
            $("#txtClient_LINNo").val("");
            $("#txtClient_CompanyEmailID").val("");
            $("#txtClient_Company_Phone_No").val("");
            $("#txtContract_Licence_No").val("");
            $("#txtLicenceValidFromdate").val("");
            $("#txtLicenceValidTodate").val("");
            $("#txtContractor_Person_Incharge_Name").val("");
            $("#txtContractor_Person_Incharge_LIN").val("");
            $("#txtContractor_Person_Incharge_PAN").val("");
            $("#txtContractor_Person_Incharge_EmailID").val("");
            $("#txtContractor_Person_Incharge_MobileNo").val("");
            $("#txtClient_Nature_of_business").val("");
            $("#txtPE_Address").val("");
            $("#txtContractor_Licensing_Officer_Designation").val("");
            $("#txtLicencing_officer_Head_Quarter").val("");
            $("#txtNature_ofwelfare_amenities_provided").val("");
            $("#txtStatutory_statute").val("");

            $("#ddlWeekoff").val("");
            $('.multipleSelect').val("");
            $('.fstChoiceRemove').click();
            $('#ddlWeekoff').fastselect();

            $("#chkMineApplicability").prop("checked", false);
            $("#divErrorsPELocation").hide();

        }



        function BindClientList(mode) {
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindClientList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //beforeSend: function () {
                //},
                success: function (data) {
                    //var Customer = JSON.parse(data.d);
                    //if (Customer.length > 0)
                    //{
                    //    var controlId = "ddlClient";
                    //    if (mode == "R")//R stands for Report
                    //        controlId = "ddlClientReport"

                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select Client"));
                    //    $.each(Customer, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.ClientID).html(value.ClientName));
                    //    })
                    //    $("#" + controlId).select2();
                    //}



                    var controlId = "ddlClient";
                    // if (mode == "R")//R stands for Report
                    //    controlId = "ddlClientReport"

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Entity"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.ClientID).html(value.UniqueName));
                        })

                    }
                    $("#" + controlId).select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindPrincipleEmployerList(mode) {
            var controlId = "ddlPrincipleEmployer";
            var client = "ddlClient";

            //if (mode == "R") {
            //    controlId = "ddlPrincipleEmployerReport";
            //    client = "ddlClientReport";
            //}  

            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var ClientObj = {
                //ClientID: $("#" + client).val()
                ClientID : queryString
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindPrincipleEmployerList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //var Customer = JSON.parse(data.d);
                    //if (Customer.length > 0)
                    //{
                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));
                    //    $.each(Customer, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                    //    })
                    //    $("#" + controlId).select2();
                    //}

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }



        function BindStateList() {
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindStateList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0) {
                    //    $("#ddlPrincipleEmployerState").empty();
                    //    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#ddlPrincipleEmployerState").select2();
                    //}

                    $("#ddlPrincipleEmployerState").empty();
                    $("#ddlPrincipleEmployerState").val("");
                    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerState").select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindClientStateList() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var ClientObj = {
                ClientID: queryString
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindClientStateList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0) {
                    //    $("#ddlPrincipleEmployerState").empty();
                    //    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#ddlPrincipleEmployerState").select2();
                    //}

                    $("#ddlPrincipleEmployerState").empty();
                    $("#ddlPrincipleEmployerState").val("");
                    $("#ddlPrincipleEmployerState").append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlPrincipleEmployerState").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerState").select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });

        }





        function BindLocationList(stateid) {
            var StateObj = {
                Code: stateid
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindLocationList",
                data: JSON.stringify({ StateObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlPrincipleEmployerLocation").empty();
                    //$("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#ddlPrincipleEmployerLocation").select2();

                    $("#ddlPrincipleEmployerLocation").empty();
                    $("#ddlPrincipleEmployerLocation").val("");
                    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerLocation").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindClientLocationList(stateid) {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var id = queryString.replace(/[^\w\s]/gi, '')

            //var id = $("#ddlClient").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindClientLocationListNew",
                data: '{"clientid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlPrincipleEmployerLocation").empty();
                    //$("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#ddlPrincipleEmployerLocation").select2();

                    $("#ddlPrincipleEmployerLocation").empty();
                    $("#ddlPrincipleEmployerLocation").val("");
                    $("#ddlPrincipleEmployerLocation").append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#ddlPrincipleEmployerLocation").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlPrincipleEmployerLocation").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        

        function BindLocationListEmployeeMaster(stateid) {

            var id = $("#ddlClient").val();
            stateid = $("#ddlPrincipleEmployerStateMaster").val();

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindClientLocationList",
                //data: JSON.stringify({ StateObj }),
                data: '{"clientid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlPrincipleEmployerLocationMaster";

                    //var Location = JSON.parse(data.d);
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindLocationListContractorMaster(stateid) {
            var id = $("#ddlPrincipleemployerContractor").val();
            var stateid = $("#ddlStateContractor").val();

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindPELocationList",
                data: '{"peid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlLocationContractor";
                    //var Location = JSON.parse(data.d);
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }




        function BindBranchListEmployeeMaster(ForContractor) {
            var BranchObj = {
                Code: $("#ddlPrincipleEmployerStateMaster").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                LocationID: $("#ddlPrincipleEmployerLocationMaster").val(),
                ClientID: $("#ddlClient").val()
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindBranchList",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    debugger;

                    var controlId = "ddlPrincipleEmployerBranchMaster";
                    if (ForContractor == "Y")
                        controlId = "ddlBranchContractor";


                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        function BindBranchListContractor() {
            var BranchObj = {
                Code: $("#ddlStateContractor").val(),
                PEID: $("#ddlPrincipleemployerContractor").val(),
                LocationID: $("#ddlLocationContractor").val(),
                ClientID: $("#ddlClient").val()
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindBranchListContractor",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    debugger;

                    var controlId = "ddlBranchContractor";

                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

       

        function BindPrincipleEmployerLocationTable() {
            debugger;
            var DetailsObj = {
                PEID: $("#ddlPrincipleEmployer").val(),
               // State: $("#ddlPrincipleEmployerState").val(),
               // Location: $("#ddlPrincipleEmployerLocation").val()

            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/BindPrincipleEmployerLocationTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblPrincipleEmployerLocation").kendoGrid({
                        dataSource:
                            {
                                data: tblPrincipleEmployerLocationList,
                                serverPaging: false,

                                batch: true,
                                
                            },
                        //height: 300,
                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        pageable: {
                            refresh: false,
                            pageSize: 10,
                            pageSizes: true,
                            buttonCount: 3,
                        },
                        height: 475,
                        reorderable: true,
                        resizable: true,
                        multi: true,
                        selectable: true,

                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },

                        columns: [
                            //{ hidden: true, field: "PLID" },
                            //{
                            //    field: "State",
                            //    title: "State",
                            //    width: "25%",
                            //    attributes: { style: 'white-space: nowrap;' },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                             {
                                 field: "State",
                                 title: "State",
                                 width: "30%",
                                 attributes: { style: 'white-space: nowrap;' },
                                 filterable: {
                                     extra: false,
                                     multi: true,
                                     search: true,
                                     operators: {
                                         string: {
                                             eq: "Is equal to",
                                             neq: "Is not equal to",
                                             contains: "Contains"
                                         }
                                     }
                                 }
                             },
                           {
                                field: "Location",
                                title: "Location",
                                width: "30%",
                                attributes: { style: 'white-space: nowrap;' },
                                filterable: {
                                    extra: false,
                                    multi: true,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            {
                                field: "Branch",
                                title: "Branch",
                                width: "45%",
                                attributes: { style: 'white-space: nowrap;' },
                                filterable: {
                                    extra: false,
                                    multi: true,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                           
                            {
                                field: "Status",
                                title: "Status",
                                width: "15%",
                                attributes: { style: 'white-space: nowrap;' },
                                filterable: {
                                    extra: false,
                                    multi: true,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            //{ field: "NatureOfBusiness", title: "Nature Of Business", width: "30%", attributes: { style: 'white-space: nowrap;' } },
                            //{
                            //    field: "Mines",
                            //    title: "Mines Applicability",
                            //    width: "15%",
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            {
                                title: "Action", lock: true, width: "10%;",
                                command: [
                                    { name: "UpdatePrincipleEmployerLocation", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                    { name: "DeletePrincipleEmployerLocation", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                ]
                            }
                        ],

                        dataBound: function () {
                            var rows = this.items();
                            $(rows).each(function () {
                                var index = $(this).index() + 1
                                    + ($("#tblPrincipleEmployerLocation").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployerLocation").data("kendoGrid").dataSource.page() - 1));
                                var rowLabel = $(this).find(".row-number");
                                $(rowLabel).html(index);
                            });

                            $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                            $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                        }

                    });

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }



        
        function displayErrors(control, msg) {
            $("#" + control).append(msg);
            $("#" + control).append("</br>");
            $("#" + control).show();
        }


        function ValidatePrincipleEmployerLocation() {
            var status = true;
            $("#divErrorsPELocation").html("");
            $("#divErrorsPELocation").addClass('alert-danger');

          

            if ($('#txtBranch1').is(':visible') && $("#txtBranch1").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Branch");
                status = false;
            }
            else if($('#txtBranch').is(':visible') && $("#txtBranch").val() == "")
                
                {
                displayErrors("divErrorsPELocation", "Please Enter Branch");
                status = false;
            }
            else {

            }


            if ($("#hiddmode").val() == "A") {
                if ($("#ddlPrincipleEmployerState").val() == null || $("#ddlPrincipleEmployerState").val() == -1 || $("#ddlPrincipleEmployerState").val() == "") {
                    displayErrors("divErrorsPELocation", "Please Select State");
                    status = false;
                }
                if ($("#ddlPrincipleEmployerLocation").val() == null || $("#ddlPrincipleEmployerLocation").val() == -1 || $("#ddlPrincipleEmployerLocation").val() == "") {
                    displayErrors("divErrorsPELocation", "Please Select Location");
                    status = false;
                }
                if ($("#txtBranch").val() == "") {
                    displayErrors("divErrorsPELocation", "Please Enter Branch");
                    status = false;
                }

            }
            else {
                if ($("#principlestatetext").val() == null || $("#principlestatetext").val() == "") {
                    displayErrors("divErrorsPELocation", "Please Select State");
                    status = false;
                }

                if ($("#principlelocationtext").val() == null || $("#principlelocationtext").val() == "") {
                    displayErrors("divErrorsPELocation", "Please Select Location");
                    status = false;
                }
                if ($("#txtBranch1").val() == "") {
                    displayErrors("divErrorsPELocation", "Please Enter Branch");
                    status = false;
                }

            }


            if ($("#txtBranch_Address").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Branch Address");
                status = false;
            }
            if ($("#txtNumber_Employee").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Number of Employees");
                status = false;
            }
            if ($("#txtContractForm").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter Start Contract Date");
                status = false;
            }
            if ($("#txtContractTo").val() == "") {
                displayErrors("divErrorsPELocation", "Please Enter End Contract Date");
                status = false;
            }
            if ($("#ddlWeekoff").val() == "") {
                displayErrors("divErrorsPELocation", "Please select weekOff");
                status = false;
            }
            
            
            //if ($("#txtbarchNatureOfBusiness").val() == "") {
            //    displayErrors("divErrorsPELocation", "Please Enter Nature Of Business");
            //    status = false;
            //}

            if ($("#txtPE_AuthorisedPerson_EmailID").val() != "") {
                var val = $("#txtPE_AuthorisedPerson_EmailID").val()
                if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid PE Authorised Person Email ID");
                    status = false;
                }
            }
           
            if ($("#txtPE_Company_PhoneNo").val() != "") {
                var val = $("#txtPE_Company_PhoneNo").val()
                if (/^[789]\d{9}$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid PE Company Phone No");
                    status = false;
                }
            }
           
            if ($("#txtClient_CompanyEmailID").val() != "") {
                var val = $("#txtClient_CompanyEmailID").val()
                if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val)) {
                 } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Client Company Email ID");
                    status = false;
                }
            }
            

            if ($("#txtClient_Company_Phone_No").val() !="")
            {
             var val = $("#txtClient_Company_Phone_No").val()
             if (/^[789]\d{9}$/.test(val)) {
                 } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Client Company Phone No");
                    status = false;
                }
            }

          
            if ($("#txtContractor_Person_Incharge_PAN").val() != "") {
                var val = $("#txtContractor_Person_Incharge_PAN").val()
                if (/[A-Z]{5}[0-9]{4}[A-Z]{1}$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Contractor Person Incharge PAN");
                    status = false;
                }
            }
           
            if ($("#txtContractor_Person_Incharge_EmailID").val() != "") {
               var val = $("#txtContractor_Person_Incharge_EmailID").val()
                if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val)) {
                } else {
                    displayErrors("divErrorsPELocation", "Please Entervalid Contractor Person Incharge Email ID");
                    status = false;
                }
            }

           
            if ($("#txtContractor_Person_Incharge_MobileNo").val() != "") {
                var val = $("#txtContractor_Person_Incharge_MobileNo").val()
                if (/^[789]\d{9}$/.test(val)) {
                 } else {
                    displayErrors("divErrorsPELocation", "Please Enter valid Contractor Person Incharge Mobile No");
                    status = false;
                }
            }
            

            return status;
        }




        function process(date) {
            var parts = date.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

        function downloadSample() {
            //alert("hi");
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerLocationCreation.aspx/DownloadSampleFile",
                //data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    alert("success");
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function chkDecimal(controlId) {

            //var val = $("#txtControctalue").val();
            var val = $("#" + controlId).val();
            if (val != "") {
                //var checkForPercentage = /^\d{1,2}\.\d{1,2}$|^\d{1,3}$/; 
                /*var checkForPercentage = /[^0-9.]/;
                if (!checkForPercentage.test(val)) {
                    alert("Enter numeric values only.");
                    $("#" + controlId).val("");
                }*/

                var rx = /^\d+(?:\.\d{1,2})?$/
                if (!rx.test(val)) {
                    alert("Enter numeric values only.");
                    $("#" + controlId).val("");
                }

            }

        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }



    </script>
  
<script type="text/javascript">





    $(document).ready(function () {
             
       
        //$(".multipleSelect").fastselect();
        
            $('#txtContractForm').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,

            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtContractTo').datepicker('setStartDate', null); //must uncomment
                }
                else {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtContractTo').datepicker('setStartDate', startDate);
                }

            });





        $('#txtContractTo').datepicker({
            pickTime: false,
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtContractForm').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractForm').datepicker('setEndDate', FromEndDate);
            }
        });

        $('#txtLicenceValidFromdate').datepicker({
            pickTime: false,
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtLicenceValidTodate').datepicker('setStartDate', null); //must uncomment
            }
            else {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#txtLicenceValidTodate').datepicker('setStartDate', startDate);
            }

        });

        $('#txtLicenceValidTodate').datepicker({
            pickTime: false,
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                //alert("its undefined");
                $('#txtLicenceValidFromdate').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtLicenceValidFromdate').datepicker('setEndDate', FromEndDate);
            }
        });


        $('#ModalPrincipleEmployerLocation').on('hidden.bs.modal', function () {
            $("#ddlPrincipleEmployerState").val("-1");
            ClrPELocation();
        })

     
    });

    function PELocationCreation() {
        var location = document.location.href;
        var queryString = location.split('clientid=')[1];
        var queryString = queryString.replace(/[^\w\s]/gi, '')
        if (queryString != "") {
            window.location.href = "../CLRA/PrincipleEmployerLocationCreation.aspx?clientid=" + queryString;
        } else
            alert('Oops.!!');

    }

    function PEMaster() {
        var location = document.location.href;
        var queryString = location.split('clientid=')[1];
        var queryString = queryString.replace(/[^\w\s]/gi, '')
        if (queryString != "") {
            window.location.href = "../CLRA/PrincipleEmployeeMasterCreation.aspx?clientid=" + queryString;
        } else
            alert('Oops.!!');

    }
    function ContractorDetails() {
        var location = document.location.href;
        var queryString = location.split('clientid=')[1];
        var queryString = queryString.replace(/[^\w\s]/gi, '')
        if (queryString != "") {
            window.location.href = "../CLRA/PrincipleEmployerContractorCreation.aspx?clientid=" + queryString;
        } else
            alert('Oops.!!');

    }

    function CLRAactivationpage() {

        var location = document.location.href;
        var queryString = location.split('clientid=')[1];
        var queryString = queryString.replace(/[^\w\s]/gi, '')
        if (queryString != "") {
            window.location.href = "../CLRA/CLRA_Assignment_Activation.aspx?clientid=" + queryString;
        } else
            alert('Oops.!!');
    }


    $("#btnUploadLocation").kendoTooltip({
        content: function (e) {
            return "Upload Bulk";
        }
    });

  
   

    $("#btnNewPELocation").kendoTooltip({
        content: function (e) {
            return "Add New Principle Employer Location";
        }
    });

</script>

  
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--</head>--%>

        <%--<form runat="server" id="formvalidation">--%>
            <%--  <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
    <!-- partial:index.partial.html -->
    
  <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li id="liPECreation" class="listItem pemt"> <%--class="active"--%>
                       <a href="../CLRA/PrincipleEmployer.aspx" onclick="<%--PECreation();--%>">Principle Employer</a>
                    </li>
                     <li id="liPELocationCreation" class="listItem pemt">
                         <a data-href="#" onclick="PELocationCreation();">Principle Employer Location</a>
                    
                    </li>
                   <li id="liPEMaster" class="listItem pemt">
                             <a data-href="#" onclick="PEMaster();">Principle Employee Master</a>                 
                    </li>
                    <li id="liContractorDetails" class="listItem pemt">
                             <a data-href="#" onclick="ContractorDetails();">Contractor Details</a>                 
                    </li>
                    <li id="liPEReport" class="listItem pemt">
                             <a href="#" onclick="CLRAactivationpage();">Compliance Assignment & Activation</a>    
                             <%--<input type="text" id="txtRohan" value="checkR" />    --%>         
                    </li>
                   
                    <%--<li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                        <input type="text" id="txtRohan" value="check" />
                    </li>--%>
                </ul>
            </header>
        </div>
    <!--PEN CONTENT     -->
        <!--PEN CONTENT     -->
    <div class="contentx">
        <!--content inner-->
        <div class="content__inner">
            <div class="container">
            </div>
            <div class="containerx overflow-hidden">
                <!--multisteps-form-->
                <!--form panels-->
                <div class="row">
                    <div class="col-12 col-lg-12 m-auto">
                       <%--   <form class="multisteps-form__form">
                          form--%>
                            <!--single form panel-->
                            
                            <!--single form panel-->
                            <div id="divPELocationCreation"  data-animation="scaleIn">
                                <h3 class=""></h3>

                                <div class="row alert alert-danger" id="divCheckPELocation">
                                    <div class="col-md-12">
                                        <div>
                                            <strong></strong><span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleEmployer">
                                            </select>
                                        </div>

                                        <!--
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divState">
                                            <label style="color: black;">Principle State</label>
                                            <select class="form-control" id="ddlPrincipleEmployerState">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divLocation">
                                            <label style="color: black;">Principle Location</label>
                                            <select class="form-control" id="ddlPrincipleEmployerLocation">
                                            </select>
                                        </div>
                                        -->   

                                        <div class="col-lg-9 col-md-9 col-sm-9 form-group hidden" id="divNewLocation" style="width:65%">
                                            <%--<button class="btn btn-primary" type="button" id="btnNewPELocation" title="Add New PE Location" style="margin-top: 20px;">Add New PE Location</button>--%>
                                            
                                            <input class="k-textbox" type="text" id="txtSearchLocation" style="width: 40%;    margin-top: 19px; height: 36px;margin-left:-13px;" placeholder="Type to search" autocomplete="off" />
                                            <button id="btnSearchLocation" class="btn btn-primary" style="height: 36px; font-weight: 400;margin-left:10px;    margin-top: 19px;">Search</button>
                                           <%--  <button id="btnClearLocation" class="btn btn-primary" style="height: 36px; font-weight: 400;margin-left:10px;margin-top: 20px;">Clear</button>--%>
                                           <button id="btnClearLocation" class="btn btn-primary" style="height: 36px; font-weight: 400;margin-left:10px;margin-top: 20px;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span><font size="2" style="zoom:1.1">Clear Filter</font></button>
                                             <button class="btn btn-primary" type="button" id="btnNewPELocation" style="margin-top: 20px; font-weight: 400; margin-left:9px;  height: 36px;"><span class="k-icon k-i-plus-outline"></span>Add New</button>

                                            <button class="btn btn-primary" type="button" id="btnUploadLocation" title="Upload" style="margin-top: 20px;margin-left:10px;    height: 36px;"><span class="k-icon k-i-upload"></span></button>
                                               <button class="btn btn-primary" style="margin-top:20px;height:36px;margin-left:10px;display:none;" type="button" id="btnDownloadEmployeedetails" title="Download" onclick="DownloadPedetails()"><span class="k-icon k-i-excel"></span></button>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div id="tblPrincipleEmployerLocation"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!--single form panel-->
                       
                        
                        <%--form ends--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <%-- </div>--%>  <%--13-10-2020--%>
    <!-- partial -->
    <script src="script.js"></script>


    <div class="modal  modal-fullscreen pem" id="ModalPrincipleEmployerLocation" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="overflow: auto; min-height: 500px; max-height: 500px; width: 929px; margin-left: -727px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-top:-11px;">Principle Employer Location</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        	<input type="hidden" id="hiddmode" value="A" />
                        <input type="hidden" id="weekmode" value="W" />
                        <div class="row" id="divMsgPELocation">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <strong></strong><span id="spanMsgPELocation"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row alert" id="divErrorsPELocation">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span></span>
                                </div>
                            </div>
                        </div>

                           <div class="row" id="hybrid1">
                               
                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                       <label style="color: black;">Principle State <span style="color: red;">*</span></label>
                        <select class="form-control" id="ddlPrincipleEmployerState">
                        </select>
                          </div>

                       <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                        		<label style="color: black;">Principle Location <span style="color: red;">*</span></label>
                            <select class="form-control" id="ddlPrincipleEmployerLocation">
                            </select>
                        </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                            	<label style="color: black;">Branch <span style="color: red;">*</span></label>
                                <input type="text" id="txtBranch" class="form-control" />
                                <label style="color: black;" id="lblPELID" class="hidden">0</label>
                               
                            </div>
                                   </div>
                             

                        <div class="row" id="hybrid2">
                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                         <label style="color: black;">Principle State</label>
                         <input type="text" id="principlestatetext" class="form-control" />
                        
                          </div>

                       <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                            <label style="color: black;">Principle Location</label>
                             <input type="text" id="principlelocationtext" class="form-control" />
                           
                        </div>

                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Branch <span style="color: red;">*</span></label>
                                <input type="text" id="txtBranch1" class="form-control" />
                                <label style="color: black;" id="lblPELID1" class="hidden">0</label>
                               
                            </div>

                               </div>

                         
                            <%--<div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Branch <span style="color: red;">*</span></label>
                                <input type="text" id="txtBranch" class="form-control" />
                                <label style="color: black;" id="lblPELID" class="hidden">0</label>
                               
                            </div>--%>
                              
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 form-group" >
                                <label style="color: black;">Branch Address<span style="color: red;">*</span></label>
                                <input type="text" id="txtBranch_Address" class="form-control" />
                                
                            </div>

                              <div class="checkbox" style="margin-top:26px;float:left;">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkMineApplicability" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mines Applicabilty</label>
                                </div>


                        </div>
                     <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 form-group" >
                                <label style="color: black;">Number of Employees<span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtNumber_Employee" maxlength="7" onkeypress="return isNumber(event)"/>
    
                            </div>

                           <div class="col-lg-4 col-md-4 col-sm-4 form-group" >
                                <label style="color: black;">Contract From<span style="color: red;">*</span></label>
                                <input type="text" id="txtContractForm" placeholder="From-Date" class="form-control" />
                               
                            </div>
                          
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contract To<span style="color: red;">*</span></label>
                                <input type="text" id="txtContractTo" placeholder="To-Date" class="form-control" />
                            </div>
                            
                        
                        </div>
                          
                    

                        <div class="row">
                          
                    

                         <div class="col-lg-12 col-md-12 col-sm-12 form-group" style="width:103%;">
                              
                             
                              <div class="col-lg-4 col-md-4 col-sm-4 form-group" id="weekoffdiv" style="width: 34%;margin-left:-16px">
                                <label style="color: black;">WeekOff<span style="color: red;">*</span></label>
                                <select multiple size="7" id="ddlWeekoff" class="multipleSelect">
                                    <option value="SUN">Sunday</option>
                                    <option value="MON">Monday</option>
                                    <option value="TUE">Tuesday</option>
                                    <option value="WED">Wednesday</option>
                                    <option value="THU">Thursday</option>
                                    <option value="FRI">Friday</option>
                                    <option value="SAT">Saturday</option>
                                </select>
                            </div>

                               <div class="col-lg-4 col-md-4 col-sm-4 form-group" >
                                <label style="color: black;">PE LIN </label>
                                <input type="text" id="txtPE_LIN" class="form-control" />
                                <label style="color: black;" id="lblPE_LIN" class="hidden">0</label>
                            </div>

                             <div class="col-lg-4 col-md-4 col-sm-4 form-group" style="margin-left:-4px;">
                                <label style="color: black;">PE Authorised Person Email ID </label>
                                <input type="text" id="txtPE_AuthorisedPerson_EmailID" class="form-control" />
                            </div>
                            </div>


                    </div>
                
                      <div class="row">
                          

                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">PE Company Phone No </label>
                                <input type="text" id="txtPE_Company_PhoneNo" maxlength="10" class="form-control" />
                                <label style="color: black;" id="lblPE_Company_PhoneNo" class="hidden">0</label>
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Client LIN No </label>
                                <input type="text" id="txtClient_LINNo" class="form-control" />
                            </div>

                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Client Company Email ID </label>
                                <input type="text" id="txtClient_CompanyEmailID" class="form-control" />
                                <label style="color: black;" id="lblClient_CompanyEmailID" class="hidden">0</label>
                            </div>
                        </div>
                        <div class="row">
                           
                          
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Client Company Phone No </label>
                                <input type="text" id="txtClient_Company_Phone_No" maxlength="10" class="form-control" />
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contract Licence No </label>
                                <input type="text" id="txtContract_Licence_No" class="form-control" />
                               
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Licence Valid From date </label>
                                <input type="text" id="txtLicenceValidFromdate" placeholder="From-Date" class="form-control"/>
                            </div>

                        </div>


                        <div class="row">
                           
                              <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Licence Valid To date </label>
                                <input type="text" id="txtLicenceValidTodate" placeholder="To-Date" class="form-control"/>
                               
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contractor Person Incharge Name </label>
                                <input type="text" id="txtContractor_Person_Incharge_Name" class="form-control" />
                            </div>

                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contractor Person Incharge LIN </label>
                                <input type="text" id="txtContractor_Person_Incharge_LIN" class="form-control" />
                               
                            </div>
                        </div>
                        <div class="row">
                          
                           
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contractor Person Incharge PAN </label>
                                <input type="text" id="txtContractor_Person_Incharge_PAN" maxlength="10" class="form-control" />
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contractor Person Incharge Email ID </label>
                                <input type="text" id="txtContractor_Person_Incharge_EmailID" class="form-control" />
                               
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contractor Person Incharge Mobile No </label>
                                <input type="text" id="txtContractor_Person_Incharge_MobileNo" maxlength="10" class="form-control" />
                            </div>
                        </div>
                      
                        <div class="row">
                           
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Client Nature of business </label>
                                <input type="text" id="txtClient_Nature_of_business" class="form-control" />
                               
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Licencing officer Head Quarter </label>
                                <input type="text" id="txtLicencing_officer_Head_Quarter" class="form-control" />
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Nature of welfare amenities provided </label>
                                <input type="text" id="txtNature_ofwelfare_amenities_provided" class="form-control" />
                               
                            </div>

                        </div>
                                     <div class="row">
                             
                             <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Statutory statute</label>
                                <input type="text" id="txtStatutory_statute" class="form-control" />
                            </div>
                        </div>        
                        </div>
                      

                        </div>
                        <div class="row" style="margin-top: 50px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnLocationSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="modal  modal-fullscreen pem" id="UploadEmployeeModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 400px; margin-left: -650px; width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                    
                          
                            <asp:UpdatePanel ID="updatepnl" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:FileUpload ID="ContractFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click" ValidationGroup="uploadUtilityValidationGroup" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <%--<a href="SampleFormat/ContractBulkUpload.xlsx"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <%--<a id="btnDownloadSample" onclick="downloadSample()"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <asp:LinkButton ID="lnkDownload" class="fa fa-file-excel-o" Text="Download Sample Format" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUploadExcel" />
                                    <%--<asp:AsyncPostBackTrigger ControlID="btnUploadExcel" EventName = "Click" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
        
  



    <div id="divUploadLocation">
        <iframe id="iframeUploadLocation" style="width: 100%; height: 300px; border: none;"></iframe>
    </div>


<%--</form>--%>


    </asp:Content>
<%--</html>--%>
    
    
    