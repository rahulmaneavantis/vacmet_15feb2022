﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.CLRA
{
    public partial class PrincipleEmployeeMaster : System.Web.UI.Page
    {
        protected static string AVACOM_RLCS_API_URL;
        protected static string avacomAPIUrl;
        protected static string TLConnectAPIUrl;
        protected static int customerID;
        protected static int BranchID;
        protected static string authKey;
        protected static string userProfileID_Encrypted;
        public int LoginUserID;
        public static List<int> branchList = new List<int>();
        public static List<int> locationList = new List<int>();
        public string ComplianceID_;
        public string ComplianceType_;
        public string ReturnRegisterChallanID_;
        public string MonthID_;
        public int Year_;
        public string ActID_;
        public string BranchID_;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);

                        avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                               
                            }
                        }
                    }

                    if (Request.QueryString["ComplianceType"] != null && Request.QueryString["ComplianceID"] != null && Request.QueryString["BranchID"] != null && Request.QueryString["Month"] != null && Request.QueryString["Year"] != null)
                    {
                        ComplianceID_ = Convert.ToString(Request.QueryString["ComplianceID"]);
                        ComplianceType_ = Convert.ToString(Request.QueryString["ComplianceType"]);
                        MonthID_ = Convert.ToString(Request.QueryString["Month"]);
                        Year_ = Convert.ToInt32(Request.QueryString["Year"]);
                        ActID_ = Convert.ToString(Request.QueryString["ActID"]);
                        BranchID_ = Convert.ToString(Request.QueryString["BranchID"]);
                      
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}