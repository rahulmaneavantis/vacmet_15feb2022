﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="PrincipleEmployerContractorCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployerContractorCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<!DOCTYPE html>--%>
<%--
<html xmlns="http://www.w3.org/1999/xhtml">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--<head runat="server">--%>
    <title></title>

    <link href="../NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/3.3.1/jquery.min.js"></script>
    <script src="../Newjs/3.3.7/bootstrap.min.js"></script>
    <script src="../Newjs/bootstrap-datepicker.min.js"></script> 
    <script src="../Newjs/jquery.validate.min.js"></script>
    <script src="../Newjs/jquery.validate.unobtrusive.js"></script>

    <link href="style.css" rel="stylesheet" />
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script src="../Newjs/fastselect.standalone.js"></script>
    <link href="../NewCSS/fastselect.min.css" rel="stylesheet" />


    <style type="text/css">
        /*.k-grid-content {
            min-height:  300px;
            max-height: 400px;
        }
        */

       .modal-header h4.modal-title{
           margin-top:-11px;
       }
       .modal-header .close {
         margin-top: -16px;

        }
       
        .modal-open {
    overflow: scroll;
}


        .btn{
            font-size:14px;
            font-weight:400;
        }

        

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .btn-primary[disabled] {
            background-color: #d7d7d7;
            border-color: #d7d7d7;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .form-control{
            height: 32px;
        }

        .k-multiselect-wrap .k-input {
            padding-top: 6px;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
        .col-md-12 {
            text-align: center;
        }
        .k-pager-sizes {
            float: left;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
             cursor:pointer;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .k-grid-header th.k-with-icon .k-link{
            text-align:center;
        }

        .inactiveclass {
        pointer-events:none;
        }


        .fstResultItem{
            text-align:left;
        }

        .checkbox label, .radio label{
            padding-left:0px;
        }
            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
                cursor:pointer;
            }

        .k-grid, .k-listview {
            margin-top: 10px;
        }

            .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            }

        .k-tooltip-content {
            width: max-content;
        }

        a:focus {
        outline:none !important;
        }

      .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected{
          box-shadow:none !important; 
      }

      .pem{
            text-align:left;

      }

      input[type=checkbox], input[type=radio]{
          margin:4px 3px 2px -3px !important;
      }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-grid input.k-checkbox + label.k-checkbox-label{
            margin-right:15px;
        }

        .k-calendar-container {
            background-color: white;
            width: 217px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: 0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }


        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left: 2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y: scroll! important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            text-align:center;
            font-weight: 400;
        }
        .k-button.k-button-icon .k-icon, .k-grid-filter .k-icon, .k-header .k-icon{
            margin-right:10px;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

      

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
            text-align:left;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height: 34px;
        }

        .k-multiselect-wrap, .k-floatwrap {
            height: 34px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 1%;
            margin-bottom: 1%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
            color: #515967;
            padding-top: 5px;
        }

        .k-filter-row th, .k-grid-header th[data-index='10'] {
            text-align: center;
        }


        .fstElement {
            font-size: 0.7em;
            height: 34px;
        }

        .fstToggleBtn {
            min-width: 16.5em;
        }

        .submitBtn {
            display: none;
        }

        .fstMultipleMode {
            display: block;
        }

            .fstMultipleMode .fstControls {
                width: 100%;
            }
      
              .k-checkbox-label:hover {
                  color: #1fd9e1;
              }

              .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
                  cursor: pointer;
                  background-color: transparent !important;
                  border-color: transparent;
                  color: #1fd9e1;
              }

              .btn {
                  font-weight: 400;
              }

              .k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
                  color: #2e2e2e;
                  background-color: #d8d6d6 !important;
              }

              .k-grid-content {
                  min-height: auto !important;
              }

              .k-multiselect-wrap .k-input {
                  padding-top: 6px;
              }

              .k-grid tbody .k-button {
                  min-width: 19px;
                  min-height: 25px;
                  background-color: transparent;
                  border: none;
                  margin-left: 0px;
                  margin-right: 0px;
                  padding-left: 2px;
                  padding-right: 2px;
                  border-radius: 25px;
              }

              .k-button.k-state-active, .k-button:active {
                  color: black;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li:hover {
                  color: white;
                  background-color: #1fd9e1;
                  border-top-left-radius: 10px;
                  border-top-right-radius: 10px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li {
                  margin-left: 5px !important;
                  margin-right: 5px !important;
              }

              .panel-heading .nav {
                  background-color: #f8f8f8;
                  border: none;
                  font-size: 11px;
                  margin: 0px 0px 0px 0;
                  border-radius: 10px;
              }

            .k-grid, .k-listview {
                  margin-top: 0px;
              }

                  .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                      -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                      box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                  }

              .k-tooltip-content {
                  width: max-content;
              }

              input[type=checkbox], input[type=radio] {
                  margin: 4px -2px 0;
                  margin-top: 1px\9;
                  line-height: normal;
              }

              .k-calendar-container {
                  background-color: white;
                  width: 217px;
              }

              .div.k-grid-footer, div.k-grid-header {
                  border-top-width: 1px;
                  margin-right: 0px;
                  margin-top: 0px;
              }


              .k-grid-footer-wrap, .k-grid-header-wrap {
                  position: relative;
                  width: 100%;
                  overflow: hidden !important;
                  border-style: solid;
                  border-width: 0 1px 0 0;
                  zoom: 1;
              }


              html {
                  color: #666666;
                  font-size: 15px;
                  font-weight: normal;
                  font-family: 'Roboto',sans-serif;
              }

              .k-checkbox-label, .k-radio-label {
                  display: inline;
              }

              .myKendoCustomClass {
                  z-index: 999 !important;
              }

              .k-header .k-grid-toolbar {
                  background: white;
                  float: left;
                  width: 100%;
              }

              .k-grid td {
                  line-height: 2.0em;
                  border-bottom-width: 1px;
                  background-color: white;
                  border-width: 0 1px 1px 0px;
              }

              .k-i-more-vertical:before {
                  content: "\e006";
              }

              .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                  background-color: #1fd9e1;
                  background-image: none;
                  background-color: white;
              }

              k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                  color: #000000;
                  border-color: #1fd9e1;
                  background-color: white;
              }

              /*#tblPrincipleEmployer .k-grid-toolbar {
            background: white;
        }*/

              .k-pager-wrap > .k-link > .k-icon {
                  margin-top: 0px;
                  margin-left: 2.7px;
                  color: inherit;
              }

              .toolbar {
                  float: left;
              }

              html .k-grid tr:hover {
                  background: white;
              }

              html .k-grid tr.k-alt:hover {
                  background: white;
              }


              .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                  margin-right: 0px;
                  margin-right: 0px;
                  margin-left: 0px;
                  margin-left: 0px;
              }

             

              .k-grid-header {
                  padding-right: 0px !important;
                  margin-right: 2px;
              }

              .k-filter-menu .k-button {
                  width: 27%;
              }

              .k-label input[type="checkbox"] {
                  margin: 0px 5px 0 !important;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  font-size: 15px;
                  background: #f8f8f8;
                  font-family: 'Roboto',sans-serif;
                  color: #2b2b2b;
                  font-weight: 400;
              }

              .k-primary {
                  border-color: #1fd9e1;
                  background-color: #1fd9e1;
              }

              .k-pager-wrap {
                  background-color: white;
                  color: #2b2b2b;
              }

              td.k-command-cell {
                  border-width: 0 0px 1px 0px;
                  text-align: center;
              }

              .k-grid-pager {
                  margin-top: -1px;
              }

              span.k-icon.k-i-calendar {
                  margin-top: 6px;
              }

              .col-md-2 {
                  width: 20%;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  border-width: 0px 0px 1px 0px;
                  background: #E9EAEA;
                  font-weight: bold;
                  margin-right: 18px;
                  font-family: 'Roboto',sans-serif;
                  font-size: 15px;
                  color: rgba(0, 0, 0, 0.5);
                  height: 20px;
                  vertical-align: middle;
              }

              .k-dropdown-wrap.k-state-default {
                  background-color: white;
                  height: 34px;
              }

              .k-multiselect-wrap, .k-floatwrap {
                  height: 34px;
              }

              .k-popup.k-calendar-container, .k-popup.k-list-container {
                  background-color: white;
              }

              .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
                  -webkit-box-shadow: inset 0 0 3px 1px white;
                  box-shadow: inset 0 0 3px 1px white;
              }

              label.k-label {
                  font-family: roboto,sans-serif !important;
                  color: #515967;
                  /* font-stretch: 100%; */
                  font-style: normal;
                  font-weight: 400;
                  min-width: max-content !important;
                  white-space: pre-wrap;
              }

              .k-multicheck-wrap .k-item {
                  line-height: 1.2em;
                  font-size: 14px;
                  margin-bottom: 5px;
              }

              label {
                  display: flex;
                  margin-bottom: 0px;
              }

              .k-state-default > .k-select {
                  border-color: #ceced2;
                  margin-top: 3px;
              }

              .k-grid-norecords {
                  width: 100%;
                  height: 100%;
                  text-align: center;
                  margin-top: 2%;
              }

              .k-grouping-header {
                  font-style: italic;
                  background-color: white;
              }

              .k-grid-toolbar {
                  background: white;
                  border: none;
              }

              .k-grid table {
                  width: 100.5%;
              }

              .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
                  color: #515967;
                  padding-top: 5px;
              }

              .k-filter-row th, .k-grid-header th[data-index='11'] {
                  text-align: center;
              }

              .k-i-filter-clear {
                  margin-top: -3px;
              }

              .k-button {
                  margin-top: 2.5px;
              }

              label.k-label:hover {
                  color: #1fd9e1;
              }

              .k-tooltip {
                  margin-top: 5px;
              }

              .k-column-menu > .k-menu {
                  background-color: white;
              }
       

        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

        .select2-container{
            width:100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>

    <script type="text/javascript">

        //var item = parent.document.getElementById("liPECreation");
        //item.classList.add("active");

        var EmployeeMasterListData = [];

        $(document).ready(function () {
            var activeclass = $('#liContractorDetails');
            activeclass.addClass('active');

            //  $("#divCheckPE").hide();
        //    $(".tabdata").hide();
        //    $("#divPECreation").show();
         //   $("#divMsg").hide();
            $("#divMsgPELocation").hide();
            $("#divMsgContractor").hide();
            $("#divErrorsPE").hide();
            $("#divErrorsPELocation").hide();
            $("#divErrorsContractor").hide();
            $("#divErrorsEmployees").hide();

            $("#divCheckPELocation").hide();
            $("#divCheckEmployees").hide();
            $("#divCheckContractor").hide();
            $("#divPrincipleEmployer").hide();
            BindPrincipleEmployerListOnEmployeeMaster("Y");
           // BindPrincipleEmployerListOnEmployeeMaster();
          // BindClientList();
            // BindPrincipleEmployerTable("");

            $('#txtContractForm').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,

            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtContractTo').datepicker('setStartDate', null); //must uncomment
                }
                else {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtContractTo').datepicker('setStartDate', startDate);
                }

            });





            $('#txtContractTo').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtContractForm').datepicker('setEndDate', null); //must uncomment
                }
                else {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtContractForm').datepicker('setEndDate', FromEndDate);
                }
            });

            $('#txtLicenceValidFromdate').datepicker({  
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,

            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtLicenceValidTodate').datepicker('setStartDate', null); //must uncomment
                }
                else {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtLicenceValidTodate').datepicker('setStartDate', startDate);
                }

            });

            $('#txtLicenceValidTodate').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtLicenceValidFromdate').datepicker('setEndDate', null); //must uncomment
                }
                else {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtLicenceValidFromdate').datepicker('setEndDate', FromEndDate);
                }
            });

            $('#txtContractorContractForm').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    $('#txtContractorContractTo').datepicker('setStartDate', null); //must uncomment
                }
                else {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtContractorContractTo').datepicker('setStartDate', startDate);
                }

            });

            $('#txtContractorContractTo').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    $('#txtContractorContractForm').datepicker('setEndDate', null); //must uncomment
                }
                else {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtContractorContractForm').datepicker('setEndDate', FromEndDate);
                }
            });

          
            $("#btnUploadContractors").on("click", function (e) {
                OpenContractorUploadPopup();
            });

           
            

            $("#btnAddContractor").on("click", function (e) {
                $("#divMsgContractor").hide();
                $("#ModalContractorMaster").modal("show");
                ClrContractorCreation();
              
           

            });

           

            $("#ddlPrincipleemployerContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlPrincipleemployerContractor").val() == "-1") {
                    $("#divStateContractor").addClass("hidden");
                    $("#divLocationContractor").addClass("hidden");
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    BindStateListContractorMaster();//must change

                    if ($("#divStateContractor").hasClass("hidden")) {
                        $("#divStateContractor").removeClass("hidden");
                    }

                    BindStateListContractorMaster();//must change

                    $("#divLocationContractor").addClass("hidden");
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }

            });

           

            $("#ddlStateContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlStateContractor").val() == "-1") {
                    $("#divLocationContractor").addClass("hidden");
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    $("#ddlStateContractor").select2();

                    if ($("#divLocationContractor").hasClass("hidden")) {
                        $("#divLocationContractor").removeClass("hidden");
                    }
                    BindLocationListContractorMaster($(this).val());

                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");

                }


            });

        

            $("#ddlLocationContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();

                if ($("#ddlLocationContractor").val() == "-1") {
                    $("#divBranchContractor").addClass("hidden");
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    $("#ddlLocationContractor").select2();
                    if ($("#divBranchContractor").hasClass("hidden")) {
                        $("#divBranchContractor").removeClass("hidden");
                    }
                    BindBranchListContractor();
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }

            });

           

            $("#ddlBranchContractor").on("change", function (e) {
                $("#divCheckContractor").html("");
                $("#divCheckContractor").hide();
                $("#divUploadContractors").hide();
                


                if ($("#ddlBranchContractor").val() == "-1") {
                    $("#divContractor").addClass("hidden");
                    $("#tblContractorMaster").addClass("hidden");
                }
                else {
                    $("#ddlBranchContractor").select2();
                    if ($("#divContractor").hasClass("hidden")) {
                        $("#divContractor").removeClass("hidden");
                    }
                    if ($("#tblContractorMaster").hasClass("hidden")) {
                        $("#tblContractorMaster").removeClass("hidden");
                    }
                    BindContractorTable();
                }

            });

        

            $("#btnContractorSave").click(function (e) {

                
                if (ValidatePrincipleEmployerContractor()) {

                    var id = $("#lblContractorId").text();
                    //alert(id);//must comment
                    $("#divErrorsContractor").hide();
                    var DetailsObj = {
                        PECID: $("#lblContractorId").text(),
                        PEC_PEID: $("#ddlPrincipleemployerContractor").val(),
                        ContractorName: $("#txtContractorName").val(),
                        Address: $("#txtContractorAddres").val(),
                        ContractFrom: $("#txtContractorContractForm").val(),
                        ContractTo: $("#txtContractorContractTo").val(),
                        NatureOfWork: $("#txtContractorNatureOfWork").val(),
                        NoOfEmployees: $("#txtContractorNoOfEmployee").val(),
                        Canteen: ($("#chkCanteenProvided").is(":checked") ? "Y" : "N"),
                        Restroom: ($("#chkRestRoomProvided").is(":checked") ? "Y" : "N"),
                        Creches: ($("#chkCrechesProvided").is(":checked") ? "Y" : "N"),
                        DrinkingWater: ($("#chkDrinkingWaterProvided").is(":checked") ? "Y" : "N"),
                        FirstAid: ($("#chkFirstAidProvided").is(":checked") ? "Y" : "N"),
                        State: $("#ddlStateContractor").val(),
                        Location: $("#ddlLocationContractor").val(),
                        Branch: $("#ddlBranchContractor").val()
                    }

                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerContractorCreation.aspx/SaveContractorDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                //if(id=="0")
                                //    alert("Contractor Saved Successfully.");
                                //else
                                //    alert("Contractor Updated Successfully.");


                                $("#divErrorsContractor").hide();
                                $("#divMsgContractor").show();

                                if (id == "0")
                                    $("#spanMsgContractor").html("Contractor Saved Successfully.");
                                else
                                    $("#spanMsgContractor").html("Contractor Updated Successfully.");

                                setTimeout(function () {

                                    $("#ModalContractorMaster").modal("hide");
                                    ClrContractorCreation();

                                }, 3000);

                                //$("#lblContractorId").text("0");
                                //$("#txtContractorName").val("");
                                //$("#txtContractorAddres").val("");
                                //$("#txtContractorContractForm").val("");
                                //$("#txtContractorContractTo").val("");
                                //$("#txtContractorNatureOfWork").val("");
                                //$("#txtContractorNoOfEmployee").val("");
                                //$("#chkCanteenProvided").prop('checked', false);
                                //$("#chkRestRoomProvided").prop('checked', false);
                                //$("#chkCrechesProvided").prop('checked', false);
                                //$("#chkDrinkingWaterProvided").prop('checked', false);
                                //$("#chkFirstAidProvided").prop('checked', false);

                                //$("#ModalContractorMaster").modal("hide");
                            }

                            //BindPrincipleEmployerTable($("#ddlClient").val());
                            BindContractorTable();
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
            });



            $(document).on("click", "#tblContractorMaster tbody tr .Update", function (e) {
             
                var item = $("#tblContractorMaster").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblContractorId").text(item.PECID);
              

               if( $('#divErrorsContractor').is(':visible')){
                   $("#divErrorsContractor").hide();
                }

               if ($('#divMsgContractor').is(':visible')) {
                   $("#divMsgContractor").hide();
               }


             
               // ClrContractorCreation();
                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployerContractorCreation.aspx/GetContractorDetails",
                    //data: JSON.stringify({ DetailsObj }),
                    data: '{pecid:' + item.PECID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var ContractorDetails = JSON.parse(data.d);
                        if (ContractorDetails != false) {
                            $('#divMsgContractor').hide();
                            $("#ModalContractorMaster").modal("show");
                           
                            $("#txtContractorName").val(ContractorDetails.ContractorName);
                            $("#txtContractorAddres").val(ContractorDetails.Address);

                            const StartdateTime = ContractorDetails.ContractFrom;
                            //const parts = StartdateTime.split(/[- :]/);
                            const parts = StartdateTime.split(/[// :]/);
                            const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                            //$("#txtContractorContractForm").val(FromDate);
                            $("#txtContractorContractForm").datepicker("setDate", FromDate);

                            const EnddateTime = ContractorDetails.ContractTo;
                            //const parts1 = EnddateTime.split(/[- :]/);
                            const parts1 = EnddateTime.split(/[// :]/);
                            const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                            //$("#txtContractorContractTo").val(ToDate);
                            $("#txtContractorContractTo").datepicker("setDate", ToDate);

                            $("#txtContractorNatureOfWork").val(ContractorDetails.NatureOfWork);
                            $("#txtContractorNoOfEmployee").val(ContractorDetails.NoOfEmployees);

                            if (ContractorDetails.Canteen == "Y")
                                $("#chkCanteenProvided").prop("checked", true);
                            else
                                $("#chkCanteenProvided").prop("checked", false);

                            if (ContractorDetails.Restroom == "Y")
                                $("#chkRestRoomProvided").prop("checked", true);
                            else
                                $("#chkRestRoomProvided").prop("checked", false);

                            if (ContractorDetails.Creches == "Y")
                                $("#chkCrechesProvided").prop("checked", true);
                            else
                                $("#chkCrechesProvided").prop("checked", false);

                            if (ContractorDetails.DrinkingWater == "Y")
                                $("#chkDrinkingWaterProvided").prop("checked", true);
                            else
                                $("#chkDrinkingWaterProvided").prop("checked", false);

                            if (ContractorDetails.FirstAid == "Y")
                                $("#chkFirstAidProvided").prop("checked", true);
                            else
                                $("#chkFirstAidProvided").prop("checked", false);
                          
                         
                            //OpenEditPrincipleEmployerPopup();
                        }
                        else
                            alert("Server error occured. Please try again");
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });


            $(document).on("click", "#tblContractorMaster tbody tr .Delete", function (e) {
                var item = $("#tblContractorMaster").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblContractorId").text(item.PECID);

                var res = confirm("Are you sure you want to delete this contractor?");
                if (res == true) {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployerContractorCreation.aspx/DeleteContractor",
                        //data: JSON.stringify({ DetailsObj }),
                        data: '{pecid:' + item.PECID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var result = JSON.parse(data.d);

                            if (result == true) {
                                alert("Contractor Deleted Successfully");
                                BindContractorTable();
                            }
                            else
                                alert("Some error occured. Try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }


            });

          



        });


        function PELocationCreation() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/PrincipleEmployerLocationCreation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');

        }

        function PEMaster() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/PrincipleEmployeeMasterCreation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');

        }
        function ContractorDetails() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/PrincipleEmployerContractorCreation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');

        }

        function CLRAactivationpage() {

            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/CLRA_Assignment_Activation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');
        }


        

        function OpenContractorUploadPopup() {
           
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '');
            var clientId = queryString;
            $('#divUploadContractors').show();
            var myWindowAdv = $("#divUploadContractors");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '50%',
                title: "Upload Contractors",
                visible: false,
                actions: ["Close"],
                //open: onOpen, 
                close: onClose
            });

            $('#iframeUploadContractors').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeUploadContractors').attr('src', "/CLRA/ContractorMasterUpload.aspx?clientid=" + clientId);
            //$('#iframeUploadContractors').attr({ 'src': "/CLRA/ContractorMasterUpload.aspx?clientid=" + clientId, "scrolling": "no", })

            var dialog = $("#divUploadContractors").data("kendoWindow");
            dialog.title("Upload Contractors");

            return false;
        }

       



        function onClose() {
           BindContractorTable();
        }

       


        function ClrContractorCreation() {

            $("#lblContractorId").text("0");
            $("#txtContractorName").val("");
            $("#txtContractorNatureOfWork").val("");
            $("#txtContractorContractForm").val("");
            $("#txtContractorContractTo").val("");
            $("#txtContractorNoOfEmployee").val("");
            $("#txtContractorAddres").val("");

            $("#chkCanteenProvided").prop("checked", false);
            $("#chkRestRoomProvided").prop("checked", false);
            $("#chkCrechesProvided").prop("checked", false);
            $("#chkDrinkingWaterProvided").prop("checked", false);
            $("#chkFirstAidProvided").prop("checked", false);
            $("#divErrorsContractor").hide();

            $('#txtContractorContractForm').datepicker('setDate', null);
            $('#txtContractorContractTo').datepicker('setDate', null);

            $('#txtContractorContractForm').datepicker('minDate', null);
            $('#txtContractorContractForm').datepicker('maxDate', null);

            $('#txtContractorContractTo').datepicker('minDate', null);
            $('#txtContractorContractTo').datepicker('maxDate', null);

        }

       


        function BindPrincipleEmployerListOnEmployeeMaster(ForContractor) {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var ClientObj = {
                ClientID: queryString
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerContractorCreation.aspx/BindPrincipleEmployerList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));
                    //$.each(Customer, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                    //})
                    //$("#" + controlId).select2();

                    var controlId = "ddlPrincipleEmployerMaster";
                    if (ForContractor == "Y") {
                        controlId = "ddlPrincipleemployerContractor";
                    }


                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                        })
                    }
                    $("#" + controlId).select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }






        function BindStateListContractorMaster() {
            var id = $("#ddlPrincipleemployerContractor").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerContractorCreation.aspx/BindPEStateList",
                data: '{peid:' + id + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlStateContractor";
                    //$("#" + controlId).empty();
                    //$("#" + controlId).val("");
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select State"));
                    //$("#" + controlId).select2();

                    //var State = JSON.parse(data.d);
                    //if (State.length > 0)
                    //{
                    //    $.each(State, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#" + controlId).select2();
                    //}

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }



        function BindLocationListContractorMaster(stateid) {
            var id = $("#ddlPrincipleemployerContractor").val();
            var stateid = $("#ddlStateContractor").val();

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerContractorCreation.aspx/BindPELocationList",
                data: '{"peid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlLocationContractor";
                    //var Location = JSON.parse(data.d);
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();


                },
                failure: function (data) {
                    alert(data);
                }
            });
        }





        function BindBranchListContractor() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var BranchObj = {
                Code: $("#ddlStateContractor").val(),
                PEID: $("#ddlPrincipleemployerContractor").val(),
                LocationID: $("#ddlLocationContractor").val(),
                ClientID: queryString
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerContractorCreation.aspx/BindBranchListContractor",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    debugger;

                    var controlId = "ddlBranchContractor";

                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

       


        function BindContractorTable() {
            debugger;
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var DetailsObj = {
                ClientID: queryString,
                PEID: $("#ddlPrincipleemployerContractor").val(),
                State: $("#ddlStateContractor").val(),
                Location: $("#ddlLocationContractor").val(),
                Branch: $("#ddlBranchContractor").val(),
            }


            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployerContractorCreation.aspx/BindContractorTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblContractorMaster").kendoGrid({
                        dataSource: {
                            data: tblPrincipleEmployerLocationList,
                            serverPaging: false,
                            batch: true,
                            pageSize: 10,
                        },
                        // height: 250,
                        //scrollable: true,
                        //width: 'auto',
                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        pageable: {
                            //numeric: true,
                            pageSize: 10,
                            pageSizes: ["All", 5, 10, 20],
                            buttonCount: 3
                        },
                        height: 475,
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        change: OnChangeContractorTable,
                        columns: [
                          //  { hidden: true, field: "PECID" },
                            //{ selectable: true, width: "5%" },                            
                            { field: "ContractorName", title: "Contractor Name", width: "25%", attributes: { style: 'white-space: nowrap;' },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            { hidden: true, field: "Address", title: "Address", width: "25%", attributes: { style: 'white-space: nowrap;' } ,
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            { field: "ContractFrom", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract From", width: "18%",
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            }, //format: "{0:dd-MMM-yyyy}"
                            { field: "ContractTo", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract To", width: "18%",
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            { field: "NatureOfWork", title: "Nature Of Work", width: "20%", attributes: { style: 'white-space: nowrap;' },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            { field: "NumberOfEmployees", title: "Number Of Employees", width: "26%",
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            { hidden: true, field: "CanteenProvided", title: "Canteen", width: "20%" },
                            { hidden: true, field: "RestroomProvided", title: "Restroom", width: "20%" },
                            { hidden: true, field: "CrechesProvided", title: "Creches", width: "20%" },
                            { hidden: true, field: "DrinkingWaterProvided", title: "Drinking Water", width: "20%" },
                            { hidden: true, field: "FirstAidProvided", title: "FirstAid", width: "20%" },
                            {
                                title: "Action", lock: true, width: "8%;",
                                command:
                                    [
                                           { name: "UpdateContractor", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                           { name: "DeleteContractor", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                    ]
                            }
                        ],
                        dataBound: function () {
                            var rows = this.items();
                            $(rows).each(function () {
                                var index = $(this).index() + 1
                                    + ($("#tblContractorMaster").data("kendoGrid").dataSource.pageSize() * ($("#tblContractorMaster").data("kendoGrid").dataSource.page() - 1));
                                var rowLabel = $(this).find(".row-number");
                                $(rowLabel).html(index);
                            });

                            $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                            $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                        }

                    });
                    $("#tblContractorMaster").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function OnChangeContractorTable(arg) {

            var rows = arg.sender.select();
            EmployeeMasterListData = [];
            rows.each(function (e) {
                var tblEmployeeRecord = $("#tblContractorMaster").data("kendoGrid");
                var dataItem = tblEmployeeRecord.dataItem(this);
                var SelecteddataItem = {
                    EmpID: dataItem.EmpID,
                    State: $("#ddlStateEmployeeMaster").val(),
                    Location: $("#ddlLocationEmployeeMaster").val(),
                    Branch: $("#ddlBranchEmployeeMaster").val(),
                    ContractFrom: $("#txtEmployeeContractForm").val(),
                    ContractTo: $("#txtEmployeeContractTo").val(),
                };
                debugger;
                EmployeeMasterListData.push(SelecteddataItem);
            })


        }


    

        function displayErrors(control, msg) {
            $("#" + control).append(msg);
            $("#" + control).append("</br>");
            $("#" + control).show();
        }

        function ValidatePrincipleEmployerContractor() {
            var status = true;
            $("#divErrorsContractor").html("");
            $("#divErrorsContractor").addClass('alert-danger');

            var fromDate = $("#txtContractorContractForm").val();
            var toDate = $("#txtContractorContractTo").val();


            if ($("#txtContractorName").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Contractor Name");
                status = false;
            }
            if ($("#txtContractorAddres").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Address");
                status = false;
            }
            if ($("#txtContractorContractForm").val() == "") {
                displayErrors("divErrorsContractor", "Please Select Contract From");
                status = false;
            }
            if ($("#txtContractorContractTo").val() == "") {
                displayErrors("divErrorsContractor", "Please Select Contract To");
                status = false;
            }
            if ($("#txtContractorNatureOfWork").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Nature Of Work");
                status = false;
            }
            //else if (Date.parse(toDate) < Date.parse(fromDate))
            //{
            //    alert("'Contract To Date' Should Be Greater than 'Contract From Date'");
            //    status = false;
            //}

            if ($("#txtContractorNoOfEmployee").val() == "") {
                displayErrors("divErrorsContractor", "Please Enter Number Of Employee");
                status = false;
            }

            return status;
        }





    </script>
  
<script type="text/javascript">





    $(document).ready(function () {
        $('#ModalContractorMaster').on('hidden.bs.modal', function () {
            ClrContractorCreation();
        })

        //$("#search_code").live('change', function () {
        //    alert(this.value)
        //});
        //$("#divCheckPE").hide();
        //$(".tabdata").hide();
        //$("#divPECreation").show();
        //$("#divMsg").hide();
        //$("#divMsgPELocation").hide();
        $("#divMsgContractor").hide();
        //$("#divErrorsPE").hide();
        //$("#divErrorsPELocation").hide();
        //$("#divErrorsContractor").hide();
        //$("#divErrorsEmployees").hide();
       
        //$("#divCheckPELocation").hide();
        //$("#divCheckEmployees").hide();
        //$("#divCheckContractor").hide();



     //  var item = parent.document.getElementById("liPECreation");
      //  item.classList.add("active");

        var sd = new Date(), ed = new Date();

      



        $('#txtContractorContractForm').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                $('#txtContractorContractTo').datepicker('setStartDate', null); //must uncomment
            }
            else {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractorContractTo').datepicker('setStartDate', startDate);
            }

        });

        $('#txtContractorContractTo').datepicker({
            pickTime: false,
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('changeDate', function (selected) {
            if (selected.date === undefined) {
                $('#txtContractorContractForm').datepicker('setEndDate', null); //must uncomment
            }
            else {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtContractorContractForm').datepicker('setEndDate', FromEndDate);
            }
        });


    });

    

   

    $("#btnUploadContractors").kendoTooltip({
        content: function (e) {
            return "Upload Bulk";
        }
    });

    $("#btnAddContractor").kendoTooltip({
        content: function (e) {
            return "Add New Contractor";
        }
    });

   

</script>

   

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--</head>--%>
    <div>
        <%--<form runat="server" id="formvalidation">--%>
            <%--  <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
    <!-- partial:index.partial.html -->
    
  <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li id="liPECreation" class="listItem pemt"> <%--class="active"--%>
                       <a href="../CLRA/PrincipleEmployer.aspx" onclick="<%--PECreation();--%>">Principle Employer</a>
                    </li>
                   <li id="liPELocationCreation" class="listItem pemt">
                         <a data-href="#" onclick="PELocationCreation();">Principle Employer Location</a>
                    
                    </li>
                   <li id="liPEMaster" class="listItem pemt">
                             <a data-href="#" onclick="PEMaster();">Principle Employee Master</a>                 
                    </li>
                    <li id="liContractorDetails" class="listItem pemt">
                             <a data-href="#" onclick="ContractorDetails();">Contractor Details</a>                 
                    </li>
                    <li id="liPEReport" class="listItem pemt">
                            <a href="#" onclick="CLRAactivationpage();">Compliance Assignment & Activation</a>    
                             <%--<input type="text" id="txtRohan" value="checkR" />    --%>         
                    </li>
                   
                    <%--<li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                        <input type="text" id="txtRohan" value="check" />
                    </li>--%>
                </ul>
            </header>
        </div>
    <div class="contentx">
        <!--content inner-->
        <div class="content__inner">
            <div class="container">
            </div>
            <div class="containerx overflow-hidden">
                <!--multisteps-form-->
                <!--form panels-->
                <div class="row">
                    <div class="col-12 col-lg-12 m-auto">
                       <%--   <form class="multisteps-form__form">
                          form--%>
                            <!--single form panel-->
                          
                            <!--single form panel-->
                            <div id="divContractorDetails" class="tabdata" data-animation="scaleIn">
                                <h3 class=""></h3>

                                <div class="row alert alert-danger" id="divCheckContractor">
                                    <div class="col-md-12">
                                        <div>
                                            <strong></strong><span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleemployerContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divStateContractor">
                                            <label style="color: black;">State</label>
                                            <select class="form-control" id="ddlStateContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divLocationContractor">
                                            <label style="color: black;">Location</label>
                                            <select class="form-control" id="ddlLocationContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divBranchContractor">
                                            <label style="color: black;">Branch</label>
                                            <select class="form-control" id="ddlBranchContractor">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-2 form-group hidden" id="divContractor">
                                            <%--<button class="btn btn-primary" type="button" id="btnAddContractor" title="Migrate">Add Contractor</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnAddContractor" style="font-weight: 400;"><span class="k-icon k-i-plus-outline"></span>Add New</button>
                                            <%-- <button class="btn btn-primary" type="button" id="btnUploadContractors" title="Upload">Upload</button>--%>
                                            <button class="btn btn-primary" type="button" id="btnUploadContractors" title="Upload"><span class="k-icon k-i-upload"></span></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div id="tblContractorMaster"></div>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                         
                        <%--form ends--%>
                    </div>
                </div>
            </div>
        </div>	
    </div>
   <%-- </div>--%>  <%--13-10-2020--%>
    <!-- partial -->
   
   
    <div class="modal  modal-fullscreen pem" id="UploadEmployeeModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 400px; margin-left: -650px; width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                    
                          
                            <asp:UpdatePanel ID="updatepnl" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:FileUpload ID="ContractFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click" ValidationGroup="uploadUtilityValidationGroup" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <%--<a href="SampleFormat/ContractBulkUpload.xlsx"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <%--<a id="btnDownloadSample" onclick="downloadSample()"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <asp:LinkButton ID="lnkDownload" class="fa fa-file-excel-o" Text="Download Sample Format" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUploadExcel" />
                                    <%--<asp:AsyncPostBackTrigger ControlID="btnUploadExcel" EventName = "Click" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="modal  modal-fullscreen  pem" id="ModalContractorMaster" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 375px; width: 834px; margin-left: -677px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contractor Details</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row" id="divMsgContractor">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <strong></strong><span id="spanMsgContractor"></span>
                                </div>
                            </div>
                        </div>


                        <div class="row alert" id="divErrorsContractor">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contractor Name <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorName" maxlength="50" />
                                <label style="color: black;" id="lblContractorId" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Address <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorAddres" maxlength="250" />
                            </div>
                        </div>
                        <div class="row">
                            <%--<div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractorContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractorContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From <span style="color: red;">*</span></label>
                                <input  type="text" class="form-control" placeholder="From-Date" id="txtContractorContractForm"/>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="To-Date" id="txtContractorContractTo" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Nature Of Work <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorNatureOfWork" maxlength="100" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">No Of Employee <span style="color: red;">*</span></label>
                                <input class="form-control" type="text" id="txtContractorNoOfEmployee" maxlength="7" onkeypress="return isNumber(event)" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group" style="text-align:left">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkCanteenProvided" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;Canteen Provided</label>
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 form-group" style="text-align:left">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkRestRoomProvided" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;Rest Room Provided</label>
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2 form-group" style="text-align:left">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkCrechesProvided" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;Creches Provided</label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group" style="text-align:left">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkDrinkingWaterProvided" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;Drinking Water Provided</label>
                                </div>
                            </div>
                               <div class="col-lg-2 col-md-2 col-sm-2 form-group" style="text-align:left">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkFirstAidProvided" value="1" />&nbsp;&nbsp;&nbsp;&nbsp;First Aid Provided</label>
                                </div>
                            </div>
                        </div>
                      
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group" style="text-align:center">
                                <button id="btnContractorSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="divUploadContractors">
        <iframe id="iframeUploadContractors" style="width: 100%; height: 300px; border: none;"></iframe>
    </div>

    

<%--</form>--%>

        </div>
    </asp:Content>
<%--</html>--%>
    
    
    