﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.CLRA
{
    public partial class ContractorMasterUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            spanErrors.Visible = false;

            if (Request.QueryString["clientid"] != null)
            {
                hdClientId.Value = Request.QueryString["clientid"];
            }
        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";
            if (ContractFileUpload.HasFile && Path.GetExtension(ContractFileUpload.FileName).ToLower() == ".xlsx")
            {
                try
                {
                    if (!Directory.Exists(Server.MapPath("~/CLRA/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/Uploaded/"));
                    }

                    string filename = Path.GetFileName(ContractFileUpload.FileName);
                    ContractFileUpload.SaveAs(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "PrincipleContractorMasterBulkUp");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'PrincipleContractorMasterBulkUp' ";
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["PrincipleContractorMasterBulkUp"];

                if (xlWorksheet != null)
                {
                    //List<string> errorMessage = new List<string>();
                    List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();

                    int xlrow2 = xlWorksheet.Dimension.End.Row;



                    if (xlrow2 > 1)
                    {
                        string ErrorFileName = "ErrorFile_ContractorUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                        hdFileName.Value = ErrorFileName;

                        #region Validations
                        using (ComplianceDBEntities db = new ComplianceDBEntities())
                        {


                            ErrorMessage objError = null;
                            string clientid = hdClientId.Value;
                            for (int i = 2; i <= xlrow2; i++)
                            {


                                string contractFrom = xlWorksheet.Cells[i, 5].Text.Trim();
                                string contractTo = xlWorksheet.Cells[i, 6].Text.Trim();

                                string canteen = xlWorksheet.Cells[i, 9].Text.Trim();
                                string restroom = xlWorksheet.Cells[i, 10].Text.Trim();
                                string creches = xlWorksheet.Cells[i, 11].Text.Trim();
                                string drinkingWater = xlWorksheet.Cells[i, 12].Text.Trim();
                                string firstAid = xlWorksheet.Cells[i, 13].Text.Trim();


                                int[] colsToCheck = new int[] { 1, 2, 4 }; //specify the column number

                                var chkDuplicate = RLCS_DocumentManagement.checkMultipleDuplicateDataExistExcelSheet(xlWorksheet, i, colsToCheck);
                                if (chkDuplicate == true)
                                {
                                    //errorMessage.Add("Principle Employer Id and Name and Branch Id combination found duplicate at Row number- " + i + ". Please provide unique data.");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Principle Employer Id and Name and Branch Id combination found duplicate. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }


                                int principleEmployerId = 0;
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {


                                    try
                                    {
                                        principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                                        var principleEmplyr = db.RLCS_PrincipleEmployerMaster.Where(t => t.ClientID == clientid && t.Status == "A" && t.PEID == principleEmployerId).FirstOrDefault();
                                        if (principleEmplyr == null)
                                        {
                                            //errorMessage.Add("Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row number" + i);
                                            objError = new ErrorMessage();
                                            objError.ErrorDescription = "Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row ";
                                            objError.RowNum = i.ToString();
                                            lstErrorMessage.Add(objError);
                                        }

                                    }
                                    catch (Exception ee)
                                    {
                                        //errorMessage.Add("Principle Employer Id should be integer. Check at row number" + i + ".");
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Principle Employer Id should be integer. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }
                                else
                                {
                                    //errorMessage.Add("Required Principle Employer Id at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Principle Employer Id. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }


                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Name. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }



                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Address at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Address. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }



                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                {
                                    if (principleEmployerId > 0)
                                    {
                                        try
                                        {
                                            int branchId = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text.Trim());
                                            var branch = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.PELID == branchId && t.Status == "A").FirstOrDefault();
                                            if (branch == null)
                                            {
                                                //errorMessage.Add("Branch " + branchId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);
                                                objError = new ErrorMessage();
                                                objError.ErrorDescription = "Branch " + branchId + " Does Not Exist for the Selected Principle Employer. Check at Row ";
                                                objError.RowNum = i.ToString();
                                                lstErrorMessage.Add(objError);
                                            }

                                        }
                                        catch (Exception ee)
                                        {
                                            //errorMessage.Add("Branch Id should be integer. Check at Row " + i);
                                            objError = new ErrorMessage();
                                            objError.ErrorDescription = "Branch Id should be integer. Check at Row ";
                                            objError.RowNum = i.ToString();
                                            lstErrorMessage.Add(objError);
                                        }
                                    }
                                }
                                else
                                {
                                    //errorMessage.Add("Required BranchId at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required BranchId. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }


                                DateTime dateContractFrom = new DateTime();
                                DateTime dateContractTo = new DateTime();
                                bool invalidDates = false;

                                if (contractFrom != "")
                                {
                                    try
                                    {

                                        //dateContractFrom = Convert.ToDateTime(contractFrom, CultureInfo.InvariantCulture);
                                        //contractFrom = dateContractFrom.ToString();

                                        DateTime dtFrom = Convert.ToDateTime(contractFrom);
                                        string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        dateContractFrom = Convert.ToDateTime(strDtFrom);
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Enter valid Contract From Date in dd-mm-yyyy format. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contract From Date in dd-mm-yyyy format. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                        invalidDates = true;
                                    }
                                }
                                else
                                {
                                    //errorMessage.Add("Required Contract From Date at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Contract From Date. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }



                                if (contractTo != "")
                                {
                                    try
                                    {
                                        // dateContractTo = Convert.ToDateTime(contractTo, CultureInfo.InvariantCulture);
                                        //contractTo = dateContractTo.ToString();

                                        DateTime dtTo = Convert.ToDateTime(contractTo);
                                        string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        dateContractTo = Convert.ToDateTime(strDtTo);
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Enter valid Contract To Date in dd-mm-yyyy format. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contract To Date in dd-mm-yyyy format. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                        invalidDates = true;
                                    }
                                }
                                else
                                {
                                    //errorMessage.Add("Required Contract To Date at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Contract To Date. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }


                                if ((contractFrom != "" && contractTo != "") && (!invalidDates))
                                {
                                    if (dateContractFrom > dateContractTo)
                                    {
                                        //errorMessage.Add("Contract From should be less than Contract To. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Contract From Date should be less than Contract To Date. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }



                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Nature of Work at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Nature of Work. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }


                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                {
                                    string numberOfEmployees = xlWorksheet.Cells[i, 8].Text.Trim();
                                    try
                                    {

                                        int lenghtOfNoEmp = numberOfEmployees.Length;
                                        if (lenghtOfNoEmp > 7)
                                        {
                                            //errorMessage.Add("Length of Number of Employees should not be more than 7 digits. Check at row number" + i + ".");
                                            objError = new ErrorMessage();
                                            objError.ErrorDescription = "Length of Number of Employees should not be more than 7 digits. Check at Row ";
                                            objError.RowNum = i.ToString();
                                            lstErrorMessage.Add(objError);
                                        }
                                        else
                                        {
                                            int noOfEmp = Convert.ToInt32(numberOfEmployees);
                                            if (noOfEmp < 0)
                                            {
                                                //errorMessage.Add("Number of Employees should not be less than 0. Check at row number" + i + ".");
                                                objError = new ErrorMessage();
                                                objError.ErrorDescription = "Number of Employees should not be less than 0. Check at Row ";
                                                objError.RowNum = i.ToString();
                                                lstErrorMessage.Add(objError);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Number of Employees should be integer. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Number of Employees should be integer. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }
                                else
                                {
                                    //errorMessage.Add("Required Number of Employees at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Number of Employees. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                //if (canteen == "" || (canteen.ToUpper()!="Y" && canteen.ToUpper()!="N"))
                                //{
                                //    //errorMessage.Add("Required Canteen in 'Y' or 'N' at Row number " + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Canteen in 'Y' or 'N'. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}


                                //if (restroom == "" || (restroom.ToUpper() != "Y" && restroom.ToUpper() != "N"))
                                //{
                                //    //errorMessage.Add("Required Restroom in 'Y' or 'N' at Row number " + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Restroom in 'Y' or 'N'. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}


                                //if (creches == "" || (creches.ToUpper() != "Y" && creches.ToUpper() != "N"))
                                //{
                                //    //errorMessage.Add("Required Creches in 'Y' or 'N' at Row number " + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Creches in 'Y' or 'N'. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}


                                //if (drinkingWater == "" || (drinkingWater.ToUpper() != "Y" && drinkingWater.ToUpper() != "N"))
                                //{
                                //    //errorMessage.Add("Required Drinking Water in 'Y' or 'N' at Row number " + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Drinking Water in 'Y' or 'N'. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}


                                //if (firstAid == "" || (firstAid.ToUpper() != "Y" && firstAid.ToUpper() != "N"))
                                //{
                                //    //errorMessage.Add("Required First Aid in 'Y' or 'N' at Row number " + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required First Aid in 'Y' or 'N'. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}
                                //ADD GG
                                if (!string.IsNullOrEmpty(canteen.ToUpper().Trim()))
                                {
                                    if (canteen.ToUpper() == "Y" || canteen.ToUpper() == "N" || canteen.ToUpper() == "YES" || canteen.ToUpper() == "NO")
                                    { }
                                    else
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Required Canteen in 'Yes/No' or 'Y/N'. Check at Row";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                                if (!string.IsNullOrEmpty(restroom.ToUpper().Trim()))
                                {
                                    if (restroom.ToUpper() == "Y" || restroom.ToUpper() == "N" || restroom.ToUpper() == "YES" || restroom.ToUpper() == "NO")
                                    { }
                                    else
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Required Restroom in 'Yes/No' or 'Y/N'. Check at Row";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                                if (!string.IsNullOrEmpty(creches.ToUpper().Trim()))
                                {
                                    if (creches.ToUpper() == "Y" || creches.ToUpper() == "N" || creches.ToUpper() == "YES" || creches.ToUpper() == "NO")
                                    { }
                                    else
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Required creches in 'Yes/No' or 'Y/N'. Check at Row";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                                if (!string.IsNullOrEmpty(drinkingWater.ToUpper().Trim()))
                                {
                                    if (drinkingWater.ToUpper() == "Y" || drinkingWater.ToUpper() == "N" || drinkingWater.ToUpper() == "YES" || drinkingWater.ToUpper() == "NO")
                                    { }
                                    else
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Required drinkingwater in 'Yes/No' or 'Y/N'. Check at Row";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                                if (!string.IsNullOrEmpty(firstAid.ToUpper().Trim()))
                                {
                                    if (firstAid.ToUpper() == "Y" || firstAid.ToUpper() == "N" || firstAid.ToUpper() == "YES" || firstAid.ToUpper() == "NO")
                                    { }
                                    else
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Required firstAid in 'Yes/No' or 'Y/N'. Check at Row";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                                //END     

                            }

                            if (lstErrorMessage.Count == 0)
                            {
                                int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                                for (int i = 2; i <= xlrow2; i++)
                                {
                                    int principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                                    string name = xlWorksheet.Cells[i, 2].Text.Trim();
                                    string address = xlWorksheet.Cells[i, 3].Text.Trim();
                                    int branchId = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text.Trim());
                                    string contractFrom = xlWorksheet.Cells[i, 5].Text.Trim();
                                    string contractTo = xlWorksheet.Cells[i, 6].Text.Trim();
                                    string natureOfWork = xlWorksheet.Cells[i, 7].Text.Trim();
                                    string noOfEmployees = xlWorksheet.Cells[i, 8].Text.Trim();
                                    string canteen = xlWorksheet.Cells[i, 9].Text.Trim();
                                    string restroom = xlWorksheet.Cells[i, 10].Text.Trim();
                                    string creches = xlWorksheet.Cells[i, 11].Text.Trim();
                                    string drinkingWater = xlWorksheet.Cells[i, 12].Text.Trim();
                                    string firstAid = xlWorksheet.Cells[i, 13].Text.Trim();

                                    DateTime dateContractFrom = new DateTime();
                                    DateTime dateContractTo = new DateTime();


                                    //dateContractFrom = Convert.ToDateTime(contractFrom, CultureInfo.InvariantCulture);
                                    //contractFrom = dateContractFrom.ToString();

                                    DateTime dtFrom = Convert.ToDateTime(contractFrom);
                                    string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dateContractFrom = Convert.ToDateTime(strDtFrom);

                                    //dateContractTo = Convert.ToDateTime(contractTo, CultureInfo.InvariantCulture);
                                    //contractTo = dateContractTo.ToString();

                                    DateTime dtTo = Convert.ToDateTime(contractTo);
                                    string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dateContractTo = Convert.ToDateTime(strDtTo);

                                    bool isCanteen = false;
                                    bool isRestroom = false;
                                    bool isCreches = false;
                                    bool isDrinkingWater = false;
                                    bool isFirstAid = false;

                                    if (canteen.ToUpper() == "Y" || canteen.ToUpper() == "YES")
                                        isCanteen = true;

                                    if (restroom.ToUpper() == "Y" || restroom.ToUpper() == "YES")
                                        isRestroom = true;

                                    if (creches.ToUpper() == "Y" || creches.ToUpper() == "YES")
                                        isCreches = true;

                                    if (drinkingWater.ToUpper() == "Y" || drinkingWater.ToUpper() == "YES")
                                        isDrinkingWater = true;

                                    if (firstAid.ToUpper() == "Y" || firstAid.ToUpper() == "YES")
                                        isFirstAid = true;

                                    RLCS_PrincipleEmployerContractor updateDetails = (from row in db.RLCS_PrincipleEmployerContractor
                                                                                      where row.PEC_PEID == principleEmployerId && row.PEC_PELID == branchId && row.PEC_ContractorName == name && row.PEC_Status == "A"
                                                                                      select row).FirstOrDefault();
                                    int updateDetails1=0;

                                    if (updateDetails != null)
                                    {
                                        if (updateDetails.PECID > 0)
                                        {
                                            updateDetails1 = updateDetails.PECID;
                                            //foreach (var item in updateDetails)
                                            //{
                                            //    if (item.PECID != null)
                                            //    {
                                            //        PECID = item.PECID;

                                            //    }
                                            //}
                                        }
                                    }

                                   


                                    bool SaveChangesdata = false;

                                    if (updateDetails != null)
                                    {
                                        //updateDetails.PEC_PEID = principleEmployerId.ToString();
                                        //updateDetails.PEC_PELID = branchId;
                                        //updateDetails.PEC_ContractorName = name;

                                        updateDetails.PEC_Address = address;
                                        updateDetails.PEC_NatureOfWork = natureOfWork;
                                        updateDetails.PEC_ContractFrom = dateContractFrom;
                                        updateDetails.PEC_ContractTo = dateContractTo;
                                        updateDetails.PEC_NoOfEmployees = Convert.ToInt32(noOfEmployees);
                                        updateDetails.PEC_CanteenProvided = isCanteen;
                                        updateDetails.PEC_RestroomProvided = isRestroom;
                                        updateDetails.PEC_Creches = isCreches;
                                        updateDetails.PEC_DrinkingWater = isDrinkingWater;
                                        updateDetails.PEC_FirstAid = isFirstAid;
                                        updateDetails.PEC_ModifiedBy = UserID.ToString();
                                        updateDetails.PEC_ModifiedDate = DateTime.Now;
                                        db.SaveChanges();
                                        SaveChangesdata = true;
                                    }
                                    else
                                    {
                                        RLCS_PrincipleEmployerContractor objContractor = new RLCS_PrincipleEmployerContractor()
                                        {
                                            PEC_PEID = principleEmployerId,
                                            PEC_PELID = branchId,
                                            PEC_ContractorName = name,
                                            PEC_Address = address,
                                            PEC_NatureOfWork = natureOfWork,
                                            PEC_ContractFrom = dateContractFrom,
                                            PEC_ContractTo = dateContractTo,
                                            PEC_NoOfEmployees = Convert.ToInt32(noOfEmployees),
                                            PEC_CanteenProvided = isCanteen,
                                            PEC_RestroomProvided = isRestroom,
                                            PEC_Creches = isCreches,
                                            PEC_DrinkingWater = isDrinkingWater,
                                            PEC_FirstAid = isFirstAid,
                                            PEC_Status = "A",
                                            PEC_CreatedBy = UserID.ToString(),
                                            PEC_CreatedDate = DateTime.Now

                                        };
                                        db.RLCS_PrincipleEmployerContractor.Add(objContractor);
                                        db.SaveChanges();
                                        SaveChangesdata = true;
                                    }
                                    bool APISuccess = false;

                                    //uncomment the section if below api works

                                    //CLRA_APICall.PrincipleEmployerContractor jsonPEC = new CLRA_APICall.PrincipleEmployerContractor()
                                    //{
                                    //    PECID = updateDetails.PECID,
                                    //    PEC_PEID = updateDetails.PEC_PEID,
                                    //    PEC_PELID = updateDetails.PEC_PEID,
                                    //    PEC_ContractorName = updateDetails.PEC_ContractorName,
                                    //    PEC_Address = address,
                                    //    PEC_NatureOfWork = natureOfWork,
                                    //    PEC_ContractFrom = dateContractFrom,
                                    //    PEC_ContractTo = dateContractTo,
                                    //    PEC_NoOfEmployees = Convert.ToInt32(noOfEmployees),
                                    //    PEC_CanteenProvided = isCanteen == true ? "YES" : "NO",
                                    //    PEC_RestroomProvided = isRestroom == true ? "YES" : "NO",
                                    //    PEC_Creches = isCreches == true ? "YES" : "NO",
                                    //    PEC_DrinkingWater = isDrinkingWater == true ? "YES" : "NO",
                                    //    PEC_FirstAid = isFirstAid == true ? "YES" : "NO",
                                    //    PEC_CreatedBy = AuthenticationHelper.UserID.ToString()
                                    //};
                                    //CLRA_APICall objAPI = new CLRA_APICall();
                                    //APISuccess = objAPI.PrincipleEmployerContractorApiCall(jsonPEC);
                                    //if (APISuccess==true || SaveChangesdata==true)
                                    if (SaveChangesdata == true &&  updateDetails1 !=0)
                                    {
                                        CLRA_APICall.Update_ProcessedStatus_PrincipleEmployerContractor(updateDetails1, APISuccess);
                                    }
                                }

                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = " Details Uploaded Successfully";
                                vsUploadUtility.CssClass = "alert alert-success";
                            }
                            else
                                GeneratePrincipleEmployeeUploadErrorCSV(lstErrorMessage, ErrorFileName);

                        }
                        #endregion
                    }
                    else
                    {
                        //string i = "2";
                        //ErrorMessage objError = null;
                        //objError = new ErrorMessage();
                        //objError.ErrorDescription = "File is empty";
                        //objError.RowNum = i.ToString();
                        //lstErrorMessage.Add(objError);
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Empty File Uploaded. Please try again.";

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        public void GeneratePrincipleEmployeeUploadErrorCSV(List<ErrorMessage> detailView, string FileName)
        {

            try
            {
              
                spanErrors.Visible = true;

                if (!Directory.Exists(Server.MapPath("~/CLRA/TempEmpError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/CLRA/TempEmpError/"));
                }

                string FilePath = Server.MapPath("~/CLRA/TempEmpError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {
                        //string content = "Emp Id, Error Description, Row No";
                        //string content = columnName + ", Error Description, Row No";
                        string content = "Error Description,Row Num";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {
                                //content = data.ToString();
                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }


            /*try
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Uploaded file contains some errors. Please check the downloaded error file.";

               

                string delimiter = "";
                StringBuilder sb = new StringBuilder();
                List<string> CsvRow = new List<string>();

                if (detailView.Count > 0)
                {
                    //string content = "Emp Id, Error Description, Row No";
                    string content = "Error Desciption";
                    CsvRow.Add(content + Environment.NewLine);

                    foreach (var data in detailView)
                    {
                        if(data!=null)
                        {
                            content = data.ToString();
                            CsvRow.Add(content + Environment.NewLine);
                        }               
                    }
                }

                sb.AppendLine(string.Join(delimiter, CsvRow));
                
                byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());

                if (bytes != null)
                {
                    Response.Clear();
                    Response.ContentType = "text/csv";
                    Response.AddHeader("Content-Length", bytes.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=ErrorFile_" + FileName + "_" + DateTime.Today.ToString() + ".csv");
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
            }
            catch(Exception e)
            {

            }*/
        }

        public void DownloadDataCSV()
        {
            //work in progress
            string FileName = "PrincipleEmployeeDetails_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
            try
            {
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    List<PrincipleEmployeeDetails> lstPEData = new List<PrincipleEmployeeDetails>();

                    lstPEData = (from P in db.RLCS_PrincipleEmployerMaster
                           join PL in db.RLCS_PrincipleEmployerLocationMaster on P.PEID equals PL.PEID
                           join SM in db.RLCS_State_Mapping on PL.State equals SM.SM_Code
                           join LM in db.RLCS_Location_City_Mapping on PL.Location equals LM.LM_Code
                           where P.ClientID == hdClientId.Value && P.Status == "A" && PL.Status == "A"
                           select new PrincipleEmployeeDetails
                           {
                               PEID = P.PEID,
                               PEName = P.PEName,
                               StateId = SM.SM_Code,
                               StateName = SM.SM_Name,
                               LocationId = LM.LM_Code,
                               LocationName = LM.LM_Name,
                               BranchId = PL.PELID,
                               Branch = PL.Branch
                           }).Distinct().ToList();

                    if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadData/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadData/"));
                    }

                    string FilePath = Server.MapPath("~/CLRA/DownloadData/") + FileName;
                    using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                    {
                        string delimiter = "";
                        StringBuilder sb = new StringBuilder();
                        List<string> CsvRow = new List<string>();
                        string content = "PEID,PEName,StateId,StateName,LocationId,LocationName,BranchId,Branch";
                        CsvRow.Add(content + Environment.NewLine);

                        if (lstPEData != null && lstPEData.Count > 0)
                        {
                            foreach (var data in lstPEData)
                            {
                                if (data != null)
                                {

                                    //content = data.ToString();
                                    content = data.PEID +","+data.PEName + ","+data.StateId + ","+data.StateName + "," + data.LocationId + "," + data.LocationName + "," + data.BranchId + "," + data.Branch;
                                    CsvRow.Add(content + Environment.NewLine);
                                }
                            }
                        }

                        sb.AppendLine(string.Join(delimiter, CsvRow));
                        swOutputFile.WriteLine(sb.ToString());
                    }
                   
                    DownloadFile(FilePath);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


           
        }



        protected void btnDownloadError_Click(object sender, EventArgs e)
        {
            string filePath = Server.MapPath("~/CLRA/TempEmpError/" + hdFileName.Value);
            DownloadFile(filePath);
        }

        protected void DownloadSample(object sender, EventArgs e)
        {
            //string filePath = Server.MapPath("~/CLRA/PrincipleContractorMasterBulkUpload.xlsx");
            if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadSamples/")))
            {
                Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadSamples/"));
            }
            string filePath = Server.MapPath("~/CLRA/DownloadSamples/SamplePrincipleEmployerContractor.xlsx");
            DownloadFile(filePath);
        }
        protected void DownloadPEDetails(object sender, EventArgs e)
        {
            DownloadDataCSV();
        }

        private void DownloadFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/plain";
                    Response.Flush();
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public class PrincipleEmployeeDetails
        {
          
            public int PEID { get; set; }
            public string PEName { get; set; }
            public string StateId { get; set; }
            public string StateName { get; set; }
            public string LocationId { get; set; }
            public string LocationName { get; set; }
            public int BranchId { get; set; }
            public string Branch { get; set; }
        }

        public class ErrorMessage
        {
            public string ErrorDescription { get; set; }
            public string RowNum { get; set; }
        }

    }
}