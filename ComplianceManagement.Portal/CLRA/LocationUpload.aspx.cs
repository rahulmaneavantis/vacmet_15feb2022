﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
namespace com.VirtuosoITech.ComplianceManagement.Portal.CLRA
{
    public partial class LocationUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            spanErrors.Visible = false;

            if (Request.QueryString["clientid"] != null || Request.QueryString["employerId"]!=null || Request.QueryString["employerState"] != null || Request.QueryString["employerLocation"] != null)  
            {
                hdClientId.Value = Request.QueryString["clientid"];
                hdEmployerId.Value = Request.QueryString["employerId"];
                hdEmployerState.Value = Request.QueryString["employerState"];
                hdEmployerLocation.Value = Request.QueryString["employerLocation"];
            }
        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";

            
            if (ContractFileUpload.HasFile && Path.GetExtension(ContractFileUpload.FileName).ToLower() == ".xlsx")
            {
                try
                {
                    if (!Directory.Exists(Server.MapPath("~/CLRA/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/Uploaded/"));
                    }

                    string filename = Path.GetFileName(ContractFileUpload.FileName);
                    ContractFileUpload.SaveAs(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "EmpLocationBulkUp");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'EmpLocationBulkUp' ";
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }
                            //ProcessChecklistData(xlWorkbook);

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["EmpLocationBulkUp"];

                if (xlWorksheet != null)
                {
                    //List<string> errorMessage = new List<string>();
                    List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();

                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    if (xlrow2 > 1)
                    {
                        string ErrorFileName = "ErrorFile_EmpLocationUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";

                        hdFileName.Value = ErrorFileName;

                        #region Validations
                        using (ComplianceDBEntities db = new ComplianceDBEntities())
                        {


                            ErrorMessage objError = null;
                            string clientid = hdClientId.Value;
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                string state = xlWorksheet.Cells[i, 1].Text.Trim();
                                //string natureOdBusiness = xlWorksheet.Cells[i, 2].Text.Trim();
                                string location = xlWorksheet.Cells[i, 2].Text.Trim();

                                string branch = xlWorksheet.Cells[i, 3].Text.Trim();
                                
                                string branchAddress = xlWorksheet.Cells[i, 4].Text.Trim();

                                var statecheck = RLCS_ClientsManagement.Checkstate(state);

                                string stateval = "";
                                bool stateflag1 = true;

                                if (statecheck.Count > 0)
                                {

                                    foreach (var item in statecheck)
                                    {
                                        if (item.SM_Code != null || item.SM_Code != "")
                                        {
                                            stateval = item.SM_Code;
                                            stateflag1 = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    stateval = "";
                                    stateflag1 = false;

                                }

                                var citycheck = RLCS_ClientsManagement.Checkcity(location);

                                string cityval = "";
                                bool cityflag1 = true;

                                if (citycheck.Count > 0)
                                {

                                    foreach (var item in citycheck)
                                    {
                                        if (item.LM_Code != null || item.LM_Code != "")
                                        {
                                            cityval = item.LM_Code;
                                            cityflag1 = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    cityval = "";
                                    cityflag1 = false;

                                }




                                //var stateNm = RLCS_ClientsManagement.CheckStateCity(branch);

                                //string statecode = "";
                                //bool stateflag = true;

                                //if (stateNm.Count > 0)
                                //{

                                //    foreach (var item in stateNm)
                                //    {
                                //        if (item.SM_Code != null || item.SM_Code != "")
                                //        {
                                //            statecode = item.SM_Code;
                                //            stateflag = true;
                                //            break;
                                //        }
                                //    }
                                //}
                                //else
                                //{
                                //    statecode = "";
                                //    stateflag = false;

                                //}

                                //if (stateNm != null)
                                //    string stateName = stateNm.SM_Code;
                                //else
                                //    string stateName = "";

                                string numberOfEmp = xlWorksheet.Cells[i, 5].Text.Trim();
                                string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();
                                string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();
                                string weekOff = xlWorksheet.Cells[i, 8].Text.Trim();
                                string minesApplicabability = xlWorksheet.Cells[i, 9].Text.Trim();
                                string peLine = xlWorksheet.Cells[i, 10].Text.Trim();
                                string peAuthorisedEmailId = xlWorksheet.Cells[i, 11].Text.Trim();
                                string peCompanyPhoneNo = xlWorksheet.Cells[i, 12].Text.Trim();
                                string clientLINNo = xlWorksheet.Cells[i, 13].Text.Trim();
                                string clientCompanyEmailId = xlWorksheet.Cells[i, 14].Text.Trim();
                                string clientCompanyPhoneNumber = xlWorksheet.Cells[i, 15].Text.Trim();
                                string contractLicenseNo = xlWorksheet.Cells[i, 16].Text.Trim();
                                string licenseValidFrom = xlWorksheet.Cells[i, 17].Text.Trim();
                                string licenseValidTo = xlWorksheet.Cells[i, 18].Text.Trim();
                                string contractOrPersonInchargeName = xlWorksheet.Cells[i, 19].Text.Trim();
                                string contractOrPersonInchargeLIN = xlWorksheet.Cells[i, 20].Text.Trim();
                                string contractOrPersonInchargePAN = xlWorksheet.Cells[i, 21].Text.Trim();
                                string contractOrPersonInchargeEmail = xlWorksheet.Cells[i, 22].Text.Trim();
                                string contractOrPersonInchargeMobNo = xlWorksheet.Cells[i, 23].Text.Trim();
                                string clientNatureOfBusiness = xlWorksheet.Cells[i, 24].Text.Trim();
                                string peAddress = xlWorksheet.Cells[i, 25].Text.Trim();
                                string contractLicenseOffDesig = xlWorksheet.Cells[i, 26].Text.Trim();
                                string licensingOfficerHeadQuarter = xlWorksheet.Cells[i, 27].Text.Trim();
                                string natureOfWelfare = xlWorksheet.Cells[i, 28].Text.Trim();
                                string statutoryStatus = xlWorksheet.Cells[i, 29].Text.Trim();


                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Principle State. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Principle Location. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }


                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Branch. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Branch Address. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                if (stateflag1 != true && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Please enter correct Principle State.   Check the correct State Name from Masters/ State-City";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                if (cityflag1 != true && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Please enter correct Principle Location. Check the correct City Name from Masters/ State-City";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Number of Employees. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Contract From. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Contract To. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 8].Text).Trim()))
                                {
                                    string s1 = xlWorksheet.Cells[i, 8].Text.Trim().ToLower();
                                    int flag1 = 0;
                                  //  int count = s1.Split(',').Length - 1;
                                    var arlist = new ArrayList()
                                        {
                                            "mon", "tue", "wed","thus" ,"fri", "sat", "sun","monday","tuesday","wednesday","thursday","friday","saturday","sunday"
                                        };

                                    foreach (var item in arlist)
                                    {
                                        if (s1.Contains(Convert.ToString(item)))
                                        {
                                            flag1 = flag1+1;
                                        }
                                    }

                                    if (flag1 <1) {

                                        //errorMessage.Add("Required Name at row number" + i + ".");
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Required Week off Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);

                                    }



                                }

                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 9].Text).Trim()))
                                {
                                    //errorMessage.Add("Required Name at row number" + i + ".");

                                    if ((xlWorksheet.Cells[i, 9].Text).Trim().ToLower() != "") {
                                        if ((xlWorksheet.Cells[i, 9].Text).Trim().ToLower() == "yes" || (xlWorksheet.Cells[i, 9].Text).Trim().ToLower() == "no")  
                                        {
                                        }
                                        else
                                        {

                                            objError = new ErrorMessage();
                                            objError.ErrorDescription = "Enter Yes/No in Mines Applicability. Check at Row ";
                                            objError.RowNum = i.ToString();
                                            lstErrorMessage.Add(objError);

                                        }
                                    }
                                }

                                //if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                                //{
                                //    //errorMessage.Add("Required Address at row number" + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Nature of Business. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 11].Text).Trim()))
                                {
                                    String theEmailPattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";
                                    bool emailVal = Regex.IsMatch((xlWorksheet.Cells[i, 11].Text).Trim(), theEmailPattern);
                                    if (emailVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid PE Authorised Person Email ID. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }

                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 12].Text).Trim()))
                                {
                                    String phoneNo = @"^[789]\d{9}$";
                                    bool phoneNoVal = Regex.IsMatch((xlWorksheet.Cells[i, 12].Text).Trim(), phoneNo);
                                    if (phoneNoVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid PE Company Phone No. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 14].Text).Trim()))
                                {
                                    String theEmailPattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";
                                    bool emailVal = Regex.IsMatch((xlWorksheet.Cells[i, 14].Text).Trim(), theEmailPattern);
                                    if (emailVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Client Company Email ID. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 15].Text).Trim()))
                                {
                                    String phoneNo = @"^[789]\d{9}$";
                                    bool phoneNoVal = Regex.IsMatch((xlWorksheet.Cells[i, 15].Text).Trim(), phoneNo);
                                    if (phoneNoVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Client Company Phone No. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }

                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 21].Text).Trim()))
                                {
                                    String PanNo = @"^[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}$";
                                    bool PanNoVal = Regex.IsMatch((xlWorksheet.Cells[i, 21].Text).Trim(), PanNo);
                                    if (PanNoVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contractor Person Incharge PAN ID. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }

                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 22].Text).Trim()))
                                {
                                    String theEmailPattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";
                                    bool emailVal = Regex.IsMatch((xlWorksheet.Cells[i, 22].Text).Trim(), theEmailPattern);
                                    if (emailVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contractor Person Incharge Email ID. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }

                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 23].Text).Trim()))
                                {
                                    String phoneNo = @"^[789]\d{9}$";
                                    bool phoneNoVal = Regex.IsMatch((xlWorksheet.Cells[i, 23].Text).Trim(), phoneNo);
                                    if (phoneNoVal == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contractor Person Incharge Mobile No. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                                {
                                    int empResult;
                                    bool isNumeric = int.TryParse((xlWorksheet.Cells[i, 5].Text).Trim(), out empResult);
                                    if (isNumeric == false)
                                    {
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter integer value for number of employees. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }
                                }



                                //int[] colsToCheck = new int[] { 1,2,4}; //specify the column number

                                //var chkDuplicate = RLCS_DocumentManagement.checkMultipleDuplicateDataExistExcelSheet(xlWorksheet, i, colsToCheck);
                                //if (chkDuplicate == true)
                                //{
                                //    //errorMessage.Add("Principle Employer Id and Name and Branch Id combination found duplicate at Row number- " + i + ". Please provide unique data.");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Principle Employer Id and Name and Branch Id combination found duplicate. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}


                                //int principleEmployerId = 0;
                                //if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                                //{


                                //    try
                                //    {
                                //        principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                                //        var principleEmplyr = db.RLCS_PrincipleEmployerMaster.Where(t => t.ClientID == clientid && t.Status == "A" && t.PEID == principleEmployerId).FirstOrDefault();
                                //        if (principleEmplyr == null)
                                //        {
                                //            //errorMessage.Add("Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row number" + i);
                                //            objError = new ErrorMessage();
                                //            objError.ErrorDescription = "Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row ";
                                //            objError.RowNum = i.ToString();
                                //            lstErrorMessage.Add(objError);
                                //        }

                                //    }
                                //    catch(Exception ee)
                                //    {
                                //        //errorMessage.Add("Principle Employer Id should be integer. Check at row number" + i + ".");
                                //        objError = new ErrorMessage();
                                //        objError.ErrorDescription = "Principle Employer Id should be integer. Check at Row ";
                                //        objError.RowNum = i.ToString();
                                //        lstErrorMessage.Add(objError);
                                //    }
                                //}
                                //else
                                //{
                                //    //errorMessage.Add("Required Principle Employer Id at row number" + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Principle Employer Id. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}

                                DateTime dateContractFrom = new DateTime();
                                DateTime dateContractTo = new DateTime();
                                DateTime datelicenseValidFrom = new DateTime();
                                DateTime datelicenseValidTo = new DateTime();
                                bool invalidDates = false;

                                if (licenseValidFrom != "")
                                {
                                    try
                                    {
                                        string licenseFromFormat = string.Format("{0:dd/MM/yyyy}", licenseValidFrom);
                                        datelicenseValidFrom = DateTime.ParseExact(licenseFromFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                        //DateTime dtFrom = Convert.ToDateTime(licenseValidFrom);
                                        //string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //dateContractFrom = Convert.ToDateTime(strDtFrom);
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Enter valid Contract From Date in dd-mm-yyyy format. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid license Valid From Date in dd-mm-yyyy format. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                        invalidDates = true;
                                    }
                                }

                                if (contractFrom != "")
                                {
                                    try
                                    {
                                        //DateTime dtFrom = Convert.ToDateTime(contractFrom);
                                        //string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //dateContractFrom = Convert.ToDateTime(strDtFrom);
                                        string contractFromFormat = string.Format("{0:dd/MM/yyyy}", contractFrom);
                                        dateContractFrom = DateTime.ParseExact(contractFromFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Enter valid Contract From Date in dd/mm/yyyy format. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contract From Date in dd/mm/yyyy format. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                        invalidDates = true;
                                    }
                                }

                                //else contractFrom
                                //{
                                //    //errorMessage.Add("Required Contract From Date at row number" + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Contract From Date. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}

                                if (licenseValidTo != "")
                                {
                                    try
                                    {
                                        // dateContractTo = Convert.ToDateTime(contractTo, CultureInfo.InvariantCulture);
                                        //contractTo = dateContractTo.ToString();

                                        //DateTime dtTo = Convert.ToDateTime(licenseValidTo);
                                        //string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //dateContractTo = Convert.ToDateTime(strDtTo);
                                        string licenseToFormat = string.Format("{0:dd/MM/yyyy}", licenseValidTo);
                                        datelicenseValidTo = DateTime.ParseExact(licenseToFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Enter valid Contract To Date in dd-mm-yyyy format. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid license Valid To Date in dd-mm-yyyy format. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                        invalidDates = true;
                                    }
                                }


                                if (contractTo != "")
                                {
                                    try
                                    {
                                        // dateContractTo = Convert.ToDateTime(contractTo, CultureInfo.InvariantCulture);
                                        //contractTo = dateContractTo.ToString();
                                        string contractToFormat = string.Format("{0:dd/MM/yyyy}", contractTo);
                                        dateContractTo = DateTime.ParseExact(contractToFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //bool isSuccess = DateTime.TryParse(contractTo, out dateTime10);
                                        //DateTime dtTo = Convert.ToDateTime(contractTo);
                                        //string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //dateContractTo = Convert.ToDateTime(strDtTo);
                                    }
                                    catch (Exception ex)
                                    {
                                        //errorMessage.Add("Enter valid Contract To Date in dd-mm-yyyy format. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Enter valid Contract To Date in dd-mm-yyyy format. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                        invalidDates = true;
                                    }
                                }

                                //else
                                //{
                                //    //errorMessage.Add("Required Contract To Date at row number" + i + ".");
                                //    objError = new ErrorMessage();
                                //    objError.ErrorDescription = "Required Contract To Date. Check at Row ";
                                //    objError.RowNum = i.ToString();
                                //    lstErrorMessage.Add(objError);
                                //}

                                if ((contractFrom != "" && contractTo != "") && (!invalidDates))
                                {
                                    if (dateContractFrom > dateContractTo)
                                    {
                                        //errorMessage.Add("Contract From should be less than Contract To. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Contract From Date should be less than Contract To Date. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }


                                if ((licenseValidFrom != "" && licenseValidTo != "") && (!invalidDates))
                                {
                                    if (dateContractFrom > dateContractTo)
                                    {
                                        //errorMessage.Add("Contract From should be less than Contract To. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "licenseValidFrom should be less than licenseValidTo. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }

                            }

                            if (lstErrorMessage.Count == 0)
                            {
                                int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                                for (int i = 2; i <= xlrow2; i++)
                                {

                                    int principleEmployerId = Convert.ToInt32(hdEmployerId.Value);
                                   string state = xlWorksheet.Cells[i, 1].Text.Trim();
                                    string location = xlWorksheet.Cells[i, 2].Text.Trim();

                                    string branch = xlWorksheet.Cells[i, 3].Text.Trim();

                                    var statecheck = RLCS_ClientsManagement.Checkstate(state);

                                    string stateval = "";
                             
                                    if (statecheck.Count > 0)
                                    {

                                        foreach (var item in statecheck)
                                        {
                                            if (item.SM_Code != null || item.SM_Code != "")
                                            {
                                                stateval = item.SM_Code;
                                               
                                              
                                            }
                                        }
                                    }
                                    var citycheck = RLCS_ClientsManagement.Checkcity(location);

                                    string cityval = "";
                                 
                                    if (citycheck.Count > 0)
                                    {

                                        foreach (var item in citycheck)
                                        {
                                            if (item.LM_Code != null || item.LM_Code != "")
                                            {
                                                cityval = item.LM_Code;
                                            }
                                        }
                                    }


                                    //var stateNm = RLCS_ClientsManagement.CheckStateCity(branch);
                                    //var statelocation = RLCS_ClientsManagement.CheckStateCity2(branch);
                                    //string statecode = "";
                                    //string location = "";

                                    //foreach (var item in stateNm)
                                    //{
                                    //    if (item.SM_Code != null || item.SM_Code != "")
                                    //    {
                                    //        statecode = item.SM_Code;
                                    //        break;
                                    //    }
                                    //    else
                                    //    {
                                    //        statecode = "";
                                    //        break;
                                    //    }
                                    //}

                                    //foreach (var item in statelocation)
                                    //{
                                    //    if (item.LM_Code != null || item.LM_Code != "")
                                    //    {
                                    //        location = item.LM_Code;
                                    //        break;
                                    //    }
                                    //    else
                                    //    {
                                    //        location = "";
                                    //        break;
                                    //    }
                                    //}


                                    string natureOdBusiness = "";
                                    string branchAddress = xlWorksheet.Cells[i, 4].Text.Trim();
                                    string numberOfEmp = xlWorksheet.Cells[i, 5].Text.Trim();
                                    string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();
                                    string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();
                                    string weekOff = (xlWorksheet.Cells[i, 8].Text.Trim()).Replace(",", "|");
                                    //string minesApplicabability = xlWorksheet.Cells[i, 7].Text.Trim();
                                    bool minesApplicabability = false;
                                    if (xlWorksheet.Cells[i, 9].Text.Trim().ToLower() == "yes")
                                    {
                                        minesApplicabability = true;
                                    }
                                    else
                                    {
                                        minesApplicabability = false;
                                    }
                                    string peLine = xlWorksheet.Cells[i, 10].Text.Trim();
                                    string peAuthorisedEmailId = xlWorksheet.Cells[i, 11].Text.Trim();
                                    string peCompanyPhoneNo = xlWorksheet.Cells[i, 12].Text.Trim();
                                    string clientLINNo = xlWorksheet.Cells[i, 13].Text.Trim();
                                    string clientCompanyEmailId = xlWorksheet.Cells[i, 14].Text.Trim();
                                    string clientCompanyPhoneNumber = xlWorksheet.Cells[i, 15].Text.Trim();
                                    string contractLicenseNo = xlWorksheet.Cells[i, 16].Text.Trim();
                                    string licenseValidFrom = xlWorksheet.Cells[i, 17].Text.Trim();
                                    string licenseValidTo = xlWorksheet.Cells[i, 18].Text.Trim();
                                    string contractOrPersonInchargeName = xlWorksheet.Cells[i, 19].Text.Trim();
                                    string contractOrPersonInchargeLIN = xlWorksheet.Cells[i, 20].Text.Trim();
                                    string contractOrPersonInchargePAN = xlWorksheet.Cells[i, 21].Text.Trim().ToUpper();
                                    string contractOrPersonInchargeEmail = xlWorksheet.Cells[i, 22].Text.Trim();
                                    string contractOrPersonInchargeMobNo = xlWorksheet.Cells[i, 23].Text.Trim();
                                    string clientNatureOfBusiness = xlWorksheet.Cells[i, 24].Text.Trim();
                                    string peAddress = xlWorksheet.Cells[i, 25].Text.Trim();
                                    string contractLicenseOffDesig = xlWorksheet.Cells[i, 26].Text.Trim();
                                    string licensingOfficerHeadQuarter = xlWorksheet.Cells[i, 27].Text.Trim();
                                    string natureOfWelfare = xlWorksheet.Cells[i, 28].Text.Trim();
                                    string statutoryStatus = xlWorksheet.Cells[i, 29].Text.Trim();

                                    //string weekOff = xlWorksheet.Cells[i, 3].Text.Trim();
                                    //bool minesApplicabability = false;
                                    //if (xlWorksheet.Cells[i, 4].Text.Trim() == "Yes")
                                    //{
                                    //    minesApplicabability = true;
                                    //}
                                    //else
                                    //{
                                    //    minesApplicabability = false;
                                    //}
                                    //string peLine = xlWorksheet.Cells[i, 5].Text.Trim();
                                    //string peAuthorisedEmailId = xlWorksheet.Cells[i, 6].Text.Trim();
                                    //string peCompanyPhoneNo = xlWorksheet.Cells[i, 7].Text.Trim();
                                    //string clientLINNo = xlWorksheet.Cells[i, 8].Text.Trim();
                                    //string clientCompanyEmailId = xlWorksheet.Cells[i, 9].Text.Trim();
                                    //string clientCompanyPhoneNumber = xlWorksheet.Cells[i, 10].Text.Trim();
                                    //string contractLicenseNo = xlWorksheet.Cells[i, 11].Text.Trim();
                                    //string licenseValidFrom = xlWorksheet.Cells[i, 12].Text.Trim();
                                    //string licenseValidTo = xlWorksheet.Cells[i, 13].Text.Trim();
                                    //string contractOrPersonInchargeName = xlWorksheet.Cells[i, 14].Text.Trim();
                                    //string contractOrPersonInchargeLIN = xlWorksheet.Cells[i, 15].Text.Trim();
                                    //string contractOrPersonInchargePAN = xlWorksheet.Cells[i, 16].Text.Trim();
                                    //string contractOrPersonInchargeEmail = xlWorksheet.Cells[i, 17].Text.Trim();
                                    //string contractOrPersonInchargeMobNo = xlWorksheet.Cells[i, 18].Text.Trim();
                                    //string clientNatureOfBusiness = xlWorksheet.Cells[i, 19].Text.Trim();
                                    //string peAddress = xlWorksheet.Cells[i, 20].Text.Trim();
                                    //string contractLicenseOffDesig = xlWorksheet.Cells[i, 21].Text.Trim();
                                    //string licensingOfficerHeadQuarter = xlWorksheet.Cells[i, 22].Text.Trim();
                                    //string natureOfWelfare = xlWorksheet.Cells[i, 23].Text.Trim();
                                    //string statutoryStatus = xlWorksheet.Cells[i, 24].Text.Trim();

                                    DateTime dateLicenseValidFrom = new DateTime();
                                    DateTime dateLicenseValidTo = new DateTime();


                                    //dateContractFrom = Convert.ToDateTime(contractFrom, CultureInfo.InvariantCulture);
                                    //contractFrom = dateContractFrom.ToString();

                                    if (!string.IsNullOrEmpty(licenseValidFrom))
                                    {
                                        //DateTime dtFrom = Convert.ToDateTime(licenseValidFrom);
                                        //string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //dateLicenseValidFrom = Convert.ToDateTime(strDtFrom);
                                        string licenseFromFormat = string.Format("{0:dd/MM/yyyy}", licenseValidFrom);
                                        dateLicenseValidFrom = DateTime.ParseExact(licenseFromFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }

                                    //dateContractTo = Convert.ToDateTime(contractTo, CultureInfo.InvariantCulture);
                                    //contractTo = dateContractTo.ToString();
                                    if (!string.IsNullOrEmpty(licenseValidTo))
                                    {
                                        //DateTime dtTo = Convert.ToDateTime(licenseValidTo);
                                        //string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        //dateLicenseValidTo = Convert.ToDateTime(strDtTo);
                                        string licenseTOFormat = string.Format("{0:dd/MM/yyyy}", licenseValidTo);
                                        dateLicenseValidTo = DateTime.ParseExact(licenseTOFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }

                                    DateTime contractFromDate = new DateTime();
                                    DateTime contractToDate = new DateTime();

                                    if (!string.IsNullOrEmpty(contractFrom))
                                    {
                                        string contractFromFormat = string.Format("{0:dd/MM/yyyy}", contractFrom);
                                        contractFromDate = DateTime.ParseExact(contractFromFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    if (!string.IsNullOrEmpty(contractTo))
                                    {
                                        string contractToFormat = string.Format("{0:dd/MM/yyyy}", contractTo);
                                        contractToDate = DateTime.ParseExact(contractToFormat, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }


                                    //RLCS_PrincipleEmployerLocationMaster updateDetails = (from row in db.RLCS_PrincipleEmployerLocationMaster
                                    //                                                      where row.PEID == principleEmployerId && row.PELID == branch && row.PEC_ContractorName == name && row.PEC_Status == "A"
                                    //                                                      select row).FirstOrDefault();

                                    RLCS_PrincipleEmployerLocationMaster objEmpLocation = new RLCS_PrincipleEmployerLocationMaster()
                                    {
                                        PEID = principleEmployerId,
                                        State = stateval,
                                        Branch = branch,
                                        Location = cityval,
                                        Address = branchAddress,
                                        NumberOfEmp = Convert.ToInt32(numberOfEmp),
                                        ContractFrom = contractFromDate,
                                        ContractTo = contractToDate,
                                        NatureOfBusiness = natureOdBusiness,
                                        Status = "A",
                                        Mines = minesApplicabability,
                                        WeekOff = weekOff,
                                        CreatedDate=DateTime.Now,
                                        ModifiedDate= DateTime.Now,
                                        PE_LIN = peLine,
                                        PE_AuthorisedPerson_EmailID = peAuthorisedEmailId,
                                        PE_Company_PhoneNo = peCompanyPhoneNo,
                                        Client_LINNo = clientLINNo,
                                        Client_CompanyEmailID = clientCompanyEmailId,
                                        Client_Company_Phone_No = clientCompanyPhoneNumber,
                                        Contract_Licence_No = contractLicenseNo,
                                        Licence_Valid_From_date = dateLicenseValidFrom,
                                        Licence_Valid_To_date = dateLicenseValidTo,
                                        Contractor_Person_Incharge_Name = contractOrPersonInchargeName,
                                        Contractor_Person_Incharge_LIN = contractOrPersonInchargeLIN,
                                        Contractor_Person_Incharge_PAN = contractOrPersonInchargePAN,
                                        Contractor_Person_Incharge_EmailID = contractOrPersonInchargeEmail,
                                        Contractor_Person_Incharge_MobileNo = contractOrPersonInchargeMobNo,
                                        Client_Nature_of_business = clientNatureOfBusiness,
                                        PE_Address = peAddress,
                                        Contractor_Licensing_Officer_Designation = contractLicenseOffDesig,
                                        Licencing_officer_Head_Quarter = licensingOfficerHeadQuarter,
                                        Nature_ofwelfare_amenities_provided = natureOfWelfare,
                                        Statutory_statute = statutoryStatus
                                    };
                                    db.RLCS_PrincipleEmployerLocationMaster.Add(objEmpLocation);
                                    db.SaveChanges();
                                }



                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = " Details Uploaded Successfully";
                                vsUploadUtility.CssClass = "alert alert-success";
                            }
                            else
                                GeneratePrincipleEmployeeUploadErrorCSV(lstErrorMessage, ErrorFileName);

                        }
                        #endregion
                    }
                    else {
                        //string i = "2";
                        //ErrorMessage objError = null;
                        //objError = new ErrorMessage();
                        //objError.ErrorDescription = "File is empty";
                        //objError.RowNum = i.ToString();
                        //lstErrorMessage.Add(objError);
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Empty File Uploaded. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        public void GeneratePrincipleEmployeeUploadErrorCSV(List<ErrorMessage> detailView, string FileName)
        {

            try
            {
              
                spanErrors.Visible = true;

                if (!Directory.Exists(Server.MapPath("~/CLRA/TempEmpError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/CLRA/TempEmpError/"));
                }

                string FilePath = Server.MapPath("~/CLRA/TempEmpError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {
                        //string content = "Emp Id, Error Description, Row No";
                        //string content = columnName + ", Error Description, Row No";
                        string content = "Error Description,Row Num";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {
                                //content = data.ToString();
                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }


            /*try
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Uploaded file contains some errors. Please check the downloaded error file.";

               

                string delimiter = "";
                StringBuilder sb = new StringBuilder();
                List<string> CsvRow = new List<string>();

                if (detailView.Count > 0)
                {
                    //string content = "Emp Id, Error Description, Row No";
                    string content = "Error Desciption";
                    CsvRow.Add(content + Environment.NewLine);

                    foreach (var data in detailView)
                    {
                        if(data!=null)
                        {
                            content = data.ToString();
                            CsvRow.Add(content + Environment.NewLine);
                        }               
                    }
                }

                sb.AppendLine(string.Join(delimiter, CsvRow));
                
                byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());

                if (bytes != null)
                {
                    Response.Clear();
                    Response.ContentType = "text/csv";
                    Response.AddHeader("Content-Length", bytes.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=ErrorFile_" + FileName + "_" + DateTime.Today.ToString() + ".csv");
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
            }
            catch(Exception e)
            {

            }*/
        }

        public void DownloadDataCSV()
        {
            //work in progress
            string FileName = "PrincipleEmployersLocation_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
            try
            {
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    List<PrincipleEmployeeDetails> lstPEData = new List<PrincipleEmployeeDetails>();

                    lstPEData = (from P in db.RLCS_PrincipleEmployerMaster
                           join PL in db.RLCS_PrincipleEmployerLocationMaster on P.PEID equals PL.PEID
                           join SM in db.RLCS_State_Mapping on PL.State equals SM.SM_Code
                           join LM in db.RLCS_Location_City_Mapping on PL.Location equals LM.LM_Code
                           where P.ClientID == hdClientId.Value && P.Status == "A" && PL.Status == "A"
                           select new PrincipleEmployeeDetails
                           {
                               PEID = P.PEID,
                               PEName = P.PEName,
                               StateId = SM.SM_Code,
                               StateName = SM.SM_Name,
                               LocationId = LM.LM_Code,
                               LocationName = LM.LM_Name,
                               BranchId = PL.PELID,
                               Branch = PL.Branch
                           }).Distinct().ToList();

                    if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadData/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadData/"));
                    }

                    string FilePath = Server.MapPath("~/CLRA/DownloadData/") + FileName;
                    using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                    {
                        string delimiter = "";
                        StringBuilder sb = new StringBuilder();
                        List<string> CsvRow = new List<string>();
                        string content = "PEID,PEName,StateId,StateName,LocationId,LocationName,BranchId,Branch,Test";
                        CsvRow.Add(content + Environment.NewLine);

                        if (lstPEData != null && lstPEData.Count > 0)
                        {
                            foreach (var data in lstPEData)
                            {
                                if (data != null)
                                {

                                    //content = data.ToString();
                                    content = data.PEID +","+data.PEName + ","+data.StateId + ","+data.StateName + "," + data.LocationId + "," + data.LocationName + "," + data.BranchId + "," + data.Branch;
                                    CsvRow.Add(content + Environment.NewLine);
                                }
                            }
                        }

                        sb.AppendLine(string.Join(delimiter, CsvRow));
                        swOutputFile.WriteLine(sb.ToString());
                    }
                   
                    DownloadFile(FilePath);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


           
        }



        protected void btnDownloadError_Click(object sender, EventArgs e)
        {
            string filePath = Server.MapPath("~/CLRA/TempEmpError/" + hdFileName.Value);
            DownloadFile(filePath);
        }

        protected void DownloadSample(object sender, EventArgs e)
        {
            //string filePath = Server.MapPath("~/CLRA/PrincipleContractorMasterBulkUpload.xlsx");
            if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadSamples/")))
            {
                Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadSamples/"));
            }
            string filePath = Server.MapPath("~/CLRA/DownloadSamples/SampleEmployeeLocation.xlsx");
            DownloadFile(filePath);
        }
        protected void DownloadEmployerLocation(object sender, EventArgs e)
        {
            DownloadDataCSV();
        }

        private void DownloadFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/plain";
                    Response.Flush();
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public class PrincipleEmployeeDetails
        {
          
            public int PEID { get; set; }
            public string PEName { get; set; }
            public string StateId { get; set; }
            public string StateName { get; set; }
            public string LocationId { get; set; }
            public string LocationName { get; set; }
            public int BranchId { get; set; }
            public string Branch { get; set; }
        }

        public class ErrorMessage
        {
            public string ErrorDescription { get; set; }
            public string RowNum { get; set; }
        }

    }
}