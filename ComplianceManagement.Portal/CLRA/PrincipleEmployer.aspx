﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="PrincipleEmployer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<!DOCTYPE html>--%>
<%--
<html xmlns="http://www.w3.org/1999/xhtml">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--<head runat="server">--%>
    <title></title>
 <link href="../NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/3.3.1/jquery.min.js"></script>
    <script src="../Newjs/3.3.7/bootstrap.min.js"></script>
    <script src="../Newjs/bootstrap-datepicker.min.js"></script>
    <script src="../Newjs/jquery.validate.min.js"></script>
    <script src="../Newjs/jquery.validate.unobtrusive.js"></script>

    <link href="style.css" rel="stylesheet" />
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" />
       <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
     <link href="../NewCSS/fastselect.min.css" rel="stylesheet" />
       <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script src="../Newjs/fastselect.standalone.js"></script>


    <style type="text/css">
        /*.k-grid-content {
            min-height:  300px;
            max-height: 400px;
        }
        */

       
       .modal-header h4.modal-title{
           margin-top:-11px;
       }
       .modal-header .close {
         margin-top: -16px;

        }


        .btn{
            font-size:14px;
            font-weight:400;
        }

        

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .btn-primary[disabled] {
            background-color: #d7d7d7;
            border-color: #d7d7d7;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .form-control{
            height: 32px;
        }

        .k-multiselect-wrap .k-input {
            padding-top: 6px;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
        .col-md-12 {
            text-align: center;
        }
        .k-pager-sizes {
            float: left;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;

        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
             cursor:pointer;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
          
        }

        .k-grid-header th.k-with-icon .k-link{
            text-align:center;
        }

        .inactiveclass {
        pointer-events:none;
        }


        .fstResultItem{
            text-align:left;
        }

        .checkbox label, .radio label{
            padding-left:0px;
        }
            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
                 cursor:pointer;
            }

        .k-grid, .k-listview {
            margin-top: 10px;
        }

            .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            }

        .k-tooltip-content {
            width: max-content;
        }

        a:focus {
        outline:none !important;
        }

      .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected{
          box-shadow:none !important; 
      }

      .pem{
            text-align:left;

      }

      input[type=checkbox], input[type=radio]{
          margin:4px 3px 2px -3px !important;
      }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-grid input.k-checkbox + label.k-checkbox-label{
            margin-right:15px;
        }

        .k-calendar-container {
            background-color: white;
            width: 217px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: 0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }


        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left: 2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y: scroll! important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            text-align:center;
            font-weight: 400;
        }
        .k-button.k-button-icon .k-icon, .k-grid-filter .k-icon, .k-header .k-icon{
            margin-right:10px;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

      

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
            text-align:left;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height: 34px;
        }

        .k-multiselect-wrap, .k-floatwrap {
            height: 34px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 1%;
            margin-bottom: 1%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
            color: #515967;
            padding-top: 5px;
        }

        .k-filter-row th, .k-grid-header th[data-index='10'] {
            text-align: center;
        }


        .fstElement {
            font-size: 0.7em;
            height: 34px;
        }

        .fstToggleBtn {
            min-width: 16.5em;
        }

        .submitBtn {
            display: none;
        }

        .fstMultipleMode {
            display: block;
        }

            .fstMultipleMode .fstControls {
                width: 100%;
            }
    </style>
 


          <style type="text/css">
              .k-checkbox-label:hover {
                  color: #1fd9e1;
              }

              .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
                  cursor: pointer;
                  background-color: transparent !important;
                  border-color: transparent;
                  color: #1fd9e1;
              }

              .btn {
                  font-weight: 400;
              }

              .k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
                  color: #2e2e2e;
                  background-color: #d8d6d6 !important;
              }

              .k-grid-content {
                  min-height: auto !important;
              }

              .k-multiselect-wrap .k-input {
                  padding-top: 6px;
              }

              .k-grid tbody .k-button {
                  min-width: 19px;
                  min-height: 25px;
                  background-color: transparent;
                  border: none;
                  margin-left: 0px;
                  margin-right: 0px;
                  padding-left: 2px;
                  padding-right: 2px;
                  border-radius: 25px;
              }

              .k-button.k-state-active, .k-button:active {
                  color: black;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li:hover {
                  color: white;
                  background-color: #1fd9e1;
                  border-top-left-radius: 10px;
                  border-top-right-radius: 10px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li {
                  margin-left: 5px !important;
                  margin-right: 5px !important;
              }

              .panel-heading .nav {
                  background-color: #f8f8f8;
                  border: none;
                  font-size: 11px;
                  margin: 0px 0px 0px 0;
                  border-radius: 10px;
              }

            .k-grid, .k-listview {
                  margin-top: 0px;
              }

                  .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                      -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                      box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                  }

              .k-tooltip-content {
                  width: max-content;
              }

              input[type=checkbox], input[type=radio] {
                  margin: 4px -2px 0;
                  margin-top: 1px\9;
                  line-height: normal;
              }

              .k-calendar-container {
                  background-color: white;
                  width: 217px;
              }

              .div.k-grid-footer, div.k-grid-header {
                  border-top-width: 1px;
                  margin-right: 0px;
                  margin-top: 0px;
              }


              .k-grid-footer-wrap, .k-grid-header-wrap {
                  position: relative;
                  width: 100%;
                  overflow: hidden !important;
                  border-style: solid;
                  border-width: 0 1px 0 0;
                  zoom: 1;
              }


              html {
                  color: #666666;
                  font-size: 15px;
                  font-weight: normal;
                  font-family: 'Roboto',sans-serif;
              }

              .k-checkbox-label, .k-radio-label {
                  display: inline;
              }

              .myKendoCustomClass {
                  z-index: 999 !important;
              }

              .k-header .k-grid-toolbar {
                  background: white;
                  float: left;
                  width: 100%;
              }

              .k-grid td {
                  line-height: 2.0em;
                  border-bottom-width: 1px;
                  background-color: white;
                  border-width: 0 1px 1px 0px;
              }

              .k-i-more-vertical:before {
                  content: "\e006";
              }

              .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                  background-color: #1fd9e1;
                  background-image: none;
                  background-color: white;
              }

              k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                  color: #000000;
                  border-color: #1fd9e1;
                  background-color: white;
              }

              /*#tblPrincipleEmployer .k-grid-toolbar {
            background: white;
        }*/

              .k-pager-wrap > .k-link > .k-icon {
                  margin-top: 0px;
                  margin-left: 2.7px;
                  color: inherit;
              }

              .toolbar {
                  float: left;
              }

              html .k-grid tr:hover {
                  background: white;
              }

              html .k-grid tr.k-alt:hover {
                  background: white;
              }


              .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                  margin-right: 0px;
                  margin-right: 0px;
                  margin-left: 0px;
                  margin-left: 0px;
              }

             

              .k-grid-header {
                  padding-right: 0px !important;
                  margin-right: 2px;
              }

              .k-filter-menu .k-button {
                  width: 27%;
              }

              .k-label input[type="checkbox"] {
                  margin: 0px 5px 0 !important;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  font-size: 15px;
                  background: #f8f8f8;
                  font-family: 'Roboto',sans-serif;
                  color: #2b2b2b;
                  font-weight: 400;
              }

              .k-primary {
                  border-color: #1fd9e1;
                  background-color: #1fd9e1;
              }

              .k-pager-wrap {
                  background-color: white;
                  color: #2b2b2b;
              }

              td.k-command-cell {
                  border-width: 0 0px 1px 0px;
                  text-align: center;
              }

              .k-grid-pager {
                  margin-top: -1px;
              }

              span.k-icon.k-i-calendar {
                  margin-top: 6px;
              }

              .col-md-2 {
                  width: 20%;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  border-width: 0px 0px 1px 0px;
                  background: #E9EAEA;
                  font-weight: bold;
                  margin-right: 18px;
                  font-family: 'Roboto',sans-serif;
                  font-size: 15px;
                  color: rgba(0, 0, 0, 0.5);
                  height: 20px;
                  vertical-align: middle;
              }

              .k-dropdown-wrap.k-state-default {
                  background-color: white;
                  height: 34px;
              }

              .k-multiselect-wrap, .k-floatwrap {
                  height: 34px;
              }

              .k-popup.k-calendar-container, .k-popup.k-list-container {
                  background-color: white;
              }

              .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
                  -webkit-box-shadow: inset 0 0 3px 1px white;
                  box-shadow: inset 0 0 3px 1px white;
              }

              label.k-label {
                  font-family: roboto,sans-serif !important;
                  color: #515967;
                  /* font-stretch: 100%; */
                  font-style: normal;
                  font-weight: 400;
                  min-width: max-content !important;
                  white-space: pre-wrap;
              }

              .k-multicheck-wrap .k-item {
                  line-height: 1.2em;
                  font-size: 14px;
                  margin-bottom: 5px;
              }

              label {
                  display: flex;
                  margin-bottom: 0px;
              }

              .k-state-default > .k-select {
                  border-color: #ceced2;
                  margin-top: 3px;
              }

              .k-grid-norecords {
                  width: 100%;
                  height: 100%;
                  text-align: center;
                  margin-top: 2%;
              }

              .k-grouping-header {
                  font-style: italic;
                  background-color: white;
              }

              .k-grid-toolbar {
                  background: white;
                  border: none;
              }

              .k-grid table {
                  width: 100.5%;
              }

              .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
                  color: #515967;
                  padding-top: 5px;
              }

              .k-filter-row th, .k-grid-header th[data-index='11'] {
                  text-align: center;
              }

              .k-i-filter-clear {
                  margin-top: -3px;
              }

              .k-button {
                  margin-top: 2.5px;
              }

              label.k-label:hover {
                  color: #1fd9e1;
              }

              .k-tooltip {
                  margin-top: 5px;
              }

              .k-column-menu > .k-menu {
                  background-color: white;
              }
     
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

        .select2-container{
            width:100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>

    <script type="text/javascript">

    

        var EmployeeMasterListData = [];

        $(document).ready(function () {
            $("#btnNewPrincipleEmployer").kendoTooltip({
                content: function (e) {
                    return "Add New Principle Employer";
                }
            });

            $("#btnSearch").kendoTooltip({
                content: function (e) {
                    return "Search";
                }
            });
            $("#btnSave").kendoTooltip({
                content: function (e) {
                    return "Search";
                }
            });

            

            $("#btnClear").kendoTooltip({
                content: function (e) {
                    return "Clear";
                }
            });
            




            var activeclass = $('#liPECreation');
            activeclass.addClass('active');

            $("#divCheckPE").hide();
        //    $(".tabdata").hide();
            $("#divPECreation").show();
            $("#divMsg").hide();
           // $("#divMsgPELocation").hide();
           // $("#divMsgContractor").hide();
            $("#divErrorsPE").hide();
           

         
           BindClientList();
          BindPrincipleEmployerTable("");

          var location = document.location.href;
          if (location != "") {
              
              var queryString = location.split('aspx')[1];
              if (queryString.includes("=")) {
                //  alert('ehy');
                  BindClientList();
                  var queryString = location.split('clientid=')[1];
                  var queryString = queryString.replace(/[^\w\s]/gi, '');

                  BindPrincipleEmployerTable(queryString);
             //     $("#ddlClient").select2.val(queryString).trigger("change");
                 // $("#ddlClient").select2().val(queryString).trigger("change");


              }
              else
              {
                //  alert('hi');

                  BindClientList();
                  BindPrincipleEmployerTable("");
              }

         
          }

      
    
            //$('#ModalPrincipleEmployer').on('hidden.bs.modal', function () {
            //    var a = window.parent.document.getElementsByClassName('pemt');
            //    a.liPELocationCreation.classList.remove('inactiveclass');
            //    a.liPEMaster.classList.remove('inactiveclass');
            //    a.liContractorDetails.classList.remove('inactiveclass');
            //    a.liPEReport.classList.remove('inactiveclass');


            //});

          

    

            $("#btnNewPrincipleEmployer").on("click", function (e) {
                $("#divMsg").hide();
                enablePrincipleEmployerTextBox("N");
                ClrEmployerCreation();
                // $("#divStatus").hide();

                if ($("#ddlClient").val() != "-1") {
                    $("#ModalPrincipleEmployer").modal("show");
                  

                  //var a=  window.parent.document.getElementsByClassName('pemt');
                  //a.liPELocationCreation.classList.add('inactiveclass');
                  //a.liPEMaster.classList.add('inactiveclass');
                  //a.liContractorDetails.classList.add('inactiveclass');
                  //a.liPEReport.classList.add('inactiveclass');

                  
                  

                    
                    $("#divCheckPE").hide();
                }
                else {
                    //alert("Please Select Client");
                    $("#divCheckPE").html("");
                    displayErrors("divCheckPE", "Please Select Entity");
                }

            });

            
            $("#ddlClient").on("change", function (e) {
                if ($("#ddlClient").val() != "-1") {

                    var url = "../CLRA/PrincipleEmployer.aspx?clientid=" + $("#ddlClient").val();
                    window.history.pushState({}, {}, url);
                    BindPrincipleEmployerTable($(this).val());
             
                //    BindPrincipleEmployerList();
                    $("#divCheckPE").html("");
                    $("#divCheckPE").hide();
                }
                else {
                    var url = "../CLRA/PrincipleEmployer.aspx";
                
                    window.history.pushState({}, {}, url);
                    BindPrincipleEmployerTable($(this).val());
                
                  //  BindPrincipleEmployerList();
                }

            });

          
           
            
            $("#btnSave").click(function (e) {
                if (ValidatePrincipleEmployer()) {
                    $("#btnSave").prop("disabled", true);
                    var id = $("#lblPEID").text();
                    //alert("id is "+id);//must comment
                    $("#divErrorsPE").hide();
                    var DetailsObj = {
                        ID: $("#lblPEID").text(),
                        ClientID: $("#ddlClient").val(),
                        PEName: $("#txtPEName").val(),
                        NatureOfBusiness: $("#txtNatureOfBusiness").val(),
                        //ContractFrom: $("#txtContractForm").val(),
                        //ContractTo: $("#txtContractTo").val(),
                        //NoOfEmployees: $("#txtNumberOfEmployees").val(),
                        //Address: $("#txtAddress").val(),
                        //Contractvalue: $("#txtControctalue").val(),
                        //SecurityDeposit: $("#txtSecurityDeposit").val(),
                        //IsCentralAct: ($("#chkCentral").is(":checked") ? 1 : 0),
                        ////,Status: ($("#chkStatus").is(":checked") ? "A" : "I")
                        //PE_PAN: $("#txtPEPAN").val()
                    }

                    //$.ajax({
                    //    type: "POST",
                    //    url: "/CLRA/PrincipleEmployer.aspx/GetPanDetails",
                    //    data: JSON.stringify({ DetailsObj }),
                    //    contentType: "application/json; charset=utf-8",
                    //    dataType: "json",
                    //    success: function (data) {
                    //        var Success = JSON.parse(data.d);
                    //        if (Success) {
                    //            $("#divErrorsPE").show();
                    //            $("#divMsg").hide();
                    //            $("#spanMsg").hide();
                    //            $("#divErrorsPE").html("PAN Already Exist...");
                    //                $("#btnSave").attr("disabled", true);
                               
                    //            setTimeout(function () {

                    //                $("#ModalPrincipleEmployer").modal("hide");
                    //                ClrEmployerCreation();
                    //                $("#btnSave").attr("disabled", false);
                    //            }, 3000);

                    //        }
                    //        else
                    //        {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployer.aspx/SavePrincipleEmployerDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#btnSave").removeAttr('disabled');
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                $("#divErrorsPE").hide();
                                $("#divMsg").show();
                                $('#txtPEName').val('');
                                $('#txtNatureOfBusiness').val('');

                                
                                $("#spanMsg").show();
                                if (id == "0") {
                                    $("#spanMsg").html("Principle Employer Saved Successfully.");
                                    $("#btnSave").attr("disabled", true);
                                }
                                else {
                                    $("#spanMsg").html("Principle Employer Updated Successfully.");
                                    $("#btnSave").attr("disabled", true);
                                }


                                setTimeout(function () {

                                    $("#ModalPrincipleEmployer").modal("hide");
                                   // ClrEmployerCreation();
                                    $("#btnSave").attr("disabled", false);
                                }, 3000);

                            }
                            else {
                                alert("Please Enter Unique Principle Employer Name");
                            }

                       //     BindPrincipleEmployerList();
                            BindPrincipleEmployerTable($("#ddlClient").val());
                            var clientid = $("#ddlClient").val();
                          //  alert(clientid)
                            if (clientid != "" && clientid != -1 && clientid != "-1") {
                           //     alert(clientid);
                               var url = "../CLRA/PrincipleEmployer.aspx?clientid=" + clientid;
                               window.history.pushState({}, {}, url);
                            }
                            //alert("he;l;op");
                            window.location.reload(true);
                        },
                        failure: function (data) {
                            alert(data);
                        },
                        error: function (error) {

                        }
                    });
                    //}
                            
                }
                //});
            });
            

            $(document).on("click", "#tblPrincipleEmployer tbody tr .Update", function (e) {
                $("#divErrorsPE").hide();
                $("#divMsg").hide();
                ClrEmployerCreation();
                enablePrincipleEmployerTextBox("E");
                var item = $("#tblPrincipleEmployer").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblID").text(item.ID);
                var DetailsObj = {
                    ID: item.ID
                }
                $.ajax({
                    type: "POST",
                    url: "/CLRA/PrincipleEmployer.aspx/GetPrincipleEmployerDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var EmployerDetails = JSON.parse(data.d);
                        if (EmployerDetails.length > 0) {
                            $("#lblPEID").text(EmployerDetails[0].ID);
                            $("#txtPEName").val(EmployerDetails[0].PEName);
                            $("#txtNatureOfBusiness").val(EmployerDetails[0].NatureOfBusiness);

                            const StartdateTime = EmployerDetails[0].ContractFrom;
                            //const parts = StartdateTime.split(/[- :]/);
                            const parts = StartdateTime.split(/[// :]/);
                            const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                            //$("#txtContractForm").val(FromDate); 

                            $("#txtContractForm").datepicker("setDate", FromDate);

                            const EnddateTime = EmployerDetails[0].ContractTo;
                            //const parts1 = EnddateTime.split(/[- :]/);
                            const parts1 = EnddateTime.split(/[// :]/);
                            const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                            //$("#txtContractTo").val(ToDate);
                            $("#txtContractTo").datepicker("setDate", ToDate);

                            $("#txtNumberOfEmployees").val(EmployerDetails[0].NoOfEmployees);
                            $("#txtAddress").val(EmployerDetails[0].Address);
                           // $("#txtControctalue").val(EmployerDetails[0].Contractvalue);
                           // $("#txtSecurityDeposit").val(EmployerDetails[0].SecurityDeposit);
                            debugger;
                            if (EmployerDetails[0].IsCentralAct != "False")
                                $("#chkCentral").prop("checked", true);
                            else
                                $("#chkCentral").prop("checked", false);

                            //if (EmployerDetails[0].Status == "A")
                            //    $("#chkStatus").prop("checked", true);
                            //else
                            //    $("#chkStatus").prop("checked", false);

                            //$("#divStatus").show();
                            $("#txtPEPAN").val(EmployerDetails[0].PE_PAN);
                            $("#ModalPrincipleEmployer").modal("show");
                            //OpenEditPrincipleEmployerPopup();
                        }
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });




           

            $(document).on("click", "#tblPrincipleEmployer tbody tr .Delete", function (e) {
                var item = $("#tblPrincipleEmployer").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblPEID").text(item.ID);
                //alert("item id is " + item.ID);

                var res = confirm("Are you sure you want to delete this Principle Employer?");
                if (res == true) {
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployer.aspx/DeletePrincipleEmployer",
                        //data: JSON.stringify({ DetailsObj }),
                        data: '{peid:' + item.ID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var result = JSON.parse(data.d);

                            if (result == true) {
                                alert("Principle Employer Deleted Successfully");
                            //    BindPrincipleEmployerList();
                                BindPrincipleEmployerTable($("#ddlClient").val());
                            }
                            else
                                alert("Server error occured. Please try again");

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }




            });



            $("#btnSearch").click(function (e) {
                e.preventDefault();

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#tblPrincipleEmployer").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "PEName", operator: "contains", value: $x },
                              { field: "NatureOfBusiness", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#tblPrincipleEmployer").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindGrid();
                }
                // return false;
            });

            
            $("#btnClear").click(function (e) {
                e.preventDefault();
                $("#txtSearch").val('');

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#tblPrincipleEmployer").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "PEName", operator: "contains", value: $x },
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#tblPrincipleEmployer").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindGrid();
                }

            });

           
            $('#txtSearch').keyup(function () {

                if ($(this).val()) {
                    $('#btnClear').show();
                }
                else {
                    $('#btnClear').hide();
                }
            });
  

        });

        function enablePrincipleEmployerTextBox(flag) {
            if (flag == "N")
                document.getElementById('txtPEName').disabled = false;
            else {
                document.getElementById('txtPEName').disabled = true;
            }
        }


        function PELocationCreation() {
            var clientid = $("#ddlClient").val();
            if (clientid != "" && clientid != -1 && clientid != "-1") {
                window.location.href = "../CLRA/PrincipleEmployerLocationCreation.aspx?clientid=" + clientid;
                } else
                    alert('Select Entity ID');
        
        }

        function PEMaster() {
            var clientid = $("#ddlClient").val();
            if (clientid != "" && clientid != -1 && clientid != "-1") {
                window.location.href = "../CLRA/PrincipleEmployeeMasterCreation.aspx?clientid=" + clientid;
            } else
                alert('Select Entity ID');

        }

        function ContractorDetails() {
            var clientid = $("#ddlClient").val();
            if (clientid != "" && clientid != -1 && clientid != "-1") {
                window.location.href = "../CLRA/PrincipleEmployerContractorCreation.aspx?clientid=" + clientid;
            } else
                alert('Select Entity ID');

        }

        function CLRAactivationpage() {
            var clientid = $("#ddlClient").val();
            if (clientid != "" && clientid != -1 && clientid != "-1") {
                window.location.href = "../CLRA/CLRA_Assignment_Activation.aspx?clientid=" + clientid;
            } else
                alert('Select Entity ID');

        }

        

       

        function ClrEmployerCreation() {
            //$("#lblPEID").val("0");
            $("#lblPEID").text("0");
         
            $("#txtPEName").val("");
            $("#txtNatureOfBusiness").val("");
      
            $("#divErrorsPE").hide();
       
        }

        
     
        function BindClientList(mode) {
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployer.aspx/BindClientList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //beforeSend: function () {
                //},
                success: function (data) {
                    //var Customer = JSON.parse(data.d);
                    //if (Customer.length > 0)
                    //{
                    //    var controlId = "ddlClient";
                    //    if (mode == "R")//R stands for Report
                    //        controlId = "ddlClientReport"

                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select Client"));
                    //    $.each(Customer, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.ClientID).html(value.ClientName));
                    //    })
                    //    $("#" + controlId).select2();
                    //}


                    var controlId = "ddlClient";
                    // if (mode == "R")//R stands for Report
                    //    controlId = "ddlClientReport"

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Entity"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.ClientID).html(value.UniqueName));
                        })

                    }
                    $("#" + controlId).select2();

                    var location = document.location.href;
                    var queryString = location.split('clientid=')[1];
                    var queryString = queryString.replace(/[^\w\s]/gi, '');
                   // alert(queryString);

                    $("#ddlClient").select2().val(queryString).trigger("change");
                    $("#ddlClient").select2().val(queryString).trigger("change");
                    BindPrincipleEmployerTable($("#ddlClient").val());
                    //$("#ddlClient").val(queryString);

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

     
       
        function BindPrincipleEmployerTable(clntid) {
            var DetailsObj = {
                ClientID: clntid
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployer.aspx/BindPrincipleEmployerTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);

                    $("#tblPrincipleEmployer").kendoGrid({

                        dataSource: {
                            data: AssignCheckList,
                            serverPaging: false,
                           
                            batch: true,
                           
                            //schema: {
                            //    data: function (response) {                            
                            //        if (response != null && response != undefined)                                
                            //            return response.Result;
                            //    },
                            //    total: function (response) {                            
                            //        if (response != null && response != undefined)
                            //            if (response.Result != null && response.Result != undefined)
                            //                return response.Result.length;
                            //    }
                            //}
                        },
                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        pageable: {
                            refresh: false,
                            pageSize: 10,
                            pageSizes: true,
                            buttonCount: 3,
                        },
                        height:475,
                        dataBinding: function () {
                            //record = 0;
                            //var total = this.dataSource._pristineTotal;
                            //if (this.dataSource.pageSize() == undefined) {
                            //    this.dataSource.pageSize(total);
                            //    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                            //}
                            //else {
                            //    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                            //}
                        },
                        reorderable: true,
                        resizable: true,
                        multi: true,
                        selectable: true,
                        columns: [

                                { field: "EntityID", title: 'Entity',
                                    width: "30%;",
                                    attributes: {

                                        style: 'white-space: nowrap;'

                                    }, filterable: {
                                        extra: false,
                                        multi: true,
                                        search: true,
                                        operators: {
                                            string: {
                                                eq: "Is equal to",
                                                neq: "Is not equal to",
                                                contains: "Contains"
                                            }
                                        }
                                    }
                                },
                                 {
                                     field: "Customername", title: 'Customer Name',
                                     width: "23%;",
                                     attributes: {

                                         style: 'white-space: nowrap;'

                                     }, filterable: {
                                         extra: false,
                                         multi: true,
                                         search: true,
                                         operators: {
                                             string: {
                                                 eq: "Is equal to",
                                                 neq: "Is not equal to",
                                                 contains: "Contains"
                                             }
                                         }
                                     }
                                 },
                                { hidden: true, field: "NoOfEmployees", menu: false },
                                { hidden: true, field: "Contractvalue", menu: false },
                                { hidden: true, field: "SecurityDeposit", menu: false },
                                { hidden: true, field: "ID", menu: false },

                            {
                                field: "PEName", title: 'Principle Employer',
                                width: "20%;",
                                attributes: {

                                    style: 'white-space: nowrap;'

                                }, filterable: {
                                    extra: false,
                                    multi: true,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            {
                                field: "NatureOfBusiness", title: 'Nature Of Business',
                                width: "16%",
                                attributes: {

                                    style: 'white-space: nowrap;'
                                },
                                filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            //{
                            //    field: "Status", title: 'Status',
                            //    width: "10%;",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'

                            //    }, filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "ContractFrom", title: 'Contract From',
                            //    width: "15%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'
                            //    },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "ContractTo", title: 'Contract To',
                            //    width: "13%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'
                            //    },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "Address",
                            //    title: 'Address',
                            //    width: "11%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'
                            //    },
                            //    filterable: {

                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},
                            //{
                            //    field: "IsCentralAct",
                            //    title: 'Is CentralAct',
                            //    width: "14%",
                            //    attributes: {

                            //        style: 'white-space: nowrap;'

                            //    },
                            //    filterable: {
                            //        extra: false,
                            //        multi: true,
                            //        search: true,
                            //        operators: {
                            //            string: {
                            //                eq: "Is equal to",
                            //                neq: "Is not equal to",
                            //                contains: "Contains"
                            //            }
                            //        }
                            //    }
                            //},

                            {
                                command:
                                    [
                                        { name: "UpdatePrincipleEmployer", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                        { name: "DeletePrincipleEmployer", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                    ], title: "Action", lock: true, width: "10%;",
                                attributes: {

                                    style: 'white-space: nowrapx;text-align:center;'
                                },
                            }
                        ],
                        dataBound: function () {
                            var rows = this.items();
                            $(rows).each(function () {
                                var index = $(this).index() + 1
                                    + ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.page() - 1));
                                var rowLabel = $(this).find(".row-number");
                                $(rowLabel).html(index);
                            });
                        }
                    });

                
                 
                    /* $("#tblPrincipleEmployer").kendoGrid({
 
                         dataSource: {
                             data: AssignCheckList,
                             serverPaging: false,
 
                             batch: true,
                             pageSize: 10,
 
                         },
                         //height: 300,
                         sortable: true,
                         filterable: true,
                         columnMenu: true,
                         pageable: {
                             refresh: true,
                             buttonCount: 3,
                             change: function (e) {
                             }
                         },
                         reorderable: true,
                         resizable: true,
                         multi: true,
                         selectable: true,
                         noRecords: {
                             template: function (e) {
                                 return "No data available";
                             }
                         },
 
 
                         columns: [
                             { hidden: true, field: "ClientID" },
                             { hidden: true, field: "NoOfEmployees" },
                             { hidden: true, field: "Contractvalue" },
                             { hidden: true, field: "SecurityDeposit" },
                             { hidden: true, field: "ID" },
                             { field: "PEName", title: "PE Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "NatureOfBusiness", title: "Nature Of Business", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "ContractFrom", title: "Contract From", width: "20%" },
                             { field: "ContractTo", title: "Contract To", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "Address", title: "Address", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "IsCentralAct", title: "Central", width: "15%", attributes: { style: 'white-space: nowrap;' } },
                              {
                                  title: "Action", lock: true, width: "10%;",
                                  command:
                                      [
                                             { name: "UpdatePrincipleEmployer", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                             { name: "DeletePrincipleEmployer", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
 
                                      ]
                              }
                         ],
 
                         dataBound: function () {
                             var rows = this.items();
                             $(rows).each(function () {
                                 var index = $(this).index() + 1
                                     + ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.pageSize() * ($("#tblPrincipleEmployer").data("kendoGrid").dataSource.page() - 1));
                                 var rowLabel = $(this).find(".row-number");
                                 $(rowLabel).html(index);
                             });
 
                             $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                             $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                             $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                         }
 
 
 
                     });*/

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }





        function ValidatePrincipleEmployer() {
            //var fromDate = $("#txtContractForm").val();
            //fromDate = new Date(fromDate);

            //var toDate = $("#txtContractTo").val();
            //toDate = new Date(toDate);





            var status = true;
            $("#divErrorsPE").html("");
            $("#divErrorsPE").addClass('alert-danger');

            //if ($("#txtControctalue").val() == "")
            //    $("#txtControctalue").val("0");

            //if ($("#txtSecurityDeposit").val() == "")
            //    $("#txtSecurityDeposit").val("0");


            if ($("#txtPEName").val() == "") {
                displayErrors("divErrorsPE", "Please Enter Principle Employer Name");
                status = false;

            }
            if ($("#txtNatureOfBusiness").val() == "") {
                displayErrors("divErrorsPE", "Please Enter Nature Of Business");
                status = false;
            }
            //if ($("#txtContractForm").val() == "") {
            //    displayErrors("divErrorsPE", "Please Select Contract From");
            //    status = false;
            //}
            //if ($("#txtContractTo").val() == "") {
            //    displayErrors("divErrorsPE", "Please Select Contract To");
            //    status = false;
            //}
            //if ($("#txtNumberOfEmployees").val() == "") {
            //    displayErrors("divErrorsPE", "Please Enter Number Of Employee");
            //    status = false;
            //}
            //if ($("#txtPEPAN").val() == "") {
            //    displayErrors("divErrorsPE", "Please Enter PE PAN");
            //    status = false;
            //}
            //if ($("#txtPEPAN").val() != "") {
            //    var val = $("#txtPEPAN").val()
            //    if (/[A-Z]{5}[0-9]{4}[A-Z]{1}$/.test(val)) {
            //    } else {
            //        displayErrors("divErrorsPE", "Please Enter valid PE PAN");
            //        status = false;
            //    }
            //}
            //if ($("#txtAddress").val() == "") {
            //    displayErrors("divErrorsPE", "Please Enter Address");
            //    status = false;
            //}
            return status;
        }


        function displayErrors(control, msg) {
            $("#" + control).append(msg);
            $("#" + control).append("</br>");
            $("#" + control).show();
        }

        






    </script>
  
<script type="text/javascript">





    $(document).ready(function () {

      

        $('#btnClear').hide();
     
     

        //$("#search_code").live('change', function () {
        //    alert(this.value)
        //});
        //$("#divCheckPE").hide();
        //$(".tabdata").hide();
        //$("#divPECreation").show();
        //$("#divMsg").hide();
        //$("#divMsgPELocation").hide();
        //$("#divMsgContractor").hide();
        //$("#divErrorsPE").hide();
        //$("#divErrorsPELocation").hide();
        //$("#divErrorsContractor").hide();
        //$("#divErrorsEmployees").hide();
       
        //$("#divCheckPELocation").hide();
        //$("#divCheckEmployees").hide();
        //$("#divCheckContractor").hide();


    

     //  var item = parent.document.getElementById("liPECreation");
      //  item.classList.add("active");

     

      


    });

    



</script>

  

   
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--</head>--%> 


        <%--<form runat="server" id="formvalidation">--%>
            <%--  <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
    <!-- partial:index.partial.html -->
    
  <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li id="liPECreation" class="listItem pemt active"> <%--class="active"--%>
                       <a href="../CLRA/PrincipleEmployer.aspx" onclick="<%--PECreation();--%>">Principle Employer</a>
                    </li>
                      <li id="liPELocationCreation" class="listItem pemt">
                         <a data-href="#" onclick="PELocationCreation();">Principle Employer Location</a>
                    
                    </li>
                   <li id="liPEMaster" class="listItem pemt">
                             <a data-href="#" onclick="PEMaster();">Principle Employee Master</a>                 
                    </li>
                    <li id="liContractorDetails" class="listItem pemt">
                             <a data-href="#" onclick="ContractorDetails();">Contractor Details</a>                 
                    </li>
                    <li id="liPEReport" class="listItem pemt">
                             <a href="#" onclick="CLRAactivationpage();">Compliance Assignment & Activation</a>    
                             <%--<input type="text" id="txtRohan" value="checkR" />    --%>         
                    </li>
                   
                    <%--<li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                        <input type="text" id="txtRohan" value="check" />
                    </li>--%>
                </ul>
            </header>
        </div>
    <!--PEN CONTENT     -->
    <div class="contentx">
        <!--content inner-->
        <div class="content__inner">
            <div class="container">
            </div>
            <div class="containerx overflow-hidden">
                <!--multisteps-form-->
                <!--form panels-->
                <div class="row">
                    <div class="col-12 col-lg-12 m-auto">
                       <%--   <form class="multisteps-form__form">
                          form--%>
                            <!--single form panel-->
                            <div id="divPECreation"  data-animation="scaleIn">
                                <div class="" style="margin-top: 25px;">
                                    <div class="row alert alert-danger" id="divCheckPE">
                                        <div class="col-md-12">
                                            <div>
                                                <strong></strong><span></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <label style="color: black;">Select Entity</label>
                                            <select class="form-control" id="ddlClient">
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group" style="margin-top: 20px;">
                                            <input class="k-textbox" type="text" id="txtSearch" style="width: 70%; height: 36px" placeholder="Type to search" autocomplete="off" />
                                            <button id="btnSearch" class="btn btn-primary" title="Search" style="height: 36px; font-weight: 400;margin-left:10px;">Search</button>
                                        </div>
                                        
                                           
                                            
                                        
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group" style="margin-left:-47px;">
                                            <%--<button class="btn btn-primary" type="button" id="btnNewPrincipleEmployer" title="Add New Principle Employer" style="margin-top: 20px;">Add New Principle Employer</button>
                                            <button id="btnClear" class="btn btn-primary" style="height: 36px; font-weight: 400;margin-left:10px;margin-top: 20px;">Clear</button>--%>
                                           <button id="btnClear" class="btn btn-primary" title="Clear Filter" style="height: 36px; font-weight: 400;margin-left:10px;margin-top: 20px;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span><font size="2" style="zoom:1.1">Clear Filter</font></button>
                                              <button class="btn btn-primary" type="button" id="btnNewPrincipleEmployer" title="Add New Principle Employer" style="margin-top: 20px; margin-left:10px; height: 36px; font-weight: 400;"><span class="k-icon k-i-plus-outline"></span>Add New</button>
                                        </div>
                                    </div>

                                    <%-- <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <div id="tblPrincipleEmployer" style="height:300px;"></div>
                                            </div>
                                        </div>--%>

                                    <div class="row colpadding0">
                                        <div class="col-md-12">
                                            <div id="tblPrincipleEmployer">
                                                <div class="k-header k-grid-toolbarx">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        
                        <%--form ends--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <%-- </div>--%>  <%--13-10-2020--%>
    <!-- partial -->


    <div class="modal  modal-fullscreen pem" id="ModalPrincipleEmployer" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" id="divValidation" style="height: 240px; width: 710px; margin-left: -650px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Principle Employer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" id="divMsg">
                            <div class="col-md-12">
                                <div id="divMsgPanel" class="alert alert-success">
                                    <strong></strong><span id="spanMsg"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row alert" id="divErrorsPE">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span id="spanErrorsPE"></span>
                                </div>
                            </div>
                        </div>

                        <%--<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Name</label>
                                <span style="color: red;">*</span>
                                <input type="text" id="txtPEName" class="form-control" />
                                <label style="color: black;" id="lblPEID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business</label>
                                <span style="color: red;">*</span>
                                <input type="text" id="txtNatureOfBusiness" class="form-control" />
                            </div>
                        </div>--%>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Name <span style="color: red;">*</span></label>

                                <input type="text" id="txtPEName" class="form-control" />
                                <label style="color: black;" id="lblPEID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business <span style="color: red;">*</span></label>

                                <input type="text" id="txtNatureOfBusiness" class="form-control" />
                            </div>
                        </div>
                      
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
    

<%--</form>--%>


    </asp:Content>
<%--</html>--%>
    
    
    