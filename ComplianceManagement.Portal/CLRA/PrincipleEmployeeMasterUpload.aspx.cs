﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.CLRA
{
    public partial class PrincipleEmployeeMasterUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            spanErrors.Visible = false;

            if(Request.QueryString["clientid"]!=null)
            {
                hdClientId.Value = Request.QueryString["clientid"];
            }
            
        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";
            if (ContractFileUpload.HasFile && Path.GetExtension(ContractFileUpload.FileName).ToLower() == ".xlsx")
            {
                try
                {
                    if (!Directory.Exists(Server.MapPath("~/CLRA/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/Uploaded/"));
                    }

                    string filename = Path.GetFileName(ContractFileUpload.FileName);
                    ContractFileUpload.SaveAs(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "PrincipleEmployeeMasterBulkUplo");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'PrincipleEmployeeMasterBulkUplo'.";
                                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["PrincipleEmployeeMasterBulkUplo"];

                if (xlWorksheet != null)
                {
                    List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    if (xlrow2 > 1)
                    { 
                        string ErrorFileName = "ErrorFile_PrincipleEmployeeUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";

                    hdFileName.Value = ErrorFileName;

                    #region Validations
                    //using (ComplianceDBEntities db = new ComplianceDBEntities())
                    //{


                    ErrorMessage objError = null;
                    string clientid = hdClientId.Value;
                    for (int i = 2; i <= xlrow2; i++)
                    {


                        // int principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                        string employeeId = xlWorksheet.Cells[i, 2].Text.Trim();
                        string stateId = xlWorksheet.Cells[i, 3].Text.Trim();
                        string locationId = xlWorksheet.Cells[i, 4].Text.Trim();
                        string branchId = xlWorksheet.Cells[i, 5].Text.Trim();
                        string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();
                        string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();

                        DateTime peContractFrom = new DateTime();
                        DateTime peContractTo = new DateTime();


                        bool IsDuplicateEmployee = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 2, employeeId);
                        if (IsDuplicateEmployee == true)
                        {
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Employee Id " + employeeId + " already exists in the uploaded excel file. Check at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }
                        //errorMessage.Add("Employee Id "+ employeeId + " already exists in the uploaded excel file. Check at Row " + i);

                        int principleEmployerId = 0;
                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                        {
                            try
                            {
                                principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                                //var principleEmplyr = db.RLCS_PrincipleEmployerMaster.Where(t => t.ClientID == clientid && t.Status == "A" && t.PEID == principleEmployerId).FirstOrDefault();
                                var principleEmplyr = RLCS_ClientsManagement.GetPrincipleEmployer(principleEmployerId.ToString(), "PE").Where(t => t.ClientID == clientid && t.Status == "A").FirstOrDefault();
                                if (principleEmplyr == null)
                                {
                                    //errorMessage.Add("Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row " + i);


                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                    principleEmployerId = 0;
                                }
                                else
                                {
                                    try
                                    {
                                        DateTime dtFm = Convert.ToDateTime(principleEmplyr.ContractFrom);
                                        string strDtFm = dtFm.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        peContractFrom = dtFm;

                                        DateTime dtTo = Convert.ToDateTime(principleEmplyr.ContractTo);
                                        string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        peContractTo = dtTo;
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                    ////peContractFrom = Convert.ToDateTime(principleEmplyr.ContractFrom);
                                    //    peContractFrom = Convert.ToDateTime(principleEmplyr.ContractFrom, CultureInfo.InvariantCulture);
                                    //    peContractTo = Convert.ToDateTime(principleEmplyr.ContractTo, CultureInfo.InvariantCulture);
                                }

                            }
                            catch (Exception ee)
                            {
                                //errorMessage.Add("Principle Employer Id should be integer. Check at row number" + i + ".");
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Principle Employer Id should be integer. Check at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }
                        }
                        else
                        {
                            //errorMessage.Add("Required Principle Employer Id at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required Principle Employer Id at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }


                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                        {
                            //var emp = db.RLCS_Employee_Master.Where(t => t.EM_ClientID == clientid && t.EM_EmpID == employeeId && t.EM_Status == "A").FirstOrDefault();

                            int customerid = RLCS_ClientsManagement.GetCustomerIDByClientID(clientid);
                            bool emp = RLCS_ClientsManagement.CheckEmployeeID(employeeId, customerid, clientid);

                            if (!emp)
                            {
                                //errorMessage.Add("Employee Id " + employeeId + " Does Not Exist for the Selected Client. Check at Row " + i);
                                objError = new ErrorMessage();
                                objError.ErrorDescription = "Employee Id " + employeeId + " Does Not Exist for the Selected Client. Check at Row ";
                                objError.RowNum = i.ToString();
                                lstErrorMessage.Add(objError);
                            }

                        }
                        else
                        {
                            //errorMessage.Add("Required Employee Id at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required Employee Id at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }


                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                        {
                            if (principleEmployerId > 0)
                            {
                                //var state = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.State == stateId && t.Status == "A").FirstOrDefault();
                                var state = RLCS_ClientsManagement.GetPrincipleEmployerLocation(principleEmployerId, stateId, "").Where(t => t.Status == "A").FirstOrDefault();
                                if (state == null)
                                {
                                    //errorMessage.Add("State " + stateId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "State " + stateId + " Does Not Exist for the Selected Principle Employer. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);

                                }

                            }
                        }
                        else
                        {
                            //errorMessage.Add("Required StateId at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required StateId at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }


                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                        {
                            if (principleEmployerId > 0)
                            {
                                // var loc = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId &&  t.State == stateId && t.Location == locationId && t.Status == "A").FirstOrDefault();
                                var loc = RLCS_ClientsManagement.GetPrincipleEmployerLocation(principleEmployerId, stateId, locationId).Where(t => t.Status == "A").FirstOrDefault();
                                if (loc == null)
                                {
                                    //errorMessage.Add("Location " + locationId + " Does Not Exist Under State " + stateId + " for the Selected Principle Employer. Check at Row " + i);
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Location " + locationId + " Does Not Exist Under State " + stateId + " for the Selected Principle Employer. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                            }
                        }
                        else
                        {
                            //errorMessage.Add("Required LocationId at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required LocationId at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }


                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                        {
                            if (principleEmployerId > 0)
                            {
                                //var branch = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.State == stateId && t.Location == locationId && t.Branch == branchId && t.Status == "A").FirstOrDefault();
                                //var branch = RLCS_ClientsManagement.GetPrincipleEmployerLocation(principleEmployerId, stateId, locationId).Where(t => t.Branch == branchId && t.Status == "A").FirstOrDefault();
                                try
                                {
                                    int bid = Convert.ToInt32(branchId);
                                    var branch = RLCS_ClientsManagement.GetPrincipleEmployerLocation(principleEmployerId, stateId, locationId).Where(t => t.PELID == bid && t.Status == "A").FirstOrDefault();
                                    if (branch == null)
                                    {
                                        //errorMessage.Add("Branch Id " + branchId + " Does Not Exist Under State " + stateId + " Under location " + locationId + " for the Selected Principle Employer. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Branch Id " + branchId + " Does Not Exist Under State " + stateId + " Under location " + locationId + " for the Selected Principle Employer. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                                catch (Exception ee)
                                {
                                    //errorMessage.Add("Branch Id should be integer" + i + ".");
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Branch Id should be integer. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                            }
                        }
                        else
                        {
                            //errorMessage.Add("Required BranchId at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required BranchId at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }


                        DateTime dateContractFrom = new DateTime();
                        DateTime dateContractTo = new DateTime();
                        bool invalidDates = false;

                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                        {
                            if (principleEmployerId > 0)
                            {
                                try
                                {
                                    dateContractFrom = Convert.ToDateTime(contractFrom, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                }
                                catch (Exception ex)
                                {
                                    //errorMessage.Add("Enter valid Contract From Date in dd-mm-yyyy format. Check at Row " + i);
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Enter valid Contract From Date in dd-mm-yyyy format. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                    invalidDates = true;
                                }
                            }
                        }
                        else
                        {
                            //errorMessage.Add("Required Contract From Date at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required Contract From Date at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }



                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                        {
                            if (principleEmployerId > 0)
                            {
                                try
                                {
                                    dateContractTo = Convert.ToDateTime(contractTo, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                }
                                catch (Exception ex)
                                {
                                    //errorMessage.Add("Enter valid Contract To Date in dd-mm-yyyy format. Check at Row " + i);
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Enter valid Contract To Date in dd-mm-yyyy format. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                    invalidDates = true;
                                }
                            }
                        }
                        else
                        {
                            //errorMessage.Add("Required Contract To Date at row number" + i + ".");
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Required Contract To Date at Row ";
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }


                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()) && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                        {
                            if (!invalidDates && principleEmployerId > 0)//if valid dates then only go ahead and check
                            {
                                if (dateContractFrom > dateContractTo)
                                {
                                    //errorMessage.Add("Contract From should be less than Contract To. Check at Row " + i);
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Contract From Date should be less than Contract To Date. Check at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }

                                else
                                {
                                    //if (dateContractFrom < peContractFrom || dateContractFrom > peContractTo)
                                            if (dateContractFrom < peContractFrom )
                                            {
                                        //errorMessage.Add("Contract From Date Should be between Contract From Date and Contract To Date Of the selected Principle Employer. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Contract From Date Should be between Contract From Date and Contract To Date Of the selected Principle Employer. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }


                                    //if (dateContractTo < peContractFrom || dateContractTo > peContractTo)
                                    if (dateContractTo < peContractFrom )
                                    {
                                        //errorMessage.Add("Contract To Date Should be between Contract From Date and Contract To Date Of the selected Principle Employer. Check at Row " + i);
                                        objError = new ErrorMessage();
                                        objError.ErrorDescription = "Contract To Date Should be between Contract From Date and Contract To Date Of the selected Principle Employer. Check at Row ";
                                        objError.RowNum = i.ToString();
                                        lstErrorMessage.Add(objError);
                                    }

                                }
                            }
                        }
                        //lstErrorMessage.Add(objError);


                    }

                    if (lstErrorMessage.Count == 0)
                    {
                        string UserID = Portal.Common.AuthenticationHelper.UserID.ToString();
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            int principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            string employeeId = xlWorksheet.Cells[i, 2].Text.Trim();
                            string stateId = xlWorksheet.Cells[i, 3].Text.Trim();
                            string locationId = xlWorksheet.Cells[i, 4].Text.Trim();
                            string branchId = xlWorksheet.Cells[i, 5].Text.Trim();
                            string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();
                            string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();

                            DateTime dateContractFrom = new DateTime();
                            DateTime dateContractTo = new DateTime();

                            //dateContractFrom = Convert.ToDateTime(contractFrom, CultureInfo.InvariantCulture);
                            //DateTime dtFrom = Convert.ToDateTime(contractFrom);
                            //string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            //dateContractFrom = Convert.ToDateTime(strDtFrom);

                            //dateContractTo = Convert.ToDateTime(contractTo, CultureInfo.InvariantCulture);
                            //DateTime dtTo = Convert.ToDateTime(contractTo);
                            //string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            //dateContractTo = Convert.ToDateTime(strDtTo);

                            //ADDED GG
                            dateContractFrom = Convert.ToDateTime(contractFrom, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                            dateContractTo = Convert.ToDateTime(contractTo, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                            int pelid = Convert.ToInt32(branchId);
                            List<string> lstEmpid = new List<string>();
                            lstEmpid.Add(employeeId);

                            saveSuccess = RLCS_ClientsManagement.SaveMigratedEmployees(clientid, pelid, lstEmpid, dateContractFrom, dateContractTo, UserID);

                            /*RLCS_PrincipleEmployeeMaster objEmployee = db.RLCS_PrincipleEmployeeMaster.Where(t => t.EmpID == employeeId && t.Status == "A").FirstOrDefault();
                            if (objEmployee != null)
                            {
                                int version = Convert.ToInt32(objEmployee.Version);
                                version++;

                                objEmployee.PELID = pelid;
                                objEmployee.ContractFrom = dateContractFrom;
                                objEmployee.ContractTo = dateContractTo;//
                                objEmployee.Status = "A";                                    
                                objEmployee.ModifiedBy = UserID.ToString();
                                objEmployee.ModifiedDate = DateTime.Now;
                                objEmployee.Version = version;
                                db.SaveChanges();
                            }
                            else
                            {
                                RLCS_PrincipleEmployeeMaster EmployeeMaster = new RLCS_PrincipleEmployeeMaster()
                                {
                                    EmpID = employeeId,
                                    PELID = pelid,
                                    ContractFrom = dateContractFrom,
                                    ContractTo = dateContractTo,
                                    Status = "A",
                                    ContractEndDate = null,
                                    ReasonForContractEnd = null,
                                    CreatedBy = UserID.ToString(),// "Admin",
                                    CreatedDate = DateTime.Now,
                                    Version = 1
                                };
                                db.RLCS_PrincipleEmployeeMaster.Add(EmployeeMaster);
                                db.SaveChanges();
                            }*/
                        }

                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = " Details Uploaded Successfully";
                        vsUploadUtility.CssClass = "alert alert-success";
                    }
                    else
                        GeneratePrincipleEmployeeUploadErrorCSV(lstErrorMessage, ErrorFileName);
                    //ErrorMessages(errorMessage);

                    //}
                    #endregion
                }
                else
                {
                        //string ErrorFileName = "ErrorFile_PrincipleEmployeeUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                        //string i = "2";
                        //ErrorMessage objError = null;
                        //objError = new ErrorMessage();
                        //objError.ErrorDescription = "File is empty";
                        //objError.RowNum = i.ToString();
                        //lstErrorMessage.Add(objError);
                        //GeneratePrincipleEmployeeUploadErrorCSV(lstErrorMessage, ErrorFileName);
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Empty File Uploaded. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        public void GeneratePrincipleEmployeeUploadErrorCSV(List<ErrorMessage> detailView, string FileName)
        {

            try
            {
                //cvUploadUtilityPage.IsValid = false;
                //cvUploadUtilityPage.ErrorMessage = "Uploaded file contains some errors. Please check the downloaded error file.";
                spanErrors.Visible = true;

                if (!Directory.Exists(Server.MapPath("~/CLRA/TempEmpError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/CLRA/TempEmpError/"));
                }

                string FilePath = Server.MapPath("~/CLRA/TempEmpError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {
                        //string content = "Emp Id, Error Description, Row No";
                        //string content = columnName + ", Error Description, Row No";
                        string content =  "Error Description,Row No";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {
                                //content = data.ToString();
                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }


            /*try
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Uploaded file contains some errors. Please check the downloaded error file.";

               

                string delimiter = "";
                StringBuilder sb = new StringBuilder();
                List<string> CsvRow = new List<string>();

                if (detailView.Count > 0)
                {
                    //string content = "Emp Id, Error Description, Row No";
                    string content = "Error Desciption";
                    CsvRow.Add(content + Environment.NewLine);

                    foreach (var data in detailView)
                    {
                        if(data!=null)
                        {
                            content = data.ToString();
                            CsvRow.Add(content + Environment.NewLine);
                        }               
                    }
                }

                sb.AppendLine(string.Join(delimiter, CsvRow));
                
                byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());

                if (bytes != null)
                {
                    Response.Clear();
                    Response.ContentType = "text/csv";
                    Response.AddHeader("Content-Length", bytes.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment;filename=ErrorFile_" + FileName + "_" + DateTime.Today.ToString() + ".csv");
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
            }
            catch(Exception e)
            {

            }*/
        }

       
        protected  void btnDownloadError_Click(object sender, EventArgs e)
        {
            string filePath = Server.MapPath("~/CLRA/TempEmpError/" + hdFileName.Value);
            DownloadFile(filePath);
        }

        protected void DownloadSample(object sender, EventArgs e)
        {
            if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadSamples/")))
            {
                Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadSamples/"));
            }

            string filePath = Server.MapPath("~/CLRA/DownloadSamples/SamplePrincipleEmployee.xlsx");
            DownloadFile(filePath);
        }


        private void DownloadFile(string Path)
        {
            try
            {               
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {          
                    Response.Clear();         
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);                    
                    Response.AddHeader("Content-Length", file.Length.ToString());                 
                    Response.ContentType = "text/plain";        
                    Response.Flush();    
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void DownloadPEDetails(object sender, EventArgs e)
        {
            DownloadDataCSV();
        }
        public void DownloadDataCSV()
        {
            //work in progress
            string FileName = "PrincipleEmployeeDetails_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
            try
            {
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    List<PrincipleEmployeeDetails> lstPEData = new List<PrincipleEmployeeDetails>();

                    lstPEData = (from P in db.RLCS_PrincipleEmployerMaster
                                 join PL in db.RLCS_PrincipleEmployerLocationMaster on P.PEID equals PL.PEID
                                 join SM in db.RLCS_State_Mapping on PL.State equals SM.SM_Code
                                 join LM in db.RLCS_Location_City_Mapping on PL.Location equals LM.LM_Code
                                 where P.ClientID == hdClientId.Value && P.Status == "A" && PL.Status == "A"
                                 select new PrincipleEmployeeDetails
                                 {
                                     PEID = P.PEID,
                                     PEName = P.PEName,
                                     StateId = SM.SM_Code,
                                     StateName = SM.SM_Name,
                                     LocationId = LM.LM_Code,
                                     LocationName = LM.LM_Name,
                                     BranchId = PL.PELID,
                                     Branch = PL.Branch
                                 }).Distinct().ToList();

                    if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadData/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadData/"));
                    }

                    string FilePath = Server.MapPath("~/CLRA/DownloadData/") + FileName;
                    using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                    {
                        string delimiter = "";
                        StringBuilder sb = new StringBuilder();
                        List<string> CsvRow = new List<string>();
                        string content = "PEID,PEName,StateId,StateName,LocationId,LocationName,BranchId,Branch";
                        CsvRow.Add(content + Environment.NewLine);

                        if (lstPEData != null && lstPEData.Count > 0)
                        {
                            foreach (var data in lstPEData)
                            {
                                if (data != null)
                                {

                                    //content = data.ToString();
                                    content = data.PEID + "," + data.PEName + "," + data.StateId + "," + data.StateName + "," + data.LocationId + "," + data.LocationName + "," + data.BranchId + "," + data.Branch;
                                    CsvRow.Add(content + Environment.NewLine);
                                }
                            }
                        }

                        sb.AppendLine(string.Join(delimiter, CsvRow));
                        swOutputFile.WriteLine(sb.ToString());
                    }

                    DownloadFile(FilePath);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }



        }

        public class PrincipleEmployeeDetails
        {

            public int PEID { get; set; }
            public string PEName { get; set; }
            public string StateId { get; set; }
            public string StateName { get; set; }
            public string LocationId { get; set; }
            public string LocationName { get; set; }
            public int BranchId { get; set; }
            public string Branch { get; set; }
        }

        public class ErrorMessage
        {
            public string ErrorDescription { get; set; }
            public string RowNum { get; set; }
        }

    }
}