﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers;
using static com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers.SetupController;
namespace com.VirtuosoITech.ComplianceManagement.Portal.CLRA
{
    public partial class PrincipleEmployerContractorCreation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        [WebMethod]
        public static string BindClientList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ClientDetails> ContractorDetailsList = new List<ClientDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //var ClientObj = (from Client in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                    //                 where Client.BranchType == "E"
                    //                 select Client).Distinct().ToList();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var ClientObj = RLCS_ClientsManagement.GetAllCLRA_client(customerID);
                    //var ClientObj = RLCS_ClientsManagement.GetClentsByServiceProvider(94);
                    foreach (var row in ClientObj)
                    {
                        ClientDetails Details = new ClientDetails();
                        Details.ClientID = row.CM_ClientID;
                        Details.ClientName = row.CM_ClientName;
                        Details.UniqueName = row.CM_ClientName + " [" + row.CM_ClientID + "] ";
                        ContractorDetailsList.Add(Details);
                    }
                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }



        [WebMethod]
        public static string SaveContractorDetails(PrincipleEmployerContractor DetailsObj)
        {
            bool Success = false;
            bool APISuccess = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                //int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                //int id = Convert.ToInt32(DetailsObj.PECID);
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                RLCS_PrincipleEmployerContractor objContractor = new RLCS_PrincipleEmployerContractor();
                objContractor.PECID = DetailsObj.PECID;
                objContractor.PEC_PEID = DetailsObj.PEC_PEID;
                objContractor.PEC_ContractorName = DetailsObj.ContractorName;
                objContractor.PEC_Address = DetailsObj.Address;
                objContractor.PEC_NatureOfWork = DetailsObj.NatureOfWork;
                objContractor.PEC_ContractFrom = GetDate(DetailsObj.ContractFrom);
                objContractor.PEC_ContractTo = GetDate(DetailsObj.ContractTo);
                objContractor.PEC_NoOfEmployees = DetailsObj.NoOfEmployees;
                objContractor.PEC_CreatedBy = Portal.Common.AuthenticationHelper.UserID.ToString();
                objContractor.PEC_ModifiedBy = Portal.Common.AuthenticationHelper.UserID.ToString();

                if (DetailsObj.Canteen == "Y")
                    objContractor.PEC_CanteenProvided = true;
                else
                    objContractor.PEC_CanteenProvided = false;

                if (DetailsObj.Restroom == "Y")
                    objContractor.PEC_RestroomProvided = true;
                else
                    objContractor.PEC_RestroomProvided = false;

                if (DetailsObj.Creches == "Y")
                    objContractor.PEC_Creches = true;
                else
                    objContractor.PEC_Creches = false;

                if (DetailsObj.DrinkingWater == "Y")
                    objContractor.PEC_DrinkingWater = true;
                else
                    objContractor.PEC_DrinkingWater = false;

                if (DetailsObj.FirstAid == "Y")
                    objContractor.PEC_FirstAid = true;
                else
                    objContractor.PEC_FirstAid = false;

                Success = RLCS_ClientsManagement.SavePrincipleEmployerContractor(objContractor, DetailsObj.State, DetailsObj.Location, DetailsObj.Branch);
                if (Success == true)
                {
                    CLRA_APICall.PrincipleEmployerContractor jsonPEC = new CLRA_APICall.PrincipleEmployerContractor()
                    {
                        PECID = DetailsObj.PECID,
                        PEC_PEID = DetailsObj.PEC_PEID,
                        PEC_PELID = objContractor.PEC_PELID,
                        PEC_ContractorName = DetailsObj.ContractorName,
                        PEC_Address = DetailsObj.Address,
                        PEC_NatureOfWork = DetailsObj.NatureOfWork,
                        PEC_ContractFrom = GetDate(DetailsObj.ContractFrom),
                        PEC_ContractTo = GetDate(DetailsObj.ContractTo),
                        PEC_NoOfEmployees = DetailsObj.NoOfEmployees,
                        PEC_CanteenProvided = DetailsObj.Canteen,
                        PEC_RestroomProvided = DetailsObj.Restroom,
                        PEC_Creches = DetailsObj.Creches,
                        PEC_DrinkingWater = DetailsObj.DrinkingWater,
                        PEC_FirstAid = DetailsObj.FirstAid,
                        PEC_CreatedBy = AuthenticationHelper.UserID.ToString()
                    };
                    CLRA_APICall objAPI = new CLRA_APICall();
                    APISuccess = objAPI.PrincipleEmployerContractorApiCall(jsonPEC);
                    if (Success)
                    {
                        CLRA_APICall.Update_ProcessedStatus_PrincipleEmployerContractor(DetailsObj.PECID, APISuccess);
                    }

                }
                return serializer.Serialize(Success);

                #region Commented Code
                /* RLCS_PrincipleEmployerContractor updateDetails = (from row in entities.RLCS_PrincipleEmployerContractor
                                                                   where row.PECID == id
                                                                   select row).FirstOrDefault();

                 int principleEmployrId = Convert.ToInt32(DetailsObj.PEC_PEID);
                 int pelid = entities.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployrId && t.State == DetailsObj.State && t.Location == DetailsObj.Location && t.Branch == DetailsObj.Branch && t.Status == "A").Select(t => t.PELID).FirstOrDefault();

                 bool isCanteen = false;
                 bool isRestroom = false;
                 bool isCreches = false;
                 bool isDrinkingWater = false;
                 bool isFirstAid = false;

                 if (DetailsObj.Canteen == "Y")
                     isCanteen = true;

                 if (DetailsObj.Restroom == "Y")
                     isRestroom = true;

                 if (DetailsObj.Creches == "Y")
                     isCreches = true;

                 if (DetailsObj.DrinkingWater == "Y")
                     isDrinkingWater = true;

                 if (DetailsObj.FirstAid == "Y")
                     isFirstAid = true;

                 if (updateDetails != null)
                 {
                     #region Update Details

                     updateDetails.PEC_PEID = DetailsObj.PEC_PEID;
                     updateDetails.PEC_PELID = pelid;
                     updateDetails.PEC_ContractorName = DetailsObj.ContractorName;
                     updateDetails.PEC_Address = DetailsObj.Address;
                     updateDetails.PEC_NatureOfWork = DetailsObj.NatureOfWork;            
                     updateDetails.PEC_ContractFrom = GetDate(DetailsObj.ContractFrom);
                     updateDetails.PEC_ContractTo = GetDate(DetailsObj.ContractTo);
                     updateDetails.PEC_NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees);
                     updateDetails.PEC_CanteenProvided = isCanteen;
                     updateDetails.PEC_RestroomProvided = isRestroom;
                     updateDetails.PEC_Creches = isCreches;
                     updateDetails.PEC_DrinkingWater = isDrinkingWater;
                     updateDetails.PEC_FirstAid = isFirstAid;                                                      
                     updateDetails.PEC_ModifiedBy = UserID.ToString();
                     updateDetails.PEC_ModifiedDate = DateTime.Now;

                     entities.SaveChanges();
                     Success = true;

                     #endregion


                 }
                 else
                 {
                     #region add new detail

                     RLCS_PrincipleEmployerContractor objContractor = new RLCS_PrincipleEmployerContractor()
                     {
                         PEC_PEID = DetailsObj.PEC_PEID,
                         PEC_PELID = pelid,
                         PEC_ContractorName = DetailsObj.ContractorName,
                         PEC_Address = DetailsObj.Address,
                         PEC_NatureOfWork = DetailsObj.NatureOfWork,
                         PEC_ContractFrom = GetDate(DetailsObj.ContractFrom),
                         PEC_ContractTo = GetDate(DetailsObj.ContractTo),
                         PEC_NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees),
                         PEC_CanteenProvided = isCanteen,
                         PEC_RestroomProvided = isRestroom,
                         PEC_Creches = isCreches,
                         PEC_DrinkingWater = isDrinkingWater,
                         PEC_FirstAid = isFirstAid,
                         PEC_Status = "A",
                         PEC_CreatedBy = UserID.ToString(),
                         PEC_CreatedDate = DateTime.Now
                     };
                     entities.RLCS_PrincipleEmployerContractor.Add(objContractor);
                     entities.SaveChanges();

                     Success = true;
                     #endregion
                 }

                 return serializer.Serialize(Success);*/
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
            }
        }


        [WebMethod]
        public static string SavePrincipleEmployerDetails(PrincipleEmployeeDetails DetailsObj)
        {
            bool Success = false;
            bool APISuccess = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                #region CommentedRegion
                /* bool Success = false;
                 JavaScriptSerializer serializer = new JavaScriptSerializer();
                 if (true)
                 {
                     int id = Convert.ToInt32(DetailsObj.ID);
                     using (ComplianceDBEntities entities = new ComplianceDBEntities())
                     {

                         RLCS_PrincipleEmployerMaster updateDetails = (from row in entities.RLCS_PrincipleEmployerMaster
                                                                       where row.PEID == id
                                                                       select row).FirstOrDefault();
                         bool isact = false;
                         if (DetailsObj.IsCentralAct == "1")
                             isact = true;

                         if (updateDetails != null)
                         {
                             #region Update Details

                             updateDetails.PEName = DetailsObj.PEName;
                             updateDetails.NatureOfBusiness = DetailsObj.NatureOfBusiness;
                             updateDetails.ContractFrom = GetDate(DetailsObj.ContractFrom);
                             updateDetails.ContractTo = GetDate(DetailsObj.ContractTo);
                             updateDetails.NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees);
                             updateDetails.Address = DetailsObj.Address;
                             updateDetails.ContractValue = Convert.ToDouble(DetailsObj.Contractvalue);
                             updateDetails.SecurityDeposit = Convert.ToDouble(DetailsObj.SecurityDeposit);
                             updateDetails.IsCentralAct = isact;
                             updateDetails.ModifiedDate = DateTime.Now;
                             updateDetails.ModifiedBy = "";
                             updateDetails.Status = DetailsObj.Status;

                             entities.SaveChanges();
                             Success = true;

                             #endregion


                         }
                         else
                         {
                             #region add new detail
                             bool isact1 = false;
                             if (DetailsObj.IsCentralAct == "1")
                                 isact1 = true;


                             RLCS_PrincipleEmployerMaster PrincipleEmployerMaster = new RLCS_PrincipleEmployerMaster()
                             {
                                 ClientID = DetailsObj.ClientID,
                                 PEName = DetailsObj.PEName,
                                 NatureOfBusiness = DetailsObj.NatureOfBusiness,
                                 ContractFrom = GetDate(DetailsObj.ContractFrom),
                                 ContractTo = GetDate(DetailsObj.ContractTo),
                                 NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees),
                                 Address = DetailsObj.Address,
                                 ContractValue = Convert.ToDouble(DetailsObj.Contractvalue),
                                 SecurityDeposit = Convert.ToDouble(DetailsObj.SecurityDeposit),
                                 IsCentralAct = isact1,
                                 CreatedDate = DateTime.Now,
                                 CreatedBy = "",
                                 Status = "A"
                             };
                             entities.RLCS_PrincipleEmployerMaster.Add(PrincipleEmployerMaster);
                             entities.SaveChanges();

                             Success = true;
                             #endregion
                         }

                         return serializer.Serialize(Success);
                     }
                 }*/
                #endregion

                int SelectedClient_Customer = RLCS_ClientsManagement.GetCustomerIDByClientID(DetailsObj.ClientID);
                RLCS_PrincipleEmployerMaster objPrinciple = new RLCS_PrincipleEmployerMaster();

                objPrinciple.ClientID = DetailsObj.ClientID;
                objPrinciple.PEID = DetailsObj.ID;
                objPrinciple.PEName = DetailsObj.PEName;
                objPrinciple.NatureOfBusiness = DetailsObj.NatureOfBusiness;

                var namecheck = RLCS_ClientsManagement.CheckName(DetailsObj.PEName, DetailsObj.ClientID);

                if (namecheck.Count > 0)
                {
                    Success = false;
                    return serializer.Serialize(Success);
                }
                else
                {

                    //objPrinciple.ContractFrom = GetDate(DetailsObj.ContractFrom);
                    //objPrinciple.ContractTo = GetDate(DetailsObj.ContractTo);
                    //objPrinciple.NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees);
                    //objPrinciple.Address = DetailsObj.Address;
                    //objPrinciple.ContractValue = Convert.ToDouble(DetailsObj.Contractvalue);
                    //objPrinciple.SecurityDeposit = Convert.ToDouble(DetailsObj.SecurityDeposit);

                    //if (DetailsObj.IsCentralAct == "1")
                    //    objPrinciple.IsCentralAct = true;
                    //else
                    //    objPrinciple.IsCentralAct = false;
                    //objPrinciple.PE_PAN = Convert.ToString(DetailsObj.PE_PAN);
                    objPrinciple.CreatedBy = Portal.Common.AuthenticationHelper.UserID.ToString();
                    objPrinciple.ModifiedBy = Portal.Common.AuthenticationHelper.UserID.ToString();
                    objPrinciple.Status = "A";
                    objPrinciple.AVACOM_CustomerID = SelectedClient_Customer;
                    Success = RLCS_ClientsManagement.SavePrincipleEmployer(objPrinciple);
                    if (Success == true)
                    {
                        CLRA_APICall.PrincipleEmployerMaster jsonPEM = new CLRA_APICall.PrincipleEmployerMaster()
                        {
                            PEID = objPrinciple.PEID.ToString(),
                            PEName = objPrinciple.PEName,
                            ClientID = objPrinciple.ClientID,
                            NatureOfBusiness = objPrinciple.NatureOfBusiness,
                            //ContractFrom = Convert.ToDateTime(objPrinciple.ContractFrom),
                            //ContractTo = Convert.ToDateTime(objPrinciple.ContractTo),
                            //NoOfEmployees = Convert.ToInt32(objPrinciple.NoOfEmployees),
                            //Address = objPrinciple.Address,
                            //CreatedBy = objPrinciple.CreatedBy,
                            //ContractValue = Convert.ToDouble(objPrinciple.ContractValue),
                            //SecurityDeposit = Convert.ToDouble(objPrinciple.SecurityDeposit),
                            //IsCentralAct = objPrinciple.IsCentralAct == true ? "Y" : "N",
                            PE_PAN = objPrinciple.PE_PAN
                        };
                        CLRA_APICall objAPI = new CLRA_APICall();
                        APISuccess = objAPI.PrincipleEmployerMasterApiCall(jsonPEM);
                        if (APISuccess)
                        {
                            CLRA_APICall.Update_ProcessedStatus_PrincipleEmployerMaster(objPrinciple.PEID, APISuccess);
                        }

                    }
                    return serializer.Serialize(Success);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
                // return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string SavePrincipleEmployerLocationDetails(PrincipleEmployeeDetailsLocation DetailsObj)
        {
            bool Success = false;
            bool APISuccess = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                int SelectedClient_Customer = RLCS_ClientsManagement.GetCustomerIDByClientID(DetailsObj.ClientID);
                int plid = Convert.ToInt32(DetailsObj.PLID);

                RLCS_PrincipleEmployerLocationMaster objPELocation = new RLCS_PrincipleEmployerLocationMaster();
                if (plid != 0)
                {
                    objPELocation.PELID = plid;
                }
                objPELocation.Branch = DetailsObj.Branch;
                objPELocation.Location = DetailsObj.Location;

                if (DetailsObj.Mines == "1")
                    objPELocation.Mines = true;
                else
                    objPELocation.Mines = false;

                // objPELocation.NatureOfBusiness = DetailsObj.NatureOfBusiness;
                objPELocation.PEID = DetailsObj.PEID;
                objPELocation.State = DetailsObj.State;
                objPELocation.Status = "A";
                objPELocation.ModifiedBy = Portal.Common.AuthenticationHelper.UserID.ToString();
                objPELocation.CreatedBy = Portal.Common.AuthenticationHelper.UserID.ToString();

                string weekoffs = string.Join("|", DetailsObj.WeekOff.ToArray());

                objPELocation.PE_LIN = DetailsObj.PE_LIN;
                objPELocation.PE_AuthorisedPerson_EmailID = DetailsObj.PE_AuthorisedPerson_EmailID;
                objPELocation.PE_Company_PhoneNo = DetailsObj.PE_Company_PhoneNo;
                objPELocation.Client_LINNo = DetailsObj.Client_LINNo;
                objPELocation.Client_CompanyEmailID = DetailsObj.Client_CompanyEmailID;
                objPELocation.Client_Company_Phone_No = DetailsObj.Client_Company_Phone_No;
                objPELocation.Contract_Licence_No = DetailsObj.Contract_Licence_No;
                objPELocation.Licence_Valid_From_date = string.IsNullOrWhiteSpace(DetailsObj.Licence_Valid_From_date) ? (DateTime?)null : GetDate(DetailsObj.Licence_Valid_From_date);
                objPELocation.Licence_Valid_To_date = string.IsNullOrWhiteSpace(DetailsObj.Licence_Valid_To_date) ? (DateTime?)null : GetDate(DetailsObj.Licence_Valid_To_date);
                objPELocation.Contractor_Person_Incharge_Name = DetailsObj.Contractor_Person_Incharge_Name;
                objPELocation.Contractor_Person_Incharge_LIN = DetailsObj.Contractor_Person_Incharge_LIN;
                objPELocation.Contractor_Person_Incharge_PAN = DetailsObj.Contractor_Person_Incharge_PAN;
                objPELocation.Contractor_Person_Incharge_EmailID = DetailsObj.Contractor_Person_Incharge_EmailID;
                objPELocation.Contractor_Person_Incharge_MobileNo = DetailsObj.Contractor_Person_Incharge_MobileNo;
                objPELocation.Client_Nature_of_business = DetailsObj.Client_Nature_of_business;
                objPELocation.PE_Address = DetailsObj.PE_Address;
                objPELocation.Contractor_Licensing_Officer_Designation = DetailsObj.Contractor_Licensing_Officer_Designation;
                objPELocation.Licencing_officer_Head_Quarter = DetailsObj.Licencing_officer_Head_Quarter;
                objPELocation.Nature_ofwelfare_amenities_provided = DetailsObj.Nature_ofwelfare_amenities_provided;
                objPELocation.Statutory_statute = DetailsObj.Statutory_statute;

                objPELocation.Address = DetailsObj.Address;
                objPELocation.NumberOfEmp = DetailsObj.NumberOfEmp;
                objPELocation.ContractFrom = GetDate(DetailsObj.ContractFrom);
                objPELocation.ContractTo = GetDate(DetailsObj.ContractTo);

                Success = RLCS_ClientsManagement.SavePrincipleEmployerLocation(objPELocation, plid, weekoffs, DetailsObj.ClientID);
                if (Success)
                {
                    CLRA_APICall.PrincipleEmployerLocationMaster jsonPELM = new CLRA_APICall.PrincipleEmployerLocationMaster()
                    {
                        PEID = objPELocation.PEID,
                        PELID = objPELocation.PELID,
                        State = objPELocation.State,
                        Location = objPELocation.Location,
                        Branch = objPELocation.Branch,
                        //NatureOfBusiness = objPELocation.NatureOfBusiness,
                        Mines = objPELocation.Mines == true ? "YES" : "NO",
                        WeekOff = objPELocation.WeekOff,
                        PE_LIN = objPELocation.PE_LIN,
                        PE_AuthorisedPerson_EmailID = objPELocation.PE_AuthorisedPerson_EmailID,
                        PE_Company_PhoneNo = objPELocation.PE_Company_PhoneNo,
                        Client_LINNo = objPELocation.Client_LINNo,
                        Client_CompanyEmailID = objPELocation.Client_CompanyEmailID,
                        Client_Company_Phone_No = objPELocation.Client_Company_Phone_No,
                        Contract_Licence_No = objPELocation.Contract_Licence_No,
                        Licence_Valid_From_date = Convert.ToString(objPELocation.Licence_Valid_From_date),
                        Licence_Valid_To_date = Convert.ToString(objPELocation.Licence_Valid_To_date),
                        Contractor_Person_Incharge_Name = objPELocation.Contractor_Person_Incharge_Name,
                        Contractor_Person_Incharge_LIN = objPELocation.Contractor_Person_Incharge_LIN,
                        Contractor_Person_Incharge_PAN = objPELocation.Contractor_Person_Incharge_PAN,
                        Contractor_Person_Incharge_EmailID = objPELocation.Contractor_Person_Incharge_EmailID,
                        CreatedBy = objPELocation.CreatedBy,
                        Contractor_Person_Incharge_MobileNo = objPELocation.Contractor_Person_Incharge_MobileNo,
                        Client_Nature_of_business = objPELocation.Client_Nature_of_business,
                        PE_Address = objPELocation.PE_Address,
                        Contractor_Licensing_Officer_Designation = objPELocation.Contractor_Licensing_Officer_Designation,
                        Licencing_officer_Head_Quarter = objPELocation.Licencing_officer_Head_Quarter,
                        Nature_ofwelfare_amenities_provided = objPELocation.Nature_ofwelfare_amenities_provided,
                        Statutory_statute = objPELocation.Statutory_statute,
                        Address = objPELocation.Address,
                        NumberOfEmp = objPELocation.NumberOfEmp,
                        ContractFrom = objPELocation.ContractFrom,
                        ContractTo = objPELocation.ContractTo

                    };
                    CLRA_APICall objAPI = new CLRA_APICall();
                    APISuccess = objAPI.PrincipleEmployerLocationMasterApiCall(jsonPELM);
                    if (APISuccess)
                    {
                        CLRA_APICall.Update_ProcessedStatus_PrincipleEmployerLocationMaster(objPELocation.PELID, APISuccess);
                    }
                }


                return serializer.Serialize(Success);
                #region Commented Code
                //int plid = Convert.ToInt32(DetailsObj.PLID);
                //string weeoff = string.Join("|", DetailsObj.WeekOff.ToArray());
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{

                //RLCS_PrincipleEmployerLocationMaster updateDetails = (from row in entities.RLCS_PrincipleEmployerLocationMaster
                //                                                      where row.PELID == plid
                //                                                      select row).FirstOrDefault();
                //bool Mines = false;
                //if (DetailsObj.Mines == "1")
                //    Mines = true;

                //if (updateDetails != null)
                //{             
                //    updateDetails.Branch = DetailsObj.Branch;
                //    updateDetails.NatureOfBusiness = DetailsObj.NatureOfBusiness;
                //    updateDetails.WeekOff = weeoff;
                //    updateDetails.Mines = Mines;
                //    updateDetails.Status = DetailsObj.Status;
                //    updateDetails.ModifiedBy = "Admin";
                //    updateDetails.ModifiedDate = DateTime.Now;
                //    entities.SaveChanges();
                //    Success = true;                       
                //}
                //else
                //{

                //    bool Mine = false;
                //    if (DetailsObj.Mines == "1")
                //        Mine = true;


                //    RLCS_PrincipleEmployerLocationMaster PrincipleEmployerMasterLocation = new RLCS_PrincipleEmployerLocationMaster()
                //    {
                //        PEID = DetailsObj.PEID,
                //        State = DetailsObj.State,
                //        Location = DetailsObj.Location,
                //        Branch = DetailsObj.Branch,
                //        NatureOfBusiness = DetailsObj.NatureOfBusiness,
                //        WeekOff = weeoff,
                //        Mines = Mine,
                //        Status = DetailsObj.Status,
                //        CreatedDate = DateTime.Now,
                //        CreatedBy = "Admin"

                //    };
                //    entities.RLCS_PrincipleEmployerLocationMaster.Add(PrincipleEmployerMasterLocation);
                //    entities.SaveChanges();

                //    Success = true;

                //}

                //return serializer.Serialize(Success);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
            }
        }

        [WebMethod]
        public static string SaveMigrateEmployeeDetails(MigrateEmployeeDetails DetailsObj)//EmployeeMasterDetails
        {
            bool Success = false;
            bool APISuccess = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                List<string> lstEmpid = new List<string>();
                //string StateCode = DetailsObj.State;
                //string Location = DetailsObj.Location;
                int BranchId = Convert.ToInt32(DetailsObj.Branch);
                string clientid = DetailsObj.ClientID;
                string userid = Portal.Common.AuthenticationHelper.UserID.ToString();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (DetailsObj.Mode == "A")
                    {
                        foreach (var item in DetailsObj.MigrateEmployee)
                        {
                            if (item != null)
                                lstEmpid.Add(item.EmpID);

                        }

                        Success = RLCS_ClientsManagement.SaveMigratedEmployees(clientid, BranchId, lstEmpid, GetDate(DetailsObj.ContractFrom), GetDate(DetailsObj.ContractTo), userid);

                        #region Commented Code

                        /*foreach (var item in DetailsObj.MigrateEmployee)
                        {
                            var lstDetails = (from em in entities.RLCS_PrincipleEmployeeMaster
                                              join lm in entities.RLCS_PrincipleEmployerLocationMaster on em.PELID equals lm.PELID
                                              join emplyr in entities.RLCS_PrincipleEmployerMaster on lm.PEID equals emplyr.PEID
                                              where emplyr.ClientID == clientid && em.EmpID == item.EmpID && lm.Status == "A" && emplyr.Status == "A"
                                              select em).ToList();

                            if (lstDetails != null && lstDetails.Count > 0)//first make all the entries for that emp having that pe inactive
                            {
                                foreach (var obj in lstDetails.ToList())
                                {
                                    RLCS_PrincipleEmployeeMaster objUpdate = entities.RLCS_PrincipleEmployeeMaster.Where(t => t.EmpID == obj.EmpID && t.PELID == obj.PELID).FirstOrDefault();
                                    if (objUpdate != null)
                                    {
                                        objUpdate.Status = "I";
                                        entities.SaveChanges();
                                    }

                                }
                            }

                            RLCS_PrincipleEmployeeMaster updateDetails = (from row in entities.RLCS_PrincipleEmployeeMaster
                                                                          where row.EmpID == item.EmpID && row.PELID == BranchId
                                                                          select row).FirstOrDefault();

                            if (updateDetails != null)//if rec found then update with Active status
                            {
                                #region Update Details
                                updateDetails.ContractFrom = GetDate(DetailsObj.ContractFrom);
                                updateDetails.ContractTo = GetDate(DetailsObj.ContractTo);
                                updateDetails.PELID = BranchId;
                                updateDetails.ModifiedBy = "Admin";
                                updateDetails.ModifiedDate = DateTime.Now;
                                updateDetails.ContractEndDate = null;
                                updateDetails.ReasonForContractEnd = null;
                                updateDetails.Status = "A";
                                entities.SaveChanges();
                                Success = true;
                                #endregion
                            }
                            else
                            {
                                RLCS_PrincipleEmployeeMaster objInsert = new RLCS_PrincipleEmployeeMaster();
                                objInsert.EmpID = item.EmpID;
                                objInsert.PELID = BranchId;
                                objInsert.ContractFrom = GetDate(DetailsObj.ContractFrom);
                                objInsert.ContractTo = GetDate(DetailsObj.ContractTo);
                                objInsert.CreatedBy = "Admin";
                                objInsert.CreatedDate = DateTime.Now;
                                objInsert.Status = "A";
                                entities.RLCS_PrincipleEmployeeMaster.Add(objInsert);
                                entities.SaveChanges();
                                Success = true;
                            }
                        }*/
                        #endregion
                    }
                    else//edit mode
                    {

                        RLCS_PrincipleEmployeeMaster objEmployee = new RLCS_PrincipleEmployeeMaster();
                        objEmployee.EmpID = DetailsObj.EmployeeID;
                        objEmployee.PELID = Convert.ToInt32(DetailsObj.PELID);
                        objEmployee.ContractFrom = GetDate(DetailsObj.ContractFrom);
                        objEmployee.ContractTo = GetDate(DetailsObj.ContractTo);

                        if (DetailsObj.ContractEndDate != "")
                            objEmployee.ContractEndDate = GetDate(DetailsObj.ContractEndDate);

                        objEmployee.ReasonForContractEnd = DetailsObj.ReasonForContractEnd;
                        objEmployee.ModifiedBy = userid;
                        Success = RLCS_ClientsManagement.UpdateMigratedEmployees(objEmployee);
                        #region Commented Code
                        /*int pelid = Convert.ToInt32(DetailsObj.PELID);
                        RLCS_PrincipleEmployeeMaster updateDetails = (from row in entities.RLCS_PrincipleEmployeeMaster
                                                                      where row.EmpID == DetailsObj.EmployeeID  && row.PELID == pelid
                                                                      select row).FirstOrDefault();
                        if (updateDetails != null)
                        {              
                            updateDetails.ContractFrom = GetDate(DetailsObj.ContractFrom);
                            updateDetails.ContractTo = GetDate(DetailsObj.ContractTo);                                
                            updateDetails.ModifiedBy = "Admin";
                            updateDetails.ModifiedDate = DateTime.Now;

                            if(DetailsObj.ContractEndDate!="")
                            {
                                updateDetails.ContractEndDate = GetDate(DetailsObj.ContractEndDate);
                            }

                            updateDetails.ReasonForContractEnd = DetailsObj.ReasonForContractEnd;

                            if (updateDetails.ContractEndDate!=null)
                                updateDetails.Status = "C";
                            else
                                updateDetails.Status = "A";

                            entities.SaveChanges();
                            Success = true;
                        }*/
                        #endregion
                        if (Success)
                        {
                            CLRA_APICall.PrincipleEmployeeMaster jsonPEM = new CLRA_APICall.PrincipleEmployeeMaster()
                            {
                                EmpID = objEmployee.EmpID,
                                PELID = Convert.ToString(objEmployee.PELID),
                                ContractFrom = objEmployee.ContractFrom,
                                ContractTo = objEmployee.ContractTo,
                                ContractEndDate = objEmployee.ContractEndDate,
                                ReasonForContractEnd = objEmployee.ReasonForContractEnd,
                                status = objEmployee.Status
                            };
                            CLRA_APICall objAPI = new CLRA_APICall();
                            bool PSuccess = objAPI.PrincipleEmployeeMasterApiCall(jsonPEM);
                            if (PSuccess)
                            {
                                CLRA_APICall.Update_ProcessedStatus_PrincipleEmployeeMaster(objEmployee.PELID, objEmployee.EmpID, PSuccess);
                            }
                        }
                    }
                    return serializer.Serialize(Success);

                }

            }
            catch (Exception ex)
            {
                Success = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //return ex.Message.ToString();
                return serializer.Serialize(Success);
            }
        }




        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }
        public void DownloadDataCSV(int peid)
        {

            int PEID_val = peid;
            //work in progress
            string FileName = "PrincipleEmployeeDetails_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
            try
            {
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    List<PrincipleEmployeeDetails1> lstPEData = new List<PrincipleEmployeeDetails1>();

                    lstPEData = (from P in db.RLCS_PrincipleEmployerLocationMaster
                                 join PL in db.RLCS_PrincipleEmployeeMaster on P.PELID equals PL.PELID
                                 join SM in db.RLCS_State_Mapping on P.State equals SM.SM_Code
                                 where P.PEID == PEID_val && P.Status == "A" && PL.Status == "A"
                                 select new PrincipleEmployeeDetails1
                                 {
                                     PEID = P.PEID,
                                     EmpID = PL.EmpID,
                                     StateId = SM.SM_Code,
                                     StateName = SM.SM_Name,
                                     BranchId = PL.PELID,
                                     Branch = P.Branch
                                 }).Distinct().ToList();

                    if (!Directory.Exists(Server.MapPath("~/CLRA/DownloadData/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/CLRA/DownloadData/"));
                    }

                    string FilePath = Server.MapPath("~/CLRA/DownloadData/") + FileName;
                    using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                    {
                        string delimiter = "";
                        StringBuilder sb = new StringBuilder();
                        List<string> CsvRow = new List<string>();
                        string content = "PEID,EMPID,StateId,StateName,BranchId,Branch";
                        CsvRow.Add(content + Environment.NewLine);

                        if (lstPEData != null && lstPEData.Count > 0)
                        {
                            foreach (var data in lstPEData)
                            {
                                if (data != null)
                                {

                                    //content = data.ToString();
                                    content = data.PEID + "," + data.EmpID + "," + data.StateId + "," + data.StateName + "," + data.BranchId + "," + data.Branch;
                                    CsvRow.Add(content + Environment.NewLine);
                                }
                            }
                        }

                        sb.AppendLine(string.Join(delimiter, CsvRow));
                        swOutputFile.WriteLine(sb.ToString());
                    }

                    try
                    {


                        //   FileInfo file = new FileInfo(FilePath);
                        // string extension = Path.GetExtension(FilePath);
                        if (File.Exists(FilePath))
                        {
                            HttpResponse response = HttpContext.Current.Response;
                            response.Buffer = true;
                            response.ClearContent();
                            response.ClearHeaders();
                            response.Clear();
                            response.ContentType = "application/csv";
                            response.AddHeader("content-disposition", "attachment; filename=" + "PrincipleFILE" + DateTime.Now.ToString("ddMMyy") + ".csv");
                            response.TransmitFile(FilePath);
                            response.Flush(); // Sends all curr
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            //response.End();
                        }

                        //FileInfo file = new FileInfo(FilePath);
                        //string extension = Path.GetExtension(FilePath);
                        //if (file.Exists)
                        //{
                        //    HttpContext.Current.Response.Clear();
                        //    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                        //    HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                        //    HttpContext.Current.Response.ContentType = "application/" + extension.ToString().Replace(".", "").ToLower() + "";
                        //    HttpContext.Current.Response.Flush();

                        //    HttpContext.Current.Response.End();
                        //    //  HttpContext.Current.ApplicationInstance.CompleteRequest();
                        //}
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
                    }


                    //PrincipleEmployerCreation PP = new PrincipleEmployerCreation();
                    //PP.DownloadFile(FilePath);
                    //DownloadFile(FilePath);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


        }


        [WebMethod]
        public string DownloadFile(string Path)
        {
            try
            {

                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/csv";
                    Response.Flush();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return "YES";
        }


        [WebMethod]
        public static string BindPrincipleEmployerTable(PrincipleEmployeeDetails DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetails> EmployerrDetailsList = new List<PrincipleEmployeeDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //var EmployerObj = (from Employer in entities.RLCS_PrincipleEmployerMaster
                    //                   where Employer.ClientID == DetailsObj.ClientID && Employer.Status == "A"
                    //                   select Employer).ToList();

                    var EmployerObj = RLCS_ClientsManagement.GetPrincipleEmployer(DetailsObj.ClientID, "C");

                    foreach (var row in EmployerObj)
                    {
                        PrincipleEmployeeDetails Details = new PrincipleEmployeeDetails();
                        Details.ID = row.PEID;
                        Details.ClientID = row.ClientID;
                        Details.PEName = row.PEName;
                        Details.NatureOfBusiness = row.NatureOfBusiness;
                        Details.ContractFrom = Convert.ToDateTime(row.ContractFrom).ToString("dd/MM/yyyy");
                        Details.ContractTo = Convert.ToDateTime(row.ContractTo).ToString("dd/MM/yyyy");
                        if (Details.ContractFrom == "01-Jan-0001")
                            Details.ContractFrom = "";

                        if (Details.ContractTo == "01-Jan-0001")
                            Details.ContractTo = "";

                        Details.NoOfEmployees = row.NoOfEmployees.ToString();
                        Details.Address = row.Address;
                        Details.Contractvalue = row.ContractValue.ToString();
                        Details.SecurityDeposit = row.SecurityDeposit.ToString();
                        Details.Status = row.Status.ToString();
                        Details.IsCentralAct = row.IsCentralAct.ToString();

                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        var clientname = (from row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                          join row2 in entities.RLCS_Client_BasicDetails on row1.CM_ClientID equals row2.CB_ClientID
                                          where row1.BranchType == "E"
                                          && row1.CM_Status == "A"
                                          && row2.IsCLRAApplicable == true
                                          && row1.IsProcessed == true
                                          && row1.AVACOM_CustomerID == customerID
                                          && row1.CM_ClientID == row.ClientID
                                          select row1).ToList();

                        var clientname1 = "";
                        foreach (var iteem in clientname)
                        {

                            clientname1 = iteem.AVACOM_BranchName;
                        }

                        Details.EntityID = clientname1 + " [" + row.ClientID + "] ";

                        EmployerrDetailsList.Add(Details);
                    }

                    return serializer.Serialize(EmployerrDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string GetPrincipleEmployerDetails(PrincipleEmployeeDetails DetailsObj)
        {
            try
            {
                int PEID = Convert.ToInt32(DetailsObj.ID);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetails> EmployerDetailsList = new List<PrincipleEmployeeDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //var employerObj = (from employer in entities.RLCS_PrincipleEmployerMaster
                    //                   where employer.PEID == PEID
                    //                   select employer).ToList();
                    //GetPrincipleEmployerLocation
                    var employerObj = RLCS_ClientsManagement.GetPrincipleEmployer(DetailsObj.ID.ToString(), "PE");

                    foreach (var row in employerObj)
                    {
                        PrincipleEmployeeDetails Details = new PrincipleEmployeeDetails();
                        Details.ID = row.PEID;
                        Details.ClientID = row.ClientID;
                        Details.PEName = row.PEName;
                        Details.NatureOfBusiness = row.NatureOfBusiness;

                        //Details.ContractFrom = Convert.ToDateTime(row.ContractFrom).ToString();
                        DateTime dtFrom = Convert.ToDateTime(row.ContractFrom);//ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Details.ContractFrom = strDtFrom;

                        //Details.ContractTo = Convert.ToDateTime(row.ContractTo).ToString();
                        DateTime dtTo = Convert.ToDateTime(row.ContractTo);//ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Details.ContractTo = strDtTo;

                        Details.NoOfEmployees = row.NoOfEmployees.ToString();
                        Details.Address = row.Address;
                        Details.Contractvalue = row.ContractValue.ToString();
                        Details.SecurityDeposit = row.SecurityDeposit.ToString();
                        Details.Status = row.Status.ToString();
                        Details.IsCentralAct = row.IsCentralAct.ToString();
                        Details.PE_PAN = Convert.ToString(row.PE_PAN);
                        EmployerDetailsList.Add(Details);
                    }

                    return serializer.Serialize(EmployerDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string GetPrincipleEmployerLocDetails(PrincipleEmployeeDetails DetailsObj)
        {

            try
            {
                int peid = Convert.ToInt32(DetailsObj.ID);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetailsLocation> LocationDetailsList = new List<PrincipleEmployeeDetailsLocation>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())

                {

                    var locationObj = (from lctn in entities.RLCS_PrincipleEmployerLocationMaster
                                       where lctn.PEID == peid
                                       select lctn).ToList();

                    foreach (var row in locationObj)
                    {
                        PrincipleEmployeeDetailsLocation Details = new PrincipleEmployeeDetailsLocation();
                        Details.PEID = row.PEID;
                        Details.Branch = row.Branch;
                        Details.NatureOfBusiness = row.NatureOfBusiness;
                        //List<string> weekOff = row.WeekOff.Split(',').ToList(); //originally
                        List<string> weekOff = row.WeekOff.Split('|').ToList();
                        Details.WeekOff = weekOff;
                        Details.Mines = row.Mines.ToString();
                        Details.Status = row.Status.ToString();

                        Details.PE_LIN = row.PE_LIN;
                        Details.PE_AuthorisedPerson_EmailID = row.PE_AuthorisedPerson_EmailID;
                        Details.PE_Company_PhoneNo = row.PE_Company_PhoneNo;
                        Details.Client_LINNo = row.Client_LINNo;
                        Details.Client_CompanyEmailID = row.Client_CompanyEmailID;


                        Details.Client_Company_Phone_No = row.Client_Company_Phone_No;
                        Details.Contract_Licence_No = row.Contract_Licence_No;


                        //DateTime LicenceValidFromdate = Convert.ToDateTime(row.Licence_Valid_From_date);//ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //string LicenceValidFromdate1 = LicenceValidFromdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //if (LicenceValidFromdate1 == "01/01/0001")
                        //{
                        //    Details.Licence_Valid_From_date = "";
                        //}
                        //else
                        //{
                        //    Details.Licence_Valid_From_date = LicenceValidFromdate1;
                        //}

                        if (row.Licence_Valid_From_date != null)
                        {
                            DateTime dtFrom = Convert.ToDateTime(row.Licence_Valid_From_date);
                            string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.Licence_Valid_From_date = strDtFrom;
                        }
                        if (row.Licence_Valid_To_date != null)
                        {
                            DateTime dtTo = Convert.ToDateTime(row.Licence_Valid_To_date);
                            string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.Licence_Valid_To_date = strDtTo;
                        }
                        //DateTime Licence_Valid_To_date = Convert.ToDateTime(row.Licence_Valid_To_date);//ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //string Licence_Valid_To_date1 = Licence_Valid_To_date.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //if (Licence_Valid_To_date1 == "01/01/0001")
                        //{
                        //    Details.Licence_Valid_To_date = "";
                        //}
                        //else
                        //{
                        //    Details.Licence_Valid_To_date = Licence_Valid_To_date1;
                        //}

                        Details.Contractor_Person_Incharge_Name = row.Contractor_Person_Incharge_Name;

                        Details.Contractor_Person_Incharge_LIN = row.Contractor_Person_Incharge_LIN;
                        Details.Contractor_Person_Incharge_PAN = row.Contractor_Person_Incharge_PAN;
                        Details.Contractor_Person_Incharge_EmailID = row.Contractor_Person_Incharge_EmailID;
                        Details.Contractor_Person_Incharge_MobileNo = row.Contractor_Person_Incharge_MobileNo;
                        Details.Client_Nature_of_business = row.Client_Nature_of_business;

                        Details.PE_Address = row.PE_Address;
                        Details.Contractor_Licensing_Officer_Designation = row.Contractor_Licensing_Officer_Designation;
                        Details.Licencing_officer_Head_Quarter = row.Licencing_officer_Head_Quarter;
                        Details.Nature_ofwelfare_amenities_provided = row.Nature_ofwelfare_amenities_provided;
                        Details.Statutory_statute = row.Statutory_statute;

                        Details.Address = row.Address;
                        Details.NumberOfEmp = row.NumberOfEmp;
                        if (row.ContractFrom != null)
                        {
                            DateTime dtFrom = Convert.ToDateTime(row.ContractFrom);
                            string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.ContractFrom = strDtFrom;
                        }
                        if (row.ContractTo != null)
                        {
                            DateTime dtTo = Convert.ToDateTime(row.ContractTo);
                            string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.ContractTo = strDtTo;
                        }


                        LocationDetailsList.Add(Details);
                    }

                    return serializer.Serialize(LocationDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }


        [WebMethod]
        public static string GetPrincipleEmployeeDetails(string empid, string principleEmpId, string emp_pelid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                int peid = Convert.ToInt32(principleEmpId);
                int pelid = Convert.ToInt32(emp_pelid);


                RLCS_PrincipleEmployeeMaster employerObj = null;
                EmployeeMasterDetails Details = new EmployeeMasterDetails();



                //employerObj = (from employee in entities.RLCS_PrincipleEmployeeMaster
                //                  join location in entities.RLCS_PrincipleEmployerLocationMaster on employee.PELID equals location.PELID
                //                  where location.PEID == peid && employee.EmpID == empid && employee.Status == "A" && employee.PELID == pelid
                //                  select employee).FirstOrDefault();

                employerObj = RLCS_ClientsManagement.GetPrincipleEmployeeDetails(peid, empid, pelid);

                if (employerObj != null)
                {
                    //Details.ContractFrom = Convert.ToDateTime(employerObj.ContractFrom).ToString();

                    DateTime dtFrom = Convert.ToDateTime(employerObj.ContractFrom);
                    string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Details.ContractFrom = strDtFrom;


                    //Details.ContractTo = Convert.ToDateTime(employerObj.ContractTo).ToString();

                    DateTime dtTo = Convert.ToDateTime(employerObj.ContractTo);
                    string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Details.ContractTo = strDtTo;

                    //Details.ContractEndDate = Convert.ToDateTime(employerObj.ContractEndDate).ToString();

                    DateTime dtEnd = Convert.ToDateTime(employerObj.ContractEndDate);
                    string strDtEnd = dtEnd.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Details.ContractEndDate = strDtEnd;

                    Details.ReasonForContractEnd = employerObj.ReasonForContractEnd;
                    return serializer.Serialize(Details);
                }
                return serializer.Serialize(false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(false);
            }
        }


        [WebMethod]
        public static string GetContractorDetails(string pecid)
        {
            try
            {
                int Id = Convert.ToInt32(pecid);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployerContractor> ContractorDetailsList = new List<PrincipleEmployerContractor>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //var employerObj = (from employer in entities.RLCS_PrincipleEmployerContractor
                    //                   where employer.PECID == Id
                    //                   select employer).ToList();

                    RLCS_PrincipleEmployerContractor employerObj = RLCS_ClientsManagement.GetPrincipleEmployerContractorDetails(Id);
                    if (employerObj != null)
                    {
                        PrincipleEmployerContractor Details = new PrincipleEmployerContractor();

                        Details.PEC_PEID = employerObj.PEC_PEID;
                        Details.ContractorName = employerObj.PEC_ContractorName;
                        Details.Address = employerObj.PEC_Address;
                        Details.NatureOfWork = employerObj.PEC_NatureOfWork;


                        //Details.ContractFrom = Convert.ToDateTime(employerObj.PEC_ContractFrom).ToString();

                        DateTime dtFrom = Convert.ToDateTime(employerObj.PEC_ContractFrom);
                        string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Details.ContractFrom = strDtFrom;

                        //Details.ContractTo = Convert.ToDateTime(employerObj.PEC_ContractTo).ToString();
                        DateTime dtTo = Convert.ToDateTime(employerObj.PEC_ContractTo);
                        string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Details.ContractTo = strDtTo;

                        Details.NoOfEmployees = Convert.ToInt32(employerObj.PEC_NoOfEmployees);

                        if (employerObj.PEC_CanteenProvided == true)
                            Details.Canteen = "Y";
                        else
                            Details.Canteen = "N";

                        if (employerObj.PEC_RestroomProvided == true)
                            Details.Restroom = "Y";
                        else
                            Details.Restroom = "N";

                        if (employerObj.PEC_Creches == true)
                            Details.Creches = "Y";
                        else
                            Details.Creches = "N";

                        if (employerObj.PEC_DrinkingWater == true)
                            Details.DrinkingWater = "Y";
                        else
                            Details.DrinkingWater = "N";

                        if (employerObj.PEC_FirstAid == true)
                            Details.FirstAid = "Y";
                        else
                            Details.FirstAid = "N";

                        //ContractorDetailsList.Add(Details);
                        return serializer.Serialize(Details);
                    }

                    return serializer.Serialize(false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }


        [WebMethod]
        public static string DeleteContractor(string pecid)
        {
            bool Success = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {

                int Id = Convert.ToInt32(pecid);

                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{

                //    RLCS_PrincipleEmployerContractor employerObj = (from employer in entities.RLCS_PrincipleEmployerContractor
                //                                                    where employer.PECID == Id
                //                                                    select employer).FirstOrDefault();

                //    employerObj.PEC_Status = "I";
                //    employerObj.PEC_ModifiedBy = Portal.Common.AuthenticationHelper.UserID.ToString();
                //    employerObj.PEC_ModifiedDate = DateTime.Now;

                //    entities.SaveChanges();
                //    Success = true;

                //    return serializer.Serialize(Success);
                //}

                Success = RLCS_ClientsManagement.DeletePrincipleEmployerContractor(Id, Portal.Common.AuthenticationHelper.UserID.ToString());
                return serializer.Serialize(Success);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
            }
        }


        [WebMethod]
        public static string DeletePrincipleEmployer(string peid)
        {
            bool Success = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {

                int Id = Convert.ToInt32(peid);

                Success = RLCS_ClientsManagement.DeletePrincipleEmployer(Id, Portal.Common.AuthenticationHelper.UserID.ToString());
                return serializer.Serialize(Success);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
            }
        }

        [WebMethod]
        public static string DeletePrincipleEmployerLocation(string plid)
        {
            bool Success = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {

                int Id = Convert.ToInt32(plid);

                Success = RLCS_ClientsManagement.DeletePrincipleEmployerLocation(Id, Portal.Common.AuthenticationHelper.UserID.ToString());
                return serializer.Serialize(Success);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
            }
        }

        [WebMethod]
        public static string DeleteEmployee(string EmpID, string PeID)
        {
            bool Success = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {

                int Id = Convert.ToInt32(PeID);
                Success = RLCS_ClientsManagement.DeletePrincipleEmployee(EmpID, Id, Portal.Common.AuthenticationHelper.UserID.ToString());
                return serializer.Serialize(Success);
                /*using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    RLCS_PrincipleEmployeeMaster objUpdate = (from em in entities.RLCS_PrincipleEmployeeMaster
                                                               join lm in entities.RLCS_PrincipleEmployerLocationMaster on em.PELID equals lm.PELID
                                                               join emplyr in entities.RLCS_PrincipleEmployerMaster on lm.PEID equals emplyr.PEID
                                                               where em.EmpID == EmpID && em.Status == "A" && emplyr.PEID == Id
                                                               select em).FirstOrDefault();

                    if (objUpdate != null)
                    {
                        objUpdate.Status = "I";
                        objUpdate.ModifiedBy = Portal.Common.AuthenticationHelper.UserID.ToString();
                        objUpdate.ModifiedDate = DateTime.Now;

                        entities.SaveChanges();
                        Success = true;
                    }

                    return serializer.Serialize(Success);
                }*/
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return serializer.Serialize(Success);
            }
        }

        [WebMethod]
        public static string GetPrincipleEmployerLocationDetails(PrincipleEmployeeDetailsLocation DetailsObj)
        {
            try
            {
                int plid = Convert.ToInt32(DetailsObj.PLID);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetailsLocation> LocationDetailsList = new List<PrincipleEmployeeDetailsLocation>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var locationObj = (from lctn in entities.RLCS_PrincipleEmployerLocationMaster
                                       where lctn.PELID == plid
                                       select lctn).ToList();

                    foreach (var row in locationObj)
                    {
                        PrincipleEmployeeDetailsLocation Details = new PrincipleEmployeeDetailsLocation();
                        Details.PLID = row.PELID;

                        string stateNm = RLCS_ClientsManagement.GetStateName(row.State);
                        if (stateNm != null)
                            Details.State = stateNm;
                        // else
                        //      Details.State = "";

                        string locationNm = RLCS_ClientsManagement.GetLocationName(row.Location);
                        if (locationNm != null)
                            Details.Location = locationNm;
                        //else
                        //    Details.Location = "";

                        Details.Branch = row.Branch;
                        Details.NatureOfBusiness = row.NatureOfBusiness;
                        //List<string> weekOff = row.WeekOff.Split(',').ToList(); //originally
                        List<string> weekOff = row.WeekOff.Split('|').ToList();
                        Details.WeekOff = weekOff;
                        Details.Mines = row.Mines.ToString();
                        Details.Status = row.Status.ToString();

                        Details.PE_LIN = row.PE_LIN;
                        Details.PE_AuthorisedPerson_EmailID = row.PE_AuthorisedPerson_EmailID;
                        Details.PE_Company_PhoneNo = row.PE_Company_PhoneNo;
                        Details.Client_LINNo = row.Client_LINNo;
                        Details.Client_CompanyEmailID = row.Client_CompanyEmailID;


                        Details.Client_Company_Phone_No = row.Client_Company_Phone_No;
                        Details.Contract_Licence_No = row.Contract_Licence_No;


                        //DateTime LicenceValidFromdate = Convert.ToDateTime(row.Licence_Valid_From_date);//ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //string LicenceValidFromdate1 = LicenceValidFromdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //if (LicenceValidFromdate1 == "01/01/0001")
                        //{
                        //    Details.Licence_Valid_From_date = "";
                        //}
                        //else
                        //{
                        //    Details.Licence_Valid_From_date = LicenceValidFromdate1;
                        //}

                        if (row.Licence_Valid_From_date != null)
                        {
                            DateTime dtFrom = Convert.ToDateTime(row.Licence_Valid_From_date);
                            string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.Licence_Valid_From_date = strDtFrom;
                        }
                        if (row.Licence_Valid_To_date != null)
                        {
                            DateTime dtTo = Convert.ToDateTime(row.Licence_Valid_To_date);
                            string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.Licence_Valid_To_date = strDtTo;
                        }
                        //DateTime Licence_Valid_To_date = Convert.ToDateTime(row.Licence_Valid_To_date);//ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //string Licence_Valid_To_date1 = Licence_Valid_To_date.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //if (Licence_Valid_To_date1 == "01/01/0001")
                        //{
                        //    Details.Licence_Valid_To_date = "";
                        //}
                        //else
                        //{
                        //    Details.Licence_Valid_To_date = Licence_Valid_To_date1;
                        //}

                        Details.Contractor_Person_Incharge_Name = row.Contractor_Person_Incharge_Name;

                        Details.Contractor_Person_Incharge_LIN = row.Contractor_Person_Incharge_LIN;
                        Details.Contractor_Person_Incharge_PAN = row.Contractor_Person_Incharge_PAN;
                        Details.Contractor_Person_Incharge_EmailID = row.Contractor_Person_Incharge_EmailID;
                        Details.Contractor_Person_Incharge_MobileNo = row.Contractor_Person_Incharge_MobileNo;
                        Details.Client_Nature_of_business = row.Client_Nature_of_business;

                        Details.PE_Address = row.PE_Address;
                        Details.Contractor_Licensing_Officer_Designation = row.Contractor_Licensing_Officer_Designation;
                        Details.Licencing_officer_Head_Quarter = row.Licencing_officer_Head_Quarter;
                        Details.Nature_ofwelfare_amenities_provided = row.Nature_ofwelfare_amenities_provided;
                        Details.Statutory_statute = row.Statutory_statute;

                        Details.Address = row.Address;
                        Details.NumberOfEmp = row.NumberOfEmp;
                        if (row.ContractFrom != null)
                        {
                            DateTime dtFrom = Convert.ToDateTime(row.ContractFrom);
                            string strDtFrom = dtFrom.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.ContractFrom = strDtFrom;
                        }
                        if (row.ContractTo != null)
                        {
                            DateTime dtTo = Convert.ToDateTime(row.ContractTo);
                            string strDtTo = dtTo.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            Details.ContractTo = strDtTo;
                        }


                        LocationDetailsList.Add(Details);
                    }

                    return serializer.Serialize(LocationDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindPrincipleEmployerList(ClientDetails ClientObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PEDetails> PEDetailsList = new List<PEDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //var PEObj = (from PE in entities.RLCS_PrincipleEmployerMaster
                    //             where PE.ClientID == ClientObj.ClientID && PE.Status == "A"
                    //             select PE).Distinct().ToList();

                    var PEObj = RLCS_ClientsManagement.GetPrincipleEmployer(ClientObj.ClientID, "C");

                    foreach (var row in PEObj)
                    {
                        PEDetails Details = new PEDetails();
                        Details.PEID = row.PEID;
                        Details.PEName = row.PEName;

                        PEDetailsList.Add(Details);
                    }
                    return serializer.Serialize(PEDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }



        [WebMethod]
        public static string BindStateList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstStateList = (from STATE in entities.RLCS_State_Mapping
                                        select new StateDetails
                                        {
                                            ID = STATE.AVACOM_StateID,
                                            Name = STATE.SM_Name,
                                            Code = STATE.SM_Code
                                        }).Distinct().ToList();
                    return serializer.Serialize(lstStateList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindClientStateList(ClientDetails ClientObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var lstStateList = (from STATE in entities.RLCS_State_Mapping
                    //                    join LM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping on STATE.SM_Code equals LM.CM_State
                    //                    where LM.BranchType == "B" && LM.CM_Status == "A" &&  STATE.SM_Status=="A" && LM.CM_ClientID == ClientObj.ClientID
                    //                    select new StateDetails
                    //                    {
                    //                        ID = STATE.AVACOM_StateID,
                    //                        Name = STATE.SM_Name,
                    //                        Code = STATE.SM_Code
                    //                    }).Distinct().ToList();

                    var lstStateList = RLCS_ClientsManagement.GetStateByType(ClientObj.ClientID, "B", "C").Select(t => new { ID = t.AVACOM_StateID, Name = t.SM_Name, Code = t.SM_Code }).Distinct().ToList();
                    return serializer.Serialize(lstStateList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindPEStateList(string peid)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //int principleEmployerId = Convert.ToInt32(peid);
                    //var lstStateList = (from STATE in entities.RLCS_State_Mapping
                    //                    join LM in entities.RLCS_PrincipleEmployerLocationMaster on STATE.SM_Code equals LM.State
                    //                    where LM.Status=="A" && LM.PEID == principleEmployerId

                    //                    select new StateDetails
                    //                    {
                    //                        ID = STATE.AVACOM_StateID,
                    //                        Name = STATE.SM_Name,
                    //                        Code = STATE.SM_Code
                    //                    }).Distinct().ToList();

                    var lstStateList = RLCS_ClientsManagement.GetStateByType(peid, "", "PE").Select(t => new { ID = t.AVACOM_StateID, Name = t.SM_Name, Code = t.SM_Code }).Distinct().ToList();
                    return serializer.Serialize(lstStateList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindPELocationList(string peid, string stateid)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                //int principleEmployerId = Convert.ToInt32(peid);
                //var lstStateList = (from LM in entities.RLCS_Location_City_Mapping
                //                    join PM in entities.RLCS_PrincipleEmployerLocationMaster on LM.LM_Code equals PM.Location
                //                    where PM.State == stateid && PM.Status == "A" && PM.PEID == principleEmployerId
                //                    select new LocationDetails
                //                    {
                //                        Name = LM.LM_Name,
                //                        Code = LM.LM_Code
                //                    }).Distinct().ToList();

                var lstCityList = RLCS_ClientsManagement.GetLocationByType(peid, stateid, "PE").Select(t => new { Name = t.LM_Name, Code = t.LM_Code }).Distinct().ToList();
                return serializer.Serialize(lstCityList);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindLocationList(StateDetails StateObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstStateList = (from Location in entities.RLCS_Location_City_Mapping
                                        where Location.SM_Code == StateObj.Code
                                        select new LocationDetails
                                        {
                                            Name = Location.LM_Name,
                                            Code = Location.LM_Code
                                        }).Distinct().ToList();
                    return serializer.Serialize(lstStateList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }


        [WebMethod]
        public static string BindClientLocationList(string clientid, string stateid)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var lstStateList = (from CM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                    //                    join Location in entities.RLCS_Location_City_Mapping on CM.CM_City equals Location.LM_Code
                    //                    where CM.CM_ClientID == clientid && CM.CM_State == stateid && CM.BranchType == "B" && CM.CM_Status == "A" && Location.LM_Status == "A"

                    //                    select new LocationDetails
                    //                    {
                    //                        Name = Location.LM_Name,
                    //                        Code = Location.LM_Code
                    //                    }).Distinct().ToList();

                    var lstCityList = RLCS_ClientsManagement.GetLocationByType(clientid, stateid, "C").Select(t => new { Name = t.LM_Name, Code = t.LM_Code }).Distinct().ToList();

                    return serializer.Serialize(lstCityList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindClientLocationListNew(string clientid, string stateid)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var lstStateList = (from CM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                    //                    join Location in entities.RLCS_Location_City_Mapping on CM.CM_City equals Location.LM_Code
                    //                    where CM.CM_ClientID == clientid && CM.CM_State == stateid && CM.BranchType == "B" && CM.CM_Status == "A" && Location.LM_Status == "A"

                    //                    select new LocationDetails
                    //                    {
                    //                        Name = Location.LM_Name,
                    //                        Code = Location.LM_Code
                    //                    }).Distinct().ToList();

                    var lstCityList = RLCS_ClientsManagement.GetLocationByType(clientid, stateid, "N").Select(t => new { Name = t.LM_Name, Code = t.LM_Code }).Distinct().ToList();

                    return serializer.Serialize(lstCityList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindBranchList(BranchDetails BranchObj)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var lstbranchList = (from branch in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                    //                     where branch.BranchType =="B" && branch.CM_Status=="A" && branch.CM_State== BranchObj.Code && branch.CM_ClientID== BranchObj.ClientID && branch.CM_City == BranchObj.LocationID 
                    //                     select new BranchDetails
                    //                     {
                    //                         Name = branch.AVACOM_BranchName,
                    //                         ID = branch.AVACOM_BranchID
                    //                     }).Distinct().ToList();

                    var lstCityList = RLCS_ClientsManagement.GetBranches_ByClient(BranchObj.ClientID).Where(t => t.CM_State == BranchObj.Code && t.CM_City == BranchObj.LocationID)
                                       .Select(t => new { Name = t.AVACOM_BranchName, Code = t.AVACOM_BranchID }).Distinct().ToList();

                    return serializer.Serialize(lstCityList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }


        [WebMethod]
        public static string BindBranchListContractor(BranchDetails BranchObj)
        {
            try
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var lstbranchList = (from LM in entities.RLCS_PrincipleEmployerLocationMaster
                    //                     join EM in entities.RLCS_PrincipleEmployerMaster on LM.PEID equals EM.PEID
                    //                     where EM.ClientID== BranchObj.ClientID && LM.PEID == BranchObj.PEID && LM.State == BranchObj.Code && LM.Location == BranchObj.LocationID && LM.Status=="A"
                    //                     select new BranchDetails
                    //                     {
                    //                         Name = LM.Branch,
                    //                         ID = LM.PELID
                    //                     }).Distinct().ToList();

                    var lstbranchList = RLCS_ClientsManagement.GetBranches_ByPrincipleEmployer(BranchObj.ClientID, Convert.ToInt32(BranchObj.PEID), BranchObj.Code, BranchObj.LocationID)
                                        .Select(t => new { Name = t.Branch, ID = t.PELID }).Distinct().ToList();
                    return serializer.Serialize(lstbranchList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        //[WebMethod]
        //public static string BindPrincipleEmployerLocationTable(PrincipleEmployeeDetailsLocation DetailsObj)
        //{
        //    try
        //    {
        //        JavaScriptSerializer serializer = new JavaScriptSerializer();
        //        List<PrincipleEmployeeDetailsLocation> LocationDetailsList = new List<PrincipleEmployeeDetailsLocation>();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            /*var locationObj = (from location in entities.RLCS_PrincipleEmployerLocationMaster
        //                               where location.PEID == DetailsObj.PEID || location.State == DetailsObj.State || location.Location == DetailsObj.Location
        //                               select location).ToList();*/ //originally

        //            var locationObj = (from location in entities.RLCS_PrincipleEmployerLocationMaster
        //                               where location.PEID == DetailsObj.PEID && location.State == DetailsObj.State && location.Location == DetailsObj.Location
        //                               select location).ToList();

        //            foreach (var row in locationObj)
        //            {
        //                PrincipleEmployeeDetailsLocation Details = new PrincipleEmployeeDetailsLocation();
        //                Details.PLID = row.PELID;
        //                var stateObj = (from state in entities.RLCS_State_Mapping
        //                                where state.SM_Code == row.State
        //                                select state).FirstOrDefault();
        //                if (stateObj != null)
        //                    Details.State = stateObj.SM_Name;
        //                else
        //                    Details.State = "";

        //                int locationid = Convert.ToInt32(row.Location);
        //                //var LocationObj = (from loction in entities.RLCS_Location_City_Mapping //originally
        //                //                   where loction.AVACOM_CityID == locationid
        //                //                   select loction).FirstOrDefault();

        //                var LocationObj = (from loction in entities.RLCS_Location_City_Mapping
        //                                   where loction.LM_Code == row.Location
        //                                   select loction).FirstOrDefault();

        //                if (LocationObj != null)
        //                    Details.Location = LocationObj.LM_Name;
        //                else
        //                    Details.Location = "";

        //                Details.Branch = row.Branch.ToString();
        //                Details.NatureOfBusiness = row.NatureOfBusiness;
        //                if (row.Mines == true)
        //                    Details.Mines = "Yes";
        //                else
        //                    Details.Mines = "No";

        //                LocationDetailsList.Add(Details);
        //            }

        //            return serializer.Serialize(LocationDetailsList);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return ex.Message.ToString();
        //    }
        //}


        [WebMethod]
        public static string BindPrincipleEmployerLocationTable(PrincipleEmployeeDetailsLocation DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetailsLocation> LocationDetailsList = new List<PrincipleEmployeeDetailsLocation>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    //var locationObj = (from location in entities.RLCS_PrincipleEmployerLocationMaster
                    //                   where location.PEID == DetailsObj.PEID && location.State == DetailsObj.State && location.Location == DetailsObj.Location
                    //                   select location).ToList();

                    var locationObj = RLCS_ClientsManagement.GetPrincipleEmployerLocationNew(Convert.ToInt32(DetailsObj.PEID)).Where(t => t.Status == "A").ToList();

                    foreach (var row in locationObj)
                    {
                        PrincipleEmployeeDetailsLocation Details = new PrincipleEmployeeDetailsLocation();

                        Details.PLID = row.PELID;
                        //var stateObj = (from state in entities.RLCS_State_Mapping
                        //                where state.SM_Code == row.State
                        //                select state).FirstOrDefault();


                        //if (stateObj != null)
                        //    Details.State = stateObj.SM_Name;
                        //else
                        //    Details.State = "";

                        string stateNm = RLCS_ClientsManagement.GetStateName(row.State);
                        if (stateNm != null)
                            Details.State = stateNm;
                        else
                            Details.State = "";

                        string state_status = RLCS_ClientsManagement.GetStateStatus(row.State);
                        if (state_status != null)
                            Details.Status = state_status;
                        else
                            Details.Status = "";

                        //int locationid = Convert.ToInt32(row.Location);

                        //var LocationObj = (from loction in entities.RLCS_Location_City_Mapping
                        //                   where loction.LM_Code == row.Location
                        //                   select loction).FirstOrDefault();

                        //if (LocationObj != null)
                        //    Details.Location = LocationObj.LM_Name;
                        //else
                        //    Details.Location = "";

                        string locationNm = RLCS_ClientsManagement.GetLocationName(row.Location);
                        if (locationNm != null)
                            Details.Location = locationNm;
                        else
                            Details.Location = "";

                        Details.Branch = row.Branch.ToString();
                        Details.NatureOfBusiness = row.NatureOfBusiness;
                        if (row.Mines == true)
                            Details.Mines = "Yes";
                        else
                            Details.Mines = "No";

                        LocationDetailsList.Add(Details);
                    }

                    return serializer.Serialize(LocationDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindEmployeeMasterTable(EmployeeMasterDetails DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetailsLocation> LocationDetailsList = new List<PrincipleEmployeeDetailsLocation>();

                string ClientID = "";
                if (DetailsObj.ClientID != null)
                    ClientID = DetailsObj.ClientID;

                string PEID = "";
                if (DetailsObj.PEID != null)
                    PEID = DetailsObj.PEID;

                string State = "";
                if (DetailsObj.State != null)
                    State = DetailsObj.State;

                string Location = "";
                if (DetailsObj.Location != null && DetailsObj.Location != "-1")
                    Location = DetailsObj.Location;

                string Branch = "";
                if (DetailsObj.Branch != null && DetailsObj.Branch != "-1")
                    Branch = DetailsObj.Branch;


                List<PROC_RLCS_CLRA_GetPrincipleEmployeeDetails_Result> lstEmployeetDetails = new List<PROC_RLCS_CLRA_GetPrincipleEmployeeDetails_Result>();
                //lstEmployeetDetails = entities.PROC_RLCS_CLRA_GetPrincipleEmployeeDetails(ClientID, PEID,State,Location,Branch).ToList();
                lstEmployeetDetails = RLCS_ClientsManagement.GetPrincipleEmployees(ClientID, PEID, State, Location, Branch).ToList();

                return serializer.Serialize(lstEmployeetDetails);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }


        //must uncomment
        [WebMethod]
        public static string BindPrincipleEmployeeReport(string clientid, string principleEmpId)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                List<Rpt_CLRA_Principle_EmployeeMaster_Result> lstDetails = new List<Rpt_CLRA_Principle_EmployeeMaster_Result>();
                //lstDetails = entities.Rpt_CLRA_Principle_EmployeeMaster(clientid, principleEmpId).ToList();
                lstDetails = RLCS_ClientsManagement.GetPrincipleEmployeeReport(clientid, principleEmpId).ToList();
                return serializer.Serialize(lstDetails);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }




        [WebMethod]
        public static string BindContractorTable(EmployeeMasterDetails DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string ClientID = "";
                    if (DetailsObj.ClientID != null)
                        ClientID = DetailsObj.ClientID;

                    string PEID = "";
                    if (DetailsObj.PEID != null)
                        PEID = DetailsObj.PEID;
                    int principleEmployerId = Convert.ToInt32(PEID);

                    string State = "";
                    if (DetailsObj.State != null)
                        State = DetailsObj.State;

                    string Location = "";
                    if (DetailsObj.Location != null && DetailsObj.Location != "-1")
                        Location = DetailsObj.Location;

                    string Branch = "";
                    if (DetailsObj.Branch != null && DetailsObj.Branch != "-1")
                        Branch = DetailsObj.Branch;

                    //int pelid = entities.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.State == DetailsObj.State && t.Location == DetailsObj.Location && t.Branch == DetailsObj.Branch && t.Status == "A").Select(t => t.PELID).FirstOrDefault();
                    int pelid = RLCS_ClientsManagement.GetPrincipleEmployerLocation(principleEmployerId, DetailsObj.State, DetailsObj.Location)
                              .Where(t => t.Branch == DetailsObj.Branch && t.Status == "A").Select(t => t.PELID).FirstOrDefault();


                    List<PROC_RLCS_CLRA_GetPrincipleEmployerContractorDetails_Result> LstContractors = new List<PROC_RLCS_CLRA_GetPrincipleEmployerContractorDetails_Result>();
                    //LstContractors = entities.PROC_RLCS_CLRA_GetPrincipleEmployerContractorDetails(ClientID, principleEmployerId, pelid).ToList();
                    LstContractors = RLCS_ClientsManagement.GetPrincipleEmployerContractors(ClientID, principleEmployerId, pelid).ToList();

                    return serializer.Serialize(LstContractors);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            if (ContractFileUpload.HasFile)//not using
            {
                try
                {
                    string filename = Path.GetFileName(ContractFileUpload.FileName);
                    ContractFileUpload.SaveAs(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/CLRA/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "PrincipleEmployeeMasterBulkUplo");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'PrincipleEmployeeMasterBulkUplo'.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "ShowPopupExcel", "ExcelPopupShow();", true);
                            }

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)//not using
        {
            try
            {
                bool saveSuccess = false;
                int uploadedVendorCount = 0;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["PrincipleEmployeeMasterBulkUplo"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    #region Validations
                    using (ComplianceDBEntities db = new ComplianceDBEntities())
                    {
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            int principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            string employeeId = xlWorksheet.Cells[i, 2].Text.Trim();
                            string stateId = xlWorksheet.Cells[i, 3].Text.Trim();
                            string locationId = xlWorksheet.Cells[i, 4].Text.Trim();
                            string branchId = xlWorksheet.Cells[i, 5].Text.Trim();
                            string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();
                            string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();
                            string clientid = "AVACYBER";//must comment

                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                            {
                                var principleEmplyr = db.RLCS_PrincipleEmployerMaster.Where(t => t.ClientID == clientid && t.Status == "A" && t.PEID == principleEmployerId).FirstOrDefault();
                                if (principleEmplyr == null)
                                    errorMessage.Add("Principle Employer Id " + principleEmployerId + " Does Not Exist for the Selected Client. Check at Row " + i);
                            }
                            else
                                errorMessage.Add("Required Principle Employer Id at row number" + i + ".");



                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                            {
                                var emp = db.RLCS_Employee_Master.Where(t => t.EM_ClientID == clientid && t.EM_EmpID == employeeId && t.EM_Status == "A").FirstOrDefault();
                                if (emp == null)
                                    errorMessage.Add("Employee Id " + employeeId + " Does Not Exist for the Selected Client. Check at Row " + i);
                            }
                            else
                                errorMessage.Add("Required Employee Id at row number" + i + ".");


                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                            {
                                var state = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.State == stateId && t.Status == "A").FirstOrDefault();
                                if (state == null)
                                    errorMessage.Add("State " + stateId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);
                            }
                            else
                                errorMessage.Add("Required StateId at row number" + i + ".");


                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                            {
                                var loc = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.Location == locationId && t.Status == "A").FirstOrDefault();
                                if (loc == null)
                                    errorMessage.Add("Location " + locationId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);
                            }
                            else
                                errorMessage.Add("Required LocationId at row number" + i + ".");

                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                            {
                                var branch = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.Branch == branchId && t.Status == "A").FirstOrDefault();
                                if (branch == null)
                                    errorMessage.Add("Branch " + branchId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);
                            }
                            else
                                errorMessage.Add("Required BranchId at row number" + i + ".");

                            DateTime dateContractFrom = new DateTime();
                            DateTime dateContractTo = new DateTime();

                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                            {
                                try
                                {
                                    dateContractFrom = DateTime.ParseExact(contractFrom, "dd/MM/yyyy", null);
                                    contractFrom = dateContractFrom.ToString();
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add("Enter valid Contract From Date in dd/MM/yyyy format. Check at Row " + i);
                                }
                            }
                            else
                                errorMessage.Add("Required Contract From Date at row number" + i + ".");


                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                            {
                                try
                                {
                                    dateContractTo = DateTime.ParseExact(contractTo, "dd/MM/yyyy", null);
                                    contractTo = dateContractTo.ToString();
                                }
                                catch (Exception ex)
                                {
                                    errorMessage.Add("Enter valid Contract To Date in dd/MM/yyyy format. Check at Row " + i);
                                }
                            }
                            else
                                errorMessage.Add("Required Contract To Date at row number" + i + ".");

                            if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()) && !String.IsNullOrEmpty((xlWorksheet.Cells[i, 7].Text).Trim()))
                            {
                                if (dateContractFrom > dateContractTo)
                                    errorMessage.Add("Contract From should be less than Contract To. Check at Row " + i);
                            }
                        }

                        if (errorMessage.Count == 0)
                        {
                            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                int principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                                string employeeId = xlWorksheet.Cells[i, 2].Text.Trim();
                                string stateId = xlWorksheet.Cells[i, 3].Text.Trim();
                                string locationId = xlWorksheet.Cells[i, 4].Text.Trim();
                                string branchId = xlWorksheet.Cells[i, 5].Text.Trim();
                                string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();

                                DateTime dateContractFrom = new DateTime();
                                DateTime dateContractTo = new DateTime();

                                dateContractFrom = DateTime.ParseExact(contractFrom, "dd/MM/yyyy", null);
                                contractFrom = dateContractFrom.ToString();

                                string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();
                                dateContractTo = DateTime.ParseExact(contractTo, "dd/MM/yyyy", null);
                                contractTo = dateContractTo.ToString();

                                string clientid = "AVACYBER";//must comment

                                int pelid = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.State == stateId && t.Location == locationId && t.Branch == branchId && t.Status == "A").Select(t => t.PELID).FirstOrDefault();

                                RLCS_PrincipleEmployeeMaster EmployeeMaster = new RLCS_PrincipleEmployeeMaster()
                                {
                                    EmpID = employeeId,
                                    PELID = pelid,
                                    ContractFrom = dateContractFrom,
                                    ContractTo = dateContractTo,
                                    Status = "A",
                                    ContractEndDate = null,
                                    ReasonForContractEnd = null,
                                    CreatedBy = UserID.ToString(),// "Admin",
                                    CreatedDate = DateTime.Now
                                };
                                db.RLCS_PrincipleEmployeeMaster.Add(EmployeeMaster);
                                db.SaveChanges();
                            }

                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = " Details Uploaded Successfully";
                            cvUploadUtilityPage.CssClass = "alert alert-success";
                        }
                        else
                            ErrorMessages(errorMessage);


                    }
                    #endregion




                    /*if (errorMessage.Count == 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {

                            

                            int principleEmployerId = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            string employeeId = xlWorksheet.Cells[i, 2].Text.Trim();
                            string stateId = xlWorksheet.Cells[i, 3].Text.Trim();
                            string locationId = xlWorksheet.Cells[i, 4].Text.Trim();
                            string branchId = xlWorksheet.Cells[i, 5].Text.Trim();
                            string contractFrom = xlWorksheet.Cells[i, 6].Text.Trim();
                            string contractTo = xlWorksheet.Cells[i, 7].Text.Trim();

                            string clientid = "AVACYBER";//must comment
                            using (ComplianceDBEntities db = new ComplianceDBEntities())
                            {
                                var principleEmplyr = db.RLCS_PrincipleEmployerMaster.Where(t => t.ClientID == clientid && t.Status == "A" && t.PEID == principleEmployerId).FirstOrDefault();
                                if(principleEmplyr == null)
                                    errorMessage.Add("Principle Employer Id "+ principleEmployerId + " Does Not Exist for the Selected Client. Check at Row " + i);

                                var state = db.RLCS_PrincipleEmployerLocationMaster.Where(t=> t.PEID == principleEmployerId && t.State == stateId && t.Status == "A").FirstOrDefault();
                                if (state == null)
                                    errorMessage.Add("State "+ stateId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);

                                var loc = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.Location == locationId && t.Status == "A").FirstOrDefault();
                                if (loc == null)
                                    errorMessage.Add("Location " + locationId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);

                                var branch = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.Branch == branchId && t.Status == "A").FirstOrDefault();
                                if (branch == null)
                                    errorMessage.Add("Branch " + branchId + " Does Not Exist for the Selected Principle Employer. Check at Row " + i);

                                var emp = db.RLCS_Employee_Master.Where(t => t.EM_ClientID == clientid && t.EM_EmpID== employeeId && t.EM_Status == "A").FirstOrDefault();
                                if (emp == null)
                                    errorMessage.Add("Employee Id " + employeeId + " Does Not Exist for the Selected Client. Check at Row " + i);

                                DateTime dateContractFrom = new DateTime();
                                DateTime dateContractTo = new DateTime(); 

                                if (xlWorksheet.Cells[i,6].Text.Trim().GetType() == typeof(string))
                                {
                                    
                                    try
                                    {
                                        dateContractFrom = DateTime.ParseExact(contractFrom, "dd/MM/yyyy", null);
                                        contractFrom = dateContractFrom.ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMessage.Add("Enter valid Contract From Date in DD-MMM-YYYY format. Check at Row " + i);
                                    }
                                }

                                if (xlWorksheet.Cells[i, 7].Text.Trim().GetType() == typeof(string))
                                {

                                    try
                                    {
                                         dateContractTo = DateTime.ParseExact(contractTo, "DD-MMM-YYYY", null);
                                        contractTo = dateContractTo.ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMessage.Add("Enter valid Contract To Date in DD-MMM-YYYY format. Check at Row " + i);
                                    }
                                }

                                if(dateContractFrom > dateContractTo)
                                    errorMessage.Add("Contract From should be less than Contract To. Check at Row " + i);

                                if(errorMessage.Count == 0)
                                {
                                    int pelid = db.RLCS_PrincipleEmployerLocationMaster.Where(t => t.PEID == principleEmployerId && t.State == stateId && t.Location == locationId && t.Branch == branchId && t.Status == "A").Select(t => t.PELID).FirstOrDefault();

                                    RLCS_PrincipleEmployeeMaster EmployeeMaster = new RLCS_PrincipleEmployeeMaster()
                                    {

                                        

                                        EmpID = employeeId,
                                        PELID = pelid.ToString(),
                                        ContractFrom = dateContractFrom,
                                        ContractTo = dateContractTo,
                                        Status = "A",
                                        ContractEndDate = null,
                                        ReasonForContractEnd = null,
                                        CreatedBy = UserID.ToString(),// "Admin",
                                        CreatedDate = DateTime.Now
                                    };
                                    db.RLCS_PrincipleEmployeeMaster.Add(EmployeeMaster);
                                    db.SaveChanges();

                                    uploadedVendorCount++;
                                    saveSuccess = true;
                                }
                            }

                            
                        }
                        #endregion
                    }
              
                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = uploadedVendorCount + " Details Uploaded Successfully";
                        cvUploadUtilityPage.CssClass = "alert alert-success";
                    }*/



                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }



        public void ErrorMessages(List<string> emsg)//not using
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        public static DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }








        [WebMethod]
        public static bool GetPanDetails(PrincipleEmployeeDetails DetailsObj)
        {
            bool PanExist = false;
            using (ComplianceDBEntities db = new ComplianceDBEntities())
            {
                var existingPAN = db.RLCS_PrincipleEmployerMaster.Where(t => t.PE_PAN == DetailsObj.PE_PAN).FirstOrDefault();
                int DuplicatePANCount = db.RLCS_PrincipleEmployerMaster.Where(t => t.PE_PAN == DetailsObj.PE_PAN).Count();
                if (existingPAN != null)
                {
                    if (existingPAN.PE_PAN == DetailsObj.PE_PAN && existingPAN.PEID != DetailsObj.ID && DuplicatePANCount != 0)
                    {
                        PanExist = true;
                    }
                }
            }

            return PanExist;
        }
        protected void DownloadFile(object sender, EventArgs e)//not using
        {
            /*string filePath = (sender as LinkButton).CommandArgument;
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
            Response.WriteFile(filePath);
            Response.End();*/

            try
            {
                //string filePath = "C:\\Users\\Admi\\Desktop\\abc.txt";
                //string filePath = Server.MapPath("~/CLRA/Uploaded/PrincipleEmployeeMasterBulkUpload.xlsx");
                string filePath = Server.MapPath("~/CLRA/SamplePrincipleEmployee.xlsx");
                FileInfo file = new FileInfo(filePath);
                if (file.Exists)
                {
                    // Clear Rsponse reference  
                    Response.Clear();
                    // Add header by specifying file name  
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    // Add header for content length  
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    // Specify content type  
                    Response.ContentType = "text/plain";
                    // Clearing flush  
                    Response.Flush();
                    // Transimiting file  
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        public class PrincipleEmployeeDetails
        {
            public int ID { get; set; }
            public string ClientID { get; set; }
            public string PEName { get; set; }
            public string NatureOfBusiness { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }
            public string NoOfEmployees { get; set; }
            public string Address { get; set; }
            public string Contractvalue { get; set; }
            public string SecurityDeposit { get; set; }
            public string Status { get; set; }
            public string IsCentralAct { get; set; }
            public string PE_PAN { get; set; }
            public string EntityID { get; set; }
        }

        public class PrincipleEmployeeDetails1
        {
            public int PEID { get; set; }
            public string EmpID { get; set; }
            public string StateId { get; set; }
            public string StateName { get; set; }
            public int BranchId { get; set; }
            public string Branch { get; set; }
        }

        public class PrincipleEmployeeDetailsLocation
        {
            public int PLID { get; set; }
            public int PEID { get; set; }
            public string State { get; set; }
            public string Location { get; set; }
            public string Branch { get; set; }
            public string NatureOfBusiness { get; set; }
            public string Status { get; set; }
            public string Mines { get; set; }
            public List<string> WeekOff { get; set; }
            public string ClientID { get; set; }
            public string PE_LIN { get; set; }
            public string PE_AuthorisedPerson_EmailID { get; set; }
            public string PE_Company_PhoneNo { get; set; }
            public string Client_LINNo { get; set; }
            public string Client_CompanyEmailID { get; set; }
            public string Client_Company_Phone_No { get; set; }
            public string Contract_Licence_No { get; set; }
            public string Licence_Valid_From_date { get; set; }
            public string Licence_Valid_To_date { get; set; }
            public string Contractor_Person_Incharge_Name { get; set; }
            public string Contractor_Person_Incharge_LIN { get; set; }
            public string Contractor_Person_Incharge_PAN { get; set; }
            public string Contractor_Person_Incharge_EmailID { get; set; }
            public string Contractor_Person_Incharge_MobileNo { get; set; }
            public string Client_Nature_of_business { get; set; }
            public string PE_Address { get; set; }
            public string Contractor_Licensing_Officer_Designation { get; set; }
            public string Licencing_officer_Head_Quarter { get; set; }
            public string Nature_ofwelfare_amenities_provided { get; set; }
            public string Statutory_statute { get; set; }

            public string Address { get; set; }
            public Nullable<int> NumberOfEmp { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }

        }

        public class ClientDetails
        {
            public string ClientID { get; set; }
            public string ClientName { get; set; }

            public string UniqueName { get; set; }
        }

        public class PEDetails
        {
            public int PEID { get; set; }
            public string PEName { get; set; }
        }

        public class StateDetails
        {
            public long ID { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
        }

        public class BranchDetails
        {
            public string ClientID { get; set; }
            public int? ID { get; set; }
            public string LocationID { get; set; }
            public int PEID { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
        }

        public class LocationDetails
        {
            public long? ID { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }

        }

        public class EmployeeMasterDetails
        {
            public string ClientID { get; set; }
            public string PEID { get; set; }
            public string EmpID { get; set; }
            public string EmpName { get; set; }
            public string State { get; set; }
            public string Location { get; set; }
            public string Branch { get; set; }
            public string TaggedBranch { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }
            public string ContractEndDate { get; set; }
            public string Status { get; set; }
            public string ReasonForContractEnd { get; set; }
            public List<MigrateEmployee> MigrateEmployee { get; set; }
        }

        public class MigrateEmployeeDetails
        {
            public string ClientID { get; set; }
            //public string PEID { get; set; }        
            //public string EmpName { get; set; }
            public string State { get; set; }
            public string Location { get; set; }
            public string Branch { get; set; }
            //public string TaggedBranch { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }
            public string ContractEndDate { get; set; }
            //public string Status { get; set; }
            public string ReasonForContractEnd { get; set; }
            public string Mode { get; set; }
            public string PELID { get; set; }
            public string EmployeeID { get; set; }
            public List<MigrateEmployee> MigrateEmployee { get; set; }
        }

        public class MigrateEmployee
        {
            public string EmpID { get; set; }
            public string State { get; set; }
            public string Location { get; set; }
            public string Branch { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }

        }

        public class PrincipleEmployerContractor
        {
            public int PECID { get; set; }
            public int PEC_PEID { get; set; }
            public string ContractorName { get; set; }
            public string Address { get; set; }
            public string NatureOfWork { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }
            public int NoOfEmployees { get; set; }
            public string Canteen { get; set; }
            public string Restroom { get; set; }
            public string Creches { get; set; }
            public string DrinkingWater { get; set; }
            public string FirstAid { get; set; }
            public string State { get; set; }
            public string Location { get; set; }
            public string Branch { get; set; }
        }

    }
}