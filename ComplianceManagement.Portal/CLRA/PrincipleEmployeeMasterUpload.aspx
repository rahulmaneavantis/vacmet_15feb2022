﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrincipleEmployeeMasterUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployeeMasterUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

    

    <style>
        input[type=file]{
            color:#666;
        }
    </style>
</head>
<body style="background: white;">
    <form runat="server" id="formvalidation">  <%-- must uncomment if no error occurs--%>
                             <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
                            <asp:UpdatePanel ID="updatepnl" runat="server">
                                <ContentTemplate> 
                                    <div class="form-group clearfix"></div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                        class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                                    <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                        ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>

                            </div>
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                       <%--must uncomment--%>
                                         <asp:LinkButton ID="lnkDownloadPEData" style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                                            data-toggle="tooltip" data-placement="left" Title="Export to Excel" ToolTip="Download Principle Employee Details" 
                                             CommandArgument = '<%# Eval("Value") %>' runat="server" OnClick = "DownloadPEDetails"></asp:LinkButton>

                                        <asp:LinkButton ID="lnkDownload" style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                                            data-toggle="tooltip" data-placement="left" Title="Export to Excel" ToolTip="Download Sample Format" 
                                             CommandArgument = '<%# Eval("Value") %>' runat="server" OnClick = "DownloadSample"></asp:LinkButton>
                                    </div>
                                    <div class="form-group clearfix"></div>
                            <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                                <div class="col-xs-4 col-sm-4 col-md-4 text-right">
                                     <label for="tbxFilterLocation" class="control-label">File to Upload</label>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <asp:FileUpload ID="ContractFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldForFileUpload" ErrorMessage="Select File to Upload" ControlToValidate="ContractFileUpload"
                                    runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                                </div>
                              
                            </div>

                                    <div class="form-group clearfix"></div>


                               <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                                     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                        <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click"  ValidationGroup="uploadUtilityValidationGroup"  />
                                         <asp:HiddenField ID="hdFileName" runat="server" />  
                                         <asp:HiddenField ID="hdClientId" runat="server" /> 
                                     </div>
                                <%--<div class="col-lg-4 col-md-4 col-sm-4 form-group">--%>
                                    <%--<a href="SampleFormat/ContractBulkUpload.xlsx"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                    
                                    <%--<asp:LinkButton ID="lnkDownload" class="fa fa-file-excel-o" Text = "Download Sample Format" CommandArgument = '<%# Eval("Value") %>' runat="server" OnClick = "DownloadFile"></asp:LinkButton>--%>
                               <%-- </div>--%>

                               </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 colpadding0" style="margin-top:10px">
                                        <div class="col-md-12">
                                           
                                            <span id="spanErrors" runat="server" style="color:red">Uploaded File Contains Some Errors. Click to Download Error File 
                                                
                                                 <asp:Button runat="server" ID="btnDownloadError" CssClass="btn btn-danger" Text="Download" OnClick="btnDownloadError_Click" />
                                                
                                                <%--<asp:LinkButton ID="lnkDownloadError" style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                                            data-toggle="tooltip" data-placement="left" Title="Export to Excel" ToolTip="Download Sample Format" 
                                             CommandArgument = '<%# Eval("Value") %>' runat="server" OnClick = "DownloadFile"></asp:LinkButton>--%>
                                            

                                          
                                            </span>
                                         </div>
                                    </div>



                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID = "btnUploadExcel" />
                                    <asp:PostBackTrigger ControlID = "lnkDownloadPEData" />
                                     <asp:PostBackTrigger ControlID = "lnkDownload" />
                                    <asp:PostBackTrigger ControlID = "btnDownloadError" />
                                    <%--<asp:AsyncPostBackTrigger ControlID="btnUploadExcel" EventName = "Click" />--%>
                                </Triggers>
                                 </asp:UpdatePanel>
                        </form>
</body>
</html>
