﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="PrincipleEmployeeMasterCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployeeMasterCreation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<!DOCTYPE html>--%>
<%--
<html xmlns="http://www.w3.org/1999/xhtml">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--<head runat="server">--%>
    <title></title>

    <link href="../NewCSS/3.3.7/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/3.3.1/jquery.min.js"></script>
    <script src="../Newjs/3.3.7/bootstrap.min.js"></script>
    <script src="../Newjs/bootstrap-datepicker.min.js"></script> 
    <script src="../Newjs/jquery.validate.min.js"></script>
    <script src="../Newjs/jquery.validate.unobtrusive.js"></script>

    <link href="style.css" rel="stylesheet" />
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <link href="../NewCSS/font-awesome.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script src="../Newjs/fastselect.standalone.js"></script>
    <link href="../NewCSS/fastselect.min.css" rel="stylesheet" />


    <style type="text/css">
        /*.k-grid-content {
            min-height:  300px;
            max-height: 400px;
        }
        */

        .modal-open {
    overflow: scroll;
}

       
       .modal-header h4.modal-title{
           margin-top:-11px;
       }
       .modal-header .close {
         margin-top: -16px;

        }


        .btn{
            font-size:14px;
            font-weight:400;
        }

        

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .btn-primary[disabled] {
            background-color: #d7d7d7;
            border-color: #d7d7d7;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .form-control{
            height: 32px;
        }

        .k-multiselect-wrap .k-input {
            padding-top: 6px;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }
        .col-md-12 {
            text-align: center;
        }
        .k-pager-sizes {
            float: left;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
             cursor:pointer;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .k-grid-header th.k-with-icon .k-link{
            text-align:center;
        }

        .inactiveclass {
        pointer-events:none;
        }


        .fstResultItem{
            text-align:left;
        }

        .checkbox label, .radio label{
            padding-left:0px;
        }
            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
                 cursor:pointer;
            }

        .k-grid, .k-listview {
            margin-top: 10px;
        }

            .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            }

        .k-tooltip-content {
            width: max-content;
        }

        a:focus {
        outline:none !important;
        }

      .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected{
          box-shadow:none !important; 
      }

      .pem{
            text-align:left;

      }

      input[type=checkbox], input[type=radio]{
          margin:4px 3px 2px -3px !important;
      }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-grid input.k-checkbox + label.k-checkbox-label{
            margin-right:15px;
        }

        .k-calendar-container {
            background-color: white;
            width: 217px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: 0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }


        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left: 2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y: scroll! important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            text-align:center;
            font-weight: 400;
        }
        .k-button.k-button-icon .k-icon, .k-grid-filter .k-icon, .k-header .k-icon{
            margin-right:10px;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

      

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
            text-align:left;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height: 34px;
        }

        .k-multiselect-wrap, .k-floatwrap {
            height: 34px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 1%;
            margin-bottom: 1%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
            color: #515967;
            padding-top: 5px;
        }

        .k-filter-row th, .k-grid-header th[data-index='10'] {
            text-align: center;
        }


        .fstElement {
            font-size: 0.7em;
            height: 34px;
        }

        .fstToggleBtn {
            min-width: 16.5em;
        }

        .submitBtn {
            display: none;
        }

        .fstMultipleMode {
            display: block;
        }

            .fstMultipleMode .fstControls {
                width: 100%;
            }
   
              .k-checkbox-label:hover {
                  color: #1fd9e1;
              }

              .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
                  cursor: pointer;
                  background-color: transparent !important;
                  border-color: transparent;
                  color: #1fd9e1;
              }

              .btn {
                  font-weight: 400;
              }

              .k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
                  color: #2e2e2e;
                  background-color: #d8d6d6 !important;
              }

              .k-grid-content {
                  min-height: auto !important;
              }

              .k-multiselect-wrap .k-input {
                  padding-top: 6px;
              }

              .k-grid tbody .k-button {
                  min-width: 19px;
                  min-height: 25px;
                  background-color: transparent;
                  border: none;
                  margin-left: 0px;
                  margin-right: 0px;
                  padding-left: 2px;
                  padding-right: 2px;
                  border-radius: 25px;
              }

              .k-button.k-state-active, .k-button:active {
                  color: black;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li > a {
                  font-size: 16px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li:hover {
                  color: white;
                  background-color: #1fd9e1;
                  border-top-left-radius: 10px;
                  border-top-right-radius: 10px;
                  margin-left: 0.5em;
                  margin-right: 0.5em;
              }

              .panel-heading .nav > li {
                  margin-left: 5px !important;
                  margin-right: 5px !important;
              }

              .panel-heading .nav {
                  background-color: #f8f8f8;
                  border: none;
                  font-size: 11px;
                  margin: 0px 0px 0px 0;
                  border-radius: 10px;
              }

            .k-grid, .k-listview {
                  margin-top: 0px;
              }

                  .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                      -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                      box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                  }

              .k-tooltip-content {
                  width: max-content;
              }

              input[type=checkbox], input[type=radio] {
                  margin: 4px -2px 0;
                  margin-top: 1px\9;
                  line-height: normal;
              }

              .k-calendar-container {
                  background-color: white;
                  width: 217px;
              }

              .div.k-grid-footer, div.k-grid-header {
                  border-top-width: 1px;
                  margin-right: 0px;
                  margin-top: 0px;
              }


              .k-grid-footer-wrap, .k-grid-header-wrap {
                  position: relative;
                  width: 100%;
                  overflow: hidden !important;
                  border-style: solid;
                  border-width: 0 1px 0 0;
                  zoom: 1;
              }


              html {
                  color: #666666;
                  font-size: 15px;
                  font-weight: normal;
                  font-family: 'Roboto',sans-serif;
              }

              .k-checkbox-label, .k-radio-label {
                  display: inline;
              }

              .myKendoCustomClass {
                  z-index: 999 !important;
              }

              .k-header .k-grid-toolbar {
                  background: white;
                  float: left;
                  width: 100%;
              }

              .k-grid td {
                  line-height: 2.0em;
                  border-bottom-width: 1px;
                  background-color: white;
                  border-width: 0 1px 1px 0px;
              }

              .k-i-more-vertical:before {
                  content: "\e006";
              }

              .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                  background-color: #1fd9e1;
                  background-image: none;
                  background-color: white;
              }

              k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                  color: #000000;
                  border-color: #1fd9e1;
                  background-color: white;
              }

              /*#tblPrincipleEmployer .k-grid-toolbar {
            background: white;
        }*/

              .k-pager-wrap > .k-link > .k-icon {
                  margin-top: 0px;
                  margin-left: 2.7px;
                  color: inherit;
              }

              .toolbar {
                  float: left;
              }

              html .k-grid tr:hover {
                  background: white;
              }

              html .k-grid tr.k-alt:hover {
                  background: white;
              }


              .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                  margin-right: 0px;
                  margin-right: 0px;
                  margin-left: 0px;
                  margin-left: 0px;
              }

             

              .k-grid-header {
                  padding-right: 0px !important;
                  margin-right: 2px;
              }

              .k-filter-menu .k-button {
                  width: 27%;
              }

              .k-label input[type="checkbox"] {
                  margin: 0px 5px 0 !important;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  font-size: 15px;
                  background: #f8f8f8;
                  font-family: 'Roboto',sans-serif;
                  color: #2b2b2b;
                  font-weight: 400;
              }

              .k-primary {
                  border-color: #1fd9e1;
                  background-color: #1fd9e1;
              }

              .k-pager-wrap {
                  background-color: white;
                  color: #2b2b2b;
              }

              td.k-command-cell {
                  border-width: 0 0px 1px 0px;
                  text-align: center;
              }

              .k-grid-pager {
                  margin-top: -1px;
              }

              span.k-icon.k-i-calendar {
                  margin-top: 6px;
              }

              .col-md-2 {
                  width: 20%;
              }

              .k-filter-row th, .k-grid-header th.k-header {
                  border-width: 0px 0px 1px 0px;
                  background: #E9EAEA;
                  font-weight: bold;
                  margin-right: 18px;
                  font-family: 'Roboto',sans-serif;
                  font-size: 15px;
                  color: rgba(0, 0, 0, 0.5);
                  height: 20px;
                  vertical-align: middle;
              }

              .k-dropdown-wrap.k-state-default {
                  background-color: white;
                  height: 34px;
              }

              .k-multiselect-wrap, .k-floatwrap {
                  height: 34px;
              }

              .k-popup.k-calendar-container, .k-popup.k-list-container {
                  background-color: white;
              }

              .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
                  -webkit-box-shadow: inset 0 0 3px 1px white;
                  box-shadow: inset 0 0 3px 1px white;
              }

              label.k-label {
                  font-family: roboto,sans-serif !important;
                  color: #515967;
                  /* font-stretch: 100%; */
                  font-style: normal;
                  font-weight: 400;
                  min-width: max-content !important;
                  white-space: pre-wrap;
              }

              .k-multicheck-wrap .k-item {
                  line-height: 1.2em;
                  font-size: 14px;
                  margin-bottom: 5px;
              }

              label {
                  display: flex;
                  margin-bottom: 0px;
              }

              .k-state-default > .k-select {
                  border-color: #ceced2;
                  margin-top: 3px;
              }

              .k-grid-norecords {
                  width: 100%;
                  height: 100%;
                  text-align: center;
                  margin-top: 2%;
              }

              .k-grouping-header {
                  font-style: italic;
                  background-color: white;
              }

              .k-grid-toolbar {
                  background: white;
                  border: none;
              }

              .k-grid table {
                  width: 100.5%;
              }

              .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
                  color: #515967;
                  padding-top: 5px;
              }

              .k-filter-row th, .k-grid-header th[data-index='11'] {
                  text-align: center;
              }

              .k-i-filter-clear {
                  margin-top: -3px;
              }

              .k-button {
                  margin-top: 2.5px;
              }

              label.k-label:hover {
                  color: #1fd9e1;
              }

              .k-tooltip {
                  margin-top: 5px;
              }

              .k-column-menu > .k-menu {
                  background-color: white;
              }
     

   
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

        .select2-container{
            width:100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>

    <script type="text/javascript">

        //var item = parent.document.getElementById("liPECreation");
        //item.classList.add("active");

        var EmployeeMasterListData = [];

        $(document).ready(function () {
      
            
            
            

            var activeclass = $('#liPEMaster');
            activeclass.addClass('active');

          //  $("#divCheckPE").hide();
        //    $(".tabdata").hide();
       //     $("#divPECreation").show();
           // $("#divMsg").hide();
          //  $("#divMsgPELocation").hide();
           // $("#divMsgContractor").hide();
          //  $("#divErrorsPE").hide();
         //   $("#divErrorsPELocation").hide();
          //  $("#divErrorsContractor").hide();
            $("#divErrorsEmployees").hide();

         //   $("#divCheckPELocation").hide();
            $("#divCheckEmployees").hide();
           // $("#divCheckContractor").hide();
         //   $("#divPrincipleEmployer").hide();
            BindPrincipleEmployerListOnEmployeeMaster();
          // BindClientList();
            // BindPrincipleEmployerTable("");

           
            $("#ContentPlaceHolder1_btnDownloadEmployeedetails").kendoTooltip({
                content: function (e) {
                    return "Download Employee Details";
                }
            });
            $("#btnMigrate").kendoTooltip({
                content: function (e) {
                    return "Migrate Employees";
                }
            });
            $("#btnUploadEmployee").kendoTooltip({
                content: function (e) {
                    return "Upload Employees";
                }
            });
            $("#btnMigrateSave").kendoTooltip({
                content: function (e) {
                    return "Save";
                }
            });

            $('#txtEmployeeContractForm').datepicker({
                pickTime: false,
              
                autoclose: true,
                dateFormat: 'dd/mm/yy',
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,

            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtEmployeeContractTo').datepicker('setStartDate', null); //must uncomment
                }
                else {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtEmployeeContractTo').datepicker('setStartDate', startDate);
                    $('#txtContractEndDate').datepicker('setStartDate', startDate);
                }

            });
            $('#txtContractEndDate').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            }).on('changeDate', function (selected) {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('#txtEmployeeContractForm').datepicker('setEndDate', FromEndDate);
            });


            $('#txtEmployeeContractTo').datepicker({
                pickTime: false,
                autoclose: true,
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            }).on('changeDate', function (selected) {
                if (selected.date === undefined) {
                    //alert("its undefined");
                    $('#txtEmployeeContractForm').datepicker('setEndDate', null); //must uncomment
                }
                else {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#txtEmployeeContractForm').datepicker('setEndDate', FromEndDate);
                }
            });



          




            $("#btnMigrate").on("click", function (e) {
                

                $("#divMsgEmployees").hide();
             

             
                $("#divMigrateLocation").show();//must uncomment if problem
                $("#ModalEmployeeMigrate").modal("show");
                $("#divContractEnd").hide();
             //   clrMigrationDetails();
                //GetContractDetails();
                //var a = window.parent.document.getElementsByClassName('pemt');
                //a.liPECreation.classList.add('inactiveclass');
                //a.liPELocationCreation.classList.add('inactiveclass');
                //a.liContractorDetails.classList.add('inactiveclass');
                //a.liPEReport.classList.add('inactiveclass');



                $("#hdMode").val("A"); //add mode

                //$("#ddlStateEmployeeMaster").select2();
                //$("#ddlLocationEmployeeMaster").select2();
                //$("#ddlBranchEmployeeMaster").select2();

            });
            $("#btnUploadEmployee").on("click", function (e) {
                //$("#UploadEmployeeModal").modal("show");
                OpenUploadPopup();
            });



            //$("#ddlClientReport").on("change", function (e) {
            //    if ($("#ddlClientReport").val() != "-1") {
            //        $("#ddlClientReport").select2();
            //        BindPrincipleEmployerList("R");
            //    }
            //});

            //$("#ddlPrincipleEmployerReport").on("change", function (e) {
            //    if ($("#ddlPrincipleEmployerReport").val() != "-1") {
            //        $("#ddlPrincipleEmployerReport").select2();

            //        if ($("#tblPrincipleEmployeeReport").hasClass("hidden")) {
            //            $("#tblPrincipleEmployeeReport").removeClass("hidden");
            //        }
            //        BindPrincipleEmployeeReport();
            //    }
            //});


          

            $("#ddlPrincipleEmployerMaster").on("change", function (e) {
                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();
             

                if ($("#ddlPrincipleEmployerMaster").val() == "-1") {
                    $("#divStateMaster").addClass("hidden");
                    $("#divLocationMaster").addClass("hidden");
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                 //   $("#ddlPrincipleEmployerMaster").select2();

                    if ($("#divStateMaster").hasClass("hidden")) {
                        $("#divStateMaster").removeClass("hidden");
                    }

                    BindStateListEmployeeMaster();
                    GetContractDetails();

                    $("#divLocationMaster").addClass("hidden");
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }

            });



            $("#ddlPrincipleEmployerStateMaster").on("change", function (e) {
                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();

                if ($("#ddlPrincipleEmployerStateMaster").val() == "-1") {
                    $("#divLocationMaster").addClass("hidden");
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerStateMaster").select2();

                    if ($("#divLocationMaster").hasClass("hidden")) {
                        $("#divLocationMaster").removeClass("hidden");
                    }
                    BindLocationListEmployeeMaster($(this).val());
                    BindEmployeeMasterTable();

                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }

            });

       

            $("#ddlPrincipleEmployerLocationMaster").on("change", function (e) {
                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();

                if ($("#ddlPrincipleEmployerLocationMaster").val() == "-1") {
                    $("#divBranchMaster").addClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerLocationMaster").select2();
                    if ($("#divBranchMaster").hasClass("hidden")) {
                        $("#divBranchMaster").removeClass("hidden");
                    }
                    BindBranchListEmployeeMaster();
                    BindEmployeeMasterTable();
                    //$("#tblEmployeeMaster").removeClass("hidden");
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }

            });

    

            $("#ddlPrincipleEmployerBranchMaster").on("change", function (e) {

                $("#divCheckEmployees").html("");
                $("#divCheckEmployees").hide();
                $("#divReAssignCompliance").hide();


                if ($("#ddlPrincipleEmployerBranchMaster").val() == "-1") {
                    $("#divMigrateMaster").addClass("hidden");
                    $("#tblEmployeeMaster").addClass("hidden");
                }
                else {
                    $("#ddlPrincipleEmployerBranchMaster").select2();

                    if ($("#divMigrateMaster").hasClass("hidden")){
                    $("#divMigrateMaster").removeClass("hidden");
                }
                 
                 

                    $('#PEIDValue').val($("#ddlPrincipleEmployerMaster").val());

                    
                    BindEmployeeMasterTable();
                    if ($("#tblEmployeeMaster").hasClass("hidden")) {
                        $("#tblEmployeeMaster").removeClass("hidden");
                    }
                }
            });

         

            $("#ddlStateEmployeeMaster").on("change", function (e) {
                $("#ddlStateEmployeeMaster").select2();
                BindMigrateLocationList($(this).val());
            });

            $("#ddlLocationEmployeeMaster").on("change", function (e) {
                $("#ddlLocationEmployeeMaster").select2();
                BindMigrateBranchListEmployeeMaster();
            });

            $("#ddlBranchEmployeeMaster").on("change", function (e) {
                $("#ddlBranchEmployeeMaster").select2();
            });

            $('#ModalEmployeeMigrate').on('shown.bs.modal', function (e) {
                BindMigrateStateList();
            })


          
            $("#btnMigrateSave").click(function (e) {
                
                
                
                if ($("#hdMode").val() == "A") //add mode
                {
                    if (EmployeeMasterListData.length == 0) {
                        alert("Please select CheckList");
                        return false;
                    }
                }


                if (ValidateEmployeeMigration()) {
                    //alert($("#txtPEContractFrom").val());
                    //alert($("#txtPEContractTo").val());
                    //alert($("#txtEmployeeContractForm").val());
                    //alert($("#txtEmployeeContractTo").val());
                  
                    var location = document.location.href;
                    var queryString = location.split('clientid=')[1];
                    var queryString = queryString.replace(/[^\w\s]/gi, '')
                  
                    $("#divErrorsEmployees").hide();
                    var DetailsObj = {
                        MigrateEmployee: EmployeeMasterListData,
                        State: $("#ddlStateEmployeeMaster").val(),
                        Location: $("#ddlLocationEmployeeMaster").val(),
                        Branch: $("#ddlBranchEmployeeMaster").val(),
                        ContractFrom: $("#txtEmployeeContractForm").val(),
                        ContractTo: $("#txtEmployeeContractTo").val(),
                        ContractEndDate: $("#txtContractEndDate").val(),
                        ReasonForContractEnd: $("#txtContractEndReason").val(),
                        ClientID: queryString,
                        Mode: $("#hdMode").val(),
                        PELID: $("#lblEMP_PELID").val(),
                        EmployeeID: $("#lblEmployeeID").val(),
                    }

                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/SaveMigrateEmployeeDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //var flag = false;
                            //if (data.d != "false") {
                            //    flag = true;
                            //}
                            //if (flag)
                            //    alert("Employee Migrated Successfully.");
                            //else
                            //    alert("Please select Employee");
                           // alert(JSON.stringify(data));

                            if (data.d == "true") {
                                //if ($("#hdMode").val() == "A")
                                //    alert("Employee Migrated Successfully.");
                                //else
                                //    alert("Employee Updated Successfully.");

                                $("#divErrorsEmployees").hide();
                                $("#divMsgEmployees").show();

                                if ($("#hdMode").val() == "A")
                                    $("#spanMsgEmployees").html("Employees Migrated Successfully.");
                                else
                                    $("#spanMsgEmployees").html("Employee Updated Successfully.");

                                EmployeeMasterListData = [];
                                BindEmployeeMasterTable();

                                setTimeout(function () {
                                    $("#ModalEmployeeMigrate").modal("hide");
                                    clrMigrationDetails();
                                }, 3000);

                                //$("#ModalEmployeeMigrate").modal("hide");
                            }
                            else
                                alert("Migration error. Try again.");

                        },
                        failure: function (data) {
                            alert(data);
                        }

                    });
                }


            });


            

            $(document).on("click", "#tblEmployeeMaster tbody tr .Update", function (e) {


                if ($('#divErrorsEmployees').is(':visible')) {
                    $("#divErrorsEmployees").hide();
                }

            

                var item = $("#tblEmployeeMaster").data("kendoGrid").dataItem($(this).closest("tr"));

                if (item.EmpID != "" && item.PEID != "") {
                    $("#lblPrincEmpId").val(item.PEID);
                    $("#lblEmployeeID").val(item.EmpID);
                    $("#lblEMP_PELID").val(item.EMP_PELID);
                    $("#hdMode").val("E"); //edit mode
                    $.ajax({
                        type: "POST",
                        url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/GetPrincipleEmployeeDetails",
                        data: '{"empid":"' + item.EmpID + '","principleEmpId":"' + item.PEID + '","emp_pelid":"' + item.EMP_PELID + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var EmpDetails = JSON.parse(data.d);
                            if (EmpDetails != false) {
                                $("#divMsgEmployees").hide();
                                $("#divContractEnd").show();
                                $("#divMigrateLocation").hide();
                                $("#ModalEmployeeMigrate").modal("show");
                              //  clrMigrationDetails();
                              
                                if (EmpDetails.ContractFrom == "01-01-0001 00:00:00" || EmpDetails.ContractFrom == "01/01/0001 00:00:00" || EmpDetails.ContractFrom == "01/01/0001")
                                    $("#txtEmployeeContractForm").val("");
                                else {
                                    const StartdateTime = EmpDetails.ContractFrom;
                                    //const parts = StartdateTime.split(/[- :]/);
                                    const parts = StartdateTime.split(/[// :]/);
                                    const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                    //$("#txtEmployeeContractForm").val(FromDate);
                                    $("#txtEmployeeContractForm").datepicker("setDate", FromDate);
                                }


                                if (EmpDetails.ContractTo == "01-01-0001 00:00:00" || EmpDetails.ContractTo == "01/01/0001 00:00:00" || EmpDetails.ContractTo == "01/01/0001")
                                    $("#txtEmployeeContractTo").val("");
                                else {
                                    const StartdateTime = EmpDetails.ContractTo;
                                    //const parts = StartdateTime.split(/[- :]/);
                                    const parts = StartdateTime.split(/[// :]/);
                                    const ToDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                    //$("#txtEmployeeContractTo").val(ToDate);
                                    $("#txtEmployeeContractTo").datepicker("setDate", ToDate);
                                }


                                // if (EmpDetails. == "01-01-0001 00:00:00")
                                if (EmpDetails.ContractEndDate == "01-01-0001 00:00:00" || EmpDetails.ContractEndDate == "01/01/0001 00:00:00" || EmpDetails.ContractEndDate == "01/01/0001")
                                    $("#txtContractEndDate").val("");
                                else {
                                    const StartdateTime = EmpDetails.ContractEndDate;
                                    //const parts = StartdateTime.split(/[- :]/);
                                    const parts = StartdateTime.split(/[// :]/);
                                    const EndDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                                    $("#txtContractEndDate").val(EndDate);
                                }
                                //$("#txtContractEndDate").val(EmpDetails.ContractEndDate);

                                $("#txtContractEndReason").val(EmpDetails.ReasonForContractEnd);
                            }
                            else
                                alert("Server error occured. Please try again.");
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }

            });


            $(document).on("click", "#tblEmployeeMaster tbody tr .Delete", function (e) {
                var item = $("#tblEmployeeMaster").data("kendoGrid").dataItem($(this).closest("tr"));
                var empid = item.EmpID;
                var peid = item.PEID;

                if (empid != "" && empid != null && peid != "" && peid != null) {
                    var res = confirm("Are you sure you want to delete this employee?");
                    if (res == true) {
                        $.ajax({
                            type: "POST",
                            url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/DeleteEmployee",
                            data: '{"EmpID":"' + empid + '","PeID":"' + peid + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var result = JSON.parse(data.d);

                                if (result == true) {
                                    alert("Employee Deleted Successfully");
                                    BindEmployeeMasterTable();
                                }
                                else
                                    alert("Server error occured. Please try again.");

                            },
                            failure: function (data) {
                                alert(data);
                            }
                        });
                    }
                }


            });



            
            $("#btnClear").click(function (e) {
                e.preventDefault();
                $("#txtSearch").val('');

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#tblPrincipleEmployer").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 10,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "PEName", operator: "contains", value: $x },
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#tblPrincipleEmployer").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindGrid();
                }

            });

          


        });



       


        function PELocationCreation() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/PrincipleEmployerLocationCreation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');

        }

        function PEMaster() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/PrincipleEmployeeMasterCreation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');

        }
        function ContractorDetails() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/PrincipleEmployerContractorCreation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');

        }

        function CLRAactivationpage() {

            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            if (queryString != "") {
                window.location.href = "../CLRA/CLRA_Assignment_Activation.aspx?clientid=" + queryString;
            } else
                alert('Oops.!!');
        }




        function OpenUploadPopup() {
          
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            var clientId = queryString;

            $('#divReAssignCompliance').show();

            var myWindowAdv = $("#divReAssignCompliance");

            myWindowAdv.kendoWindow({
                width: "50%",
                height: '50%',
                //maxHeight: '90%',
                //minHeight:'50%',
                title: "Upload Principle Employees",
                visible: false,
                actions: ["Close"]
                //open: onOpen,                    
            });

            $('#iframeReAssign').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeReAssign').attr('src', "/CLRA/PrincipleEmployeeMasterUpload.aspx?clientid=" + clientId);

            var dialog = $("#divReAssignCompliance").data("kendoWindow");
            dialog.title("Upload Principle Employees");

            return false;
        }



        function clrMigrationDetails() {
            $("#ddlStateEmployeeMaster").val("-1");
            $("#ddlLocationEmployeeMaster").empty();
            $("#ddlLocationEmployeeMaster").val("");
            $("#ddlLocationEmployeeMaster").append($("<option></option>").val("-1").html("Select Location"));
            $("#ddlLocationEmployeeMaster").select2();

            $("#ddlBranchEmployeeMaster").empty();
            $("#ddlBranchEmployeeMaster").val("");
            $("#ddlBranchEmployeeMaster").append($("<option></option>").val("-1").html("Select Branch"));
            $("#ddlBranchEmployeeMaster").select2();

            $("#lblEmployeeID").val("");
            $("#lblPrincEmpId").val("");
            $("#lblEMP_PELID").val("");
            $("#txtEmployeeContractForm").val("");
            $("#txtEmployeeContractTo").val("");
            $("#txtContractEndDate").val("");
            $("#txtContractEndReason").val("");
            $("#hdMode").val("");
            $("#divErrorsEmployees").hide();

            $('#txtEmployeeContractForm').datepicker('setDate', null);
            $('#txtEmployeeContractTo').datepicker('setDate', null);

            $('#txtEmployeeContractForm').datepicker('minDate', null);
            $('#txtEmployeeContractForm').datepicker('maxDate', null);

            $('#txtEmployeeContractTo').datepicker('minDate', null);
            $('#txtEmployeeContractTo').datepicker('maxDate', null);



        }

        function GetContractDetails() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
            var DetailsObj = {
                ID: $("#ddlPrincipleEmployerMaster").val()
            }

            $.ajax({
                type: "POST",
              //  url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/GetPrincipleEmployerDetails",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/GetPrincipleEmployerLocDetails",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var EmployerDetails = JSON.parse(data.d);
                    if (EmployerDetails.length > 0) {
                        const StartdateTime = EmployerDetails[0].ContractFrom;
                        //const parts = StartdateTime.split(/[- :]/);
                        const parts = StartdateTime.split(/[// :]/);
                        const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                        $("#txtPEContractFrom").val(FromDate);
                        const EnddateTime = EmployerDetails[0].ContractTo;
                        //const parts1 = EnddateTime.split(/[- :]/);
                        const parts1 = EnddateTime.split(/[// :]/);
                        const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                        $("#txtPEContractTo").val(ToDate);
                        $("#txtContractForm").val(EmployerDetails[0].ContractFrom);
                        $("#txtContractTo").val(EmployerDetails[0].ContractTo);
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }
       



        function BindPrincipleEmployerListOnEmployeeMaster(ForContractor) {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var ClientObj = {
                ClientID: queryString
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindPrincipleEmployerList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));
                    //$.each(Customer, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                    //})
                    //$("#" + controlId).select2();

                    var controlId = "ddlPrincipleEmployerMaster";
                    if (ForContractor == "Y") {
                        controlId = "ddlPrincipleemployerContractor";
                    }


                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Principle Employer"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.PEID).html(value.PEName));
                        })
                    }
                    $("#" + controlId).select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }


        

        function BindMigrateStateList() {

            var id = $("#ddlPrincipleEmployerMaster").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindPEStateList",
                data: '{peid:' + id + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0) {
                    //    $("#ddlStateEmployeeMaster").empty();
                    //    $("#ddlStateEmployeeMaster").append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#ddlStateEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#ddlStateEmployeeMaster").select2();
                    //}

                    $("#ddlStateEmployeeMaster").empty();
                    $("#ddlStateEmployeeMaster").val("");
                    $("#ddlStateEmployeeMaster").append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlStateEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlStateEmployeeMaster").select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindStateListEmployeeMaster() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var ClientObj = {
                ClientID: queryString
            }

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindClientStateList",
                data: JSON.stringify({ ClientObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var State = JSON.parse(data.d);
                    //if (State.length > 0)
                    //{
                    //    var controlId = "ddlPrincipleEmployerStateMaster";

                    //    $("#" + controlId).empty();
                    //    $("#" + controlId).append($("<option></option>").val("-1").html("Select State"));
                    //    $.each(State, function (data, value) {
                    //        $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //    })
                    //    $("#" + controlId).select2();
                    //}

                    var controlId = "ddlPrincipleEmployerStateMaster";
                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select State"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }



        function BindMigrateLocationList(stateid) {
            var id = $("#ddlPrincipleEmployerMaster").val();
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindPELocationList",
                data: '{"peid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlLocationEmployeeMaster").empty();
                    //$("#ddlLocationEmployeeMaster").append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlLocationEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#ddlLocationEmployeeMaster").select2();

                    $("#ddlLocationEmployeeMaster").empty();
                    $("#ddlLocationEmployeeMaster").val("");
                    $("#ddlLocationEmployeeMaster").append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#ddlLocationEmployeeMaster").append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#ddlLocationEmployeeMaster").select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindLocationListEmployeeMaster(stateid) {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var id = queryString;
            stateid = $("#ddlPrincipleEmployerStateMaster").val();

            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindClientLocationList",
                //data: JSON.stringify({ StateObj }),
                data: '{"clientid":"' + id + '","stateid":"' + stateid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var controlId = "ddlPrincipleEmployerLocationMaster";

                    //var Location = JSON.parse(data.d);
                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Location"));

                    var Location = JSON.parse(data.d);
                    if (Location.length > 0) {
                        $.each(Location, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Code).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }




        function BindMigrateBranchListEmployeeMaster() {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')


            var BranchObj = {
                Code: $("#ddlStateEmployeeMaster").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                LocationID: $("#ddlLocationEmployeeMaster").val(),
                ClientID: queryString
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindBranchListContractor",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    //$("#ddlBranchEmployeeMaster").empty();
                    //$("#ddlBranchEmployeeMaster").append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#ddlBranchEmployeeMaster").append($("<option></option>").val(value.ID).html(value.Name));
                    //})
                    //$("#ddlBranchEmployeeMaster").select2();

                    $("#ddlBranchEmployeeMaster").empty();
                    $("#ddlBranchEmployeeMaster").val("");
                    $("#ddlBranchEmployeeMaster").append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#ddlBranchEmployeeMaster").append($("<option></option>").val(value.ID).html(value.Name));
                        })

                    }
                    $("#ddlBranchEmployeeMaster").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindBranchListEmployeeMaster(ForContractor) {
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')

            var BranchObj = {
                Code: $("#ddlPrincipleEmployerStateMaster").val(),
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                LocationID: $("#ddlPrincipleEmployerLocationMaster").val(),
                ClientID: queryString
            }
            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindBranchList",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    //var Location = JSON.parse(data.d);
                    debugger;

                    var controlId = "ddlPrincipleEmployerBranchMaster";
                    if (ForContractor == "Y")
                        controlId = "ddlBranchContractor";


                    //$("#" + controlId).empty();
                    //$("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));
                    //$.each(Location, function (data, value) {
                    //    $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                    //})
                    //$("#" + controlId).select2();

                    $("#" + controlId).empty();
                    $("#" + controlId).val("");
                    $("#" + controlId).append($("<option></option>").val("-1").html("Select Branch"));

                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $.each(State, function (data, value) {
                            $("#" + controlId).append($("<option></option>").val(value.Name).html(value.Name));
                        })

                    }
                    $("#" + controlId).select2();

                },
                failure: function (data) {
                    alert(data);
                }
            });
        }



        function BindEmployeeMasterTable() {
            debugger;
            var location = document.location.href;
            var queryString = location.split('clientid=')[1];
            var queryString = queryString.replace(/[^\w\s]/gi, '')
         

            var DetailsObj = {
                ClientID: queryString,
                PEID: $("#ddlPrincipleEmployerMaster").val(),
                State: $("#ddlPrincipleEmployerStateMaster").val(),
                Location: $("#ddlPrincipleEmployerLocationMaster").val(),
                Branch: $("#ddlPrincipleEmployerBranchMaster").val(),
            }


            $.ajax({
                type: "POST",
                url: "/CLRA/PrincipleEmployeeMasterCreation.aspx/BindEmployeeMasterTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var tblPrincipleEmployerLocationList = JSON.parse(data.d);

                    $("#tblEmployeeMaster").kendoGrid({
                        dataSource: {
                            data: tblPrincipleEmployerLocationList,
                            serverPaging: false,
                            batch: true,
                            pageSize: 10,
                        },
                        //height: 250,
                        //scrollable: true,
                        //width: 'auto',
                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        pageable: {
                            //numeric: true,
                            pageSize: 10,
                            pageSizes: ["All", 5, 10, 20],
                            buttonCount: 3
                        },
                        height: 475,
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        change: OnChangeEmployeeMaster,
                        columns: [
                            //{ hidden: true, field: "PEID" },
                           // { hidden: true, field: "EMP_PELID" },
                            { selectable: true, width: "5%"},
                            {
                                field: "EmpID", title: "Emp Id", width: "15%", attributes: { style: 'white-space: nowrap;' },
                                filterable: {
                        extra: false,
                        multi: true,
                        search: true,
                        operators: {
                            string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                            }
                        }
                    }
                            },
                            { field: "EmpName", title: "Emp Name", width: "25%", attributes: { style: 'white-space: nowrap;' },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            { field: "State", title: "State/Location/Branch", width: "45%", attributes: { style: 'white-space: nowrap;' },
                            filterable: {
                                extra: false,
                                multi: true,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                            },
                            //{ field: "Location", title: "Location", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            //{ field: "Branch", title: "Branch", width: "20%" },
                            {
                                field: "TaggedBranch", title: "Tagged Branch", width: "25%",
                                filterable: {
                                    extra: false,
                                    multi: true,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                            //{
                            //    field: "ContractFrom", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract From", width: "20%"
                            //},
                            //{ field: "ContractTo", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract To", width: "20%" },                 
                            //{ field: "ContractEndDate", type: 'date', format: "{0:dd-MMM-yyyy}", title: "Contract End Date", width: "20%" },
                            //{ field: "ReasonForContractEnd", title: "Contract End Reason", width: "20%" },
                            {
                                title: "Action", lock: true, width: "10%;",
                                command:
                                [
                                    { name: "UpdateEmployeeMaster", text: "", iconClass: "k-icon k-i-edit", className: "Update" },
                                    { name: "DeleteEmployeeMaster", text: "", iconClass: "k-icon k-i-delete", className: "Delete" },
                                ]
                            }
                        ],
                        dataBound: onDataBoundTblEmployeeMaster
                        //dataBound: function ()
                        //{
                        //    onDataBoundTblEmployeeMaster();
                        //    var rows = this.items();
                        //    $(rows).each(function () {
                        //        var index = $(this).index() + 1
                        //            + ($("#tblEmployeeMaster").data("kendoGrid").dataSource.pageSize() * ($("#tblEmployeeMaster").data("kendoGrid").dataSource.page() - 1));;
                        //        var rowLabel = $(this).find(".row-number");
                        //        $(rowLabel).html(index);
                        //    });

                        //    $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                        //    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                        //    $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                        //}
                    });
                    //$("#tblEmployeeMaster").kendoTooltip({
                    //    filter: ".k-ScheduleAudit",
                    //    content: function (e) {
                    //        return "";
                    //    }
                    //});


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }



        function OnChangeEmployeeMaster(arg) {
            // alert("hi");
            var rows = arg.sender.select();
            EmployeeMasterListData = [];
            rows.each(function (e) {
                var tblEmployeeRecord = $("#tblEmployeeMaster").data("kendoGrid");
                var dataItem = tblEmployeeRecord.dataItem(this);

                var SelecteddataItem = {
                    EmpID: dataItem.EmpID,

                };
                debugger;
                EmployeeMasterListData.push(SelecteddataItem);
            })
        }

        function onDataBoundTblEmployeeMaster(e) {
            var data = this.dataSource.view();
            for (var i = 0; i < data.length; i++) {
                var uid = data[i].uid;
                var row = this.table.find("tr[data-uid='" + uid + "']");

                if (data[i].TaggedBranch == null || data[i].TaggedBranch == "") {
                    row.find(".Update").hide();
                    row.find(".Delete").hide();
                }
            }

            //var rows = this.items();
            //$(rows).each(function () {
            //    var index = $(this).index() + 1
            //        + ($("#tblEmployeeMaster").data("kendoGrid").dataSource.pageSize() * ($("#tblEmployeeMaster").data("kendoGrid").dataSource.page() - 1));;
            //    var rowLabel = $(this).find(".row-number");
            //    $(rowLabel).html(index);
            //});

            //$(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
            //$(".k-grid-edit").find("span").addClass("k-icon k-edit");
            //$(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
        }




        function displayErrors(control, msg) {
            $("#" + control).append(msg);
            $("#" + control).append("</br>");
            $("#" + control).show();
        }

        


        function ValidateEmployeeMigration() {
            var status = true;
            $("#divErrorsEmployees").html("");
            $("#divErrorsEmployees").addClass('alert-danger');


            var fromDate = $("#txtEmployeeContractForm").val();
            var processedFromDate = process(fromDate);

            var toDate = $("#txtEmployeeContractTo").val();
            var processedToDate = process(toDate);

            var endDate = $("#txtContractEndDate").val();
            var processedEndDate = process(endDate);

            var PEContractFrom = $("#txtPEContractFrom").val();
            var processedPEContractFrom = process(PEContractFrom);

            var PEContractTo = $("#txtPEContractTo").val();
            var processedPEContractTo = process(PEContractTo);


            if (($("#hdMode").val() == "A") && ($("#ddlStateEmployeeMaster").val() == "" || $("#ddlStateEmployeeMaster").val() == "-1" || $("#ddlStateEmployeeMaster").val() == null)) {
                displayErrors("divErrorsEmployees", "Please Select State");
                status = false;
            }
            if (($("#hdMode").val() == "A") && ($("#ddlLocationEmployeeMaster").val() == "" || $("#ddlLocationEmployeeMaster").val() == "-1" || $("#ddlLocationEmployeeMaster").val() == null)) {
                displayErrors("divErrorsEmployees", "Please Select Location");
                status = false;
            }
            if (($("#hdMode").val() == "A") && ($("#ddlBranchEmployeeMaster").val() == "" || $("#ddlBranchEmployeeMaster").val() == "-1" || $("#ddlBranchEmployeeMaster").val() == null)) {
                displayErrors("divErrorsEmployees", "Please Select Branch");
                status = false;
            }
            if ($("#txtEmployeeContractForm").val() == "") {
                displayErrors("divErrorsEmployees", "Please Select Contract From");
                status = false;
            }
            if ($("#txtEmployeeContractTo").val() == "") {
                displayErrors("divErrorsEmployees", "Please Select Contract To");
                status = false;
            }
            if (processedFromDate < processedPEContractFrom || processedFromDate > processedPEContractTo) {
                displayErrors("divErrorsEmployees", "'Contract From Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
                status = false;
            }
            if (processedToDate < processedPEContractFrom || processedToDate > processedPEContractTo) {
                displayErrors("divErrorsEmployees", "'Contract To Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
                status = false;
            }

            //else if (Date.parse(toDate) < Date.parse(fromDate))
            //{
            //    alert("'Contract To Date' Should Be Greater than 'Contract From Date'");
            //    return false;
            //}
            //else if (Date.parse(fromDate) < Date.parse(PEContractFrom) || Date.parse(fromDate) > Date.parse(PEContractTo))
            //{
            //    alert("'Contract From Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
            //    return false;
            //}
            //else if (Date.parse(toDate) < Date.parse(PEContractFrom) || Date.parse(toDate) > Date.parse(PEContractTo))
            //{
            //    alert("'Contract To Date' Should be between Contract From Date and Contract To Date Of the selected Principle Employer");
            //    return false;
            //}

            if (($("#hdMode").val() == "E") && ($("#txtContractEndDate").val() != "" && (processedEndDate < processedFromDate || processedEndDate > processedToDate))) {
                displayErrors("divErrorsEmployees", "'Contract End Date' Should Be Between Contract From Date and Contract To Date");
                status = false;
            }
            if (($("#hdMode").val() == "E") && ($("#txtContractEndDate").val() != "" && $("#txtContractEndReason").val() == "")) {
                displayErrors("divErrorsEmployees", "Please Enter Contract End Reason");
                status = false;
            }
            return status;
        }


        function process(date) {
            var parts = date.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

       


    </script>
  
<script type="text/javascript">





    $(document).ready(function () {

        $('#ModalEmployeeMigrate').on('hidden.bs.modal', function () {
            clrMigrationDetails();
        })

      

    });
    

   

    $("#btnUploadEmployee").kendoTooltip({
        content: function (e) {
            return "Upload Bulk";
        }
    });

    $("#btnMigrate").kendoTooltip({
        content: function (e) {
            return "Migrate Employees";
        }
    });

    $("#btnNewPrincipleEmployer").kendoTooltip({
        content: function (e) {
            return "Add New Principle Employer";
        }
    });

    

</script>

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--</head>--%>

        <%--<form runat="server" id="formvalidation">--%>
            <%--  <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
    <!-- partial:index.partial.html -->
    
  <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li id="liPECreation" class="listItem pemt"> <%--class="active"--%>
                       <a href="../CLRA/PrincipleEmployer.aspx" onclick="<%--PECreation();--%>">Principle Employer</a>
                    </li>
                   <li id="liPELocationCreation" class="listItem pemt">
                         <a data-href="#" onclick="PELocationCreation();">Principle Employer Location</a>
                    
                    </li>
                   <li id="liPEMaster" class="listItem pemt">
                             <a data-href="#" onclick="PEMaster();">Principle Employee Master</a>                 
                    </li>
                    <li id="liContractorDetails" class="listItem pemt">
                             <a data-href="#" onclick="ContractorDetails();">Contractor Details</a>                 
                    </li>
                    <li id="liPEReport" class="listItem pemt">
                             <a href="#" onclick="CLRAactivationpage();">Compliance Assignment & Activation</a>    
                             <%--<input type="text" id="txtRohan" value="checkR" />    --%>         
                    </li>
                   
                    <%--<li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                        <input type="text" id="txtRohan" value="check" />
                    </li>--%>
                </ul>
            </header>
        </div>
    <div class="contentx">
        <!--content inner-->
        <div class="content__inner">
            <div class="container">
            </div>
            <div class="containerx overflow-hidden">
                <!--multisteps-form-->
                <!--form panels-->
                <div class="row">
                    <div class="col-12 col-lg-12 m-auto">
                       <%--   <form class="multisteps-form__form">
                          form--%>
                            <!--single form panel-->
                          
                            <!--single form panel-->
                            <div id="divPEMaster" data-animation="scaleIn">
                                <h3 class=""></h3>
                                <div class="row alert alert-danger" id="divCheckEmployees">
                                    <div class="col-md-12">
                                        <div>
                                            <strong></strong><span></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                            <label style="color: black;">Principle Employer</label>
                                            <select class="form-control" id="ddlPrincipleEmployerMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divStateMaster">
                                            <label style="color: black;">Contractor state</label>
                                            <select class="form-control" id="ddlPrincipleEmployerStateMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 form-group hidden" id="divLocationMaster">
                                            <label style="color: black;">Contractor Location</label>
                                            <select class="form-control" id="ddlPrincipleEmployerLocationMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divBranchMaster">
                                            <label style="color: black;">Contractor Branch</label>
                                            <select class="form-control" id="ddlPrincipleEmployerBranchMaster">
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 form-group hidden" id="divMigrateMaster">
                                            <button class="btn btn-primary" type="button" id="btnMigrate" title="Migrate">Migrate</button>

                                            <%--<button class="btn btn-primary" type="button" id="btnUploadEmployee" title="Upload">Upload</button>--%>
                                          
                                        
                                            <button class="btn btn-primary" type="button" id="btnUploadEmployee" title="Upload"><span class="k-icon k-i-upload"></span></button>                                      
                                        	<button class="btn btn-primary" runat="server" type="button"  id="btnDownloadEmployeedetails" title="Download Employee Report" onserverclick="btnDownload_Click"><span class="k-icon k-i-excel"></span></button>
											<asp:HiddenField runat="server" ID="PEIDValue" ClientIDMode="Static"  Value="0"/>
                                          <%--<input type="hidden" id="PEIDValue" name="PEIDValue" value="" />--%>
                                             </div>
                                    </div>
                                    <div class="row">
                                        <%--<div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <div id="tblEmployeeMaster" class="table-responsive"></div>
                                            </div>--%>



                                        <div class="col-md-12">
                                            <div id="tblEmployeeMaster">
                                                <div class="k-header k-grid-toolbarx">
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                 
                                </div>
                            </div>
                         
                        <%--form ends--%>
                    </div>
                </div>
            </div>
        </div>	
    </div>
   <%-- </div>--%>  <%--13-10-2020--%>
    <!-- partial -->
    <script src="script.js"></script>

   
    <div class="modal  modal-fullscreen pem" id="UploadEmployeeModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 400px; margin-left: -650px; width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                    
                          
                            <asp:UpdatePanel ID="updatepnl" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:FileUpload ID="ContractFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click" ValidationGroup="uploadUtilityValidationGroup" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                            <%--<a href="SampleFormat/ContractBulkUpload.xlsx"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <%--<a id="btnDownloadSample" onclick="downloadSample()"><i class="fa fa-file-excel-o"></i>Download Sample Format</a>--%>
                                            <asp:LinkButton ID="lnkDownload" class="fa fa-file-excel-o" Text="Download Sample Format" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUploadExcel" />
                                    <%--<asp:AsyncPostBackTrigger ControlID="btnUploadExcel" EventName = "Click" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="modal  modal-fullscreen pem" id="ModalEmployeeMigrate" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" style="height: 350px; width: 710px; margin-left: -650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Employee Migration to Principle Employer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" id="divMsgEmployees">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    <strong></strong><span id="spanMsgEmployees"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row alert" id="divErrorsEmployees">
                            <div class="col-md-12">
                                <div>
                                    <strong></strong><span></span>
                                </div>
                            </div>
                        </div>


                        <div id="divMigrateLocation" class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Principle State <span style="color: red;">*</span></label>
                                <select class="form-control" id="ddlStateEmployeeMaster">
                                </select>
                                <label style="color: black;" id="lblEmployeeID" class="hidden">0</label>
                                <label style="color: black;" id="lblPrincEmpId" class="hidden">0</label>
                                <label style="color: black;" id="lblEMP_PELID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Principle Location <span style="color: red;">*</span></label>
                                <select class="form-control" id="ddlLocationEmployeeMaster">
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Principle Branch <span style="color: red;">*</span></label>
                                <select class="form-control" id="ddlBranchEmployeeMaster">
                                </select>
                            </div>
                        </div>



                        <%--<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract From*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtEmployeeContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract To*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtEmployeeContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>--%>



                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract From <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="From-Date" id="txtEmployeeContractForm" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract To <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="To-Date" id="txtEmployeeContractTo"/>
                            </div>
                        </div>

                        <%--<div id="divContractEnd" class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Date*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractEndDate" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Reason*</label>
                                <input type="text" id="txtContractEndReason" class="form-control" />
                            </div>
                        </div>--%>

                        <div id="divContractEnd" class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Date<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="Contract-End-Date" id="txtContractEndDate" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Contract End Reason<span style="color: red;">*</span></label>
                                <input type="text" id="txtContractEndReason" class="form-control" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnMigrateSave" style="margin-top:30px;" type="button" value="Submit" class="btn btn-primary">Save</button>
                                <input type="hidden" id="hdMode" name="hdMode" value="" />
                                <input type="hidden" id="txtPEContractFrom" name="txtPEContractFrom" value="" />
                                <input type="hidden" id="txtPEContractTo" name="txtPEContractTo" value="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="divReAssignCompliance">
        <iframe id="iframeReAssign" style="width: 100%; height: 300px; border: none;"></iframe>
    </div>

   


<%--</form>--%>


    </asp:Content>
<%--</html>--%>
    
    
    