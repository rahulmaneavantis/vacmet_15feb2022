﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <%--<link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
    <title></title>
      <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
      .MainContainer
    {
       
        /*height:1000px;*/
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1200px;

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                    

                 <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../HelpManagement/dashboard.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>

          <div class="col-sm-9 MainContainer" style="line-height:25px;border:groove;border-color:#f1f3f4">
    
              <h3>My Documents</h3><br />
     <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Compliance Document</b>
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					 
                <p>You can view and download compliance documents, also you view overview the compliance details.Follow the below process,  </p>
               <ul>
                   <li style="list-style-type:decimal">Select Compliance Document in My Document.   </li>
                   <li style="list-style-type:decimal">You will be redirected to page featuring list of compliance that are assigned to you. </li>
                   <li style="list-style-type:decimal">To find particular compliance you can set filter and Advanced Search.</li>
                   <li style="list-style-type:decimal">To view the documents, click on view icon and to download the documents, click on download icon. Also, you can view the compliance details by click on overview icon.  </li><br />
                   <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Compliance%20Document_Screenshot.png" />
               </ul>
                </div>
                
			</div>

	</div>

         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Critical Documents</b>
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
					<p>Critical document is personal folder for user, you can store your documents and these documents will not be accessible by any other user. You can create and search folder and upload file. Following are the steps,  </p> 
                    <ul>
                         <li style="list-style-type:decimal">Select Critical document in My Document  </li>
                         <li style="list-style-type:decimal">To Create - Click on New button, select New folder and in pop-up window enter the name of folder. </li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Critical%20Document_1_Screenshot.png" /><br />  <br />
                         <li style="list-style-type:decimal">Search - In given search box type the folder name.  </li>
                         <li style="list-style-type:decimal">Share- Click on folder and a Share icon will appear beside New button, click on Share icon.You will be redirected to folder holding all documents. </li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Critical%20Document_2_Screenshot.png" /><br /><br />
                         <li style="list-style-type:decimal">Here click on document to be shared and then click on Share icon, a pop-up will appear select name and hit Share button. </li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Critical%20Document_3_Screenshot.png" /><br /><br />
                         <li style="list-style-type:decimal">Delete- To delete Folder/document, select and hit the Delete icon appeared beside New button. A pop-up will appear to seek confirmation.</li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Critical%20Document_4_Screenshot.png" /><br /><br />
                         <li style="list-style-type:decimal">Upload file - Select folder, click on New button and select New file option. A pop-up window will come to upload file from your system. You can also create folder inside existing folder. </li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Critical%20Document_5_Screenshot.png" /><br /><br />
                         <li style="list-style-type:decimal">You can also Share, download, edit and delete the file. And to do so select file and options will appear beside New button. Click on the icon to Share/download/edit/delete the file.</li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Critical%20Document_6_Screenshot.png" />
                    </ul>   
               </div>

			</div>

	</div>

         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Task Documents </b>
					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
				<p>This section will hold task related document. You can view and download these documents. Following is the process,  </p>	 
                <ul>
                    <li style="list-style-type:decimal">Select Task Document in My Document, you will be redirected to page featuring list of assigned tasks. </li>
                    <li style="list-style-type:decimal">You can set filter to view specific list of tasks and to generate more specific list use Advance Search.  </li>
                    <li style="list-style-type:decimal">To download task details, click on Download icon and to download multiple documents click on Download button.</li><br />
                    <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Task%20Document_Screenshot.png" />
                </ul>
                
                </div>

			</div>

	</div>

         <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFour">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Act Documents</b>
					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body">
					<p>Act Documents allow you to access documents of assigned Act. You can view and download the documents using following steps, </p> 
                    <ul>
                        <li style="list-style-type:decimal">Click on Act Documents in My Documents, you will be redirected to page that features list of Acts.</li>
                        <li style="list-style-type:decimal">To view document, click on View icon and to download document click on Download document.  </li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Act%20Documents_1_Screenshot.png" /><br /><br />
                        <li style="list-style-type:decimal">You can also add and reduce the columns and to do so click on dropdown arrow given in column heading, here move your cursor to Columns option and tick for columns you want to see, and in this way selected columns will be viewed.</li>
                        <li style="list-style-type:decimal">Similarly, you can set filter, click on heading dropdown and move cursor to Filter option and set filter.</li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Documents_Act%20Documents_2_Screenshot.png" />
                    </ul>
           
               </div>

			</div>

	</div>
         
		

	</div><!-- panel-group -->
	
	
</div><!-- container -->
</div>

 </div>
    </form>
   

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
