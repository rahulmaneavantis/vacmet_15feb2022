﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />--%>
   <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>

      <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1395px;

      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
            .MainContainer
    {
       
        height:1395px;
    }

    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1400px;
  width:1200px;
 

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style> 
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                    

                  <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../HelpManagement/dashboard.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>

          <div class="col-sm-9 MainContainer" style="line-height:25px;border:groove;border-color:#f1f3f4">
     
              <h3>My Compliance Calender</h3>
            
       <p style="font-size:14px;">The calendar gives clear vision of compliances- Completed/Overdue/Upcoming/Completed after due date. Day wise you can view the compliances to be completed. </p>
	<ul>
        <li style="list-style-type:decimal">
            The color code helps knowing following status,
            <ul>
                <li style="list-style-type:lower-roman"><b>Green-</b> All compliances are completed </li>
                 <li style="list-style-type:lower-roman"><b>Red-</b> One or more compliances are overdue </li>
                 <li style="list-style-type:lower-roman"><b>Blue-</b> Upcoming compliance</li>
                  <li style="list-style-type:lower-roman"><b>Yellow-</b> One or more compliances are completed after due date   </li><br />
                <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_My%20Compliance%20Calendar_1_Screenshot.png" /><br /><br />
            </ul>
        </li>
        <li style="list-style-type:decimal">The table view in right hand side features current date compliances status and details, to view compliance details click on view icon.</li><br />
        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_My%20Compliance%20Calendar_2_Screenshot.png" /><br /><br />
        <li style="list-style-type:decimal">With the help of radio button, you can choose to view all compliances (with checklist) and only Statutory + Internal compliances. </li><br />
        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_My%20Compliance%20Calendar_3_Screenshot.png" />
	</ul>
</div><!-- container -->
</div>

 </div>
    </form>
</body>
</html>
