﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
  <style>
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1448px;

      
    }
       .MainContainer
    {
       
        height:1448px;
    } 
        .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1450px;
  width:1200px;
  

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>
  
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>
</head>
<body>
    <form id="form1" runat="server">
     <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                    

               <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../HelpManagement/dashboard.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>
         
          <div class="col-sm-9 MainContainer" style="border:groove;border-color:#f1f3f4">
              
              <h3>Summary of Overdue Compliances</h3>
              <p style="font-size:16px;">This section features list of overdue compliances, to view compliance details follow below steps, </p><br />
              <ul>
                   <li style="list-style-type:decimal">Click on Action icon </li><br />
                  <img class="img1" style="width:700px;height:300px; margin-left:30px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Summary%20of%20overdeu%20compliances_1_Screenshot.png" />
                   <br /><br />
                   <li style="list-style-type:decimal"> A window will appear holding tabs- Summary, Details, Historical Documents, Updates, Audit logs. And two sections - Current Documents and Comments, following are the use, </li><br />
                  <img class="img1" style="width:700px;height:350px; margin-left:30px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Summary%20of%20overdeu%20compliances_2_Screenshot.png" /> <br /><br />
                   <li style="list-style-type:decimal"><b>Summary -</b> This will feature basic details and  graphical presentation of historical completion status. </li>
                   <li style="list-style-type:decimal"><b>Details -</b> Compliance complete details will be featured.    </li>  
                   <li style="list-style-type:decimal"><b>Historical Documents-</b> All the documents related to compliance will be featured, you can view the list in ascending and descending order, adjust no. of columns and set filter.</li> 
                   <li style="list-style-type:decimal"><b>Updates -</b> The updates related to compliance will be featured here, you can view the list in ascending and descending order, adjust no. of columns and set filter.</li> 
                   <li style="list-style-type:decimal"><b>Current documents-</b> You can view the related documents in this section.  </li> 
                   <li style="list-style-type:decimal"><b>Comment -</b> To comment this section will be used, you can address particular user and comment. To do this, type "<@username> <your comment>" post. </li>           
    
              </ul>
              <p style="font-size:16px;">To view all overdue compliances, click on Show more link  </p>
              <img class="img1" style="width:700px;height:300px; margin-left:30px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_Summary%20of%20overdeu%20compliances_3_Screenshot.png" />
            </div>
    </div>

   </div>
    </form>
</body>
</html>
