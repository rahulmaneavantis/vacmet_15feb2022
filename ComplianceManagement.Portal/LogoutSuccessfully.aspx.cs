﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class LogoutSuccessfully : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
           
            }
        }

        protected void lnklogin_Click(object sender, EventArgs e)
        {
            try
            {                
                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                FormsAuthentication.SignOut();
                Session.Abandon();              
                FormsAuthentication.RedirectToLoginPage();    

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }


    }
}