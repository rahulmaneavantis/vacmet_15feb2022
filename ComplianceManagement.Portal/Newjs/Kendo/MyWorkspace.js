﻿function FilterAllMain() {

    //location details
    var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
    var locationsdetails = [];
    $.each(list1, function (i, v) {
        locationsdetails.push({
            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
        });
    });

    //risk Details
    var Riskdetails = [];
    var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    $.each(list3, function (i, v) {
        Riskdetails.push({
            field: "Risk", operator: "eq", value: parseInt(v)
        });
    });
    var ComplianceFilter = [];
    if ($("#txtSearchComplianceID").val() != "") {
        ComplianceFilter.push({
            field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
        });
    }


    var dataSource = $("#grid").data("kendoGrid").dataSource;

    if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
        && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }
    else if (ComplianceFilter.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: ComplianceFilter
                }
            ]
        });
    }
    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
    var dataSource = $("#grid").data("kendoGrid").dataSource;
    if (dataSource._total > 20 && dataSource.pageSize == undefined) {
        dataSource.pageSize(total);
    }
}

function FilterAllAdvancedSearch() {
    
    //location details
    var locationsdetails = [];
    if ($("#dropdowntree1").data("kendoDropDownTree") != undefined) {
        locationsdetails = $("#dropdowntree1").data("kendoDropDownTree")._values;
    }

    //risk Details
    var Riskdetails = [];
    if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined) {
        Riskdetails = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
    }

    var finalSelectedfilter = { logic: "and", filters: [] };

    if (locationsdetails.length > 0
        || Riskdetails.length > 0
        || ($("#dropdownlistStatus1").val() != undefined && $("#dropdownlistStatus1").val() != null && $("#dropdownlistStatus1").val() != "")
        || $("#txtSearchComplianceID1").val() != ""
        || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
        || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")
        || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
        || ($("#dropdownDept").val() != undefined && $("#dropdownDept").val() != null && $("#dropdownDept").val() != "")
        || ($("#dropdownACT").val() != undefined && $("#dropdownACT").val() != null && $("#dropdownACT").val() != ""))
    {
        if ($("#dropdownDept").val() != undefined && $("#dropdownDept").val() != null && $("#dropdownDept").val() != "") {
            var DeptFilter = { logic: "or", filters: [] };
            DeptFilter.filters.push({
                field: "DeptId", operator: "eq", value: parseInt($("#dropdownDept").val())
            });
            finalSelectedfilter.filters.push(DeptFilter);
        }

        if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
            var ActFilter = { logic: "or", filters: [] };
            ActFilter.filters.push({
                field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
            });
            finalSelectedfilter.filters.push(ActFilter);
        }
        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
            var DateFilter = { logic: "or", filters: [] };
            DateFilter.filters.push({
                field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MMM-yyyy')
            });
            finalSelectedfilter.filters.push(DateFilter);
        }
        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
            var DateFilter = { logic: "or", filters: [] };
            DateFilter.filters.push({
                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MMM-yyyy')
            });
            finalSelectedfilter.filters.push(DateFilter);
        }

        if ($("#dropdownlistStatus1").val() != undefined && $("#dropdownlistStatus1").val() != null && $("#dropdownlistStatus1").val() != "") {
            var StatusFilter = { logic: "or", filters: [] };
            StatusFilter.filters.push({
                field: "Status", operator: "eq", value: $("#dropdownlistStatus1").data("kendoDropDownList").text()
            });
            finalSelectedfilter.filters.push(StatusFilter);
        }


        if (Riskdetails.length > 0) {
            var RiskFilter = { logic: "or", filters: [] };
            $.each(Riskdetails, function (i, v) {
                RiskFilter.filters.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });
            finalSelectedfilter.filters.push(RiskFilter);
        }

        if (locationsdetails.length > 0) {
            var LocationFilter = { logic: "or", filters: [] };

            $.each(locationsdetails, function (i, v) {
                LocationFilter.filters.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(LocationFilter);
        }

        if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
            var SeqFilter = { logic: "or", filters: [] };
            SeqFilter.filters.push({
                field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
            });
            finalSelectedfilter.filters.push(SeqFilter);
        }
        if ($("#txtSearchComplianceID1").val() != "") {
            var RiskFilter = { logic: "or", filters: [] };
            RiskFilter.filters.push({
                field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID1").val()
            });
            finalSelectedfilter.filters.push(RiskFilter);
        }
        if (finalSelectedfilter.filters.length > 0) {
            var dataSource = $("#grid1").data("kendoGrid").dataSource;
            dataSource.filter(finalSelectedfilter);
        }
        else {
            $("#grid1").data("kendoGrid").dataSource.filter({});
        }
    }
    else {
        $("#grid1").data("kendoGrid").dataSource.filter({});
    }
    var dataSource = $("#grid1").data("kendoGrid").dataSource;
    if (dataSource._total > 20 && dataSource.pageSize == undefined) {
        dataSource.pageSize(total);
    }
}

function fcloseStory(obj) {

    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

}

function fCreateStoryBoard(Id, div, filtername) {

    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
    $('#' + div).css('display', 'block');

    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filtertype') {
        $('#' + div).append('Type&nbsp;:');//Dashboard               
    }
    else if (div == 'filterrisk') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom:4px;border-radius:10px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }

}