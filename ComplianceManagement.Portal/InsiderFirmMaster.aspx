﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderFirmMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderFirmMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
    .k-i-arrow-60-down{
        margin-top:5px;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div class="col-sm-12">
                     <div class="col-sm-12" style="padding:0">
                <div class="toolbar">  
                    <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                <input id="dropdownlistComplianceType" data-placeholder="Type" style="width:172px;">
                    <button  style="float:right;"  id="button" type="button" class="btn btn-primary"  onclick="OpenAdvanceSearch(event)" >Add New</button> 
                </div>
                 
                        
            </div>
        <div class="col-sm-12" style="margin-bottom:1%;" >
            
        </div>

    <div class="col-sm-12" id="grid"></div>
  </div>    
        <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div  class="col-sm-12" style="padding:0">
                       <form  class="form-inline" method="POST" id="fcontact"  >
                  
            <div class="col-sm-12" style="padding:0;padding-top:10px;">
                <div class="col-sm-6  required " style="padding:0">
                    <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                    <input id="dropdownlistComplianceType1" style="width:40%" data-placeholder="Company" >
                </div>
                <div class="col-sm-6  required " style="padding:0">
                    <label class="control-label col-sm-4" style="font-weight:450;padding-top: 5px;" for="sel1">Auditor Type: &nbsp; </label>
                    <!--<input id="dropdownrelation" style="width:40%" data-placeholder="Company" > -->

                    <select name="author" id="dropdownrelation" style="width:39%" data-placeholder="Company" onchange='CheckAuthor(this.value);'>
                        <option value="Statutory Auditor">Statutory Auditor</option>
                        <option value="Secretarial Audiotor">Secretarial Audiotor</option>
                        <option value="Cost Auditor">Cost Auditor</option>
                        <option value="Internal Auditor">Internal Auditor</option>
                        <option id="other" value="others">Other</option>
                    </select> 

                    <input type="text" name="author" id="other_type" style="display: none; width: 38%; margin-top: 5px; padding-left: 5px;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Other" autocomplete="off" />
                </div>
                <div style ="display:none; color:red; margin-left: 51%; "id="form-error"><strong>* </strong> Auditor-Type cannot be blank!</div>

                <div class="col-sm-6  required " style="padding:0;margin-top:10px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Firm/Auditor Name:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="firmname" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Firm Name" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px;">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Registration No:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="registrationno" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Registration No" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">PAN:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%"><input id="pan" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="PAN" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Email:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="email" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Email" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
            <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Mobile:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="mobile" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Mobile" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
             <div class="col-sm-6  required " style="padding:0;margin-top:10px">
                <label class="col-sm-4 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">Address:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 40%;"><input id="address" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Address" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>   
            </div>
            
            
          

                 
                
                   
           
            
            </div>
            <button id="saveBtn" style="margin-left:43%;margin-top:20px;" type="submit" onclick="saveDetails(event)" class="btn btn-primary">Save</button>
                                        
                       </form>
                      </div>
           </div>
    <input type="hidden" id="editFlag" value="0" />
        <script>
       
        $(document).ready(function () {
            $("#pagetype").text("Firm Master");
            
            $("#grid").kendoGrid({

                dataSource: {
                    transport: {
                        read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmtable/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>
                        },
                    schema: {
                        data: function (response) {
                            return response.data;
                        },
                        total: function (response) {
                            return response.total;
                        },
                        model: {
                            fields: {
                                FirmName: { type: "string" },
                                RegistrationNo: { type: "string" },
                                Address: { type: "string" },
                                PAN: { type: "string" },
                                
                            }
                        }
                    },
                    pageSize: 10
                },
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                //height: 550,
                //toolbar: ["search"],
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{

                    field: "FirmName",
                    title: "Firm Name",
                    width: 150

                }, {

                        field: "AuditorType",
                        title: "Auditor Type",
                        width: 100

                },
                    {

                        field: "CompanyName",
                        title: "Company Name",
                        width: 150

                    },{

                        field: "RegistrationNo",
                    title: "Registration No",
                    width: 120

                }, {

                        field: "PAN",
                    title: "PAN",
                    width: 90

                    }, {

                        field: "Email",
                        title: "Email",
                        width: 120

                    }, {

                        field: "Mobile",
                        title: "Mobile",
                        width: 100

                    }, {

                        field: "Address",
                        title: "Address",
                    width: 150

                    }, {
                        template: "<div class='customer-photo'" +
                            "style='background-image: url(../../Images/edit_icon.png);'" + " onClick='OpenAdvanceSearch(#= JSON.stringify(data) #)' >",
                        field: "",
                        title: "Action",
                        width: 80
                    }]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompany/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                }
            });

           /* $("#dropdownrelation").kendoDropDownList({
                placeholder: "",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory Auditor", value: "Statutory Auditor" },
                    { text: "Secretarial Audiotor", value: "Secretarial Audiotor" },
                    { text: "Cost Auditor", value: "Cost Auditor" },
                    { text: "Internal Auditor", value: "Internal Auditor" }
                ],
            });*/
            

            $("#dropdownlistComplianceType").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompanyall/?customer_id=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>",
                        }
                    }
                },
                change: function (e) {
                    //alert(e.sender.value());
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getfirmtablespecific/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + this.value()
                    },
                    schema: {
                        data: function (response) {
                            return response.data;
                        },
                        total: function (response) {
                            return response.total;
                        },
                        model: {
                            fields: {
                                Name: { type: "string" },
                                Relation: { type: "string" },
                                RelationCompany: { type: "string" },
                                Address: { type: "string" },
                                CIN: { type: "string" },

                            }
                        }
                    }


                });
                var grid = $("#grid").data("kendoGrid");
                grid.setDataSource(dataSource);
                // Use the value of the widget
            }
                
            });

        });
        </script>
    <script>
        function CheckAuthor(val) {
            let other1 = document.getElementById('other');

            var element = document.getElementById('other_type');
            element.style.display = 'none';


            if (val == "Statutory Auditor" || val == "Secretarial Audiotor" || val == "Cost Auditor" || val == "Internal Auditor") {
                //$("#dropdownrelation").val(val.AuditorType);
                element.style.display = 'none';
                $("#other_type").val(null);
            }
            else {
                element.style.display = 'block';
                //$("#dropdownrelation").val(dd.AuditorType);
                //$("#other_type").val(dd.AuditorType);
            }


            //if (val == 'others')
            //    element.style.display = 'block';
            //else
            //    element.style.display = 'none';

            element.addEventListener('blur', () => {
                let txt1 = element.value
                other1.value = txt1
            }) 
        }
    </script>

    
<script>

    function OpenAdvanceSearch(dd) {
        // alert(JSON.stringify(dd));
        if (dd.FirmName != undefined) {
            $('#editFlag').val(dd.ID);
        } else {
            $('#editFlag').val(0);
        }
        $('#firmname').val(dd.FirmName);
        $('#registrationno').val(dd.RegistrationNo);
        $('#pan').val(dd.PAN);
        $('#email').val(dd.Email);
        $('#mobile').val(dd.Mobile);
        $('#address').val(dd.Address);
       // $("#dropdownrelation").data("kendoDropDownList").value(dd.AuditorType);
       // $("#dropdownrelation").val(dd.AuditorType);
        if (dd.AuditorType == "Statutory Auditor" || dd.AuditorType == "Secretarial Audiotor" ||dd.AuditorType == "Cost Auditor" ||dd.AuditorType == "Internal Auditor") {
            $("#dropdownrelation").val(dd.AuditorType);
            $("#other_type").css("display","none")
        }
        else {
            $("#dropdownrelation").val(dd.AuditorType);
            $("#other_type").val(dd.AuditorType);

        }

        $("#dropdownlistComplianceType1").data("kendoDropDownList").value(dd.CompanyID);
        //alert(dd.CompanyID)
        var myWindowAdv = $("#divAdvanceSearchModel");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "65%",
            height: "40%",
            title: "Add New Firm",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                "Maximize",
                "Close"
            ],
            close: onClose
        });
        $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
        //e.preventDefault();
        return false;
    }
   

    function OpenAdvanceSearchFilter(e) {
        $('#divAdvanceSearchFilterModel').modal('show');
        e.preventDefault();
        return false;
    }
</script>
    
    <script>
        $("#dropdownRole").kendoDropDownList({
            placeholder: "",
            dataTextField: "text",
            dataValueField: "value",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            dataSource: [
                { text: "DP", value: "1" },
                { text: "HDCS", value: "2" },
            ],
            index: 0,
            change: function (e) {
                

            }

        });
    </script>
    <script>
        function saveDetails(e) {
            e.preventDefault();
            //confirm("Are you sure want to add details?");
            var editFlag = $("#editFlag").val();
            var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
            var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
            var firmname = $('#firmname').val();
            var registrationno = $('#registrationno').val();
            var pan = $('#pan').val();
            var address = $('#address').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            var auditorType = $('#dropdownrelation').val();
            var companyid = $('#dropdownlistComplianceType1').val();
            var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";
            //var pdata = ;

            //alert(pdata);
            
            if ((auditorType == null) || (auditorType == 'others') || (auditorType == '')) {
                $('#form-error').css("display", "block");
                setTimeout(function () {
                    $('#form-error').css("display", "none");
                }, 3000);
                return false;
            }

            
            $.ajax({
                type: 'post',
                url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createfirm/',
                data: { "user_id": user_id, "firmname": firmname, "customer_id": customer_id, "registrationno": registrationno, "address": address, "email": email, "mobile": mobile, "auditortype": auditorType, "companyid": companyid, "created_by": created_by, "pan": pan, "editflag": editFlag  },
                success: function (result) {
                    alert(result.data);
                    $("#other_type").val(null)
                    location.reload()

                },
                error: function (e, t) { alert('Something went wrong'); }
            });
            
            return false;
        }
    </script>
</asp:Content>
