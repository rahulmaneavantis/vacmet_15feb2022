﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class VendorAudit : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string user_Roles;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                customerid =Convert.ToInt32(AuthenticationHelper.CustomerID); //UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                userid = AuthenticationHelper.UserID;
                Page.Header.DataBind();
                if (Session["LastLoginTime"] != null)
                {
                    LastLoginDate = Session["LastLoginTime"].ToString();
                }

                if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                {
                    if (AuthenticationHelper.UserID != -1)
                    {                        
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                        if (cname != null)
                        {
                            CustomerName = cname;
                        }
                    }
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                   
                    if (GetApprover.Count > 0)
                    {
                        Approveruser_Roles = "APPR";
                    }
                    else
                    {
                        Approveruser_Roles = "";
                    }
                }
                user_Roles = AuthenticationHelper.Role;
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser.CustomerID == null)
                {
                    checkTaskapplicable = 2;
                }
                else
                {
                    var IsTaskApplicable = CustomerManagement.GetByTaskApplicableID(Convert.ToInt32(LoggedUser.CustomerID));
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    //Customer customer = CustomerManagement.GetByID(Convert.ToInt32(LoggedUser.CustomerID));
                    //if (customer.TaskApplicable != null)
                    //{
                    //    checkTaskapplicable = Convert.ToInt32(customer.TaskApplicable);
                    //}
                }
                TaskRoles = TaskManagment.GetAssignedTaskUserRole(AuthenticationHelper.UserID);

                if (LoggedUser.ImagePath != null)
                {
                    //ProfilePic.Src = LoggedUser.ImagePath;
                    ProfilePicTop.Src = LoggedUser.ImagePath;
                    ProfilePicSide.Src = LoggedUser.ImagePath;
                }
                else
                {
                    //ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicSide.Src = "~/UserPhotos/DefaultImage.png";
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}