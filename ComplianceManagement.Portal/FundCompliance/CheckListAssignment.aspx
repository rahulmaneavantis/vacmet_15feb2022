﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="CheckListAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance.CheckListAssignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

      <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 200px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>



    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Assign Checklist');

            BindLocation();
            PerformerUser();
            ReviewerUser();
            BindFrequency();
            BindType();
            BindYear();
            BindPeriods("C");
        });

        function BindInvestor()
        {
            $("#dropdownInvestorlist").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "InvestorName",
                dataValueField: "ID",
                optionLabel: "Select Investor Name",
                change: function (e) {
                    
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/BindDocumentSideLetterByFundDoc?CId=<% =customerid%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
        }

        function BindLocation() {
        $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                //checkboxes: {
                //    checkChildren: true
                //},
                //checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                 
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetBindFundDocumentLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Role=CADMN',
                            <%--url: '<% =Path%>Data/GetLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S',--%>
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }
        function BindFrequency() {

            $("#ddlFrequency").kendoDropDownList({
                placeholder: "Select Frequency",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Frequency",
                autoClose: true,
                dataSource: [
                    { text: "Annually", value: "A" },
                    { text: "Half Yearly", value: "H" },
                    { text: "Quarterly", value: "Q" },
                    { text: "Monthly", value: "M" },
                    { text: "Custom", value: "C" }
                ],
                index: 0,
                change: function (e) {
                    BindPeriods($("#ddlFrequency").val());
                }
            });
        }
        function BindYear() {
            $("#dropdownlistYear").kendoDropDownList({
                placeholder: "Select Year",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Year",
                autoClose: true,
                dataSource: [
                    { text: "2018", value: "2018" },
                    { text: "2019", value: "2019" },
                    { text: "2020", value: "2020" },
                    { text: "2021", value: "2021" }
                ],
                index: 0,
                change: function (e) {
                  
                }
            });
        }
        function BindType()
        {
            $("#dropdownlist").kendoDropDownList({
                placeholder: "Select Type",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Type",
                autoClose: true,
                dataSource: [
                    { text: "Fund Documents", value: "FD" },
                    { text: "Side Letter", value: "SL" }
                ],
                index: 0,
                change: function (e) 
                {
                   
                    if ($("#dropdownlist").val() == "SL") {
                        $('#divinvestor').css('display', 'block');
                        BindInvestor();
                    }
                    else
                    {
                        $('#divinvestor').css('display', 'none');
                        //$("#dropdownInvestorlist").empty();
                        $("#dropdownInvestorlist").data("kendoDropDownList").value(-1);
                    }                    
                }
            });
        }
        function PerformerUser() {
            $("#ddlFilterPerformer").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Performer",
                autoClose: true,
                change: function (e) {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Litigation/UserList?CutomerID=<% =customerid%>&Flags=" + false
                    },
                }
            });
        }
        function ReviewerUser() {
            $("#ddlFilterReviewer").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Reviewer",
                autoClose: true,
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Litigation/UserList?CutomerID=<% =customerid%>&Flags=" + false
                    },
                }
            });
        }

        function BindPeriods(Frq) {
            
            $('#ddlPeriod').empty();
            if (Frq == "C") {
                $("#ddlPeriod").kendoDropDownList({
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" }
                    ]
                });
            }
            if (Frq == "Q") {
                $("#ddlPeriod").kendoDropDownList({

                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {                                                
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan - Mar", value: "Jan - Mar" },
                        { text: "Apr - Jun", value: "Apr - Jun" },
                        { text: "Jul - Sep", value: "Jul - Sep" },
                        { text: "Oct - Dec", value: "Oct - Dec" }
                    ]
                });                
            }
            if (Frq == "A" || Frq == "Y") {
                $("#ddlPeriod").kendoDropDownList({

                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan - Dec", value: "Jan - Dec" }
                    ]
                });
            }
            if (Frq == "H") {
                $("#ddlPeriod").kendoDropDownList({
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan - Jun", value: "Jan - Jun" },
                        { text: "Jul - Dec", value: "Jul - Dec" }
                    ]
                });
            }
            if (Frq == "M") {
                $("#ddlPeriod").kendoDropDownList({

                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan", value: "Jan" },
                        { text: "Feb", value: "Feb" },
                        { text: "Mar", value: "Mar" },
                        { text: "Apr", value: "Apr" },
                        { text: "May", value: "May" },
                        { text: "Jun", value: "Jun" },
                        { text: "Jul", value: "Jul" },
                        { text: "Aug", value: "Aug" },
                        { text: "Sep", value: "Sep" },
                        { text: "Oct", value: "Oct" },
                        { text: "Nov", value: "Nov" },
                        { text: "Dec", value: "Dec" }
                    ]
                });
            }
        }
       
        function ApplyBtn(e) {
            debugger;
            if ($("#dropdowntree").val() != undefined && $("#dropdowntree").val() != null && $("#dropdowntree").val() != "") 
            {
                if ($("#ddlFilterPerformer").val() != undefined && $("#ddlFilterPerformer").val() != null && $("#ddlFilterPerformer").val() != "") 
                {
                    if ($("#ddlFilterReviewer").val() != undefined && $("#ddlFilterReviewer").val() != null && $("#ddlFilterReviewer").val() != "") 
                    {
                        if ($("#ddlFrequency").val() != undefined && $("#ddlFrequency").val() != null && $("#ddlFrequency").val() != "") 
                        {
                            if ($("#ddlPeriod").val() != undefined && $("#ddlPeriod").val() != null && $("#ddlPeriod").val() != 0) 
                            {
                                if ($("#dropdownlistYear").val() != undefined && $("#dropdownlistYear").val() != null && $("#dropdownlistYear").val() != "") 
                                {
                                    if ($("#ddlFilterPerformer").val() != $("#ddlFilterReviewer").val()) 
                                    {    
                                        BindAssignCheckList();
                                        BindUnAssignCheckList();
                                    } else
                                    {
                                        alert('Please select different Performer and Reviewer.');
                                    } 
                                }
                                else
                                {
                                    alert('Please Select Year.');
                                } 
                            }
                            else
                            {
                                alert('Please Select Period.');
                            } 
                        }
                        else
                        {
                            alert('Please Select Frequency.');
                        } 
                    }
                    else
                    {
                        alert('Please Select Reviewer.');
                    }   
                }
                else
                {
                    alert('Please Select Performer.');
                }    
            }
            else
            {
                alert('Please Select Location');
            }
        }
        function onDataBound_UnAssignCheckList() {
            kendo.ui.progress($("#UnAssignLoader"), false);
        }
         function BindUnAssignCheckList() {
             var grid = $('#tblUnAssignRecord').data("kendoGrid");
             if (grid != undefined || grid != null)
                 $('#tblUnAssignRecord').empty();
             var InvestorID=null;
             if ($("#dropdownlist").val() =="SL")     
             {
                 if ($("#dropdownInvestorlist").val() != undefined && $("#dropdownInvestorlist").val() != null) 
                 {
                     InvestorID = $("#dropdownInvestorlist").val();
                 }
             }
            $("#tblUnAssignRecord").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>data/BindUnAssignCheckList?CID=<% =customerid%>&location=' + $("#dropdowntree").val() + '&DType=' + $("#dropdownlist").val() + '&PID=' + $("#ddlFilterPerformer").val() + '&RID=' + $("#ddlFilterReviewer").val() + '&Frequency=' + $("#ddlFrequency").val() + '&Periods=' + $("#ddlPeriod").val() + '&Years=' + $("#dropdownlistYear").val() + '&InvestorID=' + InvestorID,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    pageSize: 10,

                },
                //dataBound: onDataBound_UnAssignCheckList,
                //height: 300,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                noRecords: true,                
                noRecords: {
                    template: function (e) {
                        return "No data available";
                    }
                },
                //change: onChangeUnAssign,
                columns: [
                    { selectable: true, width: "5%" },
                    //{ hidden: true, field: "Id" },
                   { field: "Clause_ref_num",filterable: { multi: true, search: true }, title: "Relevant Clause", width: "10%" },                    
                    { field: "Comp_Que",filterable: { multi: true, search: true }, title: "Compliance Question Relating to Agreed Term/Conditions", width: "30%" },
                    { field: "InvestorName",filterable: { multi: true, search: true }, title: "Investor Name", width: "10%" },
                ]
            });
            $("#tblUnAssignRecord").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        if (content.length > 1) {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else {
                            e.preventDefault();
                        }
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
           
        }
        function BindAssignCheckList() {
            debugger;
            var grid = $('#tblAssignRecord').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#tblAssignRecord').empty();

            $("#tblAssignRecord").empty();
            var InvestorID=null;
            if ($("#dropdownlist").val() =="SL")     
            {
                if ($("#dropdownInvestorlist").val() != undefined && $("#dropdownInvestorlist").val() != null) 
                {
                    InvestorID = $("#dropdownInvestorlist").val();
                }
            }

            $("#tblAssignRecord").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>data/BindAssignCheckList?CID=<% =customerid%>&location=' + $("#dropdowntree").val() + '&DType=' + $("#dropdownlist").val() + '&PID=' + $("#ddlFilterPerformer").val() + '&RID=' + $("#ddlFilterReviewer").val() + '&Frequency=' + $("#ddlFrequency").val() + '&Periods=' + $("#ddlPeriod").val() + '&Years=' + $("#dropdownlistYear").val() + '&InvestorID=' + InvestorID,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    pageSize: 10,

                },
                noRecords: true,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,

                //multi: true,
                //selectable: true,
                ////dataBound: onDataBound_UnAssignCheckList,
                ////height: 300,
                //sortable: true,
                //filterable: false,
                //columnMenu: true,
                //reorderable: true,
                //resizable: true,
                //pageable:true,
                noRecords: {
                    template: function (e) {
                        return "No data available";
                    }
                },
                //change: onChangeUnAssign,
                columns: [
                    { selectable: true, width: "5%" },
                    //{ hidden: true, field: "Id" },
                    { field: "Clause_ref_num",filterable: { multi: true, search: true }, title: "Relevant Clause", width: "10%" },                    
                    { field: "Comp_Que",filterable: { multi: true, search: true }, title: "Compliance Question Relating to Agreed Term/Conditions", width: "30%" },
                    { field: "InvestorName",filterable: { multi: true, search: true }, title: "Investor Name", width: "10%" },
                ]
            });
           
           
            $("#tblAssignRecord").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        if (content.length > 1) {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else {
                            e.preventDefault();
                        }
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
           
        }

        function SaveUnComplianceList_Click(e)
        {
            debugger;
            e.preventDefault();
            var grid = $('#tblAssignRecord').data("kendoGrid");
            var rows = $('#tblAssignRecord :checkbox:checked');
            var things = [];
            $.each(rows, function () {
                var item = grid.dataItem($(this).closest("tr"));
                things.push(item.ChecklistMappID);
            });
            if (things.length > 0) 
            {
                var InvestorID=null;
                if ($("#dropdownlist").val() =="SL") 
                {    
                    if ($("#dropdownInvestorlist").val() != undefined && $("#dropdownInvestorlist").val() != null) 
                    {
                        InvestorID = $("#dropdownInvestorlist").val();
                    }
                }
                kendo.ui.progress($(".chart-loading"), true);
                $.ajax({
                    //contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    type: "POST",
                    url: '<% =Path%>/ExportReport/SaveUnAssignCheckList',                    
                    data: {
                        UID:<% =UId%>,
                        things: JSON.stringify(things),
                        branchId: $("#dropdowntree").val(),
                        PID: $("#ddlFilterPerformer").val(),
                        RID: $("#ddlFilterReviewer").val(),
                        Year: $("#dropdownlistYear").val(),
                        FreqId: $("#ddlFrequency").val(),
                        Period: $("#ddlPeriod").val(),
                        docID: $("#dropdownlist").val(),
                        CID: <% =customerid%>,
                        InvestID:InvestorID
                    },
                    success: function (json) {
                        kendo.ui.progress($(".chart-loading"), false);
                        if (json.message == "Save Record") {
                            BindAssignCheckList();
                            BindUnAssignCheckList();
                            alert("Checklist Unassigned Successfully.");
                        }
                        else {
                            kendo.ui.progress($(".chart-loading"), false);
                            alert("Error in UnAssigning Checklist");
                        }
                    },
                    failure: function (json) {
                        kendo.ui.progress($(".chart-loading"), false);
                        alert("Error in UnAssigning Checklist");
                    }
                });
            }
            else {
                alert('please select at least one Checklist.');
            }
            //e.preventDefault();
            //return false;
        }

        function SaveComplianceList_Click(e)
        {
        e.preventDefault();
            debugger;
            var grid = $('#tblUnAssignRecord').data("kendoGrid");
            var rows = $('#tblUnAssignRecord :checkbox:checked');
            var things = [];
            $.each(rows, function () {
                var item = grid.dataItem($(this).closest("tr"));
                things.push(item.Id);
            });
            if (things.length > 0) {
                var InvestorID=null;
                if ($("#dropdownlist").val() =="SL") 
                {    
                    if ($("#dropdownInvestorlist").val() != undefined && $("#dropdownInvestorlist").val() != null) 
                    {
                        InvestorID = $("#dropdownInvestorlist").val();
                    }
                }
               

                kendo.ui.progress($(".chart-loading"), true);
                $.ajax({
                    //contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    type: "POST",
                    url: '<% =Path%>/ExportReport/SaveAssignCheckList',                    
                    data: {
                        UID:<% =UId%>,
                        things: JSON.stringify(things),
                        branchId: $("#dropdowntree").val(),
                        PID: $("#ddlFilterPerformer").val(),
                        RID: $("#ddlFilterReviewer").val(),
                        Year: $("#dropdownlistYear").val(),
                        FreqId: $("#ddlFrequency").val(),
                        Period: $("#ddlPeriod").val(),
                        docID: $("#dropdownlist").val(),
                        CID: <% =customerid%>,
                        InvestID:InvestorID
                    },
                    success: function (json) {
                        debugger
                        kendo.ui.progress($(".chart-loading"), false);
                        if (json.message == "Save Record") {
                            alert("Checklist Assigned Successfully.");
                            BindAssignCheckList();
                            BindUnAssignCheckList();                          
                        }
                        else {
                            alert("Error in Assigning Checklist");
                        }                    
                    },
                    failure: function (json) 
                    {
                        kendo.ui.progress($(".chart-loading"), false);
                    debugger
                        alert("Error");
                    }
                });
            }
            else {
                alert('please select at least one Checklist.');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <input type="text" id="products" runat="server" style="display: none;" />
    <%--<div class="mainDiv" style="background-color: #f7f7f7;">--%>
   <div class="row Dashboard-white-widget">
          <div id="example">
        <div class="row">
            <div class="col-md-12 colpadding0">
                <div class="col-md-2">
                    <input id="dropdowntree" style="width:110%;" />
                </div>                
                <div class="col-md-2">
                    <input id="ddlFilterPerformer" style="width:110%;" />
                </div>
                <div class="col-md-2">
                    <input id="ddlFilterReviewer" style="width:110%;" />
                </div>
                <div class="col-md-2">
                    <input id="ddlFrequency" style="width:110%;" />                 
                </div>
               <div class="col-md-2">                    
                    <input id="ddlPeriod" style="width:110%;" />
                </div>
            </div>
        </div>
         <div class="row" style="padding-top: 5px;">
            <div class="col-md-12 colpadding0">             
                <div class="col-md-2">
                    <input id="dropdownlistYear"  style="width:110%;" />
                </div>
                <div class="col-md-2">
                    <input id="dropdownlist" style="width:110%;" />
                </div>
                 <div class="col-md-2" id="divinvestor" style="display:none;">
                  <input id="dropdownInvestorlist"  style="width:110%;" />
                 </div>
                 <div class="col-md-2">
                 </div>
                 <div class="col-md-2">
                
                    
                 </div>
                 <div class="col-md-2">
                    <button id="btnApply" type="button" onclick="ApplyBtn(event)" class="btn btn-primary pull-left MarRight">Apply</button>                                    
                </div>
            </div>
            </div>
             <div class="row Mar-Top15">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#Assign">Assigned</a></li>
                        <li><a data-toggle="tab" href="#UNAssign">Unassigned</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="Assign" class="tab-pane fade in active">
                        <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 3px;">
                                    <button  id="SaveUnComplianceList" class="btn btn-primary pull-right" onclick="SaveUnComplianceList_Click(event)">Unassign</button>                                  
                                </div>
                            </div>
                            <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 3px;">
                                    <div id="tblAssignRecord"></div>
                                    <div class="chart-loading" id="AssignLoader"></div>
                                </div>
                            </div>
                        </div>
                        <div id="UNAssign" class="tab-pane fade">
                            <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 3px;">
                                    <button  id="SaveComplianceList" class="btn btn-primary pull-right" onclick="SaveComplianceList_Click(event)">Assign</button>
                                    <%--<button id="btnSave" type="button" class=""></button>--%>
                                </div>
                            </div>
                            <div class="row Mar-Top15">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 3px;">
                                    <div id="tblUnAssignRecord"></div>
                                    <div class="chart-loading" id="UnAssignLoader"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>         
    </div>
    </div>
    <%--<div class="chart-loading">
    </div>--%>
    <%--</div>--%>
</asp:Content>