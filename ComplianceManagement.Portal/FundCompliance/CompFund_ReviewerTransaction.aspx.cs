﻿using AjaxControlToolkit.Bundling;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.ComplianceFund;

namespace com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance
{
    public partial class CompFund_ReviewerTransaction : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                    && !string.IsNullOrEmpty(Request.QueryString["AID"])
                       && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                            && !string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    BindStatusList();
                    long CheckListID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    long scheduleonid = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    int CheckListStatus = Convert.ToInt32(Request.QueryString["SID"]);
                    if (CheckListStatus == 2 || CheckListStatus == 3)
                    {
                        btnSave.Enabled = true;
                    }
                    else
                    {
                        btnSave.Enabled = false;
                    }
                    if (CheckListID > 0)
                    {
                        GetAllValues(CheckListID);
                        BindTransactions();
                        BindGrid();
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var getChecklist = (from row in entities.CompFund_SP_RecentCheckListStatus(auditid, CheckListID, scheduleonid)
                                                select row).FirstOrDefault();
                            if (getChecklist != null)
                            {
                                chkDocument.Checked = false;
                                if (getChecklist.IsDocument == true)
                                    chkDocument.Checked = true;
                                if (!string.IsNullOrEmpty(Convert.ToString(getChecklist.ResultID)))
                                {
                                    ddlStatus.SelectedValue = Convert.ToString(getChecklist.ResultID);                                    
                                }
                                if (!string.IsNullOrEmpty(getChecklist.Remarks))
                                {
                                    txtRemark.Text = getChecklist.Remarks;
                                }
                            }   
                        }
                    }
                }
            }
        }

        private void BindStatusList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = FundMasterManagement.getStatus();
                    ddlStatus.DataTextField = "Name";
                    ddlStatus.DataValueField = "ID";
                    ddlStatus.DataSource = data;
                    ddlStatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetAllValues(long CheckListID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getChecklist = (from row in entities.CompFund_tbl_Checklist
                                    where row.IsDeleted == false && row.Id == CheckListID
                                    select row).FirstOrDefault();
                if (getChecklist != null)
                {
                    lblClause_ref_num.Text = getChecklist.Clause_ref_num.ToString();
                    lblComp_Que.Text = getChecklist.Comp_Que.ToString();
                    LblInvestorName.Text = string.Empty;
                    if (getChecklist.InvestorID != null && getChecklist.InvestorID != -1)
                    {
                        int investorID = Convert.ToInt32(getChecklist.InvestorID);
                        string investorname = (from row in entities.CompFund_tbl_InvestorMaster
                                               where row.IsDeleted == false && row.ID == investorID
                                               select row.InvestorName).FirstOrDefault();
                        LblInvestorName.Text = investorname;
                    }
                }
            }
        }
        private void BindTransactions()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                   && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    int CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    grdTransactionHistory.DataSource = FundMasterManagement.GetAllTransactionLog(ScheduledOnID, CheckListID, auditid);                    
                    grdTransactionHistory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindGrid()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                      && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    long ChecklistID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = FundMasterManagement.getDocumentData(ChecklistID, ScheduleOnID);                        
                        if (lstChecklistDetails.Count > 0)
                        {
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        else
                        {
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                vdsummary.CssClass = "alert alert-danger";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRemark.Text) && !string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        if (ddlChecklistStatus.SelectedValue == "4" && ddlStatus.SelectedValue == "4")
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Can not save because of Status is Applicable.";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                             && !string.IsNullOrEmpty(Request.QueryString["AID"])
                                && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                             && !string.IsNullOrEmpty(Request.QueryString["CBID"]))
                            {
                                long CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                                long AuditId = Convert.ToInt32(Request.QueryString["AID"].ToString());
                                long ScheduleOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                                long branchID = Convert.ToInt32(Request.QueryString["CBID"].ToString());
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    CompFund_tbl_ChecklistAuditTransaction transaction = new CompFund_tbl_ChecklistAuditTransaction()
                                    {
                                        checklistAuditInstanceID = AuditId,
                                        ChecklistAuditScheduleOnID = ScheduleOnID,
                                        StatusId = Convert.ToInt32(ddlChecklistStatus.SelectedValue),
                                        ResultId = Convert.ToInt64(ddlStatus.SelectedValue),
                                        //StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                                        //ResultId = Convert.ToInt64(ddlChecklistStatus.SelectedValue),
                                        Remarks = txtRemark.Text,
                                        Dated = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                        CreatedByText = AuthenticationHelper.User,
                                        CustomerBranchId = Convert.ToInt64(branchID),
                                        CheckListId = CheckListID,
                                        StatusChangedOn = DateTime.Now
                                    };
                                    //if (!string.IsNullOrEmpty(txtTimeLine.Text))
                                    //{
                                    //    transaction.TimeLine = Convert.ToDateTime(txtTimeLine.Text);
                                    //}
                                    bool saveflag = FundMasterManagement.CreateTransaction(transaction);
                                    if (saveflag)
                                    {
                                        if (ddlChecklistStatus.SelectedValue == "3")
                                        {
                                            //try
                                            //{
                                            //    var userdetil = entities.sp_GetScheduleDetail(Convert.ToInt32(ScheduleOnID)).FirstOrDefault();

                                            //    if (userdetil != null)
                                            //    {
                                            //        string ForMonth = string.Empty;
                                            //        string ActName = string.Empty;
                                            //        string StateName = string.Empty;
                                            //        string NatureOfCompliance = string.Empty;
                                            //        string TypeOfCompliance = string.Empty;

                                            //        long ChklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());

                                            //        var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                            //                            join row1 in entities.RLCS_Act_Mapping
                                            //                            on row.ActID equals row1.AM_ActID
                                            //                            where row.IsDeleted == false && row.Id == ChklistID
                                            //                            select new dataEmail
                                            //                            {
                                            //                                ActName = row1.AM_ActName,
                                            //                                Stateid = row.StateID,
                                            //                                NatureOfCompliance = row.NatureOfCompliance,
                                            //                                TypeOfCompliance = row.TypeOfCompliance
                                            //                            }).FirstOrDefault();

                                            //        if (getChecklist != null)
                                            //        {
                                            //            ForMonth = userdetil.ForMonth;
                                            //            NatureOfCompliance = getChecklist.NatureOfCompliance;
                                            //            TypeOfCompliance = getChecklist.TypeOfCompliance;
                                            //            ActName = getChecklist.ActName;
                                            //            StateName = getChecklist.Stateid;
                                            //        }

                                            //        DateTime SDstartDate = userdetil.ScheduleOn;
                                            //        DateTime today = DateTime.Now;

                                            //        string ClientName = userdetil.ClientName;
                                            //        string Subject = ClientName + " Documents rejected for the period(" + userdetil.ForMonth + ")";
                                            //        string formattedDate = SDstartDate.AddDays(10).ToString("dd-MM-yyyy");

                                            //        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_RLCSRejectionDocument
                                            //                        .Replace("@User", userdetil.CC_SPOC_Name)
                                            //                        .Replace("@Date", formattedDate)
                                            //                        .Replace("@ForMonth", ForMonth)
                                            //                        .Replace("@StName", StateName)
                                            //                        .Replace("@ActName", ActName)
                                            //                        .Replace("@NatureOfCompliance", NatureOfCompliance)
                                            //                        .Replace("@TypeofCompliance", TypeOfCompliance)
                                            //                        .Replace("@Status", "Team Review");

                                            //        List<string> aa = new List<string>();
                                            //        aa.Add(userdetil.CC_SPOC_Email);
                                            //        //aa.Add("amol@avantis.info");

                                            //        RLCS_Vendor_AuditScheduleMail_Log obj = new RLCS_Vendor_AuditScheduleMail_Log();
                                            //        obj.AuditID = Convert.ToInt32(AuditId);
                                            //        obj.Avacom_UserID = Convert.ToInt32(userdetil.VendorID);
                                            //        obj.ScheduleOnID = Convert.ToInt32(ScheduleOnID);
                                            //        obj.CreatedOn = DateTime.Now;
                                            //        obj.IsSend = true;
                                            //        obj.Flag = "Document Rejected from Auditor";
                                            //        entities.RLCS_Vendor_AuditScheduleMail_Log.Add(obj);
                                            //        entities.SaveChanges();

                                            //        SendGridEmailManager.SendGridMail(ConfigurationManager.AppSettings["SenderEmailAddress"], "info@avantis.co.in", aa, null, null, Subject, message);

                                            //    }
                                            //}
                                            //catch (Exception ex)
                                            //{
                                            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            //}
                                        }
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Record Saved Successfully.";
                                        vdsummary.CssClass = "alert alert-success";
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                                        vdsummary.CssClass = "alert alert-danger";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                        vdsummary.CssClass = "alert alert-danger";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(txtRemark.Text))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Required Remark.";
                        vdsummary.CssClass = "alert alert-danger";
                    }
                    else if (string.IsNullOrEmpty(ddlStatus.SelectedValue) || ddlStatus.SelectedValue == "-1")
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                        vdsummary.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                vdsummary.CssClass = "alert alert-danger";
            }
        }
        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            CompFund_tbl_FileData file = FundMasterManagement.GetFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=FundDocumentAudit.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                        }
                        BindGrid();
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {

                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        CompFund_tbl_FileData file = FundMasterManagement.GetFile(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    string CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    //lblMessage.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                                vdsummary.CssClass = "alert alert-danger";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                vdsummary.CssClass = "alert alert-danger";
            }
        }


        public class dataEmail
        {
            public string ActName { get; set; }
            public string Stateid { get; set; }
            public string NatureOfCompliance { get; set; }
            public string TypeOfCompliance { get; set; }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStatus.SelectedValue != null && ddlStatus.SelectedValue != "-1")
            {
                txtRemark.Text = string.Empty;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long CheckListID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    long scheduleonid = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    int StatusId = Convert.ToInt32(ddlStatus.SelectedValue);

                    var getChecklist = (from row in entities.CompFund_SP_RecentCheckListStatus(auditid, CheckListID, scheduleonid)
                                        select row).FirstOrDefault();
                    if (getChecklist != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(getChecklist.ResultID)))
                        {
                            if (getChecklist.ResultID == StatusId)
                            {
                                if (!string.IsNullOrEmpty(getChecklist.Remarks))
                                {
                                    txtRemark.Text = getChecklist.Remarks;
                                }
                            }
                            else
                            {
                                string SId = Convert.ToString(StatusId);
                                var getChecklist1 = (from row in entities.CompFund_tbl_Checklist_Status
                                                     where row.IsDeleted == false
                                                     && row.ChecklistID == CheckListID
                                                     && row.Status == SId
                                                     select row).FirstOrDefault();
                                if (getChecklist1 != null)
                                {
                                    txtRemark.Text = getChecklist1.Remark;
                                }
                            }
                        }
                        else
                        {
                            string SId = Convert.ToString(StatusId);
                            var getChecklist1 = (from row in entities.CompFund_tbl_Checklist_Status
                                                 where row.IsDeleted == false
                                                 && row.ChecklistID == CheckListID
                                                 && row.Status == SId
                                                 select row).FirstOrDefault();
                            if (getChecklist1 != null)
                            {
                                txtRemark.Text = getChecklist1.Remark;
                            }
                        }                        
                    }                    
                }
            }
        }
    }
}