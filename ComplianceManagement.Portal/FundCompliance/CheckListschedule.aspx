﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="CheckListschedule.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance.CheckListschedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
     <script type="text/x-kendo-template" id="template">      
               
    </script>
     <style type="text/css">
    
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 200px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>


   <%-- <style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 390px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-checkbox-wrapper {
            display: inline-block;
            vertical-align: middle;
            margin-left: -13px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }
        .k-input.k-readonly {
    width: 200px;
}
        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden !important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            width:100%;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid tbody .k-button
         {
            min-width: 21px;
            min-height: 30px;
            background: white;
            border: none;
         }
    </style>--%>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Schedule Audit');

            BindLocation();
            BindType();
            BindYear();
            BindFrequency();
            BindPeriods("C");
        });

        function BindPeriods(Frq) {

            $('#ddlPeriod').empty();
            if (Frq == "C") {
                $("#ddlPeriod").kendoDropDownList({
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" }
                    ]
                });
            }
            if (Frq == "Q") {
                $("#ddlPeriod").kendoDropDownList({

                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan - Mar", value: "Jan - Mar" },
                        { text: "Apr - Jun", value: "Apr - Jun" },
                        { text: "Jul - Sep", value: "Jul - Sep" },
                        { text: "Oct - Dec", value: "Oct - Dec" }
                    ]
                });
            }
            if (Frq == "A" || Frq == "Y") {
                $("#ddlPeriod").kendoDropDownList({

                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan - Dec", value: "Jan - Dec" }
                    ]
                });
            }
            if (Frq == "H") {
                $("#ddlPeriod").kendoDropDownList({
                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan - Jun", value: "Jan - Jun" },
                        { text: "Jul - Dec", value: "Jul - Dec" }
                    ]
                });
            }
            if (Frq == "M") {
                $("#ddlPeriod").kendoDropDownList({

                    autoWidth: true,
                    dataTextField: "text",
                    dataValueField: "value",
                    change: function () {
                    },
                    dataSource: [
                        { text: "Select Period", value: "0" },
                        { text: "Jan", value: "Jan" },
                        { text: "Feb", value: "Feb" },
                        { text: "Mar", value: "Mar" },
                        { text: "Apr", value: "Apr" },
                        { text: "May", value: "May" },
                        { text: "Jun", value: "Jun" },
                        { text: "Jul", value: "Jul" },
                        { text: "Aug", value: "Aug" },
                        { text: "Sep", value: "Sep" },
                        { text: "Oct", value: "Oct" },
                        { text: "Nov", value: "Nov" },
                        { text: "Dec", value: "Dec" }
                    ]
                });
            }
        }
        function BindFrequency() {

            $("#ddlFrequency").kendoDropDownList({
                placeholder: "Select Frequency",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Frequency",
                autoClose: true,
                dataSource: [
                    { text: "Annually", value: "A" },
                    { text: "Half Yearly", value: "H" },
                    { text: "Quarterly", value: "Q" },
                    { text: "Monthly", value: "M" },
                    { text: "Custom", value: "C" }
                ],
                index: 0,
                change: function (e) {
                    BindPeriods($("#ddlFrequency").val());
                }
            });
        }
        function BindYear() {
            $("#dropdownlistYear").kendoDropDownList({
                placeholder: "Select Year",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Year",
                autoClose: true,
                dataSource: [
                    { text: "2018", value: "2018" },
                    { text: "2019", value: "2019" },
                    { text: "2020", value: "2020" },
                    { text: "2021", value: "2021" }
                ],
                index: 0,
                change: function (e) {

                }
            });
        }
         function BindInvestor()
        {
            $("#dropdownInvestorlist").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "InvestorName",
                dataValueField: "ID",
                optionLabel: "Select Investor Name",
                change: function (e) {
                    
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/BindDocumentSideLetterByFundDoc?CId=<% =customerid%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
        }
        function BindLocation() {
        $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                //checkboxes: {
                //    checkChildren: true
                //},
                //checkAll: true,
                autoWidth: true,
                width:'100%',
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                 
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetBindFundDocumentLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Role=CADMN',
                            //url: '<% =Path%>Data/GetLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }
        
        
        function BindType()
        {
            $("#dropdownlist").kendoDropDownList({
                placeholder: "Select Type",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Type",
                autoClose: true,
                dataSource: [
                    { text: "Fund Documents", value: "FD" },
                    { text: "Side Letter", value: "SL" }
                ],
                index: 0,
                change: function (e) {
                    if ($("#dropdownlist").val() == "SL")
                    {
                        $('#divinvestor').css('display', 'block');
                        BindInvestor();
                    }
                    else {
                        $('#divinvestor').css('display', 'none');
                        //$("#dropdownInvestorlist").empty();
                        //$("#dropdownInvestorlist").data("kendoDropDownList").value(-1);
                    }
                }
            });
        }
        

        function ApplyBtn(e) {
        
            BindgridList();
        }
        function ApplySchBtn(e)
        {
            e.preventDefault();
            var rows = $('#grid :checkbox:checked');
            var grid = $('#grid').data("kendoGrid");

            if (rows.length == 0) {
                alert('Please select the checkbox of the Checklist to schedule the audit.');
                return;
            }
            if (rows.length > 0) {
                var validatopnFlag = 0;
                var things = [];
                $.each(rows, function () {
                    var item = grid.dataItem($(this).closest("tr"));
                    if (item.StartDate == undefined || item.StartDate == null
                        || item.StartDate == "") {
                        validatopnFlag = 1;
                    }
                    item.UserID =<% =UId%>
                        things.push(item);
                });

                if (validatopnFlag == 0) {
                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '<% =Path%>/Data/FundDocumentUpdate',
                            data: JSON.stringify(things),
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                            success: function (data) {
                                if (data[0].Message == "Save") {
                                    alert('Audit Scheduled Successfully.');
                                    BindgridList();
                                }
                            },
                            failure: function (response) {
                                $('#result').html(response);
                            }
                        });
                }
                else {
                    alert('Please select the Start Date of Audit.');
                    return;
                }
            }
        }
        function onDataBound_UnAssignCheckList() {
            kendo.ui.progress($("#UnAssignLoader"), false);
        }
        function editAll() {
            var theGrid = $("#grid").data("kendoGrid");
            $("#grid tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#grid").focus();
        }
        function BindgridList()
        {
            var InvestorID = null;
            if ($("#dropdownlist").val() == "SL") {
                if ($("#dropdownInvestorlist").val() != undefined && $("#dropdownInvestorlist").val() != null) {
                    InvestorID = $("#dropdownInvestorlist").val();
                }
            }
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>data/BindScheduledChecklist?CID=<% =customerid%>&CBID=' + $("#dropdowntree").val() + '&DType=' + $("#dropdownlist").val() + '&InvestorID=' + InvestorID + '&Frequency=' + $("#ddlFrequency").val() + '&Periods=' + $("#ddlPeriod").val() + '&Years=' + $("#dropdownlistYear").val(),                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                //remark: { validation: { required: true, } },
                                StartDate: { type: "date", validation: { required: true } }
                                //UserID: { type: "text" }
                            }
                        }
                    },
                    pageSize: 10
                },
                //toolbar: kendo.template($("#template").html()),
                dataBound: function (e) {
                    editAll();
                },
                sortable: true,
                filterable: true,
                //groupable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
                editable: false,
                noRecords: {
                    template: function (e) {
                        return "No data available";
                    }
                },
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                columns: [
                    {
                        selectable: true,
                        width: "42px"
                    },
                    //{
                    //    hidden: true, field: "ID", title: "ID", width: "8%",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'
                    //    }, filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        width: "15%;",
                        field: "Location", title: 'Location',

                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                        {
                            width: "10%;",
                            field: "ForMonth", title: 'For Period',
                            attributes: {
                                style: 'white-space: nowrap;'
                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                            {
                                width: "7%;",
                                field: "Year", title: 'Year',
                                attributes: {
                                    style: 'white-space: nowrap;'
                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                    {
                        width: "15%;",
                        field: "PerformerName", title: 'Performer Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        width: "12%;",
                        field: "ReviewerName", title: 'Reviewer Name',                       
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                                    
                    {
                        template: "<input id='StartDate' data-bind='value:StartDate' min='#= updatedMinDates() #' name='StartDate' type='date' class='k-textbox'/>",
                        format: "{0:yyyy-MM-dd}",
                        title: "Start Date",
                        width: "15%"
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-edit", className: "ob-overviewMain" }
                        ], title: "Action", lock: true, width: "6%", headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]

            });
            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
            $("#grid").kendoTooltip({
                filter: ".ob-overviewMain",
                content: function (e) {
                    return "Schedule Audit";
                }
            });
            $("#grid").kendoTooltip({
         
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    debugger;
                    if (content != "") {
                        if (content.length > 1) {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                    }
                }
            }).data("kendoTooltip");
            function dateTimeEditor1(container, options) {
                $('<input type="text" id="StartDate"  name="StartDate" required/>')
                    .appendTo(container)
                    .kendoDatePicker({
                        format: "dd-MM-yyyy",
                        value: kendo.toString(new Date(options.model.StartDate), 'dd-MM-yyyy'),
                        min: kendo.date.today(),
                    });
            }
         }
        function updatedMinDates() {
            var month = kendo.date.today().getMonth() + 1;
            var getmonth = month < 10 ? '0' + month : '' + month;

            var getday = kendo.date.today().getDate();
            var day = getday < 10 ? '0' + getday : '' + getday;
            return kendo.date.today().getFullYear() + "-" + getmonth + "-" + day;
        }
        $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e)
        {
            debugger;
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
           var validatopnFlag = 0;
           var things = [];
           if (item.StartDate == undefined || item.StartDate == null
               || item.StartDate == "") {
               validatopnFlag = 1;
           }
           item.UserID =<% =UId%>;
           things.push(item);
            if (true)
            {
                if (validatopnFlag == 0) {
                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '<% =Path%>/Data/FundDocumentUpdate',
                            data: JSON.stringify(things),
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                            success: function (data) {
                                if (data[0].Message == "Save") 
                                {
                                    alert('Audit Scheduled Successfully.');
                                    BindgridList();
                                }
                            },
                            failure: function (response) {
                                $('#result').html(response);
                            }
                        });
                }
                else {
                    alert('Please select the Start Date of Audit.');
                    return;
                }
            }
                return true;
            });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <input type="text" id="products" runat="server" style="display: none;" />
   <div class="row Dashboard-white-widget">
    <div id="example">
        <div class="row">
            <div class="col-md-12 colpadding0">
                <div class="col-md-2" style="margin-left: -1.5%;">
                    <input id="dropdowntree" style="width: 110%;"/>
                </div>
                <div class="col-md-2" style="display:none;">
                    <input id="dropdownlist" />
                </div>  
                <div class="col-md-2" id="divinvestor" style="display:none;">
                  <input id="dropdownInvestorlist" />
                 </div>
                  <div class="col-md-2">
                    <input id="dropdownlistYear" style="width: 110%;"/>
                </div>
                 <div class="col-md-2">
                    <input id="ddlFrequency" style="width: 110%;"/>                 
                </div>
               <div class="col-md-2">                    
                    <input id="ddlPeriod" style="width: 110%;"/>
                </div>
                
                 <div class="col-md-2" style="float:right;margin-right: -6%;">
                <button id="btnApply" type="button" onclick="ApplyBtn(event)" class="btn btn-primary pull-left MarRight" style="margin-right: 10px;">Apply</button>                                    
                                       
                    <button id="btnschedule" type="button" onclick="ApplySchBtn(event)" class="btn btn-primary pull-left MarRight" >Schedule</button>                                    
                </div>             
            </div>
        </div>
         <div class="row" style="padding-top: 5px;">
            <div class="col-md-12 colpadding0">
             
                <div class="col-md-2">
                    
                </div>
                 <div class="col-md-2">
                 </div>
                 <div class="col-md-2">
                 </div>
                
            </div>
            </div>
             <div class="row Mar-Top15">
                <div id="grid"></div>
                </div>
    </div>
    </div>
</asp:Content>
