﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.ComplianceFund;

namespace com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance
{
    public partial class Fund_ClosedTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                    && !string.IsNullOrEmpty(Request.QueryString["AID"])
                       && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                            && !string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    long CheckListID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    long scheduleonid = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    int CheckListStatus = Convert.ToInt32(Request.QueryString["SID"]);
                   
                    if (CheckListID > 0)
                    {
                        GetAllValues(CheckListID);
                        BindTransactions();
                        BindGrid();
                      
                    }
                }
            }
        }
        
        public void GetAllValues(long CheckListID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getChecklist = (from row in entities.CompFund_tbl_Checklist
                                    where row.IsDeleted == false && row.Id == CheckListID
                                    select row).FirstOrDefault();
                if (getChecklist != null)
                {
                    lblClause_ref_num.Text = getChecklist.Clause_ref_num.ToString();
                    lblComp_Que.Text = getChecklist.Comp_Que.ToString();
                    LblInvestorName.Text = string.Empty;
                    if (getChecklist.InvestorID != null && getChecklist.InvestorID != -1)
                    {
                        int investorID = Convert.ToInt32(getChecklist.InvestorID);
                        string investorname = (from row in entities.CompFund_tbl_InvestorMaster
                                               where row.IsDeleted == false && row.ID == investorID
                                               select row.InvestorName).FirstOrDefault();
                        LblInvestorName.Text = investorname;
                    }
                }
            }
        }
        private void BindTransactions()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                  && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    int CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    grdTransactionHistory.DataSource = FundMasterManagement.GetAllTransactionLog(ScheduledOnID, CheckListID, auditid);
                    grdTransactionHistory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindGrid()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                      && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    long ChecklistID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = FundMasterManagement.getDocumentData(ChecklistID, ScheduleOnID);
                        if (lstChecklistDetails.Count > 0)
                        {
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        else
                        {
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            CompFund_tbl_FileData file = FundMasterManagement.GetFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=FundDocumentAudit.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                        }
                        BindGrid();
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {

                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        CompFund_tbl_FileData file = FundMasterManagement.GetFile(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    string CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        
    }
}