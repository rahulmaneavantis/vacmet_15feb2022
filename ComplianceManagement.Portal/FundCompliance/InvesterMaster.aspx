﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="InvesterMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance.InvesterMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fopenpopup() {
            $('#divContDocTypeDialog').modal('show');
        }

        function OpenDocTypePopup(ContractDocID) {
            $('#AddDocumentTypePopUp').modal('show');            
            $("#<%=IframeDocType.ClientID %>").attr('src', "AddInvestor.aspx?InvestorId=" + ContractDocID);
        }

        function CloseDocTypePopup() {
            $('#AddDocumentTypePopUp').modal('hide');
             document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Investor Master');
        });

    </script>
    <style type="text/css">
    .modal-header-custom {
    display: block;
    float: left;
    font-size: 20px;
    color: #1fd9e1;
    /* width: 225px; */
    font-weight: 400;
}
    </style>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <asp:ValidationSummary ID="vsPageDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in" 
                    ValidationGroup="PageDocTypeValidationGroup" />
                <asp:CustomValidator ID="cvPageDocType" runat="server" EnableClientScript="False"
                    ValidationGroup="PageDocTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0">
                        <asp:TextBox runat="server" ID="tbxFilter" AutoComplete="off" CssClass="form-control" MaxLength="50"
                            PlaceHolder="Type Name to Search" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" Width="70%"/>
                    </div>
                    <div class="col-md-4 colpadding0">
                         <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                    </div>
                    <div class="col-md-2 colpadding0 text-right">                       
                        <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click"
                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Investor" CssClass="btn btn-primary">
                            <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="row">
                <asp:GridView runat="server" ID="grdinvestors" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" AllowSorting="true" GridLines="none" Width="100%"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                    OnRowCreated="grdinvestors_RowCreated" OnSorting="grdinvestors_Sorting"
                    OnRowDataBound="grdinvestors_RowDataBound"
                     OnRowCommand="grdinvestors_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                               <%#Container.DataItemIndex+1 %>                                          
                            </ItemTemplate>
                        </asp:TemplateField>

<%--                        <asp:BoundField DataField="InvestorName" HeaderText="Investor Name" SortExpression="InvestorName" ItemStyle-Width="85%" />--%>
                         <asp:TemplateField HeaderText="Investor Name" SortExpression="InvestorName" HeaderStyle-Width="85%" ItemStyle-Width="85%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InvestorName") %>' ToolTip='<%# Eval("InvestorName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditContractDocType" runat="server" CommandName="EDIT_Investors" ToolTip="Edit" data-toggle="tooltip" data-placement="bottom"
                                    CommandArgument='<%# Eval("ID") %>'>
                                    <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteContractDocType" runat="server" CommandName="DELETE_Investors" ToolTip="Delete" data-toggle="tooltip" data-placement="bottom"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Investors?');">
                                    <img src='<%# ResolveUrl("/Images/delete_icon_new.png")%>' alt="Delete" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">                                
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width:100%; float: right; margin-right:6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 colpadding0" style="margin-top: 1%;display:none;">
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Previous" CssClass="btn btn-primary" ToolTip="Go to Previous Master (Contract Type)" data-toggle="tooltip" OnClick="lnkPreviousSwitch_Click" Width="100%" runat="server" ID="LnkbtnPrevious" />
                        </div>
                        <div class="col-md-10 colpadding0">
                        </div>
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-10 colpadding0">
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Next" CssClass="btn btn-primary" runat="server" ToolTip="Go to Next Master (Custom Field)" data-toggle="tooltip" OnClick="lnkNextSwitch_Click" ID="LnkbtnNext" Style="float: right; width: 100%;" />
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
    
    <div class="modal fade" id="AddDocumentTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label id="lblName" runat="server" class="modal-header-custom">
                        </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDocType" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
