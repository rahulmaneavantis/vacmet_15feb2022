﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="AuditCheckListDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.FundCompliance.AuditCheckListDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
     <script type="text/x-kendo-template" id="template">      
               
    </script>
    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 200px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>
   <%-- <style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 200px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-checkbox-wrapper {
            display: inline-block;
            vertical-align: middle;
            margin-left: -13px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }
        .k-input.k-readonly {
    width: 200px;
}
        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden !important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            width:100%;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-grid tbody .k-button
         {
            min-width: 21px;
            min-height: 30px;
            background: white;
            border: none;
         }
    </style>--%>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Checklist Detail');
            $("#setlable").text('<% =AuditChecklistName%>');
            BindgridList();
        });
        function onDataBound_UnAssignCheckList() {
            kendo.ui.progress($("#UnAssignLoader"), false);
        }
        function editAll() {
            var theGrid = $("#grid").data("kendoGrid");
            $("#grid tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#grid").focus();
        }
        function BindgridList()
        {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>data/BindCheckAuditlist?UID=<% =UId%>&CID=<% =customerid%>&DType=<% =DType%>&AuditchecklistID=<% =AuditchecklistID%>&AuditChecklistScheduleID=<% =AuditChecklistScheduleID%>',                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {                                
                                StartDate: { type: "date", validation: { required: true } }                                
                            }
                        }
                    },
                    pageSize: 10
                },
                //toolbar: kendo.template($("#template").html()),
                dataBound: function (e) {
                    editAll();
                },
                sortable: true,
                filterable: true,
                //groupable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
                editable: false,
                //pageable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: OnGridDataBoundAdvanced,
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                columns: [
                    //{
                    //    hidden: true, field: "ID", title: "ID", width: "8%",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'
                    //    }, filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        width: "8%;",
                        field: "Clause_ref_num", title: 'Relevant Clause',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },               
                        {
                            width: "10%;",
                            field: "Comp_Que", title: 'Compliance Question relating to agreed Term/Conditions',
                            attributes: {
                                style: 'white-space: nowrap;'
                            }, filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                                 {
                                     width: "12%;",
                                     field: "InvestorName", title: 'Investor Name',
                                     attributes: {
                                         style: 'white-space: nowrap;'
                                     }, filterable: {
                                         multi: true,
                                         extra: false,
                                         search: true,
                                         operators: {
                                             string: {
                                                 eq: "Is equal to",
                                                 neq: "Is not equal to",
                                                 contains: "Contains"
                                             }
                                         }
                                     }
                                 },
                            {
                                width: "7%;",
                                field: "Status", title: 'Status',
                                attributes: {
                                    style: 'white-space: nowrap;'
                                }, filterable: {
                                    multi: true,
                                    extra: false,
                                    search: true,
                                    operators: {
                                        string: {
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains"
                                        }
                                    }
                                }
                            },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-edit", className: "ob-overviewMain" },
                        { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMainClose" }
                        ], title: "Action", lock: true, width: "6%", headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]

            });
            $("#grid").kendoTooltip({
                filter: ".ob-overviewMainClose",
                content: function (e) {
                    return "View Checklist";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".ob-overviewMain",
                content: function (e) {
                    return "Edit Checklist";
                }
            });
            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        if (content.length > 1) {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else {
                            e.preventDefault();
                        }
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
           
        }
        function closeModal(){
            BindgridList();
        }
      function OnGridDataBoundAdvanced(e) {
            debugger;
            
            var grid = $("#grid").data("kendoGrid");
            var gridData = grid.dataSource.view();           
            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;                
                if (gridData[i].RoleID == 3) 
                {
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var createUserButton = $(currentRow).find(".ob-overviewMain");
                    createUserButton.hide();
                    var createUserButton1 = $(currentRow).find(".ob-overviewMainClose");
                    createUserButton1.hide();

                    if (gridData[i].Status == "Open" || gridData[i].Status == "Team Review")
                    {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMain");
                        createUserButton.show();
                    }
                    else if (gridData[i].Status == "Closed")
                    {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMainClose");
                        createUserButton.show();
                    }
                }
                if (gridData[i].RoleID == 4)
                {
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var createUserButton = $(currentRow).find(".ob-overviewMain");
                    createUserButton.hide();
                    var createUserButton1 = $(currentRow).find(".ob-overviewMainClose");
                    createUserButton1.hide();

                    if (gridData[i].Status == "Submitted" || gridData[i].Status == "Team Review") {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMain");
                        createUserButton.show();
                    }
                    if (gridData[i].Status == "Closed") {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMainClose");
                        createUserButton.show();
                    }
                }
            }
            var statusID = <% =AuditStatusID%>;
            if (statusID == 4) 
            {
                $("#btnClosed").hide();
            }
            else
            {
                var gridforclose = $("#grid").data("kendoGrid").dataSource;
                var gridlength = gridforclose._data.length;
                var cnt = 0;
                for (var i = 0; i < gridforclose._data.length; i++) 
                {
                    if (gridforclose._data[i].RoleID == 4) 
                    {    
                        if (gridforclose._data[i].Status == "Closed") 
                        {
                            cnt = cnt + 1;
                        }
                    }
                }
                if (cnt == gridlength) 
                {                
                    $("#btnClosed").show();
                }
                else 
                {
                    $("#btnClosed").hide();
                }             
            }
        }
        <%--function OnGridDataBoundAdvanced(e) {
            debugger;
            var grid1 = $("#grid").data("kendoGrid").dataSource;

            var grid = $("#grid").data("kendoGrid");
            var gridData = grid.dataSource.view();
            var cnt = 0;
            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;                
                if (gridData[i].RoleID == 3) 
                {
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var createUserButton = $(currentRow).find(".ob-overviewMain");
                    createUserButton.hide();
                    var createUserButton1 = $(currentRow).find(".ob-overviewMainClose");
                    createUserButton1.hide();

                    if (gridData[i].Status == "Open" || gridData[i].Status == "Team Review")
                    {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMain");
                        createUserButton.show();
                    }
                    else if (gridData[i].Status == "Closed")
                    {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMainClose");
                        createUserButton.show();
                    }
                }
                if (gridData[i].RoleID == 4)
                {
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var createUserButton = $(currentRow).find(".ob-overviewMain");
                    createUserButton.hide();
                    var createUserButton1 = $(currentRow).find(".ob-overviewMainClose");
                    createUserButton1.hide();

                    if (gridData[i].Status == "Submitted" || gridData[i].Status == "Team Review") {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMain");
                        createUserButton.show();
                    }
                    if (gridData[i].Status == "Closed") {
                        var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var createUserButton = $(currentRow).find(".ob-overviewMainClose");
                        createUserButton.show();
                    }
                    if (gridData[i].Status == "Closed") {
                        cnt = cnt + 1;
                    }
                }
            }
            var statusID = <% =AuditStatusID%>;
            if (statusID == 4) 
            {
                $("#btnClosed").hide();
            }
            else
            {
                if (cnt == gridData.length) {                
                    $("#btnClosed").show();
                }
                else {
                    $("#btnClosed").hide();
                }
            }
        }--%>
        $(document).on("click", "#grid tbody tr .ob-overviewMain", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            if (item.RoleID == 3)
            {
                var modalHeight = screen.height - 200;
                if (modalHeight < 0)
                    modalHeight = 200;

                $('#divShowDialog').modal('show');
                $('.modal-dialog').css('width', '95%');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', modalHeight + "px");
                $('#showdetails').attr('src', "/FundCompliance/CompFund_PerofrmerTransaction.aspx?CHID=" + item.CheckListId + "&AID=" + item.AuditID + "&SOID=" + item.ScheduledOnID + "&SID=" + item.AuditStatusID + "&CBID=" + item.CustomerBranchID);
            }
            else if (item.RoleID == 4)
            {
                var modalHeight = screen.height - 200;
                if (modalHeight < 0)
                    modalHeight = 200;

                $('#divShowDialog').modal('show');
                $('.modal-dialog').css('width', '95%');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', modalHeight + "px");
                $('#showdetails').attr('src', "/FundCompliance/CompFund_ReviewerTransaction.aspx?CHID=" + item.CheckListId + "&AID=" + item.AuditID + "&SOID=" + item.ScheduledOnID + "&SID=" + item.AuditStatusID + "&CBID=" + item.CustomerBranchID);
            }
            return true;
        });

        $(document).on("click", "#grid tbody tr .ob-overviewMainClose", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "/FundCompliance/Fund_ClosedTransaction.aspx?CHID=" + item.CheckListId + "&AID=" + item.AuditID + "&SOID=" + item.ScheduledOnID + "&SID=" + item.AuditStatusID + "&CBID=" + item.CustomerBranchID);

            return true;
        });
        function btnclosed(e)
        {
            debugger;
            e.preventDefault();
            kendo.ui.progress($(".chart-loading"), true);
            $.ajax({
                //contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                type: "POST",
                url: '<% =Path%>/ExportReport/FundDocumentClosedUpdate',                    
                data: {
                    AID:<% =AuditchecklistID%>,
                    SID:<% =AuditChecklistScheduleID%>,
                    CID:<% =customerid%>,
                     UID:<% =UId%>,
                    //InvestID:InvestorID
                },
                success: function (json) {
                    kendo.ui.progress($(".chart-loading"), false);
                    if (json.message == "Save Record") {
                        alert("Audit Checklist Closed Successfully.");
                    }
                    if (json.message == "Record Not Save") {
                        alert("Please All checklist should be closed.");
                    }
                    else {
                        kendo.ui.progress($(".chart-loading"), false);
                        alert("Error in Audit Checklist Closed");
                    }
                },
                failure: function (json) {
                    kendo.ui.progress($(".chart-loading"), false);
                    alert("Error in Audit Checklist Closed");
                }
            });           
        }
        function BackBtn()
        {
            window.location.href = "../FundCompliance/AuditCheckList.aspx";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
       <div class="row Dashboard-white-widget">
    <div id="example">
    <div class="row Mar-Top15">
   <div class="col-md-12 colpadding0">
       <div class="col-md-8" >
        <label id="setlable" style="color: black; font-size: 25px; font-weight: 450;margin-left:-15px;"></label>   
        </div>
        <div class="col-md-1" style="float: right;margin-right: -24px;">
        <button id="btnback" type="button" onclick="BackBtn()" class="btn btn-primary pull-left MarRight" style="margin-right: 10px;">Back</button>                                                       
        </div>
        <div class="col-md-1" style="float: right;margin-right: -24px;">
        <button id="btnClosed" type="button" onclick="btnclosed(event)" class="btn btn-primary pull-left MarRight" style="margin-right: 10px;">Close</button>                                                       
        
        </div>
    </div>
    </div>
     <div class="row">
            <div class="col-md-12 colpadding0">
                <div class="col-md-2" >
                   
                </div>
                <div class="col-md-2" style="display:none;">
                   
                </div>  
                 <div class="col-md-2">
                   
                </div>
                <div class="col-md-2" id="divinvestor" style="display:none;">
                  
                 </div>
               <div class="col-md-2">
                                    
                </div>
               <div class="col-md-2">                    
                   
                </div>
                
                 <div class="col-md-2" style="float:right;">
                
                </div>             
            </div>
        </div>
        <div class="row Mar-Top15">
            <div id="grid"></div>
        </div>
        <div style="padding-top:7px;">
            
        </div>
    </div>
    <div class="chart-loading"></div>
    </div>
     <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 35px; text-align: left;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7; border-radius: 10px;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
                  <button type="button" data-dismiss="modal" hidden="hidden" id="close"></button>
            </div>
        </div>
    </div>

</asp:Content>

