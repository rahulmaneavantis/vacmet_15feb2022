﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewMgmtHelp.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.NewMgmtHelp"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 
    <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
    <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>

    <title></title>
      <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1200px;
 /*border-style: outset;*/
  border-width: 5px;

  
}
 .MainContainer{
 /*height:1000px;*/
 }
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>
  
    
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>
</head>
<body>
    <form id="form1" runat="server">&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;  <a id="Mgmt" style="font-size:16px" runat="server" href="NewMgmtHelp.aspx"><b>Management</b></a>
        &nbsp;&nbsp;&nbsp;&nbsp;<a id="perf" style="font-size:16px" runat="server"   href="NewHelp.aspx">Performer/Reviewer</a>
     <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
              <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../NewMgmtHelp.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>

          <div class="col-sm-9 MainContainer" style="line-height:25px;border:groove;border-color:#f1f3f4">
      <br />
              
              <p style="font-size:16px;">At glance you can view count of assigned Entities, Locations, Categories, Compliances, Users involved and total Penalty amount. 
              </p><br />
              <img class="img1" style="width:700px;height:400px; margin-left:30px" src="../ManagementHelpcenterScreenshot/My%20Dashboard_1_Screenshot.png" />
            
              <br /><br />
              

              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                  <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <i class="short-full glyphicon glyphicon-plus"></i>
                                  <b>Common features for all users-</b>
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">



                              <div style="color: #666; background-color: white;"><b>1.Search-</b> On this tab, you can search any Act or Compliance assigned to you.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Search_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>2.Help Center-</b> It Includes Summary of Features, FAQs, and other details in text and video regarding how to use AVACOM.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Help_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>3.Message-</b> It includes the messages received from other users of your organization.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Message_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>4.Notification center-</b> It includes the information of any latest update or amendment in the compliances assigned to you.</div>
                              <br />
                              <img class="img1" style="width: 700px; margin-left: 30px;" src="../ImagesHelpCenter/Notification_Screenshot.png" /><br />
                              <br />
                              <div style="color: #666; background-color: white;"><b>5.Profile -</b> You can change your password, security Questions and logout. And following steps will guide you for same, </div>
                              <ul style="list-style-type: lower-alpha;">
                                  <li><b>Change Password: -</b>
                                      <ul style="list-style-type: square">
                                          <li>Enter Old Password</li>
                                          <li>Enter New Password</li>
                                          <li>Confirm New Password</li>
                                          <li>Click Submit </li>
                                      </ul>
                                  </li>
                                  <li><b>Change Security Questions: -</b>
                                      <ul style="list-style-type: square;">
                                          <li>Select any three secret Questions </li>
                                          <li>Enter their Answers</li>
                                          <li>Click Submit </li>
                                      </ul>
                                  </li>
                                  <li><b>Check features you want to be showcased on Dashboard. </b></li>
                                  <li style="font-size: 16px; font-family: 'Times New Roman'"><b>Logout </b></li>
                                  <br />
                                  <br />
                                  <img class="img1" style="width: 720px; margin-left: 30px;  height: 350px" src="../ManagementHelpcenterScreenshot/Profile_Screenshot.png" />

                                  <br />
                                  <br />
                              </ul>



                              <div style="color: #666; background-color: white;">
                                  <b>6.Toggle navigation-</b><br />
                                  <b>Question -</b>  How do I hide/collapse the side menu?
                                  <br />
                                  <b>Answer-</b> Click on the hamburger icon to hide/collapse the side menu and to view menu click on hamburger icon.<br />
                                  <br />
                                  <img class="img1" style="width: 720px; margin-left: 70px; height: 350px" src="../ManagementHelpcenterScreenshot/Toggle%20navigation_Screenshot.png" />
                                  <br />
                                  <br />
                              </div>

                              <div style="color: #666; background-color: white;">
                                  <b>7.  Chat- </b>
                                  <br />
                                  <b>Question –</b> With whom can I chat?<br />
                                  <b>Answer-</b> You can connect with our Client Support Team directly through this online chat and resolve your queries regarding software and performing compliances. Following steps will help you use Chat feature, 
                        <ul style="list-style-type: square">
                            <li>Click Chat Option</li>
                            <li>Enter your Email-id    </li>
                            <li>Click Start Chat</li>
                        </ul>
                                  <br />

                                  <img class="img1" style="margin-left: 70px" src="../ImagesHelpCenter/Chat_1_Screenshot.png" /><br />
                                  <br />
                                  <br />
                                  <img class="img1" style="width: 720px; margin-left: 70px;  height: 350px" src="../ManagementHelpcenterScreenshot/Chat_2_Screenshot.png" />
                                  <br />
                                  <br />
                              </div>

                              <div style="color: #666; background-color: white;">
                                  <b>8. Internal message system-  </b>
                                  <br />
                                  <b>Question –</b> How can I send message and to whom I can send?<br />
                                  <b>Answer-</b> You can send message to other users of your organization and with message you can attach documents as well.  Following steps will help you use this feature,	   
                        <ul style="list-style-type: square">
                            <li>Click Internal Message Option </li>
                            <li>Enter the details subject, message etc.   </li>
                            <li>To attach any file with message click Choose Files and upload the file.</li>
                            <li>Click Send</li>
                        </ul>
                                  <br />
                                  <img class="img1" style="margin-left: 50px;" src="../ImagesHelpCenter/Message_1_Screenshot.png" /><br />
                                  <br />
                                  <br />
                                  <img class="img1" style="width: 720px; margin-left: 70px; height: 350px" src="../ManagementHelpcenterScreenshot/Message_2_Screenshot.png" />

                                  <br />
                                  <br />
                              </div>

                              <div style="color: #666; background-color: white;">
                                  <b>9. Support ticket-  </b>
                                  <br />
                                  <b>Question –</b>  How can I use this feature?<br />
                                  <b>Answer-</b> You can create a support ticket, bifurcating your issue into (legal, sales, technical or general query)and you will get response  on your registered email address within 48 hours.                                                               
                        <ul style="list-style-type: square">
                            <li>Click Support Ticket Option </li>
                            <li>Enter the Subject   </li>
                            <li>Select Issue from Drop Down list</li>
                            <li>Enter Message</li>
                            <li>Click Create Ticket</li>
                            <br />
                            <br />
                        </ul>
                                  <img class="img1" style="margin-left: 50px;" src="../ImagesHelpCenter/Support%20Ticket_1_Screenshot.png" /><br />
                                  <br />
                                  <img class="img1" style="width: 720px; margin-left: 80px;  height: 350px" src="../ManagementHelpcenterScreenshot/Support%20Ticket_02_Screenshot.png" />

                                  <br />
                                  <br />
                              </div>

                              <div style="color: #666; background-color: white;">
                                  <b>10.If I am Management user and Performer/Reviewer both?</b>
                                  <br />
                                  <b>Answer-</b>You can access features both users. And your Dashboard will be showcased in following manner,<br />
                                  <br />

                                  <img class="img1" style="width: 720px; margin-left: 70px; height: 350px" src="../ManagementHelpcenterScreenshot/ManagementuserandPerformerReviewer_Screenshot.png" />
                              </div>


                          </div>

                      </div>

                  </div>



              </div>
              <!-- panel-group -->
	
	
</div>
          <!-- container -->
</div>

 </div>
    </form>
       
<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
