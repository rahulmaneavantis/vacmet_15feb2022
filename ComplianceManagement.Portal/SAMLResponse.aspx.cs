﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using Saml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;


namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class SAMLResponse : System.Web.UI.Page
    {
        private string rawSamlData;

        protected void Page_Load(object sender, EventArgs e)
        {
            string userEmail = string.Empty;
            string IdentifierEntityID_idpid = string.Empty;
            string url = string.Empty;

            try
            {
                foreach (string s in Request.Params.Keys)
                {
                    if (s.ToString() == "SAMLResponse")
                    {
                        rawSamlData = Request.Params[s];
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(rawSamlData))
                {
                    LoggerMessage.InsertErrorMsg_DBLog(rawSamlData, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                byte[] samlData = Convert.FromBase64String(rawSamlData);

                // read back into a UTF string  
                string samlAssertion = Encoding.UTF8.GetString(samlData);

                XmlDocument doc = new XmlDocument();
                XmlNamespaceManager xMan = new XmlNamespaceManager(doc.NameTable);
                xMan.AddNamespace("saml2p", "urn:oasis:names:tc:SAML:2.0:protocol");
                xMan.AddNamespace("saml2", "urn:oasis:names:tc:SAML:2.0:assertion");
                xMan.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");

                doc.LoadXml(Encoding.UTF8.GetString(samlData));

                XmlNodeList nodeList = doc.GetElementsByTagName("saml2:Audience");

                if (nodeList != null)
                {
                    foreach (XmlNode curr in nodeList)
                    {
                        url = curr.InnerText;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(url))
                {
                    var uri = new Uri(url);
                    var query = HttpUtility.ParseQueryString(uri.Query);

                    IdentifierEntityID_idpid = query.Get("idpid");
                }

                if (!string.IsNullOrEmpty(IdentifierEntityID_idpid))
                {
                    //Check IdentifierEntityID Exists
                    var samlConfigRecord = SAMLManagement.CheckExistsIdentifierEntityID(IdentifierEntityID_idpid);

                    if (samlConfigRecord != null)
                    {
                        string Certificate = Server.MapPath(samlConfigRecord.CertificatePath);

                        if (System.IO.File.Exists(Certificate))
                        {
                            //string Certificate = Server.MapPath("GoogleSAML.pem");
                            string samlCertificate = System.IO.File.ReadAllText(Certificate);

                            Saml.Response samlResponse = new Response(samlCertificate, Request.Form["SAMLResponse"]);

                            if (samlResponse.IsValid())
                            {
                                string username, firstname, lastname;
                                try
                                {
                                    username = samlResponse.GetNameID();
                                    userEmail = samlResponse.GetEmail();
                                    firstname = samlResponse.GetFirstName();
                                    lastname = samlResponse.GetLastName();
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }

                                if (string.IsNullOrEmpty(userEmail))
                                {
                                    XmlNode xNode = doc.SelectSingleNode("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID", xMan);

                                    nodeList = doc.GetElementsByTagName("saml2:Attribute");

                                    if (nodeList != null)
                                    {
                                        foreach (XmlNode curr in nodeList)
                                        {
                                            if (curr.OuterXml.Contains("email"))
                                            {
                                                userEmail = curr.InnerText;
                                                break;
                                            }
                                        }
                                    }
                                }

                                Business.Data.User user = null;
                                if (!string.IsNullOrEmpty(userEmail))
                                {
                                    if (UserManagement.IsValidUser(userEmail, out user))
                                    {
                                        if (user != null)
                                        {
                                            ProceedLogin(user, string.Empty, userEmail);
                                            //ProcessAuthenticationInformation(user);
                                        }
                                        else
                                        {
                                            cvLogin.IsValid = false;
                                            cvLogin.ErrorMessage = "No Registered User found with Email - " + userEmail;
                                        }
                                    }
                                    else
                                    {
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = "No Registered User found with Email - " + userEmail;
                                    }
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Not able to get Email from Request, Please contact administrator for more details";
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Not able to recongnize you are coming from valid source or certificate expired, Please contact administrator for more details";
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Certificate not exists for IDP ID- " + IdentifierEntityID_idpid + ", Please contact administrator for more details and check it is configured properly";
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Not able to Identity Registered Customer with IDP ID- " + IdentifierEntityID_idpid + ", Please contact administrator for more details and check it is configured properly";
                    }
                }
                else
                {
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Not able to get IDP ID, Please contact administrator for more details";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ProceedLogin(User user, string ipaddress, string userEmail)
        {
            try
            {
                if (user != null)
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    bool Success = true;
                    try
                    {
                        if (user.CustomerID != null)
                        {
                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                            if (!blockip.Item1)
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    bool Auditorexpired = false;
                    if (user.RoleID == 9)
                    {
                        if (user.Enddate == null)
                        {
                            Auditorexpired = true;
                        }
                        if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                        {
                            Auditorexpired = true;
                        }
                    }
                    if (!Auditorexpired)
                    {
                        if (!(user.WrongAttempt >= 3)) /*UserManagement.WrongAttemptCount(txtemail.Text.Trim())*/
                        {
                            if (user.IsActive)
                            {
                                Session["userID"] = user.ID;
                                Session["ContactNo"] = user.ContactNumber;
                                Session["Email"] = user.Email;
                                Session["CustomerID_new"] = user.CustomerID;
                                if (user.RoleID == 9)
                                {
                                    Session["Auditstartdate"] = user.AuditStartPeriod;
                                    Session["Auditenddate"] = user.AuditEndPeriod;
                                }
                                DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate); /*UserManagement.GetByID(Convert.ToInt32(user.ID)*/
                                DateTime currentDate = DateTime.Now;
                                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                                int noDays = 0;
                                if (blockip != null && blockip.Item2.Count > 0)
                                {
                                    var data = blockip.Item2.Where(a => a.PasswordExpiry != null).ToList();
                                    if (data.Count > 0)
                                    {
                                        noDays = blockip.Item2.Select(entry => (int)entry.PasswordExpiry).FirstOrDefault();
                                    }
                                    else
                                    {
                                        noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                    }
                                }
                                else
                                {
                                    noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                }
                                int customerID = 0;
                                customerID = Convert.ToInt32(user.CustomerID);
                                if (customerID == 6 || customerID == 5 || customerID == 14)
                                {
                                    noDays = 90;
                                }
                                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                                if (dateDifference >= noDays)
                                {
                                    Session["ChangePassword"] = true;
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                                else if (user.LastLoginTime != null)
                                {
                                    //Generate Random Number 6 Digit For OTP.
                                    Random random = new Random();
                                    int value = random.Next(1000000);
                                    if (value == 0)
                                    {
                                        value = random.Next(1000000);
                                    }
                                    Session["ResendOTP"] = Convert.ToString(value);
                                    long Contact;
                                    VerifyOTP OTPData = new VerifyOTP()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        EmailId = userEmail,
                                        OTP = Convert.ToInt32(value),
                                        CreatedOn = DateTime.Now,
                                        IsVerified = false,
                                        IPAddress = ipaddress
                                    };
                                    bool OTPresult = false;
                                    try
                                    {
                                        OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                        if (OTPresult)
                                        {
                                            OTPData.MobileNo = Contact;
                                        }
                                        else
                                        {
                                            OTPData.MobileNo = 0;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        OTPData.MobileNo = 0;
                                        OTPresult = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                    try
                                    {
                                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                        {
                                            //Send Email on User Mail Id.
                                            if (user.CustomerID != 5)
                                            {
                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                                //SendGridEmailManager.SendGridMail(SenderEmailAddress, ReplyEmailAddressName, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Success = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }


                                    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                                    {
                                        try
                                        {
                                            if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                            {
                                                //Send SMS on User Mobile No.
                                                
                                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                                if (data != null)
                                                {
                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                                }
                                                else
                                                {
                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Success = false;
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                    }
                                    MaintainLoginDetail objData = new MaintainLoginDetail()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        Email = userEmail,
                                        CreatedOn = DateTime.Now,
                                        IPAddress = ipaddress,
                                        MACAddress = ipaddress,
                                        LoginFrom = "WC",
                                        //ProfileID=user.ID
                                    };
                                    UserManagement.Create(objData);
                                    if (Success)
                                    {
                                        Session["RM"] = false;
                                        Session["EA"] = Util.CalculateAESHash(userEmail.Trim());
                                        Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                        Response.Redirect("~/Users/OTPVerify.aspx", false);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                                    }
                                    else
                                    {
                                        if (UserManagement.HasUserSecurityQuestion(user.ID))
                                        {
                                            Session["RM"] = false;
                                            Session["EA"] = Util.CalculateAESHash(userEmail.Trim());
                                            Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                        }
                                        else
                                        {
                                            Session["QuestionBank"] = true;
                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled.";
                            }
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


    }


}