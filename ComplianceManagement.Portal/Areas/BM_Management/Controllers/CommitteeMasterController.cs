﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CommitteeMasterController : Controller
    {
        // GET: BM_Management/CommitteeMaster
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public ActionResult GetCommittee()
        {
            return View();
        }

        public ActionResult GetCommitteeNew()
        {
            CommittePageAuthorization _objcommitteepage = new CommittePageAuthorization();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                _objcommitteepage.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Committee") select x.Addview).FirstOrDefault();
                _objcommitteepage.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Committee") select x.Editview).FirstOrDefault();
                _objcommitteepage.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Committee") select x.DeleteView).FirstOrDefault();
            }
            return View(_objcommitteepage);
        }
        [HttpPost]
        public ActionResult CheckIsValidCommittee(int EntityId, int CommitteeId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            ICommitteeMaster objICommitteeMaster = new CommitteeMaster();

            return Json(objICommitteeMaster.CheckIsValidCommittee(CommitteeId, EntityId, customerId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Matrix(int entityId)
        {
            return PartialView("_Matrix", entityId);
        }
    }
}