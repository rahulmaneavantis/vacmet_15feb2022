﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using System.IO;
using System.Reflection;
using System.Windows.Media;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Styles;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using BM_ManegmentServices.Data;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MBP2TransactionController : Controller
    {
        // GET: BM_Management/Chargedetails
        IMBP2TransactionService objIMBP2TransactionService;
        int Customer_Id = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = AuthenticationHelper.UserID;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public MBP2TransactionController(IMBP2TransactionService objMBP2TransactionService, IEntityMaster objEntityMaster)
        {
            objIMBP2TransactionService = objMBP2TransactionService;
        }

        public ActionResult Index(int entityId)
        {
            return View(entityId);
        }

        public ActionResult GetTransaction(int id, int entityId)
        {
            var model = new MBP2TransactionVM() { MBP2TransId = 0, EntityId = entityId};
            if(id > 0)
            {
                model = objIMBP2TransactionService.GetTransaction(id, entityId);
            }
            else
            {
                var result = objIMBP2TransactionService.GetLimit(entityId);
                if(result != null)
                {
                    model.DateOfBoardResolution = result.DateOfBoardResolution;
                    model.DateOfPassingResolution = result.DateOfPassingResolution;
                }
            }
            return PartialView("_AddEditTransaction", model);
        }

        [HttpPost]
        public ActionResult AddEditTransaction(MBP2TransactionVM model)
        {
            int userId = AuthenticationHelper.UserID;
            model = objIMBP2TransactionService.CreateUpdateTransaction(model, userId);
            return PartialView("_AddEditTransaction", model);
        }

        [HttpPost]
        public ActionResult DeleteMBP2Transaction(int id, int entityId)
        {
            int userId = AuthenticationHelper.UserID;
            var result = objIMBP2TransactionService.DeleteTransaction(id, entityId, userId);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        #region Upload Excel

        public ActionResult UploadExcel(int entityId)
        {
            var model = new MBP2TransactionUploadVM();
            model.EntityId = entityId;
            return PartialView("_MBP2UploadExcel", model);
        }

        [HttpPost]
        public ActionResult UploadMBP2Excel(MBP2TransactionUploadVM objfileupload)
        {
            if (ModelState.IsValid)
            {
                var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var userId = Convert.ToInt32(AuthenticationHelper.UserID);
                objfileupload = objIMBP2TransactionService.UploadExcel(objfileupload, customerId, userId);
            }
            return PartialView("_MBP2UploadExcel", objfileupload);
        }

        public ActionResult DownloadSampleFormateofMBP2Excel()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\EntityDocument\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "MBP2.xlsx");
            string fileName = "MBP-2 Sample File.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        #endregion

        #region Limit
        public ActionResult GetMBP2Limit(int entityId)
        {
            int userId = AuthenticationHelper.UserID;
            var result = objIMBP2TransactionService.GetLimit(entityId);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetMBP2Limit(MBP2TransactionLimitVM model)
        {
            int userId = AuthenticationHelper.UserID;
            model = objIMBP2TransactionService.SetLimit(model, userId);
            return Json(new { Result = model }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditLimit(int entityId)
        {
            int userId = AuthenticationHelper.UserID;
            var model = objIMBP2TransactionService.GetLimit(entityId);
            return PartialView("_AddEditLimit", model);
        }

        [HttpPost]
        public ActionResult SaveMBP2Limit(MBP2TransactionLimitVM model)
        {
            int userId = AuthenticationHelper.UserID;
            model = objIMBP2TransactionService.SetLimit(model, userId);
            return PartialView("_AddEditLimit", model);
        }

        public ActionResult UploadMBP2document(int entityId,int NatureOfTransactionId)
        {
            EntityDocumentVM model = new EntityDocumentVM();
            model.EntityId = entityId;
            model.NatureOfTransactionId = NatureOfTransactionId;
            return PartialView("MBP2UploadDocument", model);
        }
        public ActionResult SaveMBP2Document(EntityDocumentVM _objdoc)
        {
            if (ModelState.IsValid && _objdoc != null)
            {
                _objdoc = objIMBP2TransactionService.Savechargedocument(_objdoc, UserId, Customer_Id);
            }
            return PartialView("MBP2UploadDocument", _objdoc);
        }
        public ActionResult GetMBP2EntityDocuments([DataSourceRequest] DataSourceRequest request, int entityId, int NatureOfTransactionId)
        {
            List<BM_SP_GetMBP2Documents_Result> _objDoc = new List<BM_SP_GetMBP2Documents_Result>();
            _objDoc = objIMBP2TransactionService.GetMBP2Documents(entityId,NatureOfTransactionId);
            return Json(_objDoc.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMBP2Documents(int EntityId, int NatureOfTransactionId)
        {
            EntityDocumentVM _model = new EntityDocumentVM();
            _model.EntityId = EntityId;
            _model.NatureOfTransactionId = NatureOfTransactionId;
            return PartialView("MBP2DocumentGrid", _model);
        }
        #endregion
    }
}