﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class EntitiesUserAssignmentController : Controller
    {
        // GET: BM_Management/EntitiesUserAssignment
        IEntityuserAssignment obj;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();

        public EntitiesUserAssignmentController(IEntityuserAssignment obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserAssignment()
        {
            VM_EntityuserAssignment objuserAssignment = new VM_EntityuserAssignment();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                objuserAssignment.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User Entity Assignment") select x.Addview).FirstOrDefault();
                objuserAssignment.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User Entity Assignment") select x.Editview).FirstOrDefault();
                objuserAssignment.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User Entity Assignment") select x.DeleteView).FirstOrDefault();
            }
            return View(objuserAssignment);
        }

        public ActionResult AddEditPopup(int Id)
        {
            VM_EntityuserAssignment objentity = new VM_EntityuserAssignment();
            if(Id>0)
            {
                objentity = obj.GetUserAssignmentbyId(Id);
            }
            return PartialView("_AddEditEntityAssignment", objentity);
        }

        public ActionResult AddEditUserAssignment(VM_EntityuserAssignment _objentityassignment)
        {
            if(ModelState.IsValid)
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (_objentityassignment.Id>0)
                {
                    _objentityassignment = obj.UpdateUserAssignment(_objentityassignment, userId, customerID);
                }
                else
                {
                    _objentityassignment = obj.AddUserAssignment(_objentityassignment, userId);
                }
            }
            return PartialView("_AddEditEntityAssignment",_objentityassignment);
        }

        public ActionResult AddEditMeetingTypeAssignment(int assignmentId, int entityId)
        {
            var model = obj.GetUserMeetingTypeAssignmentById(assignmentId, entityId);
            return PartialView("_UserMeetingTypeAssignment", model);
        }

        [HttpPost]
        public ActionResult SaveMeetingTypeAssignment(UserMeetingTypeAssignmentVM model)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                model = obj.SaveMeetingTypeAssignment(model, userId);
                ModelState.Clear();
            }
            return PartialView("_UserMeetingTypeAssignment", model);
        }
    }
}