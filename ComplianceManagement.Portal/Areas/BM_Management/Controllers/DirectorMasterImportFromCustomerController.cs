﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using System.IO;
using System.Reflection;
using System.Windows.Media;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Styles;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using BM_ManegmentServices.Data;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class DirectorMasterImportFromCustomerController : Controller
    {
        // GET: BM_Management/Chargedetails
        IImportDirector objIImportDirector;
        int Customer_Id = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = AuthenticationHelper.UserID;

        public DirectorMasterImportFromCustomerController(IImportDirector objImportDirector)
        {
            objIImportDirector = objImportDirector;
        }
        #region Import entity from AVACOM
        public PartialViewResult ImportFromOtherCustomer()
        {
            return PartialView("_ImportFromOtherCustomer");
        }
        public ActionResult GetDirectorListForImport([DataSourceRequest] DataSourceRequest request, int customerId)
        {
            return Json(objIImportDirector.GetDirectorListForImport(customerId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ImportDirectors([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ImportDirectorFromCustomerVM> model)
        {
            if (ModelState.IsValid && model != null)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                objIImportDirector.ImportDirectorFromOtherCustomer(model, customerID, userID);
            }
            return Json(model.ToDataSourceResult(request, ModelState));
        }
        #endregion
    }
}