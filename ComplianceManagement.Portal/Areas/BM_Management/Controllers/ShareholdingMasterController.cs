﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ShareholdingMasterController : Controller
    {
        IShareholdingMaster objShareholdingMaster;
        IDirectorMaster _objdirectormaster;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = AuthenticationHelper.UserID;
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public ShareholdingMasterController(IShareholdingMaster obj, IDirectorMaster _objdirectormaster)
        {
            objShareholdingMaster = obj;
            this._objdirectormaster = _objdirectormaster;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetShareholdingMaster(int entity_Id)
        {
            ViewBag.EntityId = entity_Id;
            VMShareholding obj = new VMShareholding();
            obj.Shareholding = new ShareHoldings();
            obj.Shareholding.Entity_Id = entity_Id;
            string EntityName = string.Empty;
            if (entity_Id > 0)
            {
                EntityName = objShareholdingMaster.getEntityName(entity_Id);
            }
            obj.EntityName = EntityName;

            return View(obj);
        }

        [HttpGet]
        public ActionResult PartnerSharesDetails(int entity_Id)
        {
          
            VMShareholding _objvmshares = new VMShareholding();
            _objvmshares.EntityID = entity_Id;
            if (entity_Id > 0)
            {
                _objvmshares.EntityName = objShareholdingMaster.getEntityName(entity_Id);
            }

            return View("PartnerSharesDetails", _objvmshares);
        }


        public PartialViewResult Share(int id)
        {
            VMShareholding obj = new VMShareholding();
            return PartialView("_PartialViewShareholdingdtls", obj);
        }
        public ActionResult Create(int id, int entityID)
        {
            VMShareholding _objVMShareholding = new VMShareholding();
            decimal totalPrefrencecapital = objShareholdingMaster.gettotalprefrenceCapital(entityID, CustomerId);
            if (id == 0)
            {
                ShareHoldings objshareholding = new ShareHoldings();
                objshareholding.Entity_Id = entityID;
                objshareholding.TotalPrefrenceCapital = totalPrefrencecapital;
                return PartialView("_PartialViewShareholding", objshareholding);
            }
            else
            {
                _objVMShareholding.Shareholding = objShareholdingMaster.getShareHoldingData(id, CustomerId);
               _objVMShareholding.Shareholding.Entity_Id = entityID;
                _objVMShareholding.Shareholding.TotalPrefrenceCapital = totalPrefrencecapital;
                return PartialView("_Add_EditShareholding", _objVMShareholding);
            }
          
        }

        [HttpPost]
        public ActionResult CreateShareMaster(ShareHoldings _objdtls)
        {
            VMShareholding _objVMShareholding = new VMShareholding();
            string view = string.Empty;
            if (ModelState.IsValid)
            {
                if (_objdtls.Id == 0)
                {
                    _objdtls = objShareholdingMaster.AddShareholdingdata(_objdtls, CustomerId, UserId);
                    if (_objdtls.Id > 0)
                    {
                        _objVMShareholding.Shareholding = objShareholdingMaster.getShareHoldingData(_objdtls.Id, CustomerId);
                        _objVMShareholding.Shareholding.Entity_Id = _objdtls.Entity_Id;
                        _objVMShareholding.Shareholding.TotalPrefrenceCapital = objShareholdingMaster.gettotalprefrenceCapital(_objdtls.Entity_Id, CustomerId);
                        _objVMShareholding.Id = _objdtls.Id;
                        ModelState.Clear();
                        _objVMShareholding.Message = new Response();
                        _objVMShareholding.Message.Success = true;
                        _objVMShareholding.Message.Message = "Saved Successfully";


                        return PartialView("_Add_EditShareholding", _objVMShareholding);
                    }
                    else
                    {
                        ModelState.Clear();

                        return PartialView("_PartialViewShareholding", _objdtls);
                    }

                }
                else
                {
                    ModelState.Clear();
                    _objdtls = objShareholdingMaster.UpdateShareholdingdata(_objdtls, CustomerId, UserId);
                    _objdtls.TotalPrefrenceCapital = objShareholdingMaster.gettotalprefrenceCapital(_objdtls.Entity_Id, CustomerId);
                    return PartialView("_PartialViewShareholding", _objdtls);
                }

            }
            else
            {
                return PartialView("_PartialViewShareholding", _objdtls);
            }
        }

        //[HttpPost]
        //public ActionResult EditShareDetails(VMShareholding vmshareholding, string command_name)
        //{

        //    ModelState.Clear();
        //    if (command_name == "btnNoofDetails")
        //    {
        //        if (vmshareholding.lstShareHoldingDetails == null)
        //        {
        //            vmshareholding.lstShareHoldingDetails = new List<ShareHoldingDetails>();
        //        }

        //        if (vmshareholding.Count < vmshareholding.lstShareHoldingDetails.Count)
        //        {
        //            List<ShareHoldingDetails> lstTemp = new List<ShareHoldingDetails>();
        //            for (int i = 0; i < vmshareholding.Count; i++)
        //            {
        //                lstTemp.Add(vmshareholding.lstShareHoldingDetails[i]);
        //            }
        //            vmshareholding.lstShareHoldingDetails = lstTemp;
        //        }
        //        else
        //        {
        //            for (int i = vmshareholding.lstShareHoldingDetails.Count; i < vmshareholding.Count; i++)
        //            {
        //                vmshareholding.lstShareHoldingDetails.Add(new ShareHoldingDetails());
        //            }
        //        }
        //    }
        //    else
        //    {

        //        if (ModelState.IsValid)
        //        {
        //            if (vmshareholding.lstShareHoldingDetails.Count > 0)
        //            {
        //                vmshareholding = objShareholdingMaster.AddShareholderdtls(vmshareholding, CustomerId);
        //                //ViewBag.Message = result;
        //            }
        //        }
        //    }
        //    return PartialView("_PartialViewShareholdingdtls", vmshareholding);
        //}

        #region Public Private ListedCompany Report Details 
        [Route("ShareholdingDetails")]
        public ActionResult GetPrivatepublicShare(int Entity_Id)
        {
            ViewBag.ErrorMessage = null;
            ViewBag.SuccessMessage = null;
            ViewBag.Error = null;
            VMPublicListedSherholders obj = new VMPublicListedSherholders();
            string EntityName = string.Empty;
            if (Entity_Id > 0)
            {
                EntityName = objShareholdingMaster.getEntityName(Entity_Id);
            }
            obj.EntityName = EntityName;
            obj.EntityId = Entity_Id;
            return View(obj);
        }

        [HttpPost]
        public ActionResult UploadPrivatepublicShare(VMPublicListedSherholders _objlistedCompany)
        {
            try
            {
                ViewBag.ErrorMessage = null;
                int UserId = AuthenticationHelper.UserID;
                string EntityName = string.Empty;
                if (_objlistedCompany.EntityId > 0 && _objlistedCompany.Allowtment_Date != null && _objlistedCompany.File != null)
                {
                    if (_objlistedCompany.File.ContentLength > 0)
                    {
                        string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/Benpos/" + _objlistedCompany.EntityId + "/";
                        string _file_Name = Path.GetFileName(_objlistedCompany.File.FileName);
                        string _path = Path.Combine(Server.MapPath(path), _file_Name);
                        bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                        if (!exists)
                            System.IO.Directory.CreateDirectory(Server.MapPath(path));

                        _objlistedCompany.File.SaveAs(_path);
                        FileInfo excelfile = new FileInfo(_path);
                        if (excelfile != null)
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flag = PrivatepublicSheetsExitsts(xlWorkbook, "Benpos");
                                if (flag == true)
                                {
                                    ProcessPrivatepublicData(xlWorkbook, _objlistedCompany.Allowtment_Date, _objlistedCompany.EntityId);
                                }
                                else
                                {
                                    ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'Benpos'.";
                                }
                            }
                        }
                    }
                }
                else
                {
                    ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'Benpos'.";
                }

                if (_objlistedCompany.EntityId > 0)
                {
                    EntityName = objShareholdingMaster.getEntityName(_objlistedCompany.EntityId);
                }
                _objlistedCompany.EntityName = EntityName;
                return PartialView("_ListedSharesForFileUpload");
            }
            catch (Exception ex)
            {
                return PartialView("_ListedSharesForFileUpload");
            }
        }


        private bool PrivatepublicSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {

                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("Benpos"))
                    {
                        if (sheet.Name.Trim().Equals("Benpos"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return false;
        }

        public ActionResult UploadFileopenPopUPForBenpose(int EntityId)
        {
            VMPublicListedSherholders obj = new VMPublicListedSherholders();
            obj.EntityId = EntityId;
            return PartialView("_ListedSharesForFileUpload", obj);
        }
        private void ProcessPrivatepublicData(ExcelPackage xlWorkbook, string allowtment_Date, int EntityId)
        {
            List<string> errorMessage = new List<string>();
            try
            {

                bool saveSuccess = false;


                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Benpos"];
                if (xlWorksheet != null)
                {

                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string SRNo = string.Empty;
                    string FOLIO_NO = string.Empty;
                    string SHARE_HOLDER_NAME = string.Empty;
                    long SHARES = 0;
                    string JOINT_HOLDER_1 = string.Empty;
                    string JOINT_HOLDER_2 = string.Empty;
                    string JOINT_HOLDER_3 = string.Empty;
                    string JOINT_HOLDER_4 = string.Empty;
                    string FATHER_or_HUSBANDNAME = string.Empty;
                    string ADDRESS_LINE1 = string.Empty;
                    string ADDRESS_LINE2 = string.Empty;
                    string ADDRESS_LINE3 = string.Empty;
                    string ADDRESS_LINE4 = string.Empty;
                    int? PINCODE = 0;
                    string EMAIL_ID = string.Empty;
                    long? PHONE_NO = -1;
                    string PANCARD_NO = string.Empty;
                    string Second_Holder_PAN_No = string.Empty;
                    string Third_Holder_PAN_No = string.Empty;
                    string CATEGORY = string.Empty;
                    string STATUS = string.Empty;
                    string OCCUPATION = string.Empty;
                    string BANK_AC_NO = string.Empty;
                    string BANK_NAME = string.Empty;
                    string BANK_ADDRESS_LINE1 = string.Empty;
                    string BANK_ADDRESS_LINE2 = string.Empty;
                    string BANK_ADDRESS_LINE3 = string.Empty;
                    string BANK_ADDRESS_LINE4 = string.Empty;
                    int? BANK_PINCODE = 0;
                    string BANK_AC_TYPE = string.Empty;
                    string MICR_CODE = string.Empty;
                    string IFSC = string.Empty;
                    string NOM_NAME = string.Empty;
                    string GAUARIDAN_NM = string.Empty;
                    string RowNumber = string.Empty;
                    int UserId = AuthenticationHelper.UserID;
                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {

                        #region 1 SrNo
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                        {
                            SRNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (string.IsNullOrEmpty(SRNo))
                            {
                                errorMessage.Add("Please Check the IsIn_No can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 2 FOLIO_NO
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                        {
                            FOLIO_NO = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            if (string.IsNullOrEmpty(FOLIO_NO))
                            {
                                errorMessage.Add("Please Check the Folio no can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 2 SHARE HOLDER NAME
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                        {
                            SHARE_HOLDER_NAME = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                        }
                        if (string.IsNullOrEmpty(SHARE_HOLDER_NAME))
                        {
                            errorMessage.Add("Please Check the Share Holder Name can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 3 SHARES
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text.Trim())))
                        {
                            SHARES = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text.Trim());
                        }
                        if (SHARES<0)
                        {
                            errorMessage.Add("Please Check the Shares  not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 4 JOINT HOLDER-1
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text.Trim())))
                        {
                            JOINT_HOLDER_1 = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                           
                        }
                        #endregion

                        #region 5 JOINT HOLDER-2
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                        {
                            JOINT_HOLDER_2 = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        }
                       
                        #endregion

                        #region 6 JOINT HOLDER-3
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                        {
                            JOINT_HOLDER_3 = Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim());
                        }
                      
                        #endregion

                        #region 7 FATHER/HUSBAND NAME
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                        {
                            FATHER_or_HUSBANDNAME = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                            if (string.IsNullOrEmpty(FATHER_or_HUSBANDNAME))
                            {
                                errorMessage.Add("Please Check the Father/Husband Name  not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 8 ADDRESS LINE-1
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                        {
                            ADDRESS_LINE1 = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim();
                        }
                        if (string.IsNullOrEmpty(ADDRESS_LINE1))
                        {
                            errorMessage.Add("Please Check the Address Line1 can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 9 ADDRESS LINE-2
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                        {
                            ADDRESS_LINE2 = Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(ADDRESS_LINE2))
                        {
                            errorMessage.Add("Please Check the Address Line2 can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 10 ADDRESS LINE-3
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                        {
                            ADDRESS_LINE3 = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim();
                            if (string.IsNullOrEmpty(ADDRESS_LINE3))
                            {
                                errorMessage.Add("Please Check the Address Line3 can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 11 ADDRESS LINE-4
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                        {
                            ADDRESS_LINE4 = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim();
                        }
                        
                        #endregion

                        #region 12 PINCODE
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                        {
                            PINCODE = Convert.ToInt32(xlWorksheet.Cells[i, 13].Text.Trim());
                        }
                       
                        #endregion

                        #region 13 EMAIL ID
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim())))
                        {
                            EMAIL_ID = Convert.ToString(xlWorksheet.Cells[i, 14].Text).Trim();
                           
                        }
                        #endregion

                        #region 14 PHONE NO
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 15].Text.Trim())))
                        {
                            PHONE_NO = Convert.ToInt64(xlWorksheet.Cells[i, 15].Text);
                        }
                      
                        #endregion

                        #region 15 PANCARD NO
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 16].Text.Trim())))
                        {
                            PANCARD_NO = Convert.ToString(xlWorksheet.Cells[i, 16].Text.Trim());
                        }
                       
                        #endregion

                        #region 16 Second Holder PAN_No
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 17].Text.Trim())))
                        {
                            Second_Holder_PAN_No = Convert.ToString(xlWorksheet.Cells[i, 17].Text).Trim();
                          
                        }
                        #endregion

                        #region 17 Third Holder PAN_No
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 18].Text.Trim())))
                        {
                            Third_Holder_PAN_No = Convert.ToString(xlWorksheet.Cells[i, 18].Text).Trim();
                        }
                       
                        #endregion

                        #region 18 CATEGORY
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 19].Text.Trim())))
                        {
                            CATEGORY = Convert.ToString(xlWorksheet.Cells[i, 19].Text.Trim());
                        }
                     
                        #endregion

                        #region 19 STATUS
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 20].Text.Trim())))
                        {
                            STATUS = Convert.ToString(xlWorksheet.Cells[i, 20].Text).Trim();
                           
                        }
                        #endregion

                        #region 20 OCCUPATION
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 21].Text.Trim())))
                        {
                            OCCUPATION = Convert.ToString(xlWorksheet.Cells[i, 21].Text).Trim();
                        }
                       
                        #endregion

                        #region 21 BANK A/C NO
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 22].Text.Trim())))
                        {
                            BANK_AC_NO = Convert.ToString(xlWorksheet.Cells[i, 22].Text.Trim());
                        }
                     
                        #endregion

                        #region 22 BANK NAME
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 23].Text.Trim())))
                        {
                            BANK_NAME = Convert.ToString(xlWorksheet.Cells[i, 23].Text).Trim();
                           
                        }
                        #endregion

                        #region 23 BANK ADDRESS LINE-1
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 24].Text.Trim())))
                        {
                            BANK_ADDRESS_LINE1 = Convert.ToString(xlWorksheet.Cells[i, 24].Text).Trim();
                        }
                       
                        #endregion

                        #region 24 BANK ADDRESS LINE-2
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 25].Text.Trim())))
                        {
                            BANK_ADDRESS_LINE2 = Convert.ToString(xlWorksheet.Cells[i, 25].Text.Trim());
                        }
                       
                        #endregion

                        #region 25 BANK ADDRESS LINE-3
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 26].Text.Trim())))
                        {
                            BANK_ADDRESS_LINE3 = Convert.ToString(xlWorksheet.Cells[i, 26].Text).Trim();
                           
                        }
                        #endregion

                        #region 26 BANK ADDRESS LINE-4
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 27].Text.Trim())))
                        {
                            BANK_ADDRESS_LINE4 = Convert.ToString(xlWorksheet.Cells[i, 27].Text).Trim();
                        }
                      
                        #endregion

                        #region 27 BANK PINCODE
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 28].Text.Trim())))
                        {
                            BANK_PINCODE = Convert.ToInt32(xlWorksheet.Cells[i, 28].Text.Trim());
                        }
                       
                        #endregion

                        #region 28 BANK A/C TYPE
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 29].Text.Trim())))
                        {
                            BANK_AC_NO = Convert.ToString(xlWorksheet.Cells[i, 29].Text).Trim();
                          
                        }
                        #endregion

                        #region 29 MICR CODE
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 30].Text.Trim())))
                        {
                            MICR_CODE = Convert.ToString(xlWorksheet.Cells[i, 30].Text).Trim();
                        }
                       
                        #endregion

                        #region 30 IFSC
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 31].Text.Trim())))
                        {
                            IFSC = Convert.ToString(xlWorksheet.Cells[i, 31].Text.Trim());
                        }
                       
                        #endregion

                        #region 31 NOM_NAME
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 32].Text.Trim())))
                        {
                            NOM_NAME = Convert.ToString(xlWorksheet.Cells[i, 32].Text).Trim();
                          
                        }
                        #endregion

                        #region 32 GAUARIDAN_NM
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 33].Text.Trim())))
                        {
                            GAUARIDAN_NM = Convert.ToString(xlWorksheet.Cells[i, 33].Text).Trim();
                        }
                       
                        #endregion

                        #region 34 RowNumber
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 34].Text.Trim())))
                        {
                            RowNumber = Convert.ToString(xlWorksheet.Cells[i, 34].Text.Trim());
                        }
                       
                        #endregion
                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;

                            FOLIO_NO = string.Empty;
                            SHARE_HOLDER_NAME = string.Empty;
                            SHARES = 0;
                            JOINT_HOLDER_1 = string.Empty;
                            JOINT_HOLDER_2 = string.Empty;
                            JOINT_HOLDER_3 = string.Empty;
                            JOINT_HOLDER_4 = string.Empty;
                            FATHER_or_HUSBANDNAME = string.Empty;
                            ADDRESS_LINE1 = string.Empty;
                            ADDRESS_LINE2 = string.Empty;
                            ADDRESS_LINE3 = string.Empty;
                            ADDRESS_LINE4 = string.Empty;
                            PINCODE = 0;
                            EMAIL_ID = string.Empty;
                            PHONE_NO = -1;
                            PANCARD_NO = string.Empty;
                            Second_Holder_PAN_No = string.Empty;
                            Third_Holder_PAN_No = string.Empty;
                            CATEGORY = string.Empty;
                            STATUS = string.Empty;
                            OCCUPATION = string.Empty;
                            BANK_AC_NO = string.Empty;
                            BANK_NAME = string.Empty;
                            BANK_ADDRESS_LINE1 = string.Empty;
                            BANK_ADDRESS_LINE2 = string.Empty;
                            BANK_ADDRESS_LINE3 = string.Empty;
                            BANK_ADDRESS_LINE4 = string.Empty;
                            BANK_PINCODE = 0;
                            BANK_AC_TYPE = string.Empty;
                            MICR_CODE = string.Empty;
                            IFSC = string.Empty;
                            NOM_NAME = string.Empty;
                            GAUARIDAN_NM = string.Empty;
                            RowNumber = string.Empty;

                            #region 1 FOLIO NO/ DP ID & CLIENT ID
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                            {
                                FOLIO_NO = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            }
                            #endregion

                            #region 2 SHARE HOLDER NAME
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                SHARE_HOLDER_NAME = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            }
                            #endregion

                            #region 3 SHARES
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text.Trim())))
                            {
                                SHARES = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text.Trim());
                            }
                            #endregion

                            #region 4 JOINT HOLDER-1
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                            {
                                JOINT_HOLDER_1 = Convert.ToString((xlWorksheet.Cells[i, 5].Text).Trim());
                            }
                            #endregion

                            #region 5 JOINT HOLDER-2
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim()))
                            {
                                JOINT_HOLDER_2 = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                            }
                            #endregion

                            #region  6 JOINT HOLDER-3
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                            {
                                JOINT_HOLDER_3 = Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim());
                            }
                            #endregion

                            #region 7 FATHER/HUSBAND NAME
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                            {
                                FATHER_or_HUSBANDNAME = Convert.ToString((xlWorksheet.Cells[i, 8].Text.Trim()));
                            }
                            #endregion

                            #region 8 ADDRESS LINE-1                        
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                            {
                                ADDRESS_LINE1 = Convert.ToString(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim()));
                            }
                            #endregion

                            #region 9 ADDRESS LINE-2
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                            {
                                ADDRESS_LINE2 = Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim());
                            }
                            #endregion

                            #region 10 ADDRESS LINE-3   
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                            {
                                ADDRESS_LINE3 = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                            }
                            #endregion

                            #region 11 ADDRESS LINE-4
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                            {
                                ADDRESS_LINE4 = Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim());
                            }
                            #endregion

                            #region 12 PINCODE
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                            {
                                PINCODE = Convert.ToInt32(xlWorksheet.Cells[i, 13].Text.Trim());
                            }
                            #endregion

                            #region 13 EMAIL ID
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim())))
                            {
                                EMAIL_ID = Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim());
                            }
                            #endregion

                            #region 14 PHONE NO
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 15].Text.Trim())))
                            {
                                PHONE_NO = Convert.ToInt64(xlWorksheet.Cells[i, 15].Text);
                            }
                            #endregion

                            #region 15 PANCARD NO
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 16].Text.Trim())))
                            {
                                PANCARD_NO = Convert.ToString(xlWorksheet.Cells[i, 16].Text.Trim());
                            }
                            #endregion

                            #region 16 Second Holder PAN_No
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 17].Text.Trim())))
                            {
                                Second_Holder_PAN_No = Convert.ToString(xlWorksheet.Cells[i, 17].Text.Trim());
                            }
                            #endregion

                            #region 17 Third Holder PAN_No
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 18].Text.Trim())))
                            {
                                Third_Holder_PAN_No = Convert.ToString(xlWorksheet.Cells[i, 18].Text.Trim());
                            }
                            #endregion

                            #region 18 CATEGORY
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 19].Text.Trim())))
                            {
                                CATEGORY = Convert.ToString(xlWorksheet.Cells[i, 19].Text.Trim());
                            }
                            #endregion

                            #region 19 STATUS
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 20].Text.Trim())))
                            {
                                STATUS = Convert.ToString(xlWorksheet.Cells[i, 20].Text.Trim());
                            }
                            #endregion

                            #region 20 OCCUPATION
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 21].Text.Trim())))
                            {
                                OCCUPATION = Convert.ToString(xlWorksheet.Cells[i, 21].Text.Trim());
                            }
                            #endregion

                            #region 21 BANK A/C NO
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 22].Text.Trim())))
                            {
                                BANK_AC_NO = Convert.ToString(xlWorksheet.Cells[i, 22].Text.Trim());
                            }
                            #endregion

                            #region 22 BANK NAME
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 23].Text.Trim())))
                            {
                                BANK_NAME = Convert.ToString(xlWorksheet.Cells[i, 23].Text.Trim());
                            }
                            #endregion

                            #region 23 BANK ADDRESS LINE-1
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 24].Text.Trim())))
                            {
                                BANK_ADDRESS_LINE1 = Convert.ToString(xlWorksheet.Cells[i, 24].Text.Trim());
                            }
                            #endregion

                            #region 24 BANK ADDRESS LINE-2
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 25].Text.Trim())))
                            {
                                BANK_ADDRESS_LINE2 = Convert.ToString(xlWorksheet.Cells[i, 25].Text.Trim());
                            }
                            #endregion

                            #region 25 BANK ADDRESS LINE-3
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 26].Text.Trim())))
                            {
                                BANK_ADDRESS_LINE3 = Convert.ToString(xlWorksheet.Cells[i, 26].Text.Trim());
                            }
                            #endregion

                            #region 26 BANK ADDRESS LINE-4
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 27].Text.Trim())))
                            {
                                BANK_ADDRESS_LINE4 = Convert.ToString(xlWorksheet.Cells[i, 27].Text.Trim());
                            }
                            #endregion

                            #region 27 BANK PINCODE
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 28].Text.Trim())))
                            {
                                BANK_PINCODE = Convert.ToInt32(xlWorksheet.Cells[i, 28].Text.Trim());
                            }
                            #endregion

                            #region 28 BANK A/C TYPE
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 29].Text.Trim())))
                            {
                                BANK_AC_TYPE = Convert.ToString(xlWorksheet.Cells[i, 29].Text.Trim());
                            }
                            #endregion

                            #region 29 MICR CODE
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 30].Text.Trim())))
                            {
                                MICR_CODE = Convert.ToString(xlWorksheet.Cells[i, 30].Text.Trim());
                            }
                            #endregion

                            #region 30 IFSC
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 31].Text.Trim())))
                            {
                                IFSC = Convert.ToString(xlWorksheet.Cells[i, 31].Text.Trim());
                            }
                            #endregion

                            #region 31 NOM_NAME
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 32].Text.Trim())))
                            {
                                NOM_NAME = Convert.ToString(xlWorksheet.Cells[i, 32].Text.Trim());
                            }
                            #endregion

                            #region 32 GAUARIDAN_NM
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 33].Text.Trim())))
                            {
                                GAUARIDAN_NM = Convert.ToString(xlWorksheet.Cells[i, 33].Text.Trim());
                            }
                            #endregion

                            if (FOLIO_NO != "")
                            {
                                BM_ShareholderListedEntityData _ObjshareholderListedEntityData = new BM_ShareholderListedEntityData();
                                _ObjshareholderListedEntityData.FOLIO_NO = FOLIO_NO;
                                _ObjshareholderListedEntityData.SHARE_HOLDER_NAME = SHARE_HOLDER_NAME;
                                _ObjshareholderListedEntityData.SHARES = SHARES;
                                _ObjshareholderListedEntityData.JOINT_HOLDER1 = JOINT_HOLDER_1;
                                _ObjshareholderListedEntityData.JOINT_HOLDER2 = JOINT_HOLDER_2;
                                _ObjshareholderListedEntityData.JOINT_HOLDER3 = JOINT_HOLDER_3;
                                _ObjshareholderListedEntityData.FATHER_HUSBANDNAME = FATHER_or_HUSBANDNAME;
                                _ObjshareholderListedEntityData.ADDRESSLINE1 = ADDRESS_LINE1;
                                _ObjshareholderListedEntityData.ADDRESSLINE2 = ADDRESS_LINE2;
                                _ObjshareholderListedEntityData.ADDRESSLINE3 = ADDRESS_LINE3;
                                _ObjshareholderListedEntityData.ADDRESSLINE4 = ADDRESS_LINE4;
                                _ObjshareholderListedEntityData.PINCODE = PINCODE;
                                _ObjshareholderListedEntityData.EMAILID = EMAIL_ID;
                                _ObjshareholderListedEntityData.PHONENO = PHONE_NO;
                                _ObjshareholderListedEntityData.PANCARDNO = PANCARD_NO;
                                _ObjshareholderListedEntityData.SecondHolderPAN_No = Second_Holder_PAN_No;
                                _ObjshareholderListedEntityData.ThirdHolderPAN_No = Third_Holder_PAN_No;
                                _ObjshareholderListedEntityData.CATEGORY = CATEGORY;
                                _ObjshareholderListedEntityData.STATUS = STATUS;
                                _ObjshareholderListedEntityData.OCCUPATION = OCCUPATION;
                                _ObjshareholderListedEntityData.BANK_AC_NO = BANK_AC_NO;
                                _ObjshareholderListedEntityData.BANKNAME = BANK_NAME;
                                _ObjshareholderListedEntityData.BANK_ADDRESS_LINE1 = BANK_ADDRESS_LINE1;
                                _ObjshareholderListedEntityData.BANK_ADDRESS_LINE2 = BANK_ADDRESS_LINE2;
                                _ObjshareholderListedEntityData.BANK_ADDRESS_LINE3 = BANK_ADDRESS_LINE3;
                                _ObjshareholderListedEntityData.BANK_ADDRESS_LINE4 = BANK_ADDRESS_LINE4;
                                _ObjshareholderListedEntityData.BANK_PINCODE = BANK_PINCODE;
                                _ObjshareholderListedEntityData.BANK_AC_TYPE = BANK_AC_TYPE;
                                _ObjshareholderListedEntityData.MICR_CODE = MICR_CODE;
                                _ObjshareholderListedEntityData.IFSC = IFSC;
                                _ObjshareholderListedEntityData.NOM_NAME = NOM_NAME;
                                _ObjshareholderListedEntityData.GAUARIDAN_NM = GAUARIDAN_NM;
                                _ObjshareholderListedEntityData.Createdby = UserId;
                                _ObjshareholderListedEntityData.EntityId = EntityId;

                                if (allowtment_Date != null)
                                {
                                    DateTime oDate = DateTime.ParseExact(allowtment_Date, "dd/MM/yyyy", null); ;
                                    _ObjshareholderListedEntityData.allotment_Date = oDate;
                                }
                                saveSuccess = objShareholdingMaster.CreatePublicPrivateListedData(_ObjshareholderListedEntityData);
                                if (!saveSuccess)
                                {
                                    ViewBag.Error = "Something went wrong.";
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                          
                            ViewBag.ErrorMessage = errorMessage;
                        }
                    }

                    if (saveSuccess)
                    {
                        ViewBag.SuccessMessage = "File Uploaded Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Server error Occurred";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Demat Share Details 

        public ActionResult GetDematShare(int Entity_Id)
        {
            ViewBag.ErrorMessage = null;
            ViewBag.SuccessMessage = null;
            VMPublicListedSherholders obj = new VMPublicListedSherholders();
            string EntityName = string.Empty;
            if (Entity_Id > 0)
            {
                EntityName = objShareholdingMaster.getEntityName(Entity_Id);
            }
            obj.EntityName = EntityName;
            obj.EntityId = Entity_Id;
            return View(obj);
        }
        [HttpPost]
        public ActionResult UploadDematShare(VMPublicListedSherholders _objPartialDemat)
        {
            if (_objPartialDemat.File != null && _objPartialDemat.Allowtment_Date != null && _objPartialDemat.EntityId > 0)
            {
                if (_objPartialDemat.File.ContentLength > 0)
                {
                    string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/ShareHolding/" + _objPartialDemat.EntityId + "/";
                    string _file_Name = Path.GetFileName(_objPartialDemat.File.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));

                    _objPartialDemat.File.SaveAs(_path);
                    FileInfo excelfile = new FileInfo(_path);
                    if (excelfile != null)
                    {
                        try
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flag = TransactionSheetsExitsts(xlWorkbook, "DmatForm");
                                if (flag == true)
                                {
                                    ProcessLegalNoticeData(xlWorkbook, _objPartialDemat.EntityId, _objPartialDemat.Allowtment_Date);
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'DmatForm'.";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ViewBag.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'DmatForm'.";
                        }
                    }
                }
            }
            string EntityName = string.Empty;
            if (_objPartialDemat.EntityId > 0)
            {
                EntityName = objShareholdingMaster.getEntityName(_objPartialDemat.EntityId);
            }
            _objPartialDemat.EntityName = EntityName;
            return PartialView("_ShareholdingDemateUpload", _objPartialDemat);
        }

        private bool TransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("DmatForm"))
                    {
                        if (sheet.Name.Trim().Equals("DmatForm"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }

        private void ProcessLegalNoticeData(ExcelPackage xlWorkbook, int Entity_Id, string Allotmentdate)
        {
            List<string> errorMessage = new List<string>();
            try
            {
                bool saveSuccess = false;


                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["DmatForm"];
                if (xlWorksheet != null)
                {

                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string IsIn_No = string.Empty;
                    string DmatAccountNo = string.Empty;
                    string DP_ID = string.Empty;
                    string ClientID = string.Empty;
                    string Depository = string.Empty;
                    string ClassOfShares = string.Empty;
                    long NominalValuePerShar = -1;
                    long TotalShareHeld = -1;
                    string BeneficiryName = string.Empty;
                    string Address = string.Empty;
                    string Email = string.Empty;
                    string PanCardNo = string.Empty;
                    string Nationality = string.Empty;


                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        IsIn_No = string.Empty;
                        DmatAccountNo = string.Empty;
                        DP_ID = string.Empty;
                        ClientID = string.Empty;
                        Depository = string.Empty;
                        ClassOfShares = string.Empty;
                        NominalValuePerShar = -1;
                        TotalShareHeld = -1;
                        BeneficiryName = string.Empty;
                        Address = string.Empty;
                        Email = string.Empty;
                        PanCardNo = string.Empty;
                        Nationality = string.Empty;

                        #region 1 IsIn_No
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                        {
                            IsIn_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            string IsInNo = IsIn_No;
                            Regex regex = new Regex(@"^[a - zA - Z0 - 9]{12}$");
                            Match match = regex.Match(IsInNo);
                            if (string.IsNullOrEmpty(IsIn_No))
                            {
                                errorMessage.Add("Please Check the IsIn_No can not be empty at row - " + i + "");
                            }
                            if (match.Success)
                            {
                                errorMessage.Add("Please Enter valid ISIN No at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 2 Dmat Account No
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                        {
                            DmatAccountNo = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                        }
                        string DemateAccount = DmatAccountNo;
                        Regex regexDemateAccount = new Regex(@"^[a - zA - Z0 - 9\s]{16}$");
                        Match matchDemateAccount = regexDemateAccount.Match(DemateAccount);
                        if (string.IsNullOrEmpty(DmatAccountNo))
                        {
                            errorMessage.Add("Please Check the DmatAccountNo can not be empty at row - " + i + "");
                        }
                        if (matchDemateAccount.Success)
                        {
                            errorMessage.Add("Please Enter valid DMAT Account No at row - " + i + "");
                        }
                        #endregion

                        #region 3 DP_ID
                        if (DmatAccountNo != null)
                        {
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                DP_ID = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                                string DepositoryId = DmatAccountNo.Substring(0, 8);
                                if (!DepositoryId.Equals(DP_ID))
                                {

                                    errorMessage.Add("Plese Enter valid Depository Id at row number - " + (count + 1) + "");
                                }

                            }
                            if (String.IsNullOrEmpty(DP_ID))
                            {
                                errorMessage.Add("Required DP_ID at row number - " + (count + 1) + "");
                            }
                            #endregion

                            #region 4 ClientID
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                            {
                                ClientID = Convert.ToString((xlWorksheet.Cells[i, 4].Text).Trim());
                                string ClientIdNumber = DmatAccountNo.Substring(8, 8);
                                if (!ClientIdNumber.Equals(ClientID))
                                {
                                    errorMessage.Add("Plese Enter valid Client Id at row number - " + (count + 1) + "");
                                }

                            }
                            if (String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                            {
                                errorMessage.Add("Required ClientID at row number - " + (count + 1) + "");
                            }
                            #endregion

                            #region 5 Depository
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                            {
                                Depository = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(Depository))
                            {
                                errorMessage.Add("Required Depository at row number - " + (count + 1) + "");
                            }
                            else
                            {
                                if (!(Depository == "NSDL" || Depository == "CDSL"))

                                    errorMessage.Add("Depository required NSDL/CDSL at row number - " + (count + 1) + "");
                            }
                        }

                        #endregion

                        #region  6 Class Of Shares
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                        {
                            ClassOfShares = Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(ClassOfShares))
                        {
                            errorMessage.Add("Required Class Of Shares at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 7 Nominal Value Per Share
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                        {
                            NominalValuePerShar = Convert.ToInt64((xlWorksheet.Cells[i, 7].Text.Trim()));
                        }
                        if (NominalValuePerShar <= 0)
                        {
                            errorMessage.Add("Required Nominal Value Per Share at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 8 Total Share Held                        
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                        {
                            TotalShareHeld = Convert.ToInt64(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim()));
                        }
                        if (TotalShareHeld <= 0)
                        {
                            errorMessage.Add("Required Total Share Held at row - " + (count + 1) + "");
                        }
                        #endregion

                        #region 9 Beneficiry Name
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                        {
                            BeneficiryName = Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(BeneficiryName))
                        {
                            errorMessage.Add("Required Beneficiry Name at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 10 Address   
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                        {
                            Address = Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Address))
                        {
                            errorMessage.Add("Required Address at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 11 Email
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                        {
                            Email = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Email))
                        {
                            errorMessage.Add("Required Email at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 12 Pan Card No
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                        {
                            PanCardNo = Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(PanCardNo))
                        {
                            errorMessage.Add("Required PanCardNo at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 13 Nationality
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                        {
                            Nationality = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Nationality))
                        {
                            errorMessage.Add("Required Nationality at row number - " + (count + 1) + "");
                        }
                        #endregion

                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            IsIn_No = string.Empty;
                            DmatAccountNo = string.Empty;
                            DP_ID = string.Empty;
                            ClientID = string.Empty;
                            Depository = string.Empty;
                            ClassOfShares = string.Empty;
                            NominalValuePerShar = -1;
                            TotalShareHeld = -1;
                            BeneficiryName = string.Empty;
                            Address = string.Empty;
                            Email = string.Empty;
                            PanCardNo = string.Empty;
                            Nationality = string.Empty;

                            #region 1 IsIn_No
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                            {
                                IsIn_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Dmat Account No
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                            {
                                DmatAccountNo = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            }
                            #endregion

                            #region 3 DP_ID
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                DP_ID = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                            }
                            #endregion

                            #region 4 ClientID
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                            {
                                ClientID = Convert.ToString((xlWorksheet.Cells[i, 4].Text).Trim());
                            }
                            #endregion

                            #region 5 Depository
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                            {
                                Depository = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            }
                            #endregion

                            #region  6 Class Of Shares
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                            {
                                ClassOfShares = Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim());
                            }
                            #endregion

                            #region 7 Nominal Value Per Share
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                            {
                                NominalValuePerShar = Convert.ToInt64((xlWorksheet.Cells[i, 7].Text.Trim()));
                            }
                            #endregion

                            #region 8 Total Share Held                        
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                            {
                                TotalShareHeld = Convert.ToInt64(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim()));
                            }
                            #endregion

                            #region 9 Beneficiry Name
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                            {
                                BeneficiryName = Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim());
                            }
                            #endregion

                            #region 10 Address   
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                            {
                                Address = Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim());
                            }
                            #endregion

                            #region 11 Email
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                            {
                                Email = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                            }
                            #endregion

                            #region 12 Pan Card No
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                            {
                                PanCardNo = Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim());
                            }
                            #endregion

                            #region 13 Nationality
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                            {
                                Nationality = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                            }
                            #endregion

                            BM_DematForm Obj_BM_DematForm = new BM_DematForm();
                            Obj_BM_DematForm.IsIn_No = IsIn_No;
                            Obj_BM_DematForm.DmatAccountNo = DmatAccountNo;
                            Obj_BM_DematForm.DP_ID = DP_ID;
                            Obj_BM_DematForm.ClientID = ClientID;
                            Obj_BM_DematForm.Depository = Depository;
                            Obj_BM_DematForm.ClassOfShares = ClassOfShares;
                            Obj_BM_DematForm.NominalValuePerShar = NominalValuePerShar;
                            Obj_BM_DematForm.TotalShareHeld = TotalShareHeld;
                            Obj_BM_DematForm.BeneficiryName = BeneficiryName;
                            Obj_BM_DematForm.Address = Address;
                            Obj_BM_DematForm.Email = Email;
                            Obj_BM_DematForm.PanCardNo = PanCardNo;
                            Obj_BM_DematForm.Nationality = Nationality;
                            if (Allotmentdate != null)
                            {
                                DateTime oDate = DateTime.ParseExact(Allotmentdate, "dd/MM/yyyy", null); ;
                                Obj_BM_DematForm.allotment_Date = oDate;
                            }
                            if (Entity_Id > 0)
                                Obj_BM_DematForm.Entity_Id = Entity_Id;
                            saveSuccess = objShareholdingMaster.CreateDmatData(Obj_BM_DematForm);
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ViewBag.ErrorMessage = errorMessage;
                        }
                    }

                    if (saveSuccess)
                    {
                        ViewBag.SuccessMessage = "Dmat Details Uploaded Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.ErrorMessage = errorMessage;
            }
        }
        #endregion

        #region Download Sample Document 
        public ActionResult DownloadSampleFormate()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\ShareholdingUploadDocument\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "DematShareholding.xlsx");
            string fileName = "DematShareholding.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult DownloadSampleFormateforBanpos()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\ShareholdingUploadDocument\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "BENPOSDump_ListedCompanies.xlsx");
            string fileName = "BENPOSDump_ListedCompanies.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        #endregion


        public ActionResult KendoPopupsharholding(int EntityId)
        {
            VMPublicListedSherholders obj = new VMPublicListedSherholders();
            obj.EntityId = EntityId;
            return PartialView("_ListedSharesForFileUpload", obj);
        }

        public ActionResult CreateShareHoldingDetails(int Id,int SharesHoldingId)
        {
            ShareHoldingDetails objSHdetails = new ShareHoldingDetails();
            objSHdetails.parentID = SharesHoldingId;
            objSHdetails.Id = Id;
            if(Id>0)
            {
                objSHdetails = objShareholdingMaster.EditSHDetails(Id, SharesHoldingId);
            }
            return PartialView("_AddEditSharesHoldingDetails", objSHdetails);
        }

        [HttpPost]
        public ActionResult AddShareHoldingDetails(ShareHoldingDetails _objDetails)
        {
            if(_objDetails.Id==0)
            {
                _objDetails = objShareholdingMaster.AddShareholderDetails(_objDetails);
            }
            else
            {
                _objDetails = objShareholdingMaster.UpdateShareholderDetails(_objDetails);
            }
            return PartialView("_AddEditSharesHoldingDetails", _objDetails);
        }

        [HttpGet, ActionName("DetailsofIntrest")]
        public PartialViewResult OpenDetailofIntrest(int DirectorId, int IntrestId)
        {
            DetailsOfInterest _objdetailsofintest = new DetailsOfInterest();
            _objdetailsofintest.Director_ID = DirectorId;
            _objdetailsofintest.Id = IntrestId;
            if (DirectorId > 0 && IntrestId > 0)
            {
                _objdetailsofintest = _objdirectormaster.GetDOIbyID(DirectorId, IntrestId);
            }

            return PartialView("_AddEditDetails_of_Intrest", _objdetailsofintest);
        }

        [HttpGet, ActionName("bodycorporate")]
        public PartialViewResult OpenBodyCorporate(int DirectorId, int IntrestId)
        {
            Body_CorporateVM objbodycorporate = new Body_CorporateVM();
            if(DirectorId > 0 && IntrestId > 0)
            {
                objbodycorporate = _objdirectormaster.GetNomneeDetails(DirectorId, IntrestId);
            }

            return PartialView("_BodyCorporate", objbodycorporate);
        }

        // added for BodyCorporateAsPartner 02Jull 2021
        [HttpGet, ActionName("bodycorporateAsPartner")]
        public PartialViewResult OpenBodyCorporateAsPartner(int PartnerId,int entityId)
        {
            Body_CorporateAsPartnerVM objbodycorporateAsPartner = new Body_CorporateAsPartnerVM();
            if (PartnerId > 0 )
            {
                objbodycorporateAsPartner = _objdirectormaster.GetBodyCorporateAsPartner(PartnerId);
            }
            else 
            {
                objbodycorporateAsPartner.entityId = entityId;
            }

            return PartialView("_BodyCorporateAsPartner", objbodycorporateAsPartner);
        }

        [HttpPost]
        public PartialViewResult SaveBodyCorporateAsPartner(Body_CorporateAsPartnerVM _objbodycorporate)
        {
            if (ModelState.IsValid)
            {
                int UserId = AuthenticationHelper.UserID;
                int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                _objbodycorporate = _objdirectormaster.SaveBodyCorporateAsPartner(_objbodycorporate, UserId);
            }
            return PartialView("_BodyCorporateAsPartner", _objbodycorporate);
        }
    }
}