﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class WhatsNewController : Controller
    {
        IWhatsnewservice objIwhatsnewservice;
        int UserId = AuthenticationHelper.UserID;
        public WhatsNewController(IWhatsnewservice objIwhatsnewservice)
        {
            this.objIwhatsnewservice = objIwhatsnewservice;
        }
        // GET: BM_Management/WhatsNew
        public ActionResult _WhatsNew()
        {
            return View();
        }
        public ActionResult UploadwhatsnewDocument(WhatsNewVM _model)
        {
            return PartialView("_whatsnewUploadDocument", _model);
        }
        [HttpPost]
        public ActionResult Savewhatsnewdocument(IEnumerable<HttpPostedFileBase> files, WhatsNewVM _objdoc)
        {
            if (_objdoc.Id==0)
            {
                _objdoc = objIwhatsnewservice.Savewhatsnewdocument(files,UserId, _objdoc);
            }
            return PartialView("_whatsnewUploadDocument", _objdoc);
        }

        public ActionResult Index()
        {
            return View("_WhatsnewGridsidebar");
        }
    }
}