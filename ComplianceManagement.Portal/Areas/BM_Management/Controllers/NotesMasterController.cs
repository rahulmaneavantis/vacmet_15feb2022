﻿using BM_ManegmentServices.VM;
using System;
using BM_ManegmentServices.Services.Masters;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using BM_ManegmentServices.Services.Meetings;
using BM_Manegment;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class NotesMasterController : Controller
    {
        INotesMaster_Service objINotesMaster_Service;

        public NotesMasterController(INotesMaster_Service objNotesMaster_Service)
        {
            objINotesMaster_Service = objNotesMaster_Service;
        }
        public ActionResult Notes()
        {
            var model = new NotesMasterVM() { eVoting = false };
            return View(model);
        }
        public ActionResult GetNotes(NotesMasterVM model)
        {
            var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            model = objINotesMaster_Service.GetNotesMaster(model, customerId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNotes(NotesMasterVM model)
        {
            if (ModelState.IsValid)
            {
                var userId = AuthenticationHelper.UserID;
                var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                model = objINotesMaster_Service.Save(model, customerId, userId);
            }
            return PartialView("_NotesDetails", model);
        }
     }
}