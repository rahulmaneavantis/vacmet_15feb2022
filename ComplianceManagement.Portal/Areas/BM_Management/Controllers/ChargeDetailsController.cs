﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using System.IO;
using System.Reflection;
using System.Windows.Media;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Styles;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using BM_ManegmentServices.Data;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ChargeDetailsController : Controller
    {
        // GET: BM_Management/Chargedetails
        IChargeDetailsMaster objaddition;
        IEntityMaster objEntityMaster;
        int Customer_Id = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = AuthenticationHelper.UserID;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public ChargeDetailsController(IChargeDetailsMaster objaddition, IEntityMaster objEntityMaster)
        {
            this.objaddition = objaddition;
            this.objEntityMaster = objEntityMaster;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddNewChargeDetails(int Id, int EntityId)
        {
            ChargeDetails_VM _objnewCompliances = new ChargeDetails_VM();
            if (Id> 0)
            {
                _objnewCompliances = objaddition.EditChargeDetails(Id, EntityId);

            }
            return PartialView("_ChargePartial", _objnewCompliances);
        }
        [HttpPost]
        public ActionResult CreateChargeDetails(ChargeDetails_VM _objadditionalcomp)
        {
            
                int UserId = AuthenticationHelper.UserID;
                if (_objadditionalcomp.ChargeId == 0)
                {
                    _objadditionalcomp = objaddition.AddChargeDetails(_objadditionalcomp, UserId);
                }
                else
                {
                _objadditionalcomp = objaddition.UpdateChargeDetails(_objadditionalcomp, UserId);
                }


            return PartialView("_ChargePartial", _objadditionalcomp);
        }
        public ActionResult ChargeMaster()
        {
            ChargeDetails_VM objAuditor = new ChargeDetails_VM();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                objAuditor.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Auditor") select x.Addview).FirstOrDefault();
                objAuditor.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Auditor") select x.Editview).FirstOrDefault();
                objAuditor.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Auditor") select x.DeleteView).FirstOrDefault();
            }

            return View(objAuditor);
        }
        [HttpPost]
        public ActionResult UploadCharges(VM_ChargeUpload _objAuditor)
        {
            if (_objAuditor.File != null)
            {
                if (_objAuditor.File.ContentLength > 0)
                {
                    string excelfileName = string.Empty;
                    string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/ChargeMaster/";
                    string _file_Name = Path.GetFileName(_objAuditor.File.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));
                    _objAuditor.File.SaveAs(_path);

                    FileInfo excelfile = new FileInfo(_path);

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flagPublic = CheckAuditorsWorkSheet(xlWorkbook, "ChargeSheet");

                            if (flagPublic == true)
                            {
                                UploadAuditorData(xlWorkbook, _objAuditor.EntityId_ChargeUpload);
                            }

                            else
                            {
                                ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'ChargeSheet'";
                            }
                        }
                    }
                }
            }
            return PartialView("_ChargeUpload", _objAuditor);
        }
        private void UploadAuditorData(ExcelPackage xlWorkbook, int entityId)
        {
            List<string> errorMessage = new List<string>();

            try
            {
                bool saveSuccess = false;
                ExcelWorksheet excelAuditor = xlWorkbook.Workbook.Worksheets["ChargeSheet"];

                if (excelAuditor != null)
                {
                    //#region Save Auditor Details

                    int count = 0;
                    int xlrow2 = excelAuditor.Dimension.End.Row;

                    #region Auditor/Firm  Data
                    string Charge_TypeDesc = string.Empty;
                    string ChargeId_Mannual = string.Empty;
                    DateTime? Charge_CreationDate = null;
                    DateTime? Registration_ChargeCreateDate = null;
                    string ShortDesc_PropertyCharged = string.Empty;
                    string Charge_Amount = string.Empty;
                    string Names_addresses_chargeholder = string.Empty;
                    string Terms_conditionsOfcharge = string.Empty;
                    string Desc_instrument = string.Empty;
                    DateTime? Charge_Modification_Date;
                    DateTime? Registration_ChargeModificationDate = null;
                    string Desc_instrument_chargemodify = string.Empty;
                    string Particulars_modification= string.Empty;
                    DateTime? Satisfaction_Date = null;
                    DateTime? Registration_satisfactionDate = null;
                    DateTime? Facts_Delaycondonation_Date = null;
                    string Reasons_delay_filing = string.Empty;
                    string ChargeTerm = string.Empty;
                    string AddressOfChargeHolder = string.Empty;
                    #endregion
                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            Charge_TypeDesc = string.Empty;
                            Charge_CreationDate = null;
                            Registration_ChargeCreateDate = null;
                            ShortDesc_PropertyCharged = string.Empty;
                            Charge_Amount = string.Empty;
                            Names_addresses_chargeholder = string.Empty;
                            Terms_conditionsOfcharge = string.Empty;
                            Desc_instrument = string.Empty;
                            Charge_Modification_Date = null;
                            Registration_ChargeModificationDate = null;
                            Desc_instrument_chargemodify = string.Empty;
                            Particulars_modification = string.Empty;
                            Satisfaction_Date = null;
                            Registration_satisfactionDate = null;
                            Facts_Delaycondonation_Date = null;
                            Reasons_delay_filing = string.Empty;
                            ChargeId_Mannual = string.Empty;
                            ChargeTerm = string.Empty;
                            AddressOfChargeHolder = string.Empty;

                            int rowIndex = 1;
                            #region Charge Id mannual
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                ChargeId_Mannual = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Charge_TypeDesc
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Charge_TypeDesc = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Charge_CreationDate
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Charge_CreationDate = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Registration_ChargeCreateDate
                            rowIndex++;
                            if (!string.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Registration_ChargeCreateDate = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region ShortDesc_PropertyCharged
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                ShortDesc_PropertyCharged = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Period in Month
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                ChargeTerm = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Charge_Amount
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Charge_Amount = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Names of chargeholder
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Names_addresses_chargeholder = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Address of charge holder
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                AddressOfChargeHolder = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Terms_conditionsOfcharge
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Terms_conditionsOfcharge = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Desc_instrument
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Desc_instrument = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Charge_Modification_Date
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Charge_Modification_Date = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region 9 Registration_ChargeModificationDate
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Registration_ChargeModificationDate = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Desc_instrument_chargemodify
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Desc_instrument_chargemodify = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Particulars_modification
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Particulars_modification = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region 12 Satisfaction_Date
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Satisfaction_Date = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #region Registration_satisfactionDate
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Registration_satisfactionDate = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion
                            #region Facts_Delaycondonation_Date
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Facts_Delaycondonation_Date = Convert.ToDateTime(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion
                            #region Reasons_delay_filing
                            rowIndex++;
                            if (!String.IsNullOrEmpty(Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim())))
                            {
                                Reasons_delay_filing = Convert.ToString(excelAuditor.Cells[i, rowIndex].Text.Trim());
                            }
                            #endregion

                            #endregion

                            if(!string.IsNullOrEmpty(ChargeId_Mannual))
                            {
                                BM_chargedetails objauditor = new BM_chargedetails();
                                //objauditor.Charge_TypeDesc = Charge_TypeDesc;
                                objauditor.Charge_CreationDate = Charge_CreationDate == null ? DateTime.Now.Date : Convert.ToDateTime(Charge_CreationDate);
                                objauditor.Registration_ChargeCreateDate = Registration_ChargeCreateDate;
                                objauditor.ShortDesc_PropertyCharged = ShortDesc_PropertyCharged;
                                objauditor.Charge_Amount = Charge_Amount;
                                objauditor.Names_addresses_chargeholder = Names_addresses_chargeholder;
                                objauditor.Terms_conditionsOfcharge = Terms_conditionsOfcharge;
                                objauditor.Desc_instrument = Desc_instrument;
                                objauditor.Charge_Modification_Date = Charge_Modification_Date;
                                objauditor.Registration_ChargeModificationDate = Registration_ChargeModificationDate;
                                objauditor.Desc_instrument_chargemodify = Desc_instrument_chargemodify;
                                objauditor.Particulars_modification = Particulars_modification;
                                objauditor.Satisfaction_Date = Satisfaction_Date;
                                objauditor.Registration_satisfactionDate = Registration_satisfactionDate;
                                objauditor.Facts_Delaycondonation_Date = Facts_Delaycondonation_Date;
                                objauditor.Reasons_delay_filing = Reasons_delay_filing;
                                objauditor.ChargeTerm = ChargeTerm;
                                objauditor.AddressOfChargeHolder = AddressOfChargeHolder;

                                //objauditor.CustomerId = Customer_Id;
                                objauditor.IsActive = true;
                                objauditor.CreatedBy = UserId;

                                var result = objaddition.ChecksaveUpdateForChargeMaster(ChargeId_Mannual, Charge_TypeDesc, entityId, objauditor);

                                if (result.Success)
                                {
                                    ViewBag.SuccessMessage = "Charge Details Uploaded Successfully";
                                }
                                else if(result.Error)
                                {
                                    errorMessage.Add(result.Message);
                                }
                            }
                        }
                    }

                    if (errorMessage.Count > 0)
                    {
                        ViewBag.ErrorMessage = errorMessage;
                    }

                    //if (saveSuccess)
                    //{
                    //    ViewBag.SuccessMessage = "Charge Details Uploaded Successfully";
                    //}
                    //else
                    //{
                    //    ViewBag.Error = "No data in excel File";
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.ErrorMessage = errorMessage;
            }
        }
        private bool CheckAuditorsWorkSheet(ExcelPackage PrivatePublicexcel, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in PrivatePublicexcel.Workbook.Worksheets)
                {
                    if (data.Equals("ChargeSheet"))
                    {
                        if (sheet.Name.Trim().Equals("ChargeSheet"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }
        public ActionResult DownloadSampleFormateforChargeMaster()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\ChargeUploadDocument\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "Charge.xlsx");
            string fileName = "Charge.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        public ActionResult UploadChargePopup(int entityId)
        {
            VM_ChargeUpload objAuditorUpload = new VM_ChargeUpload();
            objAuditorUpload.EntityId_ChargeUpload = entityId;
            return PartialView("_ChargeUpload", objAuditorUpload);
        }

        #region Upload E-Forms
        public ActionResult UploadFileopenPopUP(int entityId)
        {
            var model = new ChargeUploadEFormVM();
            model.EntityId = entityId;
            return PartialView("_ChargeUploadEForm", model);
        }

        [HttpPost]
        public ActionResult UploadChargeEForm(ChargeUploadEFormVM model)
        {
            if (ModelState.IsValid)
            {
                model.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var userId = Convert.ToInt32(AuthenticationHelper.UserID);
                model.UserId = userId;
                if (model.fromTypeCharge == "SHG1" || model.fromTypeCharge == "SHG4")
                {
                    model = objaddition.UploadChargeEform(model, userId);
                }
                else if(model.fromTypeCharge == "EXCEL")
                {

                    if (model.File != null)
                    {
                        if (model.File.ContentLength > 0)
                        {
                            string excelfileName = string.Empty;
                            string path = "~/Areas/BM_Management/Documents/" + AuthenticationHelper.CustomerID + "/ChargeMaster/";
                            string _file_Name = Path.GetFileName(model.File.FileName);
                            string _path = Path.Combine(Server.MapPath(path), _file_Name);
                            bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                            if (!exists)
                                System.IO.Directory.CreateDirectory(Server.MapPath(path));
                            model.File.SaveAs(_path);

                            FileInfo excelfile = new FileInfo(_path);

                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    bool flagPublic = CheckAuditorsWorkSheet(xlWorkbook, "ChargeSheet");

                                    if (flagPublic == true)
                                    {
                                        UploadAuditorData(xlWorkbook, (int)model.EntityId);
                                    }

                                    else
                                    {
                                        ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'ChargeSheet'";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return PartialView("_ChargeUploadEForm", model);
        }
        #endregion

        public ActionResult GetChargeDetails(long headerId)
        {
            return PartialView("_ChargeDetailsGrid", headerId);
        }
        public ActionResult Uploadchargedocument(int EntityId,long ChargeHeaderId, long ChargeDetailId)
        {
            EntityDocumentVM _model = new EntityDocumentVM();
            _model.EntityId = EntityId;
            _model.ChargeHeaderId = ChargeHeaderId;
            _model.ChargeDetailId = ChargeDetailId;
            return PartialView("_ChargeUploadGrid", _model);
        }
        public ActionResult Savechargedocument(EntityDocumentVM _objdoc)
        {
            if (ModelState.IsValid && _objdoc != null)
            {
                _objdoc = objaddition.Savechargedocument(_objdoc, UserId, Customer_Id);
            }
            return PartialView("_ChargeUploadGrid", _objdoc);
        }
        public ActionResult GetUploadEntityDocuments([DataSourceRequest] DataSourceRequest request, int EntityId, long chargeDetailsId)
        {
            List<BM_ManegmentServices.Data.BM_SP_GetEntityChargeDocuments_Result> _objDoc = new List<BM_ManegmentServices.Data.BM_SP_GetEntityChargeDocuments_Result>();
            _objDoc = objaddition.GetChargeDocuments(chargeDetailsId, EntityId);
            return Json(_objDoc.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUploaddocuments(int EntityId, long ChargeHeaderId, long ChargeDetailId)
        {
            EntityDocumentVM _model = new EntityDocumentVM();
            _model.EntityId = EntityId;
            _model.ChargeHeaderId = ChargeHeaderId;
            _model.ChargeDetailId = ChargeDetailId;
            return PartialView("_Chargedocumentgrid", _model);
        }
    }
}