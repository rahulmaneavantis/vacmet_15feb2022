﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyCompliancesController : Controller
    {
        ICompliance_Service objICompliance_Service;
        ICompliance objICompliance;
        IComplianceTransaction_Service objIComplianceTransaction_Service;
        IFileData_Service objIFileData_Service;
        IMeeting_Compliances objIMeeting_Compliances;

        public MyCompliancesController(ICompliance_Service objCompliance_Service, IMeeting_Compliances obj, ICompliance objCompliance, IComplianceTransaction_Service objComplianceTransaction_Service, IFileData_Service objFileData_Service)
        {
            objICompliance_Service = objCompliance_Service;
            objIMeeting_Compliances = obj;
            objICompliance = objCompliance;
            objIComplianceTransaction_Service = objComplianceTransaction_Service;
            objIFileData_Service = objFileData_Service;
        }
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        #region My Compliances
        public ActionResult Closed()
        {
            return MyCompliances("Closed");
        }
        public ActionResult Upcoming()
        {
            return MyCompliances("Upcoming");
        }
        public ActionResult Overdue()
        {
            return MyCompliances("Overdue");
        }
        public ActionResult MyCompliances(string status)
        {
            var model = new MyCompliancesVM();

            if (AuthenticationHelper.Role == SecretarialConst.Roles.HDCS)
            {
                model.RoleId = 4;
            }
            else
            {
                model.RoleId = 3;
            }

            model.StatusFilter = status;

            switch (status)
            {
                case "Overdue":
                case "Upcoming":
                    model.StatusId = 1;
                    break;
                case "Closed":
                    model.StatusId = 4;
                    break;
                default:
                    model.StatusId = 1;
                    break;
            }
            return View("MyCompliances", model);
        }

        [HttpPost]
        public ActionResult GetMyCompliances(MyCompliancesVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICompliance_Service.GetMyCompliances(model, userID, customerID);

            if(result !=null)
            {
                var lstMeeting = result.Select(r => new { r.EntityId, r.RoleID, r.MeetingID, r.MeetingTitle }).Distinct().ToList();
                var lstDirector = result.Where(k=>k.DirectorId >0).Select(r => new { r.DirectorId, r.CompanyName}).Distinct().ToList();
                return Json(new { MyCompliances = result, Meetings = lstMeeting, Directors = lstDirector }, JsonRequestBehavior.AllowGet);
            }
            return Json( new { MyCompliances = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMyCompliances_Director(MyCompliancesVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICompliance_Service.GetMyCompliances(userID, customerID, model.StatusId, model.RoleId);

            if (result != null)
            {
                var lstMeeting = result.Select(r => new { r.EntityId, r.RoleID, r.MeetingID, r.MeetingTitle }).Distinct().ToList();
                var lstDirector = result.Where(k => k.DirectorId > 0).Select(r => new { r.DirectorId, r.DirectorName }).Distinct().ToList();
                return Json(new { MyCompliances = result, Meetings = lstMeeting, Directors = lstDirector }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { MyCompliances = result }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Schedule -> Meeting Level compliances
        public ActionResult MeetingComplianceSchedule(long meetingId)
        {
            dynamic model = new ExpandoObject();
            model.MeetingID = meetingId;
            return PartialView("_ComplianceSchedule", model);
        }
        public ActionResult ComplianceSchedule_Read([DataSourceRequest] DataSourceRequest request, long meetingId)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("ScheduleOn", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(objIMeeting_Compliances.GetMeetingComplianceSchedule(meetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ScheduleDetails
        public PartialViewResult ScheduleDetails(long id, int entityID, int? roleID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            //var model = objIMeeting_Compliances.GetScheduleDetails(id, entityID, customerId);
            var model = objIMeeting_Compliances.GetScheduleDetailsNew(id, userID);
            if (model != null)
            {
                model.RoleID = roleID;
               
                loadComplianceStaus(model.ScheduleOnID, userID, roleID);
            }

            return PartialView("_ComplianceScheduleDetails", model);
        }

        public ActionResult ComplianceSchedule_Documents([DataSourceRequest] DataSourceRequest request, long id)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("FileID", System.ComponentModel.ListSortDirection.Descending));
            }
            return Json(objIMeeting_Compliances.GetComplianceDocuments(id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Compliance status update

        [HttpPost]
        public PartialViewResult ComplianceStatusUpdate(ComplianceTransactionVM model, HttpPostedFileBase[] complianceDoc, HttpPostedFileBase[] workingDoc, string btnRejectStatus)
        {

            if (ModelState.IsValid)
            {
                bool success = false;
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                model.CreatedBy = userID;
                model.CreatedByText = AuthenticationHelper.User;
                var docResult = CheckRemarkandDocument(model, complianceDoc, workingDoc, btnRejectStatus);//created by Ruchi on 27-01-2021

                #region check for Remark and Document
                if (docResult.Success)
                {
                    if (model.RoleID == SecretarialConst.RoleID.REVIEWER && btnRejectStatus == "Reject")
                    {
                        model.StatusId = 8;
                    }
                    model = objIComplianceTransaction_Service.CreateTransaction(model);

                    if(model != null)
                    {
                        if(model.Success)
                        {
                            #region File Data -> complianceDoc
                            if (complianceDoc != null)
                            {
                                string path = "~/Areas/BM_Management/Documents/" + customerID + "/Meeting/" + model.MeetingID_;
                                //string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
                                foreach (var item in complianceDoc)
                                {
                                    if (item.ContentLength > 0)
                                    {
                                        MemoryStream target = new MemoryStream();
                                        item.InputStream.CopyTo(target);

                                        int fileVersion = 0;

                                        var objFileData = new FileDataVM()
                                        {
                                            FileData = target.ToArray(),
                                            FileName = item.FileName,
                                            FilePath = path
                                        };

                                        fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
                                        fileVersion++;

                                        objFileData.Version = Convert.ToString(fileVersion);
                                        objFileData = objIFileData_Service.Save(objFileData, userID);

                                        if (objFileData.FileID > 0 && model.TransactionID > 0)
                                        {
                                            objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 1);
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region File Data -> workingDoc
                            if (workingDoc != null)
                            {
                                string path = "~/Areas/BM_Management/Documents/" + customerID + "/Meeting/" + model.MeetingID_;
                                //string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
                                foreach (var item in workingDoc)
                                {
                                    if (item.ContentLength > 0)
                                    {
                                        MemoryStream target = new MemoryStream();
                                        item.InputStream.CopyTo(target);

                                        int fileVersion = 0;

                                        var objFileData = new FileDataVM()
                                        {
                                            FileData = target.ToArray(),
                                            FileName = item.FileName,
                                            FilePath = path
                                        };

                                        fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
                                        fileVersion++;

                                        objFileData.Version = Convert.ToString(fileVersion);
                                        objFileData = objIFileData_Service.Save(objFileData, userID);

                                        if (objFileData.FileID > 0 && model.TransactionID > 0)
                                        {
                                            objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 2);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    if(docResult.Error)
                    {
                        model.Error = true;
                        model.Message = docResult.Message;
                    }
                    else if (model.StatusId == 4 || model.StatusId == 5)
                    {
                        model.Error = true;
                        model.Message = "Please fill Remark and select Compliance Document";
                    }
                    else if (model.StatusId == 18)
                    {
                        model.Error = true;
                        model.Message = "Please fill Remark";
                    }
                }
                #endregion

                loadComplianceStaus(model.ComplianceScheduleOnID, userID, model.RoleID);
            }
            return PartialView("_ComplianceStatusUpdate", model);
        }

        [HttpPost]
        public PartialViewResult ComplianceStatusUpdateOfCheckList(ComplianceTransactionVM model, string btnUpdateStatus)
        {
            if (ModelState.IsValid)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                model.CreatedBy = userID;
                model.CreatedByText = AuthenticationHelper.User;
                model.Dated = DateTime.Now.Date;
                if (btnUpdateStatus == "ClosedTimely")
                {
                    model.StatusId = 4;
                }
                else if (btnUpdateStatus == "NotApplicable")
                {
                    model.StatusId = 15;
                }
                else if (btnUpdateStatus == "NotComplied")
                {
                    model.StatusId = 17;
                }
                else
                {
                    model.StatusId = 4;
                }
                model = objIComplianceTransaction_Service.CreateTransaction(model);

                loadComplianceStaus(model.ComplianceScheduleOnID, userID, model.RoleID);
            }
            return PartialView("_ComplianceStatusUpdateCheckList", model);
        }

        // for multiple 14 jun 2021
        [HttpPost]
        public JsonResult ComplianceStatusUpdateOfCheckListMultiple(List<int> scheduleOnId)
        {
            foreach (var ComplianceScheduleOnIn in scheduleOnId)
            {
                var model = new ComplianceTransactionVM();
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                model.CreatedBy = userID;
                model.CreatedByText = AuthenticationHelper.User;
                model.Dated = DateTime.Now.Date;

                model.StatusId = 4;
                model.ComplianceScheduleOnID = ComplianceScheduleOnIn;
                model = objIComplianceTransaction_Service.CreateTransaction(model);
            }

            return Json(scheduleOnId, JsonRequestBehavior.AllowGet); 
        }
        //

        #region Previous Code

        //[HttpPost]
        //public PartialViewResult ComplianceStatusUpdate(ComplianceTransactionVM model, HttpPostedFileBase[] complianceDoc, HttpPostedFileBase[] workingDoc, string btnRejectStatus)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        int userID = Convert.ToInt32(AuthenticationHelper.UserID);
        //        model.CreatedBy = userID;
        //        model.CreatedByText = AuthenticationHelper.User;

        //        if(model.RoleID == SecretarialConst.RoleID.REVIEWER && btnRejectStatus == "Reject")
        //        {
        //            model.StatusId = 8;
        //        }
        //        model = objIComplianceTransaction_Service.CreateTransaction(model);

        //        #region File Data -> complianceDoc
        //        if (complianceDoc != null)
        //        {
        //            string path = "~/Areas/BM_Management/Documents/" + customerID + "/Meeting/" + model.MeetingID_;
        //            //string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
        //            foreach (var item in complianceDoc)
        //            {
        //                if (item.ContentLength > 0)
        //                {
        //                    MemoryStream target = new MemoryStream();
        //                    item.InputStream.CopyTo(target);

        //                    int fileVersion = 0;

        //                    var objFileData = new FileDataVM()
        //                    {
        //                        FileData = target.ToArray(),
        //                        FileName = item.FileName,
        //                        FilePath = path
        //                    };

        //                    fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
        //                    fileVersion++;

        //                    objFileData.Version = Convert.ToString(fileVersion);
        //                    objFileData = objIFileData_Service.Save(objFileData, userID);

        //                    if (objFileData.FileID > 0 && model.TransactionID > 0)
        //                    {
        //                        objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 1);
        //                    }
        //                }
        //            }
        //        }
        //        #endregion

        //        #region File Data -> workingDoc
        //        if (workingDoc != null)
        //        {
        //            string path = "~/Areas/BM_Management/Documents/" + customerID + "/Meeting/" + model.MeetingID_;
        //            //string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
        //            foreach (var item in workingDoc)
        //            {
        //                if (item.ContentLength > 0)
        //                {
        //                    MemoryStream target = new MemoryStream();
        //                    item.InputStream.CopyTo(target);

        //                    int fileVersion = 0;

        //                    var objFileData = new FileDataVM()
        //                    {
        //                        FileData = target.ToArray(),
        //                        FileName = item.FileName,
        //                        FilePath = path
        //                    };

        //                    fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
        //                    fileVersion++;

        //                    objFileData.Version = Convert.ToString(fileVersion);
        //                    objFileData = objIFileData_Service.Save(objFileData, userID);

        //                    if (objFileData.FileID > 0 && model.TransactionID > 0)
        //                    {
        //                        objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 2);
        //                    }
        //                }
        //            }
        //        }
        //        #endregion

        //        loadComplianceStaus(model.ComplianceScheduleOnID, userID, model.RoleID);
        //    }
        //    return PartialView("_ComplianceStatusUpdate", model);
        //}

        #endregion

        [NonAction]
        private Message CheckRemarkandDocument(ComplianceTransactionVM model, HttpPostedFileBase[] complianceDoc, HttpPostedFileBase[] workingDoc, string btnRejectStatus)
        {
            var result = new Message();
            try
            {
                int UserId = AuthenticationHelper.UserID;
                var lstUserDetails = (from schedule in entities.ComplianceScheduleOns
                                      join instance in entities.ComplianceInstances on schedule.ComplianceInstanceID equals instance.ID
                                      join assingment in entities.ComplianceAssignments on instance.ID equals assingment.ComplianceInstanceID
                                      where schedule.ID == model.ComplianceScheduleOnID
                                      select new
                                      {
                                          assingment.UserID,
                                          assingment.RoleID
                                      }).ToList();

                var isPerformer = (from assignment in lstUserDetails
                                   where assignment.RoleID == SecretarialConst.RoleID.PERFORMER
                                   && assignment.UserID == UserId
                                   select assignment.RoleID).Any();

                var isReviewer = (from assignment in lstUserDetails
                                  where assignment.RoleID == SecretarialConst.RoleID.REVIEWER
                                  && assignment.UserID == UserId
                                  select assignment.RoleID).Any();

                if (model.StatusId == 4 || model.StatusId == 5)
                {
                    if (isReviewer && isPerformer)
                    {
                        if (complianceDoc != null && model.Remarks != null)
                        {
                            result.Success = true;
                        }
                        else
                        {
                            result.Error = true;
                            result.Message = "Please fill Remark and select Compliance Document";
                        }
                    }
                    else
                    {
                        result.Success = true;
                    }
                }
                else if (model.StatusId == 18 || model.StatusId == 17)
                {
                    if (model.Remarks != null)
                    {
                        result.Success = true;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = "Please fill Remark";
                    }
                }
                else if (model.StatusId == 2)
                {
                    if (complianceDoc != null && model.Remarks != null)
                    {
                        result.Success = true;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = "Please fill Remark and select Compliance Document";
                    }
                }
                else
                {
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
            }
            return result;
        }

        public void loadComplianceStaus(long ComplianceScheduleOnID, int userID, int? roleID)
        {
            if(roleID == null)
            {
                ViewBag.ComplianceUpdateStatus = objICompliance.GetComplianceUpdateStatus(ComplianceScheduleOnID, userID);
            }
            else
            {
                ViewBag.ComplianceUpdateStatus = objICompliance.GetComplianceUpdateStatus(ComplianceScheduleOnID, userID, roleID);
            }
        }

        #endregion


        #region Director-My Compliances

        public ActionResult DirectorMyCompliances(string status)
        {
            var model = new MyCompliancesVM();
            model.RoleId = 4;
           
            model.StatusFilter = status;

            switch (status)
            {
                case "Overdue":
                case "Upcoming":
                    model.StatusId = 1;
                    break;
                case "Closed":
                    model.StatusId = 4;
                    break;
                default:
                    model.StatusId = 1;
                    break;
            }

            return View("DirectorMyCompliances", model);
        }

        #endregion

        public ActionResult GetComplianceRemark([DataSourceRequest] DataSourceRequest request)
        {
            List<tbl_ChecklistRemark> _objremark = new List<tbl_ChecklistRemark>();
            _objremark = objIMeeting_Compliances.GetAllRemark();
            // return Json(_objremark, JsonRequestBehavior.AllowGet);
            return Json(_objremark.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}