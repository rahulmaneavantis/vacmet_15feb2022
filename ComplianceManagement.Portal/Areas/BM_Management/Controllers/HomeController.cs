﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class HomeController : Controller
    {
        IHome obj;
        //public static List<int> pageId = new List<int>();
        public static VM_MastersData objpagelist = new VM_MastersData();
        public static string Role = string.Empty;
        public static List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public HomeController(IHome obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TopMenu() // This action is used for menu 
        {
            try
            {
                String key = "Authenticate" + AuthenticationHelper.UserID;
                int userId = AuthenticationHelper.UserID;
                int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                Role = AuthenticationHelper.Role;
                objpagelist.pageName = HttpContext.Cache.Get(key + "Page") as List<string>;
                authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;
                if (objpagelist.pageName == null)
                {
                    objpagelist.pageName = this.GetPage(userId, CustomerId, Role);
                    HttpContext.Cache.Insert(key + "Page", objpagelist.pageName, null, DateTime.Now.AddMinutes(30), Cache.NoSlidingExpiration);
                }
                if (authRecord == null)
                {
                    authRecord = this.getAuthorizeddetails(userId, CustomerId, Role);
                    HttpContext.Cache.Insert(key + "authrecord", authRecord, null, DateTime.Now.AddMinutes(30), Cache.NoSlidingExpiration);
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return PartialView("_sidebarMenu", objpagelist);
        }

        private List<VM_pageAuthentication> getAuthorizeddetails(int userId, int CustomerId, string Role)
        {
            List<VM_pageAuthentication> authdetails = new List<VM_pageAuthentication>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (Role == "SMNGT")
                    {
                        authdetails = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                       where row.CanView == true
                                       && row.Role == Role

                                       select new VM_pageAuthentication
                                       {
                                           Id = row.ID,
                                           pageId = row.ID,
                                           UserId = userId,
                                           Addview = row.CanAdd,
                                           Editview = row.CanEdit,
                                           DeleteView = row.CanDelete,
                                           Pageview = row.CanView,
                                           PageName = row.Name,
                                       }).ToList();
                        if (authdetails.Count == 0)
                        {
                            authdetails = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                           where row.Role == Role
                                           select new VM_pageAuthentication
                                           {
                                               Id = row.ID,
                                               pageId = row.ID,
                                               UserId = userId,
                                               Addview = true,
                                               Editview = true,
                                               DeleteView = true,
                                               Pageview = true,
                                               PageName = row.Name,
                                           }).ToList();
                        }
                    }
                    else if (Role == "DRCTR")
                    {
                        authdetails = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                       where row.CanView == true
                                       && row.Role == Role

                                       select new VM_pageAuthentication
                                       {
                                           Id = row.ID,
                                           pageId = row.ID,
                                           UserId = userId,
                                           Addview = row.CanAdd,
                                           Editview = row.CanEdit,
                                           DeleteView = row.CanDelete,
                                           Pageview = row.CanView,
                                           PageName = row.Name,
                                       }).ToList();
                        if (authdetails.Count == 0)
                        {
                            authdetails = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                           where row.Role == Role
                                           select new VM_pageAuthentication
                                           {
                                               Id = row.ID,
                                               pageId = row.ID,
                                               UserId = userId,
                                               Addview = true,
                                               Editview = true,
                                               DeleteView = true,
                                               Pageview = true,
                                               PageName = row.Name,
                                           }).ToList();
                        }
                    }
                    else
                    {
                        authdetails = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                       where row.CanView == true
                                       //  && row.Role == Role

                                       select new VM_pageAuthentication
                                       {
                                           Id = row.ID,
                                           pageId = row.ID,
                                           UserId = userId,
                                           Addview = row.CanAdd,
                                           Editview = row.CanEdit,
                                           DeleteView = row.CanDelete,
                                           Pageview = row.CanView,
                                           PageName = row.Name,
                                       }).ToList();
                        if (authdetails.Count == 0)
                        {
                            authdetails = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                               // where row.Role == Role
                                           select new VM_pageAuthentication
                                           {
                                               Id = row.ID,
                                               pageId = row.ID,
                                               UserId = userId,
                                               Addview = true,
                                               Editview = true,
                                               DeleteView = true,
                                               Pageview = true,
                                               PageName = row.Name,
                                           }).ToList();
                        }
                    }

                    return authdetails;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        private List<string> GetPage(int userId, int CustomerId, string Role)
        {
            List<string> pageName = new List<string>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (Role == "SMNGT")
                    {
                        pageName = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                    where row.CanView == true
                                    && row.Role == Role
                                    select row.Name).ToList();
                        if (pageName.Count == 0)
                        {
                            pageName = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                        where row.Role == Role
                                        select row.Name).ToList();
                        }
                    }
                    else if (Role == "DRCTR")
                    {
                        pageName = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                    where row.CanView == true
                                    && row.Role == Role
                                    select row.Name).ToList();
                        if (pageName.Count == 0)
                        {
                            pageName = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                        where row.Role == Role
                                        select row.Name).ToList();
                        }
                    }
                    else
                    {
                        pageName = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                    where row.CanView == true
                                    //  && row.Role == Role
                                    select row.Name).ToList();
                        if (pageName.Count == 0)
                        {
                            pageName = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, 0)
                                            // where row.Role == Role
                                        select row.Name).ToList();
                        }
                    }

                    return pageName;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        private string GetCustomerLogo(int customerID)
        {
            string customerLogoPath = string.Empty;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    customerLogoPath = (from cust in entities.Customers
                                        where cust.ID == customerID
                                        select cust.LogoPath).FirstOrDefault();
                    return customerLogoPath;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        private Customer GetCustomerByID(int customerID)
        {
            
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var customerRecord = (from cust in entities.Customers
                                          where cust.ID == customerID
                                          select cust).FirstOrDefault();
                    return customerRecord;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}