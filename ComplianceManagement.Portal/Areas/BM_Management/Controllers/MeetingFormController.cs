﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Windows.Media;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Fields;
using Telerik.Windows.Documents.Flow.Model.Styles;

using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.InteractiveForms;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using BM_ManegmentServices.Services.Forms;
using Syncfusion.DocIO.DLS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingFormController : Controller
    {
        IForms_Service objIForms_Service;
        public MeetingFormController(IForms_Service objForms_Service)
        {
            objIForms_Service = objForms_Service;
        }
        // GET: BM_Management/MeetingForm
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MeetingForm(long MeetingId)
        {
            var model = new MeetingFormsVM();
            model.MeetingId = MeetingId;
            model.MeetingAgendaMappingId = 0;
            model.ComplianceId = 0;
            return PartialView("_MeetingFormList", model);
        }

        #region List of forms by meetingId, MeetingAgendaMappingId, complianceId
        public ActionResult MeetingFormsList(long meetingId, long meetingAgendaMappingId, long complianceId)
        {
            var model = new MeetingFormsVM();
            model.MeetingId = meetingId;
            model.MeetingAgendaMappingId = meetingAgendaMappingId;
            model.ComplianceId = complianceId;
            return PartialView("_MeetingFormList", model);
        }
        #endregion

        public ActionResult PreviewFormFormat(long FormMappingId, long meetingAgendaMappingId)
        {
            var result = objIForms_Service.GenerateMeetingFormData(meetingAgendaMappingId, FormMappingId);
            var getFileFormate = result.FormFormat;
            return Json(getFileFormate, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadFormFormate(long FormMappingId, long meetingAgendaMappingId)
        {
            string generatectc = GenerateAttendenceDocument(FormMappingId, meetingAgendaMappingId);
            return File(generatectc, "application/force-download", Path.GetFileName(generatectc));
        }
        public string GenerateAttendenceDocument(long FormMappingId, long meetingAgendaMappingId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //Commented on 30 Dec 2021 unneccesssary call
                //string getFileFormate = objIForms_Service.GenerateMeetingFormData(meetingAgendaMappingId, FormMappingId);
                //End
                string path = "~/Areas/BM_Management/Documents/" + customerId + "/MeetingForm/" + meetingAgendaMappingId + "/"+ FormMappingId;
                string str = "Form.docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                var result = objIForms_Service.GenerateMeetingFormData(meetingAgendaMappingId, FormMappingId);
                var getFileFormate = result.FormFormat;
                var syncfusionSuccess = false;

                if(result.LetterHead == true)
                {
                    syncfusionSuccess = CreateFormDocumentSyncfusion(fileName, result.FormFormat, result.LetterHead, result.EntityId);
                }

                if(syncfusionSuccess == false)
                {
                    using (Stream output = System.IO.File.OpenWrite(fileName))
                    {
                        DocxFormatProvider provider = new DocxFormatProvider();
                        RadFlowDocument document = CreateAttendenceDocument(FormMappingId, meetingAgendaMappingId, getFileFormate);

                        provider.Export(document, output);
                    }
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public RadFlowDocument CreateAttendenceDocument(long FormMappingId, long meetingAgendaMappingId, string getFileFormate)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);

            try
            {
                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                editor.MoveToParagraphStart(paragraph);

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };
                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();
                RadFlowDocument htmlDocument = htmlProvider.Import(getFileFormate);
                editor.InsertDocument(htmlDocument);
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public bool CreateFormDocumentSyncfusion(string fileName, string formFormate, bool? pageHeader, int entityId)
        {
            var result = false;
            try
            {
                using (WordDocument doc = new WordDocument())
                {
                    doc.EnsureMinimal();

                    WParagraphStyle myParagraphStyle = (WParagraphStyle)doc.AddParagraphStyle("MyParagraphStyle");
                    myParagraphStyle.CharacterFormat.FontName = "Bookman Old Style";
                    myParagraphStyle.CharacterFormat.FontSize = 12;

                    Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider htmlProvider = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider();
                    Telerik.Windows.Documents.Flow.Model.RadFlowDocument htmlDocument = new Telerik.Windows.Documents.Flow.Model.RadFlowDocument();

                    #region Page Headers / Footer
                    var hasPageHeader = false;
                    var isValidHtml = false;
                    var objIDocumentSetting_Service = new BM_ManegmentServices.Services.DocumentManagenemt.DocumentSetting_Service();
                    var letterHead = objIDocumentSetting_Service.GetPageHeaderFooter(entityId);
                    if (letterHead != null)
                    {
                        if (letterHead.UsedInComplianceDoc)
                        {
                            if (!string.IsNullOrEmpty(letterHead.PageHeaderFormat))
                            {
                                #region Page Header
                                IWParagraph paragraphHeader = doc.LastSection.HeadersFooters.Header.AddParagraph();

                                doc.LastSection.PageSetup.HeaderDistance = 0f;


                                htmlDocument = htmlProvider.Import(letterHead.PageHeaderFormat);
                                Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings1 = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                                string htmlstringHeader = htmlProvider.Export(htmlDocument);

                                isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstringHeader, XHTMLValidationType.None);
                                if (isValidHtml)
                                {
                                    paragraphHeader.ParagraphFormat.LeftIndent = -50f;
                                    paragraphHeader.AppendHTML(htmlstringHeader);
                                    paragraphHeader.ParagraphFormat.LeftIndent = -50f;
                                    hasPageHeader = true;
                                }
                                #endregion

                                #region Footer
                                if (hasPageHeader && !string.IsNullOrEmpty(letterHead.PageFooterFormat))
                                {
                                    IWParagraph paragraphFooter = doc.LastSection.HeadersFooters.Footer.AddParagraph();

                                    doc.LastSection.PageSetup.FooterDistance = 0f;

                                    htmlDocument = htmlProvider.Import(letterHead.PageFooterFormat);
                                    string htmlstringFooter = htmlProvider.Export(htmlDocument);

                                    var isValidHtmlFooter = doc.LastSection.Body.IsValidXHTML(htmlstringFooter, XHTMLValidationType.None);
                                    if (isValidHtmlFooter)
                                    {
                                        paragraphFooter.ParagraphFormat.LeftIndent = -50f;
                                        paragraphFooter.AppendHTML(htmlstringFooter);
                                        paragraphFooter.ParagraphFormat.LeftIndent = -50f;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion

                    //if(hasPageHeader)
                    //{
                    //    doc.LastSection.PageSetup.Margins.Left = 0f;
                    //}

                    #region Convert html format in XHTML 1.0
                    htmlDocument = htmlProvider.Import(formFormate);
                    Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                    var htmlstring = htmlProvider.Export(htmlDocument);
                    #endregion

                    WSection section = doc.LastSection;

                    isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);

                    if (isValidHtml)
                    {
                        WParagraph newPara = section.AddParagraph() as WParagraph;
                        newPara = section.AddParagraph() as WParagraph;
                        //newPara.ParagraphFormat.LeftIndent = 15f;
                        newPara.AppendHTML(htmlstring);
                    }

                    try
                    {
                        #region Save Generated Doc
                        //Generate Docx
                        using (FileStream fsDocx = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite))
                        {
                            BinaryWriter bwDocx = new BinaryWriter(fsDocx);
                            if (true)
                            {
                                using (var stream = new MemoryStream())
                                {
                                    doc.Save(stream, Syncfusion.DocIO.FormatType.Docx);
                                    bwDocx.Write(stream.ToArray());
                                }
                            }
                            bwDocx.Close();
                        }

                        result = true;
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
    }
}