﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.HelpVideo;
using BM_ManegmentServices.Services.MeetingsHistory;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{

    public class HelpVideoController : Controller
    {
        IHelpVideoService objIHelpVideoService;
        public HelpVideoController(IHelpVideoService objHelpVideoService)
        {
            this.objIHelpVideoService = objHelpVideoService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("ListOfHelpVideo");
        }

        public ActionResult CreateList()
        {
            return View("ListOfHelpVideo");
        }

        public ActionResult AddEditHelpLink(int id)
        {
            var model = new HelpVideoVM();
            if(id > 0)
            {
                model = objIHelpVideoService.GetHelpLink(id);
            }
            return PartialView("_AddEditHelpLink", model);
        }

        [HttpPost]
        public ActionResult SaveHelpLink(HelpVideoVM obj)
        {
            var userId = Convert.ToInt32(AuthenticationHelper.UserID);
            obj = objIHelpVideoService.CreateHelpLink(obj, userId);
            ModelState.Clear();
            return PartialView("_AddEditHelpLink", obj);
        }
        [HttpPost]
        public ActionResult SetSerialNumbers(List<HelpVideoSRVM> lstItems)
        {
            var userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var obj = objIHelpVideoService.UpdateSerialNumbers(lstItems, userId);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLink(int id)
        {
            var userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIHelpVideoService.DeleteHelpLink(id, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HelpVideoParent()
        {
            var data = objIHelpVideoService.GetHelpParentList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HelpVideoData([DataSourceRequest] DataSourceRequest request)
        {
            var data = objIHelpVideoService.GetHelpList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult HelpVideoDataImplementation([DataSourceRequest] DataSourceRequest request)
        {
            var data = objIHelpVideoService.GetHelpListImplementation();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult HelpVideoTreeData([DataSourceRequest] DataSourceRequest request)
        {
            var data = objIHelpVideoService.GetHelpListImplementation();
            var result = data.Select(e => new HelpVideoVM
            {
                HelpVideoId = e.HelpVideoId,
                Description_ = e.Description_,
                HelpName = e.HelpName,
                ParentId = e.ParentId,
                FileType = e.FileType,
                FileURL = e.FileURL,
                SrNo = e.SrNo,
                IsLinkActive = e.IsLinkActive,
                hasChildren = data.Where(s => s.ParentId == e.HelpVideoId).Count() > 0
            }).ToTreeDataSourceResult(request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //New File

        public class CloudFile
        {
            public int HelpVideoID { get; set; }
            public string DirectoryPath { get; set; }
            public string FileNameNew { get; set; }
            public long FileSize { get; set; }
            public string FileName { get; set; }
            public string URL { get; set; }
            public long Size { get; set; }
            public long BlockCount { get; set; }
            public CloudBlockBlob BlockBlob { get; set; }
            public DateTime StartTime { get; set; }
            public string UploadStatusMessage { get; set; }
            public bool IsUploadCompleted { get; set; }
            public static CloudFile CreateFromIListBlobItem(IListBlobItem item)
            {
                if (item is CloudBlockBlob)
                {
                    var blob = (CloudBlockBlob)item;
                    return new CloudFile
                    {
                        FileName = blob.Name,
                        URL = blob.Uri.ToString(),
                        Size = blob.Properties.Length
                    };
                }
                return null;
            }
        }

        public ActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SetMetadata(int blocksCount, string fileName, long fileSize, int helpVideoId, string productMode)
        {
            string fileKey = Convert.ToString(Guid.NewGuid());
            string fileExtension = Path.GetExtension(fileName);

            var fileNameNew = fileKey + fileExtension;

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            var pathDetails = string.IsNullOrEmpty(productMode) ? "DFM" : productMode;
            var directoryPath = "cdn/AVASEC/" + pathDetails;
            CloudBlobContainer container = blobClient.GetContainerReference(directoryPath);
            container.CreateIfNotExistsAsync();

            var fileToUpload = new CloudFile()
            {
                HelpVideoID = helpVideoId,
                DirectoryPath = directoryPath,
                BlockCount = blocksCount,
                FileName = fileName,
                FileNameNew = fileNameNew,
                FileSize = fileSize,
                Size = fileSize,
                BlockBlob = container.GetBlockBlobReference(fileNameNew),
                StartTime = DateTime.Now,
                IsUploadCompleted = false,
                UploadStatusMessage = string.Empty
            };
            Session.Add("CurrentFile", fileToUpload);
            return Json(true);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UploadChunk(int id)
        {
            HttpPostedFileBase request = Request.Files["Slice"];
            byte[] chunk = new byte[request.ContentLength];
            request.InputStream.Read(chunk, 0, Convert.ToInt32(request.ContentLength));
            JsonResult returnData = null;
            string fileSession = "CurrentFile";
            if (Session[fileSession] != null)
            {
                CloudFile model = (CloudFile)Session[fileSession];
                returnData = UploadCurrentChunk(model, chunk, id);
                if (returnData != null)
                {
                    return returnData;
                }
                if (id == model.BlockCount)
                {
                    return CommitAllChunks(model);
                }
            }
            else
            {
                returnData = Json(new
                {
                    error = true,
                    isLastBlock = false,
                    message = string.Format(System.Globalization.CultureInfo.CurrentCulture,
                        "Failed to Upload file.", "Session Timed out")
                });
                return returnData;
            }

            return Json(new { error = false, isLastBlock = false, message = string.Empty });
        }

        private ActionResult CommitAllChunks(CloudFile model)
        {
            model.IsUploadCompleted = true;
            bool errorInOperation = false;
            try
            {
                var blockList = Enumerable.Range(1, (int)model.BlockCount).ToList<int>().ConvertAll(rangeElement =>
                            Convert.ToBase64String(Encoding.UTF8.GetBytes(
                                string.Format(CultureInfo.InvariantCulture, "{0:D4}", rangeElement))));
                model.BlockBlob.PutBlockList(blockList);
                var duration = DateTime.Now - model.StartTime;
                float fileSizeInKb = model.Size / 1024;
                string fileSizeMessage = fileSizeInKb > 1024 ?
                    string.Concat((fileSizeInKb / 1024).ToString(CultureInfo.CurrentCulture), " MB") :
                    string.Concat(fileSizeInKb.ToString(CultureInfo.CurrentCulture), " KB");
                model.UploadStatusMessage = string.Format(CultureInfo.CurrentCulture,
                    "File uploaded successfully. {0} took {1} seconds to upload",
                    fileSizeMessage, duration.TotalSeconds);

                var userId = Convert.ToInt32(AuthenticationHelper.UserID);
                objIHelpVideoService.UpdateHelpLink(model.HelpVideoID, model.DirectoryPath, model.FileName, model.FileNameNew, model.FileSize,  userId);
            }
            catch (StorageException e)
            {
                model.UploadStatusMessage = "Failed to Upload file. Exception - " + e.Message;
                errorInOperation = true;
            }
            finally
            {
                Session.Remove("CurrentFile");
            }
            return Json(new
            {
                error = errorInOperation,
                isLastBlock = model.IsUploadCompleted,
                message = model.UploadStatusMessage
            });
        }

        private JsonResult UploadCurrentChunk(CloudFile model, byte[] chunk, int id)
        {
            using (var chunkStream = new MemoryStream(chunk))
            {
                var blockId = Convert.ToBase64String(Encoding.UTF8.GetBytes(
                        string.Format(CultureInfo.InvariantCulture, "{0:D4}", id)));
                try
                {
                    model.BlockBlob.PutBlock(
                        blockId,
                        chunkStream, null, null,
                        new BlobRequestOptions()
                        {
                            RetryPolicy = new Microsoft.WindowsAzure.Storage.RetryPolicies.LinearRetry(TimeSpan.FromSeconds(10), 3)
                        },
                        null);
                    return null;
                }
                catch (StorageException e)
                {
                    Session.Remove("CurrentFile");
                    model.IsUploadCompleted = true;
                    model.UploadStatusMessage = "Failed to Upload file. Exception - " + e.Message;
                    return Json(new { error = true, isLastBlock = false, message = model.UploadStatusMessage });
                }
            }
        }
        //End
    }
}