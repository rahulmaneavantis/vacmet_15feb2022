﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Forms;
using BM_ManegmentServices.Services.HelpVideo;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.MeetingsHistory;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{

    public class FormsController : Controller
    {
        IForms_Service objIForms_Service;
        public FormsController(IForms_Service objForms_Service)
        {
            this.objIForms_Service = objForms_Service;
        }
        public ActionResult DPT3()
        {
            return View("DPT3", new ExcelFileFormUploadVM());
        }

        [HttpPost]
        public ActionResult UploadExcel(ExcelFileFormUploadVM model)
        {
            TempData["DPT3EFormVMData"] = null;
            if (model != null)
            {
                if (model.EntityId <= 0)
                {
                    model.Error = true;
                    model.Message = "Please Select Entity";
                    return PartialView("DPT3", model);
                }

                if (model.File == null)
                {
                    model.Error = true;
                    model.Message = "Please Upload File";
                    return PartialView("DPT3", model);
                }

                if (model.File != null)
                {
                    try
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(model.File.InputStream))
                        {
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["DPT-3"];
                            int xlrow2 = excelSheet.Dimension.End.Row;

                            DPT3EFormVM objDPT3EFormVM = new DPT3EFormVM()
                            {
                                _08_NetWorth = new _08_NetWorthVM(),
                                _15_A_ReceiptOfMoney = new _15_A_ReceiptOfMoneyVM(),
                                _15_B_AmountReceivedFrom = new _15_B_AmountReceivedFromVM(),
                                _15_C_AnyAmountReceipt = new _15_C_AnyAmountReceiptVM(),
                                _15_D_AnyAmountReceiptAsLoan = new _15_D_AnyAmountReceiptAsLoanVM(),
                                _15_I_AnyAmountReceiptAsLoan = new _15_I_AnyAmountReceiptAsLoanVM(),
                                _15_M_AnyAmountReceiptAsLoan = new _15_M_AnyAmountReceiptAsLoanVM(),
                                _15_S_AnyAmountReceiptAsLoan = new _15_S_AnyAmountReceiptAsLoanVM()
                            };

                            #region 8
                            int rowIndex = 4;//4
                            objDPT3EFormVM._08_NetWorth.PaidUpShareCapital = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //5
                            objDPT3EFormVM._08_NetWorth.FreeReserves = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //6
                            objDPT3EFormVM._08_NetWorth.SecuritiesPremium = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //7
                            objDPT3EFormVM._08_NetWorth.BalanceOfDeferred = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //8
                            objDPT3EFormVM._08_NetWorth.AccumulatedLoss = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //9
                            objDPT3EFormVM._08_NetWorth.AccumulatedUnprovidedDep = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //10
                            objDPT3EFormVM._08_NetWorth.MiscellaneousExpense = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //11
                            objDPT3EFormVM._08_NetWorth.OtherIntangible = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //12
                            objDPT3EFormVM._08_NetWorth.NetWorth = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15

                            #region 15 - A
                            rowIndex = 15; //15
                            objDPT3EFormVM._15_A_ReceiptOfMoney.CentralGovernment = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //16
                            objDPT3EFormVM._15_A_ReceiptOfMoney.stateGovernment = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //17
                            objDPT3EFormVM._15_A_ReceiptOfMoney.LocalAuthority = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //18
                            objDPT3EFormVM._15_A_ReceiptOfMoney.StatutoryAuthority = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - B
                            rowIndex = 20; //20
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignGovernments = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //21
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignInternationalBanks = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //22
                            objDPT3EFormVM._15_B_AmountReceivedFrom.MultilateralFI = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //23
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignGODFI = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //24
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignECA = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();


                            rowIndex++; //25
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignCO = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //26
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignBodyCor = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //27
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignCitizens = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //28
                            objDPT3EFormVM._15_B_AmountReceivedFrom.ForeignAuthorities = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //29
                            objDPT3EFormVM._15_B_AmountReceivedFrom.PersonsResidentsOutsideIndia = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - C
                            rowIndex = 31; //31
                            objDPT3EFormVM._15_C_AnyAmountReceipt.BankingCompany = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //32
                            objDPT3EFormVM._15_C_AnyAmountReceipt.SBI = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //33
                            objDPT3EFormVM._15_C_AnyAmountReceipt.NotifiedByTheCentralGovernment = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //34
                            objDPT3EFormVM._15_C_AnyAmountReceipt.CorrespondingNewBank = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //35
                            objDPT3EFormVM._15_C_AnyAmountReceipt.CooperativeBank = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - D
                            rowIndex = 37; //37
                            objDPT3EFormVM._15_D_AnyAmountReceiptAsLoan.PublicFinancialInstitutions = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            //rowIndex++; //38
                            //objDPT3EFormVM._15_D_AnyAmountReceiptAsLoan.Government = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //38
                            objDPT3EFormVM._15_D_AnyAmountReceiptAsLoan.AnyRegionalFI = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //39
                            objDPT3EFormVM._15_D_AnyAmountReceiptAsLoan.InsuranceCompanies = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //40
                            objDPT3EFormVM._15_D_AnyAmountReceiptAsLoan.ScheduledBanks = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - E to H
                            rowIndex++; //41
                            objDPT3EFormVM._15_E_IssueOfCommercial = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();

                            rowIndex++; //42
                            objDPT3EFormVM._15_F_FromOtherCompany = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();

                            rowIndex++; //43
                            objDPT3EFormVM._15_G_IssueOfCommercial = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();

                            rowIndex++; //44
                            objDPT3EFormVM._15_H_FromPerson = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - I
                            rowIndex++; //45
                            objDPT3EFormVM._15_I_AnyAmountReceiptAsLoan.AmountRaisedBy = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //46
                            objDPT3EFormVM._15_I_AnyAmountReceiptAsLoan.BondsOrDebentures = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - J to L
                            rowIndex++; //47
                            objDPT3EFormVM._15_J_NonConvertibleDebentures = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //48
                            objDPT3EFormVM._15_K_EmployeeOfCompany = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //49
                            objDPT3EFormVM._15_L_NonInterest = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - M
                            rowIndex = 51; //51
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.AdvanceForSupply = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //52
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.AdvanceAccounted = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();

                            rowIndex++; //53
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.SecurityDeposit = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //54
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.LongTermProjects = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //55
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.TowardsConsideration = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();

                            rowIndex++; //56
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.SectoralRegulator = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //57
                            objDPT3EFormVM._15_M_AnyAmountReceiptAsLoan.Publication = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #region 15 - N - S
                            rowIndex = 58; //58
                            objDPT3EFormVM._15_N_ByPromoters = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //59
                            objDPT3EFormVM._15_O_ByNidhiCompnay = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //60
                            objDPT3EFormVM._15_P_ByChitFund = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //61
                            objDPT3EFormVM._15_Q_SEBI = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //62
                            objDPT3EFormVM._15_R_ByStartUp = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();


                            rowIndex = 64; //64
                            objDPT3EFormVM._15_S_AnyAmountReceiptAsLoan.InvestmentFund = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //66
                            objDPT3EFormVM._15_S_AnyAmountReceiptAsLoan.CapitalFund = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //67
                            objDPT3EFormVM._15_S_AnyAmountReceiptAsLoan.InfraTrust = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //68
                            objDPT3EFormVM._15_S_AnyAmountReceiptAsLoan.RealEstateTrust = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            rowIndex++; //68
                            objDPT3EFormVM._15_S_AnyAmountReceiptAsLoan.MutualFund = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                            #endregion

                            #endregion

                            TempData["DPT3EFormVMData"] = objDPT3EFormVM;
                            model.Success = true;
                            model.CanGenerateEform = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        model.Error = true;
                        model.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                model = new ExcelFileFormUploadVM();
                model.Error = true;
            }
            return PartialView("DPT3", model);
        }

        public ActionResult DownloadDPT3(int entityId)
        {
            if (TempData["DPT3EFormVMData"] != null)
            {
                var obj = TempData["DPT3EFormVMData"] as DPT3EFormVM;
                TempData.Keep("DPT3EFormVMData");
                var form = objIForms_Service.GenerateDPT3(obj, entityId);
                if (form.Success)
                {
                    return File(form.FormData, System.Net.Mime.MediaTypeNames.Application.Octet, form.FormName);
                }
                else
                {
                    return new EmptyResult();
                }
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult DownloadSampleExcelDPT_3()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\EntityDocument\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "DPT-3 Sample File.xlsx");
            string fileName = "DPT-3 Sample File.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}