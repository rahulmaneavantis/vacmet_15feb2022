﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.UIForm;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.UIForms;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class UIFormController : Controller
    {
        IUIFormService objIUIFormService;

        public UIFormController(IUIFormService objUIFormService)
        {
            objIUIFormService = objUIFormService;
        }
        // GET: BM_Management/UIForm
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetUIForm(long UIFormID, long MeetingId, long AgendaId, long MappingId)
        {
            if(UIFormID == 1)
            {
                return AppointmentOfCFO(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 2)
            {
                return AppointmentOfCS(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 3)
            {
                return AppointmentOfCEO(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 4)
            {
                return AppointmentOfManager(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 6)
            {
                return CessationOfCFO(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 7)
            {
                return CessationOfCS(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 8)
            {
                return CessationOfCEO(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 9)
            {
                return CessationOfManager(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 11)
            {
                return AppointmentOfMD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 12)
            {
                return AppointmentOfWTH(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 13)
            {
                return AppointmentOfADDD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 14)
            {
                return AppointmentOfCAVD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 15)
            {
                return AppointmentOfNOMD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 16)
            {
                return AppointmentOfALTD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 17)
            {
                return AppointmentOfADDD_Independant(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 10 || UIFormID == 28 || UIFormID == 29)
            {
                return CessationOfADDD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 21)
            {
                return CessationOfMD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 22)
            {
                return CessationOfWTD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 23)
            {
                return CessationOfADDD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 24)
            {
                return CessationOfCAVD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 25)
            {
                return CessationOfNOMD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 26)
            {
                return CessationOfALTD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 27)
            {
                return CessationOfADDD(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 31)
            {
                return AppointmentOfInternalAuditor(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 32)
            {
                return AppointmentOfSecretarialAuditor(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 33)
            {
                return AppointmentOfFirstStatutoryAuditor(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 34)
            {
                return AppointmentOfFirstStatutoryAuditor(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 35)
            {
                return AppointmentOfCostAuditor(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 36)
            {
                return RatificationOfCostAuditor(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 37 || UIFormID == 38 || UIFormID == 39 || UIFormID == 40) 
            {
                return Auditor_ResignationDetails(MeetingId, AgendaId, MappingId, UIFormID);
            }
            else if (UIFormID == 41 || UIFormID == 42 || UIFormID == 44)
            {
                return AppointmentOfStatutoryAuditorInCaseOfCasualVacancy(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 45)
            {
                return AppointmentOfStatutoryAuditorRecommendationInBorad(MeetingId, AgendaId, MappingId, UIFormID);
            }
            else if (UIFormID == 46)
            {
                return AppointmentOfStatutoryAuditorRecommendationInBorad(MeetingId, AgendaId, MappingId, UIFormID);
            }
            else if (UIFormID == 50)
            {
                return FinancialResult_Listed(MeetingId, AgendaId, MappingId, SecretarialConst.EntityTypeID.LISTED);
            }
            else if (UIFormID == 51)
            {
                return FinancialResult_Listed(MeetingId, AgendaId, MappingId, SecretarialConst.EntityTypeID.PUBLIC);
            }
            else if (UIFormID == 52)
            {
                return FinancialResult_Listed(MeetingId, AgendaId, MappingId, SecretarialConst.EntityTypeID.PRIVATE);
            }
            else if (UIFormID == 53)
            {
                return FinancialResultForYearEnd_Private(MeetingId, AgendaId, MappingId, SecretarialConst.EntityTypeID.PRIVATE);
            }
            else if (UIFormID == 54)
            {
                return FinancialResultForYearEnd_Listed(MeetingId, AgendaId, MappingId, SecretarialConst.EntityTypeID.PRIVATE);
            }
            else if (UIFormID == 70 || UIFormID == 71)
            {
                return Calling_GM(MeetingId, AgendaId, MappingId, UIFormID);
            }
            else if (UIFormID == 100)
            {
                return BooksOfAccountsUIForm(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 101)
            {
                return WithinLocalLimit(MeetingId, AgendaId, MappingId);
            }
            else if (UIFormID == 55 || UIFormID == 56)
            {
                return IncreaseInAuthorisedCapital(MeetingId, AgendaId, MappingId);
            }
            else
            {
                return PartialView("_UIFormNotFound");
            }
        }

        #region Appointment of CEO/CFO/CS/Manager
        public PartialViewResult AppointmentOfCFO(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfCFO(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }

        public PartialViewResult AppointmentOfCS(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfCS(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfCEO(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfCEO(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }

        public PartialViewResult AppointmentOfManager(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfManager(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }

        [HttpPost]
        public PartialViewResult SaveAppointmentOfCFO(UIForm_KMPMasterVM obj)
        {
            if(ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveAppointmentOfCFO(obj, userID, customerID);

                return PartialView("_CFOAppointment", model);
            }

            return PartialView("_CFOAppointment", obj);
        }
        #endregion

        #region Cessation of CEO/CFO/CS/Manager
        public PartialViewResult CessationOfCFO(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfCFO(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }

        public PartialViewResult CessationOfCS(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfCS(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }

        public PartialViewResult CessationOfCEO(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfCEO(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }
        public PartialViewResult CessationOfManager(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfManager(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }

        [HttpPost]
        public PartialViewResult SaveCessationOfKMP(UIForm_KMPMasterCessationVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveCessationOfKMP(obj, userID, customerID);

                return PartialView("_ResignationOfKMP", model);
            }

            return PartialView("_ResignationOfKMP", obj);
        }
        #endregion

        #region Appointment of Director
        public PartialViewResult AppointmentOfMD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfMD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfWTH(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfWTD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfADDD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfADDD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfCAVD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfCAVD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfNOMD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfNOMD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfALTD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfALTD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        public PartialViewResult AppointmentOfADDD_Independant(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfADDD_Independant(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorAppointment", model);
        }
        [HttpPost]
        public PartialViewResult SaveAppointmentOfDirector(UIForm_DirectorMasterVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveAppointmentOfDirector(obj, userID, customerID);

                return PartialView("_DirectorAppointment", model);
            }

            return PartialView("_DirectorAppointment", obj);
        }
        #endregion

        #region Cessation of Director
        public PartialViewResult CessationOfDirector(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfMD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }
        public PartialViewResult CessationOfMD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfMD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }

        public PartialViewResult CessationOfWTD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfWTD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }
        public PartialViewResult CessationOfADDD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfADDD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }

        public PartialViewResult CessationOfCAVD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfCAVD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }
        public PartialViewResult CessationOfNOMD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfNOMD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }
        public PartialViewResult CessationOfALTD(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.CessationOfALTD(MeetingId, AgendaId, MappingId);
            return PartialView("_DirectorCessation", model);
        }

        [HttpPost]
        public PartialViewResult SaveCessationOfDirector(UIForm_DirectorMasterCessationVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveCessationOfDirector(obj, userID, customerID);

                return PartialView("_DirectorCessation", model);
            }

            return PartialView("_DirectorCessation", obj);
        }
        #endregion

        #region Appointment of Auditor
        public PartialViewResult AppointmentOfInternalAuditor(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOFInternalAuditor(MeetingId, AgendaId, MappingId);
            return PartialView("_Auditor_InternalAppointment", model);
        }
        [HttpPost]
        public PartialViewResult SaveAppointmentOfInternalAuditor(VMInternalAuditor obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveAppointmentOfInternalAuditor(obj, userID, customerID);

                return PartialView("_Auditor_InternalAppointment", model);
            }

            return PartialView("_Auditor_InternalAppointment", obj);
        }

        public PartialViewResult AppointmentOfSecretarialAuditor(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfSecretarialAuditor(MeetingId, AgendaId, MappingId);
            return PartialView("_Auditor_SecretarialAppointment", model);
        }
        [HttpPost]
        public PartialViewResult SaveAppointmentOfSecretarialAuditor(VM_SecreterialAuditor obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveAppointmentOfSecretarialAuditor(obj, userID, customerID);

                return PartialView("_Auditor_SecretarialAppointment", model);
            }

            return PartialView("_Auditor_SecretarialAppointment", obj);
        }


        public PartialViewResult AppointmentOfFirstStatutoryAuditor(long MeetingId, long AgendaId, long MappingId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUIFormService.AppointmentOfFirstStatutoryAuditor(MeetingId, AgendaId, MappingId, customerID);
            return PartialView("_Auditor_StatutoryAppointment", model);
        }

        //public PartialViewResult AppointmentOfStatutoryAuditor(long MeetingId, long AgendaId, long MappingId)
        //{
        //    var model = objIUIFormService.AppointmentOfStatutoryAuditor(MeetingId, AgendaId, MappingId);
        //    return PartialView("_Auditor_StatutoryAppointment", model);
        //}
        [HttpPost]
        public PartialViewResult SaveAppointmentOfStatutoryAuditor(VMStatutory_Auditor obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveAppointmentOfStatutoryAuditor(obj, userID, customerID);

                return PartialView("_Auditor_StatutoryAppointment", model);
            }

            return PartialView("_Auditor_StatutoryAppointment", obj);
        }

        public PartialViewResult AppointmentOfCostAuditor(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.AppointmentOfOriginalCostAuditor(MeetingId, AgendaId, MappingId);
            return PartialView("_Auditor_CostAppointment", model);
        }
        [HttpPost]
        public PartialViewResult SaveAppointmentOfCostAuditor(VM_CostAuditor obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveAppointmentOfCostAuditor(obj, userID, customerID);

                return PartialView("_Auditor_CostAppointment", model);
            }

            return PartialView("_Auditor_CostAppointment", obj);
        }

        public PartialViewResult RatificationOfCostAuditor(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.RatificationCostAuditor(MeetingId, AgendaId, MappingId);
            return PartialView("_Auditor_CostRatification", model);
        }
        [HttpPost]
        public PartialViewResult SaveRatificationOfCostAuditor(VM_CostAuditorRatification obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveRatificationCostAuditor(obj, userID, customerID);

                return PartialView("_Auditor_CostRatification", model);
            }

            return PartialView("_Auditor_CostRatification", obj);
        }
        #endregion

        #region Resignation Of Auditor
        public PartialViewResult ResignationOfAuditor(long MeetingId, long AgendaId, long MappingId, long UIFormId)
        {
            var model = objIUIFormService.ResignationOfAuditor(MeetingId, AgendaId, MappingId, UIFormId);
            return PartialView("_Auditor_Resignation", model);
        }
        
        [HttpPost]
        public PartialViewResult SaveResignationOfAuditor(Auditor_ResignationVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveResignationOfAuditor(obj, userID, customerID);

                return PartialView("_Auditor_Resignation", model);
            }

            return PartialView("_Auditor_Resignation", obj);
        }

        public PartialViewResult Auditor_ResignationDetails(long MeetingId, long AgendaId, long MappingId, long UIFormId)
        {
            var model = objIUIFormService.ResignationOfAuditorNew(MeetingId, AgendaId, MappingId, UIFormId);
            return PartialView("_Auditor_ResignationDetails", model);
        }

        [HttpPost]
        public PartialViewResult SaveResignationDetailsOfAuditor(UIForm_AuditorResignationVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveResignationDetailsOfAuditor(obj, userID, customerID);

                return PartialView("_Auditor_ResignationDetails", model);
            }

            return PartialView("_Auditor_ResignationDetails", obj);
        }
        #endregion

        #region Casual vacancy Auditor
        public PartialViewResult AppointmentOfStatutoryAuditorInCaseOfCasualVacancy(long MeetingId, long AgendaId, long MappingId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUIFormService.AppointmentOfStatutoryAuditorInCaseOfCasualVacancy(MeetingId, AgendaId, MappingId, customerID);
            return PartialView("_Auditor_StatutoryAppointment", model);
        }
        #endregion

        #region SubSequent
        public PartialViewResult AppointmentOfStatutoryAuditorRecommendationInBorad(long MeetingId, long AgendaId, long MappingId, long uIFormID)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUIFormService.AppointmentOfStatutoryAuditorRecommendationInBorad(MeetingId, AgendaId, MappingId, customerID, (int)uIFormID);
            return PartialView("_Auditor_StatutoryAppointment", model);
        }
        #endregion

        #region Fianancial result
        public PartialViewResult FinancialResult_Listed(long MeetingId, long AgendaId, long MappingId, int EntityTypeId)
        {
            var model = objIUIFormService.FinancialResultListed(MeetingId, AgendaId, MappingId, EntityTypeId);
            return PartialView("_FinancialResult_Listed", model);
        }

        public PartialViewResult FinancialResult_Public(long MeetingId, long AgendaId, long MappingId, int EntityTypeId)
        {
            var model = objIUIFormService.FinancialResultListed(MeetingId, AgendaId, MappingId, EntityTypeId);
            return PartialView("_FinancialResult_Listed", model);
        }

        public PartialViewResult FinancialResult_Private(long MeetingId, long AgendaId, long MappingId, int EntityTypeId)
        {
            var model = objIUIFormService.FinancialResultListed(MeetingId, AgendaId, MappingId, EntityTypeId);
            return PartialView("_FinancialResult_Listed", model);
        }

        public PartialViewResult SaveFinancialResult_Listed(UIForm_FinancialResult_ListedVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveFinancialResultListed(obj, userID, customerID);

                return PartialView("_FinancialResult_Listed", model);
            }
            return PartialView("_FinancialResult_Listed", obj);
        }

        public PartialViewResult FinancialResultForYearEnd_Private(long MeetingId, long AgendaId, long MappingId, int EntityTypeId)
        {
            var model = objIUIFormService.FinancialResultForYearEnded_Private(MeetingId, AgendaId, MappingId, EntityTypeId);
            return PartialView("_FinancialResultForYeadEnd_Private", model);
        }

        public PartialViewResult FinancialResultForYearEnd_Listed(long MeetingId, long AgendaId, long MappingId, int EntityTypeId)
        {
            var model = objIUIFormService.FinancialResultForYearEnded_Listed(MeetingId, AgendaId, MappingId, EntityTypeId);
            return PartialView("_FinancialResultForYeadEnd_Listed", model);
        }

        public PartialViewResult SaveFinancialResultYearEnd_Private(UIForm_FinancialResultYearEnded_PrivateVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveFinancialResultForYearEnded_Private(obj, userID, customerID);

                return PartialView("_FinancialResultForYeadEnd_Private", model);
            }
            return PartialView("_FinancialResultForYeadEnd_Private", obj);
        }

        public PartialViewResult SaveFinancialResultYearEnd_Listed(UIForm_FinancialResultYearEnded_ListedVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveFinancialResultForYearEnded_Listed(obj, userID, customerID);

                return PartialView("_FinancialResultForYeadEnd_Listed", model);
            }
            return PartialView("_FinancialResultForYeadEnd_Listed", obj);
        }
        #endregion

        #region Calling AGM/EGM
        public PartialViewResult Calling_GM(long MeetingId, long AgendaId, long MappingId, long UIFormID)
        {
            var model = objIUIFormService.CallingGeneralMeeting(MeetingId, AgendaId, MappingId, UIFormID);
            return PartialView("_AGMCalling", model);
        }
        public PartialViewResult SaveCalling_GM(UIForm_Calling_GeneralMeetingVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var model = objIUIFormService.SaveCallingGeneralMeeting(obj, userID, customerID);

                return PartialView("_AGMCalling", model);
            }
            return PartialView("_AGMCalling", obj);
        }
        #endregion


        #region Books of Accoutns
        public PartialViewResult BooksOfAccountsUIForm(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.BooksOfAccoutns(MeetingId, AgendaId, MappingId);
            return PartialView("_BooksOfAccountsUIForm", model);
        }
        public PartialViewResult SaveBooksOfAccountsUIForm(EntityMasterBooksOfAccountsVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                var model = objIUIFormService.SaveBooksOfAccoutns(obj, userID);

                return PartialView("_BooksOfAccountsUIForm", model);
            }
            return PartialView("_BooksOfAccountsUIForm", obj);
        }
        #endregion

        #region Shifting of Register Office
        public PartialViewResult WithinLocalLimit(long MeetingId, long AgendaId, long MappingId)
        {
            var model = objIUIFormService.WithinLocalLimit(MeetingId, AgendaId, MappingId);
            return PartialView("_ShiftingRegisterOffice", model);
        }
        public PartialViewResult SaveWithinLocalLimit(ShiftingRegisterOfficeVM obj)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                var model = objIUIFormService.SaveWithinLocalLimit(obj, userID);

                return PartialView("_ShiftingRegisterOffice", model);
            }
            return PartialView("_ShiftingRegisterOffice", obj);
        }
        #endregion

        #region Increase in Authorised Capital
        public PartialViewResult IncreaseInAuthorisedCapital(long MeetingId, long AgendaId, long MappingId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIUIFormService.IncreaseInAuthorisedCapital(MeetingId, AgendaId, MappingId, customerID, userID);
            return PartialView("_ShareCapital_IncreaseAuthorisedCapital", model);
        }

        [HttpPost]
        public ActionResult ADDEquiety(string submitButton, VMCapitalData objEquity)
        {
            try
            {
                if (submitButton == null)
                {
                    if (objEquity.shares == null)
                    {
                        objEquity.shares = new List<Shares>();
                    }

                    if (objEquity.count < objEquity.shares.Count)
                    {
                        List<Shares> lstTemp = new List<Shares>();
                        for (int i = 0; i < objEquity.count; i++)
                        {
                            lstTemp.Add(objEquity.shares[i]);
                        }
                        objEquity.shares = lstTemp;
                    }
                    else
                    {
                        for (int i = objEquity.shares.Count; i < objEquity.count; i++)
                        {

                            objEquity.shares.Add(new Shares());
                        }
                    }
                    return PartialView("_ShareCapital_IncreaseAuthorisedCapital_EQ", objEquity);
                }
                else if (submitButton == "Save")
                {

                    objEquity = objIUIFormService.AddEquietydtls(objEquity, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                    ModelState.Clear();
                    return PartialView("_ShareCapital_IncreaseAuthorisedCapital_EQ", objEquity);
                }
                else
                {
                    return PartialView("_ShareCapital_IncreaseAuthorisedCapital_EQ", objEquity);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return PartialView("_ShareCapital_IncreaseAuthorisedCapital_EQ", objEquity);
        }
        [HttpPost]
        public PartialViewResult ADDprifrenceMulti(string submitButton, VMPrifrenscShare objPrefrence)
        {
            try
            {
                if (submitButton == null)
                {
                    if (objPrefrence.PsharesCoupons == null)
                    {
                        objPrefrence.PsharesCoupons = new List<pShareCouponRate>();
                    }

                    if (objPrefrence.pcount < objPrefrence.PsharesCoupons.Count)
                    {
                        var lstTemp = new List<pShareCouponRate>();
                        for (int i = 0; i < objPrefrence.pcount; i++)
                        {
                            lstTemp.Add(objPrefrence.PsharesCoupons[i]);
                        }
                        objPrefrence.PsharesCoupons = lstTemp;
                    }
                    else
                    {
                        for (int i = objPrefrence.PsharesCoupons.Count; i < objPrefrence.pcount; i++)
                        {
                            objPrefrence.PsharesCoupons.Add(new pShareCouponRate());
                        }
                    }
                    return PartialView("_ShareCapital_IncreaseAuthorisedCapital_PRE", objPrefrence);
                }
                else if (submitButton == "Save")
                {
                    objPrefrence = objIUIFormService.AddPrefrenceMultiple(objPrefrence, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    ModelState.Clear();
                    return PartialView("_ShareCapital_IncreaseAuthorisedCapital_PRE", objPrefrence);
                }
                else
                {
                    return PartialView("_ShareCapital_IncreaseAuthorisedCapital_PRE", objPrefrence);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_ShareCapital_IncreaseAuthorisedCapital_PRE", objPrefrence);
            }
        }
        #endregion
    }
}