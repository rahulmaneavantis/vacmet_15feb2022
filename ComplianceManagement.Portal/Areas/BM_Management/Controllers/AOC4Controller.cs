﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Forms;
using BM_ManegmentServices.Services.AOC4;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.MeetingsHistory;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AOC4Controller : Controller
    {
        IAOC4Service objIAOC4Service;
        IForms_Service objIForms_Service;
        public AOC4Controller(IAOC4Service objAOC4Service, IForms_Service objForms_Service)
        {
            objIAOC4Service = objAOC4Service;
            objIForms_Service = objForms_Service;
        }

        #region Masters
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            var model = new AOC4_List_VM();
            return PartialView("_Create", model);
        }
        public ActionResult Save(AOC4_List_VM obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            obj = objIAOC4Service.CreateAOC4(obj, userID, customerID);
            return PartialView("_Create", obj);
        }
        public ActionResult AOC4(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetAOC4Master(id, customerID);
            return View("_AOC4Master", model);
        }
        #endregion

        #region Company Info
        public ActionResult CompanyInfo(long id, int entityId)
        {
            var model = objIAOC4Service.GetCompanyInfo(id, entityId);
            return PartialView("_CompanyInfo", model);
        }
        [HttpPost]
        public ActionResult SaveCompanyInfo(AOC4_CompanyInfoVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (ModelState.IsValid)
            {
                model = objIAOC4Service.SaveCompanyInfo(model, userID);
            }
            return PartialView("_CompanyInfo", model);
        }

        public ActionResult PreviewCompanyInfo(long id, int entityId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetCompanyInfo(id, entityId);
            return PartialView("_CompanyInfoPreview", model);
        }
        #endregion

        #region Balance sheet
        public ActionResult BalanceSheet(long id, int entityId)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_BalanceSheet", model);
        }

        [HttpPost]
        public ActionResult UploadExcelBalanceSheet(ExcelFileAOC4UploadVM model)
        {
            //import Excel
            try
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(model.File.InputStream))
                {
                    var strVal = "";
                    decimal actualVal = 0;
                    DateTime endPeriodDate = new DateTime();
                    int rowIndex = 4;

                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["BalanceSheet"];
                    int xlrow2 = excelSheet.Dimension.End.Row;

                    List<AOC4_BalanceSheetVM> objList = new List<AOC4_BalanceSheetVM>();
                    var objCurrentPeriodData = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = model.ExcelAOC4Id, ReportingPeriod = "C" };
                    var objPreviousPeriodData = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = model.ExcelAOC4Id, ReportingPeriod = "P" };

                    #region Current Period Data
                    rowIndex = 3;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!DateTime.TryParse(strVal, out endPeriodDate))
                        {
                            objCurrentPeriodData.EndDate = null;
                        }
                        else
                        {
                            objCurrentPeriodData.EndDate = endPeriodDate;
                        }
                    }

                    rowIndex = 6;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.Capital = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.Reserve = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.MoneyRecived = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.MoneyPending = actualVal;

                    rowIndex = 11;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LongTerm = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.Deferred = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.OtherLongTerm = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LongTermProvision = actualVal;

                    rowIndex = 16;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ShortTerm = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TradePayables = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.OtherCL = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ShortTermProvision = actualVal;

                    //rowIndex = ;
                    //actualVal = 0;
                    //strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    //if (!string.IsNullOrEmpty(strVal))
                    //{
                    //    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                    //    {
                    //        actualVal = 0;
                    //    }
                    //}
                    //objCurrentPeriodData.Total_I = actualVal;

                    rowIndex = 24;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TangibleA = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.IntangibleA = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.CapitalWIP = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.IntangibleA_UD = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.NonCI = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.DeferredTA = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.Lt_LoanAndAdv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.OtherNonCA = actualVal;

                    rowIndex = 33;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.CurrentInvestment = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }

                    objCurrentPeriodData.Invetories = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TradeReceivable = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.CashEquivalents = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ShortTermLoan = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.OtherCurrentAsset = actualVal;

                    //rowIndex++;
                    //actualVal = 0;
                    //strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    //if (!string.IsNullOrEmpty(strVal))
                    //{
                    //    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                    //    {
                    //        actualVal = 0;
                    //    }
                    //}
                    //objCurrentPeriodData.Total_II = actualVal;

                    #endregion

                    #region Previous Period Data
                    rowIndex = 3;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!DateTime.TryParse(strVal, out endPeriodDate))
                        {
                            objPreviousPeriodData.EndDate = null;
                        }
                        else
                        {
                            objPreviousPeriodData.EndDate = endPeriodDate;
                        }
                    }

                    rowIndex = 6;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.Capital = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.Reserve = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.MoneyRecived = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.MoneyPending = actualVal;

                    rowIndex = 11;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LongTerm = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.Deferred = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.OtherLongTerm = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LongTermProvision = actualVal;

                    rowIndex = 16;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ShortTerm = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TradePayables = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.OtherCL = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ShortTermProvision = actualVal;

                    //rowIndex = ;
                    //actualVal = 0;
                    //strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    //if (!string.IsNullOrEmpty(strVal))
                    //{
                    //    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                    //    {
                    //        actualVal = 0;
                    //    }
                    //}
                    //objPreviousPeriodData.Total_I = actualVal;

                    rowIndex = 24;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TangibleA = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.IntangibleA = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.CapitalWIP = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.IntangibleA_UD = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.NonCI = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.DeferredTA = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.Lt_LoanAndAdv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.OtherNonCA = actualVal;

                    rowIndex = 33;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.CurrentInvestment = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }

                    objPreviousPeriodData.Invetories = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TradeReceivable = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.CashEquivalents = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ShortTermLoan = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.OtherCurrentAsset = actualVal;

                    //rowIndex++;
                    //actualVal = 0;
                    //strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    //if (!string.IsNullOrEmpty(strVal))
                    //{
                    //    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                    //    {
                    //        actualVal = 0;
                    //    }
                    //}
                    //objPreviousPeriodData.Total_II = actualVal;

                    #endregion

                    int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    objList.Add(objCurrentPeriodData);
                    objList.Add(objPreviousPeriodData);

                    var r = objIAOC4Service.SaveBalanceSheet(objList, userID);
                    model.Success = r.Success;
                    model.Error = r.Error;
                    model.Message = r.Message;
                }
            }
            catch (Exception ex)
            {
                model.Error = true;
                model.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
            }
            return PartialView("_BalanceSheet", model);
        }

        public ActionResult PreviewBalanceSheet(long id, int entityId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetBalanceSheet(id, customerID);
            return PartialView("_BalanceSheetPreview", model);
        }
        #endregion

        #region Detailed Balance sheet
        public ActionResult DetailedBalanceSheet(long id, int entityId)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_DetailedBalanceSheet", model);
        }

        [HttpPost]
        public ActionResult UploadExcelDetailedBalanceSheet(ExcelFileAOC4UploadVM model)
        {
            //import Excel
            try
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(model.File.InputStream))
                {
                    var strVal = "";
                    decimal actualVal = 0;
                    //DateTime endPeriodDate = new DateTime();
                    int rowIndex = 4;

                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["BalanceSheet"];
                    int xlrow2 = excelSheet.Dimension.End.Row;

                    List<AOC4_DetailedBalanceSheetVM> objList = new List<AOC4_DetailedBalanceSheetVM>();
                    var objCurrentPeriodData = new AOC4_DetailedBalanceSheetVM() { BalanceSheetAOC4Id = model.ExcelAOC4Id, LT_ReportingPeriod = "C" };
                    var objPreviousPeriodData = new AOC4_DetailedBalanceSheetVM() { BalanceSheetAOC4Id = model.ExcelAOC4Id, LT_ReportingPeriod = "P" };

                    #region Current Period Data   

                    //Section A 
                    rowIndex = 4;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Bonds = actualVal;

                    rowIndex = 6;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_LoanBank = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_LoanOtherParties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Deferred_pay_liabilities = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_maturities = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Otherloans_advances = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Tot_borrowings = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LT_Aggregate_Amt = actualVal;

                    // Section B 

                    rowIndex = 19;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_LoanBank = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_LoanOtherParties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_Loans_advances_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_Tot_borrowings = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.ST_Aggregate_Amt = actualVal;

                    // Section C

                    rowIndex = 29;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_Capital_adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_Security_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_Otherloans_advances = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }

                    objCurrentPeriodData.LTA_TotLoan_Adv = actualVal;

                    rowIndex = 35;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_FromRelated_Parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_FromOthers = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_Net_loan_Adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTA_Loans_Adv_dueByDir = actualVal;

                    // Section D

                    rowIndex = 42;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_Capital_adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_Security_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_Otherloans_advances = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_TotLoan_Adv = actualVal;

                    rowIndex = 48;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_FromRelated_Parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_FromOthers = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_Net_loan_Adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.LTAD_Loans_Adv_dueByDir = actualVal;

                    // Section E
                    rowIndex = 56;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Secured_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Secured_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Unsecured_ESM = actualVal;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Unsecured_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Doubtful_ESM = actualVal;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Doubtful_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_TotalTrade_ESM = actualVal;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_TotalTrade_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Provision_ESM = actualVal;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_Provision_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_NetTrade_ESM = actualVal;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_NetTrade_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_DebtDue_ESM = actualVal;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TR_DebtDue_WSM = actualVal;

                    #endregion

                    #region Previous Period Data

                    //Section A 
                    rowIndex = 4;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Bonds = actualVal;

                    rowIndex = 6;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_LoanBank = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_LoanOtherParties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Deferred_pay_liabilities = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_maturities = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Otherloans_advances = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Tot_borrowings = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LT_Aggregate_Amt = actualVal;

                    // Section B 

                    rowIndex = 19;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_LoanBank = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_LoanOtherParties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_Loans_advances_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_Tot_borrowings = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.ST_Aggregate_Amt = actualVal;

                    // Section C

                    rowIndex = 29;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_Capital_adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_Security_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_Otherloans_advances = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }

                    objPreviousPeriodData.LTA_TotLoan_Adv = actualVal;

                    rowIndex = 35;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_FromRelated_Parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_FromOthers = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_Net_loan_Adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTA_Loans_Adv_dueByDir = actualVal;

                    // Section D

                    rowIndex = 42;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_Capital_adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_Security_Deposits = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_Loans_adv_related_parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_Otherloans_advances = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_TotLoan_Adv = actualVal;

                    rowIndex = 48;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_FromRelated_Parties = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_FromOthers = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_Net_loan_Adv = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.LTAD_Loans_Adv_dueByDir = actualVal;

                    // Section E

                    rowIndex = 56;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Secured_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Secured_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Unsecured_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Unsecured_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Doubtful_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Doubtful_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_TotalTrade_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_TotalTrade_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Provision_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_Provision_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_NetTrade_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_NetTrade_WSM = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_DebtDue_ESM = actualVal;

                    strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TR_DebtDue_WSM = actualVal;



                    #endregion

                    int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    objList.Add(objCurrentPeriodData);
                    objList.Add(objPreviousPeriodData);

                    var r = objIAOC4Service.SaveDetailedBalanceSheet(objList, userID);
                    model.Success = r.Success;
                    model.Error = r.Error;
                    model.Message = r.Message;
                }
            }
            catch (Exception ex)
            {
                model.Error = true;
                model.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
            }
            return PartialView("_DetailedBalanceSheet", model);
        }

        public ActionResult PreviewDetailedBalanceSheet(long id, int entityId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetDetailedBalanceSheet(id, customerID);
            return PartialView("_DetailedBalanceSheetPreview", model);
        }
        #endregion

        #region Financial Parameters
        public ActionResult FinancialParameters(long id, int entityId)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_FinancialParameters", model);
        }

        [HttpPost]
        public ActionResult UploadExcelFinancialParameters(ExcelFileAOC4UploadVM _objaocExcel)
        {
            if (_objaocExcel != null)
            {
                if (_objaocExcel.File != null)
                {
                    try
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(_objaocExcel.File.InputStream))
                        {
                            var strVal = "";
                            decimal actualVal = 0;
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["AOC-4"];
                            int xlrow2 = excelSheet.Dimension.End.Row;
                            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                            var objAOC4ExcelVM = new AOC4_FinancialParameterVM() { FinancialParameterAOC4Id = _objaocExcel.ExcelAOC4Id };

                            #region  Financial parameter Data

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D4")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Amt_Of_issue_Alloted = actualVal;


                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D5")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Share_app_Money_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D6")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Share_app_Money_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D7")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Share_app_Money_3 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D8")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Share_app_Money_4 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D9")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Paid_up_capital_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D10")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Paid_up_capital_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D11")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.NoOfShares = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D12")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Deposit_Accepted = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D13")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Deposit_Matured_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D14")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Deposit_Matured_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D15")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Deposit_Matured_3 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D16")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Unclaimed_Debentures = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D17")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Claimed_Debentures = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D18")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.InterestOnDepositAccrued = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D19")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Unpaid_divedend = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D20")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.InvestmentInCompany_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D21")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.InvestmentInCompany_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D22")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Capital_Reserves = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D23")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Amt_TransferforIEPF = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D24")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Inter_Corp_Deposit = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D25")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.GrossValueOfTransaction = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D26")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.CapitalSubsidies = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D27")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.CallsUnpaidByDirector = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D28")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.CallsUnpaidByOthers = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D29")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Forfeited_Shares_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D30")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Forfeited_Shares_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D31")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Borrowing_foreign_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D32")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Borrowing_foreign_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D33")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.InterCorporate_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D34")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.InterCorporate_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D35")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.CommercialPaper = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D36")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.ConversionOfWarrant_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D37")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.ConversionOfWarrant_2 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D38")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.ConversionOfWarrant_3 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D39")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.WarrantsIssueInForeignCurrency = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D40")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.WarrantsIssueInRupees = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D41")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.DefaultInPayment_1 = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D42")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.DefaultInPayment_2 = actualVal;

                            var option = false;
                            if (!string.IsNullOrEmpty(excelSheet.Cells["D43"].Text.Trim()))
                            {
                                if (excelSheet.Cells["D43"].Text.Trim().ToUpper() == "YES")
                                {
                                    option = true;
                                }
                            }
                            objAOC4ExcelVM.WheatheOperatingLease = option;

                            objAOC4ExcelVM.ProvideDetailsOfConversion = Convert.ToString(excelSheet.Cells[("C45")].Text);

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D46")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.NetWorthOfComp = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D47")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.NoOfShareHolders = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D48")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.SecuredLoan = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D49")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.GrossFixedAssets = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D50")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Depreciation_Amortization = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D51")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.Misc_Expenditure = actualVal;

                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D52")].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objAOC4ExcelVM.UnhedgedForeign = actualVal;
                            #endregion

                            var r = objIAOC4Service.SaveFinancialParameter(objAOC4ExcelVM, userID);
                            _objaocExcel.Success = r.Success;
                            _objaocExcel.Error = r.Error;
                            _objaocExcel.Message = r.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        _objaocExcel.Error = true;
                        _objaocExcel.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                _objaocExcel = new ExcelFileAOC4UploadVM();
                _objaocExcel.Error = true;
            }
            return PartialView("_FinancialParameters", _objaocExcel);
        }

        public ActionResult PreviewFinancialParameters(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetFinancialParameteritems(id, customerID);
            return PartialView("_FinancialParametersPreview", model);
        }
        #endregion

        #region Share Capital Raised Sheet
        public ActionResult ShareCapitalRaised(long id)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_ShareCapitalRaise", model);
        }
        [HttpPost]
        public ActionResult UploadExcelSharedCapitalRaise(ExcelFileAOC4UploadVM _objcapitalraise)
        {
            if (_objcapitalraise != null)
            {
                if (_objcapitalraise.File != null)
                {
                    try
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(_objcapitalraise.File.InputStream))
                        {
                            var strVal = "";
                            decimal actualVal = 0;
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["ShareCapitalRaise"];
                            int xlrow2 = excelSheet.Dimension.End.Row;
                            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                            int rowIndex = 6;

                            var objShareCapitalVM = new ShareCapitalRaisedVM() { ShareCapitalAOC4Id = _objcapitalraise.ExcelAOC4Id };

                            #region  Shared Capital Raised Data

                            rowIndex = 6;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PublicIssue_E = actualVal;

                            rowIndex = 6;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PublicIssue_P = actualVal;

                            rowIndex = 6;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PublicIssue_T = actualVal;

                            rowIndex = 6;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.BonusIssue_E = actualVal;

                            rowIndex = 7;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.BonusIssue_P = actualVal;

                            rowIndex = 7;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.BonusIssue_T = actualVal;

                            rowIndex = 8;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.RightIssue_E = actualVal;

                            rowIndex = 8;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.RightIssue_P = actualVal;

                            rowIndex = 8;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.RightIssue_T = actualVal;

                            rowIndex = 9;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PvtPlacementarising_E = actualVal;

                            rowIndex = 9;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PvtPlacementarising_P = actualVal;

                            rowIndex = 9;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PvtPlacementarising_T = actualVal;

                            rowIndex = 10;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.OtherPvtPlacement_E = actualVal;

                            rowIndex = 10;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.OtherPvtPlacement_P = actualVal;

                            rowIndex = 10;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.OtherPvtPlacement_T = actualVal;

                            rowIndex = 11;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PrferentialAllotment_E = actualVal;

                            rowIndex = 11;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PrferentialAllotment_P = actualVal;

                            rowIndex = 11;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.PrferentialAllotment_T = actualVal;

                            rowIndex = 12;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.OtherPreferentialallotment_E = actualVal;

                            rowIndex = 12;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.OtherPreferentialallotment_P = actualVal;

                            rowIndex = 12;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.OtherPreferentialallotment_T = actualVal;

                            rowIndex = 13;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.EmpStockOptionPlan_E = actualVal;

                            rowIndex = 13;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }

                            objShareCapitalVM.EmpStockOptionPlan_P = actualVal;

                            rowIndex = 13;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.EmpStockOptionPlan_T = actualVal;

                            rowIndex = 14;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.Others_E = actualVal;

                            rowIndex = 14;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.Others_P = actualVal;

                            rowIndex = 14;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.Others_T = actualVal;

                            rowIndex = 15;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.TotalAmtShares_E = actualVal;

                            rowIndex = 15;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.TotalAmtShares_P = actualVal;

                            rowIndex = 15;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objShareCapitalVM.TotalAmtShares_T = actualVal;

                            #endregion
                            var r = objIAOC4Service.SaveSharedCapitalRaised(objShareCapitalVM, userID);
                            _objcapitalraise.Success = r.Success;
                            _objcapitalraise.Error = r.Error;
                            _objcapitalraise.Message = r.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        _objcapitalraise.Error = true;
                        _objcapitalraise.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                _objcapitalraise = new ExcelFileAOC4UploadVM();
                _objcapitalraise.Error = true;
            }
            return PartialView("_ShareCapitalRaise", _objcapitalraise);
        }
        public ActionResult PreviewShareCapitalRaise(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetSharedCapitalitems(id, customerID);
            return PartialView("_ShareCapitalRaisePreview", model);
        }
        #endregion

        #region Cost Records and Cost Audit
        public ActionResult DetailsOfCostRecords(long id)
        {
            var model = objIAOC4Service.GetDetailsOfCostRecords(id);
            return PartialView("_DetailsOfCostRecords", model);
        }

        [HttpPost]
        public ActionResult SaveDetailsOfCostRecords(DetailsOfCostRecordsVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (ModelState.IsValid)
            {
                model = objIAOC4Service.SaveDetailsOfCostRecords(model, userID);
            }
            return PartialView("_DetailsOfCostRecords", model);
        }

        public ActionResult PreviewDetailsOfCostRecords(long id)
        {
            var model = objIAOC4Service.GetDetailsOfCostRecords(id);
            return PartialView("_DetailsOfCostRecordsPreview", model);
        }
        #endregion

        #region Profit & Loss
        public ActionResult ProfitLoss(long id, int entityId)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_ProfitLoss", model);
        }

        [HttpPost]
        public ActionResult UploadExcelProfitLoss(ExcelFileAOC4UploadVM model)
        {
            //import Excel
            try
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(model.File.InputStream))
                {
                    var strVal = "";
                    decimal actualVal = 0;
                    DateTime fromDate = new DateTime();
                    DateTime toDate = new DateTime();
                    int rowIndex = 3;

                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["ProfitLoss"];
                    int xlrow2 = excelSheet.Dimension.End.Row;

                    List<AOC4_ProfitLossVM> objList = new List<AOC4_ProfitLossVM>();
                    var objCurrentPeriodData = new AOC4_ProfitLossVM() { Aoc4Id = model.ExcelAOC4Id, ReportingPeriod = "C" };
                    var objPreviousPeriodData = new AOC4_ProfitLossVM() { Aoc4Id = model.ExcelAOC4Id, ReportingPeriod = "P" };

                    #region Current Period Data
                    rowIndex = 3;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!DateTime.TryParse(strVal, out fromDate))
                        {
                            objCurrentPeriodData.FROM_DATE_CR = null;
                        }
                        else
                        {
                            objCurrentPeriodData.FROM_DATE_CR = fromDate;
                        }
                    }

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!DateTime.TryParse(strVal, out toDate))
                        {
                            objCurrentPeriodData.TO_DATE_CR = null;
                        }
                        else
                        {
                            objCurrentPeriodData.TO_DATE_CR = toDate;
                        }
                    }

                    rowIndex = 7;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.SALES_GOODS_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.SALES_GOODS_T_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.SALES_SUPPLY_CR = actualVal;

                    rowIndex = 11;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.SALES_GOODS1_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.SALE_GOODS_T1_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.SALES_SUPPLY1_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.OTHER_INCOME_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TOTAL_REVENUE_CR = actualVal;

                    rowIndex = 17;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.COST_MATERIAL_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PURCHASE_STOCK_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.FINISHED_GOODS_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.WORK_IN_PROG_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.STOCK_IN_TRADE_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.EMP_BENEFIT_EX_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.MANGERIAL_REM_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PAYMENT_AUDTRS_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.INSURANCE_EXP_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.POWER_FUEL_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.FINANCE_COST_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.DEPRECTN_AMORT_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.OTHER_EXPENSES_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }

                    objCurrentPeriodData.TOTAL_EXPENSES_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROFIT_BEFORE_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.EXCEPTIONL_ITM_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROFIT_BEF_TAX_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.EXTRAORDINARY_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROF_B_TAX_7_8_C = actualVal;

                    rowIndex = 37;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.CURRENT_TAX_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.DEFERRED_TAX_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROF_LOSS_OPER_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROF_LOSS_DO_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.TAX_EXPNS_DIS_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROF_LOS_12_13_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.PROF_LOS_11_14_C = actualVal;

                    rowIndex = 45;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.BASIC_BEFR_EI_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.DILUTED_BEF_EI_C = actualVal;

                    rowIndex = 48;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.BASIC_AFTR_EI_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objCurrentPeriodData.DILUTED_AFT_EI_C = actualVal;

                    #endregion

                    #region Previous Period Data

                    rowIndex = 3;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!DateTime.TryParse(strVal, out fromDate))
                        {
                            objPreviousPeriodData.FROM_DATE_CR = null;
                        }
                        else
                        {
                            objPreviousPeriodData.FROM_DATE_CR = fromDate;
                        }
                    }

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!DateTime.TryParse(strVal, out toDate))
                        {
                            objPreviousPeriodData.TO_DATE_CR = null;
                        }
                        else
                        {
                            objPreviousPeriodData.TO_DATE_CR = toDate;
                        }
                    }

                    rowIndex = 7;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.SALES_GOODS_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.SALES_GOODS_T_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.SALES_SUPPLY_CR = actualVal;

                    rowIndex = 11;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.SALES_GOODS1_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.SALE_GOODS_T1_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.SALES_SUPPLY1_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.OTHER_INCOME_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TOTAL_REVENUE_CR = actualVal;

                    rowIndex = 17;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.COST_MATERIAL_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PURCHASE_STOCK_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.FINISHED_GOODS_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.WORK_IN_PROG_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.STOCK_IN_TRADE_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.EMP_BENEFIT_EX_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.MANGERIAL_REM_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PAYMENT_AUDTRS_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.INSURANCE_EXP_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.POWER_FUEL_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.FINANCE_COST_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.DEPRECTN_AMORT_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.OTHER_EXPENSES_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }

                    objPreviousPeriodData.TOTAL_EXPENSES_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROFIT_BEFORE_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.EXCEPTIONL_ITM_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROFIT_BEF_TAX_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.EXTRAORDINARY_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROF_B_TAX_7_8_C = actualVal;

                    rowIndex = 37;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.CURRENT_TAX_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.DEFERRED_TAX_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROF_LOSS_OPER_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROF_LOSS_DO_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.TAX_EXPNS_DIS_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROF_LOS_12_13_C = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.PROF_LOS_11_14_C = actualVal;

                    rowIndex = 45;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.BASIC_BEFR_EI_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.DILUTED_BEF_EI_C = actualVal;

                    rowIndex = 48;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.BASIC_AFTR_EI_CR = actualVal;

                    rowIndex++;
                    actualVal = 0;
                    strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                    if (!string.IsNullOrEmpty(strVal))
                    {
                        if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                        {
                            actualVal = 0;
                        }
                    }
                    objPreviousPeriodData.DILUTED_AFT_EI_C = actualVal;


                    #endregion

                    int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    objList.Add(objCurrentPeriodData);
                    objList.Add(objPreviousPeriodData);

                    var r = objIAOC4Service.SaveProfitLoss(objList, userID);
                    model.Success = r.Success;
                    model.Error = r.Error;
                    model.Message = r.Message;
                }
            }
            catch (Exception ex)
            {
                model.Error = true;
                model.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
            }
            return PartialView("_ProfitLoss", model);
        }

        public ActionResult PreviewProfitLoss(long id, int entityId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetProfitLoss(id, customerID);
            return PartialView("_ProfitLossPreview", model);
        }
        #endregion

        #region Detailed Profit and Loss items 
        public ActionResult DetailsOfPandLitems(long id)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_DetailedProfitAndLossItems", model);
        }

        [HttpPost]
        public ActionResult UploadExcelDetailedProfitLoss(ExcelFileAOC4UploadVM _objpl)
        {
            if (_objpl != null)
            {
                if (_objpl.File != null)
                {
                    try
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(_objpl.File.InputStream))
                        {
                            var strVal = "";
                            decimal actualVal = 0;
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["DetailedProfitLoss"];
                            int xlrow2 = excelSheet.Dimension.End.Row;
                            int rowIndex = 8;

                            List<DetailedProfitLossVM> objList = new List<DetailedProfitLossVM>();
                            var objCurrentPeriodData = new DetailedProfitLossVM() { DetailedOfPLAOC4Id = _objpl.ExcelAOC4Id, ReportingPeriod = "C" };
                            var objPreviousPeriodData = new DetailedProfitLossVM() { DetailedOfPLAOC4Id = _objpl.ExcelAOC4Id, ReportingPeriod = "P" };

                            #region Current Period Data

                            rowIndex = 8;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.EXP_GOODS_FOB_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.INTEREST_DIVD_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.ROYALTY_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.KNOW_HOW_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.PROF_CONS_FEE_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.OTHR_INCOME_E_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.TOTAL_EARNG_FE_C = actualVal;

                            rowIndex = 20;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.RAW_MATERIAL_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.COMPONENT_SP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.CAPITAL_GOODS_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.ROYALTY_EXP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.KNOW_HOW_EXP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.PROF_CON_FEE_E_C = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.INTEREST_EXP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.OTHER_MATTERS_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.DIVIDEND_PAID_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objCurrentPeriodData.TOT_EXP_FE_CR = actualVal;
                            #endregion

                            #region Previous Data

                            rowIndex = 8;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.EXP_GOODS_FOB_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.INTEREST_DIVD_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.ROYALTY_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.KNOW_HOW_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.PROF_CONS_FEE_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.OTHR_INCOME_E_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.TOTAL_EARNG_FE_C = actualVal;

                            rowIndex = 20;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.RAW_MATERIAL_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.COMPONENT_SP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.CAPITAL_GOODS_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.ROYALTY_EXP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.KNOW_HOW_EXP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.PROF_CON_FEE_E_C = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.INTEREST_EXP_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.OTHER_MATTERS_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.DIVIDEND_PAID_CR = actualVal;

                            rowIndex++;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }
                            }
                            objPreviousPeriodData.TOT_EXP_FE_CR = actualVal;

                            #endregion

                            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                            objList.Add(objCurrentPeriodData);
                            objList.Add(objPreviousPeriodData);

                            var r = objIAOC4Service.SaveDetailedProfitLoss(objList, userID);
                            _objpl.Success = r.Success;
                            _objpl.Error = r.Error;
                            _objpl.Message = r.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        _objpl.Error = true;
                        _objpl.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                _objpl = new ExcelFileAOC4UploadVM();
                _objpl.Error = true;
            }
            return PartialView("_DetailedProfitAndLossItems", _objpl);
        }

        public ActionResult PreviewDetailsOfPandLitems(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetDetailedProfitLossitems(id, customerID);
            return PartialView("_DetailedProfitAndLossPreview", model);
        }
        #endregion

        #region Financial parameters - Profit and loss account items
        public ActionResult FinancialParameters_PL(long id)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_FinancialParameters_PL", model);
        }

        [HttpPost]
        public ActionResult UploadExcelFinancialParameters_PL(ExcelFileAOC4UploadVM _objFPL)
        {
            if (_objFPL != null)
            {
                if (_objFPL.File != null)
                {
                    try
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(_objFPL.File.InputStream))
                        {
                            var strVal = "";
                            decimal actualVal = 0;
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["FinancialParameters_PL"];
                            int xlrow2 = excelSheet.Dimension.End.Row;
                            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                            int rowIndex = 6;

                            var objFPLVM = new FinancialParameter_PLVM() { FinancialParameterPLAOC4Id = _objFPL.ExcelAOC4Id };

                            #region  Financial parameter Profit and Loss Data
                            rowIndex = 6;
                            actualVal = 0;
                            strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                            if (!string.IsNullOrEmpty(strVal))
                            {
                                if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                {
                                    actualVal = 0;
                                }

                                objFPLVM.PROPOSED_DIVIDND = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.BASIC_EARNING_PS = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.DILUTED_EARN_PS = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.INCOME_FOREIGN = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.EXPENDIT_FOREIGN = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.REVENUE_SUBSIDIE = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.RENT_PAID = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.CONSUMPTION_STOR = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.GROSS_VALUE_TRAN = actualVal;

                                rowIndex++;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.BAD_DEBTS_RP = actualVal;

                                rowIndex = 6;
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objFPLVM.PROPOSED_DIV_PER = actualVal;

                                #endregion

                                var r = objIAOC4Service.SaveFinancialParameter_PL(objFPLVM, userID);
                                _objFPL.Success = r.Success;
                                _objFPL.Error = r.Error;
                                _objFPL.Message = r.Message;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _objFPL.Error = true;
                        _objFPL.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                _objFPL = new ExcelFileAOC4UploadVM();
                _objFPL.Error = true;
            }
            return PartialView("_FinancialParameters_PL", _objFPL);
        }

        public ActionResult PreviewFinancialParameters_PL(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetFinancialParameter_PLDefaultData(id, customerID);
            return PartialView("_PreviewFinancialParameters_PL", model);
        }
        #endregion

        #region Download Sample
        public ActionResult SampleExcelAOC4BalanceSheet(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "AOC4_BalanceSheet.xlsx");
            Stream stream = new MemoryStream(fileBytes);

            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var objBalanceSheet = objIAOC4Service.GetBalanceSheetDefaultDataForExcel(id, customerID);
            if (objBalanceSheet != null)
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    int rowIndex = 4;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["BalanceSheet"];

                    #region Current Period
                    if (objBalanceSheet.Current != null)
                    {
                        var strValue = string.Empty;
                        rowIndex = 3;
                        if(objBalanceSheet.Current.EndDate != null)
                        {
                            excelSheet.Cells[("C" + rowIndex)].Value = Convert.ToDateTime(objBalanceSheet.Current.EndDate).ToString("dd-MMM-yyyy");
                        }

                        rowIndex = 6;
                        strValue = Convert.ToString(objBalanceSheet.Current.Capital);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.Reserve);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.MoneyRecived);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.MoneyPending);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 11;
                        strValue = Convert.ToString(objBalanceSheet.Current.LongTerm);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.Deferred);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.OtherLongTerm);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.LongTermProvision);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 16;
                        strValue = Convert.ToString(objBalanceSheet.Current.ShortTerm);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.TradePayables);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.OtherCL);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.ShortTermProvision);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 24;
                        strValue = Convert.ToString(objBalanceSheet.Current.TangibleA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.IntangibleA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.CapitalWIP);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.IntangibleA_UD);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.NonCI);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.DeferredTA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.Lt_LoanAndAdv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.OtherNonCA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 33;
                        strValue = Convert.ToString(objBalanceSheet.Current.CurrentInvestment);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.Invetories);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.TradeReceivable);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.CashEquivalents);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.ShortTermLoan);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Current.OtherCurrentAsset);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;
                    }
                    #endregion

                    #region Previous Period
                    if (objBalanceSheet.Previous != null)
                    {
                        var strValue = string.Empty;
                        rowIndex = 3;
                        if (objBalanceSheet.Previous.EndDate != null)
                        {
                            excelSheet.Cells[("D" + rowIndex)].Value = Convert.ToDateTime(objBalanceSheet.Previous.EndDate).ToString("dd-MMM-yyyy");
                        }

                        rowIndex = 6;
                        strValue = Convert.ToString(objBalanceSheet.Previous.Capital);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.Reserve);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.MoneyRecived);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.MoneyPending);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 11;
                        strValue = Convert.ToString(objBalanceSheet.Previous.LongTerm);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.Deferred);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.OtherLongTerm);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.LongTermProvision);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 16;
                        strValue = Convert.ToString(objBalanceSheet.Previous.ShortTerm);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.TradePayables);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.OtherCL);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.ShortTermProvision);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 24;
                        strValue = Convert.ToString(objBalanceSheet.Previous.TangibleA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.IntangibleA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.CapitalWIP);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.IntangibleA_UD);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.NonCI);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.DeferredTA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.Lt_LoanAndAdv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.OtherNonCA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex= 33;
                        strValue = Convert.ToString(objBalanceSheet.Previous.CurrentInvestment);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.Invetories);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.TradeReceivable);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.CashEquivalents);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.ShortTermLoan);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objBalanceSheet.Previous.OtherCurrentAsset);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;
                    }
                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "AOC-4 Balance Sheet.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelAOC4DetailedBalanceSheet(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "AOC4-BalanceSheetDetailsSample.xlsx");
            Stream stream = new MemoryStream(fileBytes);

            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var objDetailedBalanceSheet = objIAOC4Service.GetDetailedBalanceSheetDefaultDataForExcel(id, customerID);
            if (objDetailedBalanceSheet != null)
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    int rowIndex = 4;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["BalanceSheet"];

                    #region Current Period
                    if (objDetailedBalanceSheet.Current != null)
                    {
                        //Section A
                        var strValue = string.Empty;
                        rowIndex = 4;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Bonds);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 6;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_LoanBank);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_LoanOtherParties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Deferred_pay_liabilities);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_maturities);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Otherloans_advances);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Tot_borrowings);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Aggregate_Amt);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        //Section B
                        rowIndex = 19;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_LoanBank);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_LoanOtherParties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Loans_advances_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Tot_borrowings);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Aggregate_Amt);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        //Section C

                        rowIndex = 29;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Capital_adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Security_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Otherloans_advances);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_TotLoan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 35;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_FromRelated_Parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_FromOthers);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Net_loan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Loans_Adv_dueByDir);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        //Section D
                        rowIndex = 42;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Capital_adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Security_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Otherloans_advances);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_TotLoan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 48;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_FromRelated_Parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_FromOthers);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Net_loan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Loans_Adv_dueByDir);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        //Section E 
                        rowIndex = 56;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Secured_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Secured_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Unsecured_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Unsecured_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Doubtful_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Doubtful_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_TotalTrade_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_TotalTrade_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Provision_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Provision_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_NetTrade_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_NetTrade_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_DebtDue_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_DebtDue_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;
                    }
                    #endregion

                    #region Previous Period
                    if (objDetailedBalanceSheet.Previous != null)
                    {
                        //Section A
                        var strValue = string.Empty;
                        rowIndex = 4;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Bonds);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 6;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_LoanBank);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_LoanOtherParties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Deferred_pay_liabilities);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_maturities);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Otherloans_advances);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Tot_borrowings);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Aggregate_Amt);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        //Section B
                        rowIndex = 19;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_LoanBank);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_LoanOtherParties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Loans_advances_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Tot_borrowings);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Aggregate_Amt);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        //Section C

                        rowIndex = 29;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Capital_adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Security_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Otherloans_advances);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_TotLoan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 35;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_FromRelated_Parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_FromOthers);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Net_loan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Loans_Adv_dueByDir);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        //Section D
                        rowIndex = 42;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Capital_adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Security_Deposits);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Loans_adv_related_parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Otherloans_advances);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_TotLoan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 48;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_FromRelated_Parties);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_FromOthers);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Net_loan_Adv);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Loans_Adv_dueByDir);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        //Section E 
                        rowIndex = 56;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Secured_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Secured_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Unsecured_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Unsecured_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Doubtful_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Doubtful_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;


                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_TotalTrade_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_TotalTrade_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Provision_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Provision_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_NetTrade_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_NetTrade_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_DebtDue_ESM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_DebtDue_WSM);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;
                    }
                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "AOC4-BalanceSheetDetailsSample.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelFinancialParameters(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "ExcelAOC4_FinancialParameter.xlsx");
            Stream stream = new MemoryStream(fileBytes);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var objAOC4ExcelVM = objIAOC4Service.GetFinancialParameteritems(id, customerID);
            if (objAOC4ExcelVM != null)
            {
                #region  Financial parameter Data   
                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    var strVal = "";
                    //decimal actualVal = 0;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["AOC-4"];
                    int xlrow2 = excelSheet.Dimension.End.Row;
                    int userID = Convert.ToInt32(AuthenticationHelper.UserID);


                    strVal = Convert.ToString(objAOC4ExcelVM.Amt_Of_issue_Alloted);
                    excelSheet.Cells[("D4")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Share_app_Money_1);
                    excelSheet.Cells[("D5")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Share_app_Money_2);
                    excelSheet.Cells[("D6")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Share_app_Money_3);
                    excelSheet.Cells[("D7")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Share_app_Money_4);
                    excelSheet.Cells[("D8")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Paid_up_capital_1);
                    excelSheet.Cells[("D9")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Paid_up_capital_2);
                    excelSheet.Cells[("D10")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.NoOfShares);
                    excelSheet.Cells[("D11")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Deposit_Accepted);
                    excelSheet.Cells[("D12")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Deposit_Matured_1);
                    excelSheet.Cells[("D13")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Deposit_Matured_2);
                    excelSheet.Cells[("D14")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Deposit_Matured_3);
                    excelSheet.Cells[("D15")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Unclaimed_Debentures);
                    excelSheet.Cells[("D16")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Claimed_Debentures);
                    excelSheet.Cells[("D17")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.InterestOnDepositAccrued);
                    excelSheet.Cells[("D18")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Unpaid_divedend);
                    excelSheet.Cells[("D19")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.InvestmentInCompany_1);
                    excelSheet.Cells[("D20")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.InvestmentInCompany_2);
                    excelSheet.Cells[("D21")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Capital_Reserves);
                    excelSheet.Cells[("D22")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Amt_TransferforIEPF);
                    excelSheet.Cells[("D23")].Value = strVal;


                    strVal = Convert.ToString(objAOC4ExcelVM.Inter_Corp_Deposit);
                    excelSheet.Cells[("D24")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.GrossValueOfTransaction);
                    excelSheet.Cells[("D25")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.CapitalSubsidies);
                    excelSheet.Cells[("D26")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.CallsUnpaidByDirector);
                    excelSheet.Cells[("D27")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.CallsUnpaidByOthers);
                    excelSheet.Cells[("D28")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Forfeited_Shares_1);
                    excelSheet.Cells[("D29")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Forfeited_Shares_2);
                    excelSheet.Cells[("D30")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Borrowing_foreign_1);
                    excelSheet.Cells[("D31")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Borrowing_foreign_2);
                    excelSheet.Cells[("D32")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.InterCorporate_1);
                    excelSheet.Cells[("D33")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.InterCorporate_2);
                    excelSheet.Cells[("D34")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.CommercialPaper);
                    excelSheet.Cells[("D35")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.ConversionOfWarrant_1);
                    excelSheet.Cells[("D36")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.ConversionOfWarrant_2);
                    excelSheet.Cells[("D37")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.ConversionOfWarrant_3);
                    excelSheet.Cells[("D38")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.WarrantsIssueInForeignCurrency);
                    excelSheet.Cells[("D39")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.WarrantsIssueInRupees);
                    excelSheet.Cells[("D40")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.DefaultInPayment_1);
                    excelSheet.Cells[("D41")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.DefaultInPayment_2);
                    excelSheet.Cells[("D42")].Value = strVal;

                    if(objAOC4ExcelVM.WheatheOperatingLease == true)
                    {
                        excelSheet.Cells[("D43")].Value = "Yes";
                    }
                    else if (objAOC4ExcelVM.WheatheOperatingLease == false)
                    {
                        excelSheet.Cells[("D43")].Value = "No";
                    }

                    strVal = Convert.ToString(objAOC4ExcelVM.ProvideDetailsOfConversion);
                    excelSheet.Cells[("C45")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.NetWorthOfComp);
                    excelSheet.Cells[("D46")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.NoOfShareHolders);
                    excelSheet.Cells[("D47")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.SecuredLoan);
                    excelSheet.Cells[("D48")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.GrossFixedAssets);
                    excelSheet.Cells[("D49")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Depreciation_Amortization);
                    excelSheet.Cells[("D50")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.Misc_Expenditure);
                    excelSheet.Cells[("D51")].Value = strVal;

                    strVal = Convert.ToString(objAOC4ExcelVM.UnhedgedForeign);
                    excelSheet.Cells[("D52")].Value = strVal;

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
                #endregion
            }

            string fileName = "ExcelAOC4_FinancialParameter.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelAOC4ShareCapitalRaise(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "ExcelAOC4_SharedCapitalRaise.xlsx");

            Stream stream = new MemoryStream(fileBytes);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var objSharedCapital = objIAOC4Service.GetSharedCapitalitems(id, customerID);
            if (objSharedCapital != null)
            {
                #region  Share Capital Data  
                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    int rowIndex = 6;
                    var strVal = "";
                    //decimal actualVal = 0;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["ShareCapitalRaise"];
                    int xlrow2 = excelSheet.Dimension.End.Row;
                    int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                    strVal = Convert.ToString(objSharedCapital.PublicIssue_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.PublicIssue_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.PublicIssue_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.BonusIssue_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.BonusIssue_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.BonusIssue_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.RightIssue_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.RightIssue_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.RightIssue_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.PvtPlacementarising_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.PvtPlacementarising_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.PvtPlacementarising_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.OtherPvtPlacement_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.OtherPvtPlacement_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.OtherPvtPlacement_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.PrferentialAllotment_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.PrferentialAllotment_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.PrferentialAllotment_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.OtherPreferentialallotment_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.OtherPreferentialallotment_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.OtherPreferentialallotment_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.EmpStockOptionPlan_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.EmpStockOptionPlan_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.EmpStockOptionPlan_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.Others_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.Others_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.Others_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    rowIndex++;
                    strVal = Convert.ToString(objSharedCapital.TotalAmtShares_E);
                    excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.TotalAmtShares_P);
                    excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                    strVal = Convert.ToString(objSharedCapital.TotalAmtShares_T);
                    excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "ExcelAOC4_SharedCapitalRaise.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelAOC4ProfitLoss(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "AOC4_ProfitLoss.xlsx");
            Stream stream = new MemoryStream(fileBytes);

            //var strVal = "";
            //decimal actualVal = 0;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();

            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var objProfitLoss = objIAOC4Service.GetProfitLossDefaultDataForExcel(id, customerID);
            if (objProfitLoss != null)
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    int rowIndex = 3;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["ProfitLoss"];

                    #region Current Period
                    if (objProfitLoss.Current != null)
                    {
                        var strValue = string.Empty;
                        rowIndex = 3;
                        if(objProfitLoss.Current.FROM_DATE_CR != null)
                        {
                            fromDate = Convert.ToDateTime(objProfitLoss.Current.FROM_DATE_CR);
                            strValue = fromDate.ToString("dd-MMM-yyyy");
                            strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                            excelSheet.Cells[("C" + rowIndex)].Value = strValue;
                        }
                        
                        rowIndex++;
                        if(objProfitLoss.Current.TO_DATE_CR != null)
                        {
                            toDate = Convert.ToDateTime(objProfitLoss.Current.TO_DATE_CR);
                            strValue = toDate.ToString("dd-MMM-yyyy");
                            strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                            excelSheet.Cells[("C" + rowIndex)].Value = strValue;
                        }

                        rowIndex = 7;
                        strValue = Convert.ToString(objProfitLoss.Current.SALES_GOODS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.SALES_GOODS_T_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.SALES_SUPPLY_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 11;
                        strValue = Convert.ToString(objProfitLoss.Current.SALES_GOODS1_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.SALE_GOODS_T1_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.SALES_SUPPLY1_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.OTHER_INCOME_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.TOTAL_REVENUE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 17;
                        strValue = Convert.ToString(objProfitLoss.Current.COST_MATERIAL_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PURCHASE_STOCK_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.FINISHED_GOODS_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.WORK_IN_PROG_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.STOCK_IN_TRADE_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.EMP_BENEFIT_EX_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.MANGERIAL_REM_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PAYMENT_AUDTRS_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.INSURANCE_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.POWER_FUEL_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.FINANCE_COST_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.DEPRECTN_AMORT_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.OTHER_EXPENSES_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.TOTAL_EXPENSES_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROFIT_BEFORE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.EXCEPTIONL_ITM_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROFIT_BEF_TAX_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.EXTRAORDINARY_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROF_B_TAX_7_8_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 37;
                        strValue = Convert.ToString(objProfitLoss.Current.CURRENT_TAX_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.DEFERRED_TAX_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROF_LOSS_OPER_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROF_LOSS_DO_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.TAX_EXPNS_DIS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROF_LOS_12_13_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.PROF_LOS_11_14_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 45;
                        strValue = Convert.ToString(objProfitLoss.Current.BASIC_BEFR_EI_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.DILUTED_BEF_EI_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex = 48;
                        strValue = Convert.ToString(objProfitLoss.Current.BASIC_AFTR_EI_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Current.DILUTED_AFT_EI_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;


                    }
                    #endregion

                    #region Previous Period
                    if (objProfitLoss.Previous != null)
                    {
                        var strValue = string.Empty;
                        rowIndex = 3;
                        if(objProfitLoss.Previous.FROM_DATE_CR != null)
                        {
                            fromDate = Convert.ToDateTime(objProfitLoss.Previous.FROM_DATE_CR);
                            strValue = fromDate.ToString("dd-MMM-yyyy");
                            strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                            excelSheet.Cells[("D" + rowIndex)].Value = strValue;
                        }

                        rowIndex++;
                        if (objProfitLoss.Previous.TO_DATE_CR != null)
                        {
                            toDate = Convert.ToDateTime(objProfitLoss.Previous.TO_DATE_CR);
                            strValue = toDate.ToString("dd-MMM-yyyy");
                            strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                            excelSheet.Cells[("D" + rowIndex)].Value = strValue;
                        }

                        rowIndex = 7;
                        strValue = Convert.ToString(objProfitLoss.Previous.SALES_GOODS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.SALES_GOODS_T_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.SALES_SUPPLY_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 11;
                        strValue = Convert.ToString(objProfitLoss.Previous.SALES_GOODS1_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.SALE_GOODS_T1_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.SALES_SUPPLY1_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.OTHER_INCOME_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.TOTAL_REVENUE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 17;
                        strValue = Convert.ToString(objProfitLoss.Previous.COST_MATERIAL_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PURCHASE_STOCK_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.FINISHED_GOODS_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.WORK_IN_PROG_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.STOCK_IN_TRADE_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.EMP_BENEFIT_EX_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.MANGERIAL_REM_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PAYMENT_AUDTRS_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.INSURANCE_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.POWER_FUEL_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.FINANCE_COST_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.DEPRECTN_AMORT_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.OTHER_EXPENSES_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.TOTAL_EXPENSES_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROFIT_BEFORE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.EXCEPTIONL_ITM_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROFIT_BEF_TAX_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.EXTRAORDINARY_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROF_B_TAX_7_8_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 37;
                        strValue = Convert.ToString(objProfitLoss.Previous.CURRENT_TAX_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.DEFERRED_TAX_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROF_LOSS_OPER_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROF_LOSS_DO_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.TAX_EXPNS_DIS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROF_LOS_12_13_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.PROF_LOS_11_14_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 45;
                        strValue = Convert.ToString(objProfitLoss.Previous.BASIC_BEFR_EI_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.DILUTED_BEF_EI_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 48;
                        strValue = Convert.ToString(objProfitLoss.Previous.BASIC_AFTR_EI_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objProfitLoss.Previous.DILUTED_AFT_EI_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;


                    }
                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "AOC4_ProfitLoss.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelAOC4DetailedProfitLoss(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "ExcelAOC4_DetailedProfitLossItems.xlsx");
            Stream stream = new MemoryStream(fileBytes);

            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var objDetailedPL = objIAOC4Service.GetDetailedProfiLossDefaultData(id, customerID);
            if (objDetailedPL != null)
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    int rowIndex = 4;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["DetailedProfitLoss"];

                    #region Current Period
                    if (objDetailedPL.Current != null)
                    {
                        var strValue = string.Empty;
                        rowIndex = 8;
                        strValue = Convert.ToString(objDetailedPL.Current.EXP_GOODS_FOB_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.INTEREST_DIVD_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.ROYALTY_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.KNOW_HOW_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.PROF_CONS_FEE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.OTHR_INCOME_E_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.TOTAL_EARNG_FE_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex = 20;
                        strValue = Convert.ToString(objDetailedPL.Current.RAW_MATERIAL_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.COMPONENT_SP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.CAPITAL_GOODS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.ROYALTY_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.KNOW_HOW_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.PROF_CON_FEE_E_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.INTEREST_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.OTHER_MATTERS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.DIVIDEND_PAID_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Current.TOT_EXP_FE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    }
                    #endregion

                    #region Previous Period
                    if (objDetailedPL.Previous != null)
                    {
                        var strValue = string.Empty;
                        rowIndex = 8;
                        strValue = Convert.ToString(objDetailedPL.Previous.EXP_GOODS_FOB_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.INTEREST_DIVD_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.ROYALTY_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.KNOW_HOW_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.PROF_CONS_FEE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.OTHR_INCOME_E_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.TOTAL_EARNG_FE_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex = 20;
                        strValue = Convert.ToString(objDetailedPL.Previous.RAW_MATERIAL_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.COMPONENT_SP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.CAPITAL_GOODS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.ROYALTY_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.KNOW_HOW_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.PROF_CON_FEE_E_C);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.INTEREST_EXP_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.OTHER_MATTERS_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.DIVIDEND_PAID_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        rowIndex++;
                        strValue = Convert.ToString(objDetailedPL.Previous.TOT_EXP_FE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;
                    }
                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "ExcelAOC4_DetailedProfitLossItems.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelFinancialParameters_PL(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "ExcelAOC4_FinancialParameter_PL.xlsx");
            Stream stream = new MemoryStream(fileBytes);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var objFPLVM = objIAOC4Service.GetFinancialParameter_PLDefaultData(id, customerID);
            if (objFPLVM != null)
            {

                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    #region  Financial parameter Profit and Loss Items
                    int rowIndex = 4;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["FinancialParameters_PL"];
                    var strValue = string.Empty;

                    rowIndex = 6;
                    strValue = Convert.ToString(objFPLVM.PROPOSED_DIVIDND);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.BASIC_EARNING_PS);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.DILUTED_EARN_PS);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.INCOME_FOREIGN);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.EXPENDIT_FOREIGN);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.REVENUE_SUBSIDIE);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.RENT_PAID);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.CONSUMPTION_STOR);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.GROSS_VALUE_TRAN);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex++;
                    strValue = Convert.ToString(objFPLVM.BAD_DEBTS_RP);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                    rowIndex = 6;
                    strValue = Convert.ToString(objFPLVM.PROPOSED_DIV_PER);
                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                    excelSheet.Cells[("E" + rowIndex)].Value = strValue;
                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "ExcelAOC4_FinancialParameter_PL.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SampleExcelReportingCSR(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "AOC4_ReportingCSR.xlsx");
            Stream stream = new MemoryStream(fileBytes);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var objCSRVM = objIAOC4Service.GetDefaultCSRExcelData(id, customerID);
            if (objCSRVM != null)
            {

                using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                {
                    #region  Reporting CSR items
                    int rowIndex = 5;
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["ReportCSR"];
                    var strValue = string.Empty;

                    foreach (var item in objCSRVM)
                    {
                        strValue = Convert.ToString(item.CSR_PROJECT_ACTV);
                        excelSheet.Cells[("B" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(item.SECTOR_PROJ_COVR_Str);
                        excelSheet.Cells[("C" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(item.STATE_UT_Str);
                        excelSheet.Cells[("D" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(item.AMOUNT_OUTLAY);
                        excelSheet.Cells[("E" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(item.AMOUNT_SPENT_PRJ);
                        excelSheet.Cells[("F" + rowIndex)].Value = strValue;

                        strValue = Convert.ToString(item.MODE_AMOUNT_SPNT_Str);
                        excelSheet.Cells[("H" + rowIndex)].Value = strValue;
                    }
                    #endregion

                    fileBytes = xlWorkbook.GetAsByteArray();
                }
            }
            string fileName = "AOC4_ReportingCSR.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        #endregion

        #region RPT
        public ActionResult RPT(long id, int entityId)
        {
            ExcelFileAOC4UploadVM model = new ExcelFileAOC4UploadVM() { ExcelAOC4Id = id };
            return PartialView("_RPT", model);
        }

        [HttpPost]
        public ActionResult UploadExcelRPT(ExcelFileAOC4UploadVM model)
        {
            //import Excel
            try
            {
                using (ExcelPackage xlWorkbook = new ExcelPackage(model.File.InputStream))
                {
                    var strVal = "";
                    decimal actualVal = 0;
                    var endPeriodDate = DateTime.Now;
                    int rowIndex = 4;

                    RPT_VM obj = new RPT_VM()
                    {
                        RPTAOC4ID = model.ExcelAOC4Id,
                        lstContracts = new List<RPT_Contracts_VM>(),
                        lstMaterialContracts = new List<RPT_MaterialContracts_VM>()
                    };

                    #region Contracts
                    ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["Contracts"];
                    int xlrow2 = excelSheet.Dimension.End.Row;
                    for (rowIndex = 4; rowIndex <= xlrow2; rowIndex++)
                    {
                        var item = new RPT_Contracts_VM();

                        strVal = Convert.ToString(excelSheet.Cells[("A" + rowIndex)].Text).Trim();
                        item.NAME_RELATED_PAR = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("B" + rowIndex)].Text).Trim();
                        item.NATURE_OF_RELATN = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                        item.NATURE_OF_CONTRA = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                        item.DURATION_OF_CONT = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            if (!DateTime.TryParse(strVal, out endPeriodDate))
                            {
                                item.DATE_OF_APPROVAL = null;
                            }
                            else
                            {
                                item.DATE_OF_APPROVAL = endPeriodDate;
                            }
                        }

                        strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                        actualVal = 0;
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                            {
                                actualVal = 0;
                            }
                        }
                        item.AMOUNT_PAID = actualVal;

                        strVal = Convert.ToString(excelSheet.Cells[("G" + rowIndex)].Text).Trim();
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            if (!DateTime.TryParse(strVal, out endPeriodDate))
                            {
                                item.DATE_SPCL_RESOLT = null;
                            }
                            else
                            {
                                item.DATE_SPCL_RESOLT = endPeriodDate;
                            }
                        }

                        obj.lstContracts.Add(item);
                    }
                    #endregion

                    #region MaterialContracts
                    excelSheet = xlWorkbook.Workbook.Worksheets["MaterialContracts"];
                    xlrow2 = excelSheet.Dimension.End.Row;
                    for (rowIndex = 4; rowIndex <= xlrow2; rowIndex++)
                    {
                        var item = new RPT_MaterialContracts_VM();

                        strVal = Convert.ToString(excelSheet.Cells[("A" + rowIndex)].Text).Trim();
                        item.NAME_RELATED_PAR = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("B" + rowIndex)].Text).Trim();
                        item.NATURE_OF_RELATN = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                        item.NATURE_OF_CONTRA = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                        item.DURATION_OF_CONT = string.IsNullOrEmpty(strVal) ? "" : strVal;

                        strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            if (!DateTime.TryParse(strVal, out endPeriodDate))
                            {
                                item.DATE_OF_APPROVAL = null;
                            }
                            else
                            {
                                item.DATE_OF_APPROVAL = endPeriodDate;
                            }
                        }

                        strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                        actualVal = 0;
                        if (!string.IsNullOrEmpty(strVal))
                        {
                            if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                            {
                                actualVal = 0;
                            }
                        }
                        item.AMOUNT_PAID = actualVal;

                        obj.lstMaterialContracts.Add(item);
                    }
                    #endregion

                    int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    var r = objIAOC4Service.SaveRPT(obj, userID);
                    model.Success = r.Success;
                    model.Error = r.Error;
                    model.Message = r.Message;
                }
            }
            catch (Exception ex)
            {
                model.Error = true;
                model.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
            }
            return PartialView("_RPT", model);
        }

        public ActionResult PreviewRPT(long id, int entityId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetRPTDetails(id, customerID);
            return PartialView("_RPTPreview", model);
        }
        #endregion

        public ActionResult SampleExcelAOC4RPT(long id)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\AOC4\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "AOC4_RPT.xlsx");
            Stream stream = new MemoryStream(fileBytes);

            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var objRPT = objIAOC4Service.GetRPTDetails(id, customerID);
            var rowIndex = 0;

            var strVal = string.Empty;

            if (objRPT != null)
            {
                if (objRPT.lstContracts.Count > 0 || objRPT.lstMaterialContracts.Count > 0)
                {
                    using (ExcelPackage xlWorkbook = new ExcelPackage(stream))
                    {
                        #region Contracts
                        if (objRPT.lstContracts.Count > 0)
                        {
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["Contracts"];
                            rowIndex = 3;
                            foreach (var item in objRPT.lstContracts)
                            {
                                rowIndex++;

                                strVal = string.IsNullOrEmpty(item.NAME_RELATED_PAR) ? "" : item.NAME_RELATED_PAR;
                                excelSheet.Cells[("A" + rowIndex)].Value = strVal;

                                strVal = string.IsNullOrEmpty(item.NATURE_OF_RELATN) ? "" : item.NATURE_OF_RELATN;
                                excelSheet.Cells[("B" + rowIndex)].Value = strVal;

                                strVal = string.IsNullOrEmpty(item.NATURE_OF_CONTRA) ? "" : item.NATURE_OF_CONTRA;
                                excelSheet.Cells[("C" + rowIndex)].Value = strVal;

                                strVal = string.IsNullOrEmpty(item.DURATION_OF_CONT) ? "" : item.DURATION_OF_CONT;
                                excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                                strVal = item.DATE_OF_APPROVAL == null ? "" : Convert.ToDateTime(item.DATE_OF_APPROVAL).ToString("dd-MMM-yyyy");
                                excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                                strVal = item.AMOUNT_PAID == null ? "" : Convert.ToString(item.AMOUNT_PAID);
                                excelSheet.Cells[("F" + rowIndex)].Value = strVal;

                                strVal = item.DATE_SPCL_RESOLT == null ? "" : Convert.ToDateTime(item.DATE_SPCL_RESOLT).ToString("dd-MMM-yyyy");
                                excelSheet.Cells[("G" + rowIndex)].Value = strVal;
                            }
                        }
                        #endregion

                        #region Material Contracts
                        if (objRPT.lstMaterialContracts.Count > 0)
                        {
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["MaterialContracts"];
                            rowIndex = 3;
                            foreach (var item in objRPT.lstMaterialContracts)
                            {
                                rowIndex++;

                                strVal = string.IsNullOrEmpty(item.NAME_RELATED_PAR) ? "" : item.NAME_RELATED_PAR;
                                excelSheet.Cells[("A" + rowIndex)].Value = strVal;

                                strVal = string.IsNullOrEmpty(item.NATURE_OF_RELATN) ? "" : item.NATURE_OF_RELATN;
                                excelSheet.Cells[("B" + rowIndex)].Value = strVal;

                                strVal = string.IsNullOrEmpty(item.NATURE_OF_CONTRA) ? "" : item.NATURE_OF_CONTRA;
                                excelSheet.Cells[("C" + rowIndex)].Value = strVal;

                                strVal = string.IsNullOrEmpty(item.DURATION_OF_CONT) ? "" : item.DURATION_OF_CONT;
                                excelSheet.Cells[("D" + rowIndex)].Value = strVal;

                                strVal = item.DATE_OF_APPROVAL == null ? "" : Convert.ToDateTime(item.DATE_OF_APPROVAL).ToString("dd-MMM-yyyy");
                                excelSheet.Cells[("E" + rowIndex)].Value = strVal;

                                strVal = item.AMOUNT_PAID == null ? "" : Convert.ToString(item.AMOUNT_PAID);
                                excelSheet.Cells[("F" + rowIndex)].Value = strVal;
                            }
                        }
                        #endregion

                        fileBytes = xlWorkbook.GetAsByteArray();
                    }
                }
            }
            string fileName = "AOC-4 Related Party Transactions.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


        #region Misc & Auth

        public ActionResult MiscAuth(long id)
        {
            var model = objIAOC4Service.GetMiscAuth(id);
            return PartialView("_Misc_Auth", model);
        }

        [HttpPost]
        public ActionResult SaveMiscAuth(MiscellaneousAuthorizationVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (ModelState.IsValid)
            {
                model = objIAOC4Service.SaveMiscAuth(model, userID);
            }
            return PartialView("_Misc_Auth", model);
        }

        public ActionResult PreviewMiscAuth(long id)
        {
            var model = objIAOC4Service.GetMiscAuth(id);
            return PartialView("_Misc_AuthPreview", model);
        }

        #endregion

        #region CSR      
        public ActionResult ReportingCSR(long id)
        {
            var model = objIAOC4Service.GetReportingCSR(id);
            return PartialView("_ReportingCSR", model);
        }
        [HttpPost]
        public ActionResult SaveReportingCSR(CSR_VM _objCSR)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (_objCSR != null)
            {
                if (_objCSR.File != null)
                {
                    try
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(_objCSR.File.InputStream))
                        {
                            var strVal = "";
                            decimal actualVal = 0;
                            ExcelWorksheet excelSheet = xlWorkbook.Workbook.Worksheets["ReportCSR"];
                            int xlrow2 = excelSheet.Dimension.End.Row;
                            int rowIndex = 5;

                            List<CSRDetails_VM> lstTemp = new List<CSRDetails_VM>();
                            var objCSRVM = new CSRDetails_VM() { CSRDetailsAOC4Id = _objCSR.CSRAOC4Id };

                            #region  Reporting CSR Data
                            //rowIndex = 5;
                            actualVal = 0;
                            for (rowIndex = 5; rowIndex <= xlrow2; rowIndex++)
                            {
                                objCSRVM = new CSRDetails_VM();
                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("B" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    objCSRVM.CSR_PROJECT_ACTV = strVal;
                                }
                                objCSRVM.CSR_PROJECT_ACTV = Convert.ToString(strVal);

                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("C" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    objCSRVM.SECTOR_PROJ_COVR_Str = strVal;
                                }

                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("D" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    objCSRVM.STATE_UT_Str = strVal;
                                }

                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("E" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objCSRVM.AMOUNT_OUTLAY = actualVal;

                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("F" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    if (!decimal.TryParse(strVal.Replace(",", ""), out actualVal))
                                    {
                                        actualVal = 0;
                                    }
                                }
                                objCSRVM.AMOUNT_SPENT_PRJ = actualVal;

                                actualVal = 0;
                                strVal = Convert.ToString(excelSheet.Cells[("H" + rowIndex)].Text).Trim();
                                if (!string.IsNullOrEmpty(strVal))
                                {
                                    objCSRVM.MODE_AMOUNT_SPNT_Str = strVal;
                                }
                                lstTemp.Add(objCSRVM);
                            }
                            _objCSR.lstDetails = lstTemp;
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        _objCSR.Error = true;
                        _objCSR.Message = BM_ManegmentServices.SecretarialConst.Messages.serverError;
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                var r = objIAOC4Service.SaveReportingCSR(_objCSR, userID);
                _objCSR.Success = r.Success;
                _objCSR.Error = r.Error;
                _objCSR.Message = r.Message;
            }
            else
            {
                _objCSR = new CSR_VM();
                _objCSR.Error = true;
            }
            return PartialView("_ReportingCSR", _objCSR);
        }

        public ActionResult PreviewReportingCSR(long id)
        {
            var model = objIAOC4Service.GetReportingCSRWithDetails(id);
            return PartialView("_PreviewReportingCSR", model);
        }
        #endregion

        #region  Auditor's Report
        public ActionResult AuditorsReports(long id)
        {
            var model = objIAOC4Service.GetAuditorsReport(id);
            //var model = new AuditorsReport_VM();
            return PartialView("_AuditorsReport", model);
        }
        public ActionResult SaveAuditorsReport(AuditorsReport_VM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (ModelState.IsValid)
            {
                model = objIAOC4Service.SaveAuditReport(model, userID);
            }
            return PartialView("_AuditorsReport", model);
        }
        public ActionResult PreviewAuditorsReport(long id)
        {
            //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIAOC4Service.GetAuditorsReport(id);
            return PartialView("_PreviewAuditorsReport", model);
        }

        #endregion

        #region Import AOC4
        public ActionResult UploadFileopenPopUP()
        {
            AOC4FormUploadVM model = new AOC4FormUploadVM();
            return PartialView("UploadAOC4", model);
        }

        public ActionResult ImportAOC4(AOC4FormUploadVM model)
        {
            if (ModelState.IsValid)
            {
                var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var userId = Convert.ToInt32(AuthenticationHelper.UserID);
                model = objIAOC4Service.UpoadAOC4(model, customerId, userId);
            }
            return PartialView("UploadAOC4", model);
        }
        #endregion

        #region Generate e-Form
        public ActionResult DownloadForm(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var form = objIForms_Service.GenerateAOC4(id, customerID);
            if (form.Success)
            {
                return File(form.FormData, System.Net.Mime.MediaTypeNames.Application.Octet, form.FormName);
            }
            else
            {
                return new EmptyResult();
            }
        }
        #endregion

    }
}