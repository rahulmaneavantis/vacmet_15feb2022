﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AdditionalActController : Controller
    {
        // GET: BM_Management/AdditionalAct
        IAdditionalActMaster objcomplianceAct;
        public AdditionalActController(IAdditionalActMaster objcomplianceAct)
        {
            this.objcomplianceAct = objcomplianceAct;
        }
        public ActionResult Index()
        {
            return View();
        }


        #region Add Additional Compliances added By Rohit on 21 Oct 2021
        public ActionResult AddNewAct()
        {
            AddtionalAct_VM _objnewAct = new AddtionalAct_VM();
            return PartialView("_AddAdditionalAct", _objnewAct);
        }


        public ActionResult CreateAdditionalAct(AddtionalAct_VM _objadditionalact)
        {
            if (ModelState.IsValid)
            {
                int UserId = AuthenticationHelper.UserID;
                if (_objadditionalact.Id == 0)
                {
                    _objadditionalact = objcomplianceAct.AddAdditionalAct(_objadditionalact, UserId);
                }
                else
                {
                    _objadditionalact = objcomplianceAct.UpdateAdditionalAct(_objadditionalact, UserId);
                }
            }
            return PartialView("_AddAdditionalAct", _objadditionalact);
        }


        #endregion

    }
}