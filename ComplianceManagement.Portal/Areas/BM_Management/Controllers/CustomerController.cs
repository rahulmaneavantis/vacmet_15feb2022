﻿using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CustomerController : Controller
    {
        // GET: BM_Management/Customer
        public ActionResult Index()
        {            
            if (AuthenticationHelper.CustomerID != null)
            {
                //var limitRecord = ICIAManagement.GetCustomerEntityLimits(Convert.ToInt32(AuthenticationHelper.CustomerID));

                //if(limitRecord!=null)
                //{
                //    model.totalCustomerCreated = limitRecord.Cu
                //}
            }

            return View("CustomerList");
        }

        public ActionResult GroupClientEntities()
        {
            return View("GroupClientEntities");
        }
    }
}