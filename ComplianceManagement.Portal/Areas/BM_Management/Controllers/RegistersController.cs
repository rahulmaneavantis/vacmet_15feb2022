﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Registers;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class RegistersController : Controller
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();
        IRegisterDetails objregister;
        IMBP2TransactionService objmbpregister;
        IDirectorMaster objIDirectorMaster;
        public RegistersController(IRegisterDetails objregister, IDirectorMaster objIDirectorMaster, IMBP2TransactionService objmbpregister)
        {
            this.objregister = objregister;
            this.objIDirectorMaster = objIDirectorMaster;
            this.objmbpregister = objmbpregister;
        }
        // GET: BM_Management/Registers
        public ActionResult Index()
        {
            return View();
        }

        #region Director Register

        public ActionResult ExportDirectorRegister(int EntityID, int DirectorId) //DirectorId -- as DetailsofInterestID
        {
            List<Directors> GetDirectorDetailsEntityWise = new List<Directors>();
            Entity EntityDetails = new Entity();
            List<DirectorSecurity> GetDirectorSecurityDetails = new List<DirectorSecurity>();
            List<DetailsOfInterest> GetCompanyDesignationDetails = new List<DetailsOfInterest>();
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            EntityDetails = objregister.getEntityDetails(EntityID);

            GetDirectorDetailsEntityWise = objregister.GetDirectorKMPDetails(EntityID, DirectorId, CustomerID);
            //var GetDirectorDetailsEntityWise = objregister.getDirectorDetails(EntityID, DirectorId, CustomerID);
            if (GetDirectorDetailsEntityWise.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        int sheetNO = 0;
                        foreach (var dir in GetDirectorDetailsEntityWise)
                        {
                            sheetNO++;
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(dir.FullName + "-" + sheetNO);

                            exWorkSheet1.Cells["A3"].Value = "REGISTER OF DIRECTORS AND KEY MANAGERIAL PERSONNEL AND THEIR SHAREHOLDING";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A3:O3"].Merge = true;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A4"].Value = "[Pursuant to section 170 of the Companies Act, 2013 and rule 17 of the Companies (Appointment and Qualification of Directors) Rules, 2014]";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A4:O4"].Merge = true;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A5"].Value = "";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A5:O5"].Merge = true;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                            ////exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Row(6).Height = 31.25;

                            exWorkSheet1.Cells["A6"].Value = "Name of the Company:";
                            //exWorkSheet1.Cells["A6"].AutoFitColumns(8);
                            //exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Cells["A6:B6"].Merge = true;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A6"].Style.WrapText = true;

                            exWorkSheet1.Cells["C6"].Value = EntityDetails.EntityName;
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["C6:F6"].Merge = true;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["C6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["C6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["C6"].Style.WrapText = false;
                            //exWorkSheet1.Cells["C6"].AutoFitColumns(8);

                            exWorkSheet1.Cells["G6"].Value = "Registered Office Address:";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["G6:H6"].Merge = true;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["G6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["G6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["G6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["G6"].Style.WrapText = true;
                            exWorkSheet1.Cells["G6"].AutoFitColumns(12);

                            exWorkSheet1.Cells["I6"].Value = EntityDetails.Address;
                            exWorkSheet1.Cells["I6:O6"].Merge = true;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["I6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["I6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["I6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["I6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["I6"].Style.WrapText = false;
                            exWorkSheet1.Cells["I6"].AutoFitColumns(12);

                            exWorkSheet1.Cells["A8"].Value = "";
                            exWorkSheet1.Cells["A8:O8"].Merge = true;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A8"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A8"].Style.WrapText = true;

                            if (dir.IsDirector)
                            {
                                exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                                exWorkSheet1.Cells["A9:C9"].Merge = true;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A9"].Style.WrapText = true;

                                exWorkSheet1.Cells["D9"].Value = dir.DIN;
                                exWorkSheet1.Cells["D9:H9"].Merge = true;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D9"].Style.WrapText = true;
                            }
                            else if (dir.IsMNGT)
                            {
                                exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                                exWorkSheet1.Cells["A9:C9"].Merge = true;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A9"].Style.WrapText = true;

                                exWorkSheet1.Cells["D9"].Value = "";
                                exWorkSheet1.Cells["D9:H9"].Merge = true;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D9"].Style.WrapText = true;
                            }
                            else
                            {
                                exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                                exWorkSheet1.Cells["A9:C9"].Merge = true;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A9"].Style.WrapText = true;

                                exWorkSheet1.Cells["D9"].Value = dir.DIN;
                                exWorkSheet1.Cells["D9:H9"].Merge = true;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D9"].Style.WrapText = true;
                            }


                            //exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                            //exWorkSheet1.Cells["A9:C9"].Merge = true;
                            //exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            //exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ////exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                            ////exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ////exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A9"].Style.WrapText = true;

                            //exWorkSheet1.Cells["D9"].Value = dir.DIN;
                            //exWorkSheet1.Cells["D9:H9"].Merge = true;
                            //exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            //exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ////exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                            ////exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ////exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["D9"].Style.WrapText = true;

                            exWorkSheet1.Cells["J9"].Value = "Nationality (including the nationality of origin, if different)";
                            exWorkSheet1.Cells["J9:L9"].Merge = true;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J9"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J9"].Style.WrapText = true;

                            exWorkSheet1.Cells["M9"].Value = dir.Nationalality;
                            exWorkSheet1.Cells["M9:O9"].Merge = true;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M9"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M9"].Style.WrapText = true;

                            exWorkSheet1.Cells["A10"].Value = "Present name and surname in Full:";
                            exWorkSheet1.Row(10).Height = 31.25;
                            exWorkSheet1.Cells["A10:C10"].Merge = true;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A10"].Style.WrapText = true;

                            exWorkSheet1.Cells["D10"].Value = dir.FullName;
                            exWorkSheet1.Cells["D10:H10"].Merge = true;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D10"].Style.WrapText = true;

                            exWorkSheet1.Cells["J10"].Value = "Occupation:";
                            exWorkSheet1.Cells["J10:L10"].Merge = true;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J10"].Style.WrapText = true;

                            exWorkSheet1.Cells["M10"].Value = dir.Occupation;
                            exWorkSheet1.Cells["M10:O10"].Merge = true;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M10"].Style.WrapText = true;

                            exWorkSheet1.Cells["A11"].Value = "Any former name and surname in Full:";
                            exWorkSheet1.Cells["A11:C11"].Merge = true;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A11"].Style.WrapText = true;

                            exWorkSheet1.Cells["D11"].Value = "";
                            exWorkSheet1.Cells["D11:H11"].Merge = true;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D11"].Style.WrapText = true;

                            exWorkSheet1.Cells["J11"].Value = "Date of Board Resolution in which appointment was made:";
                            exWorkSheet1.Cells["J11:L11"].Merge = true;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J11"].Style.WrapText = true;

                            if (dir.DateofBoardResolution != null)
                            {
                                exWorkSheet1.Cells["M11"].Value = Convert.ToDateTime(dir.DateofBoardResolution).ToString("dd-MMM-yyyy");
                            }
                            else
                            {
                                exWorkSheet1.Cells["M11"].Value = "";
                            }

                            exWorkSheet1.Cells["M11:O11"].Merge = true;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M11"].Style.WrapText = true;

                            exWorkSheet1.Cells["A12"].Value = "Father's Name:";
                            exWorkSheet1.Cells["A12:C12"].Merge = true;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            // exWorkSheet1.Cells["A12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A12"].Style.WrapText = true;

                            exWorkSheet1.Cells["D12"].Value = dir.FatherName;
                            exWorkSheet1.Cells["D12:H12"].Merge = true;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D12"].Style.WrapText = true;

                            exWorkSheet1.Cells["J12"].Value = "Date of appointment and reappointment in the company:";
                            exWorkSheet1.Cells["J12:L12"].Merge = true;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J12"].Style.WrapText = true;

                            if (dir.DateofAppointmentandReappointment != null)
                            {
                                exWorkSheet1.Cells["M12"].Value = Convert.ToDateTime(dir.DateofAppointmentandReappointment).ToString("dd-MMM-yyyy");
                            }
                            else
                            {
                                exWorkSheet1.Cells["M12"].Value = "";
                            }

                            exWorkSheet1.Cells["M12:O12"].Merge = true;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M12"].Style.WrapText = true;

                            exWorkSheet1.Cells["A13"].Value = "Mother's Name:";
                            exWorkSheet1.Cells["A13:C13"].Merge = true;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A13"].Style.WrapText = true;

                            exWorkSheet1.Cells["D13"].Value = dir.MotherName;
                            exWorkSheet1.Cells["D13:H13"].Merge = true;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D13"].Style.WrapText = true;

                            exWorkSheet1.Cells["J13"].Value = "Date  of cessation of office and reasons therefor:";
                            exWorkSheet1.Cells["J13:L13"].Merge = true;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J13"].Style.WrapText = true;

                            exWorkSheet1.Cells["M13"].Value = dir.DateofCessation;
                            exWorkSheet1.Cells["M13:O13"].Merge = true;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M13"].Style.WrapText = true;

                            exWorkSheet1.Cells["A14"].Value = "Spouse's Name (if married) and surname in Full:";
                            exWorkSheet1.Cells["A14:C14"].Merge = true;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            // exWorkSheet1.Cells["A14"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A14"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A14"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A14"].Style.WrapText = true;

                            exWorkSheet1.Cells["D14"].Value = dir.SpouseName;
                            exWorkSheet1.Cells["D14:H14"].Merge = true;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D14"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D14"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D14"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D14"].Style.WrapText = true;                            

                            exWorkSheet1.Cells["A15"].Value = "Date of Birth:";
                            exWorkSheet1.Cells["A15:C15"].Merge = true;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A15"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A15"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A15"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A15"].Style.WrapText = true;

                            exWorkSheet1.Cells["D15"].Value = dir.DateofBirth.ToString("dd-MMM-yyyy");
                            exWorkSheet1.Cells["D15:H15"].Merge = true;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D15"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D15"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D15"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D15"].Style.WrapText = true;

                            exWorkSheet1.Cells["A16"].Value = "Residential address:";
                            exWorkSheet1.Cells["A16:H16"].Merge = true;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A16"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A16"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A16"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A16"].Style.WrapText = true;

                            exWorkSheet1.Cells["A17"].Value = "Present:";
                            exWorkSheet1.Cells["A17:C17"].Merge = true;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A17"].Style.WrapText = true;
                            exWorkSheet1.Row(17).Height = 40.25;


                            exWorkSheet1.Cells["D17"].Value = dir.PresentAddress;
                            exWorkSheet1.Cells["D17:H17"].Merge = true;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D17"].Style.WrapText = true;

                            exWorkSheet1.Cells["J14"].Value = "Membership number of the Institute of Company Secretaries of India in case of Company Secretary, if applicable:";
                            exWorkSheet1.Cells["J14:L17"].Merge = true;
                            exWorkSheet1.Cells["J14:L14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J15:L17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J14:L14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J15:L17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J14:L14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J17:L17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J14"].Style.WrapText = true;

                            exWorkSheet1.Cells["M14"].Value = string.IsNullOrEmpty(dir.ICSNo) ? "" : dir.ICSNo;
                            exWorkSheet1.Cells["M14:O17"].Merge = true;
                            exWorkSheet1.Cells["M14:O14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M15:O17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M14:O14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M15:O17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M14:O14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M15:O17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M14"].Style.WrapText = true;

                            exWorkSheet1.Cells["A18"].Value = "Permanent:";
                            exWorkSheet1.Cells["A18:C18"].Merge = true;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A18"].Style.WrapText = true;
                            exWorkSheet1.Row(18).Height = 40.25;

                            exWorkSheet1.Cells["D18"].Value = dir.PresentAddress;
                            exWorkSheet1.Cells["D18:H18"].Merge = true;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D18"].Style.WrapText = true;

                            exWorkSheet1.Cells["J18"].Value = "Permenent Account Number (mandatory for Key Managerial Personnel if not having DIN):";
                            exWorkSheet1.Cells["J18:L18"].Merge = true;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J18"].Style.WrapText = true;
                            exWorkSheet1.Row(18).Height = 49.25;

                            exWorkSheet1.Cells["M18"].Value = dir.PAN;
                            exWorkSheet1.Cells["M18:O18"].Merge = true;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M18"].Style.WrapText = true;

                            exWorkSheet1.Cells["A19"].Value = "Details of securities held in Company, its holding, subsidiaries, subsidiaries of the company's holding Company and associate Companies:";
                            exWorkSheet1.Cells["A19:O19"].Merge = true;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A19"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A19"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A19"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A19"].Style.WrapText = true;

                            //exWorkSheet1.Cells["A21"].Value = "Sr.No."; ;
                            //exWorkSheet1.Cells["B21"].Value = "Name of the Company";
                            //exWorkSheet1.Cells["C21"].Value = "Number of securities";
                            //exWorkSheet1.Cells["D21"].Value = "Description of securities";
                            //exWorkSheet1.Cells["E21"].Value = "Nominal value of Securities";
                            //exWorkSheet1.Cells["F21"].Value = "Price paid for acquisition of securities";
                            //exWorkSheet1.Cells["G21"].Value = "Other consideration paid for acquisition";
                            //exWorkSheet1.Cells["A22"].LoadFromCollection(GetDirectorSecurityDetails);

                            DataView view1 = new DataView();
                            DataTable table1 = new DataTable();
                            int P = 0;

                            table1.Columns.Add("SrNo", typeof(string));
                            table1.Columns.Add("CompanyName", typeof(string));
                            table1.Columns.Add("NoofSecurities", typeof(long));
                            table1.Columns.Add("DescriptionofSecurity", typeof(string));
                            table1.Columns.Add("NominalvalueofSecurities", typeof(long));
                            table1.Columns.Add("DateofAccusation", typeof(string));
                            table1.Columns.Add("PricePaidforaccusation", typeof(decimal));
                            table1.Columns.Add("otherPricePaidforaccusation", typeof(decimal));
                            table1.Columns.Add("DateofDesposal", typeof(string));
                            table1.Columns.Add("Pricerecivefordisposal", typeof(decimal));
                            table1.Columns.Add("OtherPriceforDisposal", typeof(decimal));
                            table1.Columns.Add("CumaltiveBalance", typeof(long));
                            table1.Columns.Add("ModeofAccusation", typeof(string));
                            table1.Columns.Add("ModeOfHolding", typeof(string));
                            table1.Columns.Add("SecurityHasbeenpledge", typeof(string));
                            //GetDirectorSecurityDetails= GetDirectorSecurityDetails.Where(a=>a.k)

                            GetDirectorSecurityDetails = objregister.getDirectorSecurityDetails((int)dir.DirectorId, CustomerID, EntityID);

                            if (GetDirectorSecurityDetails.Count > 0)
                            {
                                foreach (var item in GetDirectorSecurityDetails)
                                {
                                    P++;
                                    table1.Rows.Add(P, item.CompanyName, item.NoofSecurities, item.DescriptionofSecurity, item.NominalvalueofSecurities, item.DateofAccusation, item.PricePaidforaccusation, item.otherPricePaidforaccusation, item.DateofDesposal, item.Pricerecivefordisposal, item.OtherPriceforDisposal, item.CumaltiveBalance, item.ModeofAccusation, item.ModeOfHolding, item.SecurityHasbeenpledge);
                                }

                                view1 = new System.Data.DataView(table1);

                                DataTable ExcelData = null;

                                ExcelData = view1.ToTable("Selected", false, "SrNo", "CompanyName", "NoofSecurities", "DescriptionofSecurity", "NominalvalueofSecurities", "DateofAccusation", "PricePaidforaccusation", "otherPricePaidforaccusation", "DateofDesposal", "Pricerecivefordisposal", "OtherPriceforDisposal", "CumaltiveBalance", "ModeofAccusation", "ModeOfHolding", "SecurityHasbeenpledge");

                                exWorkSheet1.Row(6).Height = 50.25;
                                exWorkSheet1.Cells["A21"].Value = "SrNo";
                                //exWorkSheet1.Cells["A21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["A21:A22"].Merge = true;
                                exWorkSheet1.Cells["A21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A21"].Style.WrapText = true;
                                exWorkSheet1.Cells["A21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["A21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["B21"].Value = "Name of the Company";

                                //exWorkSheet1.Cells["B21:B22"].Merge = true;
                                exWorkSheet1.Cells["B21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["B21"].Style.WrapText = true;
                                exWorkSheet1.Cells["B21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["B21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["C21"].Value = "Number of securities";
                                //exWorkSheet1.Cells["C21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["C21:C22"].Merge = true;
                                exWorkSheet1.Cells["C21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["C21"].Style.WrapText = true;
                                exWorkSheet1.Cells["C21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["C21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["D21"].Value = "Description of securities";
                                //exWorkSheet1.Cells["D21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["D21:D22"].Merge = true;
                                exWorkSheet1.Cells["D21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D21"].Style.WrapText = true;
                                exWorkSheet1.Cells["D21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["D21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["E21"].Value = "Nominal value of Securities";
                                //exWorkSheet1.Cells["E21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["E21:E22"].Merge = true;
                                exWorkSheet1.Cells["E21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["E21"].Style.WrapText = true;
                                exWorkSheet1.Cells["E21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["E21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["F21"].Value = "Date of Acquisition";
                                //exWorkSheet1.Cells["F21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["F21:F22"].Merge = true;
                                exWorkSheet1.Cells["F21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["F21"].Style.WrapText = true;
                                exWorkSheet1.Cells["F21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["F21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["G21"].Value = "Price paid for acquisition of securities";
                                //exWorkSheet1.Cells["G21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["G21:G22"].Merge = true;
                                exWorkSheet1.Cells["G21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["G21"].Style.WrapText = true;
                                exWorkSheet1.Cells["G21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["G21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["H21"].Value = "Other consideration paid for acquisition";
                                //exWorkSheet1.Cells["H21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["H21:H22"].Merge = true;
                                exWorkSheet1.Cells["H21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["H21"].Style.WrapText = true;
                                exWorkSheet1.Cells["H21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["H21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["I21"].Value = "Date of disposal";
                                //exWorkSheet1.Cells["I21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["I21:I22"].Merge = true;
                                exWorkSheet1.Cells["I21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["I21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["I21"].Style.WrapText = true;
                                exWorkSheet1.Cells["I21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["I21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["J21"].Value = "Price received on disposal";
                                //exWorkSheet1.Cells["J21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["J21:J22"].Merge = true;
                                exWorkSheet1.Cells["J21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["J21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["J21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["J21"].Style.WrapText = true;
                                exWorkSheet1.Cells["J21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["J21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["J21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["K21"].Value = "Other consideration received on disposal";
                                //exWorkSheet1.Cells["K21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["K21:K22"].Merge = true;
                                exWorkSheet1.Cells["K21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["K21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["K21"].Style.WrapText = true;
                                exWorkSheet1.Cells["K21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["K21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["K21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["L21"].Value = "Cumulative balance and number of securities held after each transaction";
                                //exWorkSheet1.Cells["L21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["L21:L22"].Merge = true;
                                exWorkSheet1.Cells["L21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["L21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["L21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["L21"].Style.WrapText = true;
                                exWorkSheet1.Cells["L21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["L21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["L21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["M21"].Value = "Mode of acquisition of securities";
                                //exWorkSheet1.Cells["M21"].AutoFitColumns(4);
                                // exWorkSheet1.Cells["M21:M22"].Merge = true;
                                exWorkSheet1.Cells["M21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["M21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["M21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["M21"].Style.WrapText = true;
                                exWorkSheet1.Cells["M21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["M21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["M21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["N21"].Value = "Mode of holding physical or in dematerialized form";
                                //exWorkSheet1.Cells["N21"].AutoFitColumns(4);
                                // exWorkSheet1.Cells["N21:N22"].Merge = true;
                                exWorkSheet1.Cells["N21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["N21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["N21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["N21"].Style.WrapText = true;
                                exWorkSheet1.Cells["N21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["N21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["O21"].Value = "Securities have been pledged or any encumbrance has been created";
                                //exWorkSheet1.Cells["O21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["O21:O22"].Merge = true;
                                exWorkSheet1.Cells["O21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["O21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["O21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["O21"].Style.WrapText = true;
                                exWorkSheet1.Cells["O21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["O21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                if (ExcelData.Rows.Count > 0)
                                {
                                    exWorkSheet1.Cells["A22"].LoadFromDataTable(ExcelData, false);
                                }

                                for (int j = 1; j <= 15; j++) // this will apply it from col 1 to 10
                                {
                                    exWorkSheet1.Column(j).Width = 12;
                                }

                                if (ExcelData.Rows.Count > 0)
                                {
                                    using (ExcelRange col = exWorkSheet1.Cells[22, 1, 21 + ExcelData.Rows.Count, 15])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }
                            }

                            DataView view2 = new DataView();
                            DataTable table2 = new DataTable();

                            int K = 0;
                            table2.Columns.Add("SrNo", typeof(string));
                            table2.Columns.Add("CompanyName", typeof(string));
                            table2.Columns.Add("Designation", typeof(string));
                            
                            GetCompanyDesignationDetails = objIDirectorMaster.DetailsOfIntrestList((int)dir.DirectorId, CustomerID);
                            if (GetCompanyDesignationDetails.Count > 0)
                            {
                             

                                foreach (var item in GetCompanyDesignationDetails)
                                {
                                    K++;
                                    table2.Rows.Add(K, item.EntityName, item.DirectorDesignationName);
                                }

                                DataTable ExcelData = null;
                                view2 = new System.Data.DataView(table2);                        

                                ExcelData = view2.ToTable("Selected", false, "SrNo", "CompanyName", "Designation");

                                var newRowIndex = (21 + P + 2);
                                exWorkSheet1.Row(24).Height = 33;
                                //exWorkSheet1.Column(5).Width = 12.5;

                                exWorkSheet1.Cells[("A" + newRowIndex)+":"+("C"+newRowIndex)].Value = "Office of director or Key Managerial Personnel held" + "\r\n" + "or relinquished in any other body corporate";
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Merge = true;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["J14:J15"].Merge = true;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                newRowIndex++;

                                exWorkSheet1.Cells[("A" + newRowIndex)].Value = "SrNo";
                               
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Font.Bold = true;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells[("B" + newRowIndex)].Value = "Name of the Company";

                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Font.Bold = true;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[("B" + newRowIndex + 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                exWorkSheet1.Cells[("C" + newRowIndex)].Value = "Designation";

                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Font.Bold = true;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                if (ExcelData.Rows.Count > 0)
                                {
                                    exWorkSheet1.Cells[("A" + (newRowIndex + 1))].LoadFromDataTable(ExcelData, false);
                                }

                                for (int j = 1; j <= 3; j++) // this will apply it from col 1 to 10
                                {
                                    exWorkSheet1.Column(j).Width = 12;
                                    if (j == 2)
                                    {
                                        exWorkSheet1.Column(j).Width = 24;
                                    }
                                }

                                if (ExcelData.Rows.Count > 0)
                                {
                                    using (ExcelRange col = exWorkSheet1.Cells[22 + P + 2, 1, newRowIndex + 1 + ExcelData.Rows.Count, 3])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    byte[] data = exportPackge.GetAsByteArray();

                    return File(data, "application/octet-stream", "Register of Director" + DateTime.Now + ".xlsx");
                }
            }
            else
            {
                return Json("Data not found", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult viewDirectorRegister(int EntityID, int DirectorId)
        {
            List<Directors> GetDirectorDetailsEntityWise = new List<Directors>();
            Entity EntityDetails = new Entity();
            List<DirectorSecurity> GetDirectorSecurityDetails = new List<DirectorSecurity>();
            List<DetailsOfInterest> GetCompanyDesignationDetails = new List<DetailsOfInterest>();
            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            EntityDetails = objregister.getEntityDetails(EntityID);
            int i = 0;

            GetDirectorDetailsEntityWise = objregister.GetDirectorKMPDetails(EntityID, DirectorId, CustomerID);
            //var GetDirectorDetailsEntityWise = objregister.getDirectorDetails(EntityID, DirectorId, CustomerID);
            if (GetDirectorDetailsEntityWise.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        foreach (var dir in GetDirectorDetailsEntityWise)
                        {
                            i++;

                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(dir.FullName + "-" + i);

                            exWorkSheet1.Cells["A3"].Value = "REGISTER OF DIRECTORS AND KEY MANAGERIAL PERSONNEL AND THEIR SHAREHOLDING";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A3:O3"].Merge = true;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:O3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A4"].Value = "[Pursuant to section 170 of the Companies Act, 2013 and rule 17 of the Companies (Appointment and Qualification of Directors) Rules, 2014]";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A4:O4"].Merge = true;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:O4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A5"].Value = "";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A5:O5"].Merge = true;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                            ////exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Row(6).Height = 31.25;

                            exWorkSheet1.Cells["A6"].Value = "Name of the Company:";
                            //exWorkSheet1.Cells["A6"].AutoFitColumns(8);
                            //exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Cells["A6:B6"].Merge = true;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:B6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A6"].Style.WrapText = true;

                            exWorkSheet1.Cells["C6"].Value = EntityDetails.EntityName;
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["C6:F6"].Merge = true;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6:F6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["C6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["C6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["C6"].Style.WrapText = false;
                            //exWorkSheet1.Cells["C6"].AutoFitColumns(8);

                            exWorkSheet1.Cells["G6"].Value = "Registered Office Address:";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["G6:H6"].Merge = true;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6:H6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["G6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["G6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["G6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["G6"].Style.WrapText = true;
                            exWorkSheet1.Cells["G6"].AutoFitColumns(12);

                            exWorkSheet1.Cells["I6"].Value = EntityDetails.Address;
                            exWorkSheet1.Cells["I6:O6"].Merge = true;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6:O6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["I6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["I6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["I6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["I6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["I6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["I6"].Style.WrapText = false;
                            exWorkSheet1.Cells["I6"].AutoFitColumns(12);

                            exWorkSheet1.Cells["A8"].Value = "";
                            exWorkSheet1.Cells["A8:O8"].Merge = true;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:O8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A8"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A8"].Style.WrapText = true;

                            if (dir.IsDirector)
                            {
                                exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                                exWorkSheet1.Cells["A9:C9"].Merge = true;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A9"].Style.WrapText = true;

                                exWorkSheet1.Cells["D9"].Value = dir.DIN;
                                exWorkSheet1.Cells["D9:H9"].Merge = true;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D9"].Style.WrapText = true;
                            }
                            else if (dir.IsMNGT)
                            {
                                exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                                exWorkSheet1.Cells["A9:C9"].Merge = true;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A9"].Style.WrapText = true;

                                exWorkSheet1.Cells["D9"].Value = "";
                                exWorkSheet1.Cells["D9:H9"].Merge = true;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D9"].Style.WrapText = true;
                            }
                            else
                            {
                                exWorkSheet1.Cells["A9"].Value = "Director Identification Number";
                                exWorkSheet1.Cells["A9:C9"].Merge = true;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9:C9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["A9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["A9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A9"].Style.WrapText = true;

                                exWorkSheet1.Cells["D9"].Value = dir.DIN;
                                exWorkSheet1.Cells["D9:H9"].Merge = true;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9:H9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["D9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D9"].Style.WrapText = true;
                            }

                            exWorkSheet1.Cells["J9"].Value = "Nationality (including the nationality of origin, if different)";
                            exWorkSheet1.Cells["J9:L9"].Merge = true;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9:L9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J9"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J9"].Style.WrapText = true;

                            exWorkSheet1.Cells["M9"].Value = dir.Nationalality;
                            exWorkSheet1.Cells["M9:O9"].Merge = true;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9:O9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M9"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M9"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M9"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M9"].Style.WrapText = true;

                            exWorkSheet1.Cells["A10"].Value = "Present name and surname in Full:";
                            exWorkSheet1.Row(10).Height = 31.25;
                            exWorkSheet1.Cells["A10:C10"].Merge = true;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:C10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A10"].Style.WrapText = true;

                            exWorkSheet1.Cells["D10"].Value = dir.FullName;
                            exWorkSheet1.Cells["D10:H10"].Merge = true;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10:H10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D10"].Style.WrapText = true;

                            exWorkSheet1.Cells["J10"].Value = "Occupation:";
                            exWorkSheet1.Cells["J10:L10"].Merge = true;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10:L10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J10"].Style.WrapText = true;

                            exWorkSheet1.Cells["M10"].Value = dir.Occupation;
                            exWorkSheet1.Cells["M10:O10"].Merge = true;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10:O10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M10"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M10"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M10"].Style.WrapText = true;

                            exWorkSheet1.Cells["A11"].Value = "Any former name and surname in Full:";
                            exWorkSheet1.Cells["A11:C11"].Merge = true;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11:C11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A11"].Style.WrapText = true;

                            exWorkSheet1.Cells["D11"].Value = "";
                            exWorkSheet1.Cells["D11:H11"].Merge = true;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11:H11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D11"].Style.WrapText = true;

                            exWorkSheet1.Cells["J11"].Value = "Date of Board Resolution in which appointment was made:";
                            exWorkSheet1.Cells["J11:L11"].Merge = true;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11:L11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J11"].Style.WrapText = true;

                            if (dir.DateofBoardResolution != null)
                            {
                                exWorkSheet1.Cells["M11"].Value = Convert.ToDateTime(dir.DateofBoardResolution).ToString("dd-MMM-yyyy");
                            }
                            else
                            {
                                exWorkSheet1.Cells["M11"].Value = "";
                            }

                            exWorkSheet1.Cells["M11:O11"].Merge = true;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11:O11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M11"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M11"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M11"].Style.WrapText = true;

                            exWorkSheet1.Cells["A12"].Value = "Father's Name:";
                            exWorkSheet1.Cells["A12:C12"].Merge = true;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12:C12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            // exWorkSheet1.Cells["A12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A12"].Style.WrapText = true;

                            exWorkSheet1.Cells["D12"].Value = dir.FatherName;
                            exWorkSheet1.Cells["D12:H12"].Merge = true;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12:H12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D12"].Style.WrapText = true;

                            exWorkSheet1.Cells["J12"].Value = "Date of appointment and reappointment in the company:";
                            exWorkSheet1.Cells["J12:L12"].Merge = true;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12:L12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J12"].Style.WrapText = true;

                            if (dir.DateofAppointmentandReappointment != null)
                            {
                                exWorkSheet1.Cells["M12"].Value = Convert.ToDateTime(dir.DateofAppointmentandReappointment).ToString("dd-MMM-yyyy");
                            }
                            else
                            {
                                exWorkSheet1.Cells["M12"].Value = "";
                            }

                            exWorkSheet1.Cells["M12:O12"].Merge = true;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12:O12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M12"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M12"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M12"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M12"].Style.WrapText = true;

                            exWorkSheet1.Cells["A13"].Value = "Mother's Name:";
                            exWorkSheet1.Cells["A13:C13"].Merge = true;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13:C13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A13"].Style.WrapText = true;

                            exWorkSheet1.Cells["D13"].Value = dir.MotherName;
                            exWorkSheet1.Cells["D13:H13"].Merge = true;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13:H13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D13"].Style.WrapText = true;

                            exWorkSheet1.Cells["J13"].Value = "Date  of cessation of office and reasons therefor:";
                            exWorkSheet1.Cells["J13:L13"].Merge = true;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13:L13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J13"].Style.WrapText = true;

                            exWorkSheet1.Cells["M13"].Value = dir.DateofCessation;
                            exWorkSheet1.Cells["M13:O13"].Merge = true;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13:O13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M13"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M13"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M13"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M13"].Style.WrapText = true;

                            exWorkSheet1.Cells["A14"].Value = "Spouse's Name (if married) and surname in Full:";
                            exWorkSheet1.Cells["A14:C14"].Merge = true;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14:C14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            // exWorkSheet1.Cells["A14"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A14"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A14"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A14"].Style.WrapText = true;

                            exWorkSheet1.Cells["D14"].Value = dir.SpouseName;
                            exWorkSheet1.Cells["D14:H14"].Merge = true;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14:H14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D14"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D14"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D14"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D14"].Style.WrapText = true;                     

                            exWorkSheet1.Cells["A15"].Value = "Date of Birth:";
                            exWorkSheet1.Cells["A15:C15"].Merge = true;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15:C15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A15"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A15"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A15"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A15"].Style.WrapText = true;

                            exWorkSheet1.Cells["D15"].Value = dir.DateofBirth.ToString("dd-MMM-yyyy");
                            exWorkSheet1.Cells["D15:H15"].Merge = true;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15:H15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D15"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D15"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D15"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D15"].Style.WrapText = true;

                            exWorkSheet1.Cells["A16"].Value = "Residential address:";
                            exWorkSheet1.Cells["A16:H16"].Merge = true;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16:H16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A16"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A16"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A16"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A16"].Style.WrapText = true;

                            exWorkSheet1.Cells["A17"].Value = "Present:";
                            exWorkSheet1.Cells["A17:C17"].Merge = true;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17:C17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A17"].Style.WrapText = true;
                            exWorkSheet1.Row(17).Height = 40.25;

                            exWorkSheet1.Cells["D17"].Value = dir.PresentAddress;
                            exWorkSheet1.Cells["D17:H17"].Merge = true;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17:H17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D17"].Style.WrapText = true;

                            exWorkSheet1.Cells["J14"].Value = "Membership number of the Institute of Company Secretaries of India in case of Company Secretary, if applicable:";
                            exWorkSheet1.Cells["J14:L17"].Merge = true;
                            exWorkSheet1.Cells["J14:L14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J15:L17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J14:L14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J15:L17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J14:L14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J17:L17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J14"].Style.WrapText = true;

                            exWorkSheet1.Cells["M14"].Value = string.IsNullOrEmpty(dir.ICSNo) ? "" : dir.ICSNo;
                            exWorkSheet1.Cells["M14:O17"].Merge = true;
                            exWorkSheet1.Cells["M14:O14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M15:O17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M14:O14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M15:O17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M14:O14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M15:O17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M17"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M17"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M14"].Style.WrapText = true;

                            exWorkSheet1.Cells["A18"].Value = "Permanent:";
                            exWorkSheet1.Cells["A18:C18"].Merge = true;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18:C18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["A18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A18"].Style.WrapText = true;
                            exWorkSheet1.Row(18).Height = 40.25;

                            exWorkSheet1.Cells["D18"].Value = dir.PresentAddress;
                            exWorkSheet1.Cells["D18:H18"].Merge = true;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18:H18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["D18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["D18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["D18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D18"].Style.WrapText = true;

                            exWorkSheet1.Cells["J18"].Value = "Permenent Account Number (mandatory for Key Managerial Personnel if not having DIN):";
                            exWorkSheet1.Cells["J18:L18"].Merge = true;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18:L18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["J18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["J18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["J18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J18"].Style.WrapText = true;
                            exWorkSheet1.Row(18).Height = 49.25;

                            exWorkSheet1.Cells["M18"].Value = dir.PAN;
                            exWorkSheet1.Cells["M18:O18"].Merge = true;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18:O18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["M18"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["M18"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["M18"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M18"].Style.WrapText = true;

                            exWorkSheet1.Cells["A19"].Value = "Details of securities held in Company, its holding, subsidiaries, subsidiaries of the company's holding Company and associate Companies:";
                            exWorkSheet1.Cells["A19:O19"].Merge = true;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19:O19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A19"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A19"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A19"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A19"].Style.WrapText = true;

                            //exWorkSheet1.Cells["A21"].Value = "Sr.No."; ;
                            //exWorkSheet1.Cells["B21"].Value = "Name of the Company";
                            //exWorkSheet1.Cells["C21"].Value = "Number of securities";
                            //exWorkSheet1.Cells["D21"].Value = "Description of securities";
                            //exWorkSheet1.Cells["E21"].Value = "Nominal value of Securities";
                            //exWorkSheet1.Cells["F21"].Value = "Price paid for acquisition of securities";
                            //exWorkSheet1.Cells["G21"].Value = "Other consideration paid for acquisition";
                            //exWorkSheet1.Cells["A22"].LoadFromCollection(GetDirectorSecurityDetails);

                            DataView view1 = new DataView();
                            DataTable table1 = new DataTable();
                            int P = 0;

                            table1.Columns.Add("SrNo", typeof(string));
                            table1.Columns.Add("CompanyName", typeof(string));
                            table1.Columns.Add("NoofSecurities", typeof(long));
                            table1.Columns.Add("DescriptionofSecurity", typeof(string));
                            table1.Columns.Add("NominalvalueofSecurities", typeof(long));
                            table1.Columns.Add("DateofAccusation", typeof(string));
                            table1.Columns.Add("PricePaidforaccusation", typeof(decimal));
                            table1.Columns.Add("otherPricePaidforaccusation", typeof(decimal));
                            table1.Columns.Add("DateofDesposal", typeof(string));
                            table1.Columns.Add("Pricerecivefordisposal", typeof(decimal));
                            table1.Columns.Add("OtherPriceforDisposal", typeof(decimal));
                            table1.Columns.Add("CumaltiveBalance", typeof(long));
                            table1.Columns.Add("ModeofAccusation", typeof(string));
                            table1.Columns.Add("ModeOfHolding", typeof(string));
                            table1.Columns.Add("SecurityHasbeenpledge", typeof(string));
                            //GetDirectorSecurityDetails= GetDirectorSecurityDetails.Where(a=>a.k)

                            GetDirectorSecurityDetails = objregister.getDirectorSecurityDetails((int)dir.DirectorId, CustomerID, EntityID);
                            if (GetDirectorSecurityDetails != null)
                            {
                                foreach (var item in GetDirectorSecurityDetails)
                                {
                                    P++;
                                    table1.Rows.Add(P, item.CompanyName, item.NoofSecurities, item.DescriptionofSecurity, item.NominalvalueofSecurities, item.DateofAccusation, item.PricePaidforaccusation, item.otherPricePaidforaccusation, item.DateofDesposal, item.Pricerecivefordisposal, item.OtherPriceforDisposal, item.CumaltiveBalance, item.ModeofAccusation, item.ModeOfHolding, item.SecurityHasbeenpledge);
                                }

                                view1 = new System.Data.DataView(table1);

                                DataTable ExcelData = null;

                                ExcelData = view1.ToTable("Selected", false, "SrNo", "CompanyName", "NoofSecurities", "DescriptionofSecurity", "NominalvalueofSecurities", "DateofAccusation", "PricePaidforaccusation", "otherPricePaidforaccusation", "DateofDesposal", "Pricerecivefordisposal", "OtherPriceforDisposal", "CumaltiveBalance", "ModeofAccusation", "ModeOfHolding", "SecurityHasbeenpledge");


                                exWorkSheet1.Row(6).Height = 50.25;
                                exWorkSheet1.Cells["A21"].Value = "SrNo";
                                //exWorkSheet1.Cells["A21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["A21:A22"].Merge = true;
                                exWorkSheet1.Cells["A21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A21"].Style.WrapText = true;
                                exWorkSheet1.Cells["A21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["A21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                exWorkSheet1.Cells["B21"].Value = "Name of the Company";

                                //exWorkSheet1.Cells["B21:B22"].Merge = true;
                                exWorkSheet1.Cells["B21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["B21"].Style.WrapText = true;
                                exWorkSheet1.Cells["B21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["B21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["C21"].Value = "Number of securities";
                                //exWorkSheet1.Cells["C21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["C21:C22"].Merge = true;
                                exWorkSheet1.Cells["C21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["C21"].Style.WrapText = true;
                                exWorkSheet1.Cells["C21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["C21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["D21"].Value = "Description of securities";
                                //exWorkSheet1.Cells["D21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["D21:D22"].Merge = true;
                                exWorkSheet1.Cells["D21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D21"].Style.WrapText = true;
                                exWorkSheet1.Cells["D21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["D21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["E21"].Value = "Nominal value of Securities";
                                //exWorkSheet1.Cells["E21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["E21:E22"].Merge = true;
                                exWorkSheet1.Cells["E21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["E21"].Style.WrapText = true;
                                exWorkSheet1.Cells["E21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["E21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["F21"].Value = "Date of Acquisition";
                                //exWorkSheet1.Cells["F21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["F21:F22"].Merge = true;
                                exWorkSheet1.Cells["F21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["F21"].Style.WrapText = true;
                                exWorkSheet1.Cells["F21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["F21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["G21"].Value = "Price paid for acquisition of securities";
                                //exWorkSheet1.Cells["G21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["G21:G22"].Merge = true;
                                exWorkSheet1.Cells["G21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["G21"].Style.WrapText = true;
                                exWorkSheet1.Cells["G21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["G21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["H21"].Value = "Other consideration paid for acquisition";
                                //exWorkSheet1.Cells["H21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["H21:H22"].Merge = true;
                                exWorkSheet1.Cells["H21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["H21"].Style.WrapText = true;
                                exWorkSheet1.Cells["H21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["H21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["I21"].Value = "Date of disposal";
                                //exWorkSheet1.Cells["I21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["I21:I22"].Merge = true;
                                exWorkSheet1.Cells["I21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["I21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["I21"].Style.WrapText = true;
                                exWorkSheet1.Cells["I21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["I21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["J21"].Value = "Price received on disposal";
                                //exWorkSheet1.Cells["J21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["J21:J22"].Merge = true;
                                exWorkSheet1.Cells["J21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["J21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["J21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["J21"].Style.WrapText = true;
                                exWorkSheet1.Cells["J21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["J21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["J21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["K21"].Value = "Other consideration received on disposal";
                                //exWorkSheet1.Cells["K21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["K21:K22"].Merge = true;
                                exWorkSheet1.Cells["K21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["K21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["K21"].Style.WrapText = true;
                                exWorkSheet1.Cells["K21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["K21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["K21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["L21"].Value = "Cumulative balance and number of securities held after each transaction";
                                //exWorkSheet1.Cells["L21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["L21:L22"].Merge = true;
                                exWorkSheet1.Cells["L21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["L21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["L21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["L21"].Style.WrapText = true;
                                exWorkSheet1.Cells["L21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["L21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["L21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["M21"].Value = "Mode of acquisition of securities";
                                //exWorkSheet1.Cells["M21"].AutoFitColumns(4);
                                // exWorkSheet1.Cells["M21:M22"].Merge = true;
                                exWorkSheet1.Cells["M21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["M21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["M21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["M21"].Style.WrapText = true;
                                exWorkSheet1.Cells["M21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["M21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["M21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["N21"].Value = "Mode of holding physical or in dematerialized form";
                                //exWorkSheet1.Cells["N21"].AutoFitColumns(4);
                                // exWorkSheet1.Cells["N21:N22"].Merge = true;
                                exWorkSheet1.Cells["N21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["N21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["N21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["N21"].Style.WrapText = true;
                                exWorkSheet1.Cells["N21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["N21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["O21"].Value = "Securities have been pledged or any encumbrance has been created";
                                //exWorkSheet1.Cells["O21"].AutoFitColumns(4);
                                //exWorkSheet1.Cells["O21:O22"].Merge = true;
                                exWorkSheet1.Cells["O21"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["O21"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["O21"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["O21"].Style.WrapText = true;
                                exWorkSheet1.Cells["O21"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["O21"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                if (ExcelData.Rows.Count > 0)
                                {
                                    exWorkSheet1.Cells["A22"].LoadFromDataTable(ExcelData, false);
                                }

                                for (int j = 1; j <= 15; j++) // this will apply it from col 1 to 10
                                {
                                    exWorkSheet1.Column(j).Width = 12;
                                }
                                if (ExcelData.Rows.Count > 0)
                                {
                                    using (ExcelRange col = exWorkSheet1.Cells[22, 1, 21 + ExcelData.Rows.Count, 15])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }
                            }

                            DataView view2 = new DataView();
                            DataTable table2 = new DataTable();

                            table2.Columns.Add("SrNo", typeof(string));
                            table2.Columns.Add("CompanyName", typeof(string));
                            table2.Columns.Add("Designation", typeof(string));

                            int K = 0;
                            GetCompanyDesignationDetails = objIDirectorMaster.DetailsOfIntrestList((int)dir.DirectorId, CustomerID);
                            if (GetCompanyDesignationDetails.Count > 0)
                            {
                               
                                foreach (var item in GetCompanyDesignationDetails)
                                {
                                    K++;
                                    table2.Rows.Add(K, item.EntityName, item.DirectorDesignationName);
                                }

                                view2 = new System.Data.DataView(table2);

                                DataTable ExcelData = null;

                                ExcelData = view2.ToTable("Selected", false, "SrNo", "CompanyName", "Designation");

                                var newRowIndex = (21 + P + 2);
                                exWorkSheet1.Row(24).Height = 33;
                                //exWorkSheet1.Column(5).Width = 12.5;

                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Value = "Office of director or Key Managerial Personnel held" + "\r\n" + "or relinquished in any other body corporate";
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Merge = true;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Font.Bold = true;
                                //exWorkSheet1.Cells["J14:J15"].Merge = true;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex) + ":" + ("C" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                newRowIndex++;

                                exWorkSheet1.Cells[("A" + newRowIndex)].Value = "SrNo";
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Font.Bold = true;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[("A" + newRowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells[("B" + newRowIndex)].Value = "Name of the Company";


                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Font.Bold = true;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("B" + newRowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[("B" + newRowIndex + 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                exWorkSheet1.Cells[("C" + newRowIndex)].Value = "Designation";


                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Font.Bold = true;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.WrapText = true;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[("C" + newRowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                if (ExcelData.Rows.Count > 0)
                                {
                                    exWorkSheet1.Cells[("A" + (newRowIndex + 1))].LoadFromDataTable(ExcelData, false);
                                }

                                for (int j = 1; j <= 3; j++) // this will apply it from col 1 to 10
                                {
                                    exWorkSheet1.Column(j).Width = 12;
                                    if (j == 2)
                                    {
                                        exWorkSheet1.Column(j).Width = 24;
                                    }
                                }

                                if (ExcelData.Rows.Count > 0)
                                {
                                    using (ExcelRange col = exWorkSheet1.Cells[22 + P + 2, 1, newRowIndex + 1 + ExcelData.Rows.Count, 3])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    CustomerID = Convert.ToInt16(AuthenticationHelper.CustomerID);
                    path = "~/Temp/" + CustomerID + "/";
                    Filename = "Register_of_Directors.xlsx";
                    if (!Directory.Exists(Server.MapPath(path)))
                    {
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    _path = System.IO.Path.Combine(Server.MapPath(path), Filename);
                    FileStream aFile = new FileStream(_path, FileMode.Create);
                    byte[] byData = exportPackge.GetAsByteArray();
                    aFile.Seek(0, SeekOrigin.Begin);
                    aFile.Write(byData, 0, byData.Length);
                    aFile.Close();
                }

                return Json(Filename, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Register of Members
        public ActionResult ExportMemberRegister(long EntityID, int MemberId)
        {
            List<MemberSecurities> MemberRegisterSecurities = new List<MemberSecurities>();
            List<MemberRegister> RegisterDetails = new List<MemberRegister>();
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            RegisterDetails = objregister.GetMemberofRegisterDetails(EntityID, MemberId, CustomerID);
            if (RegisterDetails.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {

                        foreach (var member in RegisterDetails)
                        {
                            MemberRegisterSecurities = objregister.GetMemberSecurities(EntityID, CustomerID, member.MemberId);
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(member.MemberName);
                            exWorkSheet1.Cells["A3"].Value = "REGISTER OF MEMBERS: FORM MGT-1";

                            exWorkSheet1.Cells["A3:W3"].Merge = true;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A4"].Value = "Pursuant to section 88(1)(a) of the Companies Act, 2013 and Rule 3(1) of the Companies (Management and Administration) Rules, 2014";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A4:W4"].Merge = true;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A5"].Value = "";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A5:O5"].Merge = true;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Row(6).Height = 31.25;

                            exWorkSheet1.Cells["A6"].Value = "Name of the Company:";
                            //exWorkSheet1.Cells["A6"].AutoFitColumns(8);
                            //exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Cells["A6:C6"].Merge = true;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A6"].Style.WrapText = true;

                            exWorkSheet1.Cells["D6"].Value = member.EntityName;
                            exWorkSheet1.Cells["D6:I6"].Merge = true;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["D6"].Style.WrapText = false;
                            //exWorkSheet1.Cells["C6"].AutoFitColumns(8);

                            exWorkSheet1.Cells["J6"].Value = "Registered Office Address:";
                            exWorkSheet1.Cells["J6:L6"].Merge = true;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["J6"].Style.WrapText = true;
                            exWorkSheet1.Cells["J6"].Style.Font.Bold = true;


                            exWorkSheet1.Cells["M6"].Value = member.EntityAddress;
                            exWorkSheet1.Cells["M6:W6"].Merge = true;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["M6"].Style.WrapText = false;

                            exWorkSheet1.Cells["A7"].Value = "Folio No.:";
                            exWorkSheet1.Cells["A7:D7"].Merge = true;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A7"].Style.WrapText = true;

                            exWorkSheet1.Cells["E7"].Value = member.FolioNumber;
                            //exWorkSheet1.Cells["E7"].Merge = true;
                            exWorkSheet1.Cells["E7"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E7"].Style.WrapText = true;

                            exWorkSheet1.Cells["G7"].Value = "Personal Details";
                            exWorkSheet1.Cells["G7"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G7:P8"].Merge = true;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G7"].Style.WrapText = true;

                            exWorkSheet1.Cells["R7"].Value = "Details of Membership";
                            exWorkSheet1.Cells["R7"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R7:W8"].Merge = true;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R7"].Style.WrapText = true;

                            exWorkSheet1.Cells["A8"].Value = "Class of Shares:";
                            exWorkSheet1.Cells["A8:D8"].Merge = true;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A8"].Style.WrapText = true;

                            exWorkSheet1.Cells["E8"].Value = member.ClassofShares;
                            exWorkSheet1.Cells["E8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E8"].Style.WrapText = true;

                            exWorkSheet1.Cells["A9"].Value = "Nominal value per share (in Rs.):";
                            exWorkSheet1.Cells["A9:D9"].Merge = true;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A9"].Style.WrapText = true;

                            exWorkSheet1.Cells["E9"].Value = member.Nominalvaluepershares;
                            exWorkSheet1.Cells["E9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E9"].Style.WrapText = true;

                            exWorkSheet1.Cells["A10"].Value = "Total shares held";
                            exWorkSheet1.Cells["A10:D10"].Merge = true;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A10"].Style.WrapText = true;

                            exWorkSheet1.Cells["E10"].Value = member.TotalSharesHeld;
                            exWorkSheet1.Cells["E10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E10"].Style.WrapText = true;

                            exWorkSheet1.Cells["G9"].Value = "Name of the Member:";
                            exWorkSheet1.Cells["G9:J9"].Merge = true;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G9"].Style.WrapText = true;

                            exWorkSheet1.Cells["K9"].Value = member.MemberName;
                            exWorkSheet1.Cells["K9:P9"].Merge = true;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K9"].Style.WrapText = true;

                            exWorkSheet1.Cells["G10"].Value = "Name of joint holders, if any";
                            exWorkSheet1.Cells["G10:J10"].Merge = true;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G10"].Style.WrapText = true;

                            exWorkSheet1.Cells["K10"].Value = member.JointHolderName;
                            exWorkSheet1.Cells["K10:P10"].Merge = true;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K10"].Style.WrapText = true;

                            exWorkSheet1.Cells["G11"].Value = "Address/Registered address (in case of body corporate)";
                            exWorkSheet1.Row(11).Height = 31.25;
                            exWorkSheet1.Cells["G11:J11"].Merge = true;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G11"].Style.WrapText = true;

                            exWorkSheet1.Cells["K11"].Value = member.Address;
                            exWorkSheet1.Cells["K11:P11"].Merge = true;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K11"].Style.WrapText = true;

                            exWorkSheet1.Cells["G12"].Value = "Email ID:";
                            exWorkSheet1.Cells["G12:J12"].Merge = true;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G12"].Style.WrapText = true;

                            exWorkSheet1.Cells["K12"].Value = member.EmailId;
                            exWorkSheet1.Cells["K12:P12"].Merge = true;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K12"].Style.WrapText = true;

                            exWorkSheet1.Cells["G13"].Value = "CIN/ Registration No.:";
                            exWorkSheet1.Cells["G13:J13"].Merge = true;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G13"].Style.WrapText = true;

                            exWorkSheet1.Cells["K13"].Value = member.CIN;
                            exWorkSheet1.Cells["K13:P13"].Merge = true;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K13"].Style.WrapText = true;

                            exWorkSheet1.Cells["G14"].Value = "Unique Identification Number:";
                            exWorkSheet1.Cells["G14:J14"].Merge = true;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G14"].Style.WrapText = true;

                            exWorkSheet1.Cells["K14"].Value = member.UIN;
                            exWorkSheet1.Cells["K14:P14"].Merge = true;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K14"].Style.WrapText = true;

                            exWorkSheet1.Cells["G15"].Value = "Father's/Mother's/Spouse's Name:";
                            exWorkSheet1.Cells["G15:J15"].Merge = true;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G15"].Style.WrapText = true;

                            exWorkSheet1.Cells["K15"].Value = member.Father_Mother_SpouseName;
                            exWorkSheet1.Cells["K15:P15"].Merge = true;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K15"].Style.WrapText = true;

                            exWorkSheet1.Cells["G16"].Value = "Status:";
                            exWorkSheet1.Cells["G16"].Merge = true;
                            exWorkSheet1.Cells["G16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G16"].Style.WrapText = true;

                            exWorkSheet1.Cells["H16"].Value = member.Status;
                            exWorkSheet1.Cells["H16:J16"].Merge = true;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["H16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H16"].Style.WrapText = true;

                            exWorkSheet1.Cells["K16"].Value = "Occupation:";
                            exWorkSheet1.Cells["K16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K16"].Style.WrapText = true;

                            exWorkSheet1.Cells["L16"].Value = member.Occupation;
                            exWorkSheet1.Cells["L16:P16"].Merge = true;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["L16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["L16"].Style.WrapText = true;

                            exWorkSheet1.Cells["G17"].Value = "PAN:";
                            exWorkSheet1.Cells["G17"].Merge = true;
                            exWorkSheet1.Cells["G17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G17"].Style.WrapText = true;

                            exWorkSheet1.Cells["H17"].Value = member.PAN;
                            exWorkSheet1.Cells["H17:J17"].Merge = true;
                            exWorkSheet1.Cells["H17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["H17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H17"].Style.WrapText = true;

                            exWorkSheet1.Cells["K17"].Value = "Nationality:";
                            exWorkSheet1.Cells["K17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K17"].Style.WrapText = true;

                            exWorkSheet1.Cells["L17"].Value = member.Nationality;
                            exWorkSheet1.Cells["L17:P17"].Merge = true;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["L17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["L17"].Style.WrapText = true;

                            //exWorkSheet1.Cells["P17"].Value = "";
                            //exWorkSheet1.Cells["P17"].Merge = true;
                            //exWorkSheet1.Cells["P17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["P17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["P17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["P17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            //exWorkSheet1.Cells["P17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            //exWorkSheet1.Cells["P17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Cells["P17"].Style.WrapText = true;

                            exWorkSheet1.Cells["G18"].Value = "";
                            exWorkSheet1.Cells["G18:P18"].Merge = true;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G18"].Style.WrapText = true;

                            exWorkSheet1.Cells["G19"].Value = "Name of Guardian:";
                            exWorkSheet1.Cells["G19:J19"].Merge = true;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G19"].Style.WrapText = true;

                            exWorkSheet1.Cells["K19"].Value = member.GuardianName;
                            exWorkSheet1.Cells["K19:P19"].Merge = true;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K19"].Style.WrapText = true;

                            exWorkSheet1.Cells["G20"].Value = "Date of birth of minor:";
                            exWorkSheet1.Cells["G20:J20"].Merge = true;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G20"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G20"].Style.WrapText = true;

                            exWorkSheet1.Cells["K20"].Value = member.DateofBirth;
                            exWorkSheet1.Cells["K20:P20"].Merge = true;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K20"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K20"].Style.WrapText = true;

                            exWorkSheet1.Cells["R9"].Value = "Date of Becoming member:";
                            exWorkSheet1.Cells["R9:S9"].Merge = true;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R9"].Style.WrapText = true;

                            exWorkSheet1.Cells["T9"].Value = member.DateofBecomingMember;
                            exWorkSheet1.Cells["T9:W9"].Merge = true;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T9"].Style.WrapText = true;

                            exWorkSheet1.Cells["R10"].Value = "Date of declaration under section 89, if applicable:";
                            exWorkSheet1.Cells["R10:S10"].Merge = true;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R10"].Style.WrapText = true;

                            exWorkSheet1.Cells["T10"].Value = member.Dateunder89;
                            exWorkSheet1.Cells["T10:W10"].Merge = true;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T10"].Style.WrapText = true;

                            exWorkSheet1.Cells["R11"].Value = "Name and address of beneficial owner:";
                            exWorkSheet1.Cells["R11:S11"].Merge = true;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R11"].Style.WrapText = true;

                            exWorkSheet1.Cells["T11"].Value = member.BenificialNameandAddress;
                            exWorkSheet1.Cells["T11:W11"].Merge = true;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T11"].Style.WrapText = true;

                            exWorkSheet1.Cells["R12"].Value = "Date of receipt of nomination, if applicable:";
                            exWorkSheet1.Cells["R12:S12"].Merge = true;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R12"].Style.WrapText = true;

                            exWorkSheet1.Cells["T12"].Value = member.DateofNomination;
                            exWorkSheet1.Cells["T12:W12"].Merge = true;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T12"].Style.WrapText = true;

                            exWorkSheet1.Cells["R13"].Value = "Name and address of nominee:";
                            exWorkSheet1.Cells["R13:S16"].Merge = true;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R13"].Style.WrapText = true;

                            exWorkSheet1.Cells["T13"].Value = member.NameandAddressofNomminee;
                            exWorkSheet1.Cells["T13:W16"].Merge = true;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T13"].Style.WrapText = true;

                            exWorkSheet1.Cells["R17"].Value = "No. of shares kept in abeyance, if applicable:";
                            exWorkSheet1.Cells["R17:S18"].Merge = true;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R17"].Style.WrapText = true;

                            exWorkSheet1.Cells["T17"].Value = member.sharesinAbayes;
                            exWorkSheet1.Cells["T17:W18"].Merge = true;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T17"].Style.WrapText = true;

                            exWorkSheet1.Cells["R19"].Value = "Date of cessation of membership:";
                            exWorkSheet1.Cells["R19:S20"].Merge = true;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R19"].Style.WrapText = true;

                            exWorkSheet1.Cells["T19"].Value = member.DateofCessation;
                            exWorkSheet1.Cells["T19:W20"].Merge = true;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T19"].Style.WrapText = true;

                            #region Member Securities
                            if (MemberRegisterSecurities.Count > 0)
                            {
                                DataView view1 = new DataView();
                                DataTable table1 = new DataTable();
                                int P = 0;

                                table1.Columns.Add("AllotmentNo", typeof(string));
                                table1.Columns.Add("DateofAllotment", typeof(string));
                                table1.Columns.Add("NumberofShares", typeof(int));
                                table1.Columns.Add("DistinctiveNoofShares_From", typeof(string));
                                table1.Columns.Add("DistinctiveNoofShares_To", typeof(string));
                                table1.Columns.Add("TransferorFolioNo", typeof(string));
                                table1.Columns.Add("NameOfTransferror", typeof(string));
                                table1.Columns.Add("DateofIssues", typeof(string));
                                table1.Columns.Add("CertificateNumber", typeof(string));
                                table1.Columns.Add("LockinPeriod", typeof(string));
                                table1.Columns.Add("AmountPayable", typeof(decimal));
                                table1.Columns.Add("AmountPaid", typeof(decimal));
                                table1.Columns.Add("AmountDue", typeof(decimal));
                                table1.Columns.Add("DescofConsidration", typeof(string));
                                table1.Columns.Add("DateofTransfer", typeof(string));
                                table1.Columns.Add("NoofSharesTransfered", typeof(int));
                                table1.Columns.Add("DistinctiveNo_Form", typeof(string));
                                table1.Columns.Add("DistinctiveNo_To", typeof(string));
                                table1.Columns.Add("TransfreeFolio", typeof(string));
                                table1.Columns.Add("NameofTransfaree", typeof(string));
                                table1.Columns.Add("BalanceofSharesHeld", typeof(int));
                                table1.Columns.Add("Remark", typeof(string));
                                table1.Columns.Add("Authentication", typeof(string));

                                foreach (var item in MemberRegisterSecurities)
                                {

                                    table1.Rows.Add(item.AllotmentNo, item.DateofAllotment, item.NumberofShares, item.DistinctiveNoofShares_From, item.DistinctiveNoofShares_To, item.TransferorFolioNo, item.NameOfTransferror, item.DateofIssues, item.CertificateNumber, item.LockinPeriod, item.AmountPayable, item.AmountPaid, item.AmountDue, item.DescofConsidration, item.DateofTransfer, item.NoofSharesTransfered, item.DistinctiveNo_Form, item.DistinctiveNo_To, item.TransfreeFolio, item.NameofTransfaree, item.BalanceofSharesHeld, item.Remark, item.Authentication);
                                }


                                view1 = new System.Data.DataView(table1);

                                DataTable ExcelData = null;

                                ExcelData = view1.ToTable("Selected", false, "AllotmentNo", "DateofAllotment", "NumberofShares", "DistinctiveNoofShares_From", "DistinctiveNoofShares_To", "TransferorFolioNo", "NameOfTransferror", "DateofIssues", "CertificateNumber", "LockinPeriod", "AmountPayable", "AmountPaid", "AmountDue", "DescofConsidration", "DateofTransfer", "NoofSharesTransfered", "DistinctiveNo_Form", "DistinctiveNo_To", "TransfreeFolio", "NameofTransfaree", "BalanceofSharesHeld", "Remark", "Authentication");


                                exWorkSheet1.Row(23).Height = 54;

                                exWorkSheet1.Cells["A23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["A23"].Value = "Allotment No./ Transfer No.";
                                exWorkSheet1.Cells["A23:A24"].Merge = true;
                                //exWorkSheet1.Cells["A23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["A23"].Style.WrapText = true;
                                exWorkSheet1.Cells["A23:A24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A23:A24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["A23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["B23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["B23"].Value = "Date of Allotment or Entry of Transfer";
                                exWorkSheet1.Cells["B23:B24"].Merge = true;
                                //exWorkSheet1.Cells["B23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["B23"].Style.WrapText = true;
                                exWorkSheet1.Cells["B23:B24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B23:B24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["B23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["B23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["C23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["C23"].Value = "No. of Shares alloted or transferred";
                                exWorkSheet1.Cells["C23:C24"].Merge = true;
                                //exWorkSheet1.Cells["C23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["C23"].Style.WrapText = true;
                                exWorkSheet1.Cells["C23:C24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C23:C24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["C23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["C23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                //exWorkSheet1.Cells["D23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["D23"].Value = "Distinctive No.of Shares  (both inclusive)  ";
                                exWorkSheet1.Cells["D23:E23"].Merge = true;
                                //exWorkSheet1.Cells["D23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D23"].Style.WrapText = true;
                                exWorkSheet1.Cells["D23:D24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D23:D24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D23:E23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["D23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["D24"].Value = "From";
                                exWorkSheet1.Cells["D24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["D24"].Style.WrapText = true;
                                exWorkSheet1.Cells["D24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["D24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["D24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["E24"].Value = "To";
                                exWorkSheet1.Cells["E24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["E24"].Style.WrapText = true;
                                exWorkSheet1.Cells["E24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["E24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["E24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["F23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["F23"].Value = "Transferor's Folio, if applicable";
                                exWorkSheet1.Cells["F23:F24"].Merge = true;
                                //exWorkSheet1.Cells["F23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["F23"].Style.WrapText = true;
                                exWorkSheet1.Cells["F23:F24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F23:F24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["F23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["F23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["G23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["G23"].Value = "Name of the transferor, if applicable";
                                exWorkSheet1.Cells["G23:G24"].Merge = true;
                                //exWorkSheet1.Cells["G23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["G23"].Style.WrapText = true;
                                exWorkSheet1.Cells["G23:G24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G23:G24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["G23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["G23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["H23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["H23"].Value = "Date of issue or endorsement of share certificicate";
                                exWorkSheet1.Cells["H23:H24"].Merge = true;
                                //exWorkSheet1.Cells["H23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["H23"].Style.WrapText = true;
                                exWorkSheet1.Cells["H23:H24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H23:H24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["H23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["H23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["I23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["I23"].Value = "Certificate No.";
                                exWorkSheet1.Cells["I23:I24"].Merge = true;
                                //exWorkSheet1.Cells["I23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["I23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["I23"].Style.WrapText = true;
                                exWorkSheet1.Cells["I23:I24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I23:I24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["I23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["I23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["J23"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["J23"].Value = "Lock in period, if any";
                                exWorkSheet1.Cells["J23:J24"].Merge = true;
                                //exWorkSheet1.Cells["J23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["J23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["J23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["J23"].Style.WrapText = true;
                                exWorkSheet1.Cells["J23:J24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J23:J24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["J23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["J23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["K23"].Value = "Amount (Rs.)";
                                exWorkSheet1.Cells["K23:M23"].Merge = true;
                                //exWorkSheet1.Cells["K23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["K23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["K23"].Style.WrapText = true;
                                exWorkSheet1.Cells["K23:K24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K23:K24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K23:M23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K23:M23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["K23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["K24"].Value = "Payable";

                                //exWorkSheet1.Cells["K24"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["K24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["K24"].Style.WrapText = true;
                                exWorkSheet1.Cells["K24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["K24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["K24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["L24"].Value = "Paid/Deemed to be paid";
                                //exWorkSheet1.Cells["L24"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["L24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["L24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["L24"].Style.WrapText = true;
                                exWorkSheet1.Cells["L24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["L24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["L24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["M24"].Value = "Due";
                                //exWorkSheet1.Cells["M24"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["M24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["M24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["M24"].Style.WrapText = true;
                                exWorkSheet1.Cells["M24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["M24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["M24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["N23"].Value = "If shares are issued for consideration other than cash, brief particulars thereof";
                                exWorkSheet1.Cells["N23:N24"].Merge = true;
                                //exWorkSheet1.Cells["N23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["N23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["N23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["N23"].Style.WrapText = true;
                                exWorkSheet1.Cells["N23:N24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N23:N24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["N23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["N23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["O23"].Value = "Date of  Transfer/ transmission/ Forfeiture/ Redemption etc.";
                                exWorkSheet1.Cells["O23:O24"].Merge = true;
                                //exWorkSheet1.Cells["O23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["O23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["O23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["O23"].Style.WrapText = true;
                                exWorkSheet1.Cells["O23:O24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O23:O24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["O23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["O23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["P23"].Value = "No.of Shares Transferred / transmitted/ forfeited/ redeemed etc";
                                exWorkSheet1.Cells["P23:P24"].Merge = true;
                                //exWorkSheet1.Cells["P23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["P23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["P23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["P23"].Style.WrapText = true;
                                exWorkSheet1.Cells["P23:P24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["P23:P24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["P23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["P23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["P23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["P23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                exWorkSheet1.Cells["Q23"].Value = "Distinctive No.of Shares (both inclusive)";
                                exWorkSheet1.Cells["Q23:R23"].Merge = true;
                                //exWorkSheet1.Cells["Q23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["Q23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["Q23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["Q23"].Style.WrapText = true;
                                exWorkSheet1.Cells["Q23:R23"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q23:R23"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q23:R23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q23:R23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q23:R23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["Q23:R23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["Q24"].Value = "From";
                                //exWorkSheet1.Cells["Q24"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["Q24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["Q24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["Q24"].Style.WrapText = true;
                                exWorkSheet1.Cells["Q24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["Q24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["Q24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["R24"].Value = "To";
                                //exWorkSheet1.Cells["R24"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["R24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["R24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["R24"].Style.WrapText = true;
                                exWorkSheet1.Cells["R24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["R24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["R24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["R24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["R24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["R24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["S23"].Value = "Transferee's folio   ";
                                exWorkSheet1.Cells["S23:S24"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["S23:S24"].Merge = true;
                                //exWorkSheet1.Cells["S23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["S23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["S23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["S23"].Style.WrapText = true;
                                exWorkSheet1.Cells["S23:S24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["S23:S24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["S23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["S23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["S23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["S23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["T23"].Value = "Name of transferee";
                                exWorkSheet1.Cells["T23:T24"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["T23:T24"].Merge = true;
                                //exWorkSheet1.Cells["T23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["T23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["T23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["T23"].Style.WrapText = true;
                                exWorkSheet1.Cells["T23:T24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["T23:T24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["T23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["T23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["T23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["T23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["U23"].Value = "Balance of Shares held ";
                                exWorkSheet1.Cells["U23:U24"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["U23:U24"].Merge = true;
                                //exWorkSheet1.Cells["U23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["U23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["U23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["U23"].Style.WrapText = true;
                                exWorkSheet1.Cells["U23:U24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["U23:U24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["U23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["U23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["U23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["U23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["V23"].Value = "Remarks";
                                //exWorkSheet1.Cells["V23:V24"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["V23:V24"].Merge = true;
                                //exWorkSheet1.Cells["V23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["V23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["V23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["V23"].Style.WrapText = true;
                                exWorkSheet1.Cells["V23:V24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["V23:V24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["V23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["V23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["V23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["V23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                exWorkSheet1.Cells["W23"].Value = "Authentication/ Signature";
                                //exWorkSheet1.Cells["V23:V24"].Style.TextRotation = 90;
                                exWorkSheet1.Cells["W23:W24"].Merge = true;
                                //exWorkSheet1.Cells["W23"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["W23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["W23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                exWorkSheet1.Cells["W23"].Style.WrapText = true;
                                exWorkSheet1.Cells["W23:W24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["W23:W24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["W23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["W23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["W23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells["W23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                #endregion

                                if (ExcelData.Rows.Count > 0)
                                {
                                    exWorkSheet1.Cells["A25"].LoadFromDataTable(ExcelData, false);
                                }

                                for (int i = 1; i <= 24; i++) // this will apply it from col 1 to 10
                                {
                                    exWorkSheet1.Column(i).Width = 12;
                                }
                                using (ExcelRange col = exWorkSheet1.Cells[25, 1, 24 + ExcelData.Rows.Count, 23])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                            }
                        }

                    }

                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    byte[] data = exportPackge.GetAsByteArray();
                    return File(data, "application/octet-stream", "Register of member" + DateTime.Now + ".xlsx");
                }
            }
            else
            {
                return Content("No Data Found");
            }
        }

        public ActionResult viewMemberRegister(long EntityID, int MemberId)
        {
            List<MemberSecurities> MemberRegisterSecurities = new List<MemberSecurities>();
            List<MemberRegister> RegisterDetails = new List<MemberRegister>();
            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            RegisterDetails = objregister.GetMemberofRegisterDetails(EntityID, MemberId, CustomerID);
            if (RegisterDetails.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        foreach (var member in RegisterDetails)
                        {
                            MemberRegisterSecurities = objregister.GetMemberSecurities(EntityID, CustomerID, member.MemberId);
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(member.MemberName);
                            exWorkSheet1.Cells["A3"].Value = "REGISTER OF MEMBERS: FORM MGT-1";

                            exWorkSheet1.Cells["A3:W3"].Merge = true;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:W3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A4"].Value = "Pursuant to section 88(1)(a) of the Companies Act, 2013 and Rule 3(1) of the Companies (Management and Administration) Rules, 2014";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A4:W4"].Merge = true;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:W4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            //exWorkSheet1.Cells["A3"].Style.WrapText = true;

                            exWorkSheet1.Cells["A5"].Value = "";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A5:O5"].Merge = true;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:O5"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //exWorkSheet1.Row(6).Height = 31.25;

                            exWorkSheet1.Cells["A6"].Value = "Name of the Company:";
                            //exWorkSheet1.Cells["A6"].AutoFitColumns(8);
                            //exWorkSheet1.Column(8).Width = 25;
                            exWorkSheet1.Cells["A6:C6"].Merge = true;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:C6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A6"].Style.Font.Bold = true;
                            //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A6"].Style.WrapText = true;

                            exWorkSheet1.Cells["D6"].Value = member.EntityName;
                            exWorkSheet1.Cells["D6:I6"].Merge = true;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6:I6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["D6"].Style.WrapText = false;
                            //exWorkSheet1.Cells["C6"].AutoFitColumns(8);

                            exWorkSheet1.Cells["J6"].Value = "Registered Office Address:";
                            exWorkSheet1.Cells["J6:L6"].Merge = true;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6:L6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["J6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["J6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["J6"].Style.WrapText = true;
                            exWorkSheet1.Cells["J6"].Style.Font.Bold = true;


                            exWorkSheet1.Cells["M6"].Value = member.EntityAddress;
                            exWorkSheet1.Cells["M6:W6"].Merge = true;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6:W6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["M6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["M6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["M6"].Style.WrapText = false;

                            exWorkSheet1.Cells["A7"].Value = "Folio No.:";
                            exWorkSheet1.Cells["A7:D7"].Merge = true;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:D7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A7"].Style.WrapText = true;

                            exWorkSheet1.Cells["E7"].Value = member.FolioNumber;
                            //exWorkSheet1.Cells["E7"].Merge = true;
                            exWorkSheet1.Cells["E7"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E7"].Style.WrapText = true;

                            exWorkSheet1.Cells["G7"].Value = "Personal Details";
                            exWorkSheet1.Cells["G7"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G7:P8"].Merge = true;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7:P8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G7"].Style.WrapText = true;

                            exWorkSheet1.Cells["R7"].Value = "Details of Membership";
                            exWorkSheet1.Cells["R7"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R7:W8"].Merge = true;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7:W8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R7"].Style.WrapText = true;

                            exWorkSheet1.Cells["A8"].Value = "Class of Shares:";
                            exWorkSheet1.Cells["A8:D8"].Merge = true;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8:D8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A8"].Style.WrapText = true;

                            exWorkSheet1.Cells["E8"].Value = member.ClassofShares;
                            exWorkSheet1.Cells["E8"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E8"].Style.WrapText = true;

                            exWorkSheet1.Cells["A9"].Value = "Nominal value per share (in Rs.):";
                            exWorkSheet1.Cells["A9:D9"].Merge = true;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9:D9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A9"].Style.WrapText = true;

                            exWorkSheet1.Cells["E9"].Value = member.Nominalvaluepershares;
                            exWorkSheet1.Cells["E9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E9"].Style.WrapText = true;

                            exWorkSheet1.Cells["A10"].Value = "Total shares held";
                            exWorkSheet1.Cells["A10:D10"].Merge = true;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10:D10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["A10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A10"].Style.WrapText = true;

                            exWorkSheet1.Cells["E10"].Value = member.TotalSharesHeld;
                            exWorkSheet1.Cells["E10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["E10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["E10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["E10"].Style.WrapText = true;

                            exWorkSheet1.Cells["G9"].Value = "Name of the Member:";
                            exWorkSheet1.Cells["G9:J9"].Merge = true;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9:J9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G9"].Style.WrapText = true;

                            exWorkSheet1.Cells["K9"].Value = member.MemberName;
                            exWorkSheet1.Cells["K9:P9"].Merge = true;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9:P9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K9"].Style.WrapText = true;

                            exWorkSheet1.Cells["G10"].Value = "Name of joint holders, if any";
                            exWorkSheet1.Cells["G10:J10"].Merge = true;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10:J10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G10"].Style.WrapText = true;

                            exWorkSheet1.Cells["K10"].Value = member.JointHolderName;
                            exWorkSheet1.Cells["K10:P10"].Merge = true;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10:P10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K10"].Style.WrapText = true;

                            exWorkSheet1.Cells["G11"].Value = "Address/Registered address (in case of body corporate)";
                            exWorkSheet1.Row(11).Height = 31.25;
                            exWorkSheet1.Cells["G11:J11"].Merge = true;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11:J11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G11"].Style.WrapText = true;

                            exWorkSheet1.Cells["K11"].Value = member.Address;
                            exWorkSheet1.Cells["K11:P11"].Merge = true;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11:P11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K11"].Style.WrapText = true;

                            exWorkSheet1.Cells["G12"].Value = "Email ID:";
                            exWorkSheet1.Cells["G12:J12"].Merge = true;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12:J12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G12"].Style.WrapText = true;

                            exWorkSheet1.Cells["K12"].Value = member.EmailId;
                            exWorkSheet1.Cells["K12:P12"].Merge = true;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12:P12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K12"].Style.WrapText = true;

                            exWorkSheet1.Cells["G13"].Value = "CIN/ Registration No.:";
                            exWorkSheet1.Cells["G13:J13"].Merge = true;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13:J13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G13"].Style.WrapText = true;

                            exWorkSheet1.Cells["K13"].Value = member.CIN;
                            exWorkSheet1.Cells["K13:P13"].Merge = true;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13:P13"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K13"].Style.WrapText = true;

                            exWorkSheet1.Cells["G14"].Value = "Unique Identification Number:";
                            exWorkSheet1.Cells["G14:J14"].Merge = true;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14:J14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G14"].Style.WrapText = true;

                            exWorkSheet1.Cells["K14"].Value = member.UIN;
                            exWorkSheet1.Cells["K14:P14"].Merge = true;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14:P14"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K14"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K14"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K14"].Style.WrapText = true;

                            exWorkSheet1.Cells["G15"].Value = "Father's/Mother's/Spouse's Name:";
                            exWorkSheet1.Cells["G15:J15"].Merge = true;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15:J15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G15"].Style.WrapText = true;

                            exWorkSheet1.Cells["K15"].Value = member.Father_Mother_SpouseName;
                            exWorkSheet1.Cells["K15:P15"].Merge = true;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15:P15"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K15"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K15"].Style.WrapText = true;

                            exWorkSheet1.Cells["G16"].Value = "Status:";
                            exWorkSheet1.Cells["G16"].Merge = true;
                            exWorkSheet1.Cells["G16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G16"].Style.WrapText = true;

                            exWorkSheet1.Cells["H16"].Value = member.Status;
                            exWorkSheet1.Cells["H16:J16"].Merge = true;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16:J16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["H16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H16"].Style.WrapText = true;

                            exWorkSheet1.Cells["K16"].Value = "Occupation:";
                            exWorkSheet1.Cells["K16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K16"].Style.WrapText = true;

                            exWorkSheet1.Cells["L16"].Value = member.Occupation;
                            exWorkSheet1.Cells["L16:P16"].Merge = true;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16:P16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["L16"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["L16"].Style.WrapText = true;

                            exWorkSheet1.Cells["G17"].Value = "PAN:";
                            exWorkSheet1.Cells["G17"].Merge = true;
                            exWorkSheet1.Cells["G17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G17"].Style.WrapText = true;

                            exWorkSheet1.Cells["H17"].Value = member.PAN;
                            exWorkSheet1.Cells["H17:J17"].Merge = true;
                            exWorkSheet1.Cells["H17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["H17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["H17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H17"].Style.WrapText = true;

                            exWorkSheet1.Cells["K17"].Value = "Nationality:";
                            exWorkSheet1.Cells["K17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K17"].Style.WrapText = true;


                            exWorkSheet1.Cells["L17"].Value = member.Nationality;
                            exWorkSheet1.Cells["L17:P17"].Merge = true;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17:P17"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["L17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["L17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["L17"].Style.WrapText = true;

                            exWorkSheet1.Cells["G18"].Value = "";
                            exWorkSheet1.Cells["G18:P18"].Merge = true;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18:P18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G18"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G18"].Style.WrapText = true;

                            exWorkSheet1.Cells["G19"].Value = "Name of Guardian:";
                            exWorkSheet1.Cells["G19:J19"].Merge = true;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19:J19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G19"].Style.WrapText = true;

                            exWorkSheet1.Cells["K19"].Value = member.GuardianName;
                            exWorkSheet1.Cells["K19:P19"].Merge = true;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19:P19"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K19"].Style.WrapText = true;

                            exWorkSheet1.Cells["G20"].Value = "Date of birth of minor:";
                            exWorkSheet1.Cells["G20:J20"].Merge = true;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20:J20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["G20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["G20"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G20"].Style.WrapText = true;

                            exWorkSheet1.Cells["K20"].Value = member.DateofBirth;
                            exWorkSheet1.Cells["K20:P20"].Merge = true;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20:P20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["K20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["K20"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["K20"].Style.WrapText = true;

                            exWorkSheet1.Cells["R9"].Value = "Date of Becoming member:";
                            exWorkSheet1.Cells["R9:S9"].Merge = true;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9:S9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R9"].Style.WrapText = true;

                            exWorkSheet1.Cells["T9"].Value = member.DateofBecomingMember;
                            exWorkSheet1.Cells["T9:W9"].Merge = true;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9:W9"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T9"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T9"].Style.WrapText = true;

                            exWorkSheet1.Cells["R10"].Value = "Date of declaration under section 89, if applicable:";
                            exWorkSheet1.Cells["R10:S10"].Merge = true;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10:S10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R10"].Style.WrapText = true;

                            exWorkSheet1.Cells["T10"].Value = member.Dateunder89;
                            exWorkSheet1.Cells["T10:W10"].Merge = true;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10:W10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T10"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T10"].Style.WrapText = true;

                            exWorkSheet1.Cells["R11"].Value = "Name and address of beneficial owner:";
                            exWorkSheet1.Cells["R11:S11"].Merge = true;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11:S11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R11"].Style.WrapText = true;

                            exWorkSheet1.Cells["T11"].Value = member.BenificialNameandAddress;
                            exWorkSheet1.Cells["T11:W11"].Merge = true;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11:W11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T11"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T11"].Style.WrapText = true;

                            exWorkSheet1.Cells["R12"].Value = "Date of receipt of nomination, if applicable:";
                            exWorkSheet1.Cells["R12:S12"].Merge = true;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12:S12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R12"].Style.WrapText = true;

                            exWorkSheet1.Cells["T12"].Value = member.DateofNomination;
                            exWorkSheet1.Cells["T12:W12"].Merge = true;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12:W12"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T12"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T12"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T12"].Style.WrapText = true;

                            exWorkSheet1.Cells["R13"].Value = "Name and address of nominee:";
                            exWorkSheet1.Cells["R13:S16"].Merge = true;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13:S16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R13"].Style.WrapText = true;

                            exWorkSheet1.Cells["T13"].Value = member.NameandAddressofNomminee;
                            exWorkSheet1.Cells["T13:W16"].Merge = true;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13:W16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T13"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T13"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T13"].Style.WrapText = true;

                            exWorkSheet1.Cells["R17"].Value = "No. of shares kept in abeyance, if applicable:";
                            exWorkSheet1.Cells["R17:S18"].Merge = true;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17:S18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R17"].Style.WrapText = true;

                            exWorkSheet1.Cells["T17"].Value = member.sharesinAbayes;
                            exWorkSheet1.Cells["T17:W18"].Merge = true;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17:W18"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T17"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T17"].Style.WrapText = true;

                            exWorkSheet1.Cells["R19"].Value = "Date of cessation of membership:";
                            exWorkSheet1.Cells["R19:S20"].Merge = true;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19:S20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["R19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["R19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["R19"].Style.WrapText = true;

                            exWorkSheet1.Cells["T19"].Value = member.DateofCessation;
                            exWorkSheet1.Cells["T19:W20"].Merge = true;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19:W20"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["T19"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            exWorkSheet1.Cells["T19"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["T19"].Style.WrapText = true;

                            if (MemberRegisterSecurities != null)
                            {
                                if (MemberRegisterSecurities.Count > 0)
                                {
                                    #region Member Securities

                                    DataView view1 = new DataView();
                                    DataTable table1 = new DataTable();
                                    int P = 0;

                                    table1.Columns.Add("AllotmentNo", typeof(string));
                                    table1.Columns.Add("DateofAllotment", typeof(string));
                                    table1.Columns.Add("NumberofShares", typeof(int));
                                    table1.Columns.Add("DistinctiveNoofShares_From", typeof(string));
                                    table1.Columns.Add("DistinctiveNoofShares_To", typeof(string));
                                    table1.Columns.Add("TransferorFolioNo", typeof(string));
                                    table1.Columns.Add("NameOfTransferror", typeof(string));
                                    table1.Columns.Add("DateofIssues", typeof(string));
                                    table1.Columns.Add("CertificateNumber", typeof(string));
                                    table1.Columns.Add("LockinPeriod", typeof(string));
                                    table1.Columns.Add("AmountPayable", typeof(decimal));
                                    table1.Columns.Add("AmountPaid", typeof(decimal));
                                    table1.Columns.Add("AmountDue", typeof(decimal));
                                    table1.Columns.Add("DescofConsidration", typeof(string));
                                    table1.Columns.Add("DateofTransfer", typeof(string));
                                    table1.Columns.Add("NoofSharesTransfered", typeof(int));
                                    table1.Columns.Add("DistinctiveNo_Form", typeof(string));
                                    table1.Columns.Add("DistinctiveNo_To", typeof(string));
                                    table1.Columns.Add("TransfreeFolio", typeof(string));
                                    table1.Columns.Add("NameofTransfaree", typeof(string));
                                    table1.Columns.Add("BalanceofSharesHeld", typeof(int));
                                    table1.Columns.Add("Remark", typeof(string));
                                    table1.Columns.Add("Authentication", typeof(string));


                                    foreach (var item in MemberRegisterSecurities)
                                    {

                                        table1.Rows.Add(item.AllotmentNo, item.DateofAllotment, item.NumberofShares, item.DistinctiveNoofShares_From, item.DistinctiveNoofShares_To, item.TransferorFolioNo, item.NameOfTransferror, item.DateofIssues, item.CertificateNumber, item.LockinPeriod, item.AmountPayable, item.AmountPaid, item.AmountDue, item.DescofConsidration, item.DateofTransfer, item.NoofSharesTransfered, item.DistinctiveNo_Form, item.DistinctiveNo_To, item.TransfreeFolio, item.NameofTransfaree, item.BalanceofSharesHeld, item.Remark, item.Authentication);
                                    }

                                    view1 = new System.Data.DataView(table1);

                                    DataTable ExcelData = null;

                                    ExcelData = view1.ToTable("Selected", false, "AllotmentNo", "DateofAllotment", "NumberofShares", "DistinctiveNoofShares_From", "DistinctiveNoofShares_To", "TransferorFolioNo", "NameOfTransferror", "DateofIssues", "CertificateNumber", "LockinPeriod", "AmountPayable", "AmountPaid", "AmountDue", "DescofConsidration", "DateofTransfer", "NoofSharesTransfered", "DistinctiveNo_Form", "DistinctiveNo_To", "TransfreeFolio", "NameofTransfaree", "BalanceofSharesHeld", "Remark", "Authentication");


                                    exWorkSheet1.Row(23).Height = 54;

                                    exWorkSheet1.Cells["A23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["A23"].Value = "Allotment No./ Transfer No.";
                                    exWorkSheet1.Cells["A23:A24"].Merge = true;
                                    //exWorkSheet1.Cells["A23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["A23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["A23:A24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["A23:A24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["A23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["A23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["A23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["A23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["B23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["B23"].Value = "Date of Allotment or Entry of Transfer";
                                    exWorkSheet1.Cells["B23:B24"].Merge = true;
                                    //exWorkSheet1.Cells["B23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["B23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["B23:B24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B23:B24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["B23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["C23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["C23"].Value = "No. of Shares allotted or transferred";
                                    exWorkSheet1.Cells["C23:C24"].Merge = true;
                                    //exWorkSheet1.Cells["C23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["C23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["C23:C24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C23:C24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["C23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    //exWorkSheet1.Cells["D23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["D23"].Value = "Distinctive No.of Shares  (both inclusive)  ";
                                    exWorkSheet1.Cells["D23:E23"].Merge = true;
                                    //exWorkSheet1.Cells["D23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["D23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["D23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["D23:D24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D23:D24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D23:E23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D23:E23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["D23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["D24"].Value = "From";
                                    exWorkSheet1.Cells["D24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["D24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["D24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["D24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["E24"].Value = "To";
                                    exWorkSheet1.Cells["E24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["E24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["E24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["E24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["E24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["E24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["E24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["E24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["E24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["F23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["F23"].Value = "Transferor's Folio, if applicable";
                                    exWorkSheet1.Cells["F23:F24"].Merge = true;
                                    //exWorkSheet1.Cells["F23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["F23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["F23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["F23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["F23:F24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["F23:F24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["F23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["F23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["F23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["F23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["G23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["G23"].Value = "Name of the transferor, if applicable";
                                    exWorkSheet1.Cells["G23:G24"].Merge = true;
                                    //exWorkSheet1.Cells["G23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["G23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["G23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["G23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["G23:G24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["G23:G24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["G23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["G23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["G23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["G23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["H23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["H23"].Value = "Date of issue or endorsement of share certificicate";
                                    exWorkSheet1.Cells["H23:H24"].Merge = true;
                                    //exWorkSheet1.Cells["H23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["H23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["H23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["H23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["H23:H24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["H23:H24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["H23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["H23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["H23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["H23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["I23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["I23"].Value = "Certificate No.";
                                    exWorkSheet1.Cells["I23:I24"].Merge = true;
                                    //exWorkSheet1.Cells["I23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["I23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["I23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["I23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["I23:I24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["I23:I24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["I23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["I23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["I23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["I23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["J23"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["J23"].Value = "Lock in period, if any";
                                    exWorkSheet1.Cells["J23:J24"].Merge = true;
                                    //exWorkSheet1.Cells["J23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["J23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["J23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["J23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["J23:J24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["J23:J24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["J23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["J23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["J23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["J23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["K23"].Value = "Amount (Rs.)";
                                    exWorkSheet1.Cells["K23:M23"].Merge = true;
                                    //exWorkSheet1.Cells["K23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["K23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["K23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["K23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["K23:K24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K23:K24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K23:M23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K23:M23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["K23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["K24"].Value = "Payable";

                                    //exWorkSheet1.Cells["K24"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["K24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["K24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["K24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["K24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["K24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["K24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["L24"].Value = "Paid/Deemed to be paid";
                                    //exWorkSheet1.Cells["L24"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["L24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["L24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["L24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["L24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["L24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["L24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["L24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["L24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["L24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["M24"].Value = "Due";
                                    //exWorkSheet1.Cells["M24"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["M24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["M24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["M24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["M24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["M24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["M24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["M24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["M24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["M24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["N23"].Value = "If shares are issued for consideration other than cash, brief particulars thereof";
                                    exWorkSheet1.Cells["N23:N24"].Merge = true;
                                    //exWorkSheet1.Cells["N23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["N23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["N23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["N23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["N23:N24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["N23:N24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["N23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["N23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["N23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["N23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["O23"].Value = "Date of  Transfer/ transmission/ Forfeiture/ Redemption etc.";
                                    exWorkSheet1.Cells["O23:O24"].Merge = true;
                                    //exWorkSheet1.Cells["O23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["O23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["O23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["O23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["O23:O24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["O23:O24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["O23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["O23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["O23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["O23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["P23"].Value = "No.of Shares Transferred / transmitted/ forfeited/ redeemed etc";
                                    exWorkSheet1.Cells["P23:P24"].Merge = true;
                                    //exWorkSheet1.Cells["P23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["P23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["P23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["P23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["P23:P24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["P23:P24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["P23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["P23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["P23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["P23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                                    exWorkSheet1.Cells["Q23"].Value = "Distinctive No.of Shares (both inclusive)";
                                    exWorkSheet1.Cells["Q23:R23"].Merge = true;
                                    //exWorkSheet1.Cells["Q23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["Q23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["Q23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["Q23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["Q23:R23"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q23:R23"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q23:R23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q23:R23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q23:R23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["Q23:R23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["Q24"].Value = "From";
                                    //exWorkSheet1.Cells["Q24"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["Q24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["Q24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["Q24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["Q24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["Q24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["Q24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["R24"].Value = "To";
                                    //exWorkSheet1.Cells["R24"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["R24"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["R24"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["R24"].Style.WrapText = true;
                                    exWorkSheet1.Cells["R24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["R24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["R24"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["R24"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["R24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["R24"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["S23"].Value = "Transferee's folio   ";
                                    exWorkSheet1.Cells["S23:S24"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["S23:S24"].Merge = true;
                                    //exWorkSheet1.Cells["S23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["S23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["S23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["S23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["S23:S24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["S23:S24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["S23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["S23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["S23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["S23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["T23"].Value = "Name of transferee";
                                    exWorkSheet1.Cells["T23:T24"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["T23:T24"].Merge = true;
                                    //exWorkSheet1.Cells["T23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["T23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["T23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["T23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["T23:T24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["T23:T24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["T23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["T23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["T23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["T23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["U23"].Value = "Balance of Shares held ";
                                    exWorkSheet1.Cells["U23:U24"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["U23:U24"].Merge = true;
                                    //exWorkSheet1.Cells["U23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["U23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["U23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["U23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["U23:U24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["U23:U24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["U23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["U23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["U23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["U23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["V23"].Value = "Remarks";
                                    //exWorkSheet1.Cells["V23:V24"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["V23:V24"].Merge = true;
                                    //exWorkSheet1.Cells["V23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["V23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["V23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["V23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["V23:V24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["V23:V24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["V23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["V23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["V23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["V23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                    exWorkSheet1.Cells["W23"].Value = "Authentication/ Signature";
                                    //exWorkSheet1.Cells["V23:V24"].Style.TextRotation = 90;
                                    exWorkSheet1.Cells["W23:W24"].Merge = true;
                                    //exWorkSheet1.Cells["W23"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["W23"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["W23"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["W23"].Style.WrapText = true;
                                    exWorkSheet1.Cells["W23:W24"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["W23:W24"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["W23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["W23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["W23"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["W23"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    #endregion

                                    if (ExcelData.Rows.Count > 0)
                                    {
                                        exWorkSheet1.Cells["A25"].LoadFromDataTable(ExcelData, false);
                                    }

                                    for (int i = 1; i <= 24; i++) // this will apply it from col 1 to 10
                                    {
                                        exWorkSheet1.Column(i).Width = 12;
                                    }
                                    using (ExcelRange col = exWorkSheet1.Cells[25, 1, 24 + ExcelData.Rows.Count, 23])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }
                            }
                        }



                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    CustomerID = Convert.ToInt16(AuthenticationHelper.CustomerID);
                    path = "~/Temp/" + CustomerID + "/";
                    Filename = "Register_of_Member.xlsx";
                    if (!Directory.Exists(Server.MapPath(path)))
                    {
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    _path = System.IO.Path.Combine(Server.MapPath(path), Filename);
                    FileStream aFile = new FileStream(_path, FileMode.Create);
                    byte[] byData = exportPackge.GetAsByteArray();
                    aFile.Seek(0, SeekOrigin.Begin);
                    aFile.Write(byData, 0, byData.Length);
                    aFile.Close();
                }

                return Json(Filename, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Register of Charge 28 Jul 2021

        public ActionResult viewChargeRegister(long EntityID, int ChargeId)
        {
            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            var RegisterDetails = objregister.GetChargeRegisterDetails(EntityID, ChargeId, CustomerID);

            if (RegisterDetails.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        #region Charge Register
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("ChargeRegister");

                        if (RegisterDetails.Count > 0)
                        {
                            DataView view1 = new DataView();
                            DataTable table1 = new DataTable();

                            table1.Columns.Add("SrNo", typeof(int));
                            table1.Columns.Add("ChargeID", typeof(string));
                            table1.Columns.Add("CreationDate");
                            table1.Columns.Add("DateOfRegistrationCreationCharge");
                            table1.Columns.Add("ShortDescription");
                            table1.Columns.Add("Amount");
                            table1.Columns.Add("NameOfHolder");
                            table1.Columns.Add("PurticularsOfTerm");

                            table1.Columns.Add("DescriptionOfInstrument");
                            table1.Columns.Add("DateOfModification");
                            table1.Columns.Add("DateOfRegistration");
                            table1.Columns.Add("DescOfIntrumentModification");
                            table1.Columns.Add("PurticularsOfModification");

                            table1.Columns.Add("DateOfSatisfication");
                            table1.Columns.Add("DateOfRegistrationSatisfication");
                            table1.Columns.Add("DateOfDelay");
                            table1.Columns.Add("ReasonForDelay");

                            var SrNo = 0;
                            foreach (var item in RegisterDetails)
                            {
                                SrNo++;
                                //table1.Rows.Add(SrNo, item.ChargeIdMannual, item.CreationDate, item.RegistrationChargeCreateDate, item.ShortDescPropertyCharged, item.ChargeAmount,
                                //   item.Namesaddresseschargeholder, item.TermsconditionsOfcharge, item.Descinstrument, item.ModificationDate, item.RegistrationModificationDate, item.Descinstrumentchargemodify,
                                //   item.Particularsmodification, item.SatisfactionDate, item.RegistrationsatisfactionDate, item.FactsDelaycondonationDate, item.Reasonsdelayfiling);

                                table1.Rows.Add(SrNo, item.ChargeIdMannual, item.CreationDate.ToString("dd/MM/yyyy"),
                                   (item.RegistrationChargeCreateDate == null ? "" : Convert.ToDateTime(item.RegistrationChargeCreateDate).ToString("dd/MM/yyyy")),
                                   item.ShortDescPropertyCharged, item.ChargeAmount,
                                   item.Namesaddresseschargeholder, item.TermsconditionsOfcharge, item.Descinstrument,
                                   (item.ModificationDate == null ? "" : Convert.ToDateTime(item.ModificationDate).ToString("dd/MM/yyyy")),
                                   (item.RegistrationModificationDate == null ? "" : Convert.ToDateTime(item.RegistrationModificationDate).ToString("dd/MM/yyyy")),
                                   item.Descinstrumentchargemodify, item.Particularsmodification,
                                   (item.SatisfactionDate == null ? "" : Convert.ToDateTime(item.SatisfactionDate).ToString("dd/MM/yyyy")),
                                   (item.RegistrationsatisfactionDate == null ? "" : Convert.ToDateTime(item.RegistrationsatisfactionDate).ToString("dd/MM/yyyy")),
                                   (item.FactsDelaycondonationDate == null ? "" : Convert.ToDateTime(item.FactsDelaycondonationDate).ToString("dd/MM/yyyy")),
                                   item.Reasonsdelayfiling);
                            }

                            view1 = new System.Data.DataView(table1);
                            DataTable ExcelData = null;

                            ExcelData = view1.ToTable("Selected", false, "SrNo", "ChargeID", "CreationDate", "DateOfRegistrationCreationCharge", "ShortDescription", "Amount", "NameOfHolder", "PurticularsOfTerm", "DescriptionOfInstrument", "DateOfModification", "DateOfRegistration", "DescOfIntrumentModification", "PurticularsOfModification", "DateOfSatisfication", "DateOfRegistrationSatisfication", "DateOfDelay", "ReasonForDelay");

                            exWorkSheet1.Cells["A1"].Value = "Form No. CHG-7";
                            exWorkSheet1.Cells["A1:Q1"].Merge = true;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);


                            exWorkSheet1.Cells["A2"].Value = "Register of charges";
                            exWorkSheet1.Cells["A2:Q2"].Merge = true;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A2"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A3"].Value = "[Pursuant to section 85 sub-rule (1) of rule 10 of the Companies (Registration of Charges) Rules, 2014]";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A3:Q3"].Merge = true;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Row(4).Height = 50;

                            exWorkSheet1.Cells["A4"].Value = "S.No";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A4"].Style.WrapText = true;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["B4"].Value = "Charge ID";
                            exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["B4"].Style.WrapText = true;
                            exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["C4"].Value = "Date of creation of charge or date of acquisition of property subject to charge";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["C4"].Style.WrapText = true;
                            exWorkSheet1.Cells["C4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["D4"].Value = "Date of registration of creation of charge";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D4"].Style.WrapText = true;
                            exWorkSheet1.Cells["D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["E4"].Value = "Short description of the property charged";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["E4"].Style.WrapText = true;
                            exWorkSheet1.Cells["E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["F4"].Value = "Period and amount secured by the charge";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["F4"].Style.WrapText = true;
                            exWorkSheet1.Cells["F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["G4"].Value = "Names and addresses of the charge holder";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["G4"].Style.WrapText = true;
                            exWorkSheet1.Cells["G4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["H4"].Value = "Particulars of the terms and conditions of the charge";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["H4"].Style.WrapText = true;
                            exWorkSheet1.Cells["H4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["I4"].Value = "Description of the instrument creating the charge";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["I4"].Style.WrapText = true;
                            exWorkSheet1.Cells["I4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["J4"].Value = "Date of modification of charge";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J4"].Style.WrapText = true;
                            exWorkSheet1.Cells["J4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["K4"].Value = "Date of registration of modification of charge";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["K4"].Style.WrapText = true;
                            exWorkSheet1.Cells["K4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["L4"].Value = "Description of the instrument modifying the charge";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["L4"].Style.WrapText = true;
                            exWorkSheet1.Cells["L4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["L4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["M4"].Value = "Particulars of modification";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M4"].Style.WrapText = true;
                            exWorkSheet1.Cells["M4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["M4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["N4"].Value = "Date of satisfaction";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["N4"].Style.WrapText = true;
                            exWorkSheet1.Cells["N4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["N4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["O4"].Value = "Date of registration of satisfaction";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["O4"].Style.WrapText = true;
                            exWorkSheet1.Cells["O4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["O4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["P4"].Value = "Facts and date of condonation of delay, if any";
                            exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["P4"].Style.WrapText = true;
                            exWorkSheet1.Cells["P4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["P4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["Q4"].Value = "Reasons for delay in filing for registration of creation, modification or satisfaction of the charge, if any";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["Q4"].Style.WrapText = true;
                            exWorkSheet1.Cells["Q4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Q4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            {
                                exWorkSheet1.Cells[5, j].Value = string.Format("({0})", j);
                                exWorkSheet1.Cells[5, j].Style.Font.Bold = true;
                                exWorkSheet1.Cells[5, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[5, j].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet1.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            //for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            //{
                            //    exWorkSheet1.Column(j).Width = 12;
                            //}

                            exWorkSheet1.Column(5).Width = 40;
                            exWorkSheet1.Column(7).Width = 30;
                            exWorkSheet1.Column(8).Width = 30;
                            exWorkSheet1.Column(9).Width = 30;
                            exWorkSheet1.Column(12).Width = 30;
                            exWorkSheet1.Column(13).Width = 30;

                            exWorkSheet1.Column(3).Width = 13.5;
                            exWorkSheet1.Column(4).Width = 13.5;
                            exWorkSheet1.Column(10).Width = 13.5;
                            exWorkSheet1.Column(11).Width = 13.5;
                            exWorkSheet1.Column(14).Width = 13.5;
                            exWorkSheet1.Column(15).Width = 13.5;

                            if (ExcelData.Rows.Count > 0)
                            {
                                using (ExcelRange col = exWorkSheet1.Cells[4, 1, 5 + ExcelData.Rows.Count, 17])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    CustomerID = Convert.ToInt16(AuthenticationHelper.CustomerID);
                    path = "~/Temp/" + CustomerID + "/";
                    Filename = "Register_of_Charge.xlsx";
                    if (!Directory.Exists(Server.MapPath(path)))
                    {
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    _path = System.IO.Path.Combine(Server.MapPath(path), Filename);
                    FileStream aFile = new FileStream(_path, FileMode.Create);
                    byte[] byData = exportPackge.GetAsByteArray();
                    aFile.Seek(0, SeekOrigin.Begin);
                    aFile.Write(byData, 0, byData.Length);
                    aFile.Close();
                }

                return Json(Filename, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportChargeRegister(long EntityID, int ChargeId)
        {
            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            var RegisterDetails = objregister.GetChargeRegisterDetails(EntityID, ChargeId, CustomerID);

            if (RegisterDetails.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        #region Charge Register
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("ChargeRegister");

                        if (RegisterDetails.Count > 0)
                        {
                            DataView view1 = new DataView();
                            DataTable table1 = new DataTable();

                            table1.Columns.Add("SrNo", typeof(int));
                            table1.Columns.Add("ChargeID", typeof(string));
                            table1.Columns.Add("CreationDate");
                            table1.Columns.Add("DateOfRegistrationCreationCharge");
                            table1.Columns.Add("ShortDescription");
                            table1.Columns.Add("Amount");
                            table1.Columns.Add("NameOfHolder");
                            table1.Columns.Add("PurticularsOfTerm");

                            table1.Columns.Add("DescriptionOfInstrument");
                            table1.Columns.Add("DateOfModification");
                            table1.Columns.Add("DateOfRegistration");
                            table1.Columns.Add("DescOfIntrumentModification");
                            table1.Columns.Add("PurticularsOfModification");

                            table1.Columns.Add("DateOfSatisfication");
                            table1.Columns.Add("DateOfRegistrationSatisfication");
                            table1.Columns.Add("DateOfDelay");
                            table1.Columns.Add("ReasonForDelay");

                            var SrNo = 0;
                            foreach (var item in RegisterDetails)
                            {
                                SrNo++;
                                table1.Rows.Add(SrNo, item.ChargeIdMannual, item.CreationDate.ToString("dd/MM/yyyy"),
                                   (item.RegistrationChargeCreateDate == null ? "" : Convert.ToDateTime(item.RegistrationChargeCreateDate).ToString("dd/MM/yyyy")),
                                   item.ShortDescPropertyCharged, item.ChargeAmount,
                                   item.Namesaddresseschargeholder, item.TermsconditionsOfcharge, item.Descinstrument,
                                   (item.ModificationDate == null ? "" : Convert.ToDateTime(item.ModificationDate).ToString("dd/MM/yyyy")),
                                   (item.RegistrationModificationDate == null ? "" : Convert.ToDateTime(item.RegistrationModificationDate).ToString("dd/MM/yyyy")),
                                   item.Descinstrumentchargemodify, item.Particularsmodification,
                                   (item.SatisfactionDate == null ? "" : Convert.ToDateTime(item.SatisfactionDate).ToString("dd/MM/yyyy")),
                                   (item.RegistrationsatisfactionDate == null ? "" : Convert.ToDateTime(item.RegistrationsatisfactionDate).ToString("dd/MM/yyyy")),
                                   (item.FactsDelaycondonationDate == null ? "" : Convert.ToDateTime(item.FactsDelaycondonationDate).ToString("dd/MM/yyyy")),
                                   item.Reasonsdelayfiling);
                            }

                            view1 = new System.Data.DataView(table1);
                            DataTable ExcelData = null;

                            ExcelData = view1.ToTable("Selected", false, "SrNo", "ChargeID", "CreationDate", "DateOfRegistrationCreationCharge", "ShortDescription", "Amount", "NameOfHolder", "PurticularsOfTerm", "DescriptionOfInstrument", "DateOfModification", "DateOfRegistration", "DescOfIntrumentModification", "PurticularsOfModification", "DateOfSatisfication", "DateOfRegistrationSatisfication", "DateOfDelay", "ReasonForDelay");

                            exWorkSheet1.Cells["A1"].Value = "Form No. CHG-7";
                            exWorkSheet1.Cells["A1:Q1"].Merge = true;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);


                            exWorkSheet1.Cells["A2"].Value = "Register of charges";
                            exWorkSheet1.Cells["A2:Q2"].Merge = true;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A2"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A3"].Value = "[Pursuant to section 85 sub-rule (1) of rule 10 of the Companies (Registration of Charges) Rules, 2014]";
                            //exWorkSheet1.Cells["A3"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A3:Q3"].Merge = true;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Row(4).Height = 50;

                            exWorkSheet1.Cells["A4"].Value = "S.No";
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A4"].Style.WrapText = true;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["B4"].Value = "Charge ID";
                            exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["B4"].Style.WrapText = true;
                            exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["C4"].Value = "Date of creation of charge or date of acquisition of property subject to charge";
                            exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["C4"].Style.WrapText = true;
                            exWorkSheet1.Cells["C4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["D4"].Value = "Date of registration of creation of charge";
                            exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D4"].Style.WrapText = true;
                            exWorkSheet1.Cells["D4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["E4"].Value = "Short description of the property charged";
                            exWorkSheet1.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["E4"].Style.WrapText = true;
                            exWorkSheet1.Cells["E4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["F4"].Value = "Period and amount secured by the charge";
                            exWorkSheet1.Cells["F4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["F4"].Style.WrapText = true;
                            exWorkSheet1.Cells["F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["G4"].Value = "Names and addresses of the charge holder";
                            exWorkSheet1.Cells["G4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["G4"].Style.WrapText = true;
                            exWorkSheet1.Cells["G4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["H4"].Value = "Particulars of the terms and conditions of the charge";
                            exWorkSheet1.Cells["H4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["H4"].Style.WrapText = true;
                            exWorkSheet1.Cells["H4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["I4"].Value = "Description of the instrument creating the charge";
                            exWorkSheet1.Cells["I4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["I4"].Style.WrapText = true;
                            exWorkSheet1.Cells["I4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["J4"].Value = "Date of modification of charge";
                            exWorkSheet1.Cells["J4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J4"].Style.WrapText = true;
                            exWorkSheet1.Cells["J4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["K4"].Value = "Date of registration of modification of charge";
                            exWorkSheet1.Cells["K4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["K4"].Style.WrapText = true;
                            exWorkSheet1.Cells["K4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["L4"].Value = "Description of the instrument modifying the charge";
                            exWorkSheet1.Cells["L4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["L4"].Style.WrapText = true;
                            exWorkSheet1.Cells["L4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["L4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["M4"].Value = "Particulars of modification";
                            exWorkSheet1.Cells["M4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M4"].Style.WrapText = true;
                            exWorkSheet1.Cells["M4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["M4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["N4"].Value = "Date of satisfaction";
                            exWorkSheet1.Cells["N4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["N4"].Style.WrapText = true;
                            exWorkSheet1.Cells["N4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["N4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["O4"].Value = "Date of registration of satisfaction";
                            exWorkSheet1.Cells["O4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["O4"].Style.WrapText = true;
                            exWorkSheet1.Cells["O4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["O4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["P4"].Value = "Facts and date of condonation of delay, if any";
                            exWorkSheet1.Cells["P4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["P4"].Style.WrapText = true;
                            exWorkSheet1.Cells["P4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["P4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["Q4"].Value = "Reasons for delay in filing for registration of creation, modification or satisfaction of the charge, if any";
                            exWorkSheet1.Cells["Q4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["Q4"].Style.WrapText = true;
                            exWorkSheet1.Cells["Q4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Q4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            {
                                exWorkSheet1.Cells[5, j].Value = string.Format("({0})", j);
                                exWorkSheet1.Cells[5, j].Style.Font.Bold = true;
                                exWorkSheet1.Cells[5, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[5, j].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet1.Cells["A6"].LoadFromDataTable(ExcelData, false);
                            }

                            //for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            //{
                            //    exWorkSheet1.Column(j).Width = 12;
                            //}

                            if (ExcelData.Rows.Count > 0)
                            {
                                using (ExcelRange col = exWorkSheet1.Cells[4, 1, 5 + ExcelData.Rows.Count, 17])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                            }

                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    byte[] data = exportPackge.GetAsByteArray();
                    return File(data, "application/octet-stream", "Register of Charge" + DateTime.Now + ".xlsx");
                }

               

               
            }
            else
            {
                return Content("No Data Found");
            }
        }

        #endregion

        #region Register of MBP2 13 Oct 2021
        public ActionResult viewMBPRegister(int EntityId, string view, int NatureOfTransactionId)
        {
            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            var RegisterDetails = objmbpregister.GetTransactionByNature(EntityId, view, NatureOfTransactionId);
            var Companyname = objmbpregister.GetCompanyName(EntityId);

            if (RegisterDetails.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        #region MBP2 Register
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MBP2Register");

                        if (RegisterDetails.Count > 0)
                        {
                            DataView view1 = new DataView();
                            DataTable table1 = new DataTable();

                            table1.Columns.Add("SrNo", typeof(int));
                            table1.Columns.Add("Nature of transaction (whether Loan/Guarantee/Security/Acquistion)");
                            table1.Columns.Add("Date of making Loan/Acquistion/Giving Guarantee/Providing Security");
                            table1.Columns.Add("Name and Address of the Person or Body Corporate to whom it is made or given or whose securities have been acquired (Listed /Unlisted entities)");
                            table1.Columns.Add("Amount of Loan");
                            table1.Columns.Add("Time period for which it is made/given");
                            table1.Columns.Add("Purpose of Loan/Acquistion/Guarantee/Security");
                            table1.Columns.Add("% of loan/acquisition /exposure on guarantee/security provided  to the paid up Capital, Free Reserves and securities Premium account and % of free  reserves and securities premium");
                            table1.Columns.Add("Date of Passing of Board resolution");
                            table1.Columns.Add("Date of passing Special resolution, if required");
                            table1.Columns.Add("Rate of Interest");
                            table1.Columns.Add("Date Of Maturity");
                            table1.Columns.Add("No. and kind of Securities");
                            table1.Columns.Add("Nominal Value and paid up value");
                            table1.Columns.Add("Cost of Acquistion ( in case of Securities how the purchase price was arrived at)");
                            table1.Columns.Add("Date of Selling Investments");
                            table1.Columns.Add("Selling Price ( how the price was arrived at)");

                            var SrNo = 0;
                            foreach (var item in RegisterDetails)
                            {
                                SrNo++;

                                table1.Rows.Add(SrNo, item.NatureOfTransaction, (item.DateOfMaking == null ? "" : Convert.ToDateTime(item.DateOfMaking).ToString("dd/MM/yyyy")), item.Name_ + (string.IsNullOrEmpty(item.Address_) ? "" : Environment.NewLine + " Address: " + item.Address_), item.Amount, item.TimePeriod,
                                    item.Purpose, item.PercentageOfReserve, (item.DateOfBoardResolution == null ? "" : Convert.ToDateTime(item.DateOfBoardResolution).ToString("dd/MM/yyyy")), (item.DateOfPassingResolution == null ? "" : Convert.ToDateTime(item.DateOfPassingResolution).ToString("dd/MM/yyyy")),
                                   item.RateOfInterest, (item.DateOfMaturity == null ? "" : Convert.ToDateTime(item.DateOfMaturity).ToString("dd/MM/yyyy")), item.NoOfSecurity + (string.IsNullOrEmpty(item.KindOfSecurity) ? "" : Environment.NewLine + " Kind Of Security: " + item.KindOfSecurity), item.NominalValue + ((item.PaidUp == null) ? "" : Environment.NewLine + " Paid Up Value: " + item.PaidUp),
                                    item.CostOfAuisition, (item.DateOfSelling == null ? "" : Convert.ToDateTime(item.DateOfSelling).ToString("dd/MM/yyyy")), item.SellingPrice);
                            }

                            view1 = new System.Data.DataView(table1);
                            DataTable ExcelData = null;

                            ExcelData = view1.ToTable("Selected", false, "SrNo", "Nature of transaction (whether Loan/Guarantee/Security/Acquistion)", "Date of making Loan/Acquistion/Giving Guarantee/Providing Security",
                                                    "Name and Address of the Person or Body Corporate to whom it is made or given or whose securities have been acquired (Listed /Unlisted entities)", "Amount of Loan",
                                                    "Time period for which it is made/given", "Purpose of Loan/Acquistion/Guarantee/Security", "% of loan/acquisition /exposure on guarantee/security provided  to the paid up Capital, Free Reserves and securities Premium account and % of free  reserves and securities premium",
                                                    "Date of Passing of Board resolution", "Date of passing Special resolution, if required", "Rate of Interest", "Date Of Maturity",
                                                    "No. and kind of Securities", "Nominal Value and paid up value",
                                                    "Cost of Acquistion ( in case of Securities how the purchase price was arrived at)",
                                                     "Date of Selling Investments", "Selling Price ( how the price was arrived at)");

                            exWorkSheet1.Cells["A1"].Value = Companyname.EntityName;
                            exWorkSheet1.Cells["A1:Q1"].Merge = true;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);


                            exWorkSheet1.Cells["A2"].Value = ((Companyname.CIN == null) ? "" : Environment.NewLine + " CIN: " + Companyname.CIN);
                            exWorkSheet1.Cells["A2:Q2"].Merge = true;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A2"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A3"].Value = (string.IsNullOrEmpty(Companyname.RegisterAddress) ? "" : Environment.NewLine + "Address:" + Companyname.RegisterAddress);
                            exWorkSheet1.Cells["A3:Q3"].Merge = true;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A4"].Value = (string.IsNullOrEmpty(Companyname.emailId) ? "" : Environment.NewLine + "Email:" + Companyname.emailId); 
                            exWorkSheet1.Cells["A4:Q4"].Merge = true;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A5"].Value = "Form No. MBP-2"; 
                            exWorkSheet1.Cells["A5:Q5"].Merge = true;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A5"].Style.WrapText = true;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;                           

                            exWorkSheet1.Cells["A6"].Value = "Register of Loans, Guarantee, Security and Acquisition made by the Company";
                            exWorkSheet1.Cells["A6:Q6"].Merge = true;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A6"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A7"].Value = "[Pursuant to section186(9) & rule 12(1)";
                            exWorkSheet1.Cells["A7:Q7"].Merge = true;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A7"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A7"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A8"].Value = "S.No";
                            exWorkSheet1.Cells["A8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A8"].Style.WrapText = true;
                            exWorkSheet1.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Row(8).Height = 124;

                            exWorkSheet1.Cells["B8"].Value = "Nature of transaction (whether Loan/Guarantee/Security/Acquistion)";
                            exWorkSheet1.Cells["B8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["B8"].Style.WrapText = true;
                            exWorkSheet1.Cells["B8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["C8"].Value = "Date of making Loan/Acquistion/Giving Guarantee/Providing Security";
                            exWorkSheet1.Cells["C8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["C8"].Style.WrapText = true;
                            exWorkSheet1.Cells["C8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["D8"].Value = "Name and Address of the Person or Body Corporate to whom it is made or given or whose securities have been acquired (Listed /Unlisted entities)";
                            exWorkSheet1.Cells["D8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D8"].Style.WrapText = true;
                            exWorkSheet1.Cells["D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["E8"].Value = "Amount of Loan";
                            exWorkSheet1.Cells["E8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["E8"].Style.WrapText = true;
                            exWorkSheet1.Cells["E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["F8"].Value = "Time period for which it is made/given";
                            exWorkSheet1.Cells["F8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["F8"].Style.WrapText = true;
                            exWorkSheet1.Cells["F8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["G8"].Value = "Purpose of Loan/Acquistion/Guarantee/Security";
                            exWorkSheet1.Cells["G8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["G8"].Style.WrapText = true;
                            exWorkSheet1.Cells["G8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["H8"].Value = "% of loan/acquisition /exposure on guarantee/security provided  to the paid up Capital, Free Reserves and securities Premium account and % of free  reserves and securities premium";
                            exWorkSheet1.Cells["H8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["H8"].Style.WrapText = true;
                            exWorkSheet1.Cells["H8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["I8"].Value = "Date of Passing of Board resolution";
                            exWorkSheet1.Cells["I8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["I8"].Style.WrapText = true;
                            exWorkSheet1.Cells["I8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["J8"].Value = "Date of passing Special resolution, if required";
                            exWorkSheet1.Cells["J8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J8"].Style.WrapText = true;
                            exWorkSheet1.Cells["J8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["K8"].Value = "Rate of Interest";
                            exWorkSheet1.Cells["K8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["K8"].Style.WrapText = true;
                            exWorkSheet1.Cells["K8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["L8"].Value = "Date of Maturity";
                            exWorkSheet1.Cells["L8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["L8"].Style.WrapText = true;
                            exWorkSheet1.Cells["L8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["L8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["M8"].Value = "No. and kind of Securities";
                            exWorkSheet1.Cells["M8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M8"].Style.WrapText = true;
                            exWorkSheet1.Cells["M8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["M8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            
                            exWorkSheet1.Cells["N8"].Value = "Nominal Value and paid up value";
                            exWorkSheet1.Cells["N8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["N8"].Style.WrapText = true;
                            exWorkSheet1.Cells["N8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["N8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["O8"].Value = "Cost of Acquistion ( in case of Securities how the purchase price was arrived at)";
                            exWorkSheet1.Cells["O8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["O8"].Style.WrapText = true;
                            exWorkSheet1.Cells["O8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["O8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["P8"].Value = "Date of Selling Investments";
                            exWorkSheet1.Cells["P8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["P8"].Style.WrapText = true;
                            exWorkSheet1.Cells["P8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["P8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["Q8"].Value = "Selling Price ( how the price was arrived at)";
                            exWorkSheet1.Cells["Q8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["Q8"].Style.WrapText = true;
                            exWorkSheet1.Cells["Q8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Q8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            {
                                exWorkSheet1.Cells[9, j].Value = string.Format("({0})", j);
                                exWorkSheet1.Cells[9, j].Style.Font.Bold = true;
                                exWorkSheet1.Cells[9, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[9, j].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet1.Cells["A10"].LoadFromDataTable(ExcelData, false);
                            }

                            //for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            //{
                            //    exWorkSheet1.Column(j).Width = 12;
                            //}

                            exWorkSheet1.Column(5).Width = 12.5;
                            exWorkSheet1.Column(6).Width = 13.5;
                            exWorkSheet1.Column(7).Width = 13.5;
                            exWorkSheet1.Column(8).Width = 33.14;
                            exWorkSheet1.Column(9).Width = 18;
                            exWorkSheet1.Column(12).Width = 18;
                            exWorkSheet1.Column(13).Width = 13.5;

                            exWorkSheet1.Column(3).Width = 22.86;
                            exWorkSheet1.Column(2).Width = 22.43;
                            exWorkSheet1.Column(4).Width = 25;
                            exWorkSheet1.Column(10).Width = 18;
                            exWorkSheet1.Column(11).Width = 18;
                            exWorkSheet1.Column(14).Width = 18;
                            exWorkSheet1.Column(15).Width = 18.44;
                            exWorkSheet1.Column(16).Width = 15.14;
                            exWorkSheet1.Column(17).Width = 14;
                            //exWorkSheet1.Column(18).Width = 18;


                            if (ExcelData.Rows.Count > 0)
                            {
                                using (ExcelRange col = exWorkSheet1.Cells[8, 1, 9 + ExcelData.Rows.Count, 17])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    CustomerID = Convert.ToInt16(AuthenticationHelper.CustomerID);
                    path = "~/Temp/" + CustomerID + "/";
                    Filename = "Register_of_MBP2.xlsx";
                    if (!Directory.Exists(Server.MapPath(path)))
                    {
                        Directory.CreateDirectory(Server.MapPath(path));
                    }
                    _path = System.IO.Path.Combine(Server.MapPath(path), Filename);
                    FileStream aFile = new FileStream(_path, FileMode.Create);
                    byte[] byData = exportPackge.GetAsByteArray();
                    aFile.Seek(0, SeekOrigin.Begin);
                    aFile.Write(byData, 0, byData.Length);
                    aFile.Close();
                }

                return Json(Filename, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportMBPRegister(int EntityId, string view,int NatureOfTransactionId)
        {
            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            var RegisterDetails = objmbpregister.GetTransactionByNature(EntityId, view, NatureOfTransactionId);
            var Companyname = objmbpregister.GetCompanyName(EntityId);

            if (RegisterDetails.Count > 0)
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        #region MBP2 Register
                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MBP2Register");

                        if (RegisterDetails.Count > 0)
                        {
                            DataView view1 = new DataView();
                            DataTable table1 = new DataTable();

                            table1.Columns.Add("SrNo", typeof(int));
                            table1.Columns.Add("Nature of transaction (whether Loan/Guarantee/Security/Acquistion)");
                            table1.Columns.Add("Date of making Loan/Acquistion/Giving Guarantee/Providing Security");
                            table1.Columns.Add("Name and Address of the Person or Body Corporate to whom it is made or given or whose securities have been acquired (Listed /Unlisted entities)");
                            table1.Columns.Add("Amount of Loan");
                            table1.Columns.Add("Time period for which it is made/given");
                            table1.Columns.Add("Purpose of Loan/Acquistion/Guarantee/Security");
                            table1.Columns.Add("% of loan/acquisition /exposure on guarantee/security provided  to the paid up Capital, Free Reserves and securities Premium account and % of free  reserves and securities premium");
                            table1.Columns.Add("Date of Passing of Board resolution");
                            table1.Columns.Add("Date of passing Special resolution, if required");
                            table1.Columns.Add("Rate of Interest");
                            table1.Columns.Add("Date Of Maturity");
                            table1.Columns.Add("No. and kind of Securities");
                            table1.Columns.Add("Nominal Value and paid up value");
                            table1.Columns.Add("Cost of Acquistion ( in case of Securities how the purchase price was arrived at)");
                            table1.Columns.Add("Date of Selling Investments");
                            table1.Columns.Add("Selling Price ( how the price was arrived at)");

                            var SrNo = 0;
                            foreach (var item in RegisterDetails)
                            {
                                SrNo++;

                                table1.Rows.Add(SrNo, item.NatureOfTransaction, (item.DateOfMaking == null ? "" : Convert.ToDateTime(item.DateOfMaking).ToString("dd/MM/yyyy")), item.Name_ + (string.IsNullOrEmpty(item.Address_) ? "" : Environment.NewLine + " Address: " + item.Address_), item.Amount, item.TimePeriod,
                                    item.Purpose, item.PercentageOfReserve, (item.DateOfBoardResolution == null ? "" : Convert.ToDateTime(item.DateOfBoardResolution).ToString("dd/MM/yyyy")), (item.DateOfPassingResolution == null ? "" : Convert.ToDateTime(item.DateOfPassingResolution).ToString("dd/MM/yyyy")),
                                   item.RateOfInterest, (item.DateOfMaturity == null ? "" : Convert.ToDateTime(item.DateOfMaturity).ToString("dd/MM/yyyy")), item.NoOfSecurity + (string.IsNullOrEmpty(item.KindOfSecurity) ? "" : Environment.NewLine + " Kind Of Security: " + item.KindOfSecurity), item.NominalValue + ((item.PaidUp == null) ? "" : Environment.NewLine + " Paid Up Value: " + item.PaidUp),
                                    item.CostOfAuisition, (item.DateOfSelling == null ? "" : Convert.ToDateTime(item.DateOfSelling).ToString("dd/MM/yyyy")), item.SellingPrice);
                            }

                            view1 = new System.Data.DataView(table1);
                            DataTable ExcelData = null;

                            ExcelData = view1.ToTable("Selected", false, "SrNo", "Nature of transaction (whether Loan/Guarantee/Security/Acquistion)", "Date of making Loan/Acquistion/Giving Guarantee/Providing Security",
                                                    "Name and Address of the Person or Body Corporate to whom it is made or given or whose securities have been acquired (Listed /Unlisted entities)", "Amount of Loan",
                                                    "Time period for which it is made/given", "Purpose of Loan/Acquistion/Guarantee/Security", "% of loan/acquisition /exposure on guarantee/security provided  to the paid up Capital, Free Reserves and securities Premium account and % of free  reserves and securities premium",
                                                    "Date of Passing of Board resolution", "Date of passing Special resolution, if required", "Rate of Interest", "Date Of Maturity",
                                                    "No. and kind of Securities", "Nominal Value and paid up value",
                                                    "Cost of Acquistion ( in case of Securities how the purchase price was arrived at)",
                                                     "Date of Selling Investments", "Selling Price ( how the price was arrived at)");

                            exWorkSheet1.Cells["A1"].Value = Companyname.EntityName;
                            exWorkSheet1.Cells["A1:Q1"].Merge = true;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:Q1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);


                            exWorkSheet1.Cells["A2"].Value = ((Companyname.CIN == null) ? "" : Environment.NewLine + " CIN: " + Companyname.CIN);
                            exWorkSheet1.Cells["A2:Q2"].Merge = true;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2:Q2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A2"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A3"].Value = (string.IsNullOrEmpty(Companyname.RegisterAddress) ? "" : Environment.NewLine + "Address:" + Companyname.RegisterAddress);
                            exWorkSheet1.Cells["A3:Q3"].Merge = true;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3:Q3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A4"].Value = (string.IsNullOrEmpty(Companyname.emailId) ? "" : Environment.NewLine + "Email:" + Companyname.emailId);
                            exWorkSheet1.Cells["A4:Q4"].Merge = true;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4:Q4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A4"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A5"].Value = "Form No. MBP-2";
                            exWorkSheet1.Cells["A5:Q5"].Merge = true;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5:Q5"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A5"].Style.WrapText = true;
                            exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["A6"].Value = "Register of Loans, Guarantee, Security and Acquisition made by the Company";
                            exWorkSheet1.Cells["A6:Q6"].Merge = true;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6:Q6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A6"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A7"].Value = "[Pursuant to section186(9) & rule 12(1)";
                            exWorkSheet1.Cells["A7:Q7"].Merge = true;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7:Q7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A7"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["A7"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A7"].Style.Fill.BackgroundColor.SetColor(Color.White);

                            exWorkSheet1.Cells["A8"].Value = "S.No";
                            exWorkSheet1.Cells["A8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["A8"].Style.WrapText = true;
                            exWorkSheet1.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Row(8).Height = 124;

                            exWorkSheet1.Cells["B8"].Value = "Nature of transaction (whether Loan/Guarantee/Security/Acquistion)";
                            exWorkSheet1.Cells["B8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["B8"].Style.WrapText = true;
                            exWorkSheet1.Cells["B8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["B8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["C8"].Value = "Date of making Loan/Acquistion/Giving Guarantee/Providing Security";
                            exWorkSheet1.Cells["C8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["C8"].Style.WrapText = true;
                            exWorkSheet1.Cells["C8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["C8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["D8"].Value = "Name and Address of the Person or Body Corporate to whom it is made or given or whose securities have been acquired (Listed /Unlisted entities)";
                            exWorkSheet1.Cells["D8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["D8"].Style.WrapText = true;
                            exWorkSheet1.Cells["D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["D8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["E8"].Value = "Amount of Loan";
                            exWorkSheet1.Cells["E8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["E8"].Style.WrapText = true;
                            exWorkSheet1.Cells["E8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["E8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["F8"].Value = "Time period for which it is made/given";
                            exWorkSheet1.Cells["F8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["F8"].Style.WrapText = true;
                            exWorkSheet1.Cells["F8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["G8"].Value = "Purpose of Loan/Acquistion/Guarantee/Security";
                            exWorkSheet1.Cells["G8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["G8"].Style.WrapText = true;
                            exWorkSheet1.Cells["G8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["H8"].Value = "% of loan/acquisition /exposure on guarantee/security provided  to the paid up Capital, Free Reserves and securities Premium account and % of free  reserves and securities premium";
                            exWorkSheet1.Cells["H8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["H8"].Style.WrapText = true;
                            exWorkSheet1.Cells["H8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["I8"].Value = "Date of Passing of Board resolution";
                            exWorkSheet1.Cells["I8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["I8"].Style.WrapText = true;
                            exWorkSheet1.Cells["I8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["J8"].Value = "Date of passing Special resolution, if required";
                            exWorkSheet1.Cells["J8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["J8"].Style.WrapText = true;
                            exWorkSheet1.Cells["J8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["K8"].Value = "Rate of Interest";
                            exWorkSheet1.Cells["K8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["K8"].Style.WrapText = true;
                            exWorkSheet1.Cells["K8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["L8"].Value = "Date of Maturity";
                            exWorkSheet1.Cells["L8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["L8"].Style.WrapText = true;
                            exWorkSheet1.Cells["L8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["L8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["M8"].Value = "No. and kind of Securities";
                            exWorkSheet1.Cells["M8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["M8"].Style.WrapText = true;
                            exWorkSheet1.Cells["M8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["M8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["N8"].Value = "Nominal Value and paid up value";
                            exWorkSheet1.Cells["N8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["N8"].Style.WrapText = true;
                            exWorkSheet1.Cells["N8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["N8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["O8"].Value = "Cost of Acquistion ( in case of Securities how the purchase price was arrived at)";
                            exWorkSheet1.Cells["O8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["O8"].Style.WrapText = true;
                            exWorkSheet1.Cells["O8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["O8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["P8"].Value = "Date of Selling Investments";
                            exWorkSheet1.Cells["P8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["P8"].Style.WrapText = true;
                            exWorkSheet1.Cells["P8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["P8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            exWorkSheet1.Cells["Q8"].Value = "Selling Price ( how the price was arrived at)";
                            exWorkSheet1.Cells["Q8"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q8"].Style.Fill.BackgroundColor.SetColor(Color.White);
                            exWorkSheet1.Cells["Q8"].Style.WrapText = true;
                            exWorkSheet1.Cells["Q8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["Q8"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            {
                                exWorkSheet1.Cells[9, j].Value = string.Format("({0})", j);
                                exWorkSheet1.Cells[9, j].Style.Font.Bold = true;
                                exWorkSheet1.Cells[9, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet1.Cells[9, j].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet1.Cells["A10"].LoadFromDataTable(ExcelData, false);
                            }

                            //for (int j = 1; j <= 17; j++) // this will apply it from col 1 to 10
                            //{
                            //    exWorkSheet1.Column(j).Width = 12;
                            //}

                            exWorkSheet1.Column(5).Width = 12.5;
                            exWorkSheet1.Column(6).Width = 13.5;
                            exWorkSheet1.Column(7).Width = 13.5;
                            exWorkSheet1.Column(8).Width = 33.14;
                            exWorkSheet1.Column(9).Width = 18;
                            exWorkSheet1.Column(12).Width = 18;
                            exWorkSheet1.Column(13).Width = 13.5;

                            exWorkSheet1.Column(3).Width = 22.86;
                            exWorkSheet1.Column(2).Width = 22.43;
                            exWorkSheet1.Column(4).Width = 25;
                            exWorkSheet1.Column(10).Width = 18;
                            exWorkSheet1.Column(11).Width = 18;
                            exWorkSheet1.Column(14).Width = 18;
                            exWorkSheet1.Column(15).Width = 18.44;
                            exWorkSheet1.Column(16).Width = 15.14;
                            exWorkSheet1.Column(17).Width = 14;
                            //exWorkSheet1.Column(18).Width = 18;


                            if (ExcelData.Rows.Count > 0)
                            {
                                using (ExcelRange col = exWorkSheet1.Cells[8, 1, 9 + ExcelData.Rows.Count, 17])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                            }
                        }
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    byte[] data = exportPackge.GetAsByteArray();
                    return File(data, "application/octet-stream", "Register of MBP-2" + DateTime.Now + ".xlsx");
                }

            }
            else
            {
                return Content("No Data Found");
            }
        }
        
        public ActionResult GetDetailsofDirectorEntityWise(int EntityId)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult OpenPopupforRegisterDetails(int RegisterId, long EntityId)
        {
            RegisterListDetails objregister = new RegisterListDetails();
            objregister.registerID = RegisterId;
            objregister.EntityId = EntityId;
            return PartialView("_RegisterDetails", objregister);
        }
    }

    public class FileModel
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
#endregion