﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class UserController : Controller
    {
        IUser objIUser;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public UserController(IUser objUser)
        {
            objIUser = objUser;
        }

        // GET: BM_Management/User
        public ActionResult UserList()
        {
            var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUser.GetProductByCustomerId(customerId);
            return View(model);
        }

        public ActionResult User()
        {
            UserVM objUserVM = new UserVM();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                objUserVM.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User") select x.Addview).FirstOrDefault();
                objUserVM.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User") select x.Editview).FirstOrDefault();
                objUserVM.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User") select x.DeleteView).FirstOrDefault();
            }
            return View(objUserVM);
        }

        public ActionResult UserCustomerMapping()
        {
            return View();
        }

        public ActionResult AddEditUserNew(int Id)
        {
            var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUser.GetUser(Id);
            return PartialView("_AddEditUserNew", model);
        }
        [HttpPost]
        public ActionResult SaveAddEditUserNew(UserVM obj)
        {
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                obj.Message = new Response();

                if (obj.UserID == 0 )
                {
                    #region Create New User
                    obj.CreatedBy = UserID;
                    obj.CustomerID = CustomerID;
                    obj.CreatedByText = AuthenticationHelper.User;

                    var hasError = false;

                    string passwordText = Business.Util.CreateRandomPassword(10);
                    obj.Password = passwordText;

                    string Mailbody = getEmailMessage(obj, obj.Password, out hasError);

                    if (hasError == true)
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Something went wrong, Please try again";
                    }
                    else
                    {
                        obj.EmailBody = Mailbody;
                        string SenderEmailAddress = System.Configuration.ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        var result = objIUser.Create(obj, SenderEmailAddress);

                        if (result.Message.Success)
                        {
                            obj.UserID = result.UserID;

                            Business.EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Account Created", Mailbody);

                            obj.Message.Success= true;
                            obj.Message.Message = result.Message.Message;
                        }
                        else
                        {
                            obj.Message.Error = result.Message.Error;
                            obj.Message.Message = result.Message.Message;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Update User
                    obj.Message = new Response();

                    obj.CustomerID = CustomerID;
                    obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                    obj.CreatedByText = AuthenticationHelper.User;

                    var hasError = false;

                    string Mailbody = SendNotificationEmailChanged(obj, out hasError);
                    if (hasError == true)
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Something went wrong, Please try again";
                    }
                    else
                    {
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        string Old_EmailId = "";
                        var result = objIUser.Update(obj, SenderEmailAddress, out Old_EmailId);

                        if (result.Message.Success)
                        {
                            if (Old_EmailId != result.Email)
                                Business.EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Email Changed", Mailbody);

                            obj.Message.Success = true;
                            obj.Message.Message = result.Message.Message;
                        }
                        else
                        {
                            obj.Message.Error = result.Message.Error;
                            obj.Message.Message = result.Message.Message;
                        }
                        
                    }
                    #endregion
                }
            }
            return PartialView("_AddEditUserNew", obj);
        }

        private string getEmailMessage(UserVM user, string passwordText, out bool hasError)
        {
            try
            {
                hasError = false;
                int customerID = -1;
                string ReplyEmailAddressName = "";
                int customerId = Convert.ToInt32(AuthenticationHelper.UserID);

                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    ReplyEmailAddressName = Business.CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                //cvUserPopup.ErrorMessage = "Something went wrong, Please try again";
                return null;
            }
        }

        private string SendNotificationEmailChanged(UserVM user, out bool hasError)
        {
            hasError = false;
            try
            {
                user.Message = new Response();
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    //change by Ruchi on 15th july 2020
                    customerID = Business.UserManagement.GetByID(Convert.ToInt32(user.UserID)).CustomerID ?? 0;
                    ReplyEmailAddressName = Business.CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                user.Message.Error = true;
                user.Message.Message = "Something went wrong, Please try again";

            }
            return null;
        }

        [HttpPost]
        public ActionResult DeactivateUser(int UserID)
        {
            //var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUser.DeactivateUser(UserID);
            return Json(new { result = model }, JsonRequestBehavior.AllowGet);
        }
    }
}