﻿using BM_ManegmentServices.Services.AnnualMeetingCalendar;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AnnualMeetingCalenderController : Controller
    {
        IAnnualCalender _objAnnual;
        IEntityMaster objEntityMaster;
        IConfiguration objConf;
        ICommitteeComp _objCommittee;

        public AnnualMeetingCalenderController(IAnnualCalender _objAnnual, IEntityMaster objEntityMaster, IConfiguration objConf, ICommitteeComp _objCommittee)
        {
            this._objAnnual = _objAnnual;
            this.objEntityMaster = objEntityMaster;
            this.objConf = objConf;
            this._objCommittee = _objCommittee;
        }

        int UserId = AuthenticationHelper.UserID;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        string role = AuthenticationHelper.Role;

        // GET: BM_Management/AnnualMeetingCalender
        //[Route("AnnualMeetingCalender")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AnnualMeeting()
        {
            AnnualMeetingVM obj = new AnnualMeetingVM();
            return View(obj);
        }

        //[Route("AddAnnualMeeting")]
        public ActionResult OpenPopUPForAnnualMeeting(int ID)
        {
            int EntityID = 0;
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string view = string.Empty;

            AddAnnualMeetingCalenderVM values = new AddAnnualMeetingCalenderVM();

            values = new AddAnnualMeetingCalenderVM
            {
                Entity = _objAnnual.GetAllEntityDetails(CustomerId, UserId, role),
                FY = _objAnnual.GetFYEntityWise(EntityID),
                //MeetingType = _objMeetings.GetMeeting(CustomerId)
                MeetingType = _objCommittee.GetAll_MeetingTypeByCustomer(customerID, false)
            };

            view = "_AddAnnualMeeting";

            return PartialView(view, values);
        }

        public ActionResult OpenEditAnnualMeeting(int ID)
        {
            string view = string.Empty;
            AnnualMeetingSheduler values = new AnnualMeetingSheduler();

            if (ID > 0)
            {
                values = _objAnnual.GetSchedulerEditvalue(ID);
            }

            view = "_EditAnnualMeeting";

            return PartialView(view, values);
        }

        [HttpPost]
        public ActionResult UpdateAnnualMeeting(AnnualMeetingSheduler _objAnnualMeeting)
        {
            if (ModelState.IsValid)
            {
                if (_objAnnualMeeting.ID == 0)
                {
                    //  _objAnnualMeeting = _objAnnual.CreateAnnualMeeting(_objAnnualMeeting, UserId, CustomerId);
                }
                else
                {
                    _objAnnualMeeting = _objAnnual.UpdateAnnualMeeting(_objAnnualMeeting, UserId, CustomerId);
                }
            }
            else
            {

            }
            return PartialView("_EditAnnualMeeting", _objAnnualMeeting);
        }

        public virtual JsonResult GetAnnualMeeting([DataSourceRequest] DataSourceRequest request)
        {

            List<AnnualMeetingSheduler> _objgetdata = new List<AnnualMeetingSheduler>();
            _objgetdata = _objAnnual.GetAnnualMeeting(CustomerId,UserId,role);
            return Json(_objgetdata.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllEntityDetails()
        {
            List<EntityDetails> objentity = new List<EntityDetails>();

            objentity = _objAnnual.GetAllEntityDetails(CustomerId, UserId, role);
            return Json(objentity, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAll_FYByEntityID(int EntityID)
        {
            List<FYearDetails> _objyear = new List<FYearDetails>();
            _objyear = _objAnnual.GetFYEntityWise(EntityID);
            return Json(_objyear, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMeeting()
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //List<CommitteeCompVM> _objMeetingTypes = new List<CommitteeCompVM>();
            //_objMeetingTypes = _objMeetings.GetMeeting(CustomerId);
            var _objMeetingTypes = _objCommittee.GetAll_MeetingTypeByCustomer(customerID, false);
            return Json(_objMeetingTypes, JsonRequestBehavior.AllowGet);
        }


        // [HttpGet]
        public JsonResult ReadAnnualMeeting([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_objAnnual.GetAllAnnualMeeting(CustomerId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateAnnualMeeting([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<AnnualMVM> _objMeeting)
        {

            AnnualMVM _objcreatemeeting = new AnnualMVM();
            if (_objMeeting != null && ModelState.IsValid)
            {
                if (_objMeeting.Count() > 0)
                {
                    bool checkDuplicatedata = _objAnnual.CheckDuplicateAnnualMeetingDate(_objMeeting,CustomerId);
                    if (checkDuplicatedata)
                    {
                        foreach (var item in _objMeeting)
                        {
                            if (item.AnnualMeetingId == 0)
                            {
                                _objcreatemeeting = _objAnnual.AddAnnualMeeting(item, CustomerId, UserId, ModelState);
                            }
                           
                        }
                    }
                    else
                    {
                       
                        ModelState.AddModelError("", "Something went wrong!Please check");
                    }
                }
            }

            return Json(_objMeeting.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult DeleteAnnualMeetingCalender([DataSourceRequest] DataSourceRequest request, AnnualMeetingSheduler _objannualmeeting)
        {
            //if (ModelState.IsValid)
            //{
                _objAnnual.DeleteAnnualCalenderMeeting(_objannualmeeting, ModelState, UserId);
            //}

            return Json(new[] { _objannualmeeting }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ActionResult CheckAnualdate(AnnualMVM _obj)
        {
            string checkvalidation = string.Empty;
            if (_obj!=null)
            {
                checkvalidation = _objAnnual.Checkvalidation(_obj, CustomerId);
            }
            return Json(checkvalidation, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckAnualSRno(AnnualMVM _obj)
        {
            string checkvalidation = string.Empty;
            if (_obj != null)
            {
               checkvalidation = _objAnnual.CheckvalidationforSrnumber(_obj, CustomerId);
            }
            return Json(checkvalidation, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AnnualMeetingDir()
        {
            AnnualMeetingVM obj = new AnnualMeetingVM();
            return View(obj);
        }
    }
}