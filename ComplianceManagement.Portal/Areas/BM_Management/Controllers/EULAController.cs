﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.EULA;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.EULA;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class EULAController : Controller
    {
        IEULA_Service objIEULA_Service;
        IFileData_Service objIFileData_Service;
        public EULAController(IEULA_Service objEULA_Service, IFileData_Service objFileData_Service)
        {
            objIEULA_Service = objEULA_Service;
            objIFileData_Service = objFileData_Service;
        }
        public ActionResult Details()
        {
            return View("EULA_Details");
        }
        public virtual JsonResult GetEULA_Details([DataSourceRequest] DataSourceRequest request)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIEULA_Service.GetEULADetails(userID);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditEULA_Details(int id, string type)
        {
            var model = objIEULA_Service.GetEULADetailsById(id, type);
            return PartialView("_ViewEULA_Details", model);
        }
        public ActionResult ViewEULA_Details()
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIEULA_Service.GetEULADetailsByUserId(userID);
            return PartialView("_ViewEULA_Details", model);
        }

        [HttpPost]
        public ActionResult SaveEULA_Details(EULA_DetailsVM model, string command_name)
        {
            if(model != null)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (command_name == "Approve")
                {
                    model = objIEULA_Service.SaveEULAStatus(model, 2, userID);
                }
                else if (command_name == "Reject")
                {
                    model = objIEULA_Service.SaveEULAStatus(model, 3, userID);
                }
                else if (command_name == "Upload")
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    model.CustomerID = customerID;
                    model.UserID = userID;
                    model = objIFileData_Service.SaveEULAFile(model);
                }
            }
            return PartialView("_ViewEULA_Details", model);
        }

        public ActionResult DownloadEULA(int id)
        {
            var fileName = Server.MapPath(objIEULA_Service.GetEULADocById(id));
            var bytes = objIFileData_Service.GetEULAFile(fileName);
            return File(bytes, "application/force-download", Path.GetFileName(fileName));
        }

        public ActionResult SendCrendentials()
        {
            return View("SendCrendentials");
        }
        [HttpPost]
        public ActionResult SendCrendentialsPost(List<int> lstId)
        {
            var objResults = new EULASendMail_VM() { ErrorMessage = new List<string>()};
            var error = false;
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            foreach (var item in lstId)
            {
                try
                {
                    var obj = objIEULA_Service.GetUser(item);

                    if(obj != null)
                    {
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                        string passwordText = Util.CreateRandomPassword(10);

                        string Mailbody = getEmailMessage(obj, passwordText);
                        if (!string.IsNullOrEmpty(Mailbody))
                        {
                            var encryptedPass = Util.CalculateAESHash(passwordText);

                            bool result = objIEULA_Service.ChangePassword(obj.UserID, encryptedPass);
                            bool result1 = objIEULA_Service.ChangePasswordAudit(obj.UserID, encryptedPass);

                            if (result && result1)
                            {
                                objIEULA_Service.WrongAttemptCountUpdate(obj.UserID);

                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "Temporary Password", Mailbody);
                                objIEULA_Service.SendCrendentialsPost(item, obj.Email, userID);
                            }
                            else
                            {
                                error = true;
                                objResults.ErrorMessage.Add(obj.Email + " : Error in send Mail." );
                            }
                        }
                    }
                    else
                    {
                        error = true;
                    }
                }
                catch (Exception ex)
                {
                    error = true;
                }
            }

            objResults.Error = error;
            return Json(objResults, JsonRequestBehavior.AllowGet);
        }

        private string getEmailMessage(UserVM user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                int customerId = Convert.ToInt32(AuthenticationHelper.UserID);

                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    ReplyEmailAddressName = Business.CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}