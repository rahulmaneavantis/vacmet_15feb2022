﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.MyMeetings;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyMeetingsController : Controller
    {
        IMeeting_Service objIMeeting_Service;
        IMainMeeting objIMainMeeting;
        IMyMeetingsService objIMyMeetingsService;

        public MyMeetingsController(IMeeting_Service objMeeting_Service, IMainMeeting objMainMeeting, IMyMeetingsService objMyMeetingsService)
        {
            objIMeeting_Service = objMeeting_Service;
            objIMainMeeting = objMainMeeting;
            objIMyMeetingsService = objMyMeetingsService;
        }
        public ActionResult Index()
        {
            return View();
        }

        #region Upcomming Meetings
        public PartialViewResult AgendaView(long MeetingId, long ParticipantId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewAgenda(MeetingId, CustomerId);
            if (model != null)
            {
                model.ParticipantID_ = ParticipantId;
            }
            return PartialView("_AgendaViewer", model);
        }

        [HttpPost]
        public ActionResult SaveAgendaComments(MeetingAgendaCommentVM meetingAgendaCommentVM)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            meetingAgendaCommentVM = objIMyMeetingsService.SaveAgendaComments(meetingAgendaCommentVM, UserID);

            return Json(meetingAgendaCommentVM, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAgendaComments(long participantId)
        {
            var model = objIMyMeetingsService.GetAgendaComments(participantId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetParticipantAgendaComments(long meetingId)
        {
            var model = objIMyMeetingsService.GetParticipantAgendaComments(meetingId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Meetings RSVP Details
        public PartialViewResult Meeting_RSVP_Details(long meetingId)
        {
            return PartialView("_RSVP_Details", meetingId);
        }
        #endregion

        #region Draft Minutes
        public ActionResult DraftMinutes()
        {
            return View();
        }

        public PartialViewResult DraftMinutesView(long MeetingId, long DraftCirculationID)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewDraftMinutes(MeetingId, CustomerId, DraftCirculationID, UserID);
            if (model != null)
            {
                model.DraftCirculationID_ = DraftCirculationID;
                model.IsDraftMinutesApproved = objIMeeting_Service.IsDraftMinutesApproved(DraftCirculationID);
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                model.lstOtherParticipantAttendance = objIMainMeeting.GetOtherParticipentAttendence(MeetingId);
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_DraftMinutesView", model);
        }
        [HttpPost]
        public ActionResult SaveComments(MeetingMinutesCommentVM meetingMinutesCommentVM)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            meetingMinutesCommentVM = objIMyMeetingsService.SaveComments(meetingMinutesCommentVM, UserID);

            return Json(meetingMinutesCommentVM, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveDraftMinutes(MeetingMinutesApproveVM obj)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            obj = objIMeeting_Service.ApproveDraftMinutes(obj, UserID);

            return PartialView("_DraftMinutesApprove", obj);
        }

        [HttpPost]
        public ActionResult GetComments(long draftCirculationID)
        {
            var model= objIMyMeetingsService.GetComments(draftCirculationID);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetParticipantComments(long meetingId)
        {
            var model = objIMyMeetingsService.GetParticipantComments(meetingId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Concluded Meeting
        public ActionResult ConcludedMeetings()
        {
            return View();
        }

        public PartialViewResult MOM(long MeetingId, long DraftCirculationID)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if (model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                model.lstOtherParticipantAttendance = objIMainMeeting.GetOtherParticipentAttendence(MeetingId);
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_MOMView", model);
        }
        #endregion

        #region Agenda List of Concluded Meetings
        public ActionResult AgendaItems()
        {
            return View("ConcludedMeetingAgenda");
        }

        public ActionResult AgendaItems_CS()
        {
            return View("ConcludedMeetingAgenda_CS");
        }
        #endregion
    }
}