﻿using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class OTPController : Controller
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();
        // GET: BM_Management/OTP
        //public ActionResult Index(string Email, long MeetingId)
        //{
        //    VMOTP obj = new VMOTP();
        //    obj.EmilId = Email;
        //    obj.MeetingId = MeetingId;
        //    return View(obj);
        //}

        public ActionResult Index(string Email, long MeetingId)
        {
            VMOTP obj = new VMOTP();
            obj.EmilId = Email;
            obj.MeetingId = MeetingId;
            var user = (from rows in entities.Users where rows.Email == Email select rows).FirstOrDefault();
            string name = string.Format("{0} {1}", user.FirstName, user.LastName);

            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", user.ID, user.Role, name, 0, 'C', user.CustomerID, 0, string.Empty, string.Empty), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            return View(obj);
        }

        public ActionResult verifyOTP(VMOTP objotp)
        {
            var checkotp = (from row in entities.VerifyOTPs where row.EmailId == objotp.EmilId && row.OTP == objotp.otp select row).FirstOrDefault();
            if (checkotp != null)
            {
                var user = (from rows in entities.Users where rows.Email == objotp.EmilId select rows).FirstOrDefault();
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);

                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", user.ID, user.Role, name, 0, 'C', user.CustomerID, 0, string.Empty, string.Empty), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

                return RedirectToAction("MeetingsDetails", "MainMeeting", new { @MeetingId = objotp.MeetingId });
            }
            else
            {
                objotp.Error = true;
                objotp.Message = "Please Enter correct otp";
                return PartialView("_PartialOtpforMeeting", objotp);
            }

        }
    }
}