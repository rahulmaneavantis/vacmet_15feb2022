﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AdditionalComplianceController : Controller
    {
        // GET: BM_Management/AdditionalCompliance

        IAdditionalComplianceMaster objaddition;
        public AdditionalComplianceController(IAdditionalComplianceMaster objaddition)
        {
            this.objaddition = objaddition;
        }
        public ActionResult Index()
        {
            return View();
        }


        #region Add Additional Compliances added By Ruchi on 27th July 2020
        public ActionResult AddNewCompliances(long Id)
        {
            AdditionalCompliances_VM _objnewCompliances = new AdditionalCompliances_VM();
            if(Id>0)
            {
                _objnewCompliances = objaddition.EditAdditionalCompliances(Id);
            }
            return PartialView("_AddadditionalCompliance", _objnewCompliances);
        }


        public ActionResult CreateAdditionalCompliances(AdditionalCompliances_VM _objadditionalcomp)
        {
            if (ModelState.IsValid)
            {
                int UserId = AuthenticationHelper.UserID;
                if (_objadditionalcomp.Id==0)
                {
                    _objadditionalcomp = objaddition.AddAdditionalCompliances(_objadditionalcomp, UserId);
                }
                else
                {
                    _objadditionalcomp = objaddition.UpdateAdditionalCompliances(_objadditionalcomp, UserId);
                }
            }
            return PartialView("_AddadditionalCompliance", _objadditionalcomp);
        }

      
        #endregion
    }
}