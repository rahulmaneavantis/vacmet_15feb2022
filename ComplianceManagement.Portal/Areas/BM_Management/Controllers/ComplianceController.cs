﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ComplianceController : Controller
    {
        // GET: BM_Management/Compliance
        ICompliance obj;
        public ComplianceController(ICompliance obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ComplienceList()
        {
            return View();
        }
        public RedirectResult AddEditCompliance()
        {
            return Redirect(Server.MapPath("~/AddEditComplience.aspx"));
        }

        public ActionResult OpenComplianceFormMapping(long ComplianceId)
        {
            FormDetails _objForm = new FormDetails();
            _objForm.ComplianceId = ComplianceId;
            var ismamed = obj.GetFormComplianceMapping(ComplianceId);
            if (ismamed.Count() > 0)
            {
                _objForm.New = false;
            }
            else
            {
                _objForm.New = true;
            }
            return PartialView("_OpenFormComplianceMapping", _objForm);
        }


        #region Form Compliance Mapping add by Ruchi on 25th Feb



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult save_checkedFormCompliancemapping([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<FormDetails> complianceListmapping)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            bool flag = false;
            if (complianceListmapping != null && ModelState.IsValid)
            {
                foreach (var complianceItem in complianceListmapping)
                {
                    if (complianceItem.IsCheked)
                    {
                        obj.AddFormDetailsItem(complianceItem, UserID, CustomerID);

                        flag = true;
                    }
                }
            }

            return Json(complianceListmapping.ToDataSourceResult(request, ModelState));
        }

        public ActionResult AgendaMappingDetails([DataSourceRequest] DataSourceRequest request, long ComplianceID)
        {
           
            List<FormDetails> objagendacompliance = new List<FormDetails>();
            if (ComplianceID > 0)
            {

                objagendacompliance = obj.GetFormComplianceMapping(ComplianceID);
            }
            return Json(objagendacompliance.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteComplianceAgendaMapping([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<FormDetails> objFormCompliance)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            bool flag = false;
            if (objFormCompliance != null)
            {
                foreach (var formItem in objFormCompliance)
                {
                    obj.DeleteFormComlianceDelete(formItem, UserID, CustomerID);
                    flag = true;
                }
            }

            return Json(objFormCompliance.ToDataSourceResult(request, ModelState));
        }
        #endregion
    }
}