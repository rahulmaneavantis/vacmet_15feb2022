﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Annotation;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class AnnotationController : Controller
    {
        IAnnotationService objIAnnotationService;
        IMeeting_Service objIMeeting_Service;
        public AnnotationController(IAnnotationService objAnnotationService, IMeeting_Service objMeeting_Service)
        {
            objIAnnotationService = objAnnotationService;
            objIMeeting_Service = objMeeting_Service;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        public ActionResult AgendaAnnotate(long meetingId, long meetingParticipantId, string docType)
        {
            return AgendaAnnotateAll(meetingId, meetingParticipantId, docType, null, false);
        }

        public ActionResult AgendaAnnotateDraftMinute(long meetingId, long meetingParticipantId, string docType, long? draftCirculationID, bool viewOnly)
        {
            return AgendaAnnotateAll(meetingId, meetingParticipantId, docType, draftCirculationID, viewOnly);
        }

        public ActionResult AgendaAnnotateAll(long meetingId, long meetingParticipantId, string docType, long? draftCirculationID, bool viewOnly)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var role = AuthenticationHelper.Role;
            var model = objIAnnotationService.GetAnnotation(meetingId, meetingParticipantId, docType, userId, role, customerId, draftCirculationID);
            if(model != null)
            {
                model.ViewOnly = viewOnly;
                if (draftCirculationID > 0)
                {
                    model.DraftCirculationID_ = draftCirculationID;
                    model.IsDraftMinutesApproved = objIMeeting_Service.IsDraftMinutesApproved(Convert.ToInt64(draftCirculationID));
                }
            }
            return View("AgendaAnnotate", model);
        }
    }
}