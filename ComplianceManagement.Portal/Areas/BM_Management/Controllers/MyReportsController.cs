﻿using BM_ManegmentServices;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Registers;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Services.Compliance;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyReportsController : Controller
    {
        // GET: BM_Management/MyReports

        ICompliance_Service objComp;

        public MyReportsController(ICompliance_Service objComp)
        {
            this.objComp = objComp;
        }
        public ActionResult Index()
        {
            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR || AuthenticationHelper.Role == SecretarialConst.Roles.SMNGT)
            {
                return View();
            }
            else if (AuthenticationHelper.Role == SecretarialConst.Roles.SMNGT)
            {
                return View();
            }
            else
            {
                return View("StatusReportCS");
            }
        }

        public ActionResult AttedanceReports()
        {
            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View("AttendanceReport");
            }
            else if (AuthenticationHelper.Role == SecretarialConst.Roles.SMNGT)
            {
                return View("AttendanceReport");
            }
            else
            {
                return View("AttendanceReportCS");
            }
        }


        public ActionResult ViewAttendanceCS(int userID, int customerID, string userRole, int Meeting_Id)
        {
            //List<MemberSecurities> MemberRegisterSecurities = new List<MemberSecurities>();
            //List<MemberRegister> RegisterDetails = new List<MemberRegister>();

            List<BM_SP_AttendanceReportCSDetail_Result> GetAttendanceReportDetail = new List<BM_SP_AttendanceReportCSDetail_Result>();

            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            GetAttendanceReportDetail = objComp.GetAttendanceReportCSDetail(userID, customerID, userRole, Meeting_Id, 0, "");
            //RegisterDetails = objregister.GetMemberofRegisterDetails(EntityID, MemberId, CustomerID);
            //if (RegisterDetails.Count > 0)
            //{

            int LastCol = 100;
            string Company = "";
            string CIN = "";
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    //foreach (var member in RegisterDetails)
                    //{
                    //MemberRegisterSecurities = objregister.GetMemberSecurities(EntityID, CustomerID, member.MemberId);
                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Attendace_Report_" + CustomerID);


                    int Rows = 8;
                    int Col = 3;
                    foreach (var member in GetAttendanceReportDetail)
                    {

                        Company = member.CompanyName;
                        CIN = member.CIN;
                        exWorkSheet1.Cells["B" + Rows + ""].Value = Convert.ToDateTime(member.MeetingDate).ToString("dd, MMMM yyyy");

                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //exWorkSheet1.Cells["B" + Rows + ""].Style.Numberformat=;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.WrapText = false;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B" + Rows + ""].AutoFitColumns(10);
                        //foreach (var memberCol in GetAttendanceReportDetail)
                        //{

                        exWorkSheet1.Cells[Rows - 1, Col].Value = member.DirectorName;

                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.WrapText = false;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Font.Bold = true;
                        exWorkSheet1.Cells[Rows - 1, Col].AutoFitColumns(10);


                        exWorkSheet1.Cells[8, Col].Value = member.Present;
                        exWorkSheet1.Cells[8, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                        exWorkSheet1.Cells[8, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells[8, Col].Style.WrapText = false;
                        if (member.Present == "YES")
                            exWorkSheet1.Cells[8, Col].Style.Font.Color.SetColor(Color.Green);
                        else
                            exWorkSheet1.Cells[8, Col].Style.Font.Color.SetColor(Color.Red);



                        //}
                        Col = Col + 1;
                        //Rows = Rows+1;
                    }

                    LastCol = Col;

                    exWorkSheet1.Cells["B3"].Value = Company;

                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["B3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B3"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B3"].Style.Font.Size = 16;
                    exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                    //exWorkSheet1.Cells["B3"].Style.WrapText = true;

                    exWorkSheet1.Cells["B4"].Value = "(CIN: " + CIN + ") ";
                    //exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                    exWorkSheet1.Cells[4, 2, 4, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                    //exWorkSheet1.Cells["B3"].Style.WrapText = true;

                    exWorkSheet1.Cells["B5"].Value = "";
                    //exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //exWorkSheet1.Row(6).Height = 31.25;

                    exWorkSheet1.Cells["B6"].Value = "Meeting Detail";
                    exWorkSheet1.Cells["B6"].AutoFitColumns(8);
                    ////exWorkSheet1.Column(8).Width = 25;
                    //exWorkSheet1.Cells["B6:G6"].Merge = true;
                    exWorkSheet1.Cells["B6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet1.Cells["B6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B6"].Style.Font.Bold = true;
                    //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                    exWorkSheet1.Cells["B6"].Style.WrapText = true;

                    exWorkSheet1.Cells["C6"].Value = "Name of the Directors ";
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["C6"].Style.WrapText = false;
                    exWorkSheet1.Cells["C6"].Style.Font.Bold = true;

                    //exWorkSheet1.Cells["C6"].AutoFitColumns(8);





                }
                catch (Exception ex)
                {
                    //  LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                CustomerID = Convert.ToInt16(AuthenticationHelper.CustomerID);
                path = "~/Temp/" + CustomerID + "/";
                Filename = "Attendance_of_Member.xlsx";
                if (!Directory.Exists(Server.MapPath(path)))
                {
                    Directory.CreateDirectory(Server.MapPath(path));
                }
                _path = System.IO.Path.Combine(Server.MapPath(path), Filename);
                FileStream aFile = new FileStream(_path, FileMode.Create);
                byte[] byData = exportPackge.GetAsByteArray();
                aFile.Seek(0, SeekOrigin.Begin);
                aFile.Write(byData, 0, byData.Length);
                aFile.Close();
            }

            return Json(Filename, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json("", JsonRequestBehavior.AllowGet);
            //}
        }

        public ActionResult ExportAttendanceCSDetail(int userID, int customerID, string userRole, int Meeting_Id)
        {           
            List<BM_SP_AttendanceReportCSDetail_Result> GetAttendanceReportDetail = new List<BM_SP_AttendanceReportCSDetail_Result>();

            string _path;
            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;

            GetAttendanceReportDetail = objComp.GetAttendanceReportCSDetail(userID, customerID, userRole, Meeting_Id, 0, "");
            
            int LastCol = 100;
            string Company = "";
            string CIN = "";

            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {                    
                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(GetAttendanceReportDetail[0].MeetingTitle);
                    //ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Attendace_Report_" + CustomerID);
                    
                    int Rows = 8;
                    int Col = 3;

                    foreach (var member in GetAttendanceReportDetail)
                    {
                        Company = member.CompanyName;
                        CIN = member.CIN;

                        exWorkSheet1.Cells["B" + Rows + ""].Value = Convert.ToDateTime(member.MeetingDate).ToString("dd, MMMM yyyy");

                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //exWorkSheet1.Cells["B" + Rows + ""].Style.Numberformat=;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.WrapText = true;
                        exWorkSheet1.Cells["B" + Rows + ""].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B" + Rows + ""].AutoFitColumns(10);
                        
                        exWorkSheet1.Cells[Rows - 1, Col].Value = member.DirectorName;

                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.WrapText = false;
                        exWorkSheet1.Cells[Rows - 1, Col].Style.Font.Bold = true;
                        exWorkSheet1.Cells[Rows - 1, Col].AutoFitColumns(10);


                        exWorkSheet1.Cells[8, Col].Value = member.Present;
                        exWorkSheet1.Cells[8, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        exWorkSheet1.Cells[8, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                        exWorkSheet1.Cells[8, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells[8, Col].Style.WrapText = false;

                        if (member.Present == "YES")
                            exWorkSheet1.Cells[8, Col].Style.Font.Color.SetColor(Color.Green);
                        else
                            exWorkSheet1.Cells[8, Col].Style.Font.Color.SetColor(Color.Red);

                        Col = Col + 1;
                    }

                    LastCol = Col;

                    exWorkSheet1.Cells["B3"].Value = Company;

                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["B3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B3"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B3"].Style.Font.Size = 16;
                    exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                    //exWorkSheet1.Cells["B3"].Style.WrapText = true;

                    exWorkSheet1.Cells["B4"].Value = "(CIN: " + CIN + ") ";
                    //exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                    exWorkSheet1.Cells[4, 2, 4, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                    //exWorkSheet1.Cells["B3"].Style.WrapText = true;

                    exWorkSheet1.Cells["B5"].Value = "";
                    //exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //exWorkSheet1.Row(6).Height = 31.25;

                    exWorkSheet1.Cells["B6"].Value = "Meeting Detail";
                    exWorkSheet1.Cells["B6"].AutoFitColumns(8);
                    ////exWorkSheet1.Column(8).Width = 25;
                    //exWorkSheet1.Cells["B6:G6"].Merge = true;
                    exWorkSheet1.Cells["B6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["B6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    exWorkSheet1.Cells["B6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B6"].Style.Font.Bold = true;
                    //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                    exWorkSheet1.Cells["B6"].Style.WrapText = true;

                    exWorkSheet1.Cells["C6"].Value = "Name of the Directors ";
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Merge = true;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells[6, 3, 6, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    exWorkSheet1.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["C6"].Style.WrapText = false;
                    exWorkSheet1.Cells["C6"].Style.Font.Bold = true;
                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                byte[] data = exportPackge.GetAsByteArray();
                return File(data, "application/octet-stream", "AttendanceReport-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
            }
        }

        public ActionResult ExportAllAttendanceCSDetail(int userID, int customerID, string userRole, int Meeting_Id, List<string> CompanyList, List<string> FinYr)
        {
            List<BM_SP_AttendanceReportCSDetail_Result> GetAttendanceReportDetail = new List<BM_SP_AttendanceReportCSDetail_Result>();

            int CustomerID = (int)(AuthenticationHelper.CustomerID);

            string Files = string.Empty;
            string path = string.Empty;
            string Filename = string.Empty;
            int CompanyId = 0;
            string FineYr = "";

            char[] spearator = { ',', ' ' };
            string[] CompanyArr = CompanyList[0].ToString().Split(spearator);

            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    if (CompanyArr.Length > 0)
                    {
                        for (int i = 0; i < CompanyArr.Length; i++)
                        {
                            int LastCol = 100;
                            string Company = "";
                            string CIN = "";
                            int NewMeetingId = 0;

                            Dictionary<int, string> Director = new Dictionary<int, string>();
                            Dictionary<int, int> Meeting = new Dictionary<int, int>();
                            Dictionary<int, int> StrtEnd = new Dictionary<int, int>();

                            if (!string.IsNullOrEmpty(CompanyArr[i].Trim()))
                                CompanyId = Convert.ToInt32(CompanyArr[i]);
                            FineYr = FinYr[0].ToString();

                            GetAttendanceReportDetail = objComp.GetAttendanceReportCSDetail(userID, customerID, userRole, 0, CompanyId, FineYr);

                            if (GetAttendanceReportDetail != null)
                            {
                                if (GetAttendanceReportDetail.Count > 0)
                                {
                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(GetAttendanceReportDetail[0].CompanyName);
                                    //ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Attendace_Report_" + GetAttendanceReportDetail[0].CompanyName);

                                    int Rows = 8;
                                    int Col = 4;
                                    StrtEnd.Add(Rows, Col);
                                    int StartRow = Rows;
                                    int StartCol = Col;

                                    foreach (var member in GetAttendanceReportDetail)
                                    {
                                        if (NewMeetingId == 0)
                                            NewMeetingId = Convert.ToInt32(member.Meeting_ID);

                                        if (NewMeetingId == member.Meeting_ID)
                                        {
                                            Company = member.CompanyName;
                                            CIN = member.CIN;

                                            exWorkSheet1.Cells["B" + Rows + ""].Value = Convert.ToDateTime(member.MeetingDate).ToString("dd, MMMM yyyy");
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            //exWorkSheet1.Cells["B" + Rows + ""].Style.Numberformat=;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.WrapText = false;
                                            exWorkSheet1.Cells["B" + Rows + ""].Style.Font.Bold = true;
                                            exWorkSheet1.Cells["B" + Rows + ""].AutoFitColumns(10);

                                            exWorkSheet1.Cells["C" + Rows + ""].Value = member.MeetingTypeName;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            //exWorkSheet1.Cells["C" + Rows + ""].Style.Numberformat=;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.WrapText = false;
                                            exWorkSheet1.Cells["C" + Rows + ""].Style.Font.Bold = true;
                                            exWorkSheet1.Cells["C" + Rows + ""].AutoFitColumns(10);

                                            exWorkSheet1.Cells[7, Col].Value = member.DirectorName;
                                            exWorkSheet1.Cells[7, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[7, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[7, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[7, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[7, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                                            exWorkSheet1.Cells[7, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            exWorkSheet1.Cells[7, Col].Style.WrapText = false;
                                            exWorkSheet1.Cells[7, Col].Style.Font.Bold = true;
                                            exWorkSheet1.Cells[7, Col].AutoFitColumns(10);

                                            Director.Add(Col, member.DirectorName);

                                            var KeyMeeting = Meeting.FirstOrDefault(x => x.Value == member.Meeting_ID).Key;
                                            if (KeyMeeting == 0)
                                            {
                                                Meeting.Add(Rows, Convert.ToInt32(member.Meeting_ID));
                                            }

                                            exWorkSheet1.Cells[8, Col].Value = member.Present;
                                            exWorkSheet1.Cells[8, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[8, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[8, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[8, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            exWorkSheet1.Cells[8, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                                            exWorkSheet1.Cells[8, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                            exWorkSheet1.Cells[8, Col].Style.WrapText = false;

                                            if (member.Present == "YES" || member.Present == "YES(VC)")
                                                exWorkSheet1.Cells[8, Col].Style.Font.Color.SetColor(Color.Green);
                                            else
                                                exWorkSheet1.Cells[8, Col].Style.Font.Color.SetColor(Color.Red);

                                            Col = Col + 1;
                                            NewMeetingId = Convert.ToInt32(member.Meeting_ID);
                                        }
                                        else
                                        {
                                            var KeyMeeting = Meeting.FirstOrDefault(x => x.Value == member.Meeting_ID).Key;
                                            if (KeyMeeting == 0)
                                            {
                                                Rows = Rows + 1;

                                                Meeting.Add(Rows, Convert.ToInt32(member.Meeting_ID));

                                                exWorkSheet1.Cells["B" + Rows + ""].Value = Convert.ToDateTime(member.MeetingDate).ToString("dd, MMMM yyyy");
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                                //exWorkSheet1.Cells["B" + Rows + ""].Style.Numberformat=;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.WrapText = false;
                                                exWorkSheet1.Cells["B" + Rows + ""].Style.Font.Bold = true;
                                                exWorkSheet1.Cells["B" + Rows + ""].AutoFitColumns(10);

                                                exWorkSheet1.Cells["C" + Rows + ""].Value = member.MeetingTypeName;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                                //exWorkSheet1.Cells["C" + Rows + ""].Style.Numberformat=;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.WrapText = false;
                                                exWorkSheet1.Cells["C" + Rows + ""].Style.Font.Bold = true;
                                                exWorkSheet1.Cells["C" + Rows + ""].AutoFitColumns(10);
                                            }

                                            var myKey = Director.FirstOrDefault(x => x.Value == member.DirectorName).Key;

                                            if (myKey != 0)
                                            {
                                                exWorkSheet1.Cells[Rows, myKey].Value = member.Present;
                                                exWorkSheet1.Cells[Rows, myKey].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, myKey].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, myKey].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, myKey].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, myKey].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                                //exWorkSheet1.Cells["B" + myKey + ""].Style.Numberformat = ExcelNumberFormat;
                                                exWorkSheet1.Cells[Rows, myKey].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                exWorkSheet1.Cells[Rows, myKey].Style.WrapText = false;
                                                if (member.Present == "YES" || member.Present == "YES(VC)")
                                                    exWorkSheet1.Cells[Rows, myKey].Style.Font.Color.SetColor(Color.Green);
                                                else
                                                    exWorkSheet1.Cells[Rows, myKey].Style.Font.Color.SetColor(Color.Red);

                                            }
                                            else
                                            {
                                                exWorkSheet1.Cells[7, Col].Value = member.DirectorName;

                                                exWorkSheet1.Cells[7, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[7, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[7, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[7, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[7, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                                //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                                                exWorkSheet1.Cells[7, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                exWorkSheet1.Cells[7, Col].Style.WrapText = false;
                                                exWorkSheet1.Cells[7, Col].Style.Font.Bold = true;
                                                exWorkSheet1.Cells[7, Col].AutoFitColumns(10);
                                                Director.Add(Col, member.DirectorName);

                                                exWorkSheet1.Cells[Rows, Col].Value = member.Present;
                                                exWorkSheet1.Cells[Rows, Col].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, Col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, Col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, Col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[Rows, Col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                                //exWorkSheet1.Cells["B" + Col + ""].Style.Numberformat = ExcelNumberFormat;
                                                exWorkSheet1.Cells[Rows, Col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                exWorkSheet1.Cells[Rows, Col].Style.WrapText = false;
                                                if (member.Present == "YES" || member.Present == "YES(VC)")
                                                    exWorkSheet1.Cells[Rows, Col].Style.Font.Color.SetColor(Color.Green);
                                                else
                                                    exWorkSheet1.Cells[Rows, Col].Style.Font.Color.SetColor(Color.Red);

                                                Col = Col + 1;
                                                //NewMeetingId = Convert.ToInt32(member.Meeting_ID);
                                            }

                                        }

                                        if (0 == 1)
                                        {
                                            Col = Col + 1;
                                            NewMeetingId = Convert.ToInt32(member.Meeting_ID);
                                        }
                                    }

                                    LastCol = Col;

                                    exWorkSheet1.Cells["B3"].Value = Company;

                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Merge = true;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["B3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["B3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B3"].Style.Font.Size = 16;
                                    exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["B3"].AutoFitColumns(10);
                                    //exWorkSheet1.Cells["B3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["B4"].Value = "(CIN: " + CIN + ") ";
                                    //exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells[4, 2, 4, LastCol - 1].Merge = true;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[3, 2, 3, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["B4"].AutoFitColumns(10);
                                    //exWorkSheet1.Cells["B3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["B5"].Value = "";
                                    //exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Merge = true;
                                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[5, 2, 5, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["B5"].AutoFitColumns(10);
                                    //exWorkSheet1.Row(6).Height = 31.25;

                                    exWorkSheet1.Cells["B6"].Value = "Meeting Date";
                                    exWorkSheet1.Cells["B6"].AutoFitColumns(8);
                                    ////exWorkSheet1.Column(8).Width = 25;
                                    //exWorkSheet1.Cells["B6:G6"].Merge = true;
                                    exWorkSheet1.Cells[6, 2, 7, 2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[6, 2, 7, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[6, 2, 7, 2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[6, 2, 7, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["B6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["B6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["B6"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B6"].AutoFitColumns(10);
                                    //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["B6"].Style.WrapText = true;
                                    exWorkSheet1.Cells[6, 2, 7, 2].Merge = true;

                                    exWorkSheet1.Cells["C6"].Value = "Meeting Type";
                                    exWorkSheet1.Cells["C6"].AutoFitColumns(8);
                                    ////exWorkSheet1.Column(8).Width = 25;
                                    //exWorkSheet1.Cells["C6:G6"].Merge = true;
                                    exWorkSheet1.Cells["C6"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C6"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C6"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["C6"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C6"].AutoFitColumns(10);
                                    //exWorkSheet1.Cells["A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    //exWorkSheet1.Cells["A6"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells[6, 3, 7, 3].Merge = true;

                                    exWorkSheet1.Cells["D6"].Value = "Name of the Directors";
                                    exWorkSheet1.Cells[6, 4, 6, LastCol - 1].Merge = true;
                                    exWorkSheet1.Cells[6, 4, 6, LastCol - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[6, 4, 6, LastCol - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[6, 4, 6, LastCol - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[6, 4, 6, LastCol - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    exWorkSheet1.Cells["D6"].Style.WrapText = false;
                                    exWorkSheet1.Cells["D6"].Style.Font.Bold = true;

                                    //StrtEnd.Add(Rows-1, Col-1);

                                    exWorkSheet1.Column(2).AutoFit();

                                    int EndRow = Rows;
                                    int EndCol = Col - 1;

                                    for (int L = StartRow; L <= EndRow; L++)
                                    {
                                        for (int M = StartCol; M <= EndCol; M++)
                                        {
                                            if (exWorkSheet1.Cells[L, M].Text == "" || exWorkSheet1.Cells[L, M].Text == " ")
                                            {

                                                exWorkSheet1.Cells[L, M].Value = "NA";
                                                exWorkSheet1.Cells[L, M].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[L, M].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[L, M].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[L, M].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                                exWorkSheet1.Cells[L, M].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                                //exWorkSheet1.Cells["B" + M + ""].Style.Numberformat = ExcelNumberFormat;
                                                exWorkSheet1.Cells[L, M].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                                exWorkSheet1.Cells[L, M].Style.WrapText = false;
                                                exWorkSheet1.Cells[L, M].Style.Font.Color.SetColor(Color.Black);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                byte[] data = null;

                if (exportPackge.Workbook.Worksheets.Count > 0)
                    data = exportPackge.GetAsByteArray();
                return File(data, "application/octet-stream", "AttendanceReport-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");

            }           
        }
    }
}