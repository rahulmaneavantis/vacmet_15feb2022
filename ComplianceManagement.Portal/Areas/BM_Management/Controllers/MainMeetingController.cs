﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Fields;
using Telerik.Windows.Documents.Flow.Model.Styles;

using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.InteractiveForms;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using System.IO;
using System.Reflection;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using System.Windows.Media;
using System.Windows;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Setting;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MainMeetingController : Controller
    {
        // GET: BM_Management/MeetingPurpose
        IMainMeeting obj;
        ITemplateservice objITemplateservice;
        IMeeting_Service objIMeeting_Service;
        ISettingService objISettingService;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        int userId = AuthenticationHelper.UserID;
        public MainMeetingController(IMainMeeting obj, ITemplateservice objTemplateservice, IMeeting_Service objMeeting_Service, ISettingService objSettingService)
        {
            this.obj = obj;
            objITemplateservice = objTemplateservice;
            objIMeeting_Service = objMeeting_Service;
            objISettingService = objSettingService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Join(string id)
        {
            var strMeetingId = Util.CalculateAESHashDecrypt(id);
            long MeetingId = 0;
            long.TryParse(strMeetingId, out MeetingId);

            return MeetingsDetails(MeetingId);
        }

        public ActionResult MeetingsDetails(long MeetingId)
        {
            MeetingAttendance_VM _meetingattendence = new MeetingAttendance_VM();
            int? getRole = obj.getRole(userId);
            //long participantid = obj.getuserparticipantId(MeetingId, userId);
            _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(MeetingId,CustomerId);
            //_meetingattendence.MeetingId = MeetingId;
            //if (getRole > 0)
            //{

            //    _meetingattendence.Role = (int)getRole;
            //    if (_meetingattendence.Role == 21 && participantid > 0)
            //    {
            //        _meetingattendence.ParticipantId = participantid;
            //    }
            //}
            _meetingattendence = obj.checkforQuorum(MeetingId, CustomerId, _meetingattendence.startstopeDateVM.EntityId);
            _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(MeetingId,CustomerId);
            if (getRole > 0)
            {
                _meetingattendence.Role = (int)getRole;
                //if (_meetingattendence.Role == 21 && participantid > 0)
                //{
                //    _meetingattendence.ParticipantId = participantid;
                //}
            }
            var participantDetails = obj.GetParticipantDetails(MeetingId, userId);
            if (participantDetails != null)
            {
                _meetingattendence.ParticipantId = participantDetails.MeetingParticipantId;
                _meetingattendence.ParticipantType = participantDetails.ParticipantType;
                _meetingattendence.CanVoteNote = participantDetails.CanVoteNote;
            }
            LoadMeetingAgenda(MeetingId);
            CountTotalSharedNotes(MeetingId, userId);
            _meetingattendence.MeetingId = MeetingId;
            return View("MeetingsDetails", _meetingattendence);
        }

        public ActionResult MeetingAttendanceNew(long MeetingId)
        {
            MeetingAttendance_VM _meetingattendence = new MeetingAttendance_VM();
            int? getRole = obj.getRole(userId);
            long participantid = obj.getuserparticipantId(MeetingId, userId);
            _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(MeetingId,CustomerId);
            _meetingattendence.MeetingId = MeetingId;
            //if (getRole > 0)
            //{
            //    _meetingattendence.Role = (int)getRole;
            //    if (_meetingattendence.Role == 21 && participantid > 0)
            //    {
            //        _meetingattendence.ParticipantId = participantid;
            //    }
            //}

            _meetingattendence = obj.checkforQuorum(MeetingId, CustomerId, _meetingattendence.startstopeDateVM.EntityId);
            _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(MeetingId, CustomerId);
            if (getRole > 0)
            {
                _meetingattendence.Role = (int)getRole;
                //if (_meetingattendence.Role == 21 && participantid > 0)
                //{
                //    _meetingattendence.ParticipantId = participantid;
                //}
            }
            var participantDetails = obj.GetParticipantDetails(MeetingId, userId);
            if (participantDetails != null)
            {
                _meetingattendence.ParticipantId = participantDetails.MeetingParticipantId;
                _meetingattendence.ParticipantType = participantDetails.ParticipantType;
                _meetingattendence.CanVoteNote = participantDetails.CanVoteNote;
            }
            LoadMeetingAgenda(MeetingId);
            CountTotalSharedNotes(MeetingId, userId);
            _meetingattendence.MeetingId = MeetingId;
            _meetingattendence.ShowAttendenceMessage = true;
            return PartialView("_MeetingAttendanceNew", _meetingattendence);
        }

        [NonAction]
        public void LoadMeetingAgenda(long MeetingId)
        {
            //ViewBag.DetailsOfAgenda_Grid = obj.GetMeetingAgenda(MeetingId).OrderBy(a => a.SrNo).ThenBy(a2 => a2.AgendaPart);
            ViewBag.DetailsOfAgenda_Grid = obj.GetMeetingAgenda(MeetingId);
        }
        [NonAction]
        public void CountTotalSharedNotes(long MeetingId, int userId)
        {
            ViewBag.CountSharedNotedUserwise = obj.getTotalNotesUserwise(MeetingId, userId);
        }
        [HttpGet]
        public ActionResult GetAgendaById(long AgendaId, long MeetingId, long MappingId)
        {
            MainMeeting_VM _meetingaddendence = new MainMeeting_VM();
            _meetingaddendence = obj.GetAgendabyIDforMainContent(MappingId);
            if (_meetingaddendence != null)
            {
                MeetingAgendaData objajendavoting = new MeetingAgendaData();
                //LoadMeetingAgenda(MeetingId);
                //objajendavoting = obj.GetMeetingResult(_meetingaddendence.MeetingId, (long)_meetingaddendence.AgendaId, (long)_meetingaddendence.MeetingAgendaMappingID);

                string Role = AuthenticationHelper.Role;
                if (Role != SecretarialConst.Roles.HDCS && Role != SecretarialConst.Roles.CS)
                {
                    _meetingaddendence.TemplateFieldEditable = false;
                }
            }
            return PartialView("_AgendaMainContents", _meetingaddendence);
        }

        #region Meeting Resolution
        public ActionResult EditResolution(long meetingId, long meetingAgendaMappingId)
        {
            MeetingResolution _objmeetingResolution = new MeetingResolution();
            _objmeetingResolution = obj.GetMeetingAgendaResolution(meetingId, meetingAgendaMappingId);
            return PartialView("_MeetingResolution", _objmeetingResolution);
        }
        [HttpPost]
        public ActionResult UpdateResolution(MeetingResolution _objmeetingResolution)
        {
            if (ModelState.IsValid)
            {
                _objmeetingResolution = obj.updateMeetingResolution(_objmeetingResolution);
            }
            return PartialView("_MeetingResolution", _objmeetingResolution);
        }
        #endregion

        #region Attendance

        [HttpGet]
        public ActionResult GetMeetingAttendance([DataSourceRequest] DataSourceRequest request, long MeetingID)
        {
            List<MeetingAttendance_VM> _meetingattendence = new List<MeetingAttendance_VM>();
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = AuthenticationHelper.UserID;
            string role = AuthenticationHelper.Role;
            if (role == SecretarialConst.Roles.HDCS || role == SecretarialConst.Roles.CS)
            {
                _meetingattendence = obj.GetPerticipenforAttendenceCS(MeetingID, customerId, userId);
            }
            else
            {
                _meetingattendence = obj.GetPerticipenforAttendence(MeetingID, customerId, userId);
            }
            return Json(_meetingattendence.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMeetingRSVP([DataSourceRequest] DataSourceRequest request, long MeetingID)
        {
            var _meetingattendence = obj.GetParticipantRSVP(MeetingID);
            return Json(_meetingattendence.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMeetingAttendanceofDirector([DataSourceRequest] DataSourceRequest request, long MeetingID)
        {
            List<MeetingAttendance_VM> _meetingattendence = new List<MeetingAttendance_VM>();
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = AuthenticationHelper.UserID;
            string role = AuthenticationHelper.Role;

            _meetingattendence = obj.GetPerticipenforAttendence(MeetingID, customerId, userId);

            return Json(_meetingattendence.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAttendenceResponse([DataSourceRequest] DataSourceRequest request,
        [Bind(Prefix = "models")] IEnumerable<MeetingAttendance_VM> _meetingattendence)
        {
            long MeetingId = 0;
            if (_meetingattendence != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in _meetingattendence)
                {
                    var result = obj.UpdateMeetingAttendence(item, UserId);
                    MeetingId = item.MeetingId;
                }
            }
            else
            {

            }
            MeetingAttendance_VM Checkquaram = new MeetingAttendance_VM();

            //Checkquaram = obj.checkforQuorum(MeetingId);
            return Json(_meetingattendence.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult UpdateAttendenceResponseForAuditor([DataSourceRequest] DataSourceRequest request,
        [Bind(Prefix = "models")] IEnumerable<MeetingAttendance_VM> _meetingattendence)
        {
            long MeetingId = 0;
            if (_meetingattendence != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in _meetingattendence)
                {
                    var result = obj.UpdateMeetingAttendenceforAuditor(item, UserId);
                    MeetingId = item.MeetingId;
                }
            }
            else
            {

            }
            MeetingAttendance_VM Checkquaram = new MeetingAttendance_VM();

            //Checkquaram = obj.checkforQuorum(MeetingId);
            return Json(_meetingattendence.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult UpdateAttendenceResponseforInvitee([DataSourceRequest] DataSourceRequest request,
       [Bind(Prefix = "models")] IEnumerable<MeetingAttendance_VM> _meetingattendence)
        {
            long MeetingId = 0;
            if (_meetingattendence != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in _meetingattendence)
                {
                    var result = obj.UpdateMeetingAttendenceforInvitee(item, UserId);
                    MeetingId = item.MeetingId;
                }
            }
            else
            {

            }
            MeetingAttendance_VM Checkquaram = new MeetingAttendance_VM();

            //Checkquaram = obj.checkforQuorum(MeetingId);
            return Json(_meetingattendence.ToDataSourceResult(request, ModelState));
        }
        #endregion Attendence


        #region Agenda

        [HttpGet]
        public ActionResult ReloadPieChart(long MeetingId, long AgendaId, long MappingId)
        {
            MeetingAgendaData objajendavoting = new MeetingAgendaData();
            objajendavoting = obj.GetMeetingResult(MeetingId, AgendaId, MappingId);
            return PartialView("_MeetingAgendaSummary", objajendavoting);
        }
        [HttpGet]
        public ActionResult MeetingVotingAgendaResponse([DataSourceRequest] DataSourceRequest request, long MeetingId, long AgendaId, long MappingId)
        {
            List<AgendaVoting> _agendavoting = new List<AgendaVoting>();
            int userId = AuthenticationHelper.UserID;
            string Role = AuthenticationHelper.Role;
            if (Role == SecretarialConst.Roles.HDCS || Role == SecretarialConst.Roles.CS)
            {
                _agendavoting = obj.GetMeetingPerticipent(MeetingId, AgendaId, MappingId);
            }
            else
            {
                _agendavoting = obj.GetMeetingPerticipentbyuser(MeetingId, AgendaId, MappingId, userId);
            }
            //LoadMeetingAgendaData(MeetingId, AgendaId);
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("Castingvote", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(_agendavoting.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult MeetingVotingbyDirector([DataSourceRequest] DataSourceRequest request, long MeetingId, long AgendaId, long MappingId)
        {
            List<AgendaVoting> _agendavoting = new List<AgendaVoting>();
            int userId = AuthenticationHelper.UserID;
            string Role = AuthenticationHelper.Role;
            if (Role == SecretarialConst.Roles.HDCS || Role == SecretarialConst.Roles.CS)
            {
                _agendavoting = obj.GetMeetingPerticipent(MeetingId, AgendaId, MappingId);
            }
            else
            {
                _agendavoting = obj.GetMeetingPerticipentbyuser(MeetingId, AgendaId, MappingId, userId);
            }
            LoadMeetingAgendaData(MeetingId, AgendaId);
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("Castingvote", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(_agendavoting.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AGMVotingAgendaResponse([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            List<AGMVoting> _agendavoting = new List<AGMVoting>();

            _agendavoting = obj.GetAgendaForAgm(MeetingId);

            return Json(_agendavoting.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAGMAgendaResponse(AGMVoting agendaResponse)
        {
            if (agendaResponse != null && ModelState.IsValid)
            {
                agendaResponse = obj.UpdateAGMAgendaResponse(agendaResponse);
            }
            return Json(agendaResponse, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateAgendaResponse([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<AGMVoting> model)
        {
            var isComplianceReopen = false;
            var message = "";
            int UserId = AuthenticationHelper.UserID;
            if (model != null && ModelState.IsValid)
            {
                model = obj.UpdateVirtualMeetingsAgendaResponse(model.ToList(), UserId);

                if (model != null)
                {
                    if (model.ToList().Count > 0)
                    {
                        var meetingId = model.ToList().FirstOrDefault().MeetingID;
                        message = "Saved Successfully.";
                        if (meetingId > 0)
                        {
                            var isDraftComplianceReopen = objIMeeting_Service.ReOpenMeetingLevelCompliance(meetingId, "CirculateDraft");
                            var isMinutesComplianceReopen = objIMeeting_Service.ReOpenMeetingLevelCompliance(meetingId, "FinalizedMinutes");

                            if (isDraftComplianceReopen == true || isMinutesComplianceReopen == true)
                            {
                                isComplianceReopen = true;
                            }
                        }
                    }
                }
            }
            return Json(new { Message = message, IsComplianceReopen = isComplianceReopen }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMeetingAgendaResponse1([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<AgendaVoting> agendaResponse)
        {
            var isComplianceReopen = false;
            var message = "";
            AgendaVoting objagenda = new AgendaVoting();
            if (agendaResponse != null && ModelState.IsValid)
            {
                var reOpenAgenda = false;
                long meetingId = 0;
                foreach (var item in agendaResponse)
                {
                    int UserId = Convert.ToInt32(AuthenticationHelper.UserID);

                    objagenda = obj.UpdateMeetingAgendaResponse(item, UserId);
                    reOpenAgenda = true;
                    meetingId = item.MeetingId;
                }

                if (reOpenAgenda == true && meetingId > 0)
                {
                    message = "Saved Successfully.";
                    var isDraftComplianceReopen = objIMeeting_Service.ReOpenMeetingLevelCompliance(meetingId, "CirculateDraft");
                    var isMinutesComplianceReopen = objIMeeting_Service.ReOpenMeetingLevelCompliance(meetingId, "FinalizedMinutes");

                    if (isDraftComplianceReopen == true || isMinutesComplianceReopen == true)
                    {
                        isComplianceReopen = true;
                    }
                }
            }
            return Json(new { Message = message, IsComplianceReopen = isComplianceReopen }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMeetingAgendaResponse([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<AgendaVoting> agendaResponse)
        {
            AgendaVoting objagenda = new AgendaVoting();
            if (agendaResponse != null && ModelState.IsValid)
            {
                foreach (var item in agendaResponse)
                {
                    int UserId = Convert.ToInt32(AuthenticationHelper.UserID);

                    objagenda = obj.UpdateMeetingAgendaResponse(item, UserId);
                }
                foreach (var item1 in agendaResponse)
                {
                    obj.GetAllAgendaDetails(item1.MeetingId, item1.AgendaId, item1.MeetingAgendaMappingId);
                    break;
                }
            }
            return Json(agendaResponse, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public void LoadMeetingAgendaData(long ParentID, long AgendaID)
        {
            //ViewBag.DetailsOfAgenda_Grid = obj.GetAllAgendaDetails(ParentID, AgendaID);
        }
        public ActionResult OpenVoting(long MeetingId, long AgendaId, long MappingId)
        {
            AgendaVoting objajendavoting = new AgendaVoting();
            objajendavoting.AgendaId = AgendaId;
            objajendavoting.MeetingId = MeetingId;
            objajendavoting.MeetingAgendaMappingId = MappingId;
            //objajendavoting.MeetingAgendaVM = obj.GetAllAgendaDetails(MeetingId, AgendaId, MappingId);
            objajendavoting.MeetingAgendaVM = obj.GetMeetingResult(MeetingId, AgendaId, MappingId);
            objajendavoting.Castingvote = objajendavoting.MeetingAgendaVM.MeetingAgendaResultVM.Result;
            return PartialView("_VotingPaperAgendawise", objajendavoting);
        }

        public ActionResult OpenNoting(long MeetingId, long AgendaId, long MappingId)
        {
            AgendaVoting objajendavoting = new AgendaVoting();
            objajendavoting.AgendaId = AgendaId;
            objajendavoting.MeetingId = MeetingId;
            objajendavoting.MeetingAgendaMappingId = MappingId;
            objajendavoting.MeetingAgendaVM = obj.GetMeetingResult(MeetingId, AgendaId, MappingId);
            return PartialView("_MeetingAgendaNoting", objajendavoting);
        }

        [HttpPost]
        public ActionResult GetAgendaResponseChartData(long MeetingID, long AgendaId, long MappingId)
        {
            int UserId = AuthenticationHelper.UserID;
            var model = obj.GetAgendaResponseChartData(MeetingID, AgendaId, MappingId, UserId);
            return Json(model);
        }

        [HttpPost]
        public ActionResult GetChartdetails([DataSourceRequest] DataSourceRequest request, long MeetingID, long AgendaId, long MappingId)
        {
            int UserId = AuthenticationHelper.UserID;
            var model = obj.GetAgendaResponseChartData(MeetingID, AgendaId, MappingId, UserId);
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ElectChairman(ChairpersonElection _objchairmanelection)
        {
            if (ModelState.IsValid)
            {
                _objchairmanelection = obj.UpdateChairmanPerson(_objchairmanelection);
            }
            return PartialView("_ChairPersionEllection", _objchairmanelection);
        }
        [HttpPost]
        public ActionResult TotalMemberPresentInAgm(MeetingAttendance_VM _objTotalMemberPresent)
        {
            bool savesuccess = false;
            if (_objTotalMemberPresent != null)
            {
                int userId = AuthenticationHelper.UserID;
                savesuccess = obj.SaveTotalMemberPresentInAgm(_objTotalMemberPresent, userId);
            }
            return Json(savesuccess, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Start Stop Meeting

        [HttpPost]
        public JsonResult SaveStartMeeting(startstopeDate _objstartstopdate)
        {
            MeetingAttendance_VM _meetingAddendence = new MeetingAttendance_VM();
            if (_objstartstopdate.MeetingId > 0)
            {
                _meetingAddendence.startstopeDateVM = obj.StartMeeting(_objstartstopdate);
            }

            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var accessToDirector = objISettingService.GetAccessToDirector(customerID);

            bool sentLink = false;
            if (accessToDirector && sentLink)
            {
                #region send Join meeting link to participant
                var user = (from MP in entities.BM_MeetingParticipant join U in entities.Users on MP.UserId equals U.ID where MP.Meeting_ID == _objstartstopdate.MeetingId select U).ToList();
                foreach (var item in user)
                {
                    Random random = new Random();
                    int value = random.Next(10000);
                    VerifyOTP OTPDataobj = entities.VerifyOTPs.Where(x => x.UserId == item.ID).FirstOrDefault();
                    if (OTPDataobj != null)
                    {
                        OTPDataobj.CreatedOn = DateTime.Now;
                        OTPDataobj.OTP = value;
                        OTPDataobj.IsVerified = false;
                        entities.SaveChanges();
                    }
                    else
                    {
                        VerifyOTP OTPData = new VerifyOTP();
                        OTPData.UserId = Convert.ToInt32(item.ID);
                        OTPData.EmailId = item.Email;
                        OTPData.MobileNo = Convert.ToInt64(item.ContactNumber);
                        OTPData.OTP = value;
                        OTPData.IsVerified = false;
                        OTPData.CreatedOn = DateTime.Now;
                        entities.VerifyOTPs.Add(OTPData);
                        entities.SaveChanges();
                    }

                    string siteName = ConfigurationManager.AppSettings["DummyPageUrl"];
                    string subject = ConfigurationManager.AppSettings["SubjectActivationCode"];

                    string body = "";

                    var mailBody = objITemplateservice.JoinMeetingTemplate();

                    var url = siteName + "?t=" + HttpUtility.UrlEncode(Util.CalculateAESHash(DateTime.Now.Ticks.ToString())) + "&u=" + HttpUtility.UrlEncode(Util.CalculateAESHash(item.Email)) + "&id=" + HttpUtility.UrlEncode(Util.CalculateAESHash(_objstartstopdate.MeetingId.ToString())) + "&d=djslfnlbdjkll";

                    if (!string.IsNullOrEmpty(mailBody))
                    {
                        //var customerID = (int)AuthenticationHelper.CustomerID;
                        var Meeting = objIMeeting_Service.GetMeetingDetails(customerID, _objstartstopdate.MeetingId);
                        if (Meeting != null)
                        {
                            if (Meeting.MeetingDate != null)
                            {
                                var dt = Convert.ToDateTime(Meeting.MeetingDate);
                                mailBody = mailBody.Replace("{{ Day }}", dt.Day.ToString());
                                mailBody = mailBody.Replace("{{ Month }}", dt.ToString("MMM"));
                                mailBody = mailBody.Replace("{{ Meeting Date }}", dt.ToString("MMM dd, yyyy dddd"));
                            }
                            mailBody = mailBody.Replace("{{ Name of the Company }}", Meeting.EntityName);
                            mailBody = mailBody.Replace("{{ type of meeting }}", Meeting.MeetingTypeName);
                            mailBody = mailBody.Replace("{{ Meeting Time }}", Meeting.MeetingTime);
                            mailBody = mailBody.Replace("{{ Meeting Venue }}", Meeting.MeetingVenue);
                            mailBody = mailBody.Replace("{{ Purpose }}", Meeting.MeetingTitle);
                            mailBody = mailBody.Replace("{{ OTP }}", value.ToString());
                            mailBody = mailBody.Replace("{{ URL }}", url);

                            body = mailBody;
                        }
                    }
                    else
                    {
                        body += "Hello,<br /><br />Your Verification Code for Meeting : " + value;
                        var style = "style='display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;'";
                        body += "<br /> <br /> <br /><a href='" + url + "' target='_blank' " + style + ">Join Meeting</a>";
                        body += "<br /><br />Thank You,";
                    }

                    if (ConfigurationManager.AppSettings["MailWorking"].Equals("Yes"))
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { item.Email }).ToList(), null, null, subject, body);
                    }
                }
                #endregion
            }

            return Json(_meetingAddendence, "MeetingsDetails", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePauseMeeting(startstopeDate _objstartstopdate)
        {
            MeetingAttendance_VM _meetingAddendence = new MeetingAttendance_VM();
            if (_objstartstopdate.MeetingId > 0)
            {
                _meetingAddendence.startstopeDateVM = obj.PauseMeeting(_objstartstopdate);
            }
            return Json(_meetingAddendence, "MeetingsDetails", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveResumeMeeting(startstopeDate _objstartstopdate)
        {
            MeetingAttendance_VM _meetingAddendence = new MeetingAttendance_VM();
            if (_objstartstopdate.MeetingId > 0)
            {
                _meetingAddendence.startstopeDateVM = obj.ResumeMeeting(_objstartstopdate);
            }

            return Json(_meetingAddendence, "MeetingsDetails", JsonRequestBehavior.AllowGet);
        }

        public ActionResult OpenChairmanElection(long MeetingId, long ParticipantId)
        {
            ChairpersonElection objchairperson = new ChairpersonElection();
            objchairperson.MeetingId = MeetingId;
            objchairperson.MeetingParticipantId = ParticipantId;
            return PartialView("_ChairPersionEllection", objchairperson);
        }

        public ActionResult CheckforChairmanMeeting(long MeetingId)
        {

            string checkIschareManIsPresentorAbsent = obj.GetChareManForMeeting(MeetingId);
            return Json(checkIschareManIsPresentorAbsent, JsonRequestBehavior.AllowGet);
        }

        #endregion Start Stop Meeting

        #region End Meeting
        public ActionResult OpenAgendaSummary(long MeetingId)
        {
            List<MeetingAgendaSummary> objmeetingagendasummary = new List<MeetingAgendaSummary>();
            objmeetingagendasummary = obj.GetAgendaMeetingwise(MeetingId, userId);
            ViewBag.Meetingid = MeetingId;
            ViewBag.CheckagendaForMeeting = obj.checkNoofAgendadisscussed(MeetingId);
            return PartialView("_AgendaSummary", objmeetingagendasummary);
        }

        [HttpPost]
        public ActionResult ConcludeMeeting(startstopeDate objstartstopedate)
        {
            MeetingAttendance_VM _meetingAddendence = new MeetingAttendance_VM();
            if (objstartstopedate.MeetingId > 0)
            {
                int userId = AuthenticationHelper.UserID;
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                _meetingAddendence.startstopeDateVM = obj.ConcludeMeeting(objstartstopedate, customerId, userId);
            }
            return Json(_meetingAddendence, "MeetingsDetails", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Adjourned Meeting
        public ActionResult GetAdjournedDetails(long meetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.Adjourned(meetingId, customerId);
            return PartialView("_Adjourn", model);
        }
        [HttpPost]
        public ActionResult Adjourned(Meeting_AdjournVM model)
        {
            if (model.MeetingAdjourn_ID > 0 && model.MeetingDate_Adjourn != null)
            {
                int userId = AuthenticationHelper.UserID;
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var result = objIMeeting_Service.Adjourned(model.MeetingAdjourn_ID, model.AdjournedTime, (DateTime)model.MeetingDate_Adjourn, model.MeetingTime_Adjourn, userId, customerId);
                return Json(result, "MeetingsDetails", JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.Error = true;
                model.Message = "Please enter Adjourned to date";
            }
            return Json(model, "MeetingsDetails", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Quarom Message
        public ActionResult GetQuarumMessage(MeetingAttendance_VM _meetingattendence)
        {

            return PartialView("_MeetingQuarumMessage", _meetingattendence);
        }
        #endregion

        #region CTC
        public ActionResult OpenCTC(long MeetingId, long AgendaId)
        {
            MeetingCTC objmeetingctc = new MeetingCTC();
            objmeetingctc = obj.getCTCdetails(MeetingId, AgendaId);
            return PartialView("_OpenCTCTemplet", objmeetingctc);
        }


        public ActionResult OpenApproveAgendaList(long MeetingId)
        {
            List<AgendaListforCTC> objagendalist = new List<AgendaListforCTC>();
            objagendalist = obj.getListofApprovedAgenda(MeetingId);
            return PartialView("_ListofApprovedAgenda", objagendalist);
        }

        #region Download in word formate
        public ActionResult DownloadCTC(long MeetingId, long AgendaId)
        {
            MeetingCTC objmeetingctc = new MeetingCTC();
            string generatectc = GenerateCTCDocument(MeetingId, AgendaId);
            return File(generatectc, "application/force-download", Path.GetFileName(generatectc));
        }
        public string GenerateCTCDocument(long MeetingId, long AgendaId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                //MeetingCTC objmeetingctc = new MeetingCTC();                
                //objmeetingctc = obj.getCTCdetails(MeetingId, AgendaId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + MeetingId + "/CTC/" + AgendaId;
                //string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + objmeetingctc.EntityId + "/CTC/" + MeetingId + "/" + AgendaId;

                string str = "CTC-" + DateTime.Now.ToString("ddMMyyyy") + ".docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    DocxFormatProvider provider = new DocxFormatProvider();
                    RadFlowDocument document = CreateCTCDocument(MeetingId, AgendaId, customerId);

                    provider.Export(document, output);

                    //PdfFormatProvider provider = new PdfFormatProvider();
                    // RadFlowDocument document = CreateCTCDocument(MeetingId, AgendaId, customerId);

                    // provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public RadFlowDocument CreateCTCDocument(long meetingId, long AgendaId, int customerId)
        {

            RadFlowDocument htmlProvider = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(htmlProvider);
            try
            {
                MeetingCTC objmeetingctc = new MeetingCTC();
                objmeetingctc = obj.getCTCdetails(meetingId, AgendaId);

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                htmlProvider.Theme = theme;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                editor.MoveToParagraphStart(paragraph);



                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var run1 = editor.InsertLine("On the letterhead of Company");
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontStyle = System.Windows.FontStyles.Italic;
                run1.FontSize = 14;


                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;
                editor.InsertLine("");
                //var paragraph1 = section.Blocks.AddParagraph();
                //paragraph1.TextAlignment = Alignment.Justified;
                //editor.MoveToParagraphStart(paragraph1);
                //editor.ParagraphFormatting.SpacingAfter.LocalValue = 2;

                string strMeetingSrNo;
                if (objmeetingctc != null)
                {
                    if (objmeetingctc.MeetingNo > 0)
                    {
                        if (objmeetingctc.MeetingNo == 1)
                        {
                            strMeetingSrNo = "1st";
                        }
                        else if (objmeetingctc.MeetingNo == 2)
                        {
                            strMeetingSrNo = "2nd";
                        }
                        else if (objmeetingctc.MeetingNo == 3)
                        {
                            strMeetingSrNo = "3rd";
                        }
                        else
                        {
                            strMeetingSrNo = objmeetingctc.MeetingNo + "th";
                        }

                        string strMeetingPlace = string.Empty;

                        if (objmeetingctc.MeetingPlace != null)
                            strMeetingPlace = objmeetingctc.MeetingPlace.ToUpper();

                        var r = editor.InsertLine("CERTIFIED TRUE COPY OF THE RESOLUTION PASSED AT THE" + " " + strMeetingSrNo + " " + "MEETING OF THE BOARD OF DIRECTORS OF" + " " + objmeetingctc.EntityName.ToUpper() + " " + "HELD  ON" + " " + objmeetingctc.MeetingDay.ToUpper() + "," + " " + (objmeetingctc.MeetingDate).ToString("dd-MMM-yyyy").ToUpper() + "," + " " + "AT" + " " + strMeetingPlace + ".");
                        r.FontWeight = System.Windows.FontWeights.Bold;
                        r.FontSize = 14;

                        HtmlFormatProvider htmlProvider1 = new HtmlFormatProvider();
                        var insertOptions = new InsertDocumentOptions
                        {
                            ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                        };

                        string AgendaItem = "<h2>" + objmeetingctc.MeetingMinutesHeadingFormate + "</h2>";
                        paragraph = section.Blocks.AddParagraph();
                        editor.MoveToParagraphStart(paragraph);
                        RadFlowDocument htmlDocument = htmlProvider1.Import(AgendaItem);
                        editor.InsertDocument(htmlDocument, insertOptions);

                        if (objmeetingctc.MeetingResolutionFormate != null)
                        {
                            paragraph = section.Blocks.AddParagraph();
                            paragraph.TextAlignment = Alignment.Justified;

                            htmlDocument = htmlProvider1.Import(objmeetingctc.MeetingResolutionFormate);
                            editor.InsertDocument(htmlDocument, insertOptions);
                        }
                    }
                }
                #endregion

                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Right;
                editor.InsertLine("");

                var r5 = editor.InsertLine("Certified True Copy");
                r5.FontStyle = System.Windows.FontStyles.Italic;
                r5.FontSize = 14;

                if (objmeetingctc.EntityName != null)
                {
                    r5 = editor.InsertLine("FOR" + " " + objmeetingctc.EntityName.ToUpper());
                }
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontSize = 14;

                if (objmeetingctc.DirectorName != null)
                {
                    r5 = editor.InsertLine(objmeetingctc.DirectorName);
                    r5.FontWeight = System.Windows.FontWeights.Bold;
                    r5.FontStyle = System.Windows.FontStyles.Italic;
                    r5.FontSize = 14;
                }
                #region Table

                var table = editor.InsertTable(2, 2);

                table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                table.Alignment = Alignment.Right;


                table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 70);

                table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);
                Border tableBorderAll = new Border(0.5, BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                table.Borders = new TableBorders(tableBorderAll);

                var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("PLACE:");
                cellRun.FontWeight = System.Windows.FontWeights.Normal;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Right;
                if (objmeetingctc.DesignationofDirector != null)
                {
                    cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("(" + objmeetingctc.DesignationofDirector + ")");
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;
                    cellRun.FontStyle = System.Windows.FontStyles.Italic;
                }

                //Day
                cellRun = table.Rows[1].Cells[0].Blocks.AddParagraph().Inlines.AddRun("DATE");
                cellRun.FontWeight = System.Windows.FontWeights.Normal;
                if (objmeetingctc.DinNumberofDirector != null)
                {
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Right;
                    cellRun = table.Rows[1].Cells[1].Blocks.AddParagraph().Inlines.AddRun("DIN : " + objmeetingctc.DinNumberofDirector);
                    cellRun.FontWeight = System.Windows.FontWeights.Bold;
                    cellRun.FontStyle = System.Windows.FontStyles.Italic;

                }
                #endregion 

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return htmlProvider;
        }
        #endregion

        #region Download in PDF Formate
        public ActionResult DownloadCTCpdf(long MeetingId, long AgendaId)
        {
            MeetingCTC objmeetingctc = new MeetingCTC();
            string generatectc = GenerateCTCDocumentpdf(MeetingId, AgendaId);
            return File(generatectc, "application/force-download", Path.GetFileName(generatectc));
            //return PartialView("");
        }
        public string GenerateCTCDocumentpdf(long MeetingId, long AgendaId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                //MeetingCTC objmeetingctc = new MeetingCTC();                
                //objmeetingctc = obj.getCTCdetails(MeetingId, AgendaId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + MeetingId + "/CTC/" + AgendaId;
                //string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + objmeetingctc.EntityId + "/CTC/" + MeetingId + "/" + AgendaId;

                string str = "CTC-" + DateTime.Now.ToString("ddMMyyyy") + ".pdf";
                string fileName = Path.Combine(Server.MapPath(path), str);

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    //DocxFormatProvider provider = new DocxFormatProvider();
                    //RadFlowDocument document = CreateCTCDocument(MeetingId, AgendaId, customerId);

                    //provider.Export(document, output);

                    PdfFormatProvider provider = new PdfFormatProvider();
                    // RadFlowDocument document = CreateCTCDocumentpdf(MeetingId, AgendaId, customerId);
                    //RadFlowDocument document = getCTCdetails(MeetingId, AgendaId);

                    RadFlowDocument document = CreateCTCDocument(MeetingId, AgendaId, customerId);
                    provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public RadFlowDocument CreateCTCDocumentpdf(long meetingId, long AgendaId, int customerId)
        {

            RadFlowDocument htmlProvider = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(htmlProvider);
            try
            {
                MeetingCTC objmeetingctc = new MeetingCTC();

                objmeetingctc = obj.getCTCdetails(meetingId, AgendaId);

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                htmlProvider.Theme = theme;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                editor.MoveToParagraphStart(paragraph);



                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var run1 = editor.InsertLine("On the letterhead of Company");
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontStyle = System.Windows.FontStyles.Italic;
                run1.FontSize = 14;


                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;
                editor.InsertLine("");
                //var paragraph1 = section.Blocks.AddParagraph();
                //paragraph1.TextAlignment = Alignment.Justified;
                //editor.MoveToParagraphStart(paragraph1);
                //editor.ParagraphFormatting.SpacingAfter.LocalValue = 2;

                string strMeetingSrNo;
                if (objmeetingctc != null)
                {
                    if (objmeetingctc.MeetingNo > 0)
                    {
                        if (objmeetingctc.MeetingNo == 1)
                        {
                            strMeetingSrNo = "1st";
                        }
                        else if (objmeetingctc.MeetingNo == 2)
                        {
                            strMeetingSrNo = "2nd";
                        }
                        else if (objmeetingctc.MeetingNo == 3)
                        {
                            strMeetingSrNo = "3rd";
                        }
                        else
                        {
                            strMeetingSrNo = objmeetingctc.MeetingNo + "th";
                        }
                        var r = editor.InsertLine("CERTIFIED TRUE COPY OF THE RESOLUTION PASSED AT THE" + " " + strMeetingSrNo + " " + "MEETING OF THE BOARD OF DIRECTORS OF" + " " + objmeetingctc.EntityName.ToUpper() + " " + "HEALD  ON" + " " + objmeetingctc.MeetingDay + "," + " " + (objmeetingctc.MeetingDate).ToString("dd-MMM-yyyy") + "," + " " + "AT" + " " + objmeetingctc.MeetingPlace + ".");
                        r.FontWeight = System.Windows.FontWeights.Bold;
                        r.FontSize = 14;

                        editor.InsertLine("");
                        paragraph = section.Blocks.AddParagraph();
                        paragraph.TextAlignment = Alignment.Justified;
                        var r1 = editor.InsertLine(objmeetingctc.MeetingMinutesHeadingFormate);
                        r1.FontWeight = System.Windows.FontWeights.Bold;
                        r1.FontSize = 14;

                        paragraph = section.Blocks.AddParagraph();
                        paragraph.TextAlignment = Alignment.Justified;
                        //var r2 = editor.InsertLine(objmeetingctc.MeetingResolutionFormate);
                        r1.FontWeight = System.Windows.FontWeights.Bold;
                        r1.FontSize = 14;
                        //var
                        HtmlFormatProvider htmlProvider1 = new HtmlFormatProvider();
                        var insertOptions = new InsertDocumentOptions
                        {
                            ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                        };
                        if (objmeetingctc.MeetingResolutionFormate != null)
                        {
                            RadFlowDocument htmlDocument = htmlProvider1.Import(objmeetingctc.MeetingResolutionFormate);

                            editor.InsertDocument(htmlDocument, insertOptions);
                        }
                    }
                }

                #endregion

                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Right;
                editor.InsertLine("");
                //paragraph = section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Right;
                //editor.MoveToParagraphStart(paragraph);
                var r5 = editor.InsertLine("Certified True Copy");
                r5.FontStyle = System.Windows.FontStyles.Italic;
                r5.FontSize = 14;

                if (objmeetingctc.EntityName != null)
                {
                    r5 = editor.InsertLine("FOR" + " " + objmeetingctc.EntityName.ToUpper());
                }
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontSize = 14;


                r5 = editor.InsertLine(objmeetingctc.DirectorName);
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontStyle = System.Windows.FontStyles.Italic;
                r5.FontSize = 14;


                editor.InsertLine("");
                editor.InsertLine("");
                r5 = editor.InsertLine(objmeetingctc.DesignationofDirector);
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontStyle = System.Windows.FontStyles.Italic;
                r5.FontSize = 14;

                r5 = editor.InsertLine(objmeetingctc.DinNumberofDirector);
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontStyle = System.Windows.FontStyles.Italic;
                r5.FontSize = 14;

                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;
                editor.InsertLine("");
                r5 = editor.InsertLine("PLACE:");
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontSize = 14;

                r5 = editor.InsertLine("Date:");
                r5.FontWeight = System.Windows.FontWeights.Bold;
                r5.FontSize = 14;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return htmlProvider;
        }
        #endregion



        public ActionResult OpenCTCFormate(long MeetingId, long AgendaId)
        {
            MeetingCTC objmeetingctc = new MeetingCTC();
            objmeetingctc.MeetingId = MeetingId;
            objmeetingctc.AgendaId = AgendaId;
            return PartialView("CTCFormate", objmeetingctc);
        }
        #endregion

        #region Attendence Preview and word Document added by Ruchi on 26th December
        public ActionResult OpenAttendencePriview(long MeetingId)
        {
            MeetingAttendance_VM _meetingattendence = new MeetingAttendance_VM();
            _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(MeetingId,CustomerId);
            _meetingattendence.AttendenceDetails = obj.getAttendenceParticipantDetails(MeetingId);
            _meetingattendence.MeetingId = MeetingId;

            return PartialView("_AttendencePriview", _meetingattendence);
        }

        #region Download Attendence Document in word Formate
        public ActionResult DownloadAttendence(long MeetingId)
        {
            MeetingCTC objmeetingctc = new MeetingCTC();
            string generatectc = GenerateAttendenceDocument(MeetingId);
            return File(generatectc, "application/force-download", Path.GetFileName(generatectc));
            //return PartialView("");
        }

        public string GenerateAttendenceDocument(long MeetingId)
        {
            try
            {
                MeetingAttendance_VM _meetingattendence = new MeetingAttendance_VM();
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(MeetingId,CustomerId);
                _meetingattendence.AttendenceDetails = obj.getAttendenceParticipantDetails(MeetingId);
                string path = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + MeetingId;
                string str = "AttendanceRegister.docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    DocxFormatProvider provider = new DocxFormatProvider();
                    RadFlowDocument document = CreateAttendenceDocument(MeetingId, customerId);

                    provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public RadFlowDocument CreateAttendenceDocument(long meetingId, int customerId)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {

                MeetingAttendance_VM _meetingattendence = new MeetingAttendance_VM();

                _meetingattendence.startstopeDateVM = obj.getMeetingstartstopDate(meetingId,CustomerId);
                _meetingattendence.AttendenceDetails = obj.getAttendenceParticipantDetails(meetingId);

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                if (_meetingattendence.startstopeDateVM.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && _meetingattendence.startstopeDateVM.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Document header

                    var section = editor.InsertSection();
                    var paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Center;
                    editor.MoveToParagraphStart(paragraph);

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;

                    var run1 = editor.InsertLine("ATTENDANCE REGISTER");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontStyle = System.Windows.FontStyles.Italic;
                    run1.FontSize = 14;

                    if (!string.IsNullOrEmpty(_meetingattendence.startstopeDateVM.EntityName))
                    {
                        var R2 = editor.InsertLine(_meetingattendence.startstopeDateVM.EntityName);
                        R2.FontWeight = System.Windows.FontWeights.Bold;
                        R2.FontStyle = System.Windows.FontStyles.Italic;
                        R2.FontSize = 14;
                        R2.FontWeight = System.Windows.FontWeights.Bold;
                        R2.FontSize = 14;

                        editor.InsertLine("");
                        paragraph = section.Blocks.AddParagraph();
                        paragraph.TextAlignment = Alignment.Justified;
                    }

                    string strMeetingSrNo;
                    if (_meetingattendence.startstopeDateVM != null)
                    {
                        if (_meetingattendence.startstopeDateVM.MeetingSrNo > 0)
                        {
                            if (_meetingattendence.startstopeDateVM.MeetingSrNo == 1)
                            {
                                strMeetingSrNo = "1st";
                            }
                            else if (_meetingattendence.startstopeDateVM.MeetingSrNo == 2)
                            {
                                strMeetingSrNo = "2nd";
                            }
                            else if (_meetingattendence.startstopeDateVM.MeetingSrNo == 3)
                            {
                                strMeetingSrNo = "3rd";
                            }
                            else
                            {
                                strMeetingSrNo = _meetingattendence.startstopeDateVM.MeetingSrNo + "th";
                            }

                            var r = editor.InsertLine(_meetingattendence.startstopeDateVM.Meetingno + "/" + _meetingattendence.startstopeDateVM.FYText + "-" + _meetingattendence.startstopeDateVM.MeetingType);
                            //var r = editor.InsertLine(_meetingattendence.startstopeDateVM.MeetingType + " No.-" + strMeetingSrNo);
                            
                            r.FontWeight = System.Windows.FontWeights.Bold;
                            r.FontSize = 14;

                            editor.InsertLine("");
                            paragraph = section.Blocks.AddParagraph();
                            paragraph.TextAlignment = Alignment.Justified;
                            var r1 = editor.InsertLine("HELD AT " + _meetingattendence.startstopeDateVM.Venu + " ON " + String.Format("{0:MMM dd,yyyy}", _meetingattendence.startstopeDateVM.StartDate) + " " + "AT" + " " + _meetingattendence.startstopeDateVM.StartTime);
                            r1.FontWeight = System.Windows.FontWeights.Bold;
                            r1.FontSize = 14;

                        }
                    }

                    #endregion

                    #region Table
                    editor.InsertLine("");
                    editor.InsertLine("");
                    if (_meetingattendence.AttendenceDetails.Count > 0)
                    {
                        var table = editor.InsertTable((_meetingattendence.AttendenceDetails.Count) + 1, 3);

                        table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                        table.Alignment = Alignment.Center;


                        table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 20);

                        table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 40);

                        table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 40);
                        Border tableBorderAll = new Border(0.5, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                        table.Borders = new TableBorders(tableBorderAll);
                        var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("Sr.No");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("NAME OF DIRECTOR");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun("PRESENT /LEAVE OF ABSENCE");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        int i = 1;
                        foreach (var item in _meetingattendence.AttendenceDetails)
                        {

                            cellRun = table.Rows[i].Cells[0].Blocks.AddParagraph().Inlines.AddRun("" + i + "");
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            //Day
                            cellRun = table.Rows[i].Cells[1].Blocks.AddParagraph().Inlines.AddRun(item.Participant);
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            cellRun = table.Rows[i].Cells[2].Blocks.AddParagraph().Inlines.AddRun(item.Attendence);
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;
                            i++;
                        }
                    }
                    #endregion

                    #region Footer
                    //var section1 = editor.InsertSection();
                    //var paragraph1 = section1.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Justified;
                    editor.MoveToParagraphStart(paragraph);
                
                    var r2 = editor.InsertLine("Authenticated By" + " " + _meetingattendence.startstopeDateVM.ChairmanoftheMeeting + " " + ", (Chairman of the Meeting) on " + String.Format("{0:MMM dd,yyyy}", _meetingattendence.startstopeDateVM.StartDate));
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;
                    editor.InsertLine("");

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    r2 = editor.InsertLine("NAME:");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;

                    r2 = editor.InsertLine("SIGNATURE:");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    r2 = editor.InsertLine("PLACE:");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;

                    r2 = editor.InsertLine("DATE:");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;
                    #endregion
                }
                else
                {
                    #region Document header
                    HtmlFormatProvider htmlProvider = new HtmlFormatProvider();
                    var insertOptions = new InsertDocumentOptions
                    {
                        ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                    };

                    var section = editor.InsertSection();
                    var paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Center;
                    editor.MoveToParagraphStart(paragraph);

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                    var run1 = editor.InsertLine("ATTENDANCE SHEET");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 16;

                    string strMeetingSrNo = "";
                    var meetingTypeName = "";
                    if (_meetingattendence.startstopeDateVM != null)
                    {
                        if (_meetingattendence.startstopeDateVM.MeetingSrNo > 0)
                        {
                            if (_meetingattendence.startstopeDateVM.MeetingSrNo == 1)
                            {
                                strMeetingSrNo = "1st";
                            }
                            else if (_meetingattendence.startstopeDateVM.MeetingSrNo == 2)
                            {
                                strMeetingSrNo = "2nd";
                            }
                            else if (_meetingattendence.startstopeDateVM.MeetingSrNo == 3)
                            {
                                strMeetingSrNo = "3rd";
                            }
                            else
                            {
                                strMeetingSrNo = _meetingattendence.startstopeDateVM.MeetingSrNo + "th";
                            }
                        }

                        if (_meetingattendence.startstopeDateVM.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                        {
                            meetingTypeName = "Annual General Meeting";
                        }
                        else if (_meetingattendence.startstopeDateVM.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                        {
                            meetingTypeName = "Extra Ordinary General Meeting";
                        }

                    }
                    #region Date
                    string mDate = string.Empty;
                    string mDay = string.Empty;
                    string mTime = string.Empty;
                    string mVenue = string.Empty;

                    if (_meetingattendence.startstopeDateVM.MeetingStartDate != null)
                    {
                        var date = Convert.ToDateTime(_meetingattendence.startstopeDateVM.MeetingStartDate);
                        mDate = date.ToString("MMMM dd, yyyy");
                        mDay = date.ToString("dddd");
                    }
                    if (!string.IsNullOrEmpty(_meetingattendence.startstopeDateVM.StartTime))
                    {
                        mTime = _meetingattendence.startstopeDateVM.StartTime;
                    }

                    if (!string.IsNullOrEmpty(_meetingattendence.startstopeDateVM.Venu))
                    {
                        mVenue = _meetingattendence.startstopeDateVM.Venu;
                    }

                    #endregion

                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Left;
                    editor.MoveToParagraphStart(paragraph);

                    var meetingDetails = "<p>ATTENDANCE REGISTER OF THE " + strMeetingSrNo + " " + meetingTypeName.ToUpper() + " OF THE MEMBERS OF " + _meetingattendence.startstopeDateVM.EntityName.ToUpper() + " HELD ON " + mDate.ToUpper() + " AT " + mTime.ToUpper() + " AT " + mVenue.ToUpper() + ".</p>";
                    var htmlDocument = htmlProvider.Import(meetingDetails);
                    editor.InsertDocument(htmlDocument, insertOptions);

                    #endregion

                    #region Director

                    var lstMeetingAttendance = obj.GetPerticipenforAttendenceCS(meetingId, customerId, null);
                    var lstOtherParticipantAttendance = obj.GetOtherParticipentAttendence(meetingId);

                    editor.InsertLine("");
                    if (lstMeetingAttendance != null)
                    {
                        var run = editor.InsertLine("PRESENT");
                        run.FontWeight = System.Windows.FontWeights.Bold;
                        run.FontSize = 14;

                        paragraph = section.Blocks.AddParagraph();
                        paragraph.TextAlignment = Alignment.Left;
                        editor.MoveToParagraphStart(paragraph);

                        run = editor.InsertLine("DIRECTORS");
                        run.FontWeight = System.Windows.FontWeights.Bold;
                        run.FontSize = 14;

                        var table = editor.InsertTable((lstMeetingAttendance.Where(k => k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).Count()) + 1, 4);

                        table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                        table.Alignment = Alignment.Center;

                        table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 10);
                        table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);
                        table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);
                        table.Rows[0].Cells[3].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);

                        Border tableBorderAll = new Border(0.5, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                        table.Borders = new TableBorders(tableBorderAll);
                        var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("Sr.No");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("NAME OF DIRECTOR");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun("DESIGNATION");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[3].Blocks.AddParagraph().Inlines.AddRun("PRESENT/LEAVE");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        int i = 1;
                        foreach (var item in lstMeetingAttendance.Where(k => k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR))
                        {
                            table.Rows[i].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 10);
                            table.Rows[i].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);
                            table.Rows[i].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);
                            table.Rows[i].Cells[3].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 30);

                            cellRun = table.Rows[i].Cells[0].Blocks.AddParagraph().Inlines.AddRun("" + i + "");
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            cellRun = table.Rows[i].Cells[1].Blocks.AddParagraph().Inlines.AddRun(item.MeetingParticipant_Name);
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            cellRun = table.Rows[i].Cells[2].Blocks.AddParagraph().Inlines.AddRun(item.Designation);
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            cellRun = table.Rows[i].Cells[3].Blocks.AddParagraph().Inlines.AddRun((item.Attendance == "P" ? "PRESENT" : "LEAVE"));
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;
                            i++;
                        }
                    }
                    #endregion

                    #region Special Invitee
                    if (lstOtherParticipantAttendance != null)
                    {
                        paragraph = section.Blocks.AddParagraph();
                        paragraph.TextAlignment = Alignment.Left;
                        editor.MoveToParagraphStart(paragraph);

                        var run = editor.InsertLine("SPECIAL INVITEES");
                        run.FontWeight = System.Windows.FontWeights.Bold;
                        run.FontSize = 14;

                        var table = editor.InsertTable((lstOtherParticipantAttendance.Where(k => k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.AUDITOR).Count()) + 1, 3);

                        table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                        table.Alignment = Alignment.Center;


                        table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 10);
                        table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 40);
                        table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 50);

                        Border tableBorderAll = new Border(0.5, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                        table.Borders = new TableBorders(tableBorderAll);
                        var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("Sr.No");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("NAME OF INVITEES");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun("DESIGNATION");
                        cellRun.FontWeight = System.Windows.FontWeights.Bold;
                        int i = 1;
                        foreach (var item in lstOtherParticipantAttendance.Where(k => k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.AUDITOR))
                        {
                            table.Rows[i].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 10);
                            table.Rows[i].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 40);
                            table.Rows[i].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 50);

                            cellRun = table.Rows[i].Cells[0].Blocks.AddParagraph().Inlines.AddRun("" + i + "");
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            cellRun = table.Rows[i].Cells[1].Blocks.AddParagraph().Inlines.AddRun(item.MeetingParticipant_Name);
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;

                            cellRun = table.Rows[i].Cells[2].Blocks.AddParagraph().Inlines.AddRun(item.Designation);
                            cellRun.FontWeight = System.Windows.FontWeights.Normal;
                            i++;
                        }
                    }

                    #endregion

                    #region KMP
                    if (lstMeetingAttendance != null)
                    {
                        if (lstMeetingAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP).Count() > 0)
                        {
                            paragraph = section.Blocks.AddParagraph();
                            paragraph.TextAlignment = Alignment.Left;
                            editor.MoveToParagraphStart(paragraph);

                            var run = editor.InsertLine("IN ATTENDANCE");
                            run.FontWeight = System.Windows.FontWeights.Bold;
                            run.FontSize = 14;

                            var table = editor.InsertTable((lstMeetingAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP).Count()) + 1, 3);

                            table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                            table.Alignment = Alignment.Center;


                            table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 10);
                            table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 40);
                            table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 50);

                            Border tableBorderAll = new Border(0.5, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                            table.Borders = new TableBorders(tableBorderAll);
                            var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("Sr.No");
                            cellRun.FontWeight = System.Windows.FontWeights.Bold;
                            cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun("NAME");
                            cellRun.FontWeight = System.Windows.FontWeights.Bold;
                            cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun("DESIGNATION");
                            cellRun.FontWeight = System.Windows.FontWeights.Bold;
                            int i = 1;
                            foreach (var item in lstMeetingAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP))
                            {
                                table.Rows[i].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 10);
                                table.Rows[i].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 40);
                                table.Rows[i].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 50);

                                cellRun = table.Rows[i].Cells[0].Blocks.AddParagraph().Inlines.AddRun("" + i + "");
                                cellRun.FontWeight = System.Windows.FontWeights.Normal;

                                cellRun = table.Rows[i].Cells[1].Blocks.AddParagraph().Inlines.AddRun(item.MeetingParticipant_Name);
                                cellRun.FontWeight = System.Windows.FontWeights.Normal;

                                cellRun = table.Rows[i].Cells[2].Blocks.AddParagraph().Inlines.AddRun(item.Designation);
                                cellRun.FontWeight = System.Windows.FontWeights.Normal;
                                i++;
                            }
                        }
                    }
                    #endregion

                    #region Member counts
                    editor.InsertLine("");
                    if (_meetingattendence.startstopeDateVM.TotalMemberForAgm != null)
                    {
                        run1 = editor.InsertLine("TOTAL MEMBER : " + Convert.ToString(_meetingattendence.startstopeDateVM.TotalMemberForAgm));
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 14;
                    }
                    else
                    {
                        run1 = editor.InsertLine("TOTAL MEMBER");
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 14;
                    }

                    if (_meetingattendence.startstopeDateVM.TotalMemberPresentForAgm != null)
                    {
                        run1 = editor.InsertLine("TOTAL MEMBER PRESENT : " + Convert.ToString(_meetingattendence.startstopeDateVM.TotalMemberPresentForAgm));
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 14;
                    }
                    else
                    {
                        run1 = editor.InsertLine("TOTAL MEMBER PRESENT");
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 14;
                    }

                    if (_meetingattendence.startstopeDateVM.TotalProxyPresentforAgm != null)
                    {
                        run1 = editor.InsertLine("TOTAL PROXY PESENT : " + Convert.ToString(_meetingattendence.startstopeDateVM.TotalProxyPresentforAgm));
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 14;
                    }
                    else
                    {
                        run1 = editor.InsertLine("TOTAL PROXY PESENT");
                        run1.FontWeight = System.Windows.FontWeights.Bold;
                        run1.FontSize = 14;
                    }
                    #endregion

                    #region Footer
                    editor.InsertLine("");
                    paragraph = section.Blocks.AddParagraph();
                    paragraph.TextAlignment = Alignment.Justified;
                    //var r2 = editor.InsertLine("Authenticated By" + " " + _meetingattendence.startstopeDateVM.ChairmanoftheMeeting + " " + ", (Chairman of the Meeting) on" + String.Format("{0:MMM dd,yyyy}", _meetingattendence.startstopeDateVM.StartDate));
                    var r2 = editor.InsertLine("Authenticated By");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;
                    editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;
                    editor.InsertLine("");

                    editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                    r2 = editor.InsertLine("PLACE:");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;

                    if (!string.IsNullOrEmpty(_meetingattendence.startstopeDateVM.ChairmanoftheMeeting))
                    {
                        r2 = editor.InsertLine(_meetingattendence.startstopeDateVM.ChairmanoftheMeeting);
                        r2.FontWeight = System.Windows.FontWeights.Bold;
                        r2.FontSize = 14;
                    }

                    r2 = editor.InsertLine("DATE:");
                    r2.FontWeight = System.Windows.FontWeights.Bold;
                    r2.FontSize = 14;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }
        #endregion
        #region Download in PDF Formate
        //public ActionResult DownloadCTCpdf(long MeetingId)
        //{
        //    MeetingCTC objmeetingctc = new MeetingCTC();
        //    string generatectc = GenerateCTCDocumentpdf(MeetingId);
        //    return File(generatectc, "application/force-download", Path.GetFileName(generatectc));
        //    //return PartialView("");
        //}
        //public string GenerateCTCDocumentpdf(long MeetingId)
        //{
        //    try
        //    {
        //        MeetingCTC objmeetingctc = new MeetingCTC();
        //        int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        objmeetingctc = obj.getCTCdetails(MeetingId);

        //        string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + objmeetingctc.EntityId + "/MeetingCTC/" + MeetingId;
        //        string str = "CTC.pdf";
        //        string fileName = Path.Combine(Server.MapPath(path), str);

        //        bool exists = System.IO.Directory.Exists(Server.MapPath(path));

        //        if (!exists)
        //            System.IO.Directory.CreateDirectory(Server.MapPath(path));

        //        using (Stream output = System.IO.File.OpenWrite(fileName))
        //        {
        //            //DocxFormatProvider provider = new DocxFormatProvider();
        //            //RadFlowDocument document = CreateCTCDocument(MeetingId, AgendaId, customerId);

        //            //provider.Export(document, output);

        //            PdfFormatProvider provider = new PdfFormatProvider();
        //            RadFlowDocument document = CreateCTCDocumentpdf(MeetingId, AgendaId, customerId);

        //            provider.Export(document, output);
        //        }

        //        return fileName;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }
        //}

        //public RadFlowDocument CreateCTCDocumentpdf(long meetingId,int customerId)
        //{

        //    RadFlowDocument htmlProvider = new RadFlowDocument();
        //    RadFlowDocumentEditor editor = new RadFlowDocumentEditor(htmlProvider);
        //    try
        //    {
        //        MeetingCTC objmeetingctc = new MeetingCTC();

        //        objmeetingctc = obj.getCTCdetails(meetingId);

        //        ThemeFontScheme fontScheme = new ThemeFontScheme(
        //       "Mine",
        //       "Bookman Old Style",   // Major 
        //       "Bookman Old Style");          // Minor 


        //        ThemeColorScheme colorScheme = new ThemeColorScheme(
        //        "Mine",
        //        Colors.Black,     // background 1 
        //        Colors.Blue,      // text 1 
        //        Colors.Brown,     // background 2 
        //        Colors.Cyan,      // text 2 
        //        Colors.Black,    //DarkGray,  // accent 1 
        //        Colors.Gray,      // accent 2 
        //        Colors.Green,     // accent 3 
        //        Colors.LightGray, // accent 4 
        //        Colors.Magenta,   // accent 5 
        //        Colors.Orange,    // accent 6 
        //        Colors.Purple,    // hyperlink 
        //        Colors.Red);      // followedHyperlink 

        //        DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
        //        htmlProvider.Theme = theme;

        //        #region Document header

        //        var section = editor.InsertSection();
        //        var paragraph = section.Blocks.AddParagraph();
        //        paragraph.TextAlignment = Alignment.Center;
        //        editor.MoveToParagraphStart(paragraph);



        //        editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
        //        editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
        //        var run1 = editor.InsertLine("On the letterhead of Company");
        //        run1.FontWeight = System.Windows.FontWeights.Bold;
        //        run1.FontStyle = System.Windows.FontStyles.Italic;
        //        run1.FontSize = 14;


        //        editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;
        //        editor.InsertLine("");
        //        //var paragraph1 = section.Blocks.AddParagraph();
        //        //paragraph1.TextAlignment = Alignment.Justified;
        //        //editor.MoveToParagraphStart(paragraph1);
        //        //editor.ParagraphFormatting.SpacingAfter.LocalValue = 2;

        //        string strMeetingSrNo;
        //        if (objmeetingctc != null)
        //        {
        //            if (objmeetingctc.MeetingNo > 0)
        //            {
        //                if (objmeetingctc.MeetingNo == 1)
        //                {
        //                    strMeetingSrNo = "1st";
        //                }
        //                else if (objmeetingctc.MeetingNo == 2)
        //                {
        //                    strMeetingSrNo = "2nd";
        //                }
        //                else if (objmeetingctc.MeetingNo == 3)
        //                {
        //                    strMeetingSrNo = "3rd";
        //                }
        //                else
        //                {
        //                    strMeetingSrNo = objmeetingctc.MeetingNo + "th";
        //                }
        //                var r = editor.InsertLine("CERTIFIED TRUE COPY OF THE RESOLUTION PASSED AT THE" + " " + strMeetingSrNo + " " + "MEETING OF THE BOARD OF DIRECTORS OF" + " " + objmeetingctc.EntityName.ToUpper() + " " + "HEALD  ON" + " " + objmeetingctc.MeetingDay + "," + " " + (objmeetingctc.MeetingDate).ToString("dd-MMM-yyyy") + "," + " " + "AT" + " " + objmeetingctc.MeetingPlace + ".");
        //                r.FontWeight = System.Windows.FontWeights.Bold;
        //                r.FontSize = 14;

        //                editor.InsertLine("");
        //                paragraph = section.Blocks.AddParagraph();
        //                paragraph.TextAlignment = Alignment.Justified;
        //                var r1 = editor.InsertLine(objmeetingctc.MeetingMinutesHeadingFormate);
        //                r1.FontWeight = System.Windows.FontWeights.Bold;
        //                r1.FontSize = 14;

        //                paragraph = section.Blocks.AddParagraph();
        //                paragraph.TextAlignment = Alignment.Justified;
        //                //var r2 = editor.InsertLine(objmeetingctc.MeetingResolutionFormate);
        //                r1.FontWeight = System.Windows.FontWeights.Bold;
        //                r1.FontSize = 14;
        //                //var
        //                HtmlFormatProvider htmlProvider1 = new HtmlFormatProvider();
        //                var insertOptions = new InsertDocumentOptions
        //                {
        //                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
        //                };
        //                RadFlowDocument htmlDocument = htmlProvider1.Import(objmeetingctc.MeetingResolutionFormate);

        //                editor.InsertDocument(htmlDocument, insertOptions);
        //            }
        //        }

        //        #endregion

        //        editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Right;
        //        editor.InsertLine("");
        //        //paragraph = section.Blocks.AddParagraph();
        //        //paragraph.TextAlignment = Alignment.Right;
        //        //editor.MoveToParagraphStart(paragraph);
        //        var r5 = editor.InsertLine("Certified True Copy");
        //        r5.FontStyle = System.Windows.FontStyles.Italic;
        //        r5.FontSize = 14;

        //        if (objmeetingctc.EntityName != null)
        //        {
        //            r5 = editor.InsertLine("FOR" + " " + objmeetingctc.EntityName.ToUpper());
        //        }
        //        r5.FontWeight = System.Windows.FontWeights.Bold;
        //        r5.FontSize = 14;


        //        r5 = editor.InsertLine("{{Name of director authorised to sign CTC}}");
        //        r5.FontWeight = System.Windows.FontWeights.Bold;
        //        r5.FontStyle = System.Windows.FontStyles.Italic;
        //        r5.FontSize = 14;


        //        editor.InsertLine("");
        //        editor.InsertLine("");
        //        r5 = editor.InsertLine("{{Designation of signatory}}");
        //        r5.FontWeight = System.Windows.FontWeights.Bold;
        //        r5.FontStyle = System.Windows.FontStyles.Italic;
        //        r5.FontSize = 14;

        //        r5 = editor.InsertLine("DIN:{{DIN of Signatory}}");
        //        r5.FontWeight = System.Windows.FontWeights.Bold;
        //        r5.FontStyle = System.Windows.FontStyles.Italic;
        //        r5.FontSize = 14;

        //        editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;
        //        editor.InsertLine("");
        //        r5 = editor.InsertLine("PLACE:");
        //        r5.FontWeight = System.Windows.FontWeights.Bold;
        //        r5.FontSize = 14;

        //        r5 = editor.InsertLine("Date:");
        //        r5.FontWeight = System.Windows.FontWeights.Bold;
        //        r5.FontSize = 14;

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //    return htmlProvider;
        //}
        #endregion
        #endregion

        #region Attendence For Auditor
        [HttpGet]
        public ActionResult GetAuditorAttendance([DataSourceRequest] DataSourceRequest request, long MeetingID)
        {
            List<MeetingAttendance_VM> _meetingattendence = new List<MeetingAttendance_VM>();
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = AuthenticationHelper.UserID;
            string role = AuthenticationHelper.Role;
            _meetingattendence = obj.GetPerticipenforAuditorAttendenceCS(MeetingID, customerId, userId);

            return Json(_meetingattendence.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetInviteeAttendance([DataSourceRequest] DataSourceRequest request, long MeetingID)
        {
            List<MeetingAttendance_VM> _meetingattendence = new List<MeetingAttendance_VM>();
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = AuthenticationHelper.UserID;
            string role = AuthenticationHelper.Role;
            _meetingattendence = obj.GetPerticipenforInviteeAttendenceCS(MeetingID, customerId, userId);

            return Json(_meetingattendence.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Agenda with noting voting (added by Ruchi on 15th of May 2020)
        [HttpGet]
        public ActionResult AgendaDetailswithvotingnoting([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            List<MeetingAgendaSummary> _objagendadeals = new List<MeetingAgendaSummary>();

            _objagendadeals = obj.GetAgendaMeetingwise(MeetingId, userId);

            return Json(_objagendadeals.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion Agenda with noting voting (added by Ruchi on 15th of May 2020)

        #region MarkAttendence by Director added on 15th of May 2020 by Ruchi
        public ActionResult MarkAttendence(long MeetingId, long ParticipantId)
        {
            string result = "";
            if (MeetingId > 0 && ParticipantId > 0)
            {
                result = obj.MarkAttendenceofDirector(MeetingId, ParticipantId, userId);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VotingNotingResponseusingDir([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<MeetingAgendaSummary> _objNotingvoting)
        {
            var results = new List<MeetingAgendaSummary>();

            if (_objNotingvoting != null && ModelState.IsValid)
            {
                foreach (var items in _objNotingvoting)
                {
                    obj.saveNotingVotingbyDir(items);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }
       [HttpPost]
        public ActionResult GetAgendaNotingResponse([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            List<MainMeeting_VM> _meetingresponse = new List<MainMeeting_VM>();
            _meetingresponse = obj.GetMeetingAgenda(MeetingId);
            return Json(_meetingresponse.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}