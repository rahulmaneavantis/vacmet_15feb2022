﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class WebViewController : Controller
    {
        // GET: BM_Management/WebView
        public ActionResult Index(long MeetingId, long ParticipantId, string Email)
        {
            string EmailId = Email;
            PreviewAgendaVM model = new PreviewAgendaVM();
            if (!string.IsNullOrEmpty(EmailId))
            {
                EmailId = getDecryptedText(EmailId.ToString());

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_ManegmentServices.Data.User user = (from row in entities.Users
                                                           where (row.Email == EmailId)
                                                             && row.IsActive == true
                                                             && row.IsDeleted == false
                                                           select row).FirstOrDefault();
                    int CustomerId = Convert.ToInt32(user.CustomerID);
                    int UserID = Convert.ToInt32(user.ID);
                    model = PreviewAgenda(MeetingId, CustomerId);
                    if (model != null)
                    {
                        model.ParticipantID_ = ParticipantId;
                        model.userID = UserID;
                    }
                }
            }
            return View(model);
        }


        public ActionResult SaveAgendaComments(MeetingAgendaCommentVM meetingAgendaCommentVM)
        {

            meetingAgendaCommentVM = SaveAgendauserComments(meetingAgendaCommentVM);

            return Json(meetingAgendaCommentVM, JsonRequestBehavior.AllowGet);
        }

        private MeetingAgendaCommentVM SaveAgendauserComments(MeetingAgendaCommentVM obj)
        {
            MeetingAgendaCommentVM _objcoments = new MeetingAgendaCommentVM();
            try
            {
                //int UserID = 9680;
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_MeetingParticipant
                                where row.MeetingParticipantId == obj.ParticipantId
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.AgendaComments = obj.CommentData;
                        _obj.UpdatedBy = obj.UserId;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        obj.Success = true;
                        obj.Message = "Saved Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public string key = "avantis";


        public string getDecryptedText(string text)
        {
            string DecryptedString = string.Empty;
            text = text.Replace(" ", "+");
            var encryptedBytes = Convert.FromBase64String(text);

            try
            {
                DecryptedString = Encoding.UTF8.GetString(DataEncryption.Decrypt(encryptedBytes, DataEncryption.GetRijndaelManaged(key)));
            }
            catch { }


            return DecryptedString;
        }

        private PreviewAgendaVM PreviewAgenda(long meetingId, int customerId)
        {
            PreviewAgendaVM _objpreview = new PreviewAgendaVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    _objpreview = (from row in entities.BM_Meetings
                                   join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                                   join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                   where row.MeetingID == meetingId && row.Customer_Id == customerId && row.IsDeleted == false
                                   select new PreviewAgendaVM
                                   {
                                       MeetingID = row.MeetingID,
                                       IsVirtual = row.IsVirtualMeeting,
                                       GenerateMinutes = false,
                                       MeetingSrNo = row.MeetingSrNo,
                                       MeetingTypeId = row.MeetingTypeId,
                                       MeetingTypeName = meeting.MeetingTypeName,
                                       Quarter_ = row.Quarter_,
                                       MeetingTitle = row.MeetingTitle,
                                       MeetingDate = row.MeetingDate,
                                       MeetingTime = row.MeetingTime,
                                       MeetingAddressType = row.MeetingVenueType,
                                       MeetingVenue = row.MeetingVenue,

                                       MeetingStartDate = row.StartMeetingDate,
                                       MeetingStartTime = row.StartTime,
                                       MeetingEndDate = row.EndMeetingDate,
                                       MeetingEndTime = row.EndMeetingTime,

                                       IsAdjourned = row.IsAdjourned,

                                       Entityt_Id = row.EntityId,
                                       EntityCIN_LLPIN = entity.CIN_LLPIN,
                                       EntityName = entity.CompanyName,
                                       EntityAddressLine1 = entity.Regi_Address_Line1,
                                       EntityAddressLine2 = entity.Regi_Address_Line2
                                   }).FirstOrDefault();

                    if (_objpreview != null)
                    {
                        _objpreview.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, null)
                                                      select new MeetingAgendaMappingVM
                                                      {
                                                          SrNo = (int)TemplateList.SrNo,
                                                          MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                                          Meeting_Id = TemplateList.MeetingID,
                                                          AgendaID = TemplateList.BM_AgendaMasterId,
                                                          PartId = TemplateList.PartID,
                                                          AgendaItemHeading = TemplateList.AgendaItemHeading,
                                                          AgendaItemText = TemplateList.AgendaItemText,

                                                          AgendaFormat = TemplateList.AgendaFormat
                                                      }).ToList();
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return _objpreview;
        }



        public ActionResult GetAgendaRelatedComments(long participantId)
        {
            var model = GetAgendaComments(participantId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAgendaRelatedComments1(long participantId)
        {
            var model = GetAgendaComments(participantId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public MeetingAgendaCommentVM GetAgendaComments(long participantId)
        {
            var obj = new MeetingAgendaCommentVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_MeetingParticipant
                                where row.MeetingParticipantId == participantId
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        obj.CommentData = _obj.AgendaComments;
                        obj.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public ActionResult Draftminutes(long MeetingId, long DraftCirculationID, string Email)
        {
            string EmailId = Email;
            PreviewAgendaVM model = new PreviewAgendaVM();
            if (!string.IsNullOrEmpty(EmailId))
            {
                EmailId = getDecryptedText(EmailId.ToString());

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_ManegmentServices.Data.User user = (from row in entities.Users
                                                           where (row.Email == EmailId)
                                                             && row.IsActive == true
                                                             && row.IsDeleted == false
                                                           select row).FirstOrDefault();
                    int CustomerId = Convert.ToInt32(user.CustomerID);
                    int UserID = Convert.ToInt32(user.ID);
                    model = PreviewDraftMinutes(MeetingId, CustomerId, DraftCirculationID, UserID);
                    if (model != null)
                    {
                        model.userID = UserID;
                        model.DraftCirculationID_ = DraftCirculationID;
                        model.IsDraftMinutesApproved = IsDraftMinutesApproved(DraftCirculationID);
                        model.lstMeetingAttendance = GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                        model.meetingMinutesDetails = GetMinutesDetails(MeetingId);
                    }
                }
            }
            return View(model);
        }
        public PreviewAgendaVM Preview(long MeetingId, int CustomerId, bool GenerateMinutes)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_Meetings
                              join meeting in entities.BM_CommitteeComp on row.MeetingTypeId equals meeting.Id
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              where row.MeetingID == MeetingId && row.Customer_Id == CustomerId && row.IsDeleted == false
                              select new PreviewAgendaVM
                              {
                                  MeetingID = row.MeetingID,
                                  IsVirtual = row.IsVirtualMeeting,
                                  GenerateMinutes = GenerateMinutes,
                                  MeetingSrNo = row.MeetingSrNo,
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = meeting.MeetingTypeName,
                                  Quarter_ = row.Quarter_,
                                  MeetingTitle = row.MeetingTitle,
                                  MeetingDate = row.MeetingDate,
                                  MeetingTime = row.MeetingTime,
                                  MeetingAddressType = row.MeetingVenueType,
                                  MeetingVenue = row.MeetingVenue,

                                  MeetingStartDate = row.StartMeetingDate,
                                  MeetingStartTime = row.StartTime,
                                  MeetingEndDate = row.EndMeetingDate,
                                  MeetingEndTime = row.EndMeetingTime,

                                  IsAdjourned = row.IsAdjourned,

                                  Entityt_Id = row.EntityId,
                                  EntityCIN_LLPIN = entity.CIN_LLPIN,
                                  EntityName = entity.CompanyName,
                                  EntityAddressLine1 = entity.Regi_Address_Line1,
                                  EntityAddressLine2 = entity.Regi_Address_Line2
                              }).FirstOrDefault();

                if (result != null)
                {
                    result.lstAgendaItems = (from TemplateList in entities.BM_SP_MeetingAgendaTemplateList(MeetingId, null, GenerateMinutes, null)
                                             select new MeetingAgendaMappingVM
                                             {
                                                 SrNo = (int)TemplateList.SrNo,
                                                 MeetingAgendaMappingID = (long)TemplateList.MeetingAgendaMappingID,
                                                 Meeting_Id = TemplateList.MeetingID,
                                                 AgendaID = TemplateList.BM_AgendaMasterId,
                                                 PartId = TemplateList.PartID,
                                                 AgendaItemHeading = TemplateList.AgendaItemHeading,
                                                 AgendaItemText = TemplateList.AgendaItemText,

                                                 AgendaFormat = TemplateList.AgendaFormat
                                             }).ToList();
                }
                return result;
            }
        }
        public MeetingMinutesDetailsVM GetMinutesDetails(long meetingId)
        {
            MeetingMinutesDetailsVM result = new MeetingMinutesDetailsVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_MeetingMinutesDetails
                                  where row.MeetingId == meetingId
                                  select new MeetingMinutesDetailsVM
                                  {
                                      MeetingMinutesDetailsId = row.Id,
                                      MOM_MeetingId = row.MeetingId,
                                      Place = row.Place,
                                      DateOfEntering = row.DateOfEntering,
                                      DateOfSigning = row.SigningOfEntering,
                                      IsCirculate = row.IsCirculate,
                                      IsFinalized = row.IsFinalized,
                                      ConfirmMeetingId = row.ConfirmMeetingId
                                  }).FirstOrDefault();
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
            return result;
        }
        public List<MeetingAttendance_VM> GetPerticipenforAttendenceCS(long meetingID, int customerId, int? userId)
        {
            List<MeetingAttendance_VM> getParticipant = new List<MeetingAttendance_VM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    getParticipant = (from row in entities.BM_SP_GetParticipantAttendance(meetingID)
                                          select new MeetingAttendance_VM
                                          {
                                              ParticipantId = row.MeetingParticipantId,
                                              MeetingParticipant_Name = row.Participant_Name,
                                              MeetingId = row.Meeting_ID,
                                              Director_Id = row.Director_Id,
                                              ImagePath = row.Photo_Doc,
                                              Designation = row.Designation,
                                              Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                              Remark = row.Reason,
                                              Position = row.Position,
                                              RSVP = row.RSVP,
                                              RSVP_Reason = row.RSVP_Reason
                                          }).ToList();

                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
            return getParticipant;
        }
        public bool IsDraftMinutesApproved(long draftCirculationID)
        {
            var result = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_MeetingMinutesDetailsTransaction
                              where row.ID == draftCirculationID && row.ApprovedOn != null
                              select row).Any();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public PreviewAgendaVM PreviewDraftMinutes(long MeetingId, int CustomerId, long DraftCirculationID, int UserID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var minutesDetails = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                          where row.ID == DraftCirculationID && row.IsDeleted == false && row.ReadOn == null
                                          select row).FirstOrDefault();
                    if (minutesDetails != null)
                    {
                        minutesDetails.ReadOn = DateTime.Now;
                        minutesDetails.UpdatedBy = UserID;
                        minutesDetails.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Preview(MeetingId, CustomerId, true);
        }

        public ActionResult GetComments(long draftCirculationID)
        {
            var model = GetdraftComments(draftCirculationID);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public MeetingMinutesCommentVM GetdraftComments(long draftCirculationID)
        {
            var obj = new MeetingMinutesCommentVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                where row.ID == draftCirculationID
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        obj.CommentData = _obj.DraftComments;
                        obj.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        [HttpPost]
        public ActionResult ApproveDraftMinutes(MeetingMinutesApproveVM obj)
        {
            int UserID = Convert.ToInt32(obj.userId);
            obj = ApproveDraftMinutesDocuments(obj, UserID);

            return PartialView("_DraftMinutesApprove", obj);
        }

        public MeetingMinutesApproveVM ApproveDraftMinutesDocuments(MeetingMinutesApproveVM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                where row.ID == obj.DraftCirculationID && row.IsDeleted == false && row.ApprovedOn == null
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.ApprovedOn = DateTime.Now;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Approved Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public ActionResult SaveComments(MeetingMinutesCommentVM meetingMinutesCommentVM)
        {
            int UserID = Convert.ToInt32(meetingMinutesCommentVM.UserId);
            meetingMinutesCommentVM = SaveMinutesComments(meetingMinutesCommentVM, UserID);

            return Json(meetingMinutesCommentVM, JsonRequestBehavior.AllowGet);
        }

        public MeetingMinutesCommentVM SaveMinutesComments(MeetingMinutesCommentVM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                                where row.ID == obj.DraftCirculationID
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.DraftComments = obj.CommentData;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        obj.Success = true;
                        obj.Message = "Saved Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
    }

    public class DataEncryption
    {
        public static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            byte[] decode = null;
            try
            {
                decode = rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            }
            catch
            {

            }
            return decode;
        }

        public static RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }
    }

}