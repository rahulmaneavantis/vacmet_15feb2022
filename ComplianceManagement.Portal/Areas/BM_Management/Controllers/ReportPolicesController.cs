﻿using BM_ManegmentServices.Services.Reports;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ReportPolicesController : Controller
    {
        IReportPolices obj;
        public ReportPolicesController(IReportPolices obj)
        {
            this.obj = obj;
        }
        // GET: BM_Management/ReportPolices
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }


        public ActionResult UploadReportview(int ID)
        {
            ReportVM objreportvm = new ReportVM();
            if(ID>0)
            {
                objreportvm = obj.GetReportPolices(ID);
            }
            return PartialView("_UploadReports", objreportvm);
        }

        public ActionResult UploadReport(ReportVM objreport)
        {
            if(objreport!=null && ModelState.IsValid)
            {
                objreport.UserId = AuthenticationHelper.UserID;
                objreport.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                objreport = obj.uploadFile(objreport);
            }
            return PartialView("_UploadReports", objreport);
        }
    }
}