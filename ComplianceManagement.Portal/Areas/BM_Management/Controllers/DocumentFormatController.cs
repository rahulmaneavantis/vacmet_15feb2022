﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Mvc;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class DocumentFormatController : Controller
    {

        IDocumentFormat objDocument;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
        public DocumentFormatController(IDocumentFormat objDoc)
        {
            objDocument = objDoc;
        }

        // GET: BM_Management/DocumentFormate
        public ActionResult Index()
        {            
            return View();                       
        }
        
        public ActionResult DocumentFormat()
        {           
            return View();
        }

        [HttpPost]
        public JsonResult SaveDocumentFormat(BM_DocumentFormat Data)
        {
            if (Data.TemplateFormate != null)
            {
                Data.CustomerId = CustomerId;
                Data.EntityId = Convert.ToInt32(0);
                Data.CrBy = UserId;
                Data.CrDate = DateTime.Now;
                objDocument.SaveDocumentFormat(Data);
            }
            return Json( Data,JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetDocumentFormat(string TemplateFormat,int CustomerId)
        {
            List<BM_DocumentFormat> documentFormats = new List<BM_DocumentFormat>();
            documentFormats = objDocument.GetDocumentFormat(TemplateFormat, CustomerId);

            var subCategoryToReturn = documentFormats.Select(S => new {
                TemplateFormate = S.TemplateFormate,
                FontName = S.FontName,
                FontSize = S.FontSize,
                FontColor = S.FontColor,
                IsBold = S.IsBold,
                IsItalic = S.IsItalic,
                IsUnderline = S.IsUnderline
            });
            //var JsonData= Newtonsoft.Json.JsonConvert.SerializeObject(documentFormats, Newtonsoft.Json.Formatting.None);

            return Json(subCategoryToReturn, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult TemplatePreview()
        {            
            return PartialView("_DocumentTemplate");
        }

        [HttpPost]
        public JsonResult TemplatePreviewShow(int CustomerId)
        {
            CustomerId = CustomerId;
            List<BM_DocumentFormat> documentFormats = new List<BM_DocumentFormat>();

            documentFormats = objDocument.GetDocumentFormat("", CustomerId);

            var model = documentFormats.Select(S => new
            {
                TemplateFormate = S.TemplateFormate,
                FontName = S.FontName,
                FontSize = S.FontSize,
                FontColor = S.FontColor,
                IsBold = S.IsBold,
                IsItalic = S.IsItalic,
                IsUnderline = S.IsUnderline
            });

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}