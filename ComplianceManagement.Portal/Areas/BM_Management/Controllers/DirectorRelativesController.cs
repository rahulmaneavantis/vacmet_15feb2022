﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.Services.Forms;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class DirectorRelativesController : Controller
    {
        IDirectorRelative objDirectorRelative;

        public DirectorRelativesController(IDirectorRelative obj)
        {
            objDirectorRelative = obj;
        }
        public ActionResult Create(long directorId)
        {
            var model = new DirectorRelativesVM();
            model.Director_ID = directorId;
            return PartialView("_AddEditRelativeNew", model);
        }

        public ActionResult Edit(long directorId, long id)
        {
            var model = objDirectorRelative.GetRelative(id, directorId);
            if(model == null)
            {
                model = new DirectorRelativesVM();
                model.Director_ID = directorId;
            }
            return PartialView("_AddEditRelativeNew", model);
        }

        public ActionResult SaveOrUpdate(DirectorRelativesVM obj)
        {

            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            if(obj.Id > 0)
            {
                obj.UpdatedBy = userID;
                obj = objDirectorRelative.UpdateRelatives(obj);
            }
            else
            {
                obj.CreatedBy = userID;
                obj = objDirectorRelative.AddRelatives(obj);
            }
            return PartialView("_AddEditRelativeNew", obj);
        }

        [HttpPost]
        public ActionResult DeleteRelative(long id, long directorId)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var obj = objDirectorRelative.Delete(id, directorId, userID);
            return Json(obj , JsonRequestBehavior.AllowGet);
        }
    }
}