﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class BankAccDetailsController : Controller
    {
        // GET: BM_Management/BankAccDetails
        IBankAccDetailsMaster objaddition;
        public BankAccDetailsController(IBankAccDetailsMaster objaddition)
        {
            this.objaddition = objaddition;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddNewDetails(int Id, int EntityId)
        {
            BankAccDetails_VM _objnewCompliances = new BankAccDetails_VM();
            if (Id > 0)
            {
                _objnewCompliances = objaddition.EditBankDetails(Id,EntityId);
            }
            return PartialView("_AddBankAcc", _objnewCompliances);
        }
        [HttpPost]
        public ActionResult CreateBankDetails(BankAccDetails_VM _objadditionalcomp)
        {
            int UserId = AuthenticationHelper.UserID;
            if (_objadditionalcomp.Id == 0)
            {
                _objadditionalcomp = objaddition.AddBankDetails(_objadditionalcomp, UserId);
            }
            else
            {
                _objadditionalcomp = objaddition.UpdateBankDetails(_objadditionalcomp, UserId);
            }

            return PartialView("_AddBankAcc", _objadditionalcomp);
        }
    }
}