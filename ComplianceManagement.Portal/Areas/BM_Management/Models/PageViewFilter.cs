﻿using Autofac.Core;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models
{
    public class PageViewFilter : ActionFilterAttribute
    {

        private ComplianceDBEntities entities = new ComplianceDBEntities();
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            int UserId = AuthenticationHelper.UserID;
            string RoleId = AuthenticationHelper.Role;
            filterContext.Controller.ViewBag.UserName = AuthenticationHelper.User;
            filterContext.Controller.ViewBag.RoleIdK = RoleId;
            base.OnResultExecuting(filterContext);
        }

    }
}