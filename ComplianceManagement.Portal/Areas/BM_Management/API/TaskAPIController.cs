﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/TaskAPI")]
    public class TaskAPIController : ApiController
    {
        ITask obj;

        public TaskAPIController(ITask obj)
        {
            this.obj = obj;
        }

        [Route("GetMeetingList")]
        [HttpGet]
        public IEnumerable<TaskMeeting> GetMeetingList(long EntityId)
        {
            IEnumerable<TaskMeeting> obj1 = new List<TaskMeeting>();

            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            string userRole = AuthenticationHelper.Role;

            obj1 = obj.GetMeetingList(EntityId, userID, userRole);
            return obj1;
        }

        [Route("GetAgendaList")]
        [HttpGet]
        public IEnumerable<BM_MeetingAgendaMapping> GetAgendaList(int MeetingId)
        {
            return obj.GetAgendaList(MeetingId);
        }

        [Route("GetAssignUser")]
        [HttpGet]
        public DataSourceResult GetAssignUser([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int TaskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.ReadData(TaskId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        //[Route("CreateAssigTask")]
        //[HttpPost]
        //public IHttpActionResult CreateAssigTask(VMTask _objentity)
        //{
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
        //    if (ModelState.IsValid)
        //    {
        //        _objentity = obj.CreateTaskAssinment(_objentity);
        //    }
        //    else
        //    {
        //        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
        //    }
        //    return Ok();

        //}

        [Route("CreateAssigTask")]
        [HttpPost]
        public HttpResponseMessage CreateAssigTask(VMTask _objentity)
        {
            if (ModelState.IsValid)
            {
                _objentity.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                _objentity.UserId = AuthenticationHelper.UserID;
                _objentity = obj.CreateTaskAssinment(_objentity);

                DataSourceResult result = new DataSourceResult
                {
                    Data = new[] { _objentity },
                    Total = 1
                };
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, result);
                // response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = _objentity.TaskAssinmentId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }


        [Route("UpdateAssigTask")]
        [HttpPut]
        public IHttpActionResult UpdateAssigTask(VMTask vmTask)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //var gridData = obj.UpdateAssigTask(_objentity);

            //return result;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                vmTask = obj.UpdateAssigTask(vmTask);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();
        }

        [Route("GetTaskDetails")]
        [HttpGet]
        public DataSourceResult GetTaskDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, bool? loadTask, bool? loadReview)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            var gridData = obj.GetTaskDetails(customerId, UserId, 3, loadTask, loadReview);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetReviewDetails")]
        [HttpGet]
        public DataSourceResult GetReviewDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, bool? loadTask, bool? loadReview)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            string role = AuthenticationHelper.Role;
            var gridData = obj.GetTaskDetails(customerId, UserId, 4, loadTask, loadReview);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetPerformerDetails")]
        [HttpGet]
        public DataSourceResult GetPerformerDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int TaskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            var gridData = obj.GetPerformerDetails(customerId, UserId, TaskId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetPerformerstatus")]
        [HttpGet]
        public DataSourceResult GetPerformerstatus([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int TaskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            var gridData = obj.GetPerformerstatus(customerId, UserId, TaskId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        //[Route("GetTask")]
        //[HttpGet]
        //public DataSourceResult GetTask([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int TaskId)
        //{
        //    int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //    int UserId = AuthenticationHelper.UserID;
        //    string role = AuthenticationHelper.Role;
        //    var gridData = obj.GetTask(TaskId);
        //    var result = new DataSourceResult()
        //    {
        //        Data = gridData,
        //        Total = gridData.Count
        //    };
        //    return result;
        //}

        [Route("GetTaskType")]
        [HttpGet]
        public DataSourceResult GetTaskType([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int TaskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            string role = AuthenticationHelper.Role;
            var gridData = obj.GetTaskType(TaskId, UserId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetTaskFileDetails")]
        [HttpGet]
        public DataSourceResult GetTaskFileDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long TaskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            List<TaskFile> _taskfile = new List<TaskFile>();
            _taskfile = obj.GetTaskFileDetails(TaskId, UserId, customerId);
            var result = new DataSourceResult()
            {
                Data = _taskfile,
                Total = _taskfile.Count
            };
            return result;
        }

        [Route("DeleteFileData")]
        [HttpDelete]
        public HttpResponseMessage DeleteEntity(long FileId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (FileId > 0)
            {
                bool result = obj.DeleteTask(FileId);
            }

            return response;
        }

        [Route("GetAgendaMinutesReviewDetails")]
        [HttpGet]
        public DataSourceResult GetAgendaMinutesReviewDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long TaskId)
        {
            var taskDetails = obj.GetReviewerTaskDetails(TaskId);
            var result = new DataSourceResult()
            {
                Data = taskDetails,
                Total = taskDetails == null ? 0 : taskDetails.Count
            };
            return result;
        }

        [Route("GetAgendaMinutesAllReviewDetails")]
        [HttpGet]
        public DataSourceResult GetAgendaMinutesAllReviewDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long entityId, long meetingId, int taskType)
        {
            var taskDetails = obj.GetReviewerTaskDetails(entityId, meetingId, taskType);
            var result = new DataSourceResult()
            {
                Data = taskDetails,
                Total = taskDetails == null ? 0 : taskDetails.Count
            };
            return result;
        }

        [Route("GetAgendaMinutesAllReviewDetailsTillReview")]
        [HttpGet]
        public DataSourceResult GetAgendaMinutesAllReviewDetailsTillReview([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long entityId, long meetingId, int taskType, long? maxTaskAssignmentId)
        {
            var taskDetails = obj.GetReviewerTaskDetails(entityId, meetingId, taskType);
            if(taskDetails != null && maxTaskAssignmentId > 0)
            {
                taskDetails = (from row in taskDetails
                              where row.Id < maxTaskAssignmentId
                               select row).OrderBy(k => k.Id).ToList();
            }

            var result = new DataSourceResult()
            {
                Data = taskDetails,
                Total = taskDetails == null ? 0 : taskDetails.Count
            };
            return result;
        }

        [Route("GetReviewLog")]
        [HttpGet]
        public DataSourceResult GetReviewLog([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long taskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var taskDetails = obj.GetReviewLog(taskId, customerId);
            //var taskDetails = obj.GetReviewLogs(taskId, customerId);
            var result = new DataSourceResult()
            {
                Data = taskDetails,
                Total = taskDetails == null ? 0 : taskDetails.Count
            };
            return result;
        }


        [Route("GetReviewLogs")]
        [HttpGet]
        public DataSourceResult GetReviewLogs(long taskId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //var taskDetails = obj.GetReviewLog(taskId, customerId);
            var taskDetails = obj.GetReviewLogs(taskId, customerId);
            var result = new DataSourceResult()
            {
                Data = taskDetails,
                Total = taskDetails == null ? 0 : taskDetails.Count
            };
            return result;
        }
    }
}
