﻿using BM_ManegmentServices.Services.AOC4;
using BM_ManegmentServices.Services.HelpVideo;
using BM_ManegmentServices.Services.Masters;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/AOC4")]
    public class AOC4APIController : ApiController
    {
        IAOC4Service objIAOC4Service;
        public AOC4APIController(IAOC4Service objAOC4Service)
        {
            objIAOC4Service = objAOC4Service;
        }

        [Route("GetSavedForms")]
        [HttpGet]
        public DataSourceResult GetSavedForms([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIAOC4Service.GetAOC4List(customerID);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetSavedFormDetails")]
        [HttpGet]
        public DataSourceResult GetSavedForms([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIAOC4Service.GetAOC4DetailsById(id, customerID);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}

