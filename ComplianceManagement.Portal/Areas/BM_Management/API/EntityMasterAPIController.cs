﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/EntityMasterAPI")]
    public class EntityMasterAPIController : ApiController
    {
        IEntityMaster objEntityMaster;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public EntityMasterAPIController(IEntityMaster obj)
        {
            objEntityMaster = obj;
        }

        [Route("GetDropDownforCustomer")]
        [HttpGet]
        public IEnumerable<Customer_VM> GetDropDownforCustomer()
        {
            return objEntityMaster.GetCustomers();
        }

        #region User-wise Entity Details

        [Route("GetEntity")]
        [HttpGet]
        public DataSourceResult GetEntity([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = objEntityMaster.GetAllEntityMaster(customerId, userID, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetEntityBranchDetails")]
        [HttpGet]
        public DataSourceResult GetEntityBranchDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = objEntityMaster.GetEntityBranchList(customerId, userID, role, EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetEntitydetails")]
        [HttpGet]
        public DataSourceResult GetEntitybyId([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = objEntityMaster.GetAllEntityMasterbyid(customerId, userID, role, EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetDropDownforEntity")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetDroupDownforEntity(bool showAll = false)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var item = objEntityMaster.GetAllEntityMaster(customerId, userID, role);

            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            if(showAll)
            {
                item.Insert(0, new EntityMasterVM() { EntityName = "All", Id = 0, });
            }
            return item;
        }

        [Route("GetEntityListForTask")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetEntityListForTask()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var item = objEntityMaster.GetEntityForTask(customerId, userID, role);

            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            return item;
        }


        [Route("EntityListCommittewise")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> EntityListCommittewise()
        {
            int userID = AuthenticationHelper.UserID;
            return objEntityMaster.EntityListdirectorwise(userID);
        }

        [Route("GetDropDownEntityForMeeting")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetAllEntityMasterForMeeting()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var item = objEntityMaster.GetAllEntityMasterForMeeting(customerId, userID, role);

            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            return item;
        }

        [Route("GetAllEntityMasterForMeetingNew")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetAllEntityMasterForMeetingNew()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            return objEntityMaster.GetAllEntityMasterForMeetingNew(customerId, userID, role);
        }

        #endregion


        [Route("CreateEntity")]
        [HttpPost]
        public IHttpActionResult CreateEntity(EntityMasterVM _objentity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = objEntityMaster.AddEntityMasterdtls(_objentity, CustomerId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }



        [Route("DeleteEntity")]
        [HttpDelete]
        public HttpResponseMessage DeleteEntity(int Id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                bool result = objEntityMaster.DeleteEntityInfo(Id, CustomerId);
            }

            return response;
        }

        [Route("UpdateEntity")]
        [HttpPut]
        public HttpResponseMessage UpdateEntity(EntityMasterVM _objentity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = objEntityMaster.UpdateEntity(_objentity);
                response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }

            return response;
        }

        [Route("GetDropDownforEntityType")]
        [HttpGet]
        public IEnumerable<BM_EntityType> GetDroupDownforEntityType()
        {
            return objEntityMaster.GetCompanyType();
        }

        [Route("GetDropDownforEntityTypeVM")]
        [HttpGet]
        public IEnumerable<EntityTypeVM> GetDroupDownforEntityTypeVM()
        {
            return objEntityMaster.GetCompanyTypeVM();
        }

        [Route("EntityTypeVMDropdownForMeeting")]
        [HttpGet]
        public IEnumerable<EntityTypeVM> EntityTypeVMDropdownForMeeting()
        {
            return objEntityMaster.EntityTypeVMForMeeting();
        }

        [Route("GetDropDownEntityAddressForMeeting")]
        [HttpGet]
        public IEnumerable<EntityAddressVM> GetEntityAddressForMeeting(int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objEntityMaster.GetEntityAddressForMeeting(EntityId, customerId);
        }

        [Route("GetDropDownforDiscriptionofBusinessActivity")]
        [HttpGet]
        public IEnumerable<BM_BusinessActivity> GetDropDownforDiscriptionofBusinessActivity()
        {
            return objEntityMaster.GetBusinessActivityCode();
        }

        [Route("GetDropDownforBusinessActivity")]
        [HttpGet]
        public IEnumerable<BM_BusinessActivity> GetDropDownforBusinessActivity(int BusinessId)
        {
            return objEntityMaster.GetBusinessActivDesc(BusinessId);
        }

        [Route("GetDropDownforNationality")]
        [HttpGet]
        public IEnumerable<BM_Nationality> GetDropDownforNationality()
        {
            return objEntityMaster.GetNationality();
        }

        //GetMultiselectorStockExchange

        [Route("GetMultiselectorStockExchange")]
        [HttpGet]
        public IEnumerable<BM_StockExchange> GetMultiselectorStockExchange()
        {
            return objEntityMaster.GetAllStockExchange(true);
        }

        [Route("GetStockExchangeForTemplateMaster")]
        [HttpGet]
        public IEnumerable<BM_StockExchange> GetStockExchangeForTemplateMaster()
        {
            return objEntityMaster.GetAllStockExchange(false);
        }

        [Route("GetDropDownforState")]
        [HttpGet]
        public IEnumerable<VMState> GetDropDownforState()
        {
            return objEntityMaster.GetStateList();
        }

        [Route("GetDropDownforStateMaster")]
        [HttpGet]
        public IEnumerable<VMState> GetDropDownforStateMaster(int CountryId)
        {
            return objEntityMaster.GetStateListforMaster(CountryId);
        }

        [Route("GetDropDownforCity")]
        [HttpGet]
        public IEnumerable<VMCity> GetDropDownforCity(int? stateId)
        {
            return objEntityMaster.GetCityList(stateId);
        }

        [Route("GetDropDownforCompanyCategory")]
        [HttpGet]
        public IEnumerable<BM_CompanyCategory> GetDropDownforCompanyCategory()
        {
            return objEntityMaster.GetCompanyCategory();
        }

        [Route("GetDropDownforCompanySubCategory")]
        [HttpGet]
        public IEnumerable<BM_CompanySubCategory> GetDropDownforCompanySubCategory()
        {
            return objEntityMaster.GetDropDownforCompanySubCategory();
        }

        [Route("GetDropDownforROCCode")]
        [HttpGet]
        public IEnumerable<BM_ROC_code> GetDropDownforROCCode()
        {
            return objEntityMaster.GetRocCode();
        }

        [Route("GetDropDownEntityForCountry")]
        [HttpGet]
        public IEnumerable<VMCountry> GetDropDownEntityForCountry()
        {
            return objEntityMaster.GetAllCountryDtls();

        }

        [Route("GetDropDownEntityForBodyCorporateCity")]
        [HttpGet]
        public IEnumerable<VMCity> GetDropDownEntityForBodyCorporateCity(int? stateId)
        {
            return objEntityMaster.GetAllCountryCitydtlsForBodyCorporate(stateId);

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                objEntityMaster.Dispose();
            }
            base.Dispose(disposing);
        }

        #region used in Director Master
        [Route("GetDropDownEntityForDetailsOfInterest")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetDropDownEntityForDetailsOfInterest()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var item = objEntityMaster.GetAllEntityMaster(customerId);
            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            item.Add(new EntityMasterVM() { Id = -1, EntityName = "Other", Entity_Type = 0 });
            return item;
            //return objEntityMaster.GetCompanyType();
        }

        [Route("EntitySubType")]
        [HttpGet]
        public IEnumerable<AssosiateType> EntitySubType()
        {
            var getentitysubtype = objEntityMaster.GetAllEntityMaster();
            return getentitysubtype;
            //return objEntityMaster.GetCompanyType();
        }

        [Route("GetDropDownEntityForKMP")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetDropDownEntityForKMP()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var item = objEntityMaster.GetAllEntityMaster(customerId);
            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            item.Add(new EntityMasterVM() { Id = -1, EntityName = "Other", Entity_Type = 0 });
            return item;
            //return objEntityMaster.GetCompanyType();
        }

        [Route("GetDropDownEntityForCommittee")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetDropDownEntityForCommittee(long DirectorId)
        {
            var item = objEntityMaster.GetAllEntityMasterForCommittee(DirectorId);
            if (item == null)
            {
                item = new List<EntityMasterVM>();
            }
            return item;
        }
        #endregion

        [Route("GetFY")]
        [HttpGet]
        public string GetFY(long EntityId)
        {
            string result = objEntityMaster.GetFY(EntityId);

            return result;
        }

        [Route("SubEntityDetails")]
        [HttpGet]
        public DataSourceResult SubEntityDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long EntityID, long CustomerID)
        {

            var gridData = objEntityMaster.GetSubEntityDetails(EntityID, CustomerID);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetEntityDetails")]
        [HttpGet]
        public IHttpActionResult GetEntityDetails(long EntityId, string CIN)
        {
            SubCompanyType objdetails = new SubCompanyType();
            if (EntityId > 0)
            {
                objdetails = objEntityMaster.getDetailsofentity(EntityId, CIN,CustomerId);
            }
            return Json(objdetails);
        }

        [Route("GetEntityForBOD")]
        [HttpGet]
        public DataSourceResult GetEntityForBOD([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = objEntityMaster.GetAllEntityForBOD(customerId, userID);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        #region Created by Ruchi  For Company Profile  on 17th June
        [Route("GetDirectorWiseEntity")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetDirectorWiseEntity()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            return objEntityMaster.GetAllEntityForBOD(customerId, userID);

        }
        #endregion Created by Ruchi  For Company Profile  on 17th June


        [Route("DirectorAssignedEntities")]
        [HttpGet]
        public List<EntityMasterVM> GetDirectorAssignedEntities(int custID, int userID)
        {
            var lstEntities = objEntityMaster.GetAllEntityForBOD(custID, userID);
            return lstEntities;
        }

        [Route("MyAssignedEntities")]
        [HttpGet]
        public List<EntityMasterVM> GetMyAssignedEntities(int custID, int userID, string userRole)
        {
            var lstEntities = objEntityMaster.GetAssignedEntities(custID, userID, userRole);
            return lstEntities;
        }

        [Route("GetEntitiesDropDown")]
        [HttpGet]
        public List<EntityMasterVM> GetEntitiesDropDown(int custID, int userID, string userRole)
        {
            var lstEntities = objEntityMaster.GetEntitiesDropDown(custID, userID, userRole);
            return lstEntities;
        }

        //[Route("EntityDocumentType")]
        //[HttpGet]
        //public IEnumerable<EntityDocTypeVM> GetDocumentType()
        //{
        //    return objEntityMaster.GetDocumentType();
        //}
		[Route("EntityDocumentType")]
        [HttpGet]
        public IEnumerable<EntityDocTypeVM> GetDocumentType(int EntityType)
        {
            return objEntityMaster.GetDocumentType(EntityType);
        }
        [Route("UploadChargeDocument")]
        [HttpGet]
        public IEnumerable<EntityDocTypeVM> UploadChargeDocument(int EntityType)
        {
            return objEntityMaster.UploadChargeDocument(EntityType);
        }

        [Route("RegulatoryAuthority")]
        [HttpGet]
        public IEnumerable<SubIndustriesVM> RegulatoryAuthority()
        {
            return objEntityMaster.GetRegulatoryAuthority();
        }
        [Route("GetEntitiesListForCopyMeeting")]
        [HttpGet]
        public List<EntityMasterVM> GetEntitiesListForCopyMeeting(int entityId, int entityType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objEntityMaster.GetEntitiesListForCopyMeeting(entityId, entityType, customerId);
        }
    }
}
