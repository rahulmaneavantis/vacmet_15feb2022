﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ChargeDetails")]
    public class ChargeDetailsAPIController : ApiController
    {
        IChargeDetailsMaster objadditional;

        public ChargeDetailsAPIController(IChargeDetailsMaster obj)
        {
            objadditional = obj;
        }


        [Route("GetAllChargeIndex")]
        [HttpGet]
        public DataSourceResult GetAllChargeIndex([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int entityId)
        {

            var gridData = objadditional.GetChargeIndex(entityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetAllChargeIndexDropdown")]
        [HttpGet]
        public IEnumerable<ChargeHeaderVM> GetAllChargeIndexDropdown(int entityId, bool showAllOption)
        {
            var result = objadditional.GetChargeIndex(entityId);
            if(result == null)
            {
                result = new List<ChargeHeaderVM>();
            }
            if(showAllOption)
            {
                result.Insert(0, new ChargeHeaderVM() { ChargeHeaderId = 0, ChargeId = "All" });
            }

            return result;
        }

        [Route("GetAllCharges")]
        [HttpGet]
        public DataSourceResult GetAllCharges([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long chargeHeaderId)
        {

            var gridData = objadditional.GetCharge(chargeHeaderId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("DeleteCustomers")]
        [HttpDelete]
        public HttpResponseMessage DeleteCustomers(long Id)
        {
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = null;
            //if (Id > 0)
            //{
            //    bool result = objadditional.DeleteChargeID(Id, UserId);
            //    if (result)
            //    {
            //        response = Request.CreateResponse(HttpStatusCode.OK);
            //    }
            //    else
            //    {
            //        response = Request.CreateResponse(HttpStatusCode.NotFound);
            //    }
            //}
            //else
            //{
            //    response = Request.CreateResponse(HttpStatusCode.BadRequest);
            //}

            return response;

        }
        [Route("GetDropDownforChargeType")]
        [HttpGet]
        public IEnumerable<ChargeTypeList_VM> GetDropDownforChargeType()
        {
            return objadditional.GetAllChargeDetails();

            
        }
        [Route("DeleteChargeDetails")]
        [HttpDelete]
        public HttpResponseMessage DeleteChargeDetails(int Id)
        {
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = null;
            if (Id > 0)
            {
                bool result = objadditional.DeleteChargeDetails(Id, UserId);
                if (result)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return response;
        }

        [Route("GetDocument")]
        [HttpGet]
        public IEnumerable<EntityDocumentVM> GetParticularDocument()
        {
            return objadditional.GetParticularDocument();
        }
    }
}
