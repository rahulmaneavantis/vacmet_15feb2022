﻿using BM_ManegmentServices.Services.Masters;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    public class KMPMasterAPIController : ApiController
    {
        IKMP_Master objKMP;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        public KMPMasterAPIController(IKMP_Master objKMP)
        {
            this.objKMP = objKMP;
        }

        [Route("api/KMIMasterAPI/GetKMP")]
        [HttpGet]
        public DataSourceResult GetKMP([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objKMP.GetKMP_Dtls(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("api/KMIMasterAPI/KMP")]
        [HttpGet]
        public IEnumerable<BM_ManegmentServices.VM.Director_MasterVM_KMP> BOD()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objKMP.GetKMP_Dtls(customerId);
        }

        [Route("api/KMPMasterAPI/KMPByDesignationId")]
        [HttpGet]
        public IEnumerable<BM_ManegmentServices.VM.Director_MasterVM_KMP> KMPByDesignationId(int DesignationId, long? EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objKMP.GetKMPByDesignation(DesignationId, EntityId, customerId);
        }


        [Route("api/KMIMasterAPI/GetKMPSecurities")]
        [HttpGet]
        public DataSourceResult GetKMPSecurities([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long KMPId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objKMP.GetKMPseurityDtails(customerId,KMPId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("api/KMIMasterAPI/DeleteKMPData")]
        [HttpDelete]
        public HttpResponseMessage DeleteKMPData(int Id)
        {
            HttpResponseMessage response = null;
            if (Id > 0)
            {
                bool result = objKMP.DeleteKMPInfo(Id, CustomerId);
                if (result)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return response;
        }
    }
}
