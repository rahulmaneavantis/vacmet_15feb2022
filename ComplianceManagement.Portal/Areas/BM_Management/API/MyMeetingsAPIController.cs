﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.Dashboard;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.Services.MyMeetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using BM_ManegmentServices.VM.MyMeetings;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/MyMeetings")]
    public class MyMeetingsAPIController : ApiController
    {
        IMeeting_Service objIMeeting_Service;
        IMainMeeting objIMainMeeting;
        IMyMeetingsService objIMyMeetingsService;

        public MyMeetingsAPIController(IMeeting_Service obj, IMainMeeting objMainMeeting, IMyMeetingsService objMyMeetingsService)
        {
            objIMeeting_Service = obj;
            objIMainMeeting = objMainMeeting;
            objIMyMeetingsService = objMyMeetingsService;
        }

        [Route("DraftMeetings")]
        [HttpGet]
        public DataSourceResult DraftMeetings([DataSourceRequest] DataSourceRequest request, int customerId, int userId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMeeting_Service.GetDraftMeetingsForDirectorApproval(customerId, userId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("DraftMeetings1")]
        [HttpGet]
        public DataSourceResult DraftMeetings1([DataSourceRequest] DataSourceRequest request,int userId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMeeting_Service.GetDraftMeetingsForDirectorApproval(userId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("ConcludedMeetings")]
        [HttpGet]
        public DataSourceResult ConcludedMeetings([DataSourceRequest] DataSourceRequest request, int customerId, int userId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMeeting_Service.GetConcludedMeetingsForDirector(customerId, userId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            //if (request.Sorts.Count == 0)
            //{
            //    request.Sorts.Add(new SortDescriptor("row_num", System.ComponentModel.ListSortDirection.Descending));
            //}
            return result;
        }

        [Route("UpcomingMeetings")]
        [HttpGet]
        public DataSourceResult UpcomingMeetings([DataSourceRequest] DataSourceRequest request, int customerId, int userId, string role)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMeeting_Service.GetUpcomingMeetings(customerId, userId, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("ConcludedMeetingAgenda")]
        [HttpGet]
        public List<ConcludedMeetingAgendaItemVM> ConcludedMeetingAgenda(int customerId, int userId)
        {
            //int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            return objIMyMeetingsService.ConcludedMeetingAgenda(customerId, userId);
        }

        [Route("ConcludedMeetingAgenda_CS")]
        [HttpGet]
        public List<ConcludedMeetingAgendaItemVM> ConcludedMeetingAgenda_CS(int customerId, int userId, string role)
        {
            return objIMyMeetingsService.ConcludedMeetingAgenda_CS(customerId, userId, role);
        }


        //Display meeting RSVP details
        [Route("MeetingsRSVPDetails")]
        [HttpGet]
        public DataSourceResult MeetingsRSVPDetails([DataSourceRequest] DataSourceRequest request, long meetingId, int customerId, int userId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var gridData = objIMainMeeting.GetPerticipenforAttendenceCS(meetingId, customerId, userId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}