﻿using BM_ManegmentServices.Services.Annotation;
using BM_ManegmentServices.VM.Annotation;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

/**
 * AnnotationController.cs
 * This is a sample of basic server-side annotation handling using ASP.NET.
 * When WebViewer makes a POST request with annotations to save, this script will save the annotations to a file.
 * When WebViewer makes a GET request to load the annotations, this script will fetch the annotations from the file and return it.
 * Note that this is only a sample and does not take account of security and concurrency.
 **/

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/AnnotationAPIController")]
    public class AnnotationAPIController : ApiController
    {
        IAnnotationService objIAnnotationService;
        public AnnotationAPIController(IAnnotationService objAnnotationService)
        {
            objIAnnotationService = objAnnotationService;
        }

        // Handle GET request sent to '/api/annotation'
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            //string filePath = GetFilePath(this.Request);
            //if (File.Exists(filePath))
            //{
            //    // Read from the XFDF file and send the string as a response
            //    string xfdfData = File.ReadAllText(filePath);
            //    return new HttpResponseMessage() { Content = new StringContent(xfdfData) };
            //}
            //else
            //{
            //    return new HttpResponseMessage(HttpStatusCode.NoContent);
            //}

            string agendaMinutesCommentsId = GetId(this.Request);
            if (!string.IsNullOrEmpty(agendaMinutesCommentsId))
            {
                long id = 0;
                long.TryParse(agendaMinutesCommentsId, out id);
                if(id > 0)
                {
                    string xfdfData = objIAnnotationService.GetAnnote(id);
                    return new HttpResponseMessage() { Content = new StringContent(xfdfData) };
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }

            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
        }

        // Handle POST request sent to '/api/annotation'
        // POST api/<controller>
        //public async System.Threading.Tasks.Task Post()
        //{
        //    string filePath = GetFilePath(this.Request);
        //    string xfdfData = await Request.Content.ReadAsStringAsync();
        //    File.WriteAllText(filePath, xfdfData);
        //}
        public async System.Threading.Tasks.Task Post()
        {
            long meetingId = 0;
            long meetingParticipantId = 0;
            long draftCirculationID_ = 0;
            var docType = "";
            var pairs = this.Request.GetQueryNameValuePairs();
            foreach (KeyValuePair<string, string> kvp in pairs)
            {
                switch (kvp.Key)
                {
                    case "MeetingId":
                        long.TryParse(kvp.Value, out meetingId);
                        break;
                    case "DocType":
                        docType = kvp.Value;
                        break;
                    case "MeetingParticipantId":
                        long.TryParse(kvp.Value, out meetingParticipantId);
                        break;
                    case "DraftCirculationID_":
                        long.TryParse(kvp.Value, out draftCirculationID_);
                        break;
                    default:
                        break;
                }
            }

            var model = new AgendaMinutesCommentsVM()
            {
                MeetingId = meetingId,
                MeetingParticipantId = meetingParticipantId,
                DocType = docType,
                DraftCirculationID_ = draftCirculationID_
            };

            string xfdfData = await Request.Content.ReadAsStringAsync();
            model.CommentData = xfdfData;

            if(model.MeetingId > 0)
            {
                int userId = System.Convert.ToInt32(Common.AuthenticationHelper.UserID);
                objIAnnotationService.SaveAnnotation(model, userId);
            }
        }

        private string GetFilePath(HttpRequestMessage request)
        {
            string id = GetId(request);
            string fileName = (id != null) ? id : "default";
            return System.Web.HttpContext.Current.Server.MapPath(string.Format("~\\{0}.xfdf", fileName));
        }

        private string GetId(HttpRequestMessage request)
        {
            var pairs = Request.GetQueryNameValuePairs();
            string agendaMinutesCommentsId = null;
            foreach (KeyValuePair<string, string> kvp in pairs)
            {
                if (kvp.Key == "id")
                {
                    agendaMinutesCommentsId = kvp.Value;
                }
            }
            return agendaMinutesCommentsId;
        }
    }
}