﻿using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    public class MainMeetingAPIController : ApiController
    {
        IMainMeeting obj;
        public MainMeetingAPIController(IMainMeeting obj)
        {
            this.obj = obj;
        }
        [Route("api/MainMeeting/GetParticipantByMeeting")]
        [HttpGet]
        public IEnumerable<ChairpersonElection> GetParticipantByMeeting(long MeetingId)
        {
            return obj.GetParticipant(MeetingId);
        }

        [Route("api/MainMeeting/GetParticipantforNotes")]
        [HttpGet]
        public IEnumerable<MeetingParticipants> GetParticipantforNotes(long MeetingId)
        {
            return obj.GetParticipantforNotes(MeetingId);
        }

        [Route("api/MainMeeting/GetAuthorizedDirector")]
        [HttpGet]
        public IEnumerable<MeetingParticipants> GetAuthorizedDirector(long MeetingId)
        {
            return obj.GetAuthorisedDirector(MeetingId);
        }

        [Route("api/MainMeeting/SaveAuthorisedDirector")]
        [HttpPost]
        public HttpResponseMessage SaveAuthorisedDirector(MeetingCTC obctc)
        {
            var response = (dynamic)null;
            obctc = obj.saveCTCAuthorizedDirector(obctc);
            if (obctc != null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, obctc);
            }
            else
            {
               response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;
        }
    }
}
