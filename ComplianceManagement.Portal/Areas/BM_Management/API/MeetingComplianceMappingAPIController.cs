﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/MeetingComplianceMappingAPI")]
    public class MeetingComplianceMappingAPIController : ApiController
    {
        IMeetingComplianceMapping obj;
        public MeetingComplianceMappingAPIController(IMeetingComplianceMapping obj)
        {
            this.obj = obj;
        }

        [Route("GetAllMeetingType")]
        [HttpGet]
        public DataSourceResult GetAllAgenda([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var role = AuthenticationHelper.Role;

            var gridData = obj.GetMeetingType();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetMeetingType")]
        [HttpGet]
        public IEnumerable<VM_MeetingCamplianceMapping> GetDropDownforCustomer()
        {
            return obj.GetMeetingType();
        }


        [Route("GetCompliance")]
        [HttpGet]
        public DataSourceResult GetCompliance([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int MeetingTypeId,int EntityTypeId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            List<VMCompliences> gridData = new List<VMCompliences>();
            gridData = obj.BindCompliancesNew(MeetingTypeId, EntityTypeId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
