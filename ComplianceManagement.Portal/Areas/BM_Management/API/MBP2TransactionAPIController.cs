﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/MBP2Transaction")]
    public class MBP2TransactionAPIController : ApiController
    {
        IMBP2TransactionService objIMBP2TransactionService;

        public MBP2TransactionAPIController(IMBP2TransactionService objMBP2TransactionService)
        {
            objIMBP2TransactionService = objMBP2TransactionService;
        }

        [Route("GetMBP2Transactions")]
        [HttpGet]
        public DataSourceResult GetMBP2Transactions([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int entityId, string view)
        {
            var gridData = objIMBP2TransactionService.GetTransactions(entityId, view);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetMBP2Nature")]
        [HttpGet]
        public IEnumerable<MBP2TransactionNatureVM> GetMBP2Nature(string showAll)
        {
            var result = objIMBP2TransactionService.GetNatureOfTransactions();
            if(result == null)
            {
                result = new List<MBP2TransactionNatureVM>();
            }
            if (showAll == "true")
            {
                result.Insert(0, new MBP2TransactionNatureVM() { Id = 0, NatureOfTransaction = "All" });
            }
            return result;
        }
        [Route("GetMBP2NatureType")]
        [HttpGet]
        public IEnumerable<MBP2TransactionNatureVM> GetMBP2NatureType(string showAll)
        {
            var result = objIMBP2TransactionService.GetNatureType();
            if (result == null)
            {
                result = new List<MBP2TransactionNatureVM>();
            }
            if (showAll == "true")
            {
                result.Insert(0, new MBP2TransactionNatureVM() { Id = 0, NatureOfTransaction = "All" });
            }
            return result;
        }
    }
}
