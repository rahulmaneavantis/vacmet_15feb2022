﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/AdditionalAct")]
    public class AdditionalActAPIController : ApiController
    {
        IAdditionalActMaster objact;

        public AdditionalActAPIController(IAdditionalActMaster objact1)
        {
            objact = objact1;
        }
        [Route("GetadditionalAct")]
        [HttpGet]
        public DataSourceResult GetadditionalAct([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            
            var gridData = objact.GetAdditionalAct();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("DeleteAdditionalAct")]
        [HttpDelete]
        public HttpResponseMessage DeleteAdditionalAct(long Id)
        {
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = null;
            if (Id > 0)
            {
                bool result = objact.DeleteAdditionalAct(Id, UserId);
                if (result)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return response;
        }
    }
}