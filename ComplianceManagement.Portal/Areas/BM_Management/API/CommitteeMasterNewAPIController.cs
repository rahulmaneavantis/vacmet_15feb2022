﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CommitteeMasterNewAPIController")]
    public class CommitteeMasterNewAPIController : ApiController
    {
        IDirectorMaster objDirectorMaster;
        ICommitteeMaster objICommitteeMaster;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public CommitteeMasterNewAPIController(IDirectorMaster obj, ICommitteeMaster objICom)
        {
            objDirectorMaster = obj;
            objICommitteeMaster = objICom;
        }

        #region Add/Update/Delete committee Member
        [Route("AddCommitee")]
        [HttpPost]
        public IHttpActionResult AddCommitee(CommitteeMasterVM _obj)
        {
            string Message = String.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                Message = objDirectorMaster.AddMember(_obj, userID, CustomerId);
         
            }
            else
            {
                response= Request.CreateResponse(HttpStatusCode.BadRequest,"Validation Fail");
            }
            return Json(Message);
        }

        [Route("UpdateCommittee")]
        [HttpPut]
        public IHttpActionResult UpdateCommittee(CommitteeMasterVM _obj)
        {
            string Message = String.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if(_obj.UserId > 0)
                {
                    _obj.Director_Id = objDirectorMaster.GetDirectorIdByUserId((int)_obj.UserId, customerID);
                }
                else
                {
                    _obj.UserId = (int)objDirectorMaster.GetUserId(_obj.Director_Id);
                }
                DetailsOfCommiteePosition d = new DetailsOfCommiteePosition()
                {
                    Id = _obj.Id,
                    Entity_Id_Committee = _obj.Entity_Id,
                    Committee_Id = _obj.Committee_Id,
                    Director_ID = _obj.Director_Id,
                    UserId = _obj.UserId,
                    Designation_Id = _obj.PositionId,
                    //Committee_Type = _obj.Committee_Type,
                    AppointmentFrom = _obj.AppointmentFrom,
                    AppointmentTo = _obj.AppointmentTo,

                    UpdatedBy = userID,
                    UpdatedOn = DateTime.Now
                };

                var hasError = false;
                var data = objDirectorMaster.CheckChairpersonExists(d, _obj.Director_Id, out hasError);

                if (hasError)
                {
                    Message = data;
                }
                else if (string.IsNullOrEmpty(data))
                {
                    hasError = false;
                    var result = objDirectorMaster.CreateDetailsOfCommitteePosition(d, _obj.Director_Id, out hasError);

                    if (hasError)
                    {
                        Message = data;
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Message);
                    }
                    else if (result)
                    {
                        Message = "Update Successfully";
                        response = Request.CreateErrorResponse(HttpStatusCode.OK, Message);
                    }
                    else
                    {
                        Message = data;
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
                    }
                }
                else
                {
                    Message = data;
                    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Message);
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(Message);

        }

        [Route("DeleteCommitteeDtls")]
        [HttpDelete]
        public IHttpActionResult DeleteCommitteeDtls(CommitteeMasterVM _obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string Message = string.Empty;
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                bool hasError = false;
                var result = objDirectorMaster.DeleteDetailsOfCommitteePosition(_obj.Id, userID, out hasError);

                if (hasError)
                {
                    Message = "Server Error Occure";
                }
                else if (result)
                {
                    Message = "true";
                }
                else
                {
                    Message = "Something wents wrong";
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(Message);

        }
        #endregion

        #region Get Compostion of Board/AGM/EGM/Committee(Statutory/Internal)
        //New Method for Get Compostion of Board/AGM/EGM/committees.
        //e.g.GetCommitteeMembersNew?EntityId=1&CommitteeId=10&shoAllCommittees=false
        //Get All Committees.
        //e.g.GetCommitteeMembersNew?EntityId=1&CommitteeId=0&shoAllCommittees=true
        [Route("GetCommitteeMembersNew")]
        [HttpGet]
        public DataSourceResult GetCommitteeMembersNew([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId, int CommitteeId, bool shoAllCommittees)
        {
            var gridData = objDirectorMaster.GetCommitteeMembers(EntityId, CommitteeId, shoAllCommittees);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        //New Method for get compostion for Board/AGM/EGM & all committees
        [Route("GetCommitteeComposition")]
        [HttpGet]
        public IEnumerable<CommitteeMasterVM> GetCommitteeComposition(int EntityId, int CommitteeId, bool? AddNew)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objDirectorMaster.GetMemberListForAddNewById(EntityId, CommitteeId, customerId, AddNew);
        }
        #endregion

        #region Check Composition is valid or not by committeeId
        public IHttpActionResult CheckIsValidCommittee(int EntityId, int CommitteeId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            return Json(objICommitteeMaster.CheckIsValidCommittee(CommitteeId, EntityId, customerId));
        }
        #endregion

        #region Committee Matrix
        [Route("GetCommitteeMatrix")]
        [HttpGet]
        public List<CommitteeMatrixVM> GetCommitteeMatrix(int entityId)
        {
            return objICommitteeMaster.GetCommitteeMatrix(entityId);
        }
        #endregion

        [Route("GetCommitteeMasterNew")]
        [HttpGet]
        public DataSourceResult GetCommitteeMasterNew([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId, int CommitteeId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objDirectorMaster.GetCommitteeMasterNew(customerId, EntityId, CommitteeId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        #region Committee Director Details
        [Route("GetCommitteeDtls")]
        [HttpGet]
        public DataSourceResult GetCommitteeDtls([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int DirectorId)
        {

            var gridData = objDirectorMaster.GetCommitteeMasterdtls(CustomerId, DirectorId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateCommitteeDtls")]
        [HttpPost]
        public HttpResponseMessage CreateCommitteeDtls(CommitteeMasterVM _objcommity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcommity = objDirectorMaster.CreateCommitteeDtls(CustomerId, _objcommity);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }

            return response;
        }

        [Route("UpdateCommitteeDtls")]
        [HttpPut]
        public HttpResponseMessage UpdateCommitteeDtls(CommitteeMasterVM _objcommity, int DirectorId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcommity = objDirectorMaster.UpdateCommitteeDtls(CustomerId, _objcommity);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }

            return response;
        }
        #endregion
        //GetDirectorPosition

        [Route("GetDirectorPosition")]
        [HttpGet]
        public IEnumerable<BM_DirectorPosition> GetDirectorPosition()
        {
            return objDirectorMaster.GetDirectorPosition();
        }
    }

}
