﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ShareholdingMasterAPI")]
    public class ShareholdingMasterAPIController : ApiController
    {
        IShareholdingMaster obj;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public ShareholdingMasterAPIController(IShareholdingMaster obj)
        {
            this.obj = obj;
        }

        //ShareholdingMasterAPI/GetShareHoldingdtlsbyID
        [Route("GetShareHoldingdtlsbyID")]
        [HttpGet]
        public IHttpActionResult GetShareHoldingdtlsbyID(int EntityId)
        {
            var objgetEntityDtls = (dynamic)null;
            if (EntityId > 0)
            {
                objgetEntityDtls = obj.getEntitIdbyId(EntityId, CustomerId);
            }
            return Json(objgetEntityDtls);
        }

     
        [Route("GetShareholdingMemberForRegister")]
        [HttpGet]
        public IEnumerable<VMShareholding> GetShareholdingMemberForRegister(int EntityId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetShareholdingMember(EntityId, CustomerId);
            return gridData;
        }


        [Route("GetShareholdingDtls")]
        [HttpGet]
        public DataSourceResult GetShareholdingDtls([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetShareholdingdata(EntityId, CustomerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetShareholdingDetails")]
        [HttpGet]
        public DataSourceResult GetShareholdingDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int SharesHoldingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetShareholdingDetails(SharesHoldingId, CustomerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetPartnerSharesList")]
        [HttpGet]
        public DataSourceResult GetPartnerSharesList([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int EntityID)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetPartnerDetails(CustomerId,EntityID);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateShareholding")]
        [HttpPost]
        public IHttpActionResult CreateShareholding(ShareHoldings _objshareholder)
        {
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objshareholder = obj.AddShareholdingdata(_objshareholder, CustomerId,UserId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("UpdateShareholding")]
        [HttpPost]
        public IHttpActionResult UpdateShareholding(VMShareholding _objshareholding)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = obj.UpdateShareholdingdtls(_objshareholding, CustomerId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("GetShareDetailsForListedCompay")]
        [HttpGet]
        public DataSourceResult GetShareDetailsForListedCompay([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int EntityId)
        {
           
            var gridData = obj.GetShareDetailsForListedCompay(EntityId,CustomerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetShareDetailsForDematform")]
        [HttpGet]
        public DataSourceResult GetShareDetailsForDematform([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int EntityId)
        {
          
            var gridData = obj.GetShareDetailsForDemateForm(EntityId, CustomerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetDropDownClassesOfShares")]
        [HttpGet]
        public IEnumerable<ClassesOfShare> GetDropDownClassesOfShares(int EntityId)
        {
            var item = obj.GetDropDownClassesOfShares(EntityId);
            if (item == null)
            {
                item = new List<ClassesOfShare>();
            }
            return item;
        }


        [Route("DeleteShareHolding")]
        [HttpDelete]
        public HttpResponseMessage DeleteShareHolding(int Id)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                bool result = obj.DeleteShareHolding(Id, CustomerId, UserId);
            }

            return response;
        }

        [Route("DeleteShareHoldingDetails")]
        [HttpDelete]
        public HttpResponseMessage DeleteShareHoldingDetails(int Id)
        {
            string result = string.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                result = obj.DeleteShareHoldingDetails(Id);
            }
            response.Content = new StringContent(result);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return response;
        }
    }
}
