﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/PageAuthenticationAPI")]
    public class page_AuthenticationAPIController : ApiController
    {
        IPageAuthentication obj;
        public page_AuthenticationAPIController(IPageAuthentication obj)
        {
            this.obj = obj;
        }
        //,int UserId,int EntityId
        //[Route("GetPageAuthentication")]
        //[HttpGet]
        //public DataSourceResult GetPageAuthentication([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        //{

        //    int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //    var gridData = obj.GetPageAuthentication(6243, 5, 1);
        //    var result = new DataSourceResult()
        //    {
        //        Data = gridData,
        //        Total = gridData.Count
        //    };
        //    return result;
        //}


        [Route("GetEntityuserWise")]
        [HttpGet]
        public IEnumerable<EntityMasterVM> GetEntityuserWise(int UserId)
        {
            return obj.GetEntityuserWise(UserId);
        }





        //[ResponseType(typeof(IEnumerable<VM_pageAuthentication>))]
        //[System.Web.Http.Route("api/PageAuthenticationAPI/GetPageAuthentication")]
        //public HttpResponseMessage GetPageAuthentication()
        //{
        //    var gridData = obj.GetPageAuthentication(6243, 5, 1);
        //   // var result = entity.Employees.ToList();
        //    return GetResultResponse(gridData);
        //}
        /// <summary>  
        /// Get Response for Each result  
        /// </summary>  
        /// <param name="Result"></param>  
        /// <returns></returns>  
        public HttpResponseMessage GetResultResponse(object Result)
        {
            HttpResponseMessage response = null;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, Result);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, Result);
            }
            return response;
        }

        [Route("GetPageAuthentication")]
        [System.Web.Http.AcceptVerbs("GET")]
        public HttpResponseMessage GetPageAuthentication()
        {
            try
            {

                var gridData = obj.GetPageAuthentication(6243, 5, 1,"");
                return Request.CreateResponse(HttpStatusCode.OK, gridData);
            }
            catch (Exception ex)
            {
               
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


       
    }
}
