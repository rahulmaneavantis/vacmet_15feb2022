﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CountryStateCityMasterAPI")]
    public class CommitteeMasterAPIController : ApiController
    {
        IDirectorMaster objDirectorMaster;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public CommitteeMasterAPIController(IDirectorMaster obj)
        {
            objDirectorMaster = obj;
        }
       
        [Route("AddMember")]
        [HttpPost]
        public IHttpActionResult AddMember(CommitteeList_ForEntity _obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                DetailsOfCommiteePosition d = new DetailsOfCommiteePosition()
                {
                    Id =0,
                    Entity_Id_Committee = _obj.Entity_Id,
                    Committee_Id = _obj.Committee_Id,
                    Director_ID = _obj.Director_ID,
                    Designation_Id = _obj.Designation_Id,
                    //Committee_Type = _obj.Committee_Type,

                };
                var hasError = false;
                var result = objDirectorMaster.CreateDetailsOfCommitteePosition(d, _obj.Director_ID, out hasError);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("UpdateMember")]
        [HttpPut]
        public IHttpActionResult UpdateMember(CommitteeList_ForEntity _obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                DetailsOfCommiteePosition d = new DetailsOfCommiteePosition()
                {
                    Id = _obj.Id,
                    Entity_Id_Committee = _obj.Entity_Id,
                    Committee_Id = _obj.Committee_Id,
                    Director_ID = _obj.Director_ID,
                    Designation_Id = _obj.Designation_Id,
                    //Committee_Type = _obj.Committee_Type,

                };
                var hasError = false;
                var result = objDirectorMaster.CreateDetailsOfCommitteePosition(d, _obj.Director_ID, out hasError);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("DeleteMember")]
        [HttpPut]
        public IHttpActionResult DeleteMember(CommitteeList_ForEntity _obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }
    }
}
