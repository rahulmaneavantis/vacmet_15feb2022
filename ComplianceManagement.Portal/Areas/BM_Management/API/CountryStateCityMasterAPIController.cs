﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CountryStateCityMasterAPI")]
    public class CountryStateCityMasterAPIController : ApiController
    {
        ICountryStateCityMaster objcountryStateCityMaster;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public CountryStateCityMasterAPIController(ICountryStateCityMaster obj)
        {
            objcountryStateCityMaster = obj;
        }


        [Route("GetAllCountries")]
        [HttpGet]
        public DataSourceResult GetAllCountries([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objcountryStateCityMaster.GetAllCountryDtls();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetAllState")]
        [HttpGet]
        public DataSourceResult GetAllState([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int countryId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objcountryStateCityMaster.GetAllstateDtls(countryId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetAllCity")]
        [HttpGet]
        public DataSourceResult GetAllCity([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int stateId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objcountryStateCityMaster.GetAllcityDtls(stateId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

       
        [Route("CreateCountries")]
        [HttpPost]
        public IHttpActionResult CreateCountries(VMCountry _objcountry)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcountry = objcountryStateCityMaster.AddCountry(_objcountry);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok(_objcountry);

        }

        [Route("UpdateCountries")]
        [HttpPut]
        public IHttpActionResult UpdateCountries(VMCountry _objcountry)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcountry = objcountryStateCityMaster.EditCountry(_objcountry);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("DeleteCountries")]
        [HttpPut]
        public IHttpActionResult DeleteCountries(VMCountry _objcountry)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = objcountryStateCityMaster.DeleteCountry(_objcountry);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("CreateState")]
        [HttpPost]
        public IHttpActionResult CreateState(VMState _objstate)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objstate = objcountryStateCityMaster.AddState(_objstate);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("UpdateState")]
        [HttpPut]
        public IHttpActionResult UpdateState(VMState _objstate)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objstate = objcountryStateCityMaster.EditState(_objstate);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("DeleteState")]
        [HttpPut]
        public IHttpActionResult DeleteState(VMState _objstate)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = objcountryStateCityMaster.DeleteState(_objstate);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("CreateCity")]
        [HttpPost]
        public IHttpActionResult CreateCity(VMCity _objcity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcity = objcountryStateCityMaster.AddCity(_objcity);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("UpdateCity")]
        [HttpPut]
        public IHttpActionResult UpdateCity(VMCity _objcity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcity = objcountryStateCityMaster.EditCity(_objcity);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("DeleteCity")]
        [HttpPut]
        public IHttpActionResult DeleteCity(VMCity _objcity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = objcountryStateCityMaster.DeleteCity(_objcity);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }
    }
}
