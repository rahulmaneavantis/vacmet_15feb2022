﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/WhatsNew")]
    public class WhatsNewAPIController : ApiController
    {
        IWhatsnewservice objwn;
        public WhatsNewAPIController(IWhatsnewservice objwhatsnew)
        {
            objwn = objwhatsnew;
        }
        [Route("GetWhatsnewData")]
        [HttpGet]
        public DataSourceResult GetWhatsnewData([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            var gridData = objwn.GetWhatsnewdata();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
        [Route("DeleteWhatsNew")]
        [HttpDelete]
        public HttpResponseMessage DeleteWhatsNew(long Id)
            {
                int UserId = AuthenticationHelper.UserID;
                HttpResponseMessage response = null;
                if (Id > 0)
                {
                    bool result = objwn.DeleteWhatsNew(Id, UserId);
                    if (result)
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                return response;
            }
        }
    }
