﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ConfigurationMasterAPI")]
    public class ConfigurationAPIController : ApiController
    {
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        IConfiguration obj;

        public ConfigurationAPIController(IConfiguration obj)
        {
            this.obj = obj;
        }

        [Route("getMeetingConfiguration")]
        [HttpGet]
        public DataSourceResult getMeetingConfiguration([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int EntityId, int custID)
        {
            string flag = "M";
            var gridData = obj.getMeetingConfiguration(EntityId, custID);

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };

            return result;
        }
        
        [Route("getCircularConfiguration")]
        [HttpGet]
        public DataSourceResult getCircularConfiguration([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,string flag)
        {
            var gridData = obj.getCircularConfiguration();

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
        
        [Route("GetMeetingType")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeetingTypeForDropDown(int EntityId)
        {
            var result = obj.GetAllMeetingType(EntityId);
            return result;
        }

        [Route("GetFYListByEntityID")]
        [HttpGet]
        public List<YearMasterVM> GetAll_FYByEntityID(int EntityID)
        {
            var result = obj.GetAll_FYByEntityID(EntityID);
            return result;
        }

        [Route("GetFYListByEntityIDForHistorical")]
        [HttpGet]
        public List<YearMasterVM> GetAll_FYByEntityIDForHistorical(int EntityID)
        {
            var result = obj.GetAll_FYByEntityIDForHistorical(EntityID);
            return result;
        }

    }
}
