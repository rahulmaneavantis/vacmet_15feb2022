﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/UserMasterAPI")]
    public class UserMasterAPIController : ApiController
    {
        IUser objIUser;

        public UserMasterAPIController(IUser obj)
        {
            objIUser = obj;
        }

        [Route("GeUsers")]
        [HttpGet]
        public DataSourceResult GeUsers([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIUser.GetUserMaster(customerId);
            //var gridData = objIUser.GetUsers(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GeUsersNew")]
        [HttpGet]
        public DataSourceResult GeUsersNew([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIUser.GetUsersNew(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GeUsersForImplementaion")]
        [HttpGet]
        public DataSourceResult GeUsersForImplementaion([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int customerId)
        {
            var gridData = objIUser.GetUsers(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetUsersAssignment")]
        [HttpGet]
        public IEnumerable<UserforDropdown> GetUsersAssignment()
        {
            List<UserforDropdown> objusers = new List<UserforDropdown>();
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            objusers = objIUser.UserAssignment(customerId);
            return objusers;
        }

        

        [Route("GetUsersDropdown")]
        [HttpGet]
        public IEnumerable<UserVM> GetUsersDropdown()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var item = objIUser.GetUsers(customerId);
            return item;
        }
        
        [Route("GetComplianceRolesDropdown")]
        [HttpGet]
        public IEnumerable<RoleVM> GetComplianceRoleDropdown()
        {
            //var item = objIUser.GetComplianceRoles();
            var item = objIUser.GetComplianceRoles(); 
            if (item == null)
            {
                item = new List<RoleVM>();
            }
            return item;
        }
        [Route("GetSecretarialRolesDropdown")]
        [HttpGet]
        public IEnumerable<RoleVM> GetSecretarialCompliancesDropdown()
        {
            List<RoleVM> item = new List<RoleVM>();

            if (AuthenticationHelper.Role == "DADMN" || AuthenticationHelper.Role == "CEXCT" || AuthenticationHelper.Role == "CSMGR")
                item = objIUser.GetSecretarialRoles_ICSI();
            else
                item = objIUser.GetSecretarialRoles_Limited();

            if (item == null)
            {
                item = new List<RoleVM>();
            }
            return item;
        }

        [Route("GetHRRolesDropdown")]
        [HttpGet]
        public IEnumerable<RoleVM> GetHRRoleDropdown()
        {
            var item = objIUser.GetHRRoles();
            if (item == null)
            {
                item = new List<RoleVM>();
            }
            return item;
        }

        [Route("CreateUser")]
        [HttpPost]
        public HttpResponseMessage CreateUser(UserVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = UserID;
                obj.CustomerID = CustomerID;
                obj.CreatedByText = AuthenticationHelper.User;

                var hasError = false;

                string passwordText = Util.CreateRandomPassword(10);
                obj.Password = passwordText;

                string Mailbody = getEmailMessage(obj, obj.Password, out hasError);
                
                if(hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    obj.EmailBody = Mailbody;
                    //obj.Password = "";
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    var result = objIUser.Create(obj, SenderEmailAddress);

                    if (result.Message.Success)
                    {                        
                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Account Created", Mailbody);
                    }

                    response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "validation failed");
            }
            return response;
        }

        //Implementation login create user
        [Route("CreateUserByImpl")]
        [HttpPost]
        public HttpResponseMessage CreateUserFromImplementation(UserVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                //int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = UserID;
                //obj.CustomerID = CustomerID;
                obj.CreatedByText = AuthenticationHelper.User;

                string passwordText = Util.CreateRandomPassword(10);
                obj.Password = passwordText;

                var hasError = false;

                string Mailbody = getEmailMessage(obj, obj.Password, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    obj.EmailBody = Mailbody;
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    var result = objIUser.Create(obj, SenderEmailAddress);

                    if (result.Message.Success)
                    {
                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Account Created.", Mailbody);
                    }

                    response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "validation failed");
            }
            return response;
        }

        [Route("UpdateUser")]
        [HttpPut]
        public HttpResponseMessage UpdateUser(UserVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                #region change by Ruchi on 15th of July 2020
                int UserID = Convert.ToInt32(obj.UserID);
                #endregion
                //int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                //int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.CreatedByText = AuthenticationHelper.User;

                var hasError = false;

                string Mailbody = SendNotificationEmailChanged(obj, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    string Old_EmailId = "";
                    var result = objIUser.Update(obj, SenderEmailAddress,out Old_EmailId);

                    if (result.Message.Success)
                    {
                        if (Old_EmailId != result.Email)
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Email Changed", Mailbody);
                    }
                    response = Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "validation failed");
            }
            return response;
        }

        

        [Route("GetUsersList")]
        [HttpGet]
        public IEnumerable<UserforDropdown> GetUsersList(int customerId)
        {
            var objusers = new List<UserforDropdown>();
            objusers = objIUser.UserAssignment(customerId);
            return objusers;
        }

        private string getEmailMessage(UserVM user, string passwordText, out bool hasError)
        {
            try
            {
                hasError = false;
                int customerID = -1;
                string ReplyEmailAddressName = "";
                int customerId = Convert.ToInt32(AuthenticationHelper.UserID);

                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalurl = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalurl = Urloutput.URL;
                }
                else
                {
                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                  .Replace("@Username", user.Email)
                                  .Replace("@User", username)
                                  .Replace("@PortalURL", Convert.ToString(portalurl))
                                  .Replace("@Password", passwordText)
                                  .Replace("@From", ReplyEmailAddressName)
                                  .Replace("@URL", Convert.ToString(portalurl));

                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                //cvUserPopup.ErrorMessage = "Something went wrong, Please try again";
                return null;
            }
        }

        private string SendNotificationEmailChanged(UserVM user, out bool hasError)
        {
            hasError = false;
            try
            {
                user.Message = new Response();
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    //change by Ruchi on 15th july 2020
                    customerID = UserManagement.GetByID(Convert.ToInt32(user.UserID)).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalurl = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalurl = Urloutput.URL;
                }
                else
                {
                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                      .Replace("@Username", user.Email)
                                      .Replace("@User", username)
                                      .Replace("@PortalURL", Convert.ToString(portalurl))
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(portalurl));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                user.Message.Error = true;
                user.Message.Message = "Something went wrong, Please try again";
                
            }
            return null;
        }

        [Route("GetRoleofsecretarial")]
        [HttpGet]
        public IEnumerable<RoleVM> GetRoleofsecretarial()
        {
            var objrole = new List<RoleVM>();
            objrole = objIUser.GetSecRoles();
            return objrole;
        }

        [Route("GetUserRolewise")]
        [HttpGet]
        public IEnumerable<UserVM> GetUserRolewise(string Role, int CustomerId)
        {
            var objrole = new List<UserVM>();
            objrole = objIUser.GetUserRolewise(Role, CustomerId);
            return objrole;
        }
    }
}