﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/BankDetails")]
    public class BankAccDetailsAPIController : ApiController
    {
        IBankAccDetailsMaster objadditional;
        public BankAccDetailsAPIController(IBankAccDetailsMaster obj)
        {
            objadditional = obj;
        }
        [Route("GetBankDetails")]
        [HttpGet]
        public DataSourceResult GetBankDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int entityId)
        {

            var gridData = objadditional.GetBankDetails(entityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
        [Route("GetDropDownforAccType")]
        [HttpGet]
        public IEnumerable<BankTypeList_VM> GetDropDownforAccType()
        {
            return objadditional.GetBankType();
        }
        [Route("DeleteAdditionalBankDetails")]
        [HttpDelete]
        public HttpResponseMessage DeleteAdditionalBankDetails(int Id)
        {
            int UserId = AuthenticationHelper.UserID;
            HttpResponseMessage response = null;
            if (Id > 0)
            {
                bool result = objadditional.DeleteAdditionalBank(Id, UserId);
                if (result)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return response;
        }
    }
}