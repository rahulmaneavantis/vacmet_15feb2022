﻿using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.VendorAuditManagement.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.VendorAuditManagement.Controllers
{
    public class VendorAuditController : Controller
    {
        // GET: VendorAuditManagement/VendorAudit

        public ActionResult AuditorCloseReport()
        {
            return View();
        }
        public ActionResult ComplianceReport()
        {
            return View();
        }

        public ActionResult ManagementDashBoard()
        {
            return View();
        }
        public ActionResult AuditorScheduleCompliances()
        {
            return PartialView();
        }

        public ActionResult ContractorCompliancesdocumentList()
        {
            return View();
        }

        public ActionResult ContractorScheduleCompliances()
        {
            return PartialView();
        }

        public ActionResult ContractorCompliancePerformer()
        {
            return View();
        }
        public ActionResult ContractorWorkspace()
        {
            return View();
        }
        public ActionResult ContractorDashboard()
        {
            return View();
        }
        public ActionResult AuditAssignment()
        {
            return View();
        }
        public ActionResult ComplianceScheduling()
        {
            return View();
        }
        public ActionResult ComplianceAssingment()
        {
            return View();
        }
        public ActionResult CityMaster()
        {
            return View();
        }
        public ActionResult ComplianceMasterNew()
        {
            return View();
        }

        public ActionResult ContractMaster()
        {
            return View();
        }
        public ActionResult ContractorType()
        {
            return View();
        }

        public ActionResult LicType()
        {
            return View();
        }

        public ActionResult ProjectMaster()
        {
            return View();
        }
        public ActionResult UserMaster()
        {
            return View();
        }
        public ActionResult Entities()
        {
            VendorEntity objModel = new VendorEntity();
            objModel.CustomerId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
            return View(objModel);
        }

        public ActionResult VenoderComplianceDataPoints()
        {
            return View();
        }

        public ActionResult VendorDynamic()
        {
            return View();
        }

        public ActionResult VendorAuditStatusReport()
        {
            return View();
        }
        public ActionResult EntityBranch(string CustID, string ParentId, string CurrId)
        {
            int custID = Convert.ToInt32(CustID);
            string CustomerName = string.Empty;

            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["KendoPathApp"].ToString();
            string requestUrl = baseAddress + "/VendorAudit/GetCustomerName?custID=" + custID;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string responseData = WebAPIUtility.Invoke("GET", requestUrl, "");
            if (!string.IsNullOrEmpty(responseData))
            {
                CustomerName = responseData;
            }


            //var ApiBaseURL = System.Configuration.ConfigurationManager.AppSettings["KendoPathApp"];
            //HttpClient _httpClient = new HttpClient();
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //_httpClient.BaseAddress = new Uri(ApiBaseURL);
            //_httpClient.DefaultRequestHeaders.Accept.Clear();
            //_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //var response = _httpClient.GetAsync("/VendorAudit/GetCustomerName?custID=" + custID).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    var task = response.Content.ReadAsStringAsync();
            //    CustomerName = task.Result;
            //}
            VendorEntity objModel = new VendorEntity();
            objModel.LstBreadScrum = objModel.getBreadCrumDetails(CustID, ParentId);
            objModel.CustomerId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
            objModel.ParentId = Convert.ToInt64(ParentId);
            objModel.PreviousId = Convert.ToInt64(CurrId);
            objModel.CustomerName = CustomerName;
            return View(objModel);
        }
        //public ActionResult EntityBranch(string CustID, string ParentId, string CurrId)
        //{
        //    int custID = Convert.ToInt32(CustID);
        //    string CustomerName = string.Empty;
        //    var ApiBaseURL = System.Configuration.ConfigurationManager.AppSettings["KendoPathApp"];
        //    HttpClient _httpClient = new HttpClient();
        //    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        //    _httpClient.BaseAddress = new Uri(ApiBaseURL);
        //    _httpClient.DefaultRequestHeaders.Accept.Clear();
        //    _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    var response = _httpClient.GetAsync("/VendorAudit/GetCustomerName?custID=" + custID).Result;
        //    if (response.IsSuccessStatusCode)
        //    {
        //        var task = response.Content.ReadAsStringAsync();
        //        CustomerName = task.Result;
        //    }
        //    VendorEntity objModel = new VendorEntity();
        //    objModel.LstBreadScrum = objModel.getBreadCrumDetails(CustID, ParentId);
        //    objModel.CustomerId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
        //    objModel.ParentId = Convert.ToInt64(ParentId);
        //    objModel.PreviousId = Convert.ToInt64(CurrId);
        //    objModel.CustomerName = CustomerName;
        //    return View(objModel);
        //}

    }
}