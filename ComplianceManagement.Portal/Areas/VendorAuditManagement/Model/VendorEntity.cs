﻿using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.VendorAuditManagement.Model
{
    public class VendorEntity
    {
        private HttpClient _httpClient { set; get; }
        HttpResponseMessage response;
        public VendorEntity()
        {
            LstBreadScrum = new List<VendorEntityBreadScrum>();
        }


        public long CustomerId { get; set; }
        public string CustomerName { get; set; }

        public long ParentId { get; set; }

        public long PreviousId { get; set; }
        public long CustomerBranchId { get; set; }

        public string CustomerBranchName { get; set; }

        public List<VendorEntityBreadScrum> LstBreadScrum { get; set; }
        public List<VendorEntityBreadScrum> getBreadCrumDetails(string CustomerID, string ParentId)
        {
            List<VendorEntityBreadScrum> lst = new List<VendorEntityBreadScrum>();
            int CiD = Convert.ToInt32(CustomerID);
            int PiD = Convert.ToInt32(ParentId);

            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["KendoPathApp"].ToString();
            string requestUrl = baseAddress + "/VendorAudit/VendorCustomerBreadScrum?CustomerId=" + CiD + "&ParentId=" + PiD;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string responseData = WebAPIUtility.Invoke("GET", requestUrl, "");
            if (!string.IsNullOrEmpty(responseData))
            {
                lst = JsonConvert.DeserializeObject<List<VendorEntityBreadScrum>>(responseData);
            }

            //    var ApiBaseURL = System.Configuration.ConfigurationManager.AppSettings["KendoPathApp"];
            //_httpClient = new HttpClient();
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //_httpClient.BaseAddress = new Uri(ApiBaseURL);
            //_httpClient.DefaultRequestHeaders.Accept.Clear();
            //_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            //response = _httpClient.GetAsync("/VendorAudit/VendorCustomerBreadScrum?CustomerId=" + CiD + "&ParentId=" + PiD).Result;
            //lst = response.Content.ReadAsAsync<List<VendorEntityBreadScrum>>().Result;
            return lst;
        }
    
    //public List<VendorEntityBreadScrum> getBreadCrumDetails(string CustomerID, string ParentId)
    //    {
    //        List<VendorEntityBreadScrum> lst = new List<VendorEntityBreadScrum>();
    //        try {
               
    //            int CiD = Convert.ToInt32(CustomerID);
    //            int PiD = Convert.ToInt32(ParentId);
    //            var ApiBaseURL = System.Configuration.ConfigurationManager.AppSettings["KendoPathApp"];
    //            _httpClient = new HttpClient();
    //            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
    //            _httpClient.BaseAddress = new Uri(ApiBaseURL);
    //            _httpClient.DefaultRequestHeaders.Accept.Clear();
    //            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    //            response = _httpClient.GetAsync("/VendorAudit/VendorCustomerBreadScrum?CustomerId=" + CiD + "&ParentId=" + PiD).Result;
    //            lst = response.Content.ReadAsAsync<List<VendorEntityBreadScrum>>().Result;
    //            return lst;
    //        }
    //        catch (Exception ex)
    //        {
    //            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

    //            return lst;
    //        }       
    //    }
    }

    public class VendorEntityBreadScrum
    {
        public int CustomerID { get; set; }
        public int ID { get; set; }

        public string Name { get; set; }
        public Nullable<int> ParentId { get; set; }
    }
}