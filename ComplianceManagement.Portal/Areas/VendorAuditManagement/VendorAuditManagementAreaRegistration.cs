﻿using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.VendorAuditManagement
{
    public class VendorAuditManagementAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "VendorAuditManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "VendorAuditManagement_default",
                "VendorAuditManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}