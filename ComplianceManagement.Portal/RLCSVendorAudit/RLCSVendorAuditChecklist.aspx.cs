﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Collections.Generic;
using Saplin.Controls;
using System.Web;
using System.IO;
using System.Configuration;
using Ionic.Zip;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSVendorAuditChecklist : System.Web.UI.Page
    {
        public static List<long> NewStepID = new List<long>();
        public static int TotalChecklistCount = 0;
        public static int TotalChecklistCloseCount = -1;
        protected static List<RLCS_VendorAuditScheduleOn> RLCS_VendorAuditScheduleOn1 = null;
        protected static List<RLCS_VendorAuditAssignment> RLCS_VendorAuditAssignment1 = null;
        protected static List<RLCS_Vendor_RecentCheckListStatus_Result> RLCS_Vendor_RecentCheckListStatus1 = null;
        protected static List<RLCSVendorFileData1> RLCSVendorFileData12 = null;
        public bool IsOfflineStatus = false;
        public static long chkIDforExpry = 0;

        //protected static List<int> roles12 = null;
        //protected static List<Role> role1 = null;
        //protected static User LoggedUser = null;
        public class RLCSVendorFileData1
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public byte[] Data { get; set; }
            public string FilePath { get; set; }
            public string FileKey { get; set; }
            public string Version { get; set; }
            public Nullable<System.DateTime> CreatedOn { get; set; }
            public string FileSize { get; set; }
            public bool IsDeleted { get; set; }
            public Nullable<int> UpdatedBy { get; set; }
            public Nullable<System.DateTime> UpdatedOn { get; set; }
            public string EnType { get; set; }
            public Nullable<long> ScheduledOnID { get; set; }
            public long ChecklistID { get; set; }

            public Nullable<DateTime> ValidTill { get; set; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            IsOfflineStatus = Convert.ToBoolean(Request.QueryString["IsOffline"]);
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AID"]))
                    && !string.IsNullOrEmpty(Convert.ToString(Request.QueryString["SOID"])))
                {
                    ViewState["AID"] = Request.QueryString["AID"];
                    ViewState["SOID"] = Request.QueryString["SOID"];
                    ViewState["VendorID"] = Request.QueryString["VendorID"];
                    long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                    long SID = Convert.ToInt64(Request.QueryString["SOID"]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        RLCS_VendorAuditScheduleOn1 = (from row in entities.RLCS_VendorAuditScheduleOn
                                                       select row).ToList();

                        RLCS_VendorAuditAssignment1 = (from row in entities.RLCS_VendorAuditAssignment
                                                       //where row.UserID == AuthenticationHelper.UserID
                                                       select row).ToList();

                        //LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        //role1 = (from row in entities.Roles
                        //         select row).ToList();

                        RLCS_Vendor_RecentCheckListStatus1 = (from row in entities.RLCS_Vendor_RecentCheckListStatus(Convert.ToInt64(Request.QueryString["AID"]), -1, Convert.ToInt64(Request.QueryString["SOID"]))
                                                              select row).ToList();

                        var RLCSVendorFileData12fromFileData = (from row in entities.RLCSVendorFileDatas
                                                join row1 in entities.RLCSVendorFileDataMappings
                                               on row.ID equals row1.FileID
                                                where row.IsDeleted == false
                                                && row1.ScheduledOnID == SID

                                                select new RLCSVendorFileData1
                                                {
                                                    ID = row.ID,
                                                    Name = row.Name,
                                                    Data = row.Data,
                                                    FilePath = row.FilePath,
                                                    FileKey = row.FileKey,
                                                    Version = row.Version,
                                                    CreatedOn = row.CreatedOn,
                                                    FileSize = row.FileSize,
                                                    IsDeleted = row.IsDeleted,
                                                    UpdatedBy = row.UpdatedBy,
                                                    UpdatedOn = row.UpdatedOn,
                                                    EnType = row.EnType,
                                                    ScheduledOnID = row1.ScheduledOnID,
                                                    ChecklistID = row1.ChecklistID,
                                                    ValidTill = row.ValidTill
                                                }).ToList();

                        var RLCSVendorFileData12fromMasterData = (from row in entities.RLCSVendorFileDatas
                                                                join row2 in entities.RLCSMasterFileDatas
                                                                on row.ID equals row2.FileID
                                                                  where row.IsDeleted == false
                                                                    && row2.ContractorID == VendorID

                                                                  select new RLCSVendorFileData1
                                                                {
                                                                    ID = row.ID,
                                                                    Name = row.Name,
                                                                    Data = row.Data,
                                                                    FilePath = row.FilePath,
                                                                    FileKey = row.FileKey,
                                                                    Version = row.Version,
                                                                    CreatedOn = row.CreatedOn,
                                                                    FileSize = row.FileSize,
                                                                    IsDeleted = row.IsDeleted,
                                                                    UpdatedBy = row.UpdatedBy,
                                                                    UpdatedOn = row.UpdatedOn,
                                                                    EnType = row.EnType,
                                                                      ScheduledOnID=row2.ScheduleOnID,
                                                                    ChecklistID = row2.ChecklistID,
                                                                    ValidTill = row.ValidTill
                                                                }).ToList();

                        RLCSVendorFileData12 = RLCSVendorFileData12fromFileData.Union(RLCSVendorFileData12fromMasterData).ToList() as List<RLCSVendorFileData1>;
                        RLCSVendorFileData12 = RLCSVendorFileData12.GroupBy(p => p.ID)
                          .Select(g => g.First())
                          .ToList();
                        //var rolesfromsp = (entities.RLCS_Sp_GetAllAssignedRoles(AuthenticationHelper.UserID)).ToList();
                        //roles12 = rolesfromsp.Where(x => x != null).Cast<int>().ToList();

                    }

                    GetAuditName();
                    string Flag = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Flag"])))
                    {
                        Flag = Convert.ToString(Request.QueryString["Flag"]);
                    }
                    if (Flag.Equals("byOtp"))
                    {
                        btnBack.Visible = false;
                    }

                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "ID";

                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        ViewState["vrole"] = Convert.ToString(vrole);
                        if(vrole== "HVAUD")
                        {
                            divUploadDoc.Visible = false;
                        }
                        else
                        {
                            divUploadDoc.Visible = true;
                        }
                        buttonvisible(vrole);
                    }
                    StepBind(ddlNewStepCheck);

                    BindAuditCheckList();
                    bindActGroupList();
                    bindChecklistStatus();
                    BindGrid();
                }
            }
        }
        private void GetAuditName()
        {
            try
            {
                int AuditId = Convert.ToInt32(ViewState["AID"]);
                int ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var AuditDetail = (from row in entities.RLCS_VendorAuditScheduleOn
                    //                   join row1 in entities.CustomerBranches
                    //                   on row.CustomerBranchID equals row1.ID
                    //                   where row1.IsDeleted == false
                    //                   && row.ID == ScheduleOnID
                    //                   && row.AuditID == AuditId
                    //                   //row.IsActive == true
                    //                   select new {row, row1 }).FirstOrDefault();
                    var AuditDetail = (from row in RLCS_VendorAuditScheduleOn1
                                       join row1 in entities.CustomerBranches
                                       on row.CustomerBranchID equals row1.ID
                                       where row1.IsDeleted == false
                                       && row.ID == ScheduleOnID
                                       && row.AuditID == AuditId
                                       //row.IsActive == true
                                       select new { row, row1 }).FirstOrDefault();
                    if (AuditDetail != null)
                    {
                        lblAuditName.Text = AuditDetail.row1.Name +" > "+ Convert.ToString(Request.QueryString["VendorName"]) + " > "+ AuditDetail.row.ForMonth;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindChecklistStatus()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.getRLCSChecklistStatus();
                    ddlChecklistStatus.DataTextField = "Name";
                    ddlChecklistStatus.DataValueField = "ID";
                    ddlChecklistStatus.DataSource = data;
                    ddlChecklistStatus.DataBind();
                    ddlChecklistStatus.Items.Insert(0, new ListItem("Select Status ", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindActGroupList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.getRLCSActGrouplistData();
                    ddlAct.DataTextField = "AM_ActName";
                    ddlAct.DataValueField = "AM_ActID";
                    ddlAct.DataSource = data;
                    ddlAct.DataBind();
                    ddlAct.Items.Insert(0, new ListItem("Select Act ", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditCheckList();
        }
        protected void ddlChecklistStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditCheckList();
        }

        private void BindAuditCheckList()
        {
            try
            {
                grdChecklist.DataSource = null;
                grdChecklist.DataBind();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int AuditId = Convert.ToInt32(ViewState["AID"]);
                    int ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                    if (AuditId > 0)
                    {
                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                        var role = "";
                        if (LoggedUser != null)
                        {
                            //role = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                            role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        }
                            var lstChecklistDetails = entities.SP_RLCS_VendorCheckListStatusDetails(AuditId, ScheduleOnID, Convert.ToInt64(AuthenticationHelper.UserID), role).ToList();
                        lstChecklistDetails = lstChecklistDetails.GroupBy(test => test.ID).Select(grp => grp.First()).ToList();
                        if (lstChecklistDetails.Count > 0)
                        {
                            string actid = string.Empty;
                            string status = string.Empty;
                            if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                            {
                                if (Convert.ToString(ddlAct.SelectedValue) != "-1")
                                {
                                    actid = Convert.ToString(ddlAct.SelectedValue);
                                    lstChecklistDetails = lstChecklistDetails.Where(entry => entry.ActID == actid).ToList();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlChecklistStatus.SelectedValue))
                            {
                                if (Convert.ToString(ddlChecklistStatus.SelectedValue) != "-1")
                                {
                                    status = Convert.ToString(ddlChecklistStatus.SelectedItem.Text);
                                    lstChecklistDetails = lstChecklistDetails.Where(entry => entry.Status == status).ToList();
                                }
                            }
                            TotalChecklistCount = lstChecklistDetails.Count;
                            string statusid = "Closed";
                            TotalChecklistCloseCount = lstChecklistDetails.Where(entry => entry.Status == statusid).ToList().Count;
                            grdChecklist.DataSource = lstChecklistDetails;
                            grdChecklist.DataBind();
                            Session["TotalRows"] = lstChecklistDetails.Count;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public class dataEmail
        {
            public string ActName { get; set; }
            public string Stateid { get; set; }
            public string NatureOfCompliance { get; set; }
            public string TypeOfCompliance { get; set; }
        }
        protected void grdChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Save_Detail"))
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        if (vrole.Equals("HVAUD") && IsOfflineStatus == false)
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                            int UserID = AuthenticationHelper.UserID;
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            int index = Convert.ToInt32(commandArgs[1]);
                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            long branchID = Convert.ToInt64(commandArgs[2]);
                            string ddlStatus = setddlstatusvalue.Text;
                            string ChecklistStatus = setchecklistddlstatusvalue.Text;
                            TextBox txtRemark = grdChecklist.Rows[index].FindControl("txtRemark1") as TextBox;
                            TextBox txtRecommendation = grdChecklist.Rows[index].FindControl("txtRecommendation") as TextBox;

                            string Remark = string.Empty;
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                Remark = txtRemark.Text.TrimStart(',').TrimEnd(',').Trim();
                                //Remark = txtRemark.Text.TrimStart(',');
                                //Remark = Remark.TrimEnd(',');
                                //Remark = Remark.Trim();
                            }
                            string Recommendation = string.Empty;
                            if (!string.IsNullOrEmpty(txtRecommendation.Text))
                            {
                                Recommendation = txtRecommendation.Text.TrimStart(',').TrimEnd(',').Trim();
                                //Recommendation = txtRecommendation.Text.TrimStart(',');
                                //Recommendation = Recommendation.TrimEnd(',');
                                //Recommendation = Recommendation.Trim();
                            }
                            if (!string.IsNullOrEmpty(Recommendation) && !string.IsNullOrEmpty(Remark) && !string.IsNullOrEmpty(ddlStatus))
                            {
                                if (!string.IsNullOrEmpty(ChecklistStatus))
                                {
                                    if (ddlStatus != "-1")
                                    {
                                        if (ChecklistStatus == "4" && ddlStatus == "4")
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Can not save because of Status is Applicable.";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                        }
                                        else
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                                {
                                                    AuditID = AuditId,
                                                    VendorAuditScheduleOnID = ScheduleOnID,
                                                    StatusId = Convert.ToInt32(ChecklistStatus),
                                                    Remarks = Remark,
                                                    Dated = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    CreatedByText = AuthenticationHelper.User,
                                                    CustomerBranchId = Convert.ToInt64(branchID),
                                                    CheckListId = CheckListID,
                                                    StatusChangedOn = DateTime.Now,
                                                    ResultId = Convert.ToInt64(ddlStatus),
                                                    Recommendation = Recommendation
                                                };
                                                bool saveflag = RLCS_VendorScheduler.CreateTransaction(transaction);
                                                if (saveflag)
                                                {
                                                    if (ChecklistStatus == "3")
                                                    {
                                                        try
                                                        {
                                                            var userdetil = entities.sp_GetScheduleDetail(Convert.ToInt32(ScheduleOnID)).FirstOrDefault();

                                                            if (userdetil != null)
                                                            {
                                                                string ForMonth = string.Empty;
                                                                string ActName = string.Empty;
                                                                string StateName = string.Empty;
                                                                string NatureOfCompliance = string.Empty;
                                                                string TypeOfCompliance = string.Empty;

                                                                //long ChklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());

                                                                var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                                                                    join row1 in entities.RLCS_Act_Mapping
                                                                                    on row.ActID equals row1.AM_ActID
                                                                                    where row.IsDeleted == false && row.Id == CheckListID
                                                                                    select new dataEmail
                                                                                    {
                                                                                        ActName = row1.AM_ActName,
                                                                                        Stateid = row.StateID,
                                                                                        NatureOfCompliance = row.NatureOfCompliance,
                                                                                        TypeOfCompliance = row.TypeOfCompliance
                                                                                    }).FirstOrDefault();

                                                                if (getChecklist != null)
                                                                {
                                                                    ForMonth = userdetil.ForMonth;
                                                                    NatureOfCompliance = getChecklist.NatureOfCompliance;
                                                                    TypeOfCompliance = getChecklist.TypeOfCompliance;
                                                                    ActName = getChecklist.ActName;
                                                                    StateName = getChecklist.Stateid;
                                                                }

                                                                DateTime SDstartDate = userdetil.ScheduleOn;
                                                                DateTime today = DateTime.Now;

                                                                string ClientName = userdetil.ClientName;
                                                                string Subject = ClientName + " Documents rejected for the period(" + userdetil.ForMonth + ")";
                                                                string formattedDate = SDstartDate.AddDays(10).ToString("dd-MM-yyyy");

                                                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_RLCSRejectionDocument
                                                                                .Replace("@User", userdetil.CC_SPOC_Name)
                                                                                .Replace("@Date", formattedDate)
                                                                                .Replace("@ForMonth", ForMonth)
                                                                                .Replace("@StName", StateName)
                                                                                .Replace("@ActName", ActName)
                                                                                .Replace("@NatureOfCompliance", NatureOfCompliance)
                                                                                .Replace("@TypeofCompliance", TypeOfCompliance)
                                                                                .Replace("@Status", "Team Review");

                                                                List<string> aa = new List<string>();
                                                                aa.Add(userdetil.CC_SPOC_Email);
                                                                //aa.Add("amol@avantis.info");

                                                                RLCS_Vendor_AuditScheduleMail_Log obj = new RLCS_Vendor_AuditScheduleMail_Log();
                                                                obj.AuditID = Convert.ToInt32(AuditId);
                                                                obj.Avacom_UserID = Convert.ToInt32(userdetil.VendorID);
                                                                obj.ScheduleOnID = Convert.ToInt32(ScheduleOnID);
                                                                obj.CreatedOn = DateTime.Now;
                                                                obj.IsSend = true;
                                                                obj.Flag = "Document Rejected from Auditor";
                                                                entities.RLCS_Vendor_AuditScheduleMail_Log.Add(obj);
                                                                entities.SaveChanges();

                                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], aa, null, null, Subject, message);

                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }
                                                    }
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ForeColor = Label2.ForeColor = System.Drawing.Color.Green;
                                                    cvDuplicateEntry.ErrorMessage = "Record Save Successfully.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "refreshpage();", true);
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Checklist Status.";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(Remark))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Remark.";
                                }
                                else if (string.IsNullOrEmpty(Recommendation))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Recommendation.";
                                }
                                else if (string.IsNullOrEmpty(ddlStatus) || ddlStatus == "-1")
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Status.";
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                            }
                        }
                        if (vrole.Equals("HVAUD") && IsOfflineStatus == true)
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                            int UserID = AuthenticationHelper.UserID;
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            int index = Convert.ToInt32(commandArgs[1]);
                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            long branchID = Convert.ToInt64(commandArgs[2]);
                            string ddlStatus = setddlstatusvalue.Text;
                            string ChecklistStatus = "4";
                            TextBox txtRemark = grdChecklist.Rows[index].FindControl("txtRemark1") as TextBox;
                            TextBox txtRecommendationAuditor = grdChecklist.Rows[index].FindControl("txtRecommendation") as TextBox;
                            //CheckBox chkChecklistStatus = grdChecklist.Rows[index].FindControl("ddlCheckliststatus1") as CheckBox;
                            //chkChecklistStatus.Text = "Closed";
                            //chkChecklistStatus.Enabled = false;
                            string Remark = string.Empty;
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                Remark = txtRemark.Text.TrimStart(',').TrimEnd(',').Trim();
                                //Remark = txtRemark.Text.TrimStart(',');
                                //Remark = Remark.TrimEnd(',');
                                //Remark = Remark.Trim();
                            }
                            string Recommendation = string.Empty;
                            if (!string.IsNullOrEmpty(txtRecommendationAuditor.Text))
                            {
                                Recommendation = txtRecommendationAuditor.Text.TrimStart(',').TrimEnd(',').Trim();
                                //Recommendation = txtRecommendation.Text.TrimStart(',');
                                //Recommendation = Recommendation.TrimEnd(',');
                                //Recommendation = Recommendation.Trim();
                            }
                            if (!string.IsNullOrEmpty(Recommendation) && !string.IsNullOrEmpty(Remark) && !string.IsNullOrEmpty(ddlStatus))
                            {
                                if (!string.IsNullOrEmpty(ChecklistStatus))
                                {
                                    if (ddlStatus != "-1")
                                    {
                                        if (ChecklistStatus == "4" && ddlStatus == "4")
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Can not save because of Status is Applicable.";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                        }
                                        else
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                                {
                                                    AuditID = AuditId,
                                                    VendorAuditScheduleOnID = ScheduleOnID,
                                                    StatusId = Convert.ToInt32(ChecklistStatus),
                                                    Remarks = Remark,
                                                    Dated = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    CreatedByText = AuthenticationHelper.User,
                                                    CustomerBranchId = Convert.ToInt64(branchID),
                                                    CheckListId = CheckListID,
                                                    StatusChangedOn = DateTime.Now,
                                                    ResultId = Convert.ToInt64(ddlStatus),
                                                    Recommendation = Recommendation
                                                };
                                                bool saveflag = RLCS_VendorScheduler.CreateTransaction(transaction);
                                                if (saveflag)
                                                {
                                                    if (ChecklistStatus == "3")
                                                    {
                                                        try
                                                        {
                                                            var userdetil = entities.sp_GetScheduleDetail(Convert.ToInt32(ScheduleOnID)).FirstOrDefault();

                                                            if (userdetil != null)
                                                            {
                                                                string ForMonth = string.Empty;
                                                                string ActName = string.Empty;
                                                                string StateName = string.Empty;
                                                                string NatureOfCompliance = string.Empty;
                                                                string TypeOfCompliance = string.Empty;

                                                                //long ChklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());

                                                                var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                                                                    join row1 in entities.RLCS_Act_Mapping
                                                                                    on row.ActID equals row1.AM_ActID
                                                                                    where row.IsDeleted == false && row.Id == CheckListID
                                                                                    select new dataEmail
                                                                                    {
                                                                                        ActName = row1.AM_ActName,
                                                                                        Stateid = row.StateID,
                                                                                        NatureOfCompliance = row.NatureOfCompliance,
                                                                                        TypeOfCompliance = row.TypeOfCompliance
                                                                                    }).FirstOrDefault();

                                                                if (getChecklist != null)
                                                                {
                                                                    ForMonth = userdetil.ForMonth;
                                                                    NatureOfCompliance = getChecklist.NatureOfCompliance;
                                                                    TypeOfCompliance = getChecklist.TypeOfCompliance;
                                                                    ActName = getChecklist.ActName;
                                                                    StateName = getChecklist.Stateid;
                                                                }

                                                                DateTime SDstartDate = userdetil.ScheduleOn;
                                                                DateTime today = DateTime.Now;

                                                                string ClientName = userdetil.ClientName;
                                                                string Subject = ClientName + " Documents rejected for the period(" + userdetil.ForMonth + ")";
                                                                string formattedDate = SDstartDate.AddDays(10).ToString("dd-MM-yyyy");

                                                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_RLCSRejectionDocument
                                                                                .Replace("@User", userdetil.CC_SPOC_Name)
                                                                                .Replace("@Date", formattedDate)
                                                                                .Replace("@ForMonth", ForMonth)
                                                                                .Replace("@StName", StateName)
                                                                                .Replace("@ActName", ActName)
                                                                                .Replace("@NatureOfCompliance", NatureOfCompliance)
                                                                                .Replace("@TypeofCompliance", TypeOfCompliance)
                                                                                .Replace("@Status", "Team Review");

                                                                List<string> aa = new List<string>();
                                                                aa.Add(userdetil.CC_SPOC_Email);
                                                                //aa.Add("amol@avantis.info");

                                                                RLCS_Vendor_AuditScheduleMail_Log obj = new RLCS_Vendor_AuditScheduleMail_Log();
                                                                obj.AuditID = Convert.ToInt32(AuditId);
                                                                obj.Avacom_UserID = Convert.ToInt32(userdetil.VendorID);
                                                                obj.ScheduleOnID = Convert.ToInt32(ScheduleOnID);
                                                                obj.CreatedOn = DateTime.Now;
                                                                obj.IsSend = true;
                                                                obj.Flag = "Document Rejected from Auditor";
                                                                entities.RLCS_Vendor_AuditScheduleMail_Log.Add(obj);
                                                                entities.SaveChanges();
                                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], aa, null, null, Subject, message);

                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }
                                                    }
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ForeColor = Label2.ForeColor = System.Drawing.Color.Green;
                                                    cvDuplicateEntry.ErrorMessage = "Record Save Successfully.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "refreshpage();", true);
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Checklist Status.";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(Remark))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Remark.";
                                }
                                else if (string.IsNullOrEmpty(Recommendation))
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Recommendation.";
                                }
                                else if (string.IsNullOrEmpty(ddlStatus) || ddlStatus == "-1")
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Required Status.";
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                            }
                        }
                        else if (vrole.Equals("HVEND"))
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                            int UserID = AuthenticationHelper.UserID;
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            int index = Convert.ToInt32(commandArgs[1]);
                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            long branchID = Convert.ToInt64(commandArgs[2]);

                            string ddlStatus = setddlstatusvalue.Text;
                            TextBox txtRemark = grdChecklist.Rows[index].FindControl("txtRemark1") as TextBox;
                            CheckBox chkDocument = grdChecklist.Rows[index].FindControl("chkDocument") as CheckBox;

                            bool FlagIsDocument = false;
                            var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentData(CheckListID, ScheduleOnID);
                            if (lstChecklistDetails.Count > 0)
                            {
                                FlagIsDocument = true;
                            }
                            else
                            {
                                if (chkDocument.Checked)
                                {
                                    FlagIsDocument = true;
                                }
                            }
                            string Remark = string.Empty;
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                Remark = txtRemark.Text.TrimStart(',').TrimEnd(',').Trim();
                                //Remark = Remark.TrimEnd(',');
                                //Remark = Remark.Trim();
                            }
                            if (FlagIsDocument)
                            {
                                if (!string.IsNullOrEmpty(Remark) && !string.IsNullOrEmpty(ddlStatus))
                                {
                                    if (ddlStatus != "-1")
                                    {
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            RLCS_VendorAuditScheduleOn checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                                     where row.ID == ScheduleOnID
                                                                                     && row.IsActive == true
                                                                                     select row).FirstOrDefault();
                                            if (checkExist != null)
                                            {
                                                if (checkExist.AuditStatusID == 1)
                                                {
                                                    checkExist.AuditStatusID = 2;
                                                    entities.SaveChanges();
                                                }
                                                RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                                {
                                                    AuditID = AuditId,
                                                    VendorAuditScheduleOnID = ScheduleOnID,
                                                    StatusId = 2,
                                                    Remarks = Remark,
                                                    Dated = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    CreatedByText = AuthenticationHelper.User,
                                                    CustomerBranchId = Convert.ToInt64(branchID),
                                                    CheckListId = CheckListID,
                                                    StatusChangedOn = DateTime.Now,
                                                    ResultId = Convert.ToInt64(ddlStatus),
                                                    IsDocument = chkDocument.Checked
                                                };
                                                bool saveflag = RLCS_VendorScheduler.CreateTransaction(transaction);
                                                if (saveflag)
                                                {
                                                    int CheckListStatus = Convert.ToInt32(Request.QueryString["SID"]);
                                                    if (CheckListStatus == 3)
                                                    {
                                                        try
                                                        {
                                                            var userdetil = entities.sp_GetScheduleDetail(Convert.ToInt32(ScheduleOnID)).FirstOrDefault();

                                                            if (userdetil != null)
                                                            {
                                                                string ForMonth = string.Empty;
                                                                string ActName = string.Empty;
                                                                string StateName = string.Empty;
                                                                string NatureOfCompliance = string.Empty;
                                                                string TypeOfCompliance = string.Empty;

                                                                long ChklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());

                                                                var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                                                                    join row1 in entities.RLCS_Act_Mapping
                                                                                    on row.ActID equals row1.AM_ActID
                                                                                    where row.IsDeleted == false && row.Id == ChklistID
                                                                                    select new dataEmail
                                                                                    {
                                                                                        ActName = row1.AM_ActName,
                                                                                        Stateid = row.StateID,
                                                                                        NatureOfCompliance = row.NatureOfCompliance,
                                                                                        TypeOfCompliance = row.TypeOfCompliance
                                                                                    }).FirstOrDefault();

                                                                if (getChecklist != null)
                                                                {
                                                                    ForMonth = userdetil.ForMonth;
                                                                    NatureOfCompliance = getChecklist.NatureOfCompliance;
                                                                    TypeOfCompliance = getChecklist.TypeOfCompliance;
                                                                    ActName = getChecklist.ActName;
                                                                    StateName = getChecklist.Stateid;
                                                                }

                                                                DateTime SDstartDate = userdetil.ScheduleOn;
                                                                DateTime today = DateTime.Now;

                                                                string ClientName = userdetil.ClientName;
                                                                string Subject = ClientName + " Documents reuploaded for the period(" + userdetil.ForMonth + ")";
                                                                string formattedDate = SDstartDate.AddDays(10).ToString("dd-MM-yyyy");

                                                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_RLCSReUploadDocument
                                                                                .Replace("@User", userdetil.CC_SPOC_Name)
                                                                                .Replace("@Date", formattedDate)
                                                                                .Replace("@ForMonth", ForMonth)
                                                                                .Replace("@StName", StateName)
                                                                                .Replace("@ActName", ActName)
                                                                                .Replace("@NatureOfCompliance", NatureOfCompliance)
                                                                                .Replace("@TypeofCompliance", TypeOfCompliance)
                                                                                .Replace("@Status", "Submitted");

                                                                List<string> aa = new List<string>();
                                                                aa.Add(userdetil.AuditorEmail);
                                                                //aa.Add("amol@avantis.info");

                                                                RLCS_Vendor_AuditScheduleMail_Log obj = new RLCS_Vendor_AuditScheduleMail_Log();
                                                                obj.AuditID = Convert.ToInt32(AuditId);
                                                                obj.Avacom_UserID = Convert.ToInt32(userdetil.VendorID);
                                                                obj.ScheduleOnID = Convert.ToInt32(ScheduleOnID);
                                                                obj.CreatedOn = DateTime.Now;
                                                                obj.IsSend = true;
                                                                obj.Flag = "Document Uploaded from Vendor in Team Review";
                                                                entities.RLCS_Vendor_AuditScheduleMail_Log.Add(obj);
                                                                entities.SaveChanges();

                                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], aa, null, null, Subject, message);

                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }
                                                    }

                                                    cvDuplicateEntry.IsValid = false;
                                                    Label2.ForeColor = System.Drawing.Color.Green;
                                                    cvDuplicateEntry.ErrorMessage = "Record Save Successfully.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "refreshpage();", true);
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(Remark))
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Required Remark.";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                    }
                                    else if (string.IsNullOrEmpty(ddlStatus) || ddlStatus == "-1")
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select check box because documents are not available";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + CheckListID + "," + index + ");", true);
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("Upload_Document"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    
                    int UserID = AuthenticationHelper.UserID;
                    long ChecklistID = Convert.ToInt64(commandArgs[0]);
                    int index = Convert.ToInt32(commandArgs[1]);
                    long AuditID = Convert.ToInt32(ViewState["AID"]);
                    long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                    
                    FileUpload docFileUpload = grdChecklist.Rows[index].FindControl("docFileUpload") as FileUpload;
                    
                    if (docFileUpload != null)
                    {
                        if (docFileUpload.HasFiles)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<RLCSVendorFileData> files = new List<RLCSVendorFileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                string directoryPath = null;
                                string version = null;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\RLCSVendorDocuments\\" + UserID + "\\" + AuditID.ToString() + "\\" + ChecklistID.ToString() + "\\" + ScheduleOnID + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/RLCSVendorDocuments/" + UserID + "/" + AuditID.ToString() + "/" + ChecklistID.ToString() + "/" + ScheduleOnID + "/" + version);
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];

                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";

                                    fileName = uploadfile.FileName;
                                    list.Add(new KeyValuePair<string, int>(fileName, 1));

                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        RLCSVendorFileData file = new RLCSVendorFileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            CreatedOn = DateTime.Now,
                                            FileSize = uploadfile.ContentLength.ToString()
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                flag = RLCSVendorMastersManagement.FileUpload(Convert.ToInt64(ChecklistID), Convert.ToInt64(ScheduleOnID), files, list, Filelist);

                                if (flag)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    Label2.ForeColor = System.Drawing.Color.Green;
                                    cvDuplicateEntry.ErrorMessage = "File Upload Successfully.";
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please attached at least one file.";
                        }
                        GridView grdDocument = grdChecklist.Rows[index].FindControl("grdDocument1") as GridView;

                        if (grdDocument != null)
                        {
                            long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                            var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentData(ChecklistID, ScheduleOnID, VendorID);
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + ChecklistID + "," + index + ");", true);                        
                    }
                }
                else if (e.CommandName.Equals("EditChecklist"))
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                        if (vrole.Equals("HVEND"))
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            string CheckListStatus = Convert.ToString(commandArgs[1]);
                            long CustomerBranchID = Convert.ToInt64(commandArgs[2]);

                            long StatusId = -1;
                            if (Convert.ToString(commandArgs[1]) == "Open")
                                StatusId = 1;

                            if (Convert.ToString(commandArgs[1]) == "Submitted")
                                StatusId = 2;

                            if (Convert.ToString(commandArgs[1]) == "Team Review")
                                StatusId = 3;

                            if (Convert.ToString(commandArgs[1]) == "Closed")
                                StatusId = 4;

                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                            ScriptManager.RegisterStartupScript(this, GetType(), "script", "openperformerpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + "," + VendorID + ");", true);
                        }
                        else if (vrole.Equals("HVAUD") && IsOfflineStatus == false)
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            string CheckListStatus = Convert.ToString(commandArgs[1]);
                            long CustomerBranchID = Convert.ToInt64(commandArgs[2]);
                            long StatusId = -1;
                            if (Convert.ToString(commandArgs[1]) == "Open")
                                StatusId = 1;

                            if (Convert.ToString(commandArgs[1]) == "Submitted")
                                StatusId = 2;

                            if (Convert.ToString(commandArgs[1]) == "Team Review")
                                StatusId = 3;

                            if (Convert.ToString(commandArgs[1]) == "Closed")
                                StatusId = 4;

                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                            ScriptManager.RegisterStartupScript(this, GetType(), "script", "openreviwerpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + "," + VendorID + ");", true);
                        }
                        else if (vrole.Equals("HVAUD") && IsOfflineStatus == true)
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            string CheckListStatus = Convert.ToString(commandArgs[1]);
                            long CustomerBranchID = Convert.ToInt64(commandArgs[2]);
                            long StatusId = -1;
                            if (Convert.ToString(commandArgs[1]) == "Open")
                                StatusId = 1;

                            if (Convert.ToString(commandArgs[1]) == "Submitted")
                                StatusId = 2;

                            if (Convert.ToString(commandArgs[1]) == "Team Review")
                                StatusId = 3;

                            if (Convert.ToString(commandArgs[1]) == "Closed")
                                StatusId = 4;

                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                            ScriptManager.RegisterStartupScript(this, GetType(), "script", "openreviwerperformerpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + "," + VendorID + ");", true);
                        }
                    }
                }
                else if (e.CommandName.Equals("ClosedChecklist"))
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {

                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        long CheckListID = Convert.ToInt64(commandArgs[0]);
                        string CheckListStatus = Convert.ToString(commandArgs[1]);
                        long CustomerBranchID = Convert.ToInt64(commandArgs[2]);

                        long StatusId = -1;
                        if (Convert.ToString(commandArgs[1]) == "Open")
                            StatusId = 1;

                        if (Convert.ToString(commandArgs[1]) == "Submitted")
                            StatusId = 2;

                        if (Convert.ToString(commandArgs[1]) == "Team Review")
                            StatusId = 3;

                        if (Convert.ToString(commandArgs[1]) == "Closed")
                            StatusId = 4;

                        long AuditId = Convert.ToInt32(ViewState["AID"]);
                        long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                        ScriptManager.RegisterStartupScript(this, GetType(), "script", "openClosedpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + ");", true);                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public static List<int> GetAssignedroleid(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.RLCS_Sp_GetAllAssignedRoles(Userid)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();
                return roles;
            }
        }
        protected bool DisplayOnlyAuditor()
        {
            try
            {
                bool result = false;
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                    var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                   
                    if (vrole.Equals("HVAUD"))
                    {
                        result = true;
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool DisplayOnlyAuditor(bool chklistStatus)
        {
            try
            {
                bool result = false;
                if (chklistStatus != true)
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                        if (vrole.Equals("HVAUD"))
                        {
                            result = true;
                        }
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool DisplayOnlyVendor()
        {
            try
            {
                bool result = false;
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                    var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                    if (vrole.Equals("HVEND") || (vrole.Equals("HVAUD") && IsOfflineStatus == true))
                    {
                        result = true;
                    }
                }                
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool DisplayOnlyVendorAuditor()
        {
            try
            {
                bool result = false;
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                    var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                    if (vrole.Equals("HVAUD") && IsOfflineStatus == true)
                    {
                        result = true;
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangeStatus(string Status,int astustid)
        {
            try
            {
                bool result = false;
                if (astustid == 5)
                {
                    result = false;
                }
                else
                {
                    int statusID = -1;
                    if (Status == "Open")
                    {
                        statusID = 1;
                    }
                    else if (Status == "Submitted")
                    {
                        statusID = 2;
                    }
                    else if (Status == "Team Review")
                    {
                        statusID = 3;
                    }
                    else
                    {
                        statusID = 4;
                    }
                    //List<int> roles = roles12;
                    List<int> roles = GetAssignedroleid(AuthenticationHelper.UserID);
                    if (roles.Contains(3))
                    {
                        result = CanChangePerformerStatus(AuthenticationHelper.UserID, 3, statusID);
                    }
                    else if (roles.Contains(4))
                    {
                        result = CanChangeReviewerStatus(AuthenticationHelper.UserID, 4, statusID);
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CanChangeStatusClosed(string Status)
        {
            try
            {
                bool result = false;
                if (!string.IsNullOrEmpty(Status))
                {
                    if (Status.Equals("Closed"))
                    {
                        result = true;
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        
        protected bool CanChangePerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 3;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangeReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || (statusID == 1 && IsOfflineStatus == true);
                    }

                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindAuditCheckList();                
                string  role = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
                {
                    role = Convert.ToString(ViewState["vrole"]);
                }
                else
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        //role = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                        role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        ViewState["vrole"] = Convert.ToString(role);                                              
                    }
                }
                buttonvisible(role);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void buttonvisible(string Role)
        {
            try
            {
                if (Role == "HVAUD")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long scheduleonID = -1;
                        long auditID = -1;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                        {
                            auditID = Convert.ToInt64(ViewState["AID"]);
                        }
                        else
                        {
                            auditID = Convert.ToInt64(Request.QueryString["AID"]);
                            ViewState["AID"] = auditID;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                        {
                            scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                        }
                        else
                        {
                            scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                            ViewState["SOID"] = scheduleonID;
                        }
                        if (auditID != -1 && scheduleonID != -1)
                        {
                            var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                              where row.AuditID == auditID
                                              && row.ID == scheduleonID
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                            if (checkExist != null)
                            {
                                if (checkExist.AuditStatusID == 4)
                                {
                                    btnAddNewStep.Visible = false;
                                    btnSave.Visible = false;
                                    txtRecomandation.Visible = false;
                                    txtObservation.Visible = false;
                                    //  btnSaveClose.Visible = true;
                                }
                                else
                                {
                                    btnAddNewStep.Visible = true;
                                }
                                if (checkExist.AuditStatusID != 4)
                                {
                                    var totalchecklist = (from row in entities.RLCS_VendorAuditStepMapping
                                                          where row.AuditID == auditID
                                                          && row.IsActive == true
                                                          && row.VendorAuditScheduleOnID == scheduleonID
                                                          select row).ToList();

                                    totalchecklist = totalchecklist.GroupBy(A => A.CheckListID).Select(aa => aa.FirstOrDefault()).ToList();


                                    var closedchecklist = (from row in entities.RLCS_VendorAuditTransaction
                                                           where row.AuditID == auditID
                                                           && row.VendorAuditScheduleOnID == scheduleonID
                                                           && row.StatusId == 4
                                                           select row).ToList();

                                    closedchecklist = closedchecklist.GroupBy(A => A.CheckListId).Select(aa => aa.FirstOrDefault()).ToList();
                                    if (totalchecklist.Count == closedchecklist.Count)
                                    {
                                        btnSaveClose.Visible = true;
                                        txtRecomandation.Visible = true;
                                        txtObservation.Visible = true;
                                        btnSave.Visible = false;
                                    }
                                    else
                                    {
                                        btnSave.Visible = true;
                                        btnSaveClose.Visible = false;
                                        txtRecomandation.Visible = true;
                                        txtObservation.Visible = true;
                                    }

                                    //if (TotalChecklistCount == TotalChecklistCloseCount)
                                    //{
                                    //    btnSaveClose.Visible = true;
                                    //    txtRecomandation.Visible = true;
                                    //    txtObservation.Visible = true;
                                    //    btnSave.Visible = false;
                                    //}
                                }
                            }
                        }
                    }
                }
                else
                {
                    btnSave.Visible = false;
                    btnSaveClose.Visible = false;
                    txtRecomandation.Visible = false;
                    txtObservation.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long scheduleonID = -1;
                    long auditID = -1;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                    {
                        auditID = Convert.ToInt64(ViewState["AID"]);
                    }
                    else
                    {
                        auditID = Convert.ToInt64(Request.QueryString["AID"]);
                        ViewState["AID"] = auditID;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                    {
                        scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                    }
                    else
                    {
                        scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                        ViewState["SOID"] = scheduleonID;
                    }
                    if (auditID != -1 && scheduleonID != -1)
                    {
                        var totalchecklist = (from row in entities.RLCS_VendorAuditStepMapping
                                              where row.AuditID == auditID
                                              && row.IsActive == true
                                              && row.VendorAuditScheduleOnID == scheduleonID
                                              select row).ToList();

                        totalchecklist = totalchecklist.GroupBy(A => A.CheckListID).Select(aa => aa.FirstOrDefault()).ToList();


                        var closedchecklist = (from row in entities.RLCS_VendorAuditTransaction
                                               where row.AuditID == auditID
                                               && row.VendorAuditScheduleOnID == scheduleonID
                                               && row.StatusId == 4
                                               select row).ToList();

                        closedchecklist = closedchecklist.GroupBy(A => A.CheckListId).Select(aa => aa.FirstOrDefault()).ToList();

                        if (totalchecklist.Count == closedchecklist.Count)
                        {
                            var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                              where row.ID == scheduleonID
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                            if (checkExist != null)
                            {
                                checkExist.AuditStatusID = 4;
                                checkExist.Recommendation = txtRecomandation.Text;
                                checkExist.Observation = txtObservation.Text;
                                entities.SaveChanges();

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Audit Closed Sucessfully.";
                                btnSaveClose.Visible = false;
                                btnAddNewStep.Visible = false;
                                txtRecomandation.Visible = true;
                                txtObservation.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }//

       
        private void StepBind(Saplin.Controls.DropDownCheckBoxes DrpNewStepCheck)
        {
            try
            {
                long scheduleonID = -1;
                long auditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                {
                    auditID = Convert.ToInt64(ViewState["AID"]);
                }
                else
                {
                    auditID = Convert.ToInt64(Request.QueryString["AID"]);
                    ViewState["AID"] = auditID;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                {
                    scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                }
                else
                {
                    scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                    ViewState["SOID"] = scheduleonID;
                }
                if (auditID != -1 && scheduleonID != -1)
                {
                    DrpNewStepCheck.DataTextField = "Name";
                    DrpNewStepCheck.DataValueField = "ID";
                    DrpNewStepCheck.DataSource = FillNewSteps(auditID, scheduleonID);
                    DrpNewStepCheck.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        public static object FillNewSteps(long AID, long SOID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getvendorid = (from row in RLCS_VendorAuditAssignment1
                                   where row.AuditId == AID && row.RoleID == 3
                                   select row.UserID).FirstOrDefault();

                //var getvendorid = (from row in entities.RLCS_VendorAuditAssignment
                //                   where row.AuditId == AID && row.RoleID==3
                //                   select row.UserID).FirstOrDefault();

                var query = (from row in entities.Sp_RLCS_VendorNewStep(AID, SOID, getvendorid)                           
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }        
        protected void ddlNewStepCheck_SelcetedIndexChanged(object sender, EventArgs e)
        {
            Saplin.Controls.DropDownCheckBoxes DrpNewStepCheck = (DropDownCheckBoxes)sender;
            NewStepID.Clear();
            for (int i = 0; i < DrpNewStepCheck.Items.Count; i++)
            {
                if (DrpNewStepCheck.Items[i].Selected)
                {
                    NewStepID.Add(Convert.ToInt64(DrpNewStepCheck.Items[i].Value));
                }
            }
        }
        public static bool CreateVendorAuditStepMapping(RLCS_VendorAuditStepMapping objRVASM)
        {           
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.RLCS_VendorAuditStepMapping.Add(objRVASM);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
               return  false;
            }
        }
        protected void btnPopupSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool savesucess = false;
                long scheduleonID = -1;
                long auditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                {
                    auditID = Convert.ToInt64(ViewState["AID"]);
                }
                else
                {
                    auditID = Convert.ToInt64(Request.QueryString["AID"]);
                    ViewState["AID"] = auditID;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                {
                    scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                }
                else
                {
                    scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                    ViewState["SOID"] = scheduleonID;
                }
                if (auditID != -1 && scheduleonID != -1)
                {
                    NewStepID.Clear();
                    for (int i = 0; i < ddlNewStepCheck.Items.Count; i++)
                    {
                        if (ddlNewStepCheck.Items[i].Selected)
                        {
                            NewStepID.Add(Convert.ToInt64(ddlNewStepCheck.Items[i].Value));
                        }
                    }
                    if (NewStepID.Count > 0)
                    {
                        foreach (var aItem in NewStepID)
                        {
                            RLCS_VendorAuditStepMapping RVASM = new RLCS_VendorAuditStepMapping()
                            {
                                AuditID = auditID,
                                VendorAuditScheduleOnID = scheduleonID,
                                CheckListID = aItem,
                                IsActive = true,
                            };
                            savesucess= CreateVendorAuditStepMapping(RVASM);
                        }
                        NewStepID.Clear();
                    }
                    if (savesucess)
                    {
                        cvpopup.IsValid = false;
                        cvpopup.ErrorMessage = "Checklist added to audit sucessfully";
                        BindAuditCheckList();
                        StepBind(ddlNewStepCheck);
                        Upvendorchecklist.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnMusterRoll_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AID"])
                          && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int UserID = AuthenticationHelper.UserID;
                    int AuditID = Convert.ToInt32(Request.QueryString["AID"].ToString());
                    int ScheduleOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    if (MusterRollFileUpload != null)
                    {
                        if (MusterRollFileUpload.HasFile)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<RLCSVendorFileData> files = new List<RLCSVendorFileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                string directoryPath = null;
                                string version = null;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\RLCSVendorDocuments\\" + UserID + "\\" + AuditID.ToString() + "\\" + ScheduleOnID + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/RLCSVendorDocuments/" + UserID + "/" + AuditID.ToString() + "/" + ScheduleOnID + "/" + version);
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];

                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";

                                    fileName = uploadfile.FileName;
                                    list.Add(new KeyValuePair<string, int>(fileName, 1));

                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        RLCSVendorFileData file = new RLCSVendorFileData()
                                        {
                                            Name =  fileName ,// ddlDocumentType.SelectedItem.ToString(),
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            CreatedOn = DateTime.Now,
                                            FileSize = uploadfile.ContentLength.ToString()
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                flag = RLCSVendorMastersManagement.MusterRollFileUpload(Convert.ToInt64(AuditID), Convert.ToInt64(ScheduleOnID), files, list, Filelist);

                                if (flag)
                                {
                                   // cvDuplicateEntry.IsValid = false;
                                   // cvDuplicateEntry.ErrorMessage = "File Upload Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "SuccessAlert", "alert('File Upload Successfully.')", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                                }
                                else
                                {
                                   // cvDuplicateEntry.IsValid = false;
                                  //  cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "SuccessAlert", "alert('Please do not upload virus file or blank files.')", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                                //cvDuplicateEntry.IsValid = false;
                                //cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }
                        else
                        {
                            //cvDuplicateEntry.IsValid = false;
                            //cvDuplicateEntry.ErrorMessage = "Please attached at least one file.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "SuccessAlert", "alert('Please attached at least one file.')", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                        }
                    }
                   BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (RLCSVendorMastersManagement.DeleteRLCSVendorFile(FileID, Convert.ToInt32(AuthenticationHelper.UserID)))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);                            
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=VendorAuditDocumnet.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        public void BindGrid()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int AuditID = Convert.ToInt32(Request.QueryString["AID"].ToString());
                    long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = RLCSVendorMastersManagement.GetMustorDocumentData(Convert.ToInt64(AuditID), Convert.ToInt64(ScheduleOnID));
                        if (lstChecklistDetails.Count > 0)
                        {
                          
                            ViewState["DocumentListCount"] = lstChecklistDetails.Count;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        else
                        {
                            ViewState["DocumentListCount"] = 0;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long scheduleonID = -1;
                    long auditID = -1;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                    {
                        auditID = Convert.ToInt64(ViewState["AID"]);
                    }
                    else
                    {
                        auditID = Convert.ToInt64(Request.QueryString["AID"]);
                        ViewState["AID"] = auditID;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                    {
                        scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                    }
                    else
                    {
                        scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                        ViewState["SOID"] = scheduleonID;
                    }
                    if (auditID != -1 && scheduleonID != -1)
                    {
                        var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                          where row.ID == scheduleonID
                                          && row.IsActive == true
                                          select row).FirstOrDefault();
                        if (checkExist != null)
                            {
                                checkExist.Recommendation = txtRecomandation.Text;
                                checkExist.Observation = txtObservation.Text;
                                entities.SaveChanges();

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Recommendation and Observation Save Sucessfully.";
                            }
                        
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdChecklist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                Label ddlstatusname = e.Row.FindControl("ddlstatusname") as Label;
                Label lblStatus = e.Row.FindControl("lblStatus") as Label;
                Label lblCustomerbranch = e.Row.FindControl("lblCustomerbranch") as Label;
                Label LblId = e.Row.FindControl("LblId") as Label;
                TextBox txtRemark = e.Row.FindControl("txtRemark1") as TextBox;
                txtRemark.Text = "";

                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    //var vrole = role1.Where(x => x.ID == (int)LoggedUser.VendorRoleID).Select(x => x.Code).FirstOrDefault();
                    var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                   
                        long CheckListID = Convert.ToInt32(LblId.Text);
                        
                        string CheckListStatus = lblStatus.Text;
                        long CustomerBranchID = Convert.ToInt32(lblCustomerbranch.Text);

                        long StatusId = -1;

                        if (CheckListStatus == "Open")
                            StatusId = 1;

                        if (CheckListStatus == "Submitted")
                            StatusId = 2;

                        if (CheckListStatus == "Team Review")
                            StatusId = 3;

                        if (CheckListStatus == "Closed")
                            StatusId = 4;

                        long AuditId = Convert.ToInt32(ViewState["AID"]);
                        long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                        DropDownList ddlstatus = (DropDownList)e.Row.FindControl("ddlStatus1");
                                        
                        var data = RLCSVendorMastersManagement.getRLCSResultStatus(vrole);
                        ddlstatus.DataTextField = "Name";
                        ddlstatus.DataValueField = "ID";
                        ddlstatus.DataSource = data;
                        ddlstatus.DataBind();
                        ddlstatus.Items.Insert(0, new ListItem("Select Status", "-1"));
                    //ddlstatus.SelectedValue = "3";
                    if (vrole.Equals("HVAUD"))
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            //var getChecklist = (from row in entities.RLCS_Vendor_RecentCheckListStatus(Convert.ToInt64(Request.QueryString["AID"]), CheckListID, ScheduleOnID)
                            //                    select row).FirstOrDefault();
                            var getChecklist = (from row in RLCS_Vendor_RecentCheckListStatus1
                                                where row.CheckListID == CheckListID
                                                select row).FirstOrDefault();
                            if (getChecklist != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(getChecklist.ResultID)))
                                {
                                    ddlstatusname.Text = data.Where(x => x.ID == getChecklist.ResultID).
                                        Select(x => x.Name).FirstOrDefault();
                                    //ddlstatus.SelectedValue = Convert.ToString(getChecklist.ResultID);
                                }
                            }
                        }
                    }

                    GridView grdDocument = e.Row.FindControl("grdDocument1") as GridView;
                    
                    if (grdDocument != null)
                    {
                        long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                        chkIDforExpry = CheckListID;
                       var lstChecklistDetails1 = RLCSVendorMastersManagement.getDocumentData(CheckListID, ScheduleOnID, VendorID);
                        //var lstChecklistDetails = (from row in RLCSVendorFileData12
                        //                    where row.ChecklistID == CheckListID
                        //                    select row).ToList();


                        grdDocument.DataSource = lstChecklistDetails1;//lstChecklistDetails;
                        grdDocument.DataBind();
                    }
                }
            }
        }
        public static List<RLCSVendorFileDataMapping> getDocumentData(long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row1 in entities.RLCSVendorFileDataMappings
                                    where row1.FileID == FileID
                                    select row1).ToList();

                return objChecklist;
            }
        }
        protected void grdDocument_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    int index = (((GridViewRow)(((Control)(sender)).Parent.BindingContainer))).RowIndex;
                    //TextBox txtRemark = grdChecklist.Rows[index].FindControl("txtRemark1") as TextBox;
                    TextBox txtRecommendation = grdChecklist.Rows[index].FindControl("txtRecommendation") as TextBox;

                    //string Remark = string.Empty;
                    //if (!string.IsNullOrEmpty(txtRemark.Text))
                    //{
                    //    Remark = txtRemark.Text;
                    //}
                    //string Remark = string.Empty;
                    //if (!string.IsNullOrEmpty(txtRemark.Text))
                    //{
                    //    Remark = txtRemark.Text.TrimStart(',');
                    //    txtRemark.Text = Remark;
                    //}
                    string Recommendation = string.Empty;
                    if (!string.IsNullOrEmpty(txtRecommendation.Text))
                    {
                        Recommendation = txtRecommendation.Text.TrimStart(',');
                        txtRecommendation.Text = Recommendation;
                    }
                    if (FileID > 0)
                    {
                        var getFiledetail = getDocumentData(FileID);
                        if (RLCSVendorMastersManagement.DeleteRLCSVendorFile(FileID, Convert.ToInt32(AuthenticationHelper.UserID)))
                        {
                            GridView grdDocument = grdChecklist.Rows[index].FindControl("grdDocument1") as GridView;
                            if (getFiledetail != null && getFiledetail.Count > 0)
                            {
                                long CheckListID = getFiledetail[0].ChecklistID;
                                long ScheduleOnID = Convert.ToInt32(getFiledetail[0].ScheduledOnID);
                                long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                                if (grdDocument != null)
                                {
                                    var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentData(CheckListID, ScheduleOnID, VendorID);
                                    grdDocument.DataSource = lstChecklistDetails;
                                    grdDocument.DataBind();
                                }
                            }                 
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            if (getFiledetail[0].ChecklistID != null)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + getFiledetail[0].ChecklistID + "," + index + ");", true);                                
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        int index = (((GridViewRow)(((Control)(sender)).Parent.BindingContainer))).RowIndex;
                        //TextBox txtRemark = grdChecklist.Rows[index].FindControl("txtRemark1") as TextBox;
                        TextBox txtRecommendation = grdChecklist.Rows[index].FindControl("txtRecommendation") as TextBox;

                        //string Remark = string.Empty;
                        //if (!string.IsNullOrEmpty(txtRemark.Text))
                        //{
                        //    Remark = txtRemark.Text;
                        //}
                        //string Remark = string.Empty;
                        //if (!string.IsNullOrEmpty(txtRemark.Text))
                        //{
                        //    Remark = txtRemark.Text.TrimStart(',');
                        //    txtRemark.Text = Remark;
                        //}
                        string Recommendation = string.Empty;
                        if (!string.IsNullOrEmpty(txtRecommendation.Text))
                        {
                            Recommendation = txtRecommendation.Text.TrimStart(',');
                            txtRecommendation.Text = Recommendation;
                        }

                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=VendorAudit.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    var getFiledetail = getDocumentData(FileID);
                                    if (getFiledetail != null && getFiledetail.Count > 0)
                                    {
                                        if (getFiledetail[0].ChecklistID != null)
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + getFiledetail[0].ChecklistID + "," + index + ");", true);
                                        }
                                    }      
                                }
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        int index = (((GridViewRow)(((Control)(sender)).Parent.BindingContainer))).RowIndex;
                        //TextBox txtRemark = grdChecklist.Rows[index].FindControl("txtRemark1") as TextBox;
                        TextBox txtRecommendation = grdChecklist.Rows[index].FindControl("txtRecommendation") as TextBox;

                        //string Remark = string.Empty;
                        //if (!string.IsNullOrEmpty(txtRemark.Text))
                        //{
                        //    Remark = txtRemark.Text;
                        //}
                        //string Remark = string.Empty;
                        //if (!string.IsNullOrEmpty(txtRemark.Text))
                        //{
                        //    Remark = txtRemark.Text.TrimStart(',');
                        //    txtRemark.Text = Remark;
                        //}
                        string Recommendation = string.Empty;
                        if (!string.IsNullOrEmpty(txtRecommendation.Text))
                        {
                            Recommendation = txtRecommendation.Text.TrimStart(',');
                            txtRecommendation.Text = Recommendation;
                        }

                        RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    string CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    //lblMessage.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                    var getFiledetail = getDocumentData(FileID);
                                    if (getFiledetail != null && getFiledetail.Count > 0)
                                    {
                                        if (getFiledetail[0].ChecklistID != null)
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Opengridplus(" + getFiledetail[0].ChecklistID + "," + index + ");", true);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            //GridViewRow row = grdChecklist.Rows[e.RowIndex] as GridViewRow;
        }

        protected void btnSave1_Click(object sender, EventArgs e)
        {
            Label lblStateID = grdChecklist.Rows[2].FindControl("lblStateID") as Label;

            TextBox txtRemark = grdChecklist.Rows[2].FindControl("txtRemark1") as TextBox;
            DropDownList ddlStatus = grdChecklist.Rows[2].FindControl("ddlStatus1") as DropDownList;
            CheckBox chkDocument = grdChecklist.Rows[2].FindControl("chkDocument") as CheckBox;
           
        }

        protected void grdChecklist_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            TextBox txtRemark = grdChecklist.Rows[0].FindControl("txtRemark1") as TextBox;
            txtRemark.Text = "";

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    TextBox txtRemark = e.Row.FindControl("txtRemark1") as TextBox;
            //    txtRemark.Text = "";
            //}
        }

        protected void grdDocument1_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            
        }

        protected bool displayExpMsg()
        {
            try
            {
                bool result = false;

                var lstChecklistDetails = (from row in RLCSVendorFileData12
                                           where row.ChecklistID == chkIDforExpry
                                           select row).ToList();
                if (lstChecklistDetails.Count > 0)
                {

                    foreach (var item in lstChecklistDetails)
                    {
                        if (item.ValidTill < DateTime.Today)
                        {
                            result = true;
                        }

                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        //protected void ddlStatus1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Label lblStateID = grdChecklist.Rows[2].FindControl("lblStateID") as Label;

        //    TextBox txtRemark = grdChecklist.Rows[2].FindControl("txtRemark1") as TextBox;
        //    DropDownList ddlStatus = grdChecklist.Rows[2].FindControl("ddlStatus1") as DropDownList;
        //    CheckBox chkDocument = grdChecklist.Rows[2].FindControl("chkDocument") as CheckBox;
        //}

        //protected void ddlStatus1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Label lblStateID = grdChecklist.Rows[2].FindControl("lblStateID") as Label;
        //    TextBox txtRemark = grdChecklist.Rows[2].FindControl("txtRemark1") as TextBox;
        //    DropDownList ddlStatus = grdChecklist.Rows[2].FindControl("ddlStatus1") as DropDownList;
        //    CheckBox chkDocument = grdChecklist.Rows[2].FindControl("chkDocument") as CheckBox;
        //}

        //protected void ddlStatus1_SelectedIndexChanged1(object sender, EventArgs e)
        //{
        //    Label lblStateID = grdChecklist.Rows[2].FindControl("lblStateID") as Label;

        //    TextBox txtRemark = grdChecklist.Rows[2].FindControl("txtRemark1") as TextBox;
        //    DropDownList ddlStatus = grdChecklist.Rows[2].FindControl("ddlStatus1") as DropDownList;
        //    CheckBox chkDocument = grdChecklist.Rows[2].FindControl("chkDocument") as CheckBox;
        //}

        //protected void UploadDocument_Click1(object sender, EventArgs e)
        //{
        //    FileUpload fu = ((GridViewRow)((WebControl)sender).NamingContainer)
        //  .FindControl("docFileUpload") as FileUpload;

        //    if (fu != null && fu.HasFile)
        //    {
        //        try
        //        {
        //            //possible issue here.
        //            //process NEED PERMISSION to write to this folder
        //            //also some checks with fu.PostedFile are recommended
        //            fu.SaveAs(Server.MapPath("~/images/" + fu.FileName));
        //            ok = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            ok = false;
        //        }
        //    }

        //    Label lblStateID = grdChecklist.Rows[0].FindControl("lblStateID") as Label;
        //    FileUpload docFileUpload = grdChecklist.Rows[0].FindControl("docFileUpload") as FileUpload;
        //    //FileUpload docFileUpload = grdChecklist.FindControl("docFileUpload") as FileUpload;

        //    if (docFileUpload != null)
        //    {
        //        if (docFileUpload.HasFile)
        //        {
        //        }
        //    }


        //protected void UploadDocumentdetail_Click(object sender, EventArgs e)
        //{
        //    FileUpload fu = ((GridViewRow)((WebControl)sender).NamingContainer)
        //  .FindControl("docFileUpload") as FileUpload;

        //    if (fu != null && fu.HasFile)
        //    {
        //        try
        //        {
        //            //possible issue here.
        //            //process NEED PERMISSION to write to this folder
        //            //also some checks with fu.PostedFile are recommended
        //            fu.SaveAs(Server.MapPath("~/images/" + fu.FileName));
        //            //ok = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            //ok = false;
        //        }
        //    }

        //    Label lblStateID = grdChecklist.Rows[0].FindControl("lblStateID") as Label;
        //    FileUpload docFileUpload = grdChecklist.Rows[0].FindControl("docFileUpload") as FileUpload;
        //    //FileUpload docFileUpload = grdChecklist.FindControl("docFileUpload") as FileUpload;

        //    if (docFileUpload != null)
        //    {
        //        if (docFileUpload.HasFile)
        //        {
        //        }
        //    }
        //}
    }
}