﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSDocumentRepository.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSDocumentRepository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../assets/fSelect.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-multiselect.js"></script>
    <script src="../assets/fSelect.js"></script>
     <style type="text/css">
        .aspNetDisabled {
            width: 100%;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #8e8e93;
            background-color: #fff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftuploadmenu');
            fhead('Document Repository');
            $('select#ContentPlaceHolder1_ddlTypeofChecklist').change(function () { if ($('select#ContentPlaceHolder1_ddlTypeofChecklist').val() == 'CustomerWise') { $('#ddlCrr1').show(); } else { $('#ddlCrr1').hide(); } });

            BindControls();
            $('#divUploadFilesAddDialog').hide();

            function fopenpopup() {
                $('#divOpenNewFolderPopup').modal('show');
            }
            function fclosepopup() {
                $('#divOpenNewFolderPopup').modal('hide');
            }
            //$("#ContentPlaceHolder1_ddlCustomer").on("change", function (e) {
            
            //    //GetCustomerFrequency($(this).val());
            //    //document.getElementById("CustomerID").value = $(this).val();
            //    GetCustomerFrequency($(this).val());
               
            //});
        });

        function GetCustomerFrequency(id) {
            debugger
            var AssignmentObj = {
                CustomerID: id
            }
            $.ajax({
                type: "POST",
                url: "./RLCSDocumentRepository.aspx/GetCustomerFrequency",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                
                success: function (data) {
                    console.log(data);
                    var Freq = JSON.parse(data.d);
                    alert(Freq);
                    if (Freq == "A") {
                        $("#txtFequency").val("Annually");
                        //$("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "H") {
                        $("#txtFequency").val("Half Yearly");
                        //$("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "Q") {
                        $("#txtFequency").val("Quarterly");
                        //$("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "M") {
                        $("#txtFequency").val("Monthly");
                        //$("#divFrequency").addClass("hidden");
                        BindPeriods(Freq);
                    }
                    if (Freq == "O") {
                        $("#txtFequency").val("OnOccurence");
                       // $("#divFrequency").removeClass("hidden");
                        $("#ddlFrequency").val("-1");
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindPeriods(Frq) {
            debugger
            $('#ddlPeriod').empty();
            if (Frq == "C") {
                FrequencyId = "5";
            }
            if (Frq == "Q") {
                $("#ddlPeriod").append($("<option></option>").val("Jan - Mar").html("Jan - Mar"));
                $("#ddlPeriod").append($("<option></option>").val("Apr - Jun").html("Apr - Jun"));
                $("#ddlPeriod").append($("<option></option>").val("Jul - Sep").html("Jul - Sep"));
                $("#ddlPeriod").append($("<option></option>").val("Oct - Dec").html("Oct - Dec"));
                FrequencyId = "4";
            }
            if (Frq == "A" || Frq == "Y") {
                $("#ddlPeriod").append($("<option></option>").val("Jan - Dec").html("Jan - Dec"));
                FrequencyId = "3";
            }
            if (Frq == "H") {
                $("#ddlPeriod").append($("<option></option>").val("Jan - Jun").html("Jan - Jun"));
                $("#ddlPeriod").append($("<option></option>").val("Jul - Dec").html("Jul - Dec"));
                FrequencyId = "2";
            }
            if (Frq == "M") {
                $("#ddlPeriod").append($("<option></option>").val("Jan").html("Jan"));
                $("#ddlPeriod").append($("<option></option>").val("Feb").html("Feb"));
                $("#ddlPeriod").append($("<option></option>").val("Mar").html("Mar"));
                $("#ddlPeriod").append($("<option></option>").val("Apr").html("Apr"));
                $("#ddlPeriod").append($("<option></option>").val("May").html("May"));
                $("#ddlPeriod").append($("<option></option>").val("Jun").html("Jun"));
                $("#ddlPeriod").append($("<option></option>").val("Jul").html("Jul"));
                $("#ddlPeriod").append($("<option></option>").val("Aug").html("Aug"));
                $("#ddlPeriod").append($("<option></option>").val("Sep").html("Sep"));
                $("#ddlPeriod").append($("<option></option>").val("Oct").html("Oct"));
                $("#ddlPeriod").append($("<option></option>").val("Nov").html("Nov"));
                $("#ddlPeriod").append($("<option></option>").val("Dec").html("Dec"));
                FrequencyId = "1";
            }

            $("#ddlPeriod").fSelect('reload');
        }

        function addUploadfile(e) {
            debugger;
            var ActID = e;
            //var ScheduleOnID = f;
            var ContractorID = $("#ContentPlaceHolder1_ddlContractor").val();
            var State = $("#ContentPlaceHolder1_ddlState").val();
            var location = $("#ContentPlaceHolder1_ddlLocationFilter").val();
            var Branch = $("#ContentPlaceHolder1_ddlBranchFilter").val();
            var StateOrCentral = $("#ContentPlaceHolder1_ddlCentralOrState").val();
            var StateFilter = $("#ContentPlaceHolder1_ddlStateFilter").val();
            <%--var YearFilter = $("#ContentPlaceHolder1_ddlYear").val();
            var PeriodFilter = '<%=SchedulePeriod%>';--%>

          
            
            if (ActID != undefined || ActID != null || ContractorID != undefined || ContractorID != null) {
                $('#divUploadFilesAddDialog').show();
                var myWindowAdv = $("#divUploadFilesAddDialog");
                myWindowAdv.kendoWindow({
                    width: "80%",
                    height: '90%',
                    title: "Upload Files",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                //$('#iframeAdd').attr('src', "../RLCSVendorAudit/RLCS_AddUploadFiles.aspx?ActID=" + ActID + "&ScheduleOnID=" + ScheduleOnID + "&ContractorID=" + ContractorID + "&State=" + State + "&location=" + location + "&Branch=" + Branch + "&StateOrCentral=" + StateOrCentral + "&StateFilter=" + StateFilter + "&YearFilter=" + YearFilter + "&PeriodFilter=" + PeriodFilter);

                $('#iframeAdd').attr('src', "../RLCSVendorAudit/RLCS_AddUploadFiles.aspx?ActID=" + ActID  + "&ContractorID=" + ContractorID + "&State=" + State + "&location=" + location + "&Branch=" + Branch + "&StateOrCentral=" + StateOrCentral + "&StateFilter=" + StateFilter);
            }
            return false;

        }

        function onClose() {
            debugger;
            <%--document.getElementById('<%= lnkbtnRefresh.ClientID %>').click();--%>
        }

        function BindControls() {

            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
        }
        function fopenpopup() {
            $('#divOpenNewFolderPopup').modal('show');
        }
        function fclosepopup() {
            $('#divOpenNewFolderPopup').modal('hide');
        }
        function fopenActMaster() {
            $('#divActMasterPopup').modal('show');
        }
        $('select#ContentPlaceHolder1_ddlTypeofChecklist').change(function () { if ($('select#ContentPlaceHolder1_ddlTypeofChecklist').val() == 'CustomerWise') { $('#ddlCrr1').show(); } else { $('#ddlCrr1').hide(); } });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 250px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        </asp:Panel>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-lg-12 col-md-12">
                        <div runat="server" id="divCase">


                            <div class="col-md-12 colpadding0" style="border: 1px solid #ccc;height: 100px;border-radius: 9px;padding:9px;">
                               <div class="col-md-1 colpadding0" style="width: 10%; margin-right: 1.5%;display:none;" >
                                    <asp:DropDownListChosen runat="server" ID="ddlTypeofChecklist"
                                        DataPlaceHolder="Type of Checklist"   Visible="false"
                                        Height="30px" Width="100%" class="form-control m-bot15" >
                                        <asp:ListItem Value="Default" Text="Default"></asp:ListItem>
                                        <asp:ListItem Value="CustomerWise" Text="CustomerWise"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                </div>

                                <asp:HiddenField ID="hdn_ScheduleOnID" runat="server" />

                               <div class="col-md-1 colpadding0" style="width: 20%; margin-right: 1.5%;" id="ddlCrr1"><%-- display:none;--%>
                                    <asp:DropDownListChosen runat="server" ID="ddlCustomer"
                                        DataPlaceHolder="Select Customer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true">
                                        
                                        </asp:DropDownListChosen>
                                   <%-- OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"--%>
                                </div>

                               
                                <div class="col-md-2 colpadding0">
                                    <asp:DropDownListChosen runat="server" ID="ddlState"
                                        DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true" />
                                </div>
                                

                                 <div class="col-md-1 colpadding0" style="width: 18%;margin-right: 0%;margin: 0px 0px 0px 12px;"><%-- display:none;--%>
                                    <asp:DropDownListChosen runat="server" ID="ddlLocationFilter" 
                                        DataPlaceHolder="Select Location" Visible="true"
                                        Height="30px" Width="100%" class="form-control m-bot15" >
                                        
                                        </asp:DropDownListChosen>
                                   <%-- OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"--%>
                                </div>

                                 <div class="col-md-1 colpadding0" style="width: 18%;margin-right: 0%;margin: 0px 0px 0px 12px;"><%-- display:none;--%>
                                    <asp:DropDownListChosen runat="server" DataPlaceHolder="Select Branch" ID="ddlBranchFilter" AutoPostBack="true"
                                         Visible="true" OnSelectedIndexChanged="ddlBranchFilter_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" >
                                        </asp:DropDownListChosen>
                                   <%-- OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"--%>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 18%;margin: 0px 15px;" id="ddlCntr"><%-- display:none;--%>
                                    <asp:DropDownListChosen runat="server" ID="ddlContractor" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged"
                                        DataPlaceHolder="Select Contractor" Visible="true" AutoPostBack="true"
                                        Height="30px" Width="111%" class="form-control m-bot15" >
                                        
                                        </asp:DropDownListChosen>
                                   <%-- OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"--%>
                                </div>

                                 <div class="col-md-1 colpadding0" style="width: 18%;margin: 0px 15px; display:none"><%-- display:none;--%>
                                    <asp:TextBox runat="server" ID="hdAuditorID"  AutoPostBack="true"
                                        Height="30px" Width="111%" class="form-control m-bot15" >
                                        
                                        </asp:TextBox>
                                   <%-- OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"--%>
                                </div>

                                 <div class="col-md-1 colpadding0" style="margin: 10px 13px 0px 0px;" >
                                    <asp:DropDownListChosen runat="server" ID="ddlCentralOrState" OnSelectedIndexChanged="ddlCentralOrState_SelectedIndexChanged1"
                                        DataPlaceHolder="Select" 
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true">
                                        <asp:ListItem Value="Central" Text="Central"></asp:ListItem>
                                        <asp:ListItem Value="State" Text="State"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0" style="width: 18%;margin: 10px 15px; ">
                                    <asp:DropDownListChosen runat="server" ID="ddlStateFilter"
                                        DataPlaceHolder="Select State"
                                        Height="30px" Width="80%" class="form-control m-bot15" AutoPostBack="true" />
                                </div>

                                <div class="col-md-2 colpadding0" style="width: 18%;margin: 10px 5px; display:none" id="divFrequency">
                                    <asp:DropDownListChosen ID="ddlFrequency" class="form-control m-bot15"  Height="30px" Width="100%"  runat="server" 
                                        DataPlaceHolder="Frequency" AutoPostBack="true"/>
                                </div>
                                <div class="col-md-2 colpadding0" style="width: 18%;margin: 10px -25px; display:none"">
                                    <asp:TextBox type="text" id="txtFequency" class="form-control m-bot15"  Height="30px" Width="80%" ReadOnly="true" runat="server" AutoPostBack="true"/>

                                    <input type="hidden" id="Frequency" name="Frequency" runat="server" />
                                </div>

                                 <div class="col-md-1 colpadding0" style="width: 10%;margin: 10px 0px; display:none"">
                                    <asp:DropDownListChosen runat="server" ID="ddlYear" AutoPostBack="true"
                                        DataPlaceHolder="Select Year" Visible="true" 
                                        Height="30px" Width="111%" class="form-control m-bot15" >
                                        
                                        </asp:DropDownListChosen>
                                </div>

                                 <div class="col-md-1 colpadding0" style="width: 10%;margin: 10px 15px; display:none"">
                                    <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                        DataPlaceHolder="Select Period" Visible="true" 
                                        Height="30px" Width="111%" class="form-control m-bot15" >
                                        
                                        </asp:DropDownListChosen>

                                     <%--<select id="ddlPeriod" class="form-control">--%>
                                    </select>
                                </div>

                                <div class="col-md-3 colpadding0" style="color: black; margin:14px 37px 0">
                                    <input type="checkbox" id="customerSpecific" runat="server" visible="true"/>
                                    Show Client Specific Checklist
                   
                                </div>
                                 <div class="col-md-4 colpadding0 k-align-right" style="color: black; margin:-60px 10px -40px 886px">
                                    <br />
                                    <asp:Button id="btnApply" runat="server" OnClick="btnApply_Click" Text="Apply" class="btn btn-primary "/>
                                    
                                     <asp:Button id="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" class="btn btn-primary "/>
                                </div>
                                <div class="col-md-4 colpadding0" style="margin-left: 1.5%;display:none">
                                    <asp:DropDownListChosen runat="server" ID="ddlActFilter"
                                        DataPlaceHolder="Select Act" 
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true" />
                                </div>
                             
                            </div>
                            <div style="clear:both;height:20px;">&nbsp;</div>
                           
                        </div>
                    </div>


                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="col-md-12 colpadding0">
        <asp:UpdatePanel ID="upUploadAuditChecklist" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView runat="server" ID="grdChecklistDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    OnRowCommand="grdChecklistDetails_RowCommand" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr. No." HeaderStyle-Font-Bold="true" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID" ItemStyle-Width="5%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label ID="txtID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Id") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State Name" HeaderStyle-Font-Bold="true" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StateName") %>' ToolTip='<%# Eval("StateName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Act ID" ItemStyle-Width="5%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label ID="lblActID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActID") %>' ToolTip='<%# Eval("ActID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Act Name" HeaderStyle-Font-Bold="true" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nature Of Compliance" HeaderStyle-Font-Bold="true" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 330px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NatureOfCompliance") %>' ToolTip='<%# Eval("NatureOfCompliance") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Upload" HeaderStyle-Font-Bold="true" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                    <u>
                                          <asp:LinkButton id="uploadFiles"  runat="server" CommandName="Upload_Checklist" data-toggle="tooltip" data-placement="bottom" CommandArgument='<%# Eval("Id")%>' ToolTip="Upload Files" ><img src="../Images/Upload%20files%20Icon1.png" /></asp:LinkButton> <%--CommandArgument='<%# Eval("CheckListMasterId") + "," + Eval("ScheduleONID")%>'-- %>
                                        </u>
                                    <%--<asp:LinkButton id="uploadFiles" runat="server" CommandName="Upload_Checklist" ToolTip="Upload Checklist" data-toggle="tooltip" CommandArgument='<%# Eval("Id") %>'><u>Upload FIles</u></asp:LinkButton>"return addUploadfile(this); return false;"--%>
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" ></asp:Label>
                                    
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                      

                        <%-- <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TypeOfCompliance") %>' ToolTip='<%# Eval("TypeOfCompliance") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Risk" ItemStyle-Width="5%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 45px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Risk") %>' ToolTip='<%# Eval("Risk") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        

                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="col-md-12 colpadding0">
        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
    </div>


    <div class="modal fade" id="divOpenNewFolderPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Upload CheckList</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upMailDocument" runat="server">
                        <ContentTemplate>
                             <div class="form-group required col-md-12">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Nature of Compliance</label>
                                        <asp:TextBox runat="server" ID="txtNatureOfCompliance" CssClass="form-control" />
                                        
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Section</label>
                                        <asp:TextBox runat="server" ID="txtSection" CssClass="form-control" />
                                    </div>
                                </div>

                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divActMasterPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 75%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Act Master</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%; max-height: 500px; overflow-y: auto;">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                            <div class="row">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-1 colpadding0" style="width: 10%; margin-right: 1.5%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlStateFilterMaster"
                                            DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlStateFilterMaster_SelectedIndexChanged"
                                            Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true">
                                            <asp:ListItem Value="Central" Text="Central" Selected></asp:ListItem>
                                            <asp:ListItem Value="State" Text="State"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-2 colpadding0" style="width: 30%; margin-right: 1.5%;">

                                        <asp:DropDownListChosen runat="server" ID="ddlStateFilter1"
                                            DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlStateFilter1_SelectedIndexChanged"
                                            Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true" />
                                    </div>
                                    <div class="col-md-7 colpadding0">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <asp:GridView runat="server" ID="grdActMaster"
                                    AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act ID" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActID") %>' ToolTip='<%# Eval("AM_ActID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Width="25%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act Group" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActGroup") %>' ToolTip='<%# Eval("AM_ActGroup") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State ID" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StateID") %>' ToolTip='<%# Eval("StateID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <div id="divUploadFilesAddDialog">
                        <div class="chart-loading colpadding0" id="loader1"></div>
                        <iframe id="iframeAdd" style="width: 100%; height: 80%; border: none"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
