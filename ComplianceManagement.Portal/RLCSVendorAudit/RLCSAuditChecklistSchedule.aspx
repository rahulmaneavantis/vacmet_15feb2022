﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSAuditChecklistSchedule.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSAuditChecklistSchedule" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../assets/fSelect.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-multiselect.js"></script>
    <script src="../assets/fSelect.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <%--<script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>
    <style type="text/css">
        .k-grid-content {
            max-height: 250px;
        }

        .Mar-Top15 {
            margin-top: 15px;
        }

        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .MarRight {
            margin-right: 20px;
        }

        .form-control {
            width: 100%;
            height: 28px;
            padding: 2px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #8e8e93;
            background-color: #fff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 100%;
        }

        .AddNewspan1 {
            padding: 5px;
            border: 2px solid #f4f0f0;
            border-radius: 51px;
            display: inline-block;
            height: 26px;
            width: 26px;
        }

        .modal-header-custom {
            display: block;
            float: left;
            font-size: 20px;
            color: #1fd9e1; /*width: 225px;*/
            font-weight: 400;
        }

        #DivVenoder > div.fs-wrap {
            width: 245px;
        }

        #divLocation > div.fs-wrap {
            width: 237px;
        }

        #divbranch > div.fs-wrap {
            width: 237px;
        }
    </style>
    <script type="text/javascript">
        var AssignScheduleAuditListData = [];
        window.onunload = function () { };
        $(document).ready(function () {
            binddate();
            setactivemenu('leftnoticesmenu');
            fhead('Assign Schedule');
            MultiSelectDropdown();
            BindCustomerList();
            BindEntityList(0);
            BindEntityStateList(0);
            BindEntityLocationList([0]);
            BindScheduleAuditList();
            BindEntityBranchList([0]);
            //Bindview();
            $("#ddlCustomer").on("change", function (e) {               
                BindEntityStateList($(this).val());
                BindEntityBranchList([0]);
                $("#ddlLocation").empty();
                $("#ddlVendor").empty();
                $("#ddlVendor").fSelect('reload');
                $("#ddlLocation").fSelect('reload');
            });

            $("#ddlEntity").on("change", function (e) {
                $("#ddlEntityState").empty();
                $("#ddlLocation").empty();
                $("#ddlBranch").empty();
                BindEntityStateList($(this).val());
                //BindEntityBranchList([0]);
            });

            $("#ddlEntityState").on("change", function (e) {               
                $("#ddlLocation").empty();
                if ('<%= ContractCustomer %>' == 'False') {
                    $("#ddlBranch").empty();
                }
                BindEntityLocationList($(this).val());
            });

            $("#ddlLocation").on("change", function (e) {
                BindEntityBranchList($(this).val());
            });
            $("#ddlBranch").on("change", function (e) {
                
                BindVendorList($(this).val());
            });

          
            $("#btnApply").on("click", function (e) {
                var VendorList = $("#ddlVendor").val();
                var Customer = $("#ddlCustomer").val();
                if (Customer == "-1") {
                    alert("Please select Customer");
                }
                else if (VendorList == null) {
                    alert("Please select at least One Contractor")
                }
                else {
                    BindScheduleAuditList();
                }
            });

            $("#btnClear").on("click", function (e) {
                BindCustomerList();
                BindEntityStateList(0);
                BindEntityLocationList(0);
                $("#ddlVendor").empty();
                $("#ddlVendor").fSelect('reload');
                $("#ddlLocation").empty();
                $("#ddlLocation").fSelect('reload');
                $("#ddlBranch").empty();
                $("#ddlBranch").fSelect('reload');
            });

            //$('#tblScheduleAuditorList').on('click', '.chkbxComplianceAssign', function () {
            //    debugger
            //    var AssignScheduleDetailsObjNew = {
            //        AssignScheduleDetails: AssignScheduleAuditListData
            //    }

            //    $.each(AssignScheduleDetailsObjNew.AssignScheduleDetails, function (i) {
            //        $.each(AssignScheduleDetailsObjNew.AssignScheduleDetails[i], function (key, val) {
                        
            //            if (key === "IsOffline")
            //            {
            //                alert("Etneter " + val);
            //                val = true;
            //            }
            //        });
            //    });
            //    alert(JSON.stringify(AssignScheduleDetailsObjNew));
            //});

            $("#btnAssignScheduleAudit").click(function (e) {
                debugger
                
               
                var gridData = $("#tblScheduleAuditorList").data("kendoGrid");
                console.log(gridData);
                ProceedClick();
                               
                debugger
                if (AssignScheduleAuditListData.length > 0) {
                    var AssignScheduleDetailsObj = {
                        AssignScheduleDetails: AssignScheduleAuditListData
                    }
                    $.ajax({
                        type: "POST",
                        url: "./RLCSAuditChecklistSchedule.aspx/AssignScheduleAudit",
                        data: JSON.stringify({ AssignScheduleDetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data)
                                alert("Selected Scheduled Audit Assign Successfully.");

                            BindScheduleAuditList();
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
                else {
                    AssignCheckListValidation();
                }
            });

            $("#btnRecreateAudit").click(function (e) {
                if ($.trim($("#txtInvoiceNo").val()) != "") {
                    var ReCreateAuditDetailsObj = {
                        ScheduleID: $("#lblScheduleID").text(),
                        InVoiceNo: $("#txtInvoiceNo").val()
                    }
                    $.ajax({
                        type: "POST",
                        url: "./RLCSAuditChecklistSchedule.aspx/ReCreateAuditSave",
                        data: JSON.stringify({ ReCreateAuditDetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success == "InvoiceExist") {
                                alert("Invoice Already Exist");
                                $("#txtInvoiceNo");
                            }
                            else if (Success) {
                                alert(" Audit Create Successfully.");
                                BindScheduleAuditList();
                                $("#txtInvoiceNo").val("");
                                $("#RecreateAuditModal").modal("hide");
                            }

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
                else {
                    alert("Please Enter Invoice Number!");
                }
            });


            $(document).on("click", "#tblScheduleAuditorList tbody tr .ReCreate", function (e) {

                var item = $("#tblScheduleAuditorList").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblScheduleID").text(item.ID);
                var CustomerName = item.Customer;
                var Branch = item.Location;
                var ContractorName = item.VendorName;
                var AuditPeriod = item.ForMonth;
                $("#lblConstumerName").text(CustomerName);
                $("#lblBranch").text(Branch);
                $("#lblContractorName").text(ContractorName);
                $("#lblAuditPeriod").text(AuditPeriod);
                $("#RecreateAuditModal").modal("show");
            });
        });

      <%--   function Bindview()
        {
            if ('<%= ContractCustomer %>' == 'False') 
            {
                ddlstatediv.style.display = "block";
                divLocation.style.display = "block";--%>
        //    }
        //    else
        //    {
        //        ddlstatediv.style.display = "none";
        //        divLocation.style.display = "none";
        //    }
        //}


        function ProceedClick() {
            debugger
            //$("#tblScheduleAuditorList").data("kendoGrid");
            AssignScheduleAuditListData = [];
            
            var gridMapping = $('#tblScheduleAuditorList').data().kendoGrid;
            var gridData = gridMapping.dataSource.data();
            var ScheduleOnID = "";
            var AuditID = "";
            var BranchID = "";
            var AuditorID = "";
            var VendorID = "";
            var ScheduleOn = "";
            var PONO = "";
            var InvoiceNo = "";
            var IsOffline = "false";
            var BranchIDList = "";
            var strBranchIDList1 = "";

            var rows = gridMapping.select();
            
            rows.each(function (index, row) {
                var selectedItem = gridMapping.dataItem(row);
                var AssignScheduleDetails = {
                    ScheduleOnID: selectedItem.AuditScheduleOnID,
                    AuditID: selectedItem.AuditID,
                    BranchID: selectedItem.BranchID,
                    AuditorID: selectedItem.AVACOM_AuditorID,
                    VendorID: selectedItem.AVACOM_VendorID,
                    ScheduleOn: selectedItem.StartDate,
                    PONO: selectedItem.PONO,
                    InvoiceNo: selectedItem.InVoiceNo,
                    IsOffline: selectedItem.IsOffline == null ? false : selectedItem.IsOffline
                }
                AssignScheduleAuditListData.push(AssignScheduleDetails)

            });
        }

        function BindCustomerList() {
            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindAuditCustomerList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Customer = JSON.parse(data.d);
                    $("#ddlCustomer").empty();
                    $("#ddlCustomer").append($("<option></option>").val("-1").html("Select Principal Employer"));
                    $.each(Customer, function (data, value) {
                        $("#ddlCustomer").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlCustomer").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityList(id) {
            var AssignmentObj = {
                CustomerID: id
            }
            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindAuditEntityList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Entity = JSON.parse(data.d);
                    $('#ddlEntity').empty();
                    $("#ddlEntity").append($("<option></option>").val("-1").html("Select Entity"));
                    $.each(Entity, function (data, value) {
                        $("#ddlEntity").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlEntity").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityStateList(id) {
            var AssignmentObj = {
                EntityID: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindAuditEntityStateList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Entity = JSON.parse(data.d);
                    $('#ddlEntityState').empty();
                    $("#ddlEntityState").append($("<option></option>").val("-1").html("Select State"));
                    $.each(Entity, function (data, value) {
                        $("#ddlEntityState").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlEntityState").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityLocationList(id) {
            var AssignmentObj = {
                StateID: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindAuditEntityLocationList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Location = JSON.parse(data.d);
                    $('#ddlLocation').empty();
                    $.each(Location, function (data, value) {
                        $("#ddlLocation").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlLocation").fSelect('reload');
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindVendorList(id) {
            debugger;
            var AssignmentObj = {
                Branches: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindAuditVendorList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Vendors = JSON.parse(data.d);

                    $("#ddlVendor").empty();
                    $.each(Vendors, function (data, value) {
                        $("#ddlVendor").append($("<option></option>").val(value.ID).html(value.ContractorName));
                    })
                    $("#ddlVendor").fSelect('reload');
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindEntityBranchList(id) {

            var AssignmentObj = {
                LocationID: id,
                CustomerID: $("#ddlCustomer").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindEntityBranchList",
                data: JSON.stringify({ AssignmentObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Location = JSON.parse(data.d);
                    $('#ddlBranch').empty();
                    $.each(Location, function (data, value) {
                        $("#ddlBranch").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlBranch").fSelect('reload');
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function MultiSelectDropdown() {
            $('#ddlVendor').attr("multiple", "multiple");
            $('#ddlVendor').fSelect({ placeholder: "Select Contractor" });
            $('#ddlLocation').attr("multiple", "multiple");
            $('#ddlLocation').fSelect({ placeholder: "Select Location" });
            $('#ddlBranch').attr("multiple", "multiple");
            $('#ddlBranch').fSelect({ placeholder: "Select Branch" });
        }

        function BindScheduleAuditList() {
            var AuditListFilterObj = {
                VendorIDs: $("#ddlVendor").val(),
                Branches: $("#ddlBranch").val()
            }

            $.ajax({
                type: "POST",
                url: "./RLCSAuditChecklistSchedule.aspx/BindScheduleAuditList",
                data: JSON.stringify({ AuditListFilterObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var AssignCheckList = JSON.parse(data.d);

                    $("#tblScheduleAuditorList").kendoGrid({
                        dataSource: {
                            data: AssignCheckList,
                            schema: {
                                model: {
                                    fields: {
                                        Customer: { editable: false },
                                        Location: { editable: false },
                                        VendorName: { editable: false },
                                        AuditorName: { editable: false },
                                        VendorName: { editable: false },
                                        ForMonth: { editable: false },
                                        StartDate: { editable: true },
                                        PONO: { editable: true },
                                        InVoiceNo: { editable: true },
                                        IsOffline: { editable: false },
                                    }
                                }
                            }
                        },
                        editable: true,
                        height: 300,
                        sortable: true,
                        filterable: true,
                        columnMenu: true,
                        reorderable: true,
                        resizable: true,
                        change: onChangeUnAssign,
                        //dataBound: onDataBound,
                        columns: [
                            { selectable: true, width: "5%" },
                            { hidden: true, field: "AuditScheduleOnID" },
                            { hidden: true, field: "AVACOM_VendorID" },
                            { hidden: true, field: "AVACOM_AuditorID" },
                            { hidden: true, field: "AuditID" },
                            { hidden: true, field: "BranchID" },
                            { field: "Customer", title: "Customer Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Location", title: "Branch", width: "15%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "VendorName", title: "Contractor", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "AuditorName", title: "Auditor", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "ForMonth", title: "Auditor Period", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            {
                                field: "StartDate", title: "Start Date", format: "{0:dd/MM/yyyy }", width: "20%",
                                
                                editor: function (container, options) {
                                    var input = $("<input/>");
                                    input.attr("name", options.field);
                                    input.attr("class", "datePicker");

                                    input.appendTo(container);

                                    input.kendoDatePicker({ format: "dd/MM/yyyy" });
                                }
                            },
                        {
                            field: "PONO", title: "PO No", width: "13%", attributes: { style: 'white-space: nowrap;' }, editor: function (container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                            }
                        },
                        {
                            field: "InVoiceNo", title: "Invoice No", width: "13%", attributes: { style: 'white-space: nowrap;' }, editor: function (container1, options1) {
                                var input = $("<input/>");
                                input.attr("name", options1.field);
                                input.appendTo(container1);
                            }
                        },
                        //{
                        //   field: "IsOffline", width: "20%", attributes: { style: 'white-space: nowrap;' },
                        //   template: "<input type='checkbox' class='checkbox' />", 
                        //   }

                         {
                             field: "IsOffline", template: "<input type='checkbox' #= IsOffline ? checked='checked':'' # class='chkbxComplianceAssign' id='chkOffline'/>", width: "20%"
                         },
                       
                        
                         {
                             title: "Action", lock: true, width: "18%;",
                             command: [{ name: "RecreateAudit", text: "ReCreate", className: "ReCreate" }
                             ],
                         }],
                    });

                    $("#tblScheduleAuditorList").kendoTooltip({
                        filter: "td",
                        position: "down",
                        content: function (e) {
                            debugger;
                            var content = e.target.context.textContent;
                            if (content != "") {
                                if (content.length > 1) {
                                    return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                                }
                                //else {
                                //    e.preventDefault();
                                //}
                            }
                            //else
                            //    e.preventDefault();
                        }
                    }).data("kendoTooltip");

                },
                failure: function (response) {
                    alert(response.d);
                }
            });

            
        }
        function onChangeUnAssign(arg) {
            debugger
            var rows = arg.sender.select();
            var AssignScheduleDetails = null;
            var dataItemUpdated = null;
            var IsOfflineChecked = false;
            var tblUnAssignRecord = $("#tblScheduleAuditorList").data("kendoGrid");
            var dataItem = tblUnAssignRecord.dataItem(rows);
            //AssignScheduleAuditListData = [];

            $('#tblScheduleAuditorList').on('click', '.chkbxComplianceAssign', function () {
                debugger
                var checked = $(this).is(':checked');
                var grid = $('#tblScheduleAuditorList').data().kendoGrid;
                dataItemUpdated = grid.dataItem($(this).closest('tr'));
                if (dataItemUpdated != undefined) {

                    if (checked == true) {
                        dataItemUpdated.IsOffline = true;
                    }
                    else {
                        dataItemUpdated.IsOffline = false;
                    }
                }

                //AssignScheduleAuditListData.push(dataItemUpdated);
            });

            //rows.each(function (e) {
            //    debugger
            //    var tblUnAssignRecord = $("#tblScheduleAuditorList").data("kendoGrid");
            //    var dataItem = tblUnAssignRecord.dataItem(this);
            //    var crntrow = $(this);
                
            //    var StartDate = crntrow[0].cells[11].innerText;
               
                
            //    AssignScheduleDetails = {
            //        ScheduleOnID: dataItem.AuditScheduleOnID,
            //        AuditID: dataItem.AuditID,
            //        BranchID: dataItem.BranchID,
            //        AuditorID: dataItem.AVACOM_AuditorID,
            //        VendorID: dataItem.AVACOM_VendorID,
            //        ScheduleOn: StartDate,
            //        PONO: dataItem.PONO,
            //        InvoiceNo: dataItem.InVoiceNo,
            //        IsOffline: dataItem.IsOffline
            //    };

            //        AssignScheduleAuditListData.push(AssignScheduleDetails);

            //    });
        }
        function binddate() {
            $("#headerStartdate").kendoDatePicker({
                format: "dd/MM/yyyy",
                change: onDindateChange
            });
        }
        function onDindateChange() {
            
            var theGrid = $("#tblScheduleAuditorList").data("kendoGrid");
            var gridData = theGrid.dataSource._data;
            for (var i = 0; i < gridData.length; i++) {
                gridData[i].StartDate = $("#headerStartdate").val();
            }
            $("#tblScheduleAuditorList").data("kendoGrid").refresh();
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <select id="ddlCustomer" class="form-control">
                    </select>
                    <input type="hidden" id="CustomerID" name="CustomerID" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 hidden">
                    <select class="form-control" id="ddlEntity">
                    </select>
                    <input type="hidden" id="EntityID" name="EntityID" />
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" id="ddlstatediv">
                    <select class="form-control" id="ddlEntityState">
                    </select>
                    <input type="hidden" id="EntityStateID" name="EntityStateID" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3" id="divLocation">
                    <select class="form-control" id="ddlLocation">
                    </select>
                    <input type="hidden" id="LocationID" name="LocationID" />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3" id="divbranch">
                    <select class="form-control" id="ddlBranch">
                    </select>
                </div>
            </div>

            <div class="row" style="margin-top: 15px;">
                <div class="col-lg-4 col-md-4 col-sm-4" id="DivVenoder">
                    <select id="ddlVendor" class="form-control">
                    </select>
                    <input type="hidden" id="VendorID" name="VendorID" />
                </div>
                  <div class="col-lg-2 col-md-2 col-sm-2">
                 <input type="date" id="headerStartdate" style="display:none" />
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="pull-right">
                        <button id="btnApply" type="button" class="btn btn-primary pull-left MarRight">Apply</button>
                        <button type="button" id="btnClear" class="btn btn-primary pull-left">Clear</button>
                    </div>
                </div>

            </div>

            <div class="row" style="margin-top: 15px;">
                <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 10px;padding-left: 650px;">
                   
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div id="tblScheduleAuditorList"></div>
                </div>
            </div>

            <div class="row" style="margin-top: 15px;">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="pull-right">
                        <button id="btnAssignScheduleAudit" type="button" class="btn btn-primary pull-left">Schedule Audit</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal  modal-fullscreen" id="RecreateAuditModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Audit</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;" id="lblConstumerName"></label>
                                <label style="color: black;" id="lblScheduleID" class="hidden"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;" id="lblBranch"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;" id="lblContractorName"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;" id="lblAuditPeriod"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Invoice No</label>
                                <input type="text" id="txtInvoiceNo" class="form-control" />

                            </div>
                        </div>
                       <%-- <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">IsOffline</label>
                                <input type="checkbox" id="chkOffline" class="form-control" />
                            </div>

                        </div>--%>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnRecreateAudit" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
