﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSUploadChecklist.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSUploadChecklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .aspNetDisabled {
            width: 100%;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #8e8e93;
            background-color: #fff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftuploadmenu');
            fhead('Checklist Master');
            $('select#ContentPlaceHolder1_ddlTypeofChecklist').change(function () { if ($('select#ContentPlaceHolder1_ddlTypeofChecklist').val() == 'CustomerWise') { $('#ddlCrr1').show(); } else { $('#ddlCrr1').hide(); }  });
        });

        function fopenpopup() {
            $('#divOpenNewFolderPopup').modal('show');
        }
        function fclosepopup() {
            $('#divOpenNewFolderPopup').modal('hide');
        }
        function fopenActMaster() {
            $('#divActMasterPopup').modal('show');
        }
        $('select#ContentPlaceHolder1_ddlTypeofChecklist').change(function () { if ($('select#ContentPlaceHolder1_ddlTypeofChecklist').val() == 'CustomerWise') { $('#ddlCrr1').show(); } else { $('#ddlCrr1').hide(); } });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 250px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        </asp:Panel>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-lg-12 col-md-12">
                        <div runat="server" id="divCase">


                            <div class="col-md-12 colpadding0" style="border: 1px solid #ccc;height: 49px;border-radius: 9px;padding:9px;">
                               <div class="col-md-1 colpadding0" style="width: 10%; margin-right: 1.5%;display:none;" >
                                    <asp:DropDownListChosen runat="server" ID="ddlTypeofChecklist"
                                        DataPlaceHolder="Type of Checklist"   Visible="false"
                                        Height="30px" Width="100%" class="form-control m-bot15" >
                                        <asp:ListItem Value="Default" Text="Default"></asp:ListItem>
                                        <asp:ListItem Value="CustomerWise" Text="CustomerWise"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                </div>
                               <div class="col-md-1 colpadding0" style="width: 20%; margin-right: 1.5%;" id="ddlCrr1"><%-- display:none;--%>
                                    <asp:DropDownListChosen runat="server" ID="ddlCustomer"
                                        DataPlaceHolder="Select Customer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true">
                                        
                                        </asp:DropDownListChosen>
                                   <%-- OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"--%>
                                </div>
                                <div class="col-md-1 colpadding0" style="width: 10%; margin-right: 1.5%;" >
                                    <asp:DropDownListChosen runat="server" ID="ddlCentralOrState"
                                        DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlCentralOrState_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true">
                                        <asp:ListItem Value="Central" Text="Central"></asp:ListItem>
                                        <asp:ListItem Value="State" Text="State"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-2 colpadding0">
                                    <asp:DropDownListChosen runat="server" ID="ddlStateFilter"
                                        DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlStateFilter_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true" />
                                </div>
                                <div class="col-md-4 colpadding0" style="margin-left: 1.5%;">
                                    <asp:DropDownListChosen runat="server" ID="ddlActFilter"
                                        DataPlaceHolder="Select Act" OnSelectedIndexChanged="ddlActFilter_SelectedIndexChanged"
                                        Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true" />
                                </div>
                             
                            </div>
                            <div style="clear:both;height:20px;">&nbsp;</div>
                            <div class="col-md-12 colpadding0" style="border: 1px solid #ccc;height: 49px;border-radius: 9px;padding:9px;">
                                  <div class="col-md-3">
                                    <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                    <asp:RequiredFieldValidator ID="rfvFileUpload" ErrorMessage="Please Select File" ControlToValidate="MasterFileUpload"
                                        runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                                </div>
                                <div class="col-md-1 colpadding0 text-right">
                                    <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="uploadUtilityValidationGroup"
                                        class="btn btn-primary" OnClick="btnUploadFile_Click" OnClientClick="showProgress()" />

                                    <asp:Button ID="btnAddNew" runat="server" Text="Add New"
                                        class="btn btn-primary" OnClientClick="fopenpopup()" Visible="false" />
                                </div>
                                <div class="col-md-2 colpadding0 text-right">
                                    <asp:LinkButton ID="lnkSampleFormat" class="newlink" Font-Underline="True" OnClick="lnkSampleFormat_Click"
                                        data-toggle="tooltip" data-placement="bottom" runat="server" Text="Sample Format(Excel)"
                                        ToolTip="Download Sample Excel Document Format for Checklist Upload"></asp:LinkButton>
                                </div>
                                <div class="col-md-2 colpadding0 text-right">
                                     <asp:Button ID="btnActMaster" runat="server" Text="Act Master" 
                                        class="btn btn-primary" OnClick="btnActMaster_Click" OnClientClick="showProgress()" />
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="lnkSampleFormat" />
        </Triggers>
    </asp:UpdatePanel>


    <div class="col-md-12 colpadding0">
        <asp:UpdatePanel ID="upUploadAuditChecklist" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView runat="server" ID="grdChecklistDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    OnRowCommand="grdChecklistDetails_RowCommand" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State Name" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SM_Name") %>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Act ID" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActID") %>' ToolTip='<%# Eval("ActID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nature Of Compliance" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 230px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NatureOfCompliance") %>' ToolTip='<%# Eval("NatureOfCompliance") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField><asp:TemplateField HeaderText="Type" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TypeOfCompliance") %>' ToolTip='<%# Eval("TypeOfCompliance") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Risk" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 45px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Risk") %>' ToolTip='<%# Eval("Risk") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                            <ItemTemplate>

                                <asp:LinkButton ID="lnkEditChecklist" runat="server" CommandName="Edit_Checklist" ToolTip="Edit Checklist" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("Id") %>'>
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Checklist" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="col-md-12 colpadding0">
        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
    </div>


    <div class="modal fade" id="divOpenNewFolderPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Update Checklist Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upMailDocument" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="FolderValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsFolderDocumentValidationGroup" />
                                    <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">


                                <div class="form-group required col-md-12">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">Act</label>
                                    <asp:Label ID="lblAct" runat="server" Text="" Style="width: 88%; font-size: 13px; color: #333;"></asp:Label>

                                </div>
                            <div class="form-group required col-md-12">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">State</label>
                                <asp:Label ID="lblState" runat="server" Text="" Style="width: 88%; font-size: 13px; color: #333;"></asp:Label>
                            </div>

                                <div class="form-group required col-md-12">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Nature of Compliance</label>
                                        <asp:TextBox runat="server" ID="txtNatureOfCompliance" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="Foldermsg" ErrorMessage="Required Nature Of Compliance"
                                            ControlToValidate="txtNatureOfCompliance" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Section</label>
                                        <asp:TextBox runat="server" ID="txtSection" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required col-md-12">

                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Rule</label>
                                        <asp:TextBox runat="server" ID="txtCheckListRule" CssClass="form-control" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Form</label>
                                        <asp:TextBox runat="server" ID="txtForm" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required col-md-12">

                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Type Of Compliance</label>
                                        <asp:TextBox runat="server" ID="txtTypeOfCompliance" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required Type Of Compliance"
                                            ControlToValidate="txtTypeOfCompliance" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Frequency</label>
                                        <asp:TextBox runat="server" ID="txtFrequency" CssClass="form-control" />
                                    </div>
                                </div>
                                 <div class="form-group required col-md-12">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Risk</label>
                                        <asp:DropDownList runat="server" ID="ddlrisk" AutoPostBack="true" class="form-control m-bot15">
                                            <asp:ListItem Text="Select Risk" Value="-1" Selected="True"/>
                                            <asp:ListItem Text="High"  Value="0"/>
                                            <asp:ListItem Text="Medium"  Value="1"/>
                                            <asp:ListItem Text="Low"  Value="2" />
                                        </asp:DropDownList>                                       
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                </div>                                
                                <div class="form-group required col-md-12" style="display: none;">
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Type</label>
                                        <asp:TextBox runat="server" ID="txtType" CssClass="form-control" />
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                </div>
                                 <div class="form-group required col-md-12" >
                                    <div class="form-group col-md-6">
                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Description</label>
                                        <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control" />
                                    </div>
                                    <div class="form-group col-md-6">
                                          <label style="                                                  width: 3%;
                                                  display: block;
                                                  float: left;
                                                  font-size: 13px;
                                                  color: red;">&nbsp;</label>
                                        <label style="width: 50%; display: block; float: left; font-size: 13px; color: #333;">
                                            Consequences</label>
                                        <asp:TextBox runat="server" ID="txtConsequences" CssClass="form-control" />
                                    </div>
                                </div>
                                <asp:HiddenField ID="HiddenFieldchechecklistId" runat="server" Value="0" />
                                <asp:HiddenField ID="HiddenFieldactid" runat="server" Value="0" />
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12 text-center">
                                    <asp:Button Text="Update" runat="server" ID="btnUpdateChecklist" CssClass="btn btn-primary"
                                        ValidationGroup="vsFolderDocumentValidationGroup" OnClick="btnUpdateChecklist_Click"></asp:Button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divActMasterPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 75%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Act Master</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%; max-height: 500px; overflow-y: auto;">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                            <div class="row">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-1 colpadding0" style="width: 10%; margin-right: 1.5%;">
                                        <asp:DropDownListChosen runat="server" ID="ddlStateFilterMaster"
                                            DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlStateFilterMaster_SelectedIndexChanged"
                                            Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true">
                                            <asp:ListItem Value="Central" Text="Central" Selected></asp:ListItem>
                                            <asp:ListItem Value="State" Text="State"></asp:ListItem>
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-2 colpadding0" style="width: 30%; margin-right: 1.5%;">

                                        <asp:DropDownListChosen runat="server" ID="ddlStateFilter1"
                                            DataPlaceHolder="Select State" OnSelectedIndexChanged="ddlStateFilter1_SelectedIndexChanged"
                                            Height="30px" Width="100%" class="form-control m-bot15" AutoPostBack="true" />
                                    </div>
                                    <div class="col-md-7 colpadding0">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <asp:GridView runat="server" ID="grdActMaster"
                                    AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act ID" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActID") %>' ToolTip='<%# Eval("AM_ActID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Width="25%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act Group" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_ActGroup") %>' ToolTip='<%# Eval("AM_ActGroup") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State ID" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AM_StateID") %>' ToolTip='<%# Eval("AM_StateID") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>


</asp:Content>
