﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCS_ContractorMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCS_ContractorMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('Create Contract');
            BindContractorList(0);
            BindClientList();
            // BindContractor(0);
            $("#ddlClient").on("change", function (e) {
                BindContractorList($(this).val());
                $("#ddlContractor").val("-1").trigger("change");
                BindContractor($("#ddlContractor").val());
            });


            $("#btnNew").on("click", function (e) {
                if ($("#ddlClient").val() != "-1") {
                    $("#lblID").text("0");
                    $("#txtClientID").val($("#ddlClient").val());
                    $("#txtContractorName").val("");
                    $("#txtSPOCName").val("");
                    $("#txtSPOCEmailID").val("");
                    $("#txtSPOCMobileNo").val("");
                    $("#NewContractorModal").modal("show");
                }
                else {
                    alert("Please Select Principal Employer");
                }
            });
            $("#btnSave").click(function (e) {
                if (SaveValidation()) {
                    var DetailsObj = {
                        ID: $("#lblID").text(),
                        ContractorID: "",
                        ContractorName: $("#txtContractorName").val(),
                        SPOCName: $("#txtSPOCName").val(),
                        SPOCEmailID: $("#txtSPOCEmailID").val(),
                        SPOCMobileNo: $("#txtSPOCMobileNo").val(),
                        ClientID: $("#ddlClient").val()
                    }

                    $.ajax({
                        type: "POST",
                        url: "./RLCS_ContractorMaster.aspx/SaveContractDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Result = JSON.parse(data.d);
                            debugger;
                            if (Result.Success) {
                                alert(Result.Message);
                                $("#lblID").text("0");
                                $("#txtContractorName").val("");
                                $("#txtSPOCName").val("");
                                $("#txtSPOCEmailID").val("");
                                $("#txtSPOCMobileNo").val("");
                                $("#NewContractorModal").modal("hide");
                                BindContractorList($("#ddlClient").val());
                                BindContractor(0);
                            }
                            else {
                                alert(Result.Message);
                            }
                            
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
            });
            $(document).on("click", "#tblContractor tbody tr .Update", function (e) {
                var item = $("#tblContractor").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblID").text(item.ID);
                var DetailsObj = {
                    ID: item.ID
                }
                $.ajax({
                    type: "POST",
                    url: "./RLCS_ContractorMaster.aspx/GetContractorDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var ContractorDetails = JSON.parse(data.d);
                        if (ContractorDetails.length > 0) {
                            $("#txtClientID").val($("#ddlClient").val());
                            $("#txtContractorName").val(ContractorDetails[0].ContractorName);
                            $("#txtSPOCName").val(ContractorDetails[0].SPOCName);
                            $("#txtSPOCEmailID").val(ContractorDetails[0].SPOCEmailID);
                            $("#txtSPOCMobileNo").val(ContractorDetails[0].SPOCMobileNo);
                            $("#NewContractorModal").modal("show");
                        }
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });
            $(document).on("click", "#tblContractor tbody tr .MappingLocation", function (e) {
                var item = $("#tblContractor").data("kendoGrid").dataItem($(this).closest("tr"));
                window.location.href = "./RLCS_CreateAuditContract.aspx?ContractorID=" + item.ID + "&ContractorName=" + item.ContractorName + "&ClientID=" + $("#ddlClient").val() + "&ClientName=" + $("#ddlClient option:selected").text();
            });

            $("#ddlContractor").on("change", function (e) {
                BindContractor($(this).val());
            });

            $("#txtSPOCMobileNo").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Only Number");
                    return false;
                }
            });
        });
        function BindContractorList(id) {
            debugger;
            var DetailsObj = {
                ClientID: id
            }
            $.ajax({
                type: "POST",
                url: "./RLCS_ContractorMaster.aspx/BindContractorList",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Customer = JSON.parse(data.d);
                    if (Customer.length > 0) {
                        $("#ddlContractor").empty();
                        $("#ddlContractor").append($("<option></option>").val("-1").html("Select Contractor"));
                        $.each(Customer, function (data, value) {
                            $("#ddlContractor").append($("<option></option>").val(value.ID).html(value.ContractorName));
                        })
                        $("#ddlContractor").select2();
                    }
                    else {
                        $("#ddlContractor").empty();
                        $("#ddlContractor").append($("<option></option>").val("-1").html("Select Contractor"));
                        $("#ddlContractor").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindContractor(id) {
            debugger;
            var DetailsObj = {
                ID: id,
                ClientID: $("#ddlClient").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCS_ContractorMaster.aspx/BindContractor",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);
                    debugger;

                    $("#tblContractor").kendoGrid({
                        dataSource: {
                            data: AssignCheckList
                        },
                        height: 300,
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        columns: [
                            { hidden: true, field: "ID" },
                            { field: "ContractorName", title: "Contractor Name", width: "30%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "SPOCName", title: "SPOC Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "SPOCEmailID", title: "SPOC Email ID", width: "20%" },
                            { field: "SPOCMobileNo", title: "SPOC Mobile No", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             {
                                 title: "Action", lock: true, width: "30%;",
                                 command: [{ name: "UpdateContractor", text: "Update", className: "Update" },
                                 { name: "MappingLocation", text: "LocationMapping", className: "MappingLocation" }],
                             }
                        ]
                    });
                    $("#tblContractor").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function SaveValidation() {
            var MobileNo = $('#txtSPOCMobileNo').val();
            if ($.trim($('#txtContractorName').val()) == "") {
                alert("Please Enter Contractor Name");
                return false;
            }
            else if ($.trim($('#txtSPOCName').val()) == "") {
                alert("Please Enter SPOC Name");
                return false;
            }
            else if ($.trim($('#txtSPOCEmailID').val()) == "") {
                alert("Please Enter SPOC Mail ID");
                return false;
            }
            else if ($.trim($('#txtSPOCMobileNo').val()) == "") {
                alert("Please Enter SPOC Mobile Number");
                return false;
            }
            else if (MobileNo.length != "10") {
                alert("Mobile Number Should be 10 Digit");
                return false;
            }
            else {
                return true;
            }

        }

        function BindClientList() {
            $.ajax({
                type: "POST",
                url: "./RLCS_ContractorMaster.aspx/BindClientList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Customer = JSON.parse(data.d);
                    var value = "";
                    if (Customer.length > 0) {
                        $("#ddlClient").empty();
                        $("#ddlClient").append($("<option></option>").val("-1").html("Select Principal Employer"));
                        $.each(Customer, function (data, value) {
                            $("#ddlClient").append($("<option></option>").val(value.ClientID).html(value.ClientName + "[" + value.ClientID + "]"));
                        })
                        $("#ddlClient").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }
    </script>
    <style>
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }


        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <select id="ddlClient" class="form-control"></select>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <select id="ddlContractor" class="form-control"></select>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <button id="btnNew" type="button" class="btn btn-primary">Add New Contractor</button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                    <div id="tblContractor"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal  modal-fullscreen" id="NewContractorModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" id="divValidation">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Contractor</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row ">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Client ID</label>
                                <input type="text" id="txtClientID" class="form-control" readonly />
                                <label style="color: black;" id="lblID" class="hidden"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Contractor Name</label>
                                <input type="text" id="txtContractorName" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">SPOC Name</label>
                                <input type="text" id="txtSPOCName" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">SPOC Email ID</label>
                                <input type="text" id="txtSPOCEmailID" class="form-control" />

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">SPOC Mobile No</label>
                                <input type="text" id="txtSPOCMobileNo" class="form-control" maxlength="10" />

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
