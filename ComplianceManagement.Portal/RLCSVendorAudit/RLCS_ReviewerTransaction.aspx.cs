﻿using AjaxControlToolkit.Bundling;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_ReviewerTransaction : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                    && !string.IsNullOrEmpty(Request.QueryString["AID"])
                       && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                            && !string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    BindStatusList();
                    long CheckListID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long auditid = Convert.ToInt64(Request.QueryString["AID"].ToString());
                    long scheduleonid = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    int CheckListStatus = Convert.ToInt32(Request.QueryString["SID"]);
                    if (CheckListStatus == 2 || CheckListStatus == 3)
                    {
                        btnSave.Enabled = true;
                    }
                    else
                    {
                        btnSave.Enabled = false;
                    }
                    if (CheckListID > 0)
                    {
                        GetAllValues(CheckListID);
                        BindTransactions();
                        BindGrid();
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var getChecklist = (from row in entities.RLCS_Vendor_RecentCheckListStatus(auditid, CheckListID, scheduleonid)
                                                select row).FirstOrDefault();
                            if (getChecklist != null)
                            {
                                chkDocument.Checked = false;
                                if (getChecklist.IsDocument)                                
                                    chkDocument.Checked = true;
                                
                                if (!string.IsNullOrEmpty(Convert.ToString(getChecklist.ResultID)))
                                {
                                    ddlStatus.SelectedValue = Convert.ToString(getChecklist.ResultID);
                                }
                                txtTimeLine.Text = getChecklist.TimeLine != null ? getChecklist.TimeLine.Value.ToString("dd-MMM-yyyy") : null;                                
                            }
                        }
                    }                                        
                }
            }
        }

        private void BindStatusList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                        if (!string.IsNullOrEmpty(vrole))
                        {
                            var data = RLCSVendorMastersManagement.getRLCSResultStatus(vrole);
                            ddlStatus.DataTextField = "Name";
                            ddlStatus.DataValueField = "ID";
                            ddlStatus.DataSource = data;
                            ddlStatus.DataBind();
                            ddlStatus.Items.Insert(0, new ListItem("Select Status ", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetAllValues(long CheckListID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                    where row.IsDeleted == false && row.Id == CheckListID
                                    select row).FirstOrDefault();
                if (getChecklist != null)
                {
                    lblChecklistName.Text = getChecklist.NatureOfCompliance.ToString();
                    //lblDescription.Text = getChecklist.Description.ToString();
                    lblDescription.Text = string.Empty;
                    if (getChecklist.Description != null)
                    {
                        lblDescription.Text = getChecklist.Description.ToString();
                    }
                    LblState.Text = getChecklist.StateID.ToString();
                    LblSection.Text = getChecklist.Section.ToString();
                    LblChecklistRule.Text = getChecklist.Checklist_Rule.ToString();
                    LbltypeofCompliance.Text = getChecklist.TypeOfCompliance.ToString();
                }
            }
        }
        private void BindTransactions()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                   && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    grdTransactionHistory.DataSource = RLCSVendorMastersManagement.GetAllTransactionLog(ScheduledOnID, CheckListID);
                    grdTransactionHistory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindGrid()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                      && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    long ChecklistID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    long VendorID = Convert.ToInt64(Request.QueryString["VendorID"]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentData(ChecklistID, ScheduleOnID, VendorID);
                        if (lstChecklistDetails.Count > 0)
                        {
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        else
                        {
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRemark.Text) && !string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        if (ddlChecklistStatus.SelectedValue == "4" && ddlStatus.SelectedValue == "4")
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Can not save because of Status is Applicable.";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["CHID"])
                             && !string.IsNullOrEmpty(Request.QueryString["AID"])
                                && !string.IsNullOrEmpty(Request.QueryString["SOID"])
                             && !string.IsNullOrEmpty(Request.QueryString["CBID"]))
                            {
                                long CheckListID = Convert.ToInt32(Request.QueryString["CHID"].ToString());
                                long AuditId = Convert.ToInt32(Request.QueryString["AID"].ToString());
                                long ScheduleOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                                long branchID = Convert.ToInt32(Request.QueryString["CBID"].ToString());
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                    {
                                        AuditID = AuditId,
                                        VendorAuditScheduleOnID = ScheduleOnID,
                                        StatusId = Convert.ToInt32(ddlChecklistStatus.SelectedValue),
                                        Remarks = txtRemark.Text,
                                        Dated = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                        CreatedByText = AuthenticationHelper.User,
                                        CustomerBranchId = Convert.ToInt64(branchID),
                                        CheckListId = CheckListID,
                                        StatusChangedOn = DateTime.Now,
                                        ResultId = Convert.ToInt64(ddlStatus.SelectedValue),
                                        Recommendation = txtRecommendation.Text
                                    };
                                    if (!string.IsNullOrEmpty(txtTimeLine.Text))
                                    {
                                        transaction.TimeLine = Convert.ToDateTime(txtTimeLine.Text);
                                    }
                                    bool saveflag = RLCS_VendorScheduler.CreateTransaction(transaction);
                                    if (saveflag)
                                    {
                                        if (ddlChecklistStatus.SelectedValue == "3")
                                        {
                                            try
                                            {
                                                var userdetil = entities.sp_GetScheduleDetail(Convert.ToInt32(ScheduleOnID)).FirstOrDefault();

                                                if (userdetil != null)
                                                {
                                                    string ForMonth = string.Empty;
                                                    string ActName = string.Empty;
                                                    string StateName = string.Empty;
                                                    string NatureOfCompliance = string.Empty;
                                                    string TypeOfCompliance = string.Empty;

                                                    long ChklistID = Convert.ToInt32(Request.QueryString["CHID"].ToString());

                                                    var getChecklist = (from row in entities.RLCS_tbl_CheckListMaster
                                                                        join row1 in entities.RLCS_Act_Mapping
                                                                        on row.ActID equals row1.AM_ActID
                                                                        where row.IsDeleted == false && row.Id == ChklistID
                                                                        select new dataEmail
                                                                        {
                                                                            ActName = row1.AM_ActName,
                                                                            Stateid = row.StateID,
                                                                            NatureOfCompliance = row.NatureOfCompliance,
                                                                            TypeOfCompliance = row.TypeOfCompliance
                                                                        }).FirstOrDefault();

                                                    if (getChecklist != null)
                                                    {
                                                        ForMonth = userdetil.ForMonth;
                                                        NatureOfCompliance = getChecklist.NatureOfCompliance;
                                                        TypeOfCompliance = getChecklist.TypeOfCompliance;
                                                        ActName = getChecklist.ActName;
                                                        StateName = getChecklist.Stateid;
                                                    }

                                                    DateTime SDstartDate = userdetil.ScheduleOn;
                                                    DateTime today = DateTime.Now;

                                                    string ClientName = userdetil.ClientName;
                                                    string Subject = ClientName + " Documents rejected for the period(" + userdetil.ForMonth + ")";
                                                    string formattedDate = SDstartDate.AddDays(10).ToString("dd-MM-yyyy");

                                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_RLCSRejectionDocument
                                                                    .Replace("@User", userdetil.CC_SPOC_Name)
                                                                    .Replace("@Date", formattedDate)
                                                                    .Replace("@ForMonth", ForMonth)
                                                                    .Replace("@StName", StateName)
                                                                    .Replace("@ActName", ActName)
                                                                    .Replace("@NatureOfCompliance", NatureOfCompliance)
                                                                    .Replace("@TypeofCompliance", TypeOfCompliance)
                                                                    .Replace("@Status", "Team Review");
                                                                    
                                                    List<string> aa = new List<string>();
                                                    aa.Add(userdetil.CC_SPOC_Email);
                                                    //aa.Add("amol@avantis.info");
                                                    
                                                    RLCS_Vendor_AuditScheduleMail_Log obj = new RLCS_Vendor_AuditScheduleMail_Log();
                                                    obj.AuditID = Convert.ToInt32(AuditId);
                                                    obj.Avacom_UserID = Convert.ToInt32(userdetil.VendorID);
                                                    obj.ScheduleOnID = Convert.ToInt32(ScheduleOnID);
                                                    obj.CreatedOn = DateTime.Now;
                                                    obj.IsSend = true;
                                                    obj.Flag = "Document Rejected from Auditor";
                                                    entities.RLCS_Vendor_AuditScheduleMail_Log.Add(obj);
                                                    entities.SaveChanges();

                                                    string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], aa, null, null, Subject, message);

                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                                                
                                            }
                                        }
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Record Save Successfully.";
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(txtRemark.Text))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Required Remark.";
                    }
                    else if (string.IsNullOrEmpty(ddlStatus.SelectedValue) || ddlStatus.SelectedValue == "-1")
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Required Status.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {               
                if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=VendorAudit.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                        }
                        BindGrid();
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {

                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    string CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    //lblMessage.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }


        public class dataEmail
        {
            public string ActName { get; set; }
            public string Stateid { get; set; }
            public string NatureOfCompliance { get; set; }
            public string TypeOfCompliance { get; set; }
        }
    }
}