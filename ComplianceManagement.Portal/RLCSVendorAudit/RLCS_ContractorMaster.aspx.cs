﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_ContractorMaster : System.Web.UI.Page
    {
        protected static string user_Roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                user_Roles = AuthenticationHelper.Role;

            }
        }

        [WebMethod]
        public static string BindContractorList(ContractDetails DetailsObj)
        {
            try
            {

               
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   
                        var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                             where Contractor.ClientID == DetailsObj.ClientID
                                             select Contractor).ToList();
                        foreach (var row in ContractorObj)
                        {
                            ContractDetails Details = new ContractDetails();
                            Details.ID = row.ID;
                            Details.ContractorID = row.ContractorID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SPOCName;
                            Details.SPOCEmailID = row.SPOCEmailID;
                            Details.SPOCMobileNo = row.SPOCMobileNo;
                            ContractorDetailsList.Add(Details);

                        }
                   

                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string SaveContractDetails(ContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;
                ReturnDetails RtrnMsg = new ReturnDetails();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                    int CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int id = Convert.ToInt32(DetailsObj.ID);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        RLCS_VendorContractorMaster updateDetails = (from row in entities.RLCS_VendorContractorMaster
                                                                     where row.ID == id
                                                                     select row).FirstOrDefault();

                        if (updateDetails != null)
                        {
                            #region Update Details
                            if (CheckDuplicateContractor(DetailsObj.ClientID, DetailsObj.SPOCEmailID, DetailsObj.ContractorName))
                            {
                                updateDetails.ContractorID = DetailsObj.ContractorID;
                                updateDetails.ContractorName = DetailsObj.ContractorName;
                                updateDetails.SPOCName = DetailsObj.SPOCName;
                                updateDetails.SPOCEmailID = DetailsObj.SPOCEmailID;
                                updateDetails.SPOCMobileNo = DetailsObj.SPOCMobileNo;

                                entities.SaveChanges();

                                if (CheckDuplicateMailID(DetailsObj.SPOCEmailID))
                                {
                                    string passwordText = Util.CreateRandomPassword(10);
                                    string Password = Util.CalculateAESHash(passwordText);
                                    User UserMaster = new User()
                                    {
                                        FirstName = DetailsObj.SPOCName,
                                        LastName = "",
                                        Email = DetailsObj.SPOCEmailID,
                                        Password = Password,
                                        ContactNumber = DetailsObj.SPOCMobileNo,
                                        IsDeleted = false,
                                        CreatedBy = UserID,
                                        CreatedByText = "Admin",
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        RoleID = 23,
                                        LastLoginTime = DateTime.Now,
                                        VendorRoleID = 23,
                                        EnType = "A",
                                        CustomerID = CustID
                                    };
                                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                                    {
                                        FirstName = DetailsObj.SPOCName,
                                        LastName = "",
                                        Email = DetailsObj.SPOCEmailID,
                                        Password = Password,
                                        ContactNumber = DetailsObj.SPOCMobileNo,
                                        IsDeleted = false,
                                        CreatedBy = 1,
                                        CreatedByText = "Admin",
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        RoleID = 23,
                                        LastLoginTime = DateTime.Now,
                                        VendorRoleID = 23,
                                        EnType = "A",
                                        CustomerID = CustID
                                    };

                                    bool flagexist = true;
                                    bool emailExists;
                                    UserManagement.Exists(UserMaster, out emailExists);
                                    if (emailExists)
                                    {
                                        flagexist = false;
                                    }
                                    else
                                    {
                                        UserManagementRisk.Exists(mstUser, out emailExists);
                                        if (emailExists)
                                        {
                                            flagexist = false;
                                        }
                                    }
                                    if (flagexist)
                                    {
                                        string message = SendNotificationEmail(UserMaster, passwordText);

                                        int resultValue = CreateNewVendor(UserMaster);
                                        if (resultValue > 0)
                                        {
                                            bool result = Create(mstUser, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                                            if (result == false)
                                            {
                                                UserManagement.deleteUser(resultValue);
                                            }
                                        }
                                        Success = true;
                                        RtrnMsg.Message = "Contractor Update Successfully.";
                                        RtrnMsg.Success = true;
                                    }
                                    else
                                    {
                                        RtrnMsg.Message = "User with Same Email already Exists.";
                                        RtrnMsg.Success = false;
                                    }
                                }
                                else
                                {
                                    bool output = UpdateUser(DetailsObj.SPOCEmailID, DetailsObj.SPOCMobileNo, DetailsObj.SPOCName);
                                    if (output)
                                    {
                                        bool output1 = UpdateAuditUser(DetailsObj.SPOCEmailID, DetailsObj.SPOCMobileNo, DetailsObj.SPOCName);
                                        if (output1)
                                        {
                                            Success = true;
                                            RtrnMsg.Message = "Contractor Saved Successfully.";
                                            RtrnMsg.Success = true;
                                        }
                                        else
                                        {
                                            Success = true;
                                            RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                            RtrnMsg.Success = false;
                                        }
                                    }
                                    else
                                    {
                                        RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                        RtrnMsg.Success = false;
                                    }
                                }
                            }
                            else
                            {
                                updateDetails.SPOCName = DetailsObj.SPOCName;
                                updateDetails.SPOCMobileNo = DetailsObj.SPOCMobileNo;
                                entities.SaveChanges();
                                bool output = UpdateUser(DetailsObj.SPOCEmailID, DetailsObj.SPOCMobileNo, DetailsObj.SPOCName);
                                if (output)
                                {
                                    bool output1 = UpdateAuditUser(DetailsObj.SPOCEmailID, DetailsObj.SPOCMobileNo, DetailsObj.SPOCName);
                                    if (output1)
                                    {
                                        Success = true;
                                        RtrnMsg.Message = "Contractor Saved Successfully.";
                                        RtrnMsg.Success = true;
                                    }
                                    else
                                    {
                                        Success = true;
                                        RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                        RtrnMsg.Success = false;
                                    }
                                }
                                else
                                {
                                    RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                    RtrnMsg.Success = false;
                                }
                                //RtrnMsg.Message = "Contractor Update Successfully.";
                                //RtrnMsg.Success = true;

                            }
                            #endregion
                        }
                        else
                        {
                            #region add new detail
                            if (CheckDuplicateContractor(DetailsObj.ClientID,DetailsObj.SPOCEmailID,DetailsObj.ContractorName))
                            {
                                RLCS_VendorContractorMaster ContractorMaster = new RLCS_VendorContractorMaster()
                                {
                                    ContractorID = DetailsObj.ContractorID,
                                    ContractorName = DetailsObj.ContractorName,
                                    SPOCName = DetailsObj.SPOCName,
                                    SPOCMobileNo = DetailsObj.SPOCMobileNo,
                                    SPOCEmailID = DetailsObj.SPOCEmailID,
                                    ClientID = DetailsObj.ClientID

                                };
                                entities.RLCS_VendorContractorMaster.Add(ContractorMaster);
                                entities.SaveChanges();
                                if (CheckDuplicateMailID(DetailsObj.SPOCEmailID))
                                {
                                    string passwordText = Util.CreateRandomPassword(10);
                                    string Password = Util.CalculateAESHash(passwordText);
                                    User UserMaster = new User()
                                    {
                                        FirstName = DetailsObj.SPOCName,
                                        LastName = "",
                                        Email = DetailsObj.SPOCEmailID,
                                        Password = Password,
                                        ContactNumber = DetailsObj.SPOCMobileNo,
                                        IsDeleted = false,
                                        CreatedBy = UserID,
                                        CreatedByText = "Admin",
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        RoleID = 23,
                                        LastLoginTime = DateTime.Now,
                                        VendorRoleID = 23,
                                        EnType = "A",
                                        CustomerID = CustID
                                    };
                                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                                    {
                                        FirstName = DetailsObj.SPOCName,
                                        LastName = "",
                                        Email = DetailsObj.SPOCEmailID,
                                        Password = Password,
                                        ContactNumber = DetailsObj.SPOCMobileNo,
                                        IsDeleted = false,
                                        CreatedBy = 1,
                                        CreatedByText = "Admin",
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        RoleID = 23,
                                        LastLoginTime = DateTime.Now,
                                        VendorRoleID = 23,
                                        EnType = "A",
                                        CustomerID = CustID
                                    };

                                    bool flagexist = true;
                                    bool emailExists;
                                    UserManagement.Exists(UserMaster, out emailExists);
                                    if (emailExists)
                                    {
                                        flagexist = false;
                                    }
                                    else
                                    {
                                        UserManagementRisk.Exists(mstUser, out emailExists);
                                        if (emailExists)
                                        {
                                            flagexist = false;
                                        }
                                    }
                                    if (flagexist)
                                    {
                                        string message = SendNotificationEmail(UserMaster, passwordText);

                                        int resultValue = CreateNewVendor(UserMaster);
                                        if (resultValue > 0)
                                        {
                                            bool result = Create(mstUser, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                                            if (result == false)
                                            {
                                                UserManagement.deleteUser(resultValue);
                                            }
                                        }
                                        //entities.Users.Add(UserMaster);
                                        //entities.SaveChanges();
                                        Success = true;
                                        RtrnMsg.Message = "Contractor Saved Successfully.";
                                        RtrnMsg.Success = true;
                                    }
                                    else
                                    {
                                        RtrnMsg.Message = "User with Same Email already Exists.";
                                        RtrnMsg.Success = false;
                                    }
                                }
                                else
                                {
                                    bool output = UpdateUser(DetailsObj.SPOCEmailID, DetailsObj.SPOCMobileNo, DetailsObj.SPOCName);
                                    if (output)
                                    {
                                        bool output1 = UpdateAuditUser(DetailsObj.SPOCEmailID, DetailsObj.SPOCMobileNo, DetailsObj.SPOCName);
                                        if (output1)
                                        {
                                            Success = true;
                                            RtrnMsg.Message = "Contractor Save Successfully.";
                                            RtrnMsg.Success = true;
                                        }
                                        else
                                        {
                                            Success = true;
                                            RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                            RtrnMsg.Success = false;
                                        }
                                    }
                                    else
                                    {
                                        RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                        RtrnMsg.Success = false;
                                    }
                                    //User UserObj = (from row in entities.Users
                                    //                where row.Email == DetailsObj.SPOCEmailID
                                    //                select row).FirstOrDefault();
                                    //if (UserObj != null)
                                    //{
                                    //    UserObj.FirstName = DetailsObj.SPOCName;
                                    //    UserObj.ContactNumber = DetailsObj.SPOCMobileNo;
                                    //    UserObj.VendorRoleID = 23;
                                    //    entities.SaveChanges();
                                    //    Success = true;
                                    //    RtrnMsg.Message = "Contractor Save Successfully.";
                                    //    RtrnMsg.Success = true;
                                    //}
                                }   
                            }
                            else
                            {
                                RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                RtrnMsg.Success = false;
                            }                           
                            #endregion
                        }
                        return serializer.Serialize(RtrnMsg);
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        public static bool UpdateAuditUser(string Email, string ContactNo, string Name)
        {
            bool result = false;
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mst_User UserObj = (from row in entities.mst_User
                                    where row.Email == Email
                                    select row).FirstOrDefault();
                    if (UserObj != null)
                    {
                        UserObj.FirstName = Name;
                        UserObj.ContactNumber = ContactNo;
                        UserObj.VendorRoleID = 23;
                        entities.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public static bool UpdateUser(string Email,string ContactNo,string Name)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User UserObj = (from row in entities.Users
                                    where row.Email == Email
                                    select row).FirstOrDefault();
                    if (UserObj != null)
                    {
                        UserObj.FirstName = Name;
                        UserObj.ContactNumber = ContactNo;
                        UserObj.VendorRoleID = 23;
                        entities.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
            return result;
        }
        public static bool Create(mst_User user, string SenderEmailAddress, string message)
        {
            int Addid = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagementRisk.GetByID(Convert.ToInt32(user.CustomerID)).Status;

                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.IsActive = true;                          
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.mst_User.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();

                        //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);

                        return true;

                    }
                    catch (Exception ex)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDMstUser(Addid);
                        }
                        return false;
                    }
                }
            }
        }
        public static int CreateNewVendor(User user)
        {
            int Addid = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID)).Status;
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;                      
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.UtcNow;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();
                        return Addid;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDUser(Addid);
                        }
                        return Addid = 0;
                    }
                }
            }
        }

        public static string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                      .Replace("@Username", user.Email)
                                      .Replace("@User", username)
                                      .Replace("@PortalURL", Convert.ToString(portalURL))
                                      .Replace("@Password", passwordText)
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(portalURL));
                                  
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvEmailError.IsValid = false;
                //cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        [WebMethod]
        public static string BindContractor(ContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;
                int id = Convert.ToInt32(DetailsObj.ID);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (id > 0)
                    {
                        var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                             where Contractor.ID == id && Contractor.ClientID== DetailsObj.ClientID
                                             select Contractor).ToList();
                        foreach (var row in ContractorObj)
                        {
                            ContractDetails Details = new ContractDetails();
                            Details.ID = row.ID;
                            Details.ContractorID = row.ContractorID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SPOCName;
                            Details.SPOCEmailID = row.SPOCEmailID;
                            Details.SPOCMobileNo = row.SPOCMobileNo;

                            ContractorDetailsList.Add(Details);
                        }
                    }
                    else
                    {
                        var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                             where  Contractor.ClientID == DetailsObj.ClientID
                                             select Contractor).ToList();
                        foreach (var row in ContractorObj)
                        {
                            ContractDetails Details = new ContractDetails();
                            Details.ID = row.ID;
                            Details.ContractorID = row.ContractorID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SPOCName;
                            Details.SPOCEmailID = row.SPOCEmailID;
                            Details.SPOCMobileNo = row.SPOCMobileNo;

                            ContractorDetailsList.Add(Details);
                        }
                    }



                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string GetContractorDetails(ContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                         where Contractor.ID == DetailsObj.ID
                                         select Contractor).ToList();

                    foreach (var row in ContractorObj)
                    {
                        ContractDetails Details = new ContractDetails();
                        Details.ID = row.ID;
                        Details.ContractorID = row.ContractorID;
                        Details.ContractorName = row.ContractorName;
                        Details.SPOCName = row.SPOCName;
                        Details.SPOCEmailID = row.SPOCEmailID;
                        Details.SPOCMobileNo = row.SPOCMobileNo;

                        ContractorDetailsList.Add(Details);
                    }

                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindClientList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ClientDetails> ClientDetailsList = new List<ClientDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ClientObj = (from Client in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                         where Client.BranchType == "E"
                                         select Client).Distinct().ToList();
                    string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                    if (CheckContractCustomer == "True")
                    {
                        int Custid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        ClientObj = ClientObj.Where(x => x.AVACOM_CustomerID == Custid && x.AVACOM_CustomerID != null).ToList();
                    }
                    else
                    {
                        ClientObj = (from Client in ClientObj
                                     join C in entities.Customers
                                     on Client.AVACOM_CustomerID equals C.ID
                                     where C.ServiceProviderID != 95
                                     select Client).Distinct().ToList();
                    }
                    foreach (var row in ClientObj)
                    {
                        ClientDetails Details = new ClientDetails();
                        Details.ClientID = row.CM_ClientID;
                        Details.ClientName = row.CM_ClientName;
                        Details.CustomerID = row.AVACOM_CustomerID.ToString();
                        ClientDetailsList.Add(Details);
                    }

                    return serializer.Serialize(ClientDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        public static bool CheckDuplicateMailID(string MailId)
        {
            try
            {
                bool Success = true;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User UserObj = (from row in entities.Users
                                    where row.Email == MailId
                                    select row).FirstOrDefault();

                    if (UserObj != null)
                    {
                        Success = false;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckDuplicateContractor(string ClientID,string EmailID,string ContractorName)
        {
            try
            {
                bool Success = true;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    RLCS_VendorContractorMaster contractorObj = (from row in entities.RLCS_VendorContractorMaster
                                    where row.ClientID == ClientID && row.SPOCEmailID== EmailID && row.ContractorName == ContractorName
                                    select row).FirstOrDefault();

                    if (contractorObj != null)
                    {
                        Success = false;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
           
        public class ContractDetails
        {
            public int ID { get; set; }
            public string ClientID { get; set; }
            public string ContractorID { get; set; }
            public string ContractorName { get; set; }
            public string SPOCName { get; set; }
            public string SPOCEmailID { get; set; }
            public string SPOCMobileNo { get; set; }

        }

        public class ClientDetails
        {
            public string ClientID { get; set; }
            public string CustomerID { get; set; }
            public string ClientName { get; set; }

        }

        public class ReturnDetails
        {
            public bool Success { get; set; }
            public string Message { get; set; }
        }

    }
}