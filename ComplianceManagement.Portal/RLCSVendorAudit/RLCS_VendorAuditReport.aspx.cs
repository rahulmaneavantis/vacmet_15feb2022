﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.Web.Services;
using iTextSharp.text.pdf.draw;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_VendorAuditReport : System.Web.UI.Page
    {
        public List<string> AuditPeriod = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        ViewState["vrole"] = Convert.ToString(vrole);
                    }
                    BindCustomerFilter();
                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                    BindPeriod("");



                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlVendorListBase_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            long customerID = -1;
            string role = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
            {
                role = Convert.ToString(ViewState["vrole"]);
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
            }
            else
            {
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                    ViewState["vrole"] = Convert.ToString(role);
                }
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);
            }

            if (customerID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetVendorAuditAssignedLocationBranches((int)customerID, AuthenticationHelper.UserID, role)).ToList();
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                            BindLocationFilter(assignedbranchIDs, (int)customerID);
                        }
                    }

                    var query = (from row in entities.RLCS_VendorAuditInstance
                                 where row.AVACOM_CustomerID == customerID
                                 orderby row.ID descending
                                 select row).FirstOrDefault();

                    txtFrequency.Text = query.CC_Frequency;
                    if (query.CC_Frequency == "H")
                    {
                        txtFrequency.Text = "Half Yearly";
                    }
                    else if (query.CC_Frequency == "Q")
                    {
                        txtFrequency.Text = "Quarterly";
                    }
                    else if (query.CC_Frequency == "M")
                    {
                        txtFrequency.Text = "Monthly";
                    }
                    else if (query.CC_Frequency == "O")
                    {
                        txtFrequency.Text = "Occurence";
                    }
                    
                }
            }
            BindPeriod("");
            BindGrid();
            bindPageNumber();
            ShowGridDetail();

        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdChecklistDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                bindVendorList();
                BindGrid();
                bindPageNumber();
                ShowGridDetail();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdChecklistDetails.PageIndex = chkSelectedPage - 1;
            grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
            ShowGridDetail();
        }

        public void BindPeriod(string Frequency)
        {
            string role = string.Empty;
            role = Convert.ToString(ViewState["vrole"]);
            var lstCustomers = GetAuditPeriod(AuthenticationHelper.UserID, role, Frequency);
            if ((ddlFinYear.SelectedValue != "-1") && (ddlFinYear.SelectedIndex != -1))
            {
                var yearLastTwo = ddlFinYear.SelectedValue.Substring(2, 2);
                //lstCustomers = lstCustomers.Where(sid => sid.Any(si => si == yearLastTwo.ToCharArray()[0]));
                ddlPeriod.DataSource = lstCustomers.Distinct().ToList();
                ddlPeriod.DataBind();
                ddlPeriod.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Period", ""));
            }
        }
        public List<string> GetAuditPeriod(int UserID, string role, string Freq)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {

                    var lstChecklistDetails = (from row in entities.SP_RLCS_VendorAudit_CheckListReport(-1, -1, Convert.ToInt32(ddlFilterCustomer.SelectedValue), role, UserID)
                                               select row).ToList();
                    if (Freq != "")
                    {
                        lstChecklistDetails = lstChecklistDetails.Select(entity => entity).Where(entity => entity.Frequency == Freq).ToList();
                    }
                    lstChecklistDetails.GroupBy(entity => entity.ForMonth).Select(entity => entity.FirstOrDefault()).ToList();
                    //var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(ddlFilterCustomer.SelectedValue), UserID, role)
                    //select row).ToList();
                    //if (mgmtQueryStatutory.Count > 0)
                    //{
                    //    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                    //}
                    AuditPeriod = lstChecklistDetails.Select(x => x.ForMonth).ToList();

                }
                else
                {
                    var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(Convert.ToInt32(311), UserID, role)
                                              select row).ToList();
                    if (mgmtQueryStatutory.Count > 0)
                    {
                        mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    AuditPeriod = mgmtQueryStatutory.Select(x => x.ForMonth).ToList();
                }
            }

            return AuditPeriod;
        }
        private void BindCustomerFilter()
        {
            string role = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
            {
                role = Convert.ToString(ViewState["vrole"]);
            }
            else
            {
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                    ViewState["vrole"] = Convert.ToString(role);
                }
            }
            string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
            if (CheckContractCustomer == "True")
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, role, AuthenticationHelper.CustomerID);

                ddlFilterCustomer.DataSource = lstCustomers;
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataBind();
            }
            else
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);

                ddlFilterCustomer.DataSource = lstCustomers;
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataBind();
            }
            //var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);
            //ddlFilterCustomer.DataTextField = "Name";
            //ddlFilterCustomer.DataValueField = "ID";
            //ddlFilterCustomer.DataSource = lstCustomers;
            //ddlFilterCustomer.DataBind();
        }
        private void BindLocationFilter(List<int> assignedBranchList, int Customerid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Customerid);
                    tvFilterLocation.Nodes.Clear();
                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindVendorList()
        {
            try
            {
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.GetAllVendor(branchID);
                    ddlVendorListBase.DataTextField = "Name";
                    ddlVendorListBase.DataValueField = "ID";
                    ddlVendorListBase.DataSource = data;
                    ddlVendorListBase.DataBind();
                    ddlVendorListBase.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Vendor", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindGrid()
        {
            try
            {
                int customerID = -1;
                int branchID = -1;
                long vendorid = -1;
                string Freq = string.Empty;
                string Period = string.Empty;
                DateTime AuditDate = new DateTime();
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && ddlFilterCustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlVendorListBase.SelectedValue))
                {
                    vendorid = Convert.ToInt32(ddlVendorListBase.SelectedValue);
                }
                if (!string.IsNullOrEmpty(txtFrequency.Text))
                {
                    Freq = txtFrequency.Text.Substring(0, 1);
                }
                if (!(string.IsNullOrEmpty(ddlPeriod.SelectedValue)))
                {
                    Period = ddlPeriod.SelectedValue;

                }
               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Session["TotalRows"] = null;

                    long userID = -1;
                    var role = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
                    {
                        role = Convert.ToString(ViewState["vrole"]);
                    }
                    else
                    {
                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                        if (LoggedUser != null)
                        {
                            role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                            ViewState["vrole"] = Convert.ToString(role);
                        }
                    }
                    if (role.Trim().Contains("HVADM"))
                    {
                        userID = -1;
                    }
                    else
                    {
                        userID = AuthenticationHelper.UserID;
                    }
                    var lstChecklistDetails = entities.SP_RLCS_VendorAudit_CheckListReport(vendorid, branchID, customerID, role, userID).ToList();
                    lstChecklistDetails = lstChecklistDetails.Distinct().ToList();

                    if (lstChecklistDetails.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(Freq))
                        {
                            //lstChecklistDetails = lstChecklistDetails.Where(entry => entry.Frequency.Equals(Freq)).ToList();
                        }
                        if (!string.IsNullOrEmpty(ddlFinYear.SelectedValue) && ddlFinYear.SelectedValue != "-1")
                        {
                            DateTime startDate = new DateTime(Convert.ToInt32(ddlFinYear.SelectedValue), 4, 1); // 1st Feb this year
                            DateTime endDate = new DateTime(Convert.ToInt32(ddlFinYear.SelectedValue) + 1, 3, 31); // Last day in January next year

                            lstChecklistDetails = lstChecklistDetails.Where(x => x.StartDate >= startDate && x.StartDate <= endDate).ToList();
                        }
                        if (!string.IsNullOrEmpty(Period))
                        {
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.ForMonth.Trim().ToUpper() == ddlPeriod.SelectedValue.Trim().ToUpper()).ToList();
                        }
                        if (!string.IsNullOrEmpty(txtInvoiceNo.Text))
                        {
                            lstChecklistDetails = lstChecklistDetails.Where(e => e.InVoiceNo != "" && txtInvoiceNo.Text.ToLower().Contains(e.InVoiceNo.ToLower())).ToList();
                        }
                        if (!(string.IsNullOrEmpty(txtAuditDate.Text)))
                        {
                            AuditDate = Convert.ToDateTime(txtAuditDate.Text);
                            DateTime dt = AuditDate.AddDays(1);
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.StartDate >= AuditDate && x.StartDate <= dt).ToList();
                            //lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditedDate >= AuditDate && x.AuditedDate <= dt).ToList();
                        }

                        grdChecklistDetails.DataSource = lstChecklistDetails;
                        grdChecklistDetails.DataBind();
                        Session["TotalRows"] = lstChecklistDetails.Count;
                    }
                    else
                    {
                        grdChecklistDetails.DataSource = lstChecklistDetails;
                        grdChecklistDetails.DataBind();
                    }
                    lstChecklistDetails.Clear();
                    lstChecklistDetails = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void txtchanged(object sender, EventArgs e)
        {
            BindGrid();
        }
        #region Page Number-Bottom
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.SelectedValue = null;
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        protected void grdChecklistDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    #region Sheet 1   
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long AuditID = Convert.ToInt64(commandArgs[0]);
                        long CustomerBranchID = Convert.ToInt64(commandArgs[1]);
                        long VendorID = Convert.ToInt64(commandArgs[2]);
                        long ScheduleOnID = Convert.ToInt64(commandArgs[3]);
                        string AuditorName = Convert.ToString(commandArgs[4]);
                        string ClientName = Convert.ToString(commandArgs[5]);
                        string ForMonth = Convert.ToString(commandArgs[6]);

                        string AuditedDate = string.Empty;
                        if (commandArgs[7] != null)
                        {
                            DateTime DT = Convert.ToDateTime(commandArgs[7].ToString());
                            AuditedDate = DT.ToString("dd-MMM-yyyy");
                        }

                        string StartDate = string.Empty;
                        if (commandArgs[8] != null)
                        {
                            DateTime SDDDT = Convert.ToDateTime(commandArgs[8].ToString());
                            StartDate = SDDDT.ToString("dd-MMM-yyyy");
                        }

                        string EndDate = string.Empty;
                        if (commandArgs[9] != null)
                        {
                            DateTime EDDDT = Convert.ToDateTime(commandArgs[9].ToString());
                            EndDate = EDDDT.ToString("dd-MMM-yyyy");
                        }
                        #region code                                                                               
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {

                            if (AuditID != -1 && CustomerBranchID != -1 && VendorID != -1 && ScheduleOnID != -1)
                            {
                                var instanceDetails = (from row in entities.RLCS_VendorAuditInstance
                                                       where row.ID == AuditID && row.CC_Status == "A"
                                                       && row.IsActive == false
                                                       select row).FirstOrDefault();
                                if (instanceDetails != null)
                                {
                                    string clientSPOCName = RLCS_ClientsManagement.GetClientSPOC(instanceDetails.CC_ClientID, ConfigurationManager.AppSettings["RLCSAPIURL"] + "Masters/");

                                    var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(ScheduleOnID, AuditID)
                                                 select row).ToList();

                                    int compliedCount = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                                    int NotcompliedCount = table.Where(a => a.ResultStatusID == 2).ToList().Count;
                                    int AppliANDNotApplicableCount = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;
                                    int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount;

                                    int HighCount = table.Where(entry => entry.Risk.Equals("High")).ToList().Count;
                                    int LowCount = table.Where(entry => entry.Risk.Equals("Low")).ToList().Count;
                                    int MediumCount = table.Where(entry => entry.Risk.Equals("Medium")).ToList().Count;
                                    int RisktotalPoints = HighCount + LowCount + MediumCount;

                                    int CompliedHighCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("High")).ToList().Count;
                                    int CompliedMedCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Medium")).ToList().Count;
                                    int CompliedLowCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Low")).ToList().Count;


                                    int NotcompliedHighCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("High")).ToList().Count;
                                    int NotcompliedMedCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Medium")).ToList().Count;
                                    int NotcompliedLowCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Low")).ToList().Count;

                                    int NotApplicableHighCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("High")).ToList().Count;
                                    int NotApplicableMedCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Medium")).ToList().Count;
                                    int NotApplicableLowCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Low")).ToList().Count;

                                    int totalHigh = CompliedHighCount + NotcompliedHighCount + NotApplicableHighCount;
                                    int totalMedium = CompliedMedCount + NotcompliedMedCount + NotApplicableMedCount;
                                    int totalLow = CompliedLowCount + NotcompliedLowCount + NotApplicableLowCount;

                                    string fileName = string.Empty;

                                    //fileName = "OverAllReport " + ForMonth + ".xlsx";
                                    fileName = "AuditReport " + ForMonth + ".xlsx";

                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(ForMonth);
                                    //ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Audit Checklist Report");
                                    DataTable ExcelData1 = null;
                                    DataView view1 = new System.Data.DataView((table as List<SP_RLCS_VendorAudit_DetailsCheckListReport_Result>).ToDataTable());
                                    ExcelData1 = view1.ToTable("Selected", false, "SNO", "StateID", "ActName", "NatureOFCompliance", "Risk", "Form", "Section", "ResultStatus", "Audit_Observation", "Recommendation", "TypeOfCompliance", "Consequences");

                                    int cnt = 1;
                                    #region
                                    foreach (DataRow item in ExcelData1.Rows)
                                    {
                                        item["SNO"] = cnt;
                                        cnt++;
                                    }//foreach end
                                    #endregion

                                    #region

                                    exWorkSheet1.Cells["A1"].Value = "Vendor Audit Checklist";
                                    exWorkSheet1.Cells["A1:L1"].Merge = true;
                                    exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.Gray);
                                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A1"].Style.Font.Size = 14;
                                    exWorkSheet1.Cells["A1:L1"].Style.Font.Italic = true;
                                    exWorkSheet1.Cells["A1:L1"].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells["A1:L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells["A1:L1"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["A1:L1"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["A1:L1"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["A1:L1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                    //row 2
                                    exWorkSheet1.Cells["A2"].Value = "1";
                                    exWorkSheet1.Cells["A2:B2"].Merge = true;
                                    exWorkSheet1.Cells["A2"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["A2:B16"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A2:B16"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["A2:B16"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["A2:B16"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["A2:B16"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["A2:B16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                                    exWorkSheet1.Cells["C2"].Value = "Client Name";
                                    exWorkSheet1.Cells["C2"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C2"].Style.WrapText = true;
                                    exWorkSheet1.Cells["C2:C16"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C2:C16"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["C2:C16"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["C2:C16"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["C2:C16"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["C2:C16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                    exWorkSheet1.Cells["D2"].Value = ClientName;
                                    exWorkSheet1.Cells["D2:J2"].Merge = true;
                                    exWorkSheet1.Cells["D2"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D2:L16"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["D2:L16"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["D2:L16"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells["D2:L16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                    //row 3
                                    exWorkSheet1.Cells["A3"].Value = "2";
                                    exWorkSheet1.Cells["A3:B3"].Merge = true;
                                    exWorkSheet1.Cells["A3"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C3"].Value = "Name of the Client SPOC";
                                    exWorkSheet1.Cells["C3"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D3"].Value = clientSPOCName;//instanceDetails.CC_SPOC_Name;
                                    exWorkSheet1.Cells["D3:J3"].Merge = true;
                                    exWorkSheet1.Cells["D3"].Style.WrapText = true;

                                    //row 4
                                    exWorkSheet1.Cells["A4"].Value = "3";
                                    exWorkSheet1.Cells["A4:B4"].Merge = true;
                                    exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C4"].Value = "Name of Vendor & Address";
                                    exWorkSheet1.Cells["C4"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D4"].Value = instanceDetails.CC_ContractorName + ',' + instanceDetails.CC_ContractorAddress;
                                    exWorkSheet1.Cells["D4:J4"].Merge = true;
                                    exWorkSheet1.Cells["D4"].Style.WrapText = true;

                                    //row 5
                                    exWorkSheet1.Cells["A5"].Value = "4";
                                    exWorkSheet1.Cells["A5:B5"].Merge = true;
                                    exWorkSheet1.Cells["A5"].AutoFitColumns(5);


                                    exWorkSheet1.Cells["C5"].Value = "Name of Vendor 's Representative and Mobile No.";
                                    exWorkSheet1.Cells["C5"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C5"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D5"].Value = instanceDetails.CC_SPOC_Name + "," + "Mob-" + instanceDetails.CC_SPOC_Mobile;
                                    exWorkSheet1.Cells["D5:J5"].Merge = true;
                                    exWorkSheet1.Cells["D5"].Style.WrapText = true;

                                    //row 6
                                    exWorkSheet1.Cells["A6"].Value = "5";
                                    exWorkSheet1.Cells["A6:B6"].Merge = true;
                                    exWorkSheet1.Cells["A6"].AutoFitColumns(5);



                                    exWorkSheet1.Cells["C6"].Value = "Nature of Work";
                                    exWorkSheet1.Cells["C6"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C6"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D6"].Value = instanceDetails.CC_NatureOfBusiness;
                                    exWorkSheet1.Cells["D6:J6"].Merge = true;
                                    exWorkSheet1.Cells["D6"].Style.WrapText = true;


                                    //row 7
                                    exWorkSheet1.Cells["A7"].Value = "6";
                                    exWorkSheet1.Cells["A7:B7"].Merge = true;
                                    exWorkSheet1.Cells["A7"].AutoFitColumns(5);


                                    exWorkSheet1.Cells["C7"].Value = "ESIC Number";
                                    exWorkSheet1.Cells["C7"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C7"].Style.WrapText = true;


                                    exWorkSheet1.Cells["D7"].Value = instanceDetails.CC_ESIC_Number;
                                    exWorkSheet1.Cells["D7:J7"].Merge = true;
                                    exWorkSheet1.Cells["D7"].Style.WrapText = true;

                                    //row 8
                                    exWorkSheet1.Cells["A8"].Value = "7";
                                    exWorkSheet1.Cells["A8:B8"].Merge = true;
                                    exWorkSheet1.Cells["A8"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C8"].Value = "PF Number";
                                    exWorkSheet1.Cells["C8"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C8"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D8"].Value = instanceDetails.CC_PF_Code;
                                    exWorkSheet1.Cells["D8:J8"].Merge = true;
                                    exWorkSheet1.Cells["D8"].Style.WrapText = true;

                                    //row 9
                                    exWorkSheet1.Cells["A9"].Value = "8";
                                    exWorkSheet1.Cells["A9:B9"].Merge = true;
                                    exWorkSheet1.Cells["A9"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C9"].Value = "Audit Date";
                                    exWorkSheet1.Cells["C9"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C9"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D9"].Value = instanceDetails.CC_ContractFrom;
                                    exWorkSheet1.Cells["D9:J9"].Merge = true;
                                    exWorkSheet1.Cells["D9"].Style.WrapText = true;
                                    exWorkSheet1.Cells["D9"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                    exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                                    //row 10
                                    exWorkSheet1.Cells["A10"].Value = "9";
                                    exWorkSheet1.Cells["A10:B10"].Merge = true;
                                    exWorkSheet1.Cells["A10"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C10"].Value = "Auditor Name";
                                    exWorkSheet1.Cells["C10"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C10"].Style.WrapText = true;


                                    exWorkSheet1.Cells["D10"].Value = AuditorName; //+ "(" + instanceDetails.CC_Auditor + ")";
                                    exWorkSheet1.Cells["D10:J10"].Merge = true;
                                    exWorkSheet1.Cells["D10"].Style.WrapText = true;


                                    //row 11
                                    exWorkSheet1.Cells["A11"].Value = "10";
                                    exWorkSheet1.Cells["A11:B11"].Merge = true;
                                    exWorkSheet1.Cells["A11"].AutoFitColumns(5);


                                    exWorkSheet1.Cells["C11"].Value = "No.of Employees";
                                    exWorkSheet1.Cells["C11"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C11"].Style.WrapText = true;

                                    //exWorkSheet1.Cells["D11"].Value = instanceDetails.CC_NoOfJoinees;
                                    exWorkSheet1.Cells["D11"].Value = instanceDetails.CC_NoOfJoinees + "( Male -" + instanceDetails.CC_MaleHeadCount + ", Female-" + instanceDetails.CC_FeMaleHeadCount + ")";
                                    exWorkSheet1.Cells["D11:J11"].Merge = true;
                                    exWorkSheet1.Cells["D11"].Style.WrapText = true;
                                    exWorkSheet1.Cells["D11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                                    //row 12
                                    exWorkSheet1.Cells["A12"].Value = "11";
                                    exWorkSheet1.Cells["A12:B12"].Merge = true;
                                    exWorkSheet1.Cells["A12"].AutoFitColumns(5);


                                    exWorkSheet1.Cells["C12"].Value = "Audit for the month";
                                    exWorkSheet1.Cells["C12"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C12"].Style.WrapText = true;


                                    exWorkSheet1.Cells["D12"].Value = ForMonth;
                                    exWorkSheet1.Cells["D12:J12"].Merge = true;
                                    exWorkSheet1.Cells["D12"].Style.WrapText = true;

                                    //row 13
                                    exWorkSheet1.Cells["A13"].Value = "12";
                                    exWorkSheet1.Cells["A13:B13"].Merge = true;
                                    exWorkSheet1.Cells["A13"].AutoFitColumns(5);


                                    exWorkSheet1.Cells["C13"].Value = "Branch";
                                    exWorkSheet1.Cells["C13"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C13"].Style.WrapText = true;


                                    exWorkSheet1.Cells["D13"].Value = instanceDetails.CC_BranchID;
                                    exWorkSheet1.Cells["D13:J13"].Merge = true;
                                    exWorkSheet1.Cells["D13"].Style.WrapText = true;

                                    exWorkSheet1.Cells["A14"].Value = "13";
                                    exWorkSheet1.Cells["A14:B14"].Merge = true;
                                    exWorkSheet1.Cells["A14"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C14"].Value = "Audited Date";
                                    exWorkSheet1.Cells["C14"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C14"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D14"].Value = AuditedDate;
                                    exWorkSheet1.Cells["D14:J14"].Merge = true;
                                    exWorkSheet1.Cells["D14"].Style.WrapText = true;

                                    exWorkSheet1.Cells["A15"].Value = "14";
                                    exWorkSheet1.Cells["A15:B15"].Merge = true;
                                    exWorkSheet1.Cells["A15"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C15"].Value = "Audit Start Date";
                                    exWorkSheet1.Cells["C15"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C15"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D15"].Value = StartDate;
                                    exWorkSheet1.Cells["D15:J15"].Merge = true;
                                    exWorkSheet1.Cells["D15"].Style.WrapText = true;

                                    exWorkSheet1.Cells["A16"].Value = "15";
                                    exWorkSheet1.Cells["A16:B16"].Merge = true;
                                    exWorkSheet1.Cells["A16"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["C16"].Value = "Audit End Date";
                                    exWorkSheet1.Cells["C16"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C16"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D16"].Value = EndDate;
                                    exWorkSheet1.Cells["D16:J16"].Merge = true;
                                    exWorkSheet1.Cells["D16"].Style.WrapText = true;

                                    exWorkSheet1.Cells["A17"].LoadFromDataTable(ExcelData1, true);

                                    exWorkSheet1.Cells["A17"].Value = "S NO";
                                    exWorkSheet1.Cells["A17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["A17"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["B17"].Value = "State";
                                    exWorkSheet1.Cells["B17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["B17"].AutoFitColumns(10);

                                    exWorkSheet1.Cells["C17"].Value = "Act Name";
                                    exWorkSheet1.Cells["C17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["C17"].AutoFitColumns(40);

                                    exWorkSheet1.Cells["D17"].Value = "Nature Of Compliance";
                                    exWorkSheet1.Cells["D17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["D17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["D17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["E17"].Value = "Risk";
                                    exWorkSheet1.Cells["E17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["E17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["E17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["F17"].Value = "Form";
                                    exWorkSheet1.Cells["F17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["F17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["F17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["G17"].Value = "Section & Rule";
                                    exWorkSheet1.Cells["G17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["G17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["G17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["H17"].Value = "Compliance Status";
                                    exWorkSheet1.Cells["H17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["H17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["H17"].AutoFitColumns(20);

                                    exWorkSheet1.Cells["I17"].Value = "Audit Observations";
                                    exWorkSheet1.Cells["I17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["I17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["I17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["J17"].Value = "Recommendations";
                                    exWorkSheet1.Cells["J17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["J17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["J17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["K17"].Value = "Compliance Type";
                                    exWorkSheet1.Cells["K17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["K17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["K17"].AutoFitColumns(35);

                                    exWorkSheet1.Cells["L17"].Value = "Consequences";
                                    exWorkSheet1.Cells["L17"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["L17"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["L17"].AutoFitColumns(35);



                                    #endregion
                                    int rowval = 18;

                                    foreach (DataRow itemdata in ExcelData1.Rows)
                                    {

                                        string result = Convert.ToString(itemdata["ResultStatus"]);
                                        if (result == "Not Complied")
                                        {
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                        }
                                        else if (result == "Not Applicable")
                                        {
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                        }
                                        else if (result == "Complied")
                                        {
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                        }
                                        else if (result == "Applicable")
                                        {
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["A" + rowval + ":L" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                        }
                                        rowval++;
                                    }

                                    #region  chart
                                    int chartstart = ExcelData1.Rows.Count + 17;

                                    int chartHeader = chartstart + 2;
                                    string header = "C" + chartHeader;
                                    string header1 = "D" + chartHeader;
                                    string header2 = "E" + chartHeader;

                                    string headermerge = header + ":" + header2;

                                    exWorkSheet1.Cells[header].Value = "Compliance Summary";
                                    exWorkSheet1.Cells[headermerge].Merge = true;
                                    exWorkSheet1.Cells[headermerge].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[header].Style.WrapText = true;


                                    chartstart = chartstart + 3;
                                    int range = (chartstart + 1);
                                    int rangenext = (range + 1);
                                    int rangenext1 = (rangenext + 1);

                                    string x = "C" + chartstart;
                                    string y = "C" + range;
                                    string z = "C" + rangenext;
                                    string t = "C" + rangenext1;

                                    string percenatge = "E" + chartstart;
                                    string percenatge1 = "E" + range;
                                    string percenatge2 = "E" + rangenext;

                                    string x1 = "D" + chartstart;
                                    string y1 = "D" + range;
                                    string z1 = "D" + rangenext;
                                    string t1 = "D" + rangenext1;

                                    //exWorkSheet1.Cells[percenatge].Value = string.Format("{0}%", (int)(((double)compliedCount / (double)totalPoints) * 100));
                                    //exWorkSheet1.Cells[percenatge1].Value = string.Format("{0}%", (int)(((double)NotcompliedCount / (double)totalPoints) * 100));
                                    //exWorkSheet1.Cells[percenatge2].Value = string.Format("{0}%", (int)(((double)AppliANDNotApplicableCount / (double)totalPoints) * 100));

                                    exWorkSheet1.Cells[percenatge].Value = string.Format("{0:0.00}%", ((double)compliedCount / (double)totalPoints) * 100);
                                    exWorkSheet1.Cells[percenatge1].Value = string.Format("{0:0.00}%", ((double)NotcompliedCount / (double)totalPoints) * 100);
                                    exWorkSheet1.Cells[percenatge2].Value = string.Format("{0:0.00}%", ((double)AppliANDNotApplicableCount / (double)totalPoints) * 100);


                                    exWorkSheet1.Cells[x1].Value = compliedCount;
                                    exWorkSheet1.Cells[y1].Value = NotcompliedCount;
                                    exWorkSheet1.Cells[z1].Value = AppliANDNotApplicableCount;
                                    exWorkSheet1.Cells[t1].Value = totalPoints;

                                    exWorkSheet1.Cells[x].Value = "Complied";
                                    exWorkSheet1.Cells[y].Value = "Not Complied";
                                    exWorkSheet1.Cells[z].Value = "Not Applicable";
                                    exWorkSheet1.Cells[t].Value = "Total";

                                    var myChart = exWorkSheet1.Drawings.AddChart("chart", eChartType.Pie3D);

                                    string s = x1 + ":" + z1;
                                    string s1 = x + ":" + z;

                                    // Define series for the chart
                                    var series = myChart.Series.Add(s, s1);


                                    var pieSeries = (ExcelPieChartSerie)series;
                                    pieSeries.Explosion = 5;

                                    pieSeries.DataLabel.ShowPercent = true;
                                    pieSeries.DataLabel.ShowLeaderLines = true;
                                    pieSeries.DataLabel.Position = eLabelPosition.BestFit;

                                    myChart.Border.Fill.Color = System.Drawing.Color.Green;
                                    myChart.Title.Text = "Compliance Summary";
                                    //myChart.Title.Text = "My Chart";
                                    myChart.SetSize(400, 400);
                                    // Add to 6th row and to the 6th column
                                    myChart.SetPosition(chartstart + 13, 0, 2, 0);

                                    #endregion

                                    #region New Table 
                                    int chartstart1 = ExcelData1.Rows.Count + 25;
                                    int chartHeader1 = chartstart1 + 2;

                                    string header01 = "C" + chartHeader1;
                                    string header11 = "D" + chartHeader1;
                                    string header12 = "E" + chartHeader1;
                                    string header13 = "F" + chartHeader1;
                                    string header14 = "G" + chartHeader1;

                                    exWorkSheet1.Cells[header01].Value = "Category";
                                    exWorkSheet1.Cells[header01].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[header01].Style.WrapText = true;
                                    exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                    exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                    exWorkSheet1.Cells[header11].Value = "High Risk";
                                    exWorkSheet1.Cells[header11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[header11].Style.WrapText = true;


                                    exWorkSheet1.Cells[header12].Value = "Medium Risk";
                                    exWorkSheet1.Cells[header12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[header12].Style.WrapText = true;

                                    exWorkSheet1.Cells[header13].Value = "Low Risk";
                                    exWorkSheet1.Cells[header13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[header13].Style.WrapText = true;

                                    exWorkSheet1.Cells[header14].Value = "Total";
                                    exWorkSheet1.Cells[header14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[header14].Style.WrapText = true;

                                    chartstart1 = chartstart1 + 3;
                                    int range1 = (chartstart1 + 1);
                                    int rangenext01 = (range1 + 1);
                                    int rangenext11 = (rangenext01 + 1);

                                    string x11 = "C" + chartstart1;
                                    string y11 = "C" + range1;
                                    string z11 = "C" + rangenext01;
                                    string t11 = "C" + rangenext11;

                                    string H1 = "D" + chartstart1;
                                    string H2 = "D" + range1;
                                    string H3 = "D" + rangenext01;
                                    string H4 = "D" + rangenext11;

                                    string Med1 = "E" + chartstart1;
                                    string Med2 = "E" + range1;
                                    string Med3 = "E" + rangenext01;
                                    string Med4 = "E" + rangenext11;

                                    string L1 = "F" + chartstart1;
                                    string L2 = "F" + range1;
                                    string L3 = "F" + rangenext01;
                                    string L4 = "F" + rangenext11;

                                    string T1 = "G" + chartstart1;
                                    string T2 = "G" + range1;
                                    string T3 = "G" + rangenext01;
                                    string T4 = "G" + rangenext11;

                                    exWorkSheet1.Cells[H1].Value = CompliedHighCount;
                                    exWorkSheet1.Cells[H2].Value = NotcompliedHighCount;
                                    exWorkSheet1.Cells[H3].Value = NotApplicableHighCount;
                                    exWorkSheet1.Cells[H4].Value = totalHigh;

                                    exWorkSheet1.Cells[Med1].Value = CompliedMedCount;
                                    exWorkSheet1.Cells[Med2].Value = NotcompliedMedCount;
                                    exWorkSheet1.Cells[Med3].Value = NotApplicableMedCount;
                                    exWorkSheet1.Cells[Med4].Value = totalMedium;


                                    exWorkSheet1.Cells[L1].Value = CompliedLowCount;
                                    exWorkSheet1.Cells[L2].Value = NotcompliedLowCount;
                                    exWorkSheet1.Cells[L3].Value = NotApplicableLowCount;
                                    exWorkSheet1.Cells[L4].Value = totalLow;

                                    exWorkSheet1.Cells[T1].Value = compliedCount;
                                    exWorkSheet1.Cells[T2].Value = NotcompliedCount;
                                    exWorkSheet1.Cells[T3].Value = AppliANDNotApplicableCount;
                                    exWorkSheet1.Cells[T4].Value = "";

                                    exWorkSheet1.Cells[x11].Value = "Complied";
                                    exWorkSheet1.Cells[x11 + ":" + x11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells[x11 + ":" + x11].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[y11].Value = "Not Complied";
                                    exWorkSheet1.Cells[y11 + ":" + y11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells[y11 + ":" + y11].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                    exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[z11].Value = "Not Applicable";
                                    exWorkSheet1.Cells[z11 + ":" + z11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells[z11 + ":" + z11].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                    exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells[t11].Value = "Total";
                                    exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                                    #endregion

                                    #region Risk chart
                                    int Rchartstart = ExcelData1.Rows.Count + 17;

                                    int RchartHeader = Rchartstart + 2;
                                    string Rheader = "G" + RchartHeader;
                                    string Rheader1 = "H" + RchartHeader;
                                    string Rheader2 = "I" + RchartHeader;

                                    string Rheadermerge = Rheader + ":" + Rheader2;

                                    exWorkSheet1.Cells[Rheader].Value = "Risk Summary";
                                    exWorkSheet1.Cells[Rheadermerge].Merge = true;
                                    exWorkSheet1.Cells[Rheadermerge].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[Rheader].Style.WrapText = true;


                                    Rchartstart = Rchartstart + 3;
                                    int Rrange = (Rchartstart + 1);
                                    int Rrangenext = (Rrange + 1);
                                    int Rrangenext1 = (Rrangenext + 1);

                                    string Rx = "G" + Rchartstart;
                                    string Ry = "G" + Rrange;
                                    string Rz = "G" + Rrangenext;
                                    string Rt = "G" + Rrangenext1;

                                    string Rpercenatge = "I" + Rchartstart;
                                    string Rpercenatge1 = "I" + Rrange;
                                    string Rpercenatge2 = "I" + Rrangenext;

                                    string Rx1 = "H" + Rchartstart;
                                    string Ry1 = "H" + Rrange;
                                    string Rz1 = "H" + Rrangenext;
                                    string Rt1 = "H" + Rrangenext1;

                                    //exWorkSheet1.Cells[Rpercenatge].Value = string.Format("{0}%", (int)(((double)HighCount / (double)RisktotalPoints) * 100));
                                    //exWorkSheet1.Cells[Rpercenatge1].Value = string.Format("{0}%", (int)(((double)MediumCount / (double)RisktotalPoints) * 100));
                                    //exWorkSheet1.Cells[Rpercenatge2].Value = string.Format("{0}%", (int)(((double)LowCount / (double)RisktotalPoints) * 100));

                                    exWorkSheet1.Cells[Rpercenatge].Value = string.Format("{0:0.00}%", ((double)HighCount / (double)RisktotalPoints) * 100);
                                    exWorkSheet1.Cells[Rpercenatge1].Value = string.Format("{0:0.00}%", ((double)MediumCount / (double)RisktotalPoints) * 100);
                                    exWorkSheet1.Cells[Rpercenatge2].Value = string.Format("{0:0.00}%", ((double)LowCount / (double)RisktotalPoints) * 100);


                                    exWorkSheet1.Cells[Rx1].Value = HighCount;
                                    exWorkSheet1.Cells[Ry1].Value = MediumCount;
                                    exWorkSheet1.Cells[Rz1].Value = LowCount;
                                    exWorkSheet1.Cells[Rt1].Value = RisktotalPoints;

                                    exWorkSheet1.Cells[Rx].Value = "High";
                                    exWorkSheet1.Cells[Ry].Value = "Medium";
                                    exWorkSheet1.Cells[Rz].Value = "Low";
                                    exWorkSheet1.Cells[Rt].Value = "Total";

                                    var RmyChart = exWorkSheet1.Drawings.AddChart("Rchart", eChartType.Pie3D);

                                    string Rs = Rx1 + ":" + Rz1;
                                    string Rs1 = Rx + ":" + Rz;

                                    // Define series for the chart
                                    var Rseries = RmyChart.Series.Add(Rs, Rs1);

                                    var RpieSeries = (ExcelPieChartSerie)Rseries;
                                    RpieSeries.Explosion = 5;
                                    RpieSeries.DataLabel.ShowPercent = true;
                                    RpieSeries.DataLabel.ShowLeaderLines = true;
                                    RpieSeries.DataLabel.Position = eLabelPosition.BestFit;


                                    RmyChart.Border.Fill.Color = System.Drawing.Color.Green;
                                    RmyChart.Title.Text = "Risk Summary";
                                    //myChart.Title.Text = "My Chart";
                                    RmyChart.SetSize(400, 400);
                                    // Add to 6th row and to the 6th column
                                    RmyChart.SetPosition(Rchartstart + 13, 0, 6, 0);

                                    #endregion

                                    using (ExcelRange col = exWorkSheet1.Cells[RchartHeader, 7, RchartHeader + 4, 9])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                    using (ExcelRange col = exWorkSheet1.Cells[17, 1, 17 + ExcelData1.Rows.Count, 12])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    using (ExcelRange col = exWorkSheet1.Cells[chartHeader, 3, chartHeader + 4, 5])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                            }
                        }//Using End
                        #endregion
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("CHANGE_STATUS_PDF"))
                {


                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    long CustomerBranchID = Convert.ToInt64(commandArgs[1]);
                    long VendorID = Convert.ToInt64(commandArgs[2]);
                    long ScheduleOnID = Convert.ToInt64(commandArgs[3]);
                    string AuditorName = Convert.ToString(commandArgs[4]);
                    string ClientName = Convert.ToString(commandArgs[5]);
                    string ForMonth = Convert.ToString(commandArgs[6]);
                    string PONO = "-";
                    string InvoiceNO = "-";
                    string AuditObservation = "-";
                    string AuditRecommendation = "-";

                    string AuditedDate = string.Empty;
                    if (commandArgs[7] != null)
                    {
                        DateTime DT = Convert.ToDateTime(commandArgs[7].ToString());
                        AuditedDate = DT.ToString("dd-MMM-yyyy");
                    }

                    string StartDate = string.Empty;
                    if (commandArgs[8] != null)
                    {
                        DateTime SDDDT = Convert.ToDateTime(commandArgs[8].ToString());
                        StartDate = SDDDT.ToString("dd-MMM-yyyy");
                    }

                    string EndDate = string.Empty;
                    if (commandArgs[9] != null)
                    {
                        DateTime EDDDT = Convert.ToDateTime(commandArgs[9].ToString());
                        EndDate = EDDDT.ToString("dd-MMM-yyyy");
                    }
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        if (AuditID != -1 && CustomerBranchID != -1 && VendorID != -1 && ScheduleOnID != -1)
                        {
                            var instanceDetails = (from row in entities.RLCS_VendorAuditInstance
                                                   where row.ID == AuditID && row.CC_Status == "A"
                                                   && row.IsActive == false
                                                   select row).FirstOrDefault();
                            var ClientDetails = (from row in entities.Customers
                                                 where row.ID == instanceDetails.AVACOM_CustomerID
                                                 select row).FirstOrDefault();
                            if (instanceDetails != null)
                            {
                                //  string clientSPOCName = RLCS_ClientsManagement.GetClientSPOC(instanceDetails.CC_ClientID, ConfigurationManager.AppSettings["RLCSAPIURL"] + "Masters/");
                                string clientSPOCName = "";

                                var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(ScheduleOnID, AuditID)
                                             select row).ToList();

                                PONO = table.Select(entry => entry.PONO).FirstOrDefault();
                                InvoiceNO = table.Select(entry => entry.InVoiceNo).FirstOrDefault();
                                AuditObservation = table.Select(entry => entry.AuditObservation).FirstOrDefault();
                                AuditRecommendation = table.Select(entry => entry.AuditRecommendation).FirstOrDefault();

                                int compliedCount = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                                int NotcompliedCount = table.Where(a => a.ResultStatusID == 2).ToList().Count;
                                int AppliANDNotApplicableCount = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;
                                int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount;

                                int HighCount = table.Where(entry => entry.Risk.Equals("High")).ToList().Count;
                                int LowCount = table.Where(entry => entry.Risk.Equals("Low")).ToList().Count;
                                int MediumCount = table.Where(entry => entry.Risk.Equals("Medium")).ToList().Count;
                                int RisktotalPoints = HighCount + LowCount + MediumCount;

                                int CompliedHighCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("High")).ToList().Count;
                                int CompliedMedCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Medium")).ToList().Count;
                                int CompliedLowCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Low")).ToList().Count;


                                int NotcompliedHighCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("High")).ToList().Count;
                                int NotcompliedMedCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Medium")).ToList().Count;
                                int NotcompliedLowCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Low")).ToList().Count;

                                int NotApplicableHighCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("High")).ToList().Count;
                                int NotApplicableMedCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Medium")).ToList().Count;
                                int NotApplicableLowCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Low")).ToList().Count;

                                int totalHigh = CompliedHighCount + NotcompliedHighCount + NotApplicableHighCount;
                                int totalMedium = CompliedMedCount + NotcompliedMedCount + NotApplicableMedCount;
                                int totalLow = CompliedLowCount + NotcompliedLowCount + NotApplicableLowCount;

                                string fileName = string.Empty;

                                fileName = "AuditReport " + ForMonth + ".pdf";

                                DataTable ExcelData1 = null;
                                DataView view1 = new System.Data.DataView((table as List<SP_RLCS_VendorAudit_DetailsCheckListReport_Result>).ToDataTable());
                                ExcelData1 = view1.ToTable("Selected", false, "SNO", "StateID", "ActName", "NatureOFCompliance", "Risk", "Form", "Section", "ResultStatus", "Audit_Observation", "Recommendation", "TypeOfCompliance", "Description", "Consequences");

                                int cnt = 1;

                                foreach (DataRow item in ExcelData1.Rows)
                                {
                                    item["SNO"] = cnt;
                                    cnt++;
                                }

                                Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
                                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                                pdfDoc.Open();


                                PdfPTable tableogo = new PdfPTable(2);
                                // tableogo.HeaderRows = 1;
                                //Company Logo  
                                PdfPCell Logocell = ImageCell("~/Images/AuditTeamLease.png", 30f, PdfPCell.ALIGN_LEFT);
                                Logocell.Border = PdfPCell.NO_BORDER;
                                tableogo.AddCell(Logocell);
                                tableogo.AddCell(getCell("", PdfPCell.ALIGN_CENTER));
                                pdfDoc.Add(tableogo);


                                Paragraph ReportHeader = new Paragraph("Audit Report Summary", FontFactory.GetFont("Arial", 15, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                ReportHeader.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(ReportHeader);

                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable Invoicetable = new PdfPTable(3);
                                PdfPCell cellInvoiceNo = new PdfPCell(new Phrase("Invoice No", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellInvoiceNo.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                Invoicetable.AddCell(cellInvoiceNo);
                                PdfPCell cellPONo = new PdfPCell(new Phrase("P.O No", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellPONo.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                Invoicetable.AddCell(cellPONo);
                                PdfPCell cellCondFor = new PdfPCell(new Phrase("Conducted For", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellCondFor.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                Invoicetable.AddCell(cellCondFor);

                                Invoicetable.AddCell(InvoiceNO);
                                Invoicetable.AddCell(PONO);
                                Invoicetable.AddCell(ClientDetails.Name);
                                pdfDoc.Add(Invoicetable);

                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable AuditNametable = new PdfPTable(4);
                                PdfPCell cellCAuditorName = new PdfPCell(new Phrase("Auditor Name", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellCAuditorName.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellCAuditorName);
                                PdfPCell cellUnit = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellUnit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellUnit);
                                PdfPCell cellDateOfAudit = new PdfPCell(new Phrase("Date Of Audit", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellDateOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellDateOfAudit);
                                PdfPCell cellPeriodOfAudit = new PdfPCell(new Phrase("Period Of Audit", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellPeriodOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellPeriodOfAudit);

                                AuditNametable.AddCell(AuditorName);
                                AuditNametable.AddCell(instanceDetails.CC_ContractorName + ',' + instanceDetails.CC_BranchID);
                                AuditNametable.AddCell(StartDate);
                                //AuditNametable.AddCell(AuditedDate);
                                AuditNametable.AddCell(ForMonth);
                                pdfDoc.Add(AuditNametable);

                                pdfDoc.Add(Chunk.NEWLINE);


                                Paragraph OverallCmplScoreHeader = new Paragraph("Overall Compliance Score %", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                OverallCmplScoreHeader.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(OverallCmplScoreHeader);

                                string CopmliedCountPercentage = string.Format("{0:0.00}", (((double)compliedCount + (double)AppliANDNotApplicableCount) / (double)totalPoints) * 100);
                                string NotCopmliedCountPercentage = string.Format("{0:0.00}", ((double)NotcompliedCount / (double)totalPoints) * 100);
                               // string ApllicableandNotApllicableCountPercentage = string.Format("{0:0.00}", ((double)AppliANDNotApplicableCount / (double)totalPoints) * 100);


                                double[] yValues = { Convert.ToDouble(CopmliedCountPercentage), Convert.ToDouble(NotCopmliedCountPercentage)};
                                string[] xValues = { "Complied", "Not Complied" };
                                Chart1.Series["Default"].Points.DataBindXY(xValues, yValues);

                                Chart1.Series["Default"].Points[0].Color = Color.LightGreen;
                                Chart1.Series["Default"].Points[1].Color = Color.LightPink;
                              

                                Chart1.Series["Default"].ChartType = SeriesChartType.Doughnut;
                                //  Chart1.Series["Default"]["PieLabelStyle"] = "Disabled";
                                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                Chart1.Legends[0].Enabled = true;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    Chart1.SaveImage(stream, ChartImageFormat.Png);
                                    iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                                    chartImage.ScalePercent(75f);
                                    chartImage.Alignment = Element.ALIGN_CENTER;
                                    pdfDoc.Add(chartImage);
                                }



                                pdfDoc.Add(Chunk.NEWLINE);

                                Paragraph OverallCmplScoreHeader1 = new Paragraph(" Compliance % By Compliance Type", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                OverallCmplScoreHeader1.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(OverallCmplScoreHeader1);


                                pdfDoc.Add(Chunk.NEWLINE);

                                List<string> compliedstatus = new List<string>();
                                compliedstatus.Add("Complied");
                                compliedstatus.Add("Not Applicable");

                                foreach (string status in compliedstatus)
                                {
                                    string[] x = new string[5] { "Returns", "Registers", "Remittance", "Record", "License" };

                                    int Regsiter1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Registers" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Return1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Returns" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Document1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Remittance" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int License1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "License" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Record1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Record" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Remittance1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Remittance" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Abstracts1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Abstracts" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();

                                    double Regsiterp1 = (((double)Regsiter1 / (double)totalPoints) * 100);
                                    double Returnp1 = (((double)Return1 / (double)totalPoints) * 100);
                                    double Documentp1 = (((double)Document1 / (double)totalPoints) * 100);
                                    double Licensep1 = (((double)License1 / (double)totalPoints) * 100);
                                    double Recordp1 = (((double)Record1 / (double)totalPoints) * 100);
                                    double Remittancep1 = (((double)Remittance1 / (double)totalPoints) * 100);
                                    double Abstractsp1 = (((double)Abstracts1 / (double)totalPoints) * 100);

                                    Regsiterp1 = Convert.ToDouble(String.Format("{0:0.00}", Regsiterp1));
                                    Returnp1 = Convert.ToDouble(String.Format("{0:0.00}", Returnp1));
                                    Documentp1 = Convert.ToDouble(String.Format("{0:0.00}", Documentp1));
                                    Licensep1 = Convert.ToDouble(String.Format("{0:0.00}", Licensep1));
                                    Recordp1 = Convert.ToDouble(String.Format("{0:0.00}", Recordp1));
                                    Remittancep1 = Convert.ToDouble(String.Format("{0:0.00}", Remittancep1));
                                    Abstractsp1 = Convert.ToDouble(String.Format("{0:0.00}", Abstractsp1));


                                    double[] y = new double[5] { Returnp1, Regsiterp1, Remittancep1, Recordp1, Licensep1 };


                                    Chart4.Series.Add(new Series(status));
                                    Chart4.Series[status].IsValueShownAsLabel = true;
                                    Chart4.Series[status].ChartType = SeriesChartType.StackedBar;
                                    Chart4.Series[status].Points.DataBindXY(x, y);

                                    Chart4.Series[status].Points[0].Color = Color.LightPink;
                                    Chart4.Series[status].Points[1].Color = Color.LightGreen;
                                }

                                Chart4.Legends[0].Enabled = true;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    Chart4.SaveImage(stream, ChartImageFormat.Png);
                                    iTextSharp.text.Image chartImage4 = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                                    chartImage4.ScalePercent(75f);
                                    chartImage4.Alignment = Element.ALIGN_CENTER;
                                    pdfDoc.Add(chartImage4);
                                }

                                pdfDoc.Add(Chunk.NEWLINE);


                                Paragraph OverallCmplScoreHeaderRisk = new Paragraph(" Compliance % By Risk", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                OverallCmplScoreHeaderRisk.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(OverallCmplScoreHeaderRisk);





                                foreach (string status in compliedstatus)
                                {
                                    string[] x = new string[3] { "High", "Medium", "Low" };

                                    double High = (((double)HighCount / (double)RisktotalPoints) * 100);
                                    double Medium = (((double)MediumCount / (double)RisktotalPoints) * 100);
                                    double Low = (((double)LowCount / (double)RisktotalPoints) * 100);

                                    High = Convert.ToDouble(String.Format("{0:0.00}", High));
                                    Medium = Convert.ToDouble(String.Format("{0:0.00}", Medium));
                                    Low = Convert.ToDouble(String.Format("{0:0.00}", Low));


                                    double[] y = new double[3] { High, Medium, Low };


                                    Chart3.Series.Add(new Series(status));
                                    Chart3.Series[status].IsValueShownAsLabel = true;
                                    Chart3.Series[status].ChartType = SeriesChartType.StackedBar;
                                    Chart3.Series[status].Points.DataBindXY(x, y);

                                    Chart3.Series[status].Points[0].Color = Color.LightGreen;
                                    Chart3.Series[status].Points[1].Color = Color.LightPink;
                                }


                                //Chart3.Series["Complied"].Points[0].Color = Color.LightPink;
                                //Chart3.Series["Complied"].Points[1].Color = Color.LightGreen;
                                //Chart3.Series["Not Applicable"].Points[0].Color = Color.LightPink;
                                //Chart3.Series["Not Applicable"].Points[1].Color = Color.LightGreen;

                                Chart4.Legends[0].Enabled = true;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    Chart3.SaveImage(stream, ChartImageFormat.Png);
                                    iTextSharp.text.Image chartImage3 = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                                    chartImage3.ScalePercent(75f);
                                    chartImage3.Alignment = Element.ALIGN_CENTER;
                                    pdfDoc.Add(chartImage3);
                                }

                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable CompliancesStatustable = new PdfPTable(3);
                                PdfPCell cellCompliancesStatusCount = new PdfPCell(new Phrase("Compliances Status Count", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                cellCompliancesStatusCount.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellCompliancesStatusCount.Colspan = 3;
                                cellCompliancesStatusCount.HorizontalAlignment = Element.ALIGN_CENTER;
                                CompliancesStatustable.AddCell(cellCompliancesStatusCount);
                                PdfPCell cellCompliancesCleared = new PdfPCell(new Phrase("Compliances Cleared", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                CompliancesStatustable.AddCell(cellCompliancesCleared);
                                PdfPCell cellCompliancesNotCleared = new PdfPCell(new Phrase("Compliances Not Cleared", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                CompliancesStatustable.AddCell(cellCompliancesNotCleared);
                                PdfPCell cellCompliancesNotApplicable = new PdfPCell(new Phrase("Compliances Not Applicable", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                CompliancesStatustable.AddCell(cellCompliancesNotApplicable);
                                CompliancesStatustable.AddCell(compliedCount.ToString());
                                CompliancesStatustable.AddCell(NotcompliedCount.ToString());
                                CompliancesStatustable.AddCell(AppliANDNotApplicableCount.ToString());
                                pdfDoc.Add(CompliancesStatustable);

                                pdfDoc.Add(Chunk.NEWLINE);
                                //Third Table 
                                PdfPTable AuditObservationstable = new PdfPTable(2);

                                PdfPCell cellAuditObservations = new PdfPCell(new Phrase("Summary Of Audit Observations", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                cellAuditObservations.Colspan = 2;
                                cellAuditObservations.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellAuditObservations.HorizontalAlignment = Element.ALIGN_CENTER;
                                AuditObservationstable.AddCell(cellAuditObservations);

                                AuditObservationstable.AddCell("Auditor Observation");
                                AuditObservationstable.AddCell("Auditor Recommendation");
                                AuditObservationstable.AddCell(AuditObservation);
                                AuditObservationstable.AddCell(AuditRecommendation);



                                pdfDoc.Add(AuditObservationstable);
                                //End Third Table
                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable Objectivestable = new PdfPTable(1);
                                PdfPCell cellObjectivesOfAudit = new PdfPCell(new Phrase("Objectives Of The Audit", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                cellObjectivesOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellObjectivesOfAudit.HorizontalAlignment = Element.ALIGN_CENTER;
                                cellObjectivesOfAudit.Colspan = 1;
                                Objectivestable.AddCell(cellObjectivesOfAudit);
                                Objectivestable.AddCell("To Check Compliance with the Invoice Submitted.");
                                pdfDoc.Add(Objectivestable);

                                pdfDoc.Add(Chunk.NEWLINE);
                                PdfPTable ScopeOfAudittable = new PdfPTable(1);
                                PdfPCell cellScopeOfAudit = new PdfPCell(new Phrase("Scope Of The Audit Review", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                cellScopeOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellScopeOfAudit.HorizontalAlignment = Element.ALIGN_CENTER;
                                cellScopeOfAudit.Colspan = 1;
                                ScopeOfAudittable.AddCell(cellScopeOfAudit);
                                PdfPCell cellListOfActs = new PdfPCell(new Phrase("List Of Acts Covered Under The Audit", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                ScopeOfAudittable.AddCell(cellListOfActs);
                                var distinctValues = ExcelData1.AsEnumerable()
                                                .Select(row => new
                                                {
                                                    ActName = row.Field<string>("ActName")
                                                })
                                                .Distinct();

                                foreach (var item in distinctValues)
                                {
                                    ScopeOfAudittable.AddCell(item.ActName);
                                }
                                pdfDoc.Add(ScopeOfAudittable);
                                pdfDoc.Add(Chunk.NEWLINE);


                                PdfPTable ListOfCompliancestable = new PdfPTable(5);
                                ListOfCompliancestable.HeaderRows = 2;
                                PdfPCell cellListOfCompliances = new PdfPCell(new Phrase("List Of Compliances", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                cellListOfCompliances.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellListOfCompliances.HorizontalAlignment = Element.ALIGN_CENTER;
                                cellListOfCompliances.Colspan = 5;
                                ListOfCompliancestable.AddCell(cellListOfCompliances);
                                PdfPCell cellCompany = new PdfPCell(new Phrase("Company", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellCompany);
                                PdfPCell cellLocation = new PdfPCell(new Phrase("Location", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellLocation);
                                PdfPCell cellDescription = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellDescription);
                                PdfPCell cellRisk = new PdfPCell(new Phrase("Risk", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellRisk);
                                PdfPCell cellStatus = new PdfPCell(new Phrase("Status", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellStatus);

                                foreach (var item in distinctValues)
                                {

                                    PdfPCell cellActName = new PdfPCell(new Phrase(item.ActName, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                    cellActName.Colspan = 6;
                                    cellActName.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ListOfCompliancestable.AddCell(cellActName);
                                    var results = (from myRow in ExcelData1.AsEnumerable()
                                                   where myRow.Field<string>("ActName") == item.ActName
                                                   select myRow).Distinct();
                                    foreach (var item1 in results)
                                    {
                                        ListOfCompliancestable.AddCell(instanceDetails.CC_ContractorName + ',' + instanceDetails.CC_ContractorAddress);
                                        ListOfCompliancestable.AddCell(instanceDetails.CC_BranchID);
                                        ListOfCompliancestable.AddCell(item1[11].ToString());
                                        ListOfCompliancestable.AddCell(item1[4].ToString());
                                        ListOfCompliancestable.AddCell(item1[7].ToString());
                                    }
                                }
                                pdfDoc.Add(ListOfCompliancestable);


                                pdfDoc.Add(Chunk.NEWLINE);
                                var ListOfNotCompliance = (from myRow in ExcelData1.AsEnumerable()
                                                           where myRow.Field<string>("ResultStatus") == "Not Complied"
                                                           select myRow).Distinct();

                                int NCID = 1;
                                foreach (var item1 in ListOfNotCompliance)
                                {

                                    PdfPTable ListOfNotCompliancestable = new PdfPTable(2);
                                    PdfPCell cellNatureOfCompliances = new PdfPCell(new Phrase("NC ID: # " + NCID + item1[3].ToString(), FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                    cellNatureOfCompliances.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                    cellNatureOfCompliances.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cellNatureOfCompliances.Colspan = 2;
                                    ListOfNotCompliancestable.AddCell(cellNatureOfCompliances);
                                    PdfPCell cellAct = new PdfPCell(new Phrase("Act", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellAct);
                                    PdfPCell cellRule = new PdfPCell(new Phrase("Rule", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellRule);

                                    ListOfNotCompliancestable.AddCell(item1[2].ToString());
                                    ListOfNotCompliancestable.AddCell(item1[6].ToString());

                                    PdfPCell cellNCDescription = new PdfPCell(new Phrase("Description -" + item1[11].ToString()));
                                    cellNCDescription.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cellNCDescription.Colspan = 2;
                                    ListOfNotCompliancestable.AddCell(cellNCDescription);

                                    PdfPCell cellNCConsequence = new PdfPCell(new Phrase("Consequence -" + item1[12].ToString()));
                                    cellNCConsequence.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cellNCConsequence.Colspan = 2;
                                    ListOfNotCompliancestable.AddCell(cellNCConsequence);

                                    PdfPCell cellNCObservation = new PdfPCell(new Phrase("Audit Observation", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellNCObservation);
                                    PdfPCell cellNCRecommendation = new PdfPCell(new Phrase("Audit Recommendation", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellNCRecommendation);

                                    ListOfNotCompliancestable.AddCell(item1[8].ToString());
                                    ListOfNotCompliancestable.AddCell(item1[9].ToString());


                                    pdfDoc.Add(ListOfNotCompliancestable);

                                    pdfDoc.Add(Chunk.NEWLINE);
                                    NCID++;
                                }

                                pdfWriter.CloseStream = false;
                                pdfDoc.Close();

                                Response.Buffer = true;
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.Write(pdfDoc);
                                //  Response.End();


                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                        }
                    }


                }
                else if (e.CommandName.Equals("CHANGE_STATUS_DraftPDF"))
                {


                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    long AuditID = Convert.ToInt64(commandArgs[0]);
                    long CustomerBranchID = Convert.ToInt64(commandArgs[1]);
                    long VendorID = Convert.ToInt64(commandArgs[2]);
                    long ScheduleOnID = Convert.ToInt64(commandArgs[3]);
                    string AuditorName = Convert.ToString(commandArgs[4]);
                    string ClientName = Convert.ToString(commandArgs[5]);
                    string ForMonth = Convert.ToString(commandArgs[6]);
                    string PONO = "-";
                    string InvoiceNO = "-";
                    string AuditObservation = "-";
                    string AuditRecommendation = "-";

                    string AuditedDate = string.Empty;
                    if (commandArgs[7] != null)
                    {
                        DateTime DT = Convert.ToDateTime(commandArgs[7].ToString());
                        AuditedDate = DT.ToString("dd-MMM-yyyy");
                    }

                    string StartDate = string.Empty;
                    if (commandArgs[8] != null)
                    {
                        DateTime SDDDT = Convert.ToDateTime(commandArgs[8].ToString());
                        StartDate = SDDDT.ToString("dd-MMM-yyyy");
                    }

                    string EndDate = string.Empty;
                    if (commandArgs[9] != null && commandArgs[9] != "")
                    {
                        DateTime EDDDT = Convert.ToDateTime(commandArgs[9].ToString());
                        EndDate = EDDDT.ToString("dd-MMM-yyyy");
                    }
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        if (AuditID != -1 && CustomerBranchID != -1 && VendorID != -1 && ScheduleOnID != -1)
                        {
                            var instanceDetails = (from row in entities.RLCS_VendorAuditInstance
                                                   where row.ID == AuditID && row.CC_Status == "A"
                                                   && row.IsActive == false
                                                   select row).FirstOrDefault();
                            var ClientDetails = (from row in entities.Customers
                                                 where row.ID == instanceDetails.AVACOM_CustomerID
                                                 select row).FirstOrDefault();
                            if (instanceDetails != null)
                            {
                                // string clientSPOCName = RLCS_ClientsManagement.GetClientSPOC(instanceDetails.CC_ClientID, ConfigurationManager.AppSettings["RLCSAPIURL"] + "Masters/");
                                string clientSPOCName = "";

                                var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(ScheduleOnID, AuditID)
                                             select row).ToList();

                                PONO = table.Select(entry => entry.PONO).FirstOrDefault();
                                InvoiceNO = table.Select(entry => entry.InVoiceNo).FirstOrDefault();
                                AuditObservation = table.Select(entry => entry.AuditObservation).FirstOrDefault();
                                AuditRecommendation = table.Select(entry => entry.AuditRecommendation).FirstOrDefault();

                                int compliedCount = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                                int NotcompliedCount = table.Where(a => a.ResultStatusID == 2).ToList().Count;
                                int AppliANDNotApplicableCount = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;
                                int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount;

                                int HighCount = table.Where(entry => entry.Risk.Equals("High")).ToList().Count;
                                int LowCount = table.Where(entry => entry.Risk.Equals("Low")).ToList().Count;
                                int MediumCount = table.Where(entry => entry.Risk.Equals("Medium")).ToList().Count;
                                int RisktotalPoints = HighCount + LowCount + MediumCount;

                                int CompliedHighCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("High")).ToList().Count;
                                int CompliedMedCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Medium")).ToList().Count;
                                int CompliedLowCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Low")).ToList().Count;


                                int NotcompliedHighCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("High")).ToList().Count;
                                int NotcompliedMedCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Medium")).ToList().Count;
                                int NotcompliedLowCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Low")).ToList().Count;

                                int NotApplicableHighCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("High")).ToList().Count;
                                int NotApplicableMedCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Medium")).ToList().Count;
                                int NotApplicableLowCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Low")).ToList().Count;

                                int totalHigh = CompliedHighCount + NotcompliedHighCount + NotApplicableHighCount;
                                int totalMedium = CompliedMedCount + NotcompliedMedCount + NotApplicableMedCount;
                                int totalLow = CompliedLowCount + NotcompliedLowCount + NotApplicableLowCount;

                                string fileName = string.Empty;

                                fileName = "AuditReport " + ForMonth + ".pdf";

                                DataTable ExcelData1 = null;
                                DataView view1 = new System.Data.DataView((table as List<SP_RLCS_VendorAudit_DetailsCheckListReport_Result>).ToDataTable());
                                ExcelData1 = view1.ToTable("Selected", false, "SNO", "StateID", "ActName", "NatureOFCompliance", "Risk", "Form", "Section", "ResultStatus", "Audit_Observation", "Recommendation", "TypeOfCompliance", "Description", "Consequences");

                                int cnt = 1;

                                foreach (DataRow item in ExcelData1.Rows)
                                {
                                    item["SNO"] = cnt;
                                    cnt++;
                                }

                                Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
                                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                                pdfDoc.Open();


                                PdfPTable tableogo = new PdfPTable(2);
                                // tableogo.HeaderRows = 1;
                                //Company Logo
                                PdfPCell Logocell = ImageCell("~/Images/AuditTeamLease.png", 30f, PdfPCell.ALIGN_LEFT);
                                Logocell.Border = PdfPCell.NO_BORDER;
                                tableogo.AddCell(Logocell);
                                tableogo.AddCell(getCell("", PdfPCell.ALIGN_CENTER));
                                pdfDoc.Add(tableogo);


                                Paragraph ReportHeader = new Paragraph("Audit Report Summary", FontFactory.GetFont("Arial", 15, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                ReportHeader.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(ReportHeader);

                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable Invoicetable = new PdfPTable(3);
                                PdfPCell cellInvoiceNo = new PdfPCell(new Phrase("Invoice No", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellInvoiceNo.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                Invoicetable.AddCell(cellInvoiceNo);
                                PdfPCell cellPONo = new PdfPCell(new Phrase("P.O No", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellPONo.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                Invoicetable.AddCell(cellPONo);
                                PdfPCell cellCondFor = new PdfPCell(new Phrase("Conducted For", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellCondFor.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                Invoicetable.AddCell(cellCondFor);

                                Invoicetable.AddCell(InvoiceNO);
                                Invoicetable.AddCell(PONO);
                                Invoicetable.AddCell(ClientDetails.Name);
                                pdfDoc.Add(Invoicetable);

                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable AuditNametable = new PdfPTable(4);
                                PdfPCell cellCAuditorName = new PdfPCell(new Phrase("Auditor Name", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellCAuditorName.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellCAuditorName);
                                PdfPCell cellUnit = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellUnit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellUnit);
                                PdfPCell cellDateOfAudit = new PdfPCell(new Phrase("Date Of Audit", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellDateOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellDateOfAudit);
                                PdfPCell cellPeriodOfAudit = new PdfPCell(new Phrase("Period Of Audit", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                cellPeriodOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                AuditNametable.AddCell(cellPeriodOfAudit);

                                AuditNametable.AddCell(AuditorName);
                                AuditNametable.AddCell(instanceDetails.CC_ContractorName + ',' + instanceDetails.CC_BranchID);
                                //AuditNametable.AddCell(AuditedDate);
                                AuditNametable.AddCell(StartDate);
                                AuditNametable.AddCell(ForMonth);
                                pdfDoc.Add(AuditNametable);

                                pdfDoc.Add(Chunk.NEWLINE);


                                Paragraph OverallCmplScoreHeader = new Paragraph("Overall Compliance Score %", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                OverallCmplScoreHeader.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(OverallCmplScoreHeader);

                                string CopmliedCountPercentage = string.Format("{0:0.00}", (((double)compliedCount + (double)AppliANDNotApplicableCount) / (double)totalPoints) * 100);
                                string NotCopmliedCountPercentage = string.Format("{0:0.00}", ((double)NotcompliedCount / (double)totalPoints) * 100);
                               // string ApllicableandNotApllicableCountPercentage = string.Format("{0:0.00}", ((double)AppliANDNotApplicableCount / (double)totalPoints) * 100);


                                double[] yValues = { Convert.ToDouble(CopmliedCountPercentage), Convert.ToDouble(NotCopmliedCountPercentage) };
                                string[] xValues = { "Complied", "Not Complied" };
                                Chart1.Series["Default"].Points.DataBindXY(xValues, yValues);

                                Chart1.Series["Default"].Points[0].Color = Color.LightGreen;
                                Chart1.Series["Default"].Points[1].Color = Color.LightPink;
                               

                                Chart1.Series["Default"].ChartType = SeriesChartType.Doughnut;
                                //  Chart1.Series["Default"]["PieLabelStyle"] = "Disabled";
                                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                                Chart1.Legends[0].Enabled = true;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    Chart1.SaveImage(stream, ChartImageFormat.Png);
                                    iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                                    chartImage.ScalePercent(75f);
                                    chartImage.Alignment = Element.ALIGN_CENTER;
                                    pdfDoc.Add(chartImage);
                                }



                                pdfDoc.Add(Chunk.NEWLINE);

                                Paragraph OverallCmplScoreHeader1 = new Paragraph(" Compliance % By Compliance Type", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                OverallCmplScoreHeader1.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(OverallCmplScoreHeader1);


                                pdfDoc.Add(Chunk.NEWLINE);

                                List<string> compliedstatus = new List<string>();
                                compliedstatus.Add("Complied");
                                compliedstatus.Add("Not Applicable");

                                foreach (string status in compliedstatus)
                                {
                                    string[] x = new string[5] { "Returns", "Registers", "Remittance", "Record", "License" };

                                    int Regsiter1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Registers" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Return1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Returns" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Document1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Remittance" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int License1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "License" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Record1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Record" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Remittance1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Remittance" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();
                                    int Abstracts1 = (from myRow in ExcelData1.AsEnumerable() where myRow.Field<string>("TypeOfCompliance") == "Abstracts" && myRow.Field<string>("ResultStatus") == status select myRow).ToList().Count();

                                    double Regsiterp1 = (((double)Regsiter1 / (double)totalPoints) * 100);
                                    double Returnp1 = (((double)Return1 / (double)totalPoints) * 100);
                                    double Documentp1 = (((double)Document1 / (double)totalPoints) * 100);
                                    double Licensep1 = (((double)License1 / (double)totalPoints) * 100);
                                    double Recordp1 = (((double)Record1 / (double)totalPoints) * 100);
                                    double Remittancep1 = (((double)Remittance1 / (double)totalPoints) * 100);
                                    double Abstractsp1 = (((double)Abstracts1 / (double)totalPoints) * 100);

                                    Regsiterp1 = Convert.ToDouble(String.Format("{0:0.00}", Regsiterp1));
                                    Returnp1 = Convert.ToDouble(String.Format("{0:0.00}", Returnp1));
                                    Documentp1 = Convert.ToDouble(String.Format("{0:0.00}", Documentp1));
                                    Licensep1 = Convert.ToDouble(String.Format("{0:0.00}", Licensep1));
                                    Recordp1 = Convert.ToDouble(String.Format("{0:0.00}", Recordp1));
                                    Remittancep1 = Convert.ToDouble(String.Format("{0:0.00}", Remittancep1));
                                    Abstractsp1 = Convert.ToDouble(String.Format("{0:0.00}", Abstractsp1));


                                    double[] y = new double[5] { Returnp1, Regsiterp1, Remittancep1, Recordp1, Licensep1 };


                                    Chart4.Series.Add(new Series(status));
                                    Chart4.Series[status].IsValueShownAsLabel = true;
                                    Chart4.Series[status].ChartType = SeriesChartType.StackedBar;
                                    Chart4.Series[status].Points.DataBindXY(x, y);

                                    Chart4.Series[status].Points[0].Color = Color.LightPink;
                                    Chart4.Series[status].Points[1].Color = Color.LightGreen;
                                }

                                Chart4.Legends[0].Enabled = true;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    Chart4.SaveImage(stream, ChartImageFormat.Png);
                                    iTextSharp.text.Image chartImage4 = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                                    chartImage4.ScalePercent(75f);
                                    chartImage4.Alignment = Element.ALIGN_CENTER;
                                    pdfDoc.Add(chartImage4);
                                }

                                pdfDoc.Add(Chunk.NEWLINE);


                                Paragraph OverallCmplScoreHeaderRisk = new Paragraph(" Compliance % By Risk", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                                OverallCmplScoreHeaderRisk.Alignment = Element.ALIGN_CENTER;
                                pdfDoc.Add(OverallCmplScoreHeaderRisk);





                                foreach (string status in compliedstatus)
                                {
                                    string[] x = new string[3] { "High", "Medium", "Low" };

                                    double High = (((double)HighCount / (double)RisktotalPoints) * 100);
                                    double Medium = (((double)MediumCount / (double)RisktotalPoints) * 100);
                                    double Low = (((double)LowCount / (double)RisktotalPoints) * 100);

                                    High = Convert.ToDouble(String.Format("{0:0.00}", High));
                                    Medium = Convert.ToDouble(String.Format("{0:0.00}", Medium));
                                    Low = Convert.ToDouble(String.Format("{0:0.00}", Low));


                                    double[] y = new double[3] { High, Medium, Low };


                                    Chart3.Series.Add(new Series(status));
                                    Chart3.Series[status].IsValueShownAsLabel = true;
                                    Chart3.Series[status].ChartType = SeriesChartType.StackedBar;
                                    Chart3.Series[status].Points.DataBindXY(x, y);

                                    Chart3.Series[status].Points[0].Color = Color.LightGreen;
                                    Chart3.Series[status].Points[1].Color = Color.LightPink;
                                }


                                //Chart3.Series["Complied"].Points[0].Color = Color.LightPink;
                                //Chart3.Series["Complied"].Points[1].Color = Color.LightGreen;
                                //Chart3.Series["Not Applicable"].Points[0].Color = Color.LightPink;
                                //Chart3.Series["Not Applicable"].Points[1].Color = Color.LightGreen;

                                Chart4.Legends[0].Enabled = true;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    Chart3.SaveImage(stream, ChartImageFormat.Png);
                                    iTextSharp.text.Image chartImage3 = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                                    chartImage3.ScalePercent(75f);
                                    chartImage3.Alignment = Element.ALIGN_CENTER;
                                    pdfDoc.Add(chartImage3);
                                }

                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable CompliancesStatustable = new PdfPTable(3);
                                PdfPCell cellCompliancesStatusCount = new PdfPCell(new Phrase("Compliances Status Count", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                cellCompliancesStatusCount.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellCompliancesStatusCount.Colspan = 3;
                                cellCompliancesStatusCount.HorizontalAlignment = Element.ALIGN_CENTER;
                                CompliancesStatustable.AddCell(cellCompliancesStatusCount);
                                PdfPCell cellCompliancesCleared = new PdfPCell(new Phrase("Compliances Cleared", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                CompliancesStatustable.AddCell(cellCompliancesCleared);
                                PdfPCell cellCompliancesNotCleared = new PdfPCell(new Phrase("Compliances Not Cleared", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                CompliancesStatustable.AddCell(cellCompliancesNotCleared);
                                PdfPCell cellCompliancesNotApplicable = new PdfPCell(new Phrase("Compliances Not Applicable", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                CompliancesStatustable.AddCell(cellCompliancesNotApplicable);
                                CompliancesStatustable.AddCell(compliedCount.ToString());
                                CompliancesStatustable.AddCell(NotcompliedCount.ToString());
                                CompliancesStatustable.AddCell(AppliANDNotApplicableCount.ToString());
                                pdfDoc.Add(CompliancesStatustable);

                                pdfDoc.Add(Chunk.NEWLINE);
                                //Third Table 
                                PdfPTable AuditObservationstable = new PdfPTable(2);

                                PdfPCell cellAuditObservations = new PdfPCell(new Phrase("Summary Of Audit Observations", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                cellAuditObservations.Colspan = 2;
                                cellAuditObservations.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellAuditObservations.HorizontalAlignment = Element.ALIGN_CENTER;
                                AuditObservationstable.AddCell(cellAuditObservations);

                                AuditObservationstable.AddCell("Auditor Observation");
                                AuditObservationstable.AddCell("Auditor Recommendation");
                                AuditObservationstable.AddCell(AuditObservation);
                                AuditObservationstable.AddCell(AuditRecommendation);



                                pdfDoc.Add(AuditObservationstable);
                                //End Third Table
                                pdfDoc.Add(Chunk.NEWLINE);

                                PdfPTable Objectivestable = new PdfPTable(1);
                                PdfPCell cellObjectivesOfAudit = new PdfPCell(new Phrase("Objectives Of The Audit", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                cellObjectivesOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellObjectivesOfAudit.HorizontalAlignment = Element.ALIGN_CENTER;
                                cellObjectivesOfAudit.Colspan = 1;
                                Objectivestable.AddCell(cellObjectivesOfAudit);
                                Objectivestable.AddCell("To Check Compliance with the Invoice Submitted.");
                                pdfDoc.Add(Objectivestable);

                                pdfDoc.Add(Chunk.NEWLINE);
                                PdfPTable ScopeOfAudittable = new PdfPTable(1);
                                PdfPCell cellScopeOfAudit = new PdfPCell(new Phrase("Scope Of The Audit Review", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                cellScopeOfAudit.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellScopeOfAudit.HorizontalAlignment = Element.ALIGN_CENTER;
                                cellScopeOfAudit.Colspan = 1;
                                ScopeOfAudittable.AddCell(cellScopeOfAudit);
                                PdfPCell cellListOfActs = new PdfPCell(new Phrase("List Of Acts Covered Under The Audit", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                ScopeOfAudittable.AddCell(cellListOfActs);
                                var distinctValues = ExcelData1.AsEnumerable()
                                                .Select(row => new
                                                {
                                                    ActName = row.Field<string>("ActName")
                                                })
                                                .Distinct();

                                foreach (var item in distinctValues)
                                {
                                    ScopeOfAudittable.AddCell(item.ActName);
                                }
                                pdfDoc.Add(ScopeOfAudittable);
                                pdfDoc.Add(Chunk.NEWLINE);


                                PdfPTable ListOfCompliancestable = new PdfPTable(5);
                                ListOfCompliancestable.HeaderRows = 2;
                                PdfPCell cellListOfCompliances = new PdfPCell(new Phrase("List Of Compliances", FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                cellListOfCompliances.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                cellListOfCompliances.HorizontalAlignment = Element.ALIGN_CENTER;
                                cellListOfCompliances.Colspan = 5;
                                ListOfCompliancestable.AddCell(cellListOfCompliances);
                                PdfPCell cellCompany = new PdfPCell(new Phrase("Company", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellCompany);
                                PdfPCell cellLocation = new PdfPCell(new Phrase("Location", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellLocation);
                                PdfPCell cellDescription = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellDescription);
                                PdfPCell cellRisk = new PdfPCell(new Phrase("Risk", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellRisk);
                                PdfPCell cellStatus = new PdfPCell(new Phrase("Status", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                ListOfCompliancestable.AddCell(cellStatus);

                                foreach (var item in distinctValues)
                                {

                                    PdfPCell cellActName = new PdfPCell(new Phrase(item.ActName, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                                    cellActName.Colspan = 6;
                                    cellActName.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ListOfCompliancestable.AddCell(cellActName);
                                    var results = (from myRow in ExcelData1.AsEnumerable()
                                                   where myRow.Field<string>("ActName") == item.ActName
                                                   select myRow).Distinct(); 
                                    foreach (var item1 in results)
                                    {
                                        ListOfCompliancestable.AddCell(instanceDetails.CC_ContractorName + ',' + instanceDetails.CC_ContractorAddress);
                                        ListOfCompliancestable.AddCell(instanceDetails.CC_BranchID);
                                        ListOfCompliancestable.AddCell(item1[11].ToString());
                                        ListOfCompliancestable.AddCell(item1[4].ToString());
                                        // ListOfCompliancestable.AddCell(item1[7].ToString());
                                        ListOfCompliancestable.AddCell(item1[7].ToString());

                                    }


                                }
                                pdfDoc.Add(ListOfCompliancestable);


                                pdfDoc.Add(Chunk.NEWLINE);
                                var ListOfNotCompliance = (from myRow in ExcelData1.AsEnumerable()
                                                           where myRow.Field<string>("ResultStatus") == "Not Complied"
                                                           select myRow).Distinct();

                                int NCID = 1;
                                foreach (var item1 in ListOfNotCompliance)
                                {

                                    PdfPTable ListOfNotCompliancestable = new PdfPTable(2);
                                    PdfPCell cellNatureOfCompliances = new PdfPCell(new Phrase("NC ID: # " + NCID + item1[3].ToString(), FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD)));
                                    cellNatureOfCompliances.BackgroundColor = new BaseColor(System.Drawing.Color.LightGoldenrodYellow);
                                    cellNatureOfCompliances.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cellNatureOfCompliances.Colspan = 2;
                                    ListOfNotCompliancestable.AddCell(cellNatureOfCompliances);
                                    PdfPCell cellAct = new PdfPCell(new Phrase("Act", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellAct);
                                    PdfPCell cellRule = new PdfPCell(new Phrase("Rule", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellRule);

                                    ListOfNotCompliancestable.AddCell(item1[2].ToString());
                                    ListOfNotCompliancestable.AddCell(item1[6].ToString());

                                    PdfPCell cellNCDescription = new PdfPCell(new Phrase("Description -" + item1[11].ToString()));
                                    cellNCDescription.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cellNCDescription.Colspan = 2;
                                    ListOfNotCompliancestable.AddCell(cellNCDescription);

                                    PdfPCell cellNCConsequence = new PdfPCell(new Phrase("Consequence -" + item1[12].ToString()));
                                    cellNCConsequence.HorizontalAlignment = Element.ALIGN_LEFT;
                                    cellNCConsequence.Colspan = 2;
                                    ListOfNotCompliancestable.AddCell(cellNCConsequence);

                                    PdfPCell cellNCObservation = new PdfPCell(new Phrase("Audit Observation", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellNCObservation);
                                    PdfPCell cellNCRecommendation = new PdfPCell(new Phrase("Audit Recommendation", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    ListOfNotCompliancestable.AddCell(cellNCRecommendation);

                                    ListOfNotCompliancestable.AddCell(item1[8].ToString());
                                    ListOfNotCompliancestable.AddCell(item1[9].ToString());


                                    pdfDoc.Add(ListOfNotCompliancestable);

                                    pdfDoc.Add(Chunk.NEWLINE);
                                    NCID++;
                                }




                                pdfWriter.CloseStream = false;
                                pdfDoc.Close();

                                Response.Buffer = true;
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.Write(pdfDoc);
                                //  Response.End();


                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void lnkBtnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                int branchID = -1;
                long vendorid = -1;
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && ddlFilterCustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlVendorListBase.SelectedValue))
                {
                    vendorid = Convert.ToInt32(ddlVendorListBase.SelectedValue);
                }

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    #region code                                                                               
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Session["TotalRows"] = null;

                        long userID = -1;
                        var role = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
                        {
                            role = Convert.ToString(ViewState["vrole"]);
                        }
                        else
                        {
                            User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                            if (LoggedUser != null)
                            {
                                role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                                ViewState["vrole"] = Convert.ToString(role);
                            }
                        }
                        if (role.Trim().Contains("HVADM"))
                        {
                            userID = -1;
                        }
                        else
                        {
                            userID = AuthenticationHelper.UserID;
                        }

                        DataTable Output = new DataTable();
                        Output.Columns.Add("SNO", typeof(string));
                        Output.Columns.Add("Client Name", typeof(string));
                        Output.Columns.Add("Customer Branch Name", typeof(string));
                        Output.Columns.Add("Responsibility", typeof(string));
                        Output.Columns.Add("Vendor Name", typeof(string));
                        Output.Columns.Add("Auditor Name", typeof(string));
                        Output.Columns.Add("For Month", typeof(string));
                        Output.Columns.Add("Start Date", typeof(string));
                        Output.Columns.Add("End Date", typeof(string));
                        Output.Columns.Add("Status Name", typeof(string));
                        Output.Columns.Add("Nil", typeof(string));
                        Output.Columns.Add("Percentage of Nil", typeof(string));
                        Output.Columns.Add("Complied", typeof(string));
                        Output.Columns.Add("Percentage of Complied", typeof(string));
                        Output.Columns.Add("Not Complied", typeof(string));
                        Output.Columns.Add("Percentage of Not Complied", typeof(string));
                        Output.Columns.Add("Not Applicable", typeof(string));
                        Output.Columns.Add("Percentage of Not Applicable", typeof(string));
                        Output.Columns.Add("total", typeof(string));

                        var table = entities.SP_RLCS_VendorAudit_CheckListReport(vendorid, branchID, customerID, role, userID).ToList();
                        table = table.Distinct().ToList();
                        int cnt = 1;
                        if (true)
                        {
                            string HRDeptId = Convert.ToString(RLCSVendorMastersManagement.GetDeptID("HR"));
                            string AdminDeptId = Convert.ToString(RLCSVendorMastersManagement.GetDeptID("Admin"));

                            foreach (var item in table)
                            {
                                string Responsibility = string.Empty;
                                if (!string.IsNullOrEmpty(item.DepartmentType))
                                {
                                    if (item.DepartmentType.Trim() == AdminDeptId.Trim())
                                    {
                                        Responsibility = "Admin";
                                    }
                                }
                                if (!string.IsNullOrEmpty(item.DepartmentType))
                                {
                                    if (item.DepartmentType.Trim() == HRDeptId.Trim())
                                    {
                                        Responsibility = "HR";
                                    }
                                }
                                int NillCount = 0;
                                int compliedCount = 0;
                                int NotcompliedCount = 0;
                                int AppliANDNotApplicableCount = 0;

                                var ClosedChecklistDetail = entities.SP_RLCS_Vendor_GeClosedChecklistCount(Convert.ToInt32(item.ScheduleonID), item.AuditID).ToList();

                                if (item.StatusName.Equals("In Progress") || item.StatusName.Equals("Open"))
                                {
                                    int checklistCount = (from row in entities.RLCS_VendorAuditStepMapping
                                                          where row.AuditID == item.AuditID && row.VendorAuditScheduleOnID == item.ScheduleonID
                                                          && row.IsActive == true
                                                          select row).ToList().Count;

                                    NillCount = checklistCount - ClosedChecklistDetail.Count;
                                }

                                compliedCount = ClosedChecklistDetail.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                                NotcompliedCount = ClosedChecklistDetail.Where(a => a.ResultStatusID == 2).ToList().Count;
                                AppliANDNotApplicableCount = ClosedChecklistDetail.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;
                                int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount + NillCount;
                                string NillPercentage = "0.00%";
                                if (NillCount > 0)
                                {
                                    NillPercentage = string.Format("{0:0.00}%", (((double)NillCount / (double)totalPoints) * 100));
                                    //NillPercentage = string.Format("{0}%", (int)(((double)NillCount / (double)totalPoints) * 100));                                    
                                }
                                string compliedPercentage = "0.00%";
                                if (compliedCount > 0)
                                {
                                    compliedPercentage = string.Format("{0:0.00}%", (((double)compliedCount / (double)totalPoints) * 100));
                                    //compliedPercentage = string.Format("{0}%", (int)(((double)compliedCount / (double)totalPoints) * 100));
                                }
                                string NotcompliedPercentage = "0.00%";
                                if (NotcompliedCount > 0)
                                {
                                    NotcompliedPercentage = string.Format("{0:0.00}%", (((double)NotcompliedCount / (double)totalPoints) * 100));
                                    //NotcompliedPercentage = string.Format("{0}%", (int)(((double)NotcompliedCount / (double)totalPoints) * 100));
                                }
                                string AppliANDNotApplicablePercentage = "0.00%";
                                if (AppliANDNotApplicableCount > 0)
                                {
                                    AppliANDNotApplicablePercentage = string.Format("{0:0.00}%", (((double)AppliANDNotApplicableCount / (double)totalPoints) * 100));
                                    //AppliANDNotApplicablePercentage = string.Format("{0}%", (int)(((double)AppliANDNotApplicableCount / (double)totalPoints) * 100));
                                }
                                string SD = string.Empty;
                                if (item.StartDate != null)
                                    SD = Convert.ToDateTime(item.StartDate).ToString("dd-MMM-yyyy");

                                string ED = string.Empty;
                                if (item.EndDate != null)
                                {
                                    DateTime DT = Convert.ToDateTime(item.EndDate.ToString());
                                    ED = DT.ToString("dd-MMM-yyyy");
                                }

                                Output.Rows.Add(cnt, item.ClientName, item.CustomerBranchName, Responsibility, item.VendorName, item.AuditorName,
                                        item.ForMonth, SD, ED, item.StatusName, NillCount, NillPercentage,
                                        compliedCount, compliedPercentage, NotcompliedCount, NotcompliedPercentage,
                                        AppliANDNotApplicableCount, AppliANDNotApplicablePercentage, totalPoints
                                        );
                                cnt = cnt + 1;
                            }

                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Summary Report");
                            System.Data.DataTable ExcelData1 = null;
                            DataView view1 = new System.Data.DataView(Output);
                            ExcelData1 = view1.ToTable();
                            exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData1, true);
                            exWorkSheet1.Column(1).Width = 10;
                            exWorkSheet1.Column(2).Width = 50;
                            exWorkSheet1.Column(3).Width = 45;
                            exWorkSheet1.Column(4).Width = 30;
                            exWorkSheet1.Column(5).Width = 30;
                            exWorkSheet1.Column(6).Width = 30;
                            exWorkSheet1.Column(7).Width = 30;
                            exWorkSheet1.Column(8).Width = 30;
                            exWorkSheet1.Column(9).Width = 30;
                            exWorkSheet1.Column(10).Width = 30;
                            exWorkSheet1.Column(11).Width = 30;
                            exWorkSheet1.Column(12).Width = 30;
                            exWorkSheet1.Column(13).Width = 30;
                            exWorkSheet1.Column(14).Width = 30;
                            exWorkSheet1.Column(15).Width = 30;
                            exWorkSheet1.Column(16).Width = 30;
                            exWorkSheet1.Column(17).Width = 30;
                            exWorkSheet1.Column(18).Width = 30;

                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, ExcelData1.Rows.Count + 1, 19])
                            {
                                col.Style.WrapText = true;
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                exWorkSheet1.Cells["A1:S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A1:S1"].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            }


                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=SummaryReport-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }

                }//Using End
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdChecklistDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                        Button btnsave = (Button)e.Row.FindControl("btnChangeStatus");
                        Button btnpdf = (Button)e.Row.FindControl("btnExportToPdf");
                        Button btnDraft = (Button)e.Row.FindControl("btnDraftReport");

                        string status = lblStatus.Text;

                        if (!String.IsNullOrEmpty(status))
                        {
                            btnsave.Visible = false;
                            btnpdf.Visible = false;
                            btnDraft.Visible = false;
                            if (status.Equals("Closed"))
                            {
                                btnsave.Visible = true;
                                btnpdf.Visible = true;
                            }
                            else if (status.Equals("In Progress"))
                            {
                                btnDraft.Visible = true;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkBtnOverAllExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string fileName = string.Empty;
                    fileName = "OverAllReport.xlsx";
                    string Freq = string.Empty;
                    string Period = string.Empty;
                    int customerID = -1;
                    int branchID = -1;
                    long vendorid = -1;
                    if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && ddlFilterCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlVendorListBase.SelectedValue))
                    {
                        vendorid = Convert.ToInt32(ddlVendorListBase.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(txtFrequency.Text))
                    {
                        Freq = txtFrequency.Text.Substring(0, 1);
                    }
                    if (!(string.IsNullOrEmpty(ddlPeriod.SelectedValue)))
                    {
                        Period = ddlPeriod.SelectedValue;

                    }
                    Session["TotalRows"] = null;

                    long userID = -1;
                    var role = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
                    {
                        role = Convert.ToString(ViewState["vrole"]);
                    }
                    else
                    {
                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                        if (LoggedUser != null)
                        {
                            role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                            ViewState["vrole"] = Convert.ToString(role);
                        }
                    }
                    if (role.Trim().Contains("HVADM"))
                    {
                        userID = -1;
                    }
                    else
                    {
                        userID = AuthenticationHelper.UserID;
                    }

                    var lstChecklistDetails = entities.SP_RLCS_VendorAudit_CheckListReport(vendorid, branchID, customerID, role, userID).ToList();
                    lstChecklistDetails = lstChecklistDetails.Distinct().ToList();
                    lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditStatusID == 4).ToList();
                    if (!string.IsNullOrEmpty(Freq))
                    {
                        lstChecklistDetails = lstChecklistDetails.Where(entry => entry.Frequency.Equals(Freq)).ToList();
                    }
                    if (!string.IsNullOrEmpty(Period))
                    {
                        lstChecklistDetails = lstChecklistDetails.Where(x => x.ForMonth.Trim().ToUpper() == ddlPeriod.SelectedValue.Trim().ToUpper()).ToList();
                    }
                    if (!string.IsNullOrEmpty(ddlFinYear.SelectedValue) && ddlFinYear.SelectedValue != "-1")
                    {
                        DateTime startDate = new DateTime(Convert.ToInt32(ddlFinYear.SelectedValue), 4, 1); // 1st Feb this year
                        DateTime endDate = new DateTime(Convert.ToInt32(ddlFinYear.SelectedValue) + 1, 3, 31); // Last day in January next year

                        lstChecklistDetails = lstChecklistDetails.Where(x => x.StartDate >= startDate && x.StartDate <= endDate).ToList();
                    }
                    if (lstChecklistDetails.Count > 0)
                    {
                        #region Sheet    
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {

                            #region Summary Sheet
                            //Add Summary Sheet
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("ContractorSummary");


                            DataTable ExcelDataSummary = new DataTable("ExcelDataSummary");
                            DataColumn dtColumn;
                            DataRow myDataRow;
                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(Int32);
                            dtColumn.ColumnName = "SRNO";
                            dtColumn.Caption = "Sr No";
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = true;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Month";
                            dtColumn.Caption = "Month";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Entity";
                            dtColumn.Caption = "Entity";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Responsibility";
                            dtColumn.Caption = "Responsibility";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Link";
                            dtColumn.Caption = "Link";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Location";
                            dtColumn.Caption = "Location";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "VendorsName";
                            dtColumn.Caption = "Vendors Name";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Complied";
                            dtColumn.Caption = "Complied";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "PrtgofComplied";
                            dtColumn.Caption = "Percentage of complied ";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "NotComplied";
                            dtColumn.Caption = "Not Complied ";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Prctgofnotcomplied";
                            dtColumn.Caption = "Percentage of not complied";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "NotApplicable";
                            dtColumn.Caption = "Not Applicable";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);

                            dtColumn = new DataColumn();
                            dtColumn.DataType = typeof(String);
                            dtColumn.ColumnName = "Total";
                            dtColumn.Caption = "Total";
                            dtColumn.AutoIncrement = false;
                            dtColumn.ReadOnly = false;
                            dtColumn.Unique = false;
                            ExcelDataSummary.Columns.Add(dtColumn);
                            int SrNo = 1;
                            foreach (var details in lstChecklistDetails)
                            {
                                long AuditID = Convert.ToInt64(details.AuditID);
                                long ScheduleOnID = Convert.ToInt64(details.ScheduleonID);
                                var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(ScheduleOnID, AuditID)
                                             select row).ToList();

                                int compliedCount = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                                int NotcompliedCount = table.Where(a => a.ResultStatusID == 2).ToList().Count;
                                int AppliANDNotApplicableCount = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;
                                int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount;

                                string compliedPercentage = string.Format("{0:0.00}%", ((double)compliedCount / (double)totalPoints) * 100);
                                string NotcompliedPercentage = string.Format("{0:0.00}%", ((double)NotcompliedCount / (double)totalPoints) * 100);


                                myDataRow = ExcelDataSummary.NewRow();
                                myDataRow["SRNO"] = SrNo;
                                myDataRow["Month"] = details.ForMonth.Trim();
                                myDataRow["Entity"] = details.ClientName;

                                myDataRow["Responsibility"] = (details.DeparmentName == null ? "" : details.DeparmentName.Trim());

                                myDataRow["Link"] = details.CustomerBranchName + "_" + details.VendorName;
                                myDataRow["Location"] = details.CustomerBranchName;
                                myDataRow["VendorsName"] = details.VendorName;
                                myDataRow["Complied"] = compliedCount;
                                myDataRow["PrtgofComplied"] = compliedPercentage;
                                myDataRow["NotComplied"] = NotcompliedCount;
                                myDataRow["Prctgofnotcomplied"] = NotcompliedPercentage;
                                myDataRow["NotApplicable"] = AppliANDNotApplicableCount;
                                myDataRow["Total"] = totalPoints;
                                ExcelDataSummary.Rows.Add(myDataRow);
                                SrNo++;
                            }

                            exWorkSheet1.Cells["A1"].Value = "Contractor's Statutory Compliance Audit Report – Summary";
                            exWorkSheet1.Cells["A1:M1"].Merge = true;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Font.Size = 14;
                            exWorkSheet1.Cells["A1:M1"].Style.Font.Italic = true;
                            exWorkSheet1.Cells["A1:M1"].Style.Font.UnderLine = true;
                            exWorkSheet1.Cells["A1:M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["A1:M1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:M1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:O1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            exWorkSheet1.Cells["A1:O1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelDataSummary, true);
                            exWorkSheet1.Cells["A3"].Value = "S.NO";
                            exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["A3"].AutoFitColumns(5);

                            exWorkSheet1.Cells["B3"].Value = "Month";
                            exWorkSheet1.Cells["B3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["B3"].AutoFitColumns(10);

                            exWorkSheet1.Cells["C3"].Value = "Entity";
                            exWorkSheet1.Cells["C3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["C3"].AutoFitColumns(40);

                            exWorkSheet1.Cells["D3"].Value = "Responsibility";
                            exWorkSheet1.Cells["D3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["D3"].AutoFitColumns(35);

                            exWorkSheet1.Cells["E3"].Value = "Link";
                            exWorkSheet1.Cells["E3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["E3"].AutoFitColumns(35);


                            exWorkSheet1.Cells["F3"].Value = "Location";
                            exWorkSheet1.Cells["F3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["F3"].AutoFitColumns(21);

                            exWorkSheet1.Cells["G3"].Value = "Vendors Name";
                            exWorkSheet1.Cells["G3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["G3"].AutoFitColumns(35);

                            exWorkSheet1.Cells["H3"].Value = "Complied";
                            exWorkSheet1.Cells["H3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["H3"].AutoFitColumns(35);




                            exWorkSheet1.Cells["I3"].Value = "Percentage of Complied";
                            exWorkSheet1.Cells["I3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["I3"].AutoFitColumns(20);


                            exWorkSheet1.Cells["J3"].Value = "Not Complied";
                            exWorkSheet1.Cells["J3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["J3"].AutoFitColumns(35);


                            exWorkSheet1.Cells["K3"].Value = "Percentage of not complied";
                            exWorkSheet1.Cells["K3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["K3"].AutoFitColumns(35);


                            exWorkSheet1.Cells["L3"].Value = "Not Applicable";
                            exWorkSheet1.Cells["L3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["L3"].AutoFitColumns(35);

                            exWorkSheet1.Cells["M3"].Value = "Total";
                            exWorkSheet1.Cells["M3"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M3"].Style.Font.Size = 12;
                            exWorkSheet1.Cells["M3"].AutoFitColumns(35);

                            int row1 = 4;
                            int cont = 0;
                            foreach (DataRow itemdata in ExcelDataSummary.Rows)
                            {
                                cont++;
                                string SheetName = "";
                                if (!string.IsNullOrEmpty(Period))
                                {
                                    SheetName = Convert.ToString(itemdata[5]) + "_" + Convert.ToString(itemdata[6]);
                                }
                                else
                                {
                                    SheetName = Convert.ToString(itemdata[5]) + "_" + Convert.ToString(itemdata[6]) + cont;
                                }

                                ExcelRange Rng = exWorkSheet1.Cells["E" + row1];
                                Rng.Hyperlink = new Uri("#'" + SheetName + "'!B2", UriKind.Relative);
                                Rng.Style.Font.Color.SetColor(Color.Blue);
                                exWorkSheet1.Cells["A3:M3"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                exWorkSheet1.Cells["A3:M3"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                exWorkSheet1.Cells["A3:M3"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                exWorkSheet1.Cells["A3:M3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                                exWorkSheet1.Cells["A" + row1 + ":M" + row1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A" + row1 + ":M" + row1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A" + row1 + ":M" + row1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                exWorkSheet1.Cells["A" + row1 + ":M" + row1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                row1++;
                            }


                            #endregion
                            //End Summary Sheet

                            int count = 0;
                            foreach (var item1 in lstChecklistDetails)
                            {
                                count++;
                                string SheetName = "";
                                if (!string.IsNullOrEmpty(Period))
                                {
                                    SheetName = Convert.ToString(item1.CustomerBranchName) + "_" + Convert.ToString(item1.VendorName) + count;
                                }
                                else
                                {
                                    SheetName = Convert.ToString(item1.CustomerBranchName) + "_" + Convert.ToString(item1.VendorName) + count;
                                }
                                //ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(item1.ClientName.Substring(18) + count);
                                exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(SheetName);
                                //ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Audit Checklist Report" + count);

                                long AuditID = Convert.ToInt64(item1.AuditID);
                                long CustomerBranchID = Convert.ToInt64(item1.AVACOM_BranchID);
                                long VendorID = Convert.ToInt64(item1.Avacom_VendorID);
                                long ScheduleOnID = Convert.ToInt64(item1.ScheduleonID);
                                string AuditorName = Convert.ToString(item1.AuditorName);
                                string ClientName = Convert.ToString(item1.ClientName);
                                string ForMonth = Convert.ToString(item1.ForMonth.Trim());




                                string AuditedDate = string.Empty;
                                if (item1.AuditedDate != null)
                                    AuditedDate = item1.AuditedDate.ToString("dd-MMM-yyyy");

                                string StartDate = string.Empty;
                                if (item1.StartDate != null)
                                    StartDate = Convert.ToDateTime(item1.StartDate).ToString("dd-MMM-yyyy");

                                string EndDate = string.Empty;
                                if (item1.EndDate != null)
                                    EndDate = Convert.ToDateTime(item1.StartDate).ToString("dd-MMM-yyyy");

                                #region code                                                                                                      
                                if (AuditID != -1 && CustomerBranchID != -1 && VendorID != -1 && ScheduleOnID != -1)
                                {
                                    var instanceDetails = (from row in entities.RLCS_VendorAuditInstance
                                                           where row.ID == AuditID && row.CC_Status == "A"
                                                           && row.IsActive == false
                                                           select row).FirstOrDefault();
                                    if (instanceDetails != null)
                                    {
                                        string clientSPOCName = RLCS_ClientsManagement.GetClientSPOC(instanceDetails.CC_ClientID, ConfigurationManager.AppSettings["RLCSAPIURL"] + "Masters/");

                                        var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(ScheduleOnID, AuditID)
                                                     select row).ToList();

                                        int compliedCount = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                                        int NotcompliedCount = table.Where(a => a.ResultStatusID == 2).ToList().Count;
                                        int AppliANDNotApplicableCount = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;
                                        int totalPoints = compliedCount + NotcompliedCount + AppliANDNotApplicableCount;


                                        int HighCount = table.Where(entry => entry.Risk.Equals("High")).ToList().Count;
                                        int LowCount = table.Where(entry => entry.Risk.Equals("Low")).ToList().Count;
                                        int MediumCount = table.Where(entry => entry.Risk.Equals("Medium")).ToList().Count;
                                        int RisktotalPoints = HighCount + LowCount + MediumCount;

                                        int CompliedHighCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("High")).ToList().Count;
                                        int CompliedMedCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Medium")).ToList().Count;
                                        int CompliedLowCount = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Low")).ToList().Count;


                                        int NotcompliedHighCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("High")).ToList().Count;
                                        int NotcompliedMedCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Medium")).ToList().Count;
                                        int NotcompliedLowCount = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Low")).ToList().Count;

                                        int NotApplicableHighCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("High")).ToList().Count;
                                        int NotApplicableMedCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Medium")).ToList().Count;
                                        int NotApplicableLowCount = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Low")).ToList().Count;

                                        int totalHigh = CompliedHighCount + NotcompliedHighCount + NotApplicableHighCount;
                                        int totalMedium = CompliedMedCount + NotcompliedMedCount + NotApplicableMedCount;
                                        int totalLow = CompliedLowCount + NotcompliedLowCount + NotApplicableLowCount;


                                        DataTable ExcelData1 = null;
                                        DataView view1 = new System.Data.DataView((table as List<SP_RLCS_VendorAudit_DetailsCheckListReport_Result>).ToDataTable());
                                        ExcelData1 = view1.ToTable("Selected", false, "SNO", "StateID", "ActName", "NatureOFCompliance", "Description", "Risk", "Form", "Section", "ResultStatus", "Audit_Observation", "Recommendation", "TypeOfCompliance", "Consequences");

                                        int cnt = 1;
                                        #region
                                        foreach (DataRow item in ExcelData1.Rows)
                                        {
                                            item["SNO"] = cnt;
                                            cnt++;
                                        }//foreach end
                                        #endregion

                                        #region

                                        exWorkSheet1.Cells["A1"].Value = "Vendor Audit Checklist";
                                        exWorkSheet1.Cells["A1:M1"].Merge = true;
                                        exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.Gray);
                                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A1"].Style.Font.Size = 14;
                                        exWorkSheet1.Cells["A1:M1"].Style.Font.Italic = true;
                                        exWorkSheet1.Cells["A1:M1"].Style.Font.UnderLine = true;
                                        exWorkSheet1.Cells["A1:M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["A1:M1"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["A1:M1"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["A1:M1"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["A1:M1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                        //row 2
                                        exWorkSheet1.Cells["A2"].Value = "1";
                                        exWorkSheet1.Cells["A2:B2"].Merge = true;
                                        exWorkSheet1.Cells["A2"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["A2:B16"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A2:B16"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A2:B16"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["A2:B16"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["A2:B16"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["A2:B16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                                        exWorkSheet1.Cells["C2"].Value = "Client Name";
                                        exWorkSheet1.Cells["C2"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C2"].Style.WrapText = true;
                                        exWorkSheet1.Cells["C2:C16"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["C2:C16"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["C2:C16"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["C2:C16"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["C2:C16"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["C2:C16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                        exWorkSheet1.Cells["D2"].Value = ClientName;
                                        exWorkSheet1.Cells["D2:J2"].Merge = true;
                                        exWorkSheet1.Cells["D2"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D2:M16"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["D2:M16"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["D2:M16"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                        exWorkSheet1.Cells["D2:M16"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                        //row 3
                                        exWorkSheet1.Cells["A3"].Value = "2";
                                        exWorkSheet1.Cells["A3:B3"].Merge = true;
                                        exWorkSheet1.Cells["A3"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C3"].Value = "Name of the Client SPOC";
                                        exWorkSheet1.Cells["C3"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C3"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D3"].Value = clientSPOCName;//instanceDetails.CC_SPOC_Name;
                                        exWorkSheet1.Cells["D3:J3"].Merge = true;
                                        exWorkSheet1.Cells["D3"].Style.WrapText = true;

                                        //row 4
                                        exWorkSheet1.Cells["A4"].Value = "3";
                                        exWorkSheet1.Cells["A4:B4"].Merge = true;
                                        exWorkSheet1.Cells["A4"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C4"].Value = "Name of Vendor & Address";
                                        exWorkSheet1.Cells["C4"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C4"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D4"].Value = instanceDetails.CC_ContractorName + ',' + instanceDetails.CC_ContractorAddress;
                                        exWorkSheet1.Cells["D4:J4"].Merge = true;
                                        exWorkSheet1.Cells["D4"].Style.WrapText = true;

                                        //row 5
                                        exWorkSheet1.Cells["A5"].Value = "4";
                                        exWorkSheet1.Cells["A5:B5"].Merge = true;
                                        exWorkSheet1.Cells["A5"].AutoFitColumns(5);


                                        exWorkSheet1.Cells["C5"].Value = "Name of Vendor 's Representative and Mobile No.";
                                        exWorkSheet1.Cells["C5"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C5"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D5"].Value = instanceDetails.CC_SPOC_Name + "," + "Mob-" + instanceDetails.CC_SPOC_Mobile;
                                        exWorkSheet1.Cells["D5:J5"].Merge = true;
                                        exWorkSheet1.Cells["D5"].Style.WrapText = true;

                                        //row 6
                                        exWorkSheet1.Cells["A6"].Value = "5";
                                        exWorkSheet1.Cells["A6:B6"].Merge = true;
                                        exWorkSheet1.Cells["A6"].AutoFitColumns(5);



                                        exWorkSheet1.Cells["C6"].Value = "Nature of Work";
                                        exWorkSheet1.Cells["C6"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C6"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D6"].Value = instanceDetails.CC_NatureOfBusiness;
                                        exWorkSheet1.Cells["D6:J6"].Merge = true;
                                        exWorkSheet1.Cells["D6"].Style.WrapText = true;


                                        //row 7
                                        exWorkSheet1.Cells["A7"].Value = "6";
                                        exWorkSheet1.Cells["A7:B7"].Merge = true;
                                        exWorkSheet1.Cells["A7"].AutoFitColumns(5);


                                        exWorkSheet1.Cells["C7"].Value = "ESIC Number";
                                        exWorkSheet1.Cells["C7"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C7"].Style.WrapText = true;


                                        exWorkSheet1.Cells["D7"].Value = instanceDetails.CC_ESIC_Number;
                                        exWorkSheet1.Cells["D7:J7"].Merge = true;
                                        exWorkSheet1.Cells["D7"].Style.WrapText = true;

                                        //row 8
                                        exWorkSheet1.Cells["A8"].Value = "7";
                                        exWorkSheet1.Cells["A8:B8"].Merge = true;
                                        exWorkSheet1.Cells["A8"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C8"].Value = "PF Number";
                                        exWorkSheet1.Cells["C8"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C8"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D8"].Value = instanceDetails.CC_PF_Code;
                                        exWorkSheet1.Cells["D8:J8"].Merge = true;
                                        exWorkSheet1.Cells["D8"].Style.WrapText = true;

                                        //row 9
                                        exWorkSheet1.Cells["A9"].Value = "8";
                                        exWorkSheet1.Cells["A9:B9"].Merge = true;
                                        exWorkSheet1.Cells["A9"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C9"].Value = "Audit Date";
                                        exWorkSheet1.Cells["C9"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C9"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D9"].Value = instanceDetails.CC_ContractFrom;
                                        exWorkSheet1.Cells["D9:J9"].Merge = true;
                                        exWorkSheet1.Cells["D9"].Style.WrapText = true;
                                        exWorkSheet1.Cells["D9"].Style.Numberformat.Format = "dd-MMM-yyyy";
                                        exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                                        //row 10
                                        exWorkSheet1.Cells["A10"].Value = "9";
                                        exWorkSheet1.Cells["A10:B10"].Merge = true;
                                        exWorkSheet1.Cells["A10"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C10"].Value = "Auditor Name";
                                        exWorkSheet1.Cells["C10"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C10"].Style.WrapText = true;


                                        exWorkSheet1.Cells["D10"].Value = AuditorName; // + "(" + instanceDetails.CC_Auditor + ")"; 
                                        //exWorkSheet1.Cells["D10"].Value = AuditorName;
                                        //exWorkSheet1.Cells["D10"].Value = instanceDetails.CC_Auditor;
                                        exWorkSheet1.Cells["D10:J10"].Merge = true;
                                        exWorkSheet1.Cells["D10"].Style.WrapText = true;


                                        //row 11
                                        exWorkSheet1.Cells["A11"].Value = "10";
                                        exWorkSheet1.Cells["A11:B11"].Merge = true;
                                        exWorkSheet1.Cells["A11"].AutoFitColumns(5);


                                        exWorkSheet1.Cells["C11"].Value = "No.of Employees";
                                        exWorkSheet1.Cells["C11"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C11"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D11"].Value = instanceDetails.CC_NoOfJoinees + "( Male -" + instanceDetails.CC_MaleHeadCount + ", Female-" + instanceDetails.CC_FeMaleHeadCount + ")";
                                        //exWorkSheet1.Cells["D11"].Value = instanceDetails.CC_NoOfJoinees;
                                        exWorkSheet1.Cells["D11:J11"].Merge = true;
                                        exWorkSheet1.Cells["D11"].Style.WrapText = true;
                                        exWorkSheet1.Cells["D11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                                        //row 12
                                        exWorkSheet1.Cells["A12"].Value = "11";
                                        exWorkSheet1.Cells["A12:B12"].Merge = true;
                                        exWorkSheet1.Cells["A12"].AutoFitColumns(5);


                                        exWorkSheet1.Cells["C12"].Value = "Audit for the month";
                                        exWorkSheet1.Cells["C12"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C12"].Style.WrapText = true;


                                        exWorkSheet1.Cells["D12"].Value = ForMonth;
                                        exWorkSheet1.Cells["D12:J12"].Merge = true;
                                        exWorkSheet1.Cells["D12"].Style.WrapText = true;

                                        //row 13
                                        exWorkSheet1.Cells["A13"].Value = "12";
                                        exWorkSheet1.Cells["A13:B13"].Merge = true;
                                        exWorkSheet1.Cells["A13"].AutoFitColumns(5);


                                        exWorkSheet1.Cells["C13"].Value = "Branch";
                                        exWorkSheet1.Cells["C13"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C13"].Style.WrapText = true;


                                        exWorkSheet1.Cells["D13"].Value = instanceDetails.CC_BranchID;
                                        exWorkSheet1.Cells["D13:J13"].Merge = true;
                                        exWorkSheet1.Cells["D13"].Style.WrapText = true;

                                        //row 14
                                        exWorkSheet1.Cells["A14"].Value = "13";
                                        exWorkSheet1.Cells["A14:B14"].Merge = true;
                                        exWorkSheet1.Cells["A14"].AutoFitColumns(5);


                                        exWorkSheet1.Cells["C14"].Value = "Audited Date";
                                        exWorkSheet1.Cells["C14"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C14"].Style.WrapText = true;


                                        exWorkSheet1.Cells["D14"].Value = AuditedDate;
                                        exWorkSheet1.Cells["D14:J14"].Merge = true;
                                        exWorkSheet1.Cells["D14"].Style.WrapText = true;

                                        //row 15
                                        exWorkSheet1.Cells["A15"].Value = "14";
                                        exWorkSheet1.Cells["A15:B15"].Merge = true;
                                        exWorkSheet1.Cells["A15"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C15"].Value = "Audit Start Date";
                                        exWorkSheet1.Cells["C15"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C15"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D15"].Value = StartDate;
                                        exWorkSheet1.Cells["D15:J15"].Merge = true;
                                        exWorkSheet1.Cells["D15"].Style.WrapText = true;

                                        //row 16
                                        exWorkSheet1.Cells["A16"].Value = "15";
                                        exWorkSheet1.Cells["A16:B16"].Merge = true;
                                        exWorkSheet1.Cells["A16"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["C16"].Value = "Audit End Date";
                                        exWorkSheet1.Cells["C16"].AutoFitColumns(40);
                                        exWorkSheet1.Cells["C16"].Style.WrapText = true;

                                        exWorkSheet1.Cells["D16"].Value = EndDate;
                                        exWorkSheet1.Cells["D16:J16"].Merge = true;
                                        exWorkSheet1.Cells["D16"].Style.WrapText = true;


                                        //start
                                        exWorkSheet1.Cells["A17"].LoadFromDataTable(ExcelData1, true);

                                        exWorkSheet1.Cells["A17"].Value = "S NO";
                                        exWorkSheet1.Cells["A17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A17"].AutoFitColumns(5);

                                        exWorkSheet1.Cells["B17"].Value = "State";
                                        exWorkSheet1.Cells["B17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["B17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B17"].AutoFitColumns(10);

                                        exWorkSheet1.Cells["C17"].Value = "Act Name";
                                        exWorkSheet1.Cells["C17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["C17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["C17"].AutoFitColumns(40);

                                        exWorkSheet1.Cells["D17"].Value = "Nature Of Compliance";
                                        exWorkSheet1.Cells["D17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["D17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["D17"].AutoFitColumns(35);

                                        exWorkSheet1.Cells["E17"].Value = "Description";
                                        exWorkSheet1.Cells["E17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["E17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["E17"].AutoFitColumns(35);

                                        exWorkSheet1.Cells["F17"].Value = "Risk";
                                        exWorkSheet1.Cells["F17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["F17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["F17"].AutoFitColumns(21);

                                        exWorkSheet1.Cells["G17"].Value = "Form";
                                        exWorkSheet1.Cells["G17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["G17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["G17"].AutoFitColumns(35);

                                        exWorkSheet1.Cells["H17"].Value = "Section & Rule";
                                        exWorkSheet1.Cells["H17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["H17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["H17"].AutoFitColumns(35);


                                        exWorkSheet1.Cells["I17"].Value = "Compliance Status";
                                        exWorkSheet1.Cells["I17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["I17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["I17"].AutoFitColumns(20);

                                        exWorkSheet1.Cells["J17"].Value = "Audit Observations";
                                        exWorkSheet1.Cells["J17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["J17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["J17"].AutoFitColumns(35);

                                        exWorkSheet1.Cells["K17"].Value = "Recommendations";
                                        exWorkSheet1.Cells["K17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["K17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["K17"].AutoFitColumns(35);

                                        exWorkSheet1.Cells["L17"].Value = "Compliance Type";
                                        exWorkSheet1.Cells["L17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["L17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["L17"].AutoFitColumns(35);

                                        exWorkSheet1.Cells["M17"].Value = "Consequences";
                                        exWorkSheet1.Cells["M17"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["M17"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["M17"].AutoFitColumns(35);

                                        #endregion
                                        int rowval = 18;

                                        foreach (DataRow itemdata in ExcelData1.Rows)
                                        {

                                            string result = Convert.ToString(itemdata["ResultStatus"]);
                                            if (result == "Not Complied")
                                            {
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                            }
                                            else if (result == "Not Applicable")
                                            {
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                            }
                                            else if (result == "Complied")
                                            {
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                            }
                                            else if (result == "Applicable")
                                            {
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells["A" + rowval + ":M" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                            }
                                            rowval++;
                                        }

                                        #region Complied chart
                                        int chartstart = ExcelData1.Rows.Count + 17;

                                        int chartHeader = chartstart + 2;
                                        string header = "C" + chartHeader;
                                        string header1 = "D" + chartHeader;
                                        string header2 = "E" + chartHeader;

                                        string headermerge = header + ":" + header2;

                                        exWorkSheet1.Cells[header].Value = "Compliance Summary";
                                        exWorkSheet1.Cells[headermerge].Merge = true;
                                        exWorkSheet1.Cells[headermerge].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells[header].Style.WrapText = true;


                                        chartstart = chartstart + 3;
                                        int range = (chartstart + 1);
                                        int rangenext = (range + 1);
                                        int rangenext1 = (rangenext + 1);

                                        string x = "C" + chartstart;
                                        string y = "C" + range;
                                        string z = "C" + rangenext;
                                        string t = "C" + rangenext1;

                                        string percenatge = "E" + chartstart;
                                        string percenatge1 = "E" + range;
                                        string percenatge2 = "E" + rangenext;

                                        string x1 = "D" + chartstart;
                                        string y1 = "D" + range;
                                        string z1 = "D" + rangenext;
                                        string t1 = "D" + rangenext1;

                                        //exWorkSheet1.Cells[percenatge].Value = string.Format("{0}%", (int)(((double)compliedCount / (double)totalPoints) * 100));
                                        //exWorkSheet1.Cells[percenatge1].Value = string.Format("{0}%", (int)(((double)NotcompliedCount / (double)totalPoints) * 100));
                                        //exWorkSheet1.Cells[percenatge2].Value = string.Format("{0}%", (int)(((double)AppliANDNotApplicableCount / (double)totalPoints) * 100));

                                        exWorkSheet1.Cells[percenatge].Value = string.Format("{0:0.00}%", ((double)compliedCount / (double)totalPoints) * 100);
                                        exWorkSheet1.Cells[percenatge1].Value = string.Format("{0:0.00}%", ((double)NotcompliedCount / (double)totalPoints) * 100);
                                        exWorkSheet1.Cells[percenatge2].Value = string.Format("{0:0.00}%", ((double)AppliANDNotApplicableCount / (double)totalPoints) * 100);


                                        exWorkSheet1.Cells[x1].Value = compliedCount;
                                        exWorkSheet1.Cells[y1].Value = NotcompliedCount;
                                        exWorkSheet1.Cells[z1].Value = AppliANDNotApplicableCount;
                                        exWorkSheet1.Cells[t1].Value = totalPoints;

                                        exWorkSheet1.Cells[x].Value = "Complied";
                                        exWorkSheet1.Cells[y].Value = "Not Complied";
                                        exWorkSheet1.Cells[z].Value = "Not Applicable";
                                        exWorkSheet1.Cells[t].Value = "Total";

                                        var myChart = exWorkSheet1.Drawings.AddChart("chart", eChartType.Pie3D);

                                        string s = x1 + ":" + z1;
                                        string s1 = x + ":" + z;

                                        // Define series for the chart
                                        var series = myChart.Series.Add(s, s1);

                                        var pieSeries = (ExcelPieChartSerie)series;
                                        pieSeries.Explosion = 5;
                                        pieSeries.DataLabel.ShowPercent = true;
                                        pieSeries.DataLabel.ShowLeaderLines = true;
                                        pieSeries.DataLabel.Position = eLabelPosition.BestFit;


                                        myChart.Border.Fill.Color = System.Drawing.Color.Green;
                                        myChart.Title.Text = "Compliance Summary";
                                        //myChart.Title.Text = "My Chart";
                                        myChart.SetSize(400, 400);
                                        // Add to 6th row and to the 6th column
                                        myChart.SetPosition(chartstart + 13, 0, 2, 0);

                                        #endregion

                                        #region New Table 
                                        int chartstart1 = ExcelData1.Rows.Count + 25;
                                        int chartHeader1 = chartstart1 + 2;

                                        string header01 = "C" + chartHeader1;
                                        string header11 = "D" + chartHeader1;
                                        string header12 = "E" + chartHeader1;
                                        string header13 = "F" + chartHeader1;
                                        string header14 = "G" + chartHeader1;

                                        exWorkSheet1.Cells[header01].Value = "Category";
                                        exWorkSheet1.Cells[header01].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[header01].Style.WrapText = true;
                                        exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                        exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells["C" + chartHeader1 + ":G" + chartHeader1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                        exWorkSheet1.Cells[header11].Value = "High Risk";
                                        exWorkSheet1.Cells[header11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[header11].Style.WrapText = true;


                                        exWorkSheet1.Cells[header12].Value = "Medium Risk";
                                        exWorkSheet1.Cells[header12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[header12].Style.WrapText = true;

                                        exWorkSheet1.Cells[header13].Value = "Low Risk";
                                        exWorkSheet1.Cells[header13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[header13].Style.WrapText = true;

                                        exWorkSheet1.Cells[header14].Value = "Total";
                                        exWorkSheet1.Cells[header14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[header14].Style.WrapText = true;

                                        chartstart1 = chartstart1 + 3;
                                        int range1 = (chartstart1 + 1);
                                        int rangenext01 = (range1 + 1);
                                        int rangenext11 = (rangenext01 + 1);

                                        string x11 = "C" + chartstart1;
                                        string y11 = "C" + range1;
                                        string z11 = "C" + rangenext01;
                                        string t11 = "C" + rangenext11;

                                        string H1 = "D" + chartstart1;
                                        string H2 = "D" + range1;
                                        string H3 = "D" + rangenext01;
                                        string H4 = "D" + rangenext11;

                                        string Med1 = "E" + chartstart1;
                                        string Med2 = "E" + range1;
                                        string Med3 = "E" + rangenext01;
                                        string Med4 = "E" + rangenext11;

                                        string L1 = "F" + chartstart1;
                                        string L2 = "F" + range1;
                                        string L3 = "F" + rangenext01;
                                        string L4 = "F" + rangenext11;

                                        string T1 = "G" + chartstart1;
                                        string T2 = "G" + range1;
                                        string T3 = "G" + rangenext01;
                                        string T4 = "G" + rangenext11;

                                        exWorkSheet1.Cells[H1].Value = CompliedHighCount;
                                        exWorkSheet1.Cells[H2].Value = NotcompliedHighCount;
                                        exWorkSheet1.Cells[H3].Value = NotApplicableHighCount;
                                        exWorkSheet1.Cells[H4].Value = totalHigh;

                                        exWorkSheet1.Cells[Med1].Value = CompliedMedCount;
                                        exWorkSheet1.Cells[Med2].Value = NotcompliedMedCount;
                                        exWorkSheet1.Cells[Med3].Value = NotApplicableMedCount;
                                        exWorkSheet1.Cells[Med4].Value = totalMedium;


                                        exWorkSheet1.Cells[L1].Value = CompliedLowCount;
                                        exWorkSheet1.Cells[L2].Value = NotcompliedLowCount;
                                        exWorkSheet1.Cells[L3].Value = NotApplicableLowCount;
                                        exWorkSheet1.Cells[L4].Value = totalLow;

                                        exWorkSheet1.Cells[T1].Value = compliedCount;
                                        exWorkSheet1.Cells[T2].Value = NotcompliedCount;
                                        exWorkSheet1.Cells[T3].Value = AppliANDNotApplicableCount;
                                        exWorkSheet1.Cells[T4].Value = "";

                                        exWorkSheet1.Cells[x11].Value = "Complied";
                                        exWorkSheet1.Cells[x11 + ":" + x11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells[x11 + ":" + x11].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                        exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[x11 + ":G" + chartstart1 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[y11].Value = "Not Complied";
                                        exWorkSheet1.Cells[y11 + ":" + y11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells[y11 + ":" + y11].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                        exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[y11 + ":G" + range1 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[z11].Value = "Not Applicable";
                                        exWorkSheet1.Cells[z11 + ":" + z11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells[z11 + ":" + z11].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                        exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[z11 + ":G" + rangenext01 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells[t11].Value = "Total";
                                        exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        exWorkSheet1.Cells[t11 + ":G" + rangenext11 + ""].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                                        #endregion

                                        #region Risk chart
                                        int Rchartstart = ExcelData1.Rows.Count + 17;

                                        int RchartHeader = Rchartstart + 2;
                                        string Rheader = "G" + RchartHeader;
                                        string Rheader1 = "H" + RchartHeader;
                                        string Rheader2 = "I" + RchartHeader;

                                        string Rheadermerge = Rheader + ":" + Rheader2;

                                        exWorkSheet1.Cells[Rheader].Value = "Risk Summary";
                                        exWorkSheet1.Cells[Rheadermerge].Merge = true;
                                        exWorkSheet1.Cells[Rheadermerge].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells[Rheader].Style.WrapText = true;


                                        Rchartstart = Rchartstart + 3;
                                        int Rrange = (Rchartstart + 1);
                                        int Rrangenext = (Rrange + 1);
                                        int Rrangenext1 = (Rrangenext + 1);

                                        string Rx = "G" + Rchartstart;
                                        string Ry = "G" + Rrange;
                                        string Rz = "G" + Rrangenext;
                                        string Rt = "G" + Rrangenext1;

                                        string Rpercenatge = "I" + Rchartstart;
                                        string Rpercenatge1 = "I" + Rrange;
                                        string Rpercenatge2 = "I" + Rrangenext;

                                        string Rx1 = "H" + Rchartstart;
                                        string Ry1 = "H" + Rrange;
                                        string Rz1 = "H" + Rrangenext;
                                        string Rt1 = "H" + Rrangenext1;

                                        //exWorkSheet1.Cells[Rpercenatge].Value = string.Format("{0}%", (int)(((double)HighCount / (double)RisktotalPoints) * 100));
                                        //exWorkSheet1.Cells[Rpercenatge1].Value = string.Format("{0}%", (int)(((double)MediumCount / (double)RisktotalPoints) * 100));
                                        //exWorkSheet1.Cells[Rpercenatge2].Value = string.Format("{0}%", (int)(((double)LowCount / (double)RisktotalPoints) * 100));

                                        exWorkSheet1.Cells[Rpercenatge].Value = string.Format("{0:0.00}%", ((double)HighCount / (double)RisktotalPoints) * 100);
                                        exWorkSheet1.Cells[Rpercenatge1].Value = string.Format("{0:0.00}%", ((double)MediumCount / (double)RisktotalPoints) * 100);
                                        exWorkSheet1.Cells[Rpercenatge2].Value = string.Format("{0:0.00}%", ((double)LowCount / (double)RisktotalPoints) * 100);

                                        exWorkSheet1.Cells[Rx1].Value = HighCount;
                                        exWorkSheet1.Cells[Ry1].Value = MediumCount;
                                        exWorkSheet1.Cells[Rz1].Value = LowCount;
                                        exWorkSheet1.Cells[Rt1].Value = RisktotalPoints;

                                        exWorkSheet1.Cells[Rx].Value = "High";
                                        exWorkSheet1.Cells[Ry].Value = "Medium";
                                        exWorkSheet1.Cells[Rz].Value = "Low";
                                        exWorkSheet1.Cells[Rt].Value = "Total";

                                        var RmyChart = exWorkSheet1.Drawings.AddChart("Rchart", eChartType.Pie3D);

                                        string Rs = Rx1 + ":" + Rz1;
                                        string Rs1 = Rx + ":" + Rz;

                                        // Define series for the chart
                                        var Rseries = RmyChart.Series.Add(Rs, Rs1);

                                        var RpieSeries = (ExcelPieChartSerie)Rseries;
                                        RpieSeries.Explosion = 5;
                                        RpieSeries.DataLabel.ShowPercent = true;
                                        RpieSeries.DataLabel.ShowLeaderLines = true;
                                        RpieSeries.DataLabel.Position = eLabelPosition.BestFit;

                                        RmyChart.Border.Fill.Color = System.Drawing.Color.Green;
                                        RmyChart.Title.Text = "Risk Summary";
                                        //myChart.Title.Text = "My Chart";
                                        RmyChart.SetSize(400, 400);
                                        // Add to 6th row and to the 6th column
                                        RmyChart.SetPosition(Rchartstart + 13, 0, 6, 0);

                                        #endregion

                                        using (ExcelRange col = exWorkSheet1.Cells[17, 1, 17 + ExcelData1.Rows.Count, 13])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }

                                        using (ExcelRange col = exWorkSheet1.Cells[RchartHeader, 7, RchartHeader + 4, 9])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }

                                        using (ExcelRange col = exWorkSheet1.Cells[chartHeader, 3, chartHeader + 4, 5])
                                        {
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }

                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                                }
                                #endregion
                            }
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        #endregion
                    }
                    else
                    {

                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
            BindPeriod(txtFrequency.Text.Substring(0, 1).ToString());
        }

        protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            BindPeriod("");
            bindPageNumber();
            ShowGridDetail();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        //Function to Convert Base64 to Image obj
        public static System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                return image;
            }
        }



        //private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2, Color color)
        //{
        //    PdfContentByte contentByte = writer.DirectContent;
        //    contentByte.SetColorStroke(color);
        //    contentByte.MoveTo(x1, y1);
        //    contentByte.LineTo(x2, y2);
        //    contentByte.Stroke();
        //}
        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            //cell.BorderColor = Color.WHITE;
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 0f;
            return cell;
        }
        private static PdfPCell ImageCell(string path, float scale, int align)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path));
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            //  cell.BorderColor = Color.;
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 0f;
            cell.PaddingTop = 0f;
            return cell;
        }

        public PdfPCell getCell(String text, int alignment)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text));
            cell.Border = PdfPCell.NO_BORDER;
            return cell;
        }

        protected void txtAuditDate_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}