﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSVendorAuditDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSVendorAuditDashboard" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">
    <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="/avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="/newcss/spectrum.css" rel="stylesheet" />

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>


    <script type="text/javascript" src="/newjs/spectrum.js"></script>
    <script src="/Newjs/jquery.cookie.js"></script>

    <style type="text/css">
       .modal-dialog {
                left: 50%;
                right: auto;
                width: 1200px;
                padding-top: 30px;
                padding-bottom: 30px;
            }
        .info-box .count {
            color: #fff;
        }

        .chosen-container {
            text-align: left;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .info-box:hover {
            /*color: #FF7473;*/
            font-weight: 500;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            z-index: 5;
        }

        .function-selector-radio-label {
            font-size: 12px;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }

        #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
            border: 1px solid white;
        }

        .TMB {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 16.5% !important;
        }

        .TMB1 {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 19.5% !important;
        }

        .TMBImg {
            padding-left: 0px !important;
            padding-right: 0px !important;
            margin-left: -7%;
            margin-right: -3%;
        }

        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 45px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }
            .responsive-calendar{
                width: 103%;
            }
        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
            height: 285px !important;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 95px !important;
        }

        .Dashboard-white-widget {
            padding: 5px 0px 0px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        .TMBImg > img {
            width: 47px;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 18px;
        }

        td > label {
            padding: 6px;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 20px;
        }

        span.input-group-addon {
            padding: 0px;
        }

        td > label {
            padding: 3px 4px 0 4px;
            margin-top: -1%;
        }

        .nav-tabs > li > a {
            color: #333 !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #1fd9e1 !important;
        }
    </style>
    <style type="text/css">
        .Cancelled-bg {
            color: #fff;
            background: #8F45AD;
        }

        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btnss {
            background-image: url(../../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        .form-control {
            width: 85%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #8e8e93;
            background-color: #fff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .Dashboard-white-widget {
            background: none;
        }
    </style>
    <%--   <script type="text/javascript">
        $(document).ready(function () {
            debugger;
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                var hdnevent = $('#ContentPlaceHolder1_HdnTreeEvent').val();
                if (hdnevent == "Event") {
                    DropDownTree();
                }

            });
        });
       
   
    </script>--%>
    <script type="text/javascript">
        $(function () {
            $("[id*=tvBranches] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        })
    </script>

    <script language="javascript" type="text/javascript">

        //sandesh code start
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            // var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  name="' + getId + 'CheckBox" id="' + getId + 'CheckBox"><a id="' + getId + '" href="' + $(anch).attr('href') + '" onclick="' + $(anch).attr('onclick') + '" >' + anch.html() + '</a></br>';
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
        //added by sandesh end

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
            }


            $('#divFilterLocation').toggle();
        });
        $('#BodyContent_tbxFilterLocation').keyup(function () {
            FnSearch();
        });
        // })
    </script>

    <script type="text/javascript">


        function fopenmsgCancelschedule() {
            alert('Unable to cancel this audit it is in process');
        }


        function openModal() {
            $('#AuditPopUp').modal('show');
            return true;
        }
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            //prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
            BindRescheduleDate();
        });

        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {

            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
        }
        function BindRescheduleDate() {
            $(function () {
                $('input[id*=txtStartDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
        }
        function fopenpopup(flag) {
            if (flag == 0) {
                alert('Select End Date');
            } else if (flag == 5) {
                alert('Value is not a DateTime');
            } else if (flag == 4) {
                alert('End date cannot be less than start date');
            } else if (flag == 3) {
                alert('End date cannot be start date');
            }
            BindControls();
        }

        function FRemarkUpdate() {
            alert('Audit Cancellation Save Successfully.');
            window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
        }
        function FRescheduleUpdate() {
            alert('Audit Reschedule Save Successfully.');
            window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
        }
        function FRescheduledateValidation() {
            alert('End Date should be greater than Start Date.');
            BindRescheduleDate();
        }
        function UploadExcelSheetValidation() {
            debugger;
            if ($('#ContentPlaceHolder1_ExcelFileUpload').val() == "") {
                alert('Please Select Excel Sheet');
                return false;
            }
           
        }

        function FRescheduleValidation() {
            alert('Audit Reschedule not Save Successfully.');
            BindRescheduleDate();
        }
        function FAlreadyValidation() {
            alert('Audit Already Rescheduled for selected period.');
            BindRescheduleDate();
        }

        function fopenpopupUpdateStatus() {
            $('#divOpenUpdateStatusPopup').modal('show');
        }
        function fopenpopupReschedule() {
            BindRescheduleDate();
            $('#divOpenUpdateReschedulePopup').modal('show');
        }
        function OpenUploadDocumentPopup() {
            debugger;
            $("#UploadDocumentCountModal").modal("show");
        }
    </script>
    <script>
   
    </script>
    <style type="text/css">
        .filterlocation {
            overflow: auto;
            border-left: 1px solid #c7c7cc;
            border-right: 1px solid #c7c7cc;
            border-bottom: 1px solid #c7c7cc;
            background-color: #ffffff;
            color: #8e8e93 !important;
        }

        .compliances-bg {
            color: #fff;
            background: #64B973;
        }

        .overdue-bg {
            color: #fff;
            background: #FF7473;
        }

        .info-box {
            min-height: 75px !important;
            width: 90%;
            margin-left: 30px;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 0px"></div>

    <section class="wrapper" style="width: 98%; margin: 0;">
        <div class="row">
            <div class="dashboard">
                <!--Performer Summary-->
                <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="min-height: 41px; padding: 5px;">
                            <h2>Audit Summary</h2>
                            <div class="panel-actions" style="display: none;">
                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="collapsePerformer" class="panel-collapse collapse in">
                        <div class="row ">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                <div class="info-box bluenew-bg">
                                    <div class="titleMD">Open</div>
                                    <div class="col-md-12">
                                        <a href="#" onclick="return fcallcal('open')">
                                            <div class="count" runat="server" id="divOpencount">0</div>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                          
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                <div class="info-box compliances-bg">
                                    <div class="titleMD">Closed</div>
                                    <div class="col-md-12">
                                        <a href="#" onclick="return fcallcal('complete')">
                                            <div class="count" runat="server" id="divClosedcount">0</div>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                           
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                <div class="info-box Cancelled-bg">
                                    <div class="titleMD">Cancelled</div>
                                    <div class="col-md-12">
                                        <a href="#" onclick="return fcallcal('delayed')">
                                            <div class="count" runat="server" id="divCancelledcount">0</div>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                <div class="info-box overdue-bg">
                                    <div class="titleMD">Overdue</div>
                                    <div class="col-md-12">
                                        <a href="#" onclick="return fcallcal('Overdue')">
                                            <div class="count" runat="server" id="divOverduecount">0</div>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <%  if (user_Roles.Contains("HVAUD"))
                            { %>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                <div class="info-box overdue-bg" style="background: #9ba097;">
                                    <div class="titleMD"></div>
                                    <div class="col-md-12">
                                        <a href="#" onclick="return OpenUploadDocumentPopup()">
                                             <div class="count" runat="server" id="div1" style="font-size: 18px;">Submitted</div>
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <%}%>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <asp:HiddenField runat="server" ID="HdnCustVal" />
    <asp:HiddenField runat="server" ID="HdnTreeEvent" Value="" />
    <%-- <div class="row Dashboard-white-widget">
                <div class="dashboard">--%>

    <%-- <div class="col-md-12 colpadding0" style="margin-bottom:10px;"  runat="server">
    --%>
    <div class="row" id="ComplianceCalender" style="margin-bottom: 10px;">
        <div class="col-md-12">
            <div class="row" style="background-color: #ffffff; padding-top: 15px;">
                <div class="col-md-12">
                    <div class="row">
                    <%--<div class="col-md-4" style="background-color: #ffffff; height: 409px">
                        <div>
                            <fieldset runat="server" id="fldsCalender">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <div class="radiobuttoncalendercontainer ml5">
                                            <asp:RadioButtonList ID="rdbcalender" runat="server"
                                                RepeatDirection="Horizontal" AutoPostBack="false"
                                                OnSelectedIndexChanged="rdbcalender_SelectedIndexChanged" Visible="false">
                                                <asp:ListItem Text="Statutory + Internal " Value="0" />
                                                <asp:ListItem Text="All(Including Checklist)" Value="1" Selected="True" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="rdbcalender" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </fieldset>
                        </div>

                        <!-- Responsive calendar - START -->
                        <div class="responsive-calendar mr10" id="calenderDiv" style="margin-left: -11px;">
                            <div class="chart-loading" id="idloadercal2"></div>
                            <%--//<img src="../images/processing.gif" style="position: absolute;">
                        </div>
                        <!-- Responsive calendar - END -->
                    </div>--%>

                    <div class="col-md-12" style="background-color: #ffffff;">
                        <div style="">
                            <span id="clsdatel" style="color: #515967;"></span>
                            <br />
                          <%--  <i style="color: #515967;">Select a date from calendar to view details</i>--%>
                        </div>
                        <div class="clearfix"></div>
                        <div id="datacal">
                            <div class="chart-loading" id="idloadercal"></div>
                            <%--<img src="../images/processing.gif" id="imgcaldate" style="position: absolute;">--%>
                            <iframe id="calframe" src="about:blank" scrolling="yes" frameborder="0" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>


    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="col-lg-12 col-md-12 " style="">
                <section class="panel" style="border-radius: 10px;">
                    <div class="clearfix"></div>
                     <div class="panel-heading" style="min-height: 41px; padding: 5px;">
                            <h2>Upcoming Audits</h2>
                            <div class="panel-actions" style="display: none;">
                                <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <%--<div class="col-md-12 colpadding0" style="text-align: right; float: right">--%>
                            <div class="row col-md-12 colpadding0">
                                <div class="col-md-1">
                                    <%-- <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;width:30px">Show </p>
                                    </div>--%>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 64px; float: left; margin-left: 10px;">
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2" style="width: 105px;">
                                    <asp:DropDownList runat="server" ID="ddlAuditType" class="form-control m-bot15 search-select"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="All" Value="-1" Selected="True" />
                                        <asp:ListItem Text="Open" Value="1" />
                                        <asp:ListItem Text="Closed" Value="4" />
                                        <asp:ListItem Text="Cancelled" Value="5" />
                                    </asp:DropDownList>
                                </div>

                                <%-- <div class="col-md-2 colpadding0">--%>
                                <div class="col-md-4 colpadding0 pl0" id="CustomerList1" runat="server" style="width: 300px">
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterCustomer" Width="300px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        OnSelectedIndexChanged="ddlFilterCustomer_SelectedIndexChanged"
                                        DataPlaceHolder="Select Customer"
                                        class="form-control" Enabled="true"
                                        AutoPostBack="true" />

                                </div>
                                
                                <div class="col-md-2">
                                <asp:DropDownListChosen runat="server" ID="ddlState"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                                        DataPlaceHolder="Select State"
                                        class="form-control" Width="165px" Enabled="true"
                                        AutoPostBack="true" />
                               </div>
                                <%-- OnSelectedIndexChanged="ddlFilterCustomer_SelectedIndexChanged"--%>
                               

                                <%--       style="padding-left: 38px;--%>

                                 <div id="FilterLocationdiv" runat="server" class="col-md-4">

                                    <asp:TextBox runat="server" ID="tbxFilterLocation" placeholder="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Entity/State/Location" Style="padding: 0px; margin: 0px; width: 103%"
                                        CssClass="form-control" />
                                    <div style="position: absolute; z-index: 10;     margin-top: -20px;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" CssClass="filterlocation" BackColor="White"
                                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="329px"
                                            Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)">
                                        </asp:TreeView>

                                        <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>

                                        <asp:Button ID="btnlocation" runat="server" Text="Select" OnClick="btnlocation_Click" />
                                        <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" />

                                    </div>
                                </div>                               
                            </div>
                            <div class="row col-md-12" style="">
                                <div class="col-md-3">
                                    <asp:DropDownListChosen runat="server" ID="ddlAuditPeriod"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                                        OnSelectedIndexChanged="ddlFilterPeriod_SelectedIndexChanged"
                                        DataPlaceHolder="Select Period"
                                        class="form-control" Width="200px" Enabled="true"
                                        AutoPostBack="true" />
                                </div>
                                <div class="col-md-3 colpadding0 pl0">
                                       
                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                                </div>
                                <div  class="col-md-3 colpadding0 pl0">
                                       <asp:Button ID="btnClearFilters" CausesValidation="false" class="btn btn-search asp-btn mr-10" runat="server" Text="Clear" OnClick="btnClearFilters_Click" />
                  
                                </div>
                            </div>

                            <div class="clearfix">
                                <div runat="server" id="DivRecordsScrum" style="float: right;" class="AdvanceSearchScrum">
                                    <p style="padding-right: 0px !Important;">
                                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>

                            </div>
                        </div>

                        <div class="tab-content ">
                            <div runat="server" id="performerdocuments" class="tab-pane active">
                                <asp:GridView runat="server" ID="grdAuditDetail" AutoGenerateColumns="false" AllowSorting="true"
                                    GridLines="None" CssClass="table" OnRowCommand="grdAuditDetail_RowCommand"
                                    OnRowDataBound="grdAuditDetail_RowDataBound"
                                    AllowPaging="True" PageSize="10"
                                    DataKeyNames="AuditID">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                <asp:Label ID="lblAuditScheduleOnID" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                <asp:Label ID="lblAuditStatusID" runat="server" Text='<%# Eval("AuditStatusID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lbllocation" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                        Text='<%# Eval("Location") %>' ToolTip='<%# Eval("Location") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="LabeVendorName" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                        Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Audit Period ">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="LabeAuditName" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                        Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                    Text='<%# Eval("StartDate")!= null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy"):""%>'
                                                    ToolTip='<%# Eval("StartDate")!= null ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>

                                                <asp:TextBox ID="txtEndDate" class="form-control" runat="server"
                                                    Text='<%# Eval("EndDate")!= null ?  Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") : ""%>'>
                                                </asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton runat="server" ID="btnChangeStatus" ToolTip="Edit"
                                                            CommandName="CHANGE_EDIT" CommandArgument='<%# Eval("AuditID") + "," + Eval("AuditScheduleOnID") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Save End Date" />
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="lnkSave" runat="server" CommandName="CHANGE_SAVE" ToolTip="Save" data-toggle="tooltip"
                                                            CommandArgument='<%# Eval("AuditID") + "," + Eval("AuditScheduleOnID") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/Save-icon.png")%>' alt="Save End Date" /></asp:LinkButton>

                                                        <asp:LinkButton ID="lnkCancel" runat="server" CommandName="CHANGE_STATUS" ToolTip="Change Status" data-toggle="tooltip"
                                                            CommandArgument='<%# Eval("AuditID") + "," + Eval("AuditScheduleOnID") + "," + Eval("Location")  + "," + Eval("ForMonth") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change For Cancel" /></asp:LinkButton>


                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnChangeStatus" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div style="margin-bottom: 4px">
                                    <table width="100%" style="height: 10px">
                                        <tr>
                                            <td>
                                                <div style="margin-bottom: 4px">
                                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <div class="table-paging" style="margin-bottom: 20px">
                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="../../img/paging-left-active.png" OnClick="lBPrevious_Click" />

                                        <div class="table-paging-text">
                                            <p>
                                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="../../img/paging-right-active.png" OnClick="lBNext_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <%-- </div>
            </div>--%>
    <tr id="trErrorMessage" runat="server" visible="true">
        <td colspan="3" style="background-color: #e9e1e1;">
            <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                ForeColor="Red"></asp:Label>
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
        </td>
    </tr>

    <div class="modal fade" id="divOpenUpdateStatusPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" style="font-weight: 600; font-size: 20px;">
                        Audit Cancellation</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upMailDocument" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="FolderValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsFolderDocumentValidationGroup" />
                                    <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row" style="margin: 9px;">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2" style="color: #333;">
                                        Location:
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label runat="server" ID="lblLocation" Style="color: #333;"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 9px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2" style="color: #333;">
                                        Period:                                        
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label runat="server" ID="lblPeriod" Style="color: #333;"></asp:Label>
                                    </div>
                                </div>
                            </div>


                            <div class="row" style="margin: 9px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-2" style="color: #333;">
                                        <asp:Label runat="server" ID="Label3" Style="color: red;">*</asp:Label>
                                        Remark:                                        
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox runat="server" ID="txtRemark" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required Remark"
                                            ControlToValidate="txtRemark" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="HiddenFieldAuditId" runat="server" Value="0" />
                            <asp:HiddenField ID="HiddenFieldScheduleOnId" runat="server" Value="0" />

                            <div class="row">
                                <div class="form-group required col-md-12">

                                    <div class="col-md-2" style="color: #333;">
                                    </div>
                                    <div class="col-md-8 text-center">
                                        <asp:Button Text="Update" runat="server" ID="btnUpdateStatus" CssClass="btn btn-primary"
                                            ValidationGroup="vsFolderDocumentValidationGroup" OnClick="btnUpdateStatus_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="divOpenUpdateReschedulePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" style="font-weight: 600; font-size: 20px;">
                        Reschedule Audit</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="None"
                                        class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsFolderDocumentValidationGroup1" />
                                    <asp:CustomValidator ID="cvReschedule" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                </div>
                            </div>
                            <div class="row" style="margin: 9px;">

                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3" style="color: #333;">
                                        Location:
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label runat="server" ID="lblLocation1" Style="color: #333;"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 9px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3" style="color: #333;">
                                        Period:                                        
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label runat="server" ID="lblPeriod1" Style="color: #333;"></asp:Label>
                                    </div>
                                </div>
                            </div>


                            <div class="row" style="margin: 9px;">
                                <div class="col-md-12 colpadding0">

                                    <div class="col-md-3" style="color: #333;">
                                        <asp:Label runat="server" ID="Label1" Style="color: red;">*</asp:Label>
                                        Start Date:                                        
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox runat="server" ID="txtStartDate" placeholder="Start Date" autocomplete="off" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Required Start Date"
                                            ControlToValidate="txtStartDate" runat="server" ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin: 9px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3" style="color: #333;">
                                        <asp:Label runat="server" ID="Label2" Style="color: red;">*</asp:Label>
                                        End Date:                                        
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox runat="server" ID="txtEndDate" autocomplete="off" placeholder="End Date" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Required End Date"
                                            ControlToValidate="txtEndDate" runat="server" ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin: 9px;">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3" style="color: #333;">
                                        <asp:Label runat="server" ID="Label5" Style="color: red;">*</asp:Label>
                                        Remark:                                        
                                    </div>
                                    <div class="col-md-8">
                                        <asp:TextBox runat="server" ID="txtRemarkReschedule" TextMode="MultiLine" Rows="3"
                                            CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                            ErrorMessage="Required Remark"
                                            ControlToValidate="txtRemarkReschedule" runat="server"
                                            ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="HiddenFieldScheduleOnId1" runat="server" Value="0" />
                            <asp:HiddenField ID="HiddenFieldAuditId1" runat="server" Value="0" />

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="col-md-3" style="color: #333;">
                                    </div>
                                    <div class="col-md-8 text-center">
                                        <asp:Button Text="Save" runat="server" ID="btnReschedule" CssClass="btn btn-primary"
                                            ValidationGroup="vsFolderDocumentValidationGroup1" OnClick="btnReschedule_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="divUploadExcelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" style="font-weight: 600; font-size: 20px;">
                       Upload Excel</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;height: 70px;">
                   <div class="col-lg-5 col-md-5 col-sm-5">
                                <asp:FileUpload ID="ExcelFileUpload" Style="margin-top: 6px;color:black;"   runat="server" />
                        </div>
                            <div class="col-lg-1 col-md-1 col-sm-1">
                                <asp:Button ID="btnExcelUpload" runat="server" Text="Upload" 
                                        class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload" OnClientClick="return UploadExcelSheetValidation()" OnClick="btnExcelUpload_Click"
                                      />
                        </div> 
                </div>
            </div>
        </div>
    </div>


      <!-- Modal CheckList Count -->
    <div class="modal fade" id="UploadDocumentCountModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Submitted Document Summary</h4>
                </div>
                <div class="modal-body">
                     <div class="row Mar-Top15">
                        <div class="col-lg-12 col-md-12 col-sm-12 pull-left">
                        <asp:LinkButton ID="lnkDownloadSubmittedCount" runat="server" Style="float: right;" ToolTip="Download Submitted Count" data-toggle="tooltip" data-placement="bottom" OnClick="lnkDownloadSubmittedCount_Click">                                   
                    <img src='../../Images/download_icon_new.png' alt="Download Upcoming Hearing"/></asp:LinkButton>
                    </div>
                    </div>
                    <div class="row Mar-Top15">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div>
                <asp:GridView runat="server" ID="grdSubmittedDocumentDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                     AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" OnRowCommand="grdSubmittedDocumentDetails_RowCommand" PageSize="10" AllowPaging="true" >
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Period" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>' ></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Open" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Open") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Submitted" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Submitted") %>' ></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Closed" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Closed") %>' ></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Team Review" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Team_Review") %>' ></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Total") %>' ></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                            <ItemTemplate>
                                  <asp:LinkButton runat="server" CommandName="CHANGE_EDIT" ID="btnChangeStatus" ToolTip="Edit" OnClientClick="fopenpopup()"
                                                                CommandArgument='<%# Eval("AuditID") + "," + Eval("ScheduledOnID") + "," + Eval("VendorName") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Save End Date" />
                                                            </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
                                
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!--End  Modal CheckList Count -->
    <script>

        $(document).ready(function () {
            //$.get("../RLCSVendorAudit/MyComplianceCalenderAudit.aspx?m=8&type=0", function (data) {
            //    debugger;
            //    $("#calenderDiv").html(data);
            //});
            setInterval(setcolor, 1000);

            $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=null&type=0&AuditType=null')
        });

        var strop = 0;


        function fcallcal(val) {
            debugger;
            strop = 1;
            // $(".responsive-calendar").html('<div class="chart-loading" id="idloadercal4"></div>');
            //$(".responsive-calendar").html('<img src="../images/processing.gif">');
            $.get("../RLCSVendorAudit/MyComplianceCalenderAudit.aspx?m=8&type=0&AuditStatus=" + val, function (data) {
               // $("#calenderDiv").html("");
               // $("#calenderDiv").html(data);
                strop = 0;
            });
            if (val !=undefined && val!=null) {
                if(val =="open")
                {
                    $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=null&type=0&AuditType=1')
                }
                else if (val == "complete") {
                    $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=null&type=0&AuditType=4')
                }
                else if (val == "delayed") {
                    $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=null&type=0&AuditType=5')
                }
               else if (val == "Overdue") {
                    $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=null&type=0&AuditType=1&overdue=O')
               }
               else
               {
                 //  

               }
            }
        }

        function setcolor() {
            $('.overdue').closest('div').find('a').css('background-color', '#FF0000');
            $('.complete').closest('div').find('a').css('background-color', '#006500');
            $('.pending').closest('div').find('a').css('background-color', '#00008d');
            $('.delayed').closest('div').find('a').css('background-color', '#ffcd70');
        }

        $(document).ready(function (ClassName) {
            $("a").addClass(ClassName);
        });

        function formatDate(date) {
            var monthNames = [
              "January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"
            ];
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }

        function fclosepopcal(dt) {
            $('#divreports').modal('hide');
            fcal(dt)
        }

        function hideloader() {
            $('#imgcaldate').hide();
            $('#imgcaldate1').hide();
        }

        function fcal(dt,Type) {
            debugger;
            try {
                // $(".responsive-calendar").html(ddata);
                var ddata = new Date(dt);
                $('#clsdatel').html('Compliance items for date ' + formatDate(ddata));
                $('#clsdatel1').html('Compliance items for date ' + formatDate(ddata));
            } catch (e) { }

            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            if (firstDay != dt) {
                //$('#imgcaldate').show();
                $('#idloadercal').show();
                if (Type != undefined) {

                    if (Type == "open") {
                        $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=' + dt + '&type=0&AuditType=1')
                    }
                    else if (Type == "complete") {
                        $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=' + dt + '&type=0&AuditType=4')
                    }
                    else if (Type == "delayed") {
                        $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=' + dt + '&type=0&AuditType=5')
                    }
                    else if (Type == "Overdue") {
                        $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=' + dt + '&type=0&AuditType=1&overdue=O')
                      
                    }

                    //$('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=' + dt + '&type=0&AuditType=' + Type)
                }
                else
                    $('#calframe').attr('src', '../RLCSVendorAudit/RLCS_VendorAuditMyCalenderData.aspx?m=8&date=' + dt + '&type=0')

            }
            //$('#imgcaldate1').show();
          
            return;
        }

        function Editpopup(AuditID, AuditScheduleOnID, VendorName) {
            debugger;
            window.location = '/RLCSVendorAudit/RLCSVendorAuditChecklist.aspx?AID=' + AuditID + '&SOID=' + AuditScheduleOnID + '&VendorName=' + VendorName;
        }
    </script>

</asp:Content>

