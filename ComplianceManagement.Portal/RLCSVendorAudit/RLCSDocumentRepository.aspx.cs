﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Data;
using System.Web.Script.Serialization;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSDocumentRepository : System.Web.UI.Page
    {

        static long  ddlContractorID = 0;
        static long ddlAuditID = 0;
        public static string SchedulePeriod;

        public static long  CustomerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    bindCustomer();
                    //BindActMasterGrid();
                    //bindActList();
                    //bindStateList();
                    //BindGrid();

                    //if (ddlCentralOrState.SelectedValue == "Central")
                    //    ddlStateFilter.Enabled = false;
                    //else
                    //{
                    //    bindStateList();
                    //    ddlStateFilter.Enabled = true;
                    //}

                    //if (ddlStateFilterMaster.SelectedValue == "Central")
                    //    ddlStateFilter1.Enabled = false;
                    //else
                    //{
                    //    bindStateNewList();
                    //    ddlStateFilter1.Enabled = true;
                    //}
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #region bind StateDetails 
        private void bindStateList(int CustID)
        {
            try
            {
                var StateList = RLCSVendorMastersManagement.GetStateEntityWise(CustID);//RLCSVendorMastersManagement.getRLCSddlStateData();
                ddlState.DataTextField = "NAME";
                ddlState.DataValueField = "ID";
                ddlState.DataSource = StateList;
                ddlState.DataBind();
                //ddlStateFilter.Items.Insert(0, new ListItem("Select State", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindStateNewList()
        {
            try
            {
                var StateList = RLCSVendorMastersManagement.getRLCSddlStateData();
                StateList = StateList.Where(x => !x.SM_Code.Equals("CENTRAL")).ToList();

                ddlStateFilter1.DataTextField = "SM_Name";
                ddlStateFilter1.DataValueField = "SM_Code";
                ddlStateFilter1.DataSource = StateList;
                ddlStateFilter1.DataBind();
                ddlStateFilter1.Items.Insert(0, new ListItem("Select State", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion


        private void bindFrequency(int CustID)
        {
            try
            {
                List<string> listStringPeriod = new List<string>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.RLCS_VendorAuditInstance
                                 where row.AVACOM_CustomerID == CustID
                                 orderby row.ID descending
                                 select row).FirstOrDefault();

                    if (query.CC_Frequency == "A" || query.CC_Frequency == "Y")
                    {
                        txtFequency.Text = "Annually";
                        listStringPeriod.Add("Jan - Dec"); 
                    }
                    if (query.CC_Frequency == "H")
                    {
                        txtFequency.Text = "Half Yearly";
                        listStringPeriod.Add("Jan - Jun");
                        listStringPeriod.Add("Jul - Dec");
                    }
                    if (query.CC_Frequency == "Q")
                    {
                        txtFequency.Text = "Quarterly";
                        listStringPeriod.Add("Jan - Mar");
                        listStringPeriod.Add("Apr - Jun");
                        listStringPeriod.Add("Jul - Sep");
                        listStringPeriod.Add("Oct - Dec");
                    }
                    if (query.CC_Frequency == "M")
                    {
                        txtFequency.Text = "Monthly";
                        listStringPeriod.Add("Jan");
                        listStringPeriod.Add("Feb");
                        listStringPeriod.Add("Mar");
                        listStringPeriod.Add("Apr");
                        listStringPeriod.Add("May");
                        listStringPeriod.Add("Jun");
                        listStringPeriod.Add("Jul");
                        listStringPeriod.Add("Aug");
                        listStringPeriod.Add("Sep");
                        listStringPeriod.Add("Oct");
                        listStringPeriod.Add("Nov");
                        listStringPeriod.Add("Dec");
                    }
                    if (query.CC_Frequency == "O")
                    {
                        txtFequency.Text = "OnOccurence";
                        listStringPeriod = null;
                        ddlPeriod.DataSource = null;
                    }

                    
                    ddlPeriod.DataSource = listStringPeriod;
                    ddlPeriod.DataBind();
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "BindPeriods(" + query.CC_Frequency + ");", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region bind act detail 
        private void bindActList()
        {
            try
            {
                string stateorcentral = string.Empty;
                if (!string.IsNullOrEmpty(ddlCentralOrState.SelectedValue))
                {
                    stateorcentral = Convert.ToString(ddlCentralOrState.SelectedItem.Text);
                }
                var Actlist = RLCSVendorMastersManagement.getRLCSActlistData(stateorcentral);
                ddlActFilter.DataTextField = "AM_ActName";
                ddlActFilter.DataValueField = "AM_ActID";
                ddlActFilter.DataSource = Actlist;
                ddlActFilter.DataBind();
                ddlActFilter.Items.Insert(0, new ListItem("Select Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        public void bindCustomer()
        {

            string role = string.Empty;
            try
            {
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (LoggedUser != null)
                {
                    role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                    ViewState["vrole"] = Convert.ToString(role);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
          

            string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
            if (CheckContractCustomer == "True")
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, role, AuthenticationHelper.CustomerID);
                
                ddlCustomer.DataSource = lstCustomers;
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataBind();
            }
            else
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);

                ddlCustomer.DataSource = lstCustomers;
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataBind();
            }

            //bindStateList(Convert.ToInt32(ddlCustomer.SelectedValue));
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlCustomer.SelectedValue == null || ddlCustomer.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Customer.');", true);
                }

                if (ddlState.SelectedValue == null || ddlState.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select State.');", true);
                }

                if (ddlLocationFilter.SelectedValue == null || ddlLocationFilter.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Location.');", true);
                }

                if (ddlBranchFilter.SelectedValue == null || ddlBranchFilter.SelectedValue == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Branch.');", true);
                }

                if (ddlContractor.SelectedValue != "")
                {
                    if (ddlPeriod.SelectedValue == "")
                    {
                        SchedulePeriod = "";
                    }
                    BindGrid();
                    upUploadAuditChecklist.Update();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select contractor.');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlCustomer.SelectedIndex = -1;
                ddlState.SelectedIndex = -1;
                ddlLocationFilter.SelectedIndex = -1;
                ddlBranchFilter.SelectedIndex = -1;
                ddlContractor.SelectedIndex = -1;
                ddlCentralOrState.Text = "Central";
                ddlStateFilter.SelectedIndex = -1;
                txtFequency.Text = "";
                ddlYear.SelectedIndex = -1;
                ddlPeriod.SelectedIndex = -1;
                customerSpecific.Checked = false;
                grdChecklistDetails.DataSource = null;
                grdChecklistDetails.DataBind();

                upUploadAuditChecklist.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void BindGrid()
        {
            try
            {
                List<VendorUnassignCheckListDetails> UNAssignlstActDetailsList = new List<VendorUnassignCheckListDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region new  
                    //var lstChecklistDetails = entities.SP_RLCS_VendorACT_Mapping().ToList();
                    var lstChecklistDetails = entities.SP_RLCS_Vendor_CheckListDetail().ToList();


                    string stateorcentral = string.Empty;
                    if (!string.IsNullOrEmpty(ddlCentralOrState.SelectedValue))
                    {
                        stateorcentral = Convert.ToString(ddlCentralOrState.SelectedItem.Text);
                        if (stateorcentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                        {
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.StateID.ToUpper().Trim() == stateorcentral.ToUpper().Trim()).ToList();
                        }
                        else
                        {
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.StateName.ToUpper().Trim() != "Central".ToUpper().Trim()).ToList();
                        }
                    }
                    string StateID = string.Empty;
                    if (!string.IsNullOrEmpty(ddlStateFilter.SelectedValue))
                    {
                        if (ddlStateFilter.SelectedValue != "-1")
                        {
                            StateID = Convert.ToString(ddlStateFilter.SelectedValue);
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.StateID == StateID).ToList();
                        }
                    }

                    if (ddlContractor.SelectedValue != "")
                    {
                        ddlContractorID = Convert.ToInt32(ddlContractor.SelectedValue);
                        ddlAuditID = Convert.ToInt64(hdAuditorID.Text);
                        if (!string.IsNullOrEmpty(ddlStateFilter.SelectedValue) &&  (ddlStateFilter.SelectedValue != "-1"))
                        {

                            //lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditID == ddlAuditID && x.AM_StateID == ddlStateFilter.SelectedValue).ToList();
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.StateID == ddlStateFilter.SelectedValue).ToList();
                        }
                        //else
                        //{
                        //    lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditID == ddlAuditID).ToList();
                        //}

                        if (customerSpecific.Checked)
                        {
                            lstChecklistDetails = lstChecklistDetails.Where(x => x.CustomerID == CustomerID).ToList(); ;
                        }
                    }
                    
                    //if (ddlPeriod.SelectedValue != "")
                    //{
                    //    var yearLastTwo = ddlYear.SelectedValue.Substring(2, 2);
                    //    string FirstMonth = ddlPeriod.SelectedValue.Substring(0, 3);
                    //    string SecondMonth = ddlPeriod.SelectedValue.Substring(ddlPeriod.SelectedValue.Length - 3);
                    //    SchedulePeriod = FirstMonth + " " + yearLastTwo + " " + "-" + " " + SecondMonth + " " + yearLastTwo;
                    //    lstChecklistDetails = lstChecklistDetails.Where(x => x.SchedulePeriod == SchedulePeriod).ToList();
                    //}
                    //lstChecklistDetails = lstChecklistDetails.GroupBy(p => p.ChecklistID)
                    //      .Select(g => g.First())
                    //      .ToList();

                    if (lstChecklistDetails.Count > 0)
                    {
                        foreach (var row in lstChecklistDetails)
                        {
                            long Audid = Convert.ToInt64(ddlAuditID);
                            var AuditDetailsObj = (from aduit in entities.RLCS_VendorAuditInstance
                                                   where aduit.ID == Audid
                                                   select aduit).FirstOrDefault();
                            string VendorName = AuditDetailsObj.CC_ContractorName;
                            int BranchID = Convert.ToInt32(AuditDetailsObj.AVACOM_BranchID);

                            var BranchDetailsObj = (from brch in entities.CustomerBranches
                                                    where brch.ID == BranchID
                                                    select brch).FirstOrDefault();
                            string BranchName = BranchDetailsObj.Name;


                            VendorUnassignCheckListDetails UNAssignlstActDetails = new VendorUnassignCheckListDetails();

                            UNAssignlstActDetails.Id = Convert.ToInt32(row.Id);
                            //AssignlstActDetails.ScheduleONID = row.ScheduleOnID;
                            UNAssignlstActDetails.ChecklistID = row.ChecklistID;
                            UNAssignlstActDetails.ActID = row.ActID;
                            UNAssignlstActDetails.NatureOfCompliance = row.NatureOfCompliance;
                            UNAssignlstActDetails.AM_ActName = row.AM_ActName;
                            UNAssignlstActDetails.AM_ActGroup = row.AM_ActGroup;
                            UNAssignlstActDetails.StateID = row.StateID;
                            UNAssignlstActDetails.StateName = row.StateName;
                            UNAssignlstActDetails.CreatedOn = Convert.ToDateTime(row.CreatedOn);
                            UNAssignlstActDetails.Branch = BranchName;
                            UNAssignlstActDetails.Risk = row.Risk;
                            UNAssignlstActDetailsList.Add(UNAssignlstActDetails);


                        }
                        grdChecklistDetails.DataSource = null;
                        grdChecklistDetails.DataSource = UNAssignlstActDetailsList.Distinct();
                        grdChecklistDetails.DataBind();
                        Session["TotalRows"] = UNAssignlstActDetailsList.Count;
                    }
                    else
                    {
                        grdChecklistDetails.DataSource = null;
                        grdChecklistDetails.DataBind();
                    }

                    lstChecklistDetails.Clear();
                    lstChecklistDetails = null;
                    UNAssignlstActDetailsList = null;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Page Number-Bottom
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

        //        BindGrid();

        //        int count = Convert.ToInt32(GetTotalPagesCount());
        //        if (count > 0)
        //        {
        //            int gridindex = grdChecklistDetails.PageIndex;
        //            string chkcindition = (gridindex + 1).ToString();
        //            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
        //        }
        //        bindPageNumber();
        //        ShowGridDetail();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        //protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
        //    if (chkSelectedPage > 0)
        //        grdChecklistDetails.PageIndex = chkSelectedPage - 1;

        //    else
        //        grdChecklistDetails.PageIndex = 0;

        //    grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

        //    BindGrid(); ShowGridDetail();
        //}

        //private void bindPageNumber()
        //{
        //    try
        //    {
        //        int count = Convert.ToInt32(GetTotalPagesCount());

        //        if (DropDownListPageNo.Items.Count > 0)
        //        {
        //            DropDownListPageNo.Items.Clear();
        //            DropDownListPageNo.SelectedValue = null;
        //        }

        //        DropDownListPageNo.DataTextField = "ID";
        //        DropDownListPageNo.DataValueField = "ID";

        //        DropDownListPageNo.DataBind();
        //        for (int i = 1; i <= count; i++)
        //        {
        //            string chkPageID = i.ToString();
        //            DropDownListPageNo.Items.Add(chkPageID);
        //        }
        //        if (count > 0)
        //        {
        //            DropDownListPageNo.SelectedValue = ("1").ToString();
        //        }
        //        else if (count == 0)
        //        {
        //            DropDownListPageNo.Items.Add("0");
        //            DropDownListPageNo.SelectedValue = ("0").ToString();
        //        }
        //        ShowGridDetail();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void ShowGridDetail()
        //{
        //    if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
        //    {
        //        var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
        //        var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
        //        var EndRecord = 0;
        //        var TotalRecord = 0;
        //        var TotalValue = PageSize * PageNumber;

        //        TotalRecord = Convert.ToInt32(Session["TotalRows"]);
        //        if (TotalRecord < TotalValue)
        //        {
        //            EndRecord = TotalRecord;
        //        }
        //        else
        //        {
        //            EndRecord = TotalValue;
        //        }
        //        lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
        //        lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
        //        lblTotalRecord.Text = TotalRecord.ToString();
        //    }
        //    else
        //    {
        //        lblStartRecord.Text = "0 ";
        //        lblEndRecord.Text = "0 ";
        //        lblTotalRecord.Text = "0";
        //    }
        //}

        //private int GetTotalPagesCount()
        //{
        //    try
        //    {
        //        TotalRows.Value = "0";
        //        if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
        //        {
        //            TotalRows.Value = Convert.ToString(Session["TotalRows"]);
        //        }

        //        int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

        //        // total page item to be displyed
        //        int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

        //        // remaing no of pages
        //        if (pageItemRemain > 0)// set total No of pages
        //        {
        //            totalPages = totalPages + 1;
        //        }
        //        else
        //        {
        //            totalPages = totalPages + 0;
        //        }
        //        return totalPages;
        //    }
        //    catch (Exception ex)
        //    {
        //        return 0;
        //    }
        //}

        #endregion

        #region Common Methods
     

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        #endregion

       
        
        public class customerlist
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
      

        protected void grdChecklistDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Upload_Checklist"))
                {
                    long ChecklistID = -1;
                    long SchedOnId = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    ChecklistID = Convert.ToInt64(commandArgs[0]);
                    //SchedOnId = Convert.ToInt64(commandArgs[1]);
                    int userID = AuthenticationHelper.UserID;
                   
                    if (ChecklistID > 0)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "addUploadfile(" + ChecklistID + " , " + SchedOnId  + ");", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "addUploadfile(" + ChecklistID +  ");", true);
                    }//
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCustomer.SelectedIndex > 0)
            {
                //BindContractor(Convert.ToInt32(ddlCustomer.SelectedValue));
                bindStateList(Convert.ToInt32(ddlCustomer.SelectedValue));
                bindFrequency(Convert.ToInt32(ddlCustomer.SelectedValue));
                CustomerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                //BindGrid();ddlContractor.SelectedValue
                //upUploadAuditChecklist.Update();var data = entities.SP_RLCS_Vendor_GetContractorsList(branchid, Customerid).ToList();
            }
        }

        //protected void ddlCentralOrState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlCentralOrState.SelectedValue == "Central")
        //    {
        //        ddlState.Enabled = false;
        //        ddlState.SelectedValue = "-1";
        //    }
        //    else
        //    {
        //        //bindStateList();
        //        ddlState.Enabled = true;
        //    }
        //    bindActList();
        //    //BindGrid();
        //    //upUploadAuditChecklist.Update();
        //}
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindContractor(Convert.ToInt32(ddlCustomer.SelectedValue), ddlState.SelectedValue);
                bindLocation(ddlState.SelectedValue,Convert.ToInt32(ddlCustomer.SelectedValue));
                bindBranch(Convert.ToInt32(ddlCustomer.SelectedValue), ddlState.SelectedValue);


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            
        }

        protected void ddlLocationFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindBranch(Convert.ToInt32(ddlCustomer.SelectedValue), ddlLocationFilter.SelectedValue);
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        public void bindLocation(string StateID, int CustomerID)
        {
            try
            {
                List<object> EntityLocation = new List<object>();
                EntityLocation = RLCSVendorMastersManagement.GetLocationStateWise((dynamic)StateID, (int)CustomerID);
                ddlLocationFilter.DataSource = EntityLocation;
                ddlLocationFilter.DataValueField = "Name";
                ddlLocationFilter.DataTextField = "Name";
                ddlLocationFilter.DataBind();

                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void bindBranch(int CustomerID, string LocationID)
        {
            try
            {
                var branches = RLCSVendorMastersManagement.GetBranchLocationWise((int)CustomerID, LocationID);
                ddlBranchFilter.DataSource = branches;
                ddlBranchFilter.DataValueField = "ID";
                ddlBranchFilter.DataTextField = "Name";
                ddlBranchFilter.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        
        public void BindActMasterGrid()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region new                   
                    var lstActMasterlistDetails = entities.RLCS_Act_Mapping.Where(x => x.IsDeleted == false).ToList();

                    if (!string.IsNullOrEmpty(ddlStateFilterMaster.SelectedValue))
                    {
                        if (ddlStateFilterMaster.SelectedValue == "Central")
                        {
                            lstActMasterlistDetails = lstActMasterlistDetails.Where(x => x.AM_StateID == "Central").ToList();
                        }
                        else
                        {
                            lstActMasterlistDetails = lstActMasterlistDetails.Where(x => x.AM_StateID != "Central").ToList();
                        }

                        string StateID = string.Empty;
                        if (!string.IsNullOrEmpty(ddlStateFilter1.SelectedValue))
                        {
                            if (ddlStateFilter1.SelectedValue != "-1")
                            {
                                StateID = Convert.ToString(ddlStateFilter1.SelectedValue);
                                lstActMasterlistDetails = lstActMasterlistDetails.Where(x => x.AM_StateID == StateID).ToList();
                            }
                        }
                    }

                    grdActMaster.DataSource = lstActMasterlistDetails;
                    grdActMaster.DataBind();

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlStateFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActMasterGrid();
        }

        protected void ddlStateFilterMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStateFilterMaster.SelectedValue == "Central")
            {
                ddlStateFilter1.Enabled = false;
                ddlStateFilter1.SelectedValue = "-1";
            }
            else
            {
                bindStateNewList();
                ddlStateFilter1.Enabled = true;
            }
            BindActMasterGrid();
        }

        protected void ddlCentralOrState_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.getRLCSddlStateData();
                    data = data.Where(x => !x.SM_Code.Equals("CENTRAL")).ToList();
                    ddlStateFilter.DataTextField = "SM_Name";
                    ddlStateFilter.DataValueField = "SM_Code";
                    ddlStateFilter.DataSource = data;
                    ddlStateFilter.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlBranchFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var contractorDataList = entities.SP_RLCS_Vendor_GetContractorsList(Convert.ToInt32(ddlBranchFilter.SelectedValue), Convert.ToInt32(ddlCustomer.SelectedValue)).ToList();
                    ddlContractor.DataSource = contractorDataList;
                    ddlContractor.DataValueField = "ID";
                    ddlContractor.DataTextField = "CC_ContractorName";
                    ddlContractor.DataBind();

                   
                    if (ddlBranchFilter.SelectedValue != "" && ddlCustomer.SelectedValue != "")
                    {
                        BindYear(Convert.ToInt32(ddlBranchFilter.SelectedValue), Convert.ToInt32(ddlCustomer.SelectedValue));
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void ddlContractor_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ddlContractorID = Convert.ToInt32(ddlContractor.SelectedValue);
                var contractorDataList = entities.SP_RLCS_Vendor_GetContractorsList(Convert.ToInt32(ddlBranchFilter.SelectedValue), Convert.ToInt32(ddlCustomer.SelectedValue)).ToList();
                var auditID = contractorDataList.Where(x => x.ID == ddlContractorID).ToList();
                //hdAuditorID.DataSource = auditID;
                //hdAuditorID.DataTextField = "AuditID";
                hdAuditorID.Text = auditID[0].AuditID.ToString();
            }
        }

        protected void BindYear(int BranchID, int CustID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var contractorYearList = entities.SP_RLCS_Vendor_GetContractorsList(BranchID, CustID).ToList();
                    //ddlContractorID = Convert.ToInt32(ddlContractor.SelectedValue);
                    var contractorStartYear = contractorYearList.GroupBy(x =>x.ContractStartYear).Select(y => y.FirstOrDefault());//contractorYearList.Where(x => x.ID == ddlContractorID).ToList();
                    var contractorEndYear = contractorYearList.GroupBy(x => x.ContractEndYear).Select(y => y.FirstOrDefault());

                    var contractorYears  = contractorStartYear.Concat(contractorEndYear);

                    var years = new List<int>();

                    foreach (var item in contractorYears)
                    {
                        years.Add(Convert.ToInt32(item.ContractEndYear));
                        years.Add(Convert.ToInt32(item.ContractStartYear));
                    }
                    var finalYearList = years.Distinct().ToList();
                    finalYearList = finalYearList.OrderByDescending(q => q).ToList();
                    ddlYear.DataSource = finalYearList;
                    ddlYear.DataBind();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlYear.SelectedValue != "")
                {
                    var yearLastTwo = ddlYear.SelectedValue.Substring(2, 2);
                    if (ddlPeriod.SelectedValue != "")
                    {
                        string FirstMonth = ddlPeriod.SelectedValue.Substring(0, 3);
                        string SecondMonth = ddlPeriod.SelectedValue.Substring(ddlPeriod.SelectedValue.Length - 3);
                        SchedulePeriod = FirstMonth + " " + yearLastTwo + " " + "-" + " " + SecondMonth + " " + yearLastTwo;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

       

        [WebMethod]
        public static string GetCustomerFrequency(ActAssignmentDetails AssignmentObj)
        {

            string Frequency = "";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int ID = Convert.ToInt32(AssignmentObj.CustomerID);

                    if (ID > 0)
                    {
                        var query = (from row in entities.RLCS_VendorAuditInstance
                                     where row.AVACOM_CustomerID == ID
                                     orderby row.ID descending
                                     select row).FirstOrDefault();

                        Frequency = query.CC_Frequency;
                    }

                    return serializer.Serialize(Frequency);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }

        }


        public class VendorUnassignCheckListDetails
        {
            public long Id { get; set; }
            public string ChecklistID { get; set; }
            public string ActID { get; set; }
            public string NatureOfCompliance { get; set; }
            public string AM_ActName { get; set; }
            public string AM_ActGroup { get; set; }
            public string StateID { get; set; }
            public string StateName { get; set; }
            public DateTime CreatedOn { get; set; }
            public bool isDeleted { get; set; }
            public string SM_Status { get; set; }
            public string Risk { get; set; }
            public string VendorName { get; set; }
            public int VendorID { get; set; }
            public int AuditID { get; set; }
            public string Period { get; set; }
            public string Branch { get; set; }

        }



        public class VendorAssignCheckListDetails
        {
            public int Id { get; set; }
            public string ActID { get; set; }
            public string AM_ActName { get; set; }
            public int VendorID { get; set; }
            public Nullable<int> CreatedBy { get; set; }
            public Nullable<System.DateTime> CreatedOn { get; set; }
            public string NatureOfCompliance { get; set; }
            public string VendorName { get; set; }
            public string AM_StateID { get; set; }
            public string StateName { get; set; }
            public string AM_ActGroup { get; set; }
            public string Risk { get; set; }
            public string SchedulePeriod { get; set; }
            public Nullable<long> AuditID { get; set; }
            public Nullable<int> CheckListMasterId { get; set; }
            public string BranchName { get; set; }

            public Nullable<long> ScheduleONID { get; set; }
        }

        public class ActAssignmentDetails
        {
            public string AuditID { get; set; }
            public string ScheduledOnID { get; set; }
            public string CheckListID { get; set; }

            public string DeleteVendorID { get; set; }
            public string SchedulePeriod { get; set; }

            public string ID { get; set; }
            public string CustomerID { get; set; }
            public string EntityID { get; set; }
            public string StateID { get; set; }
            public List<string> Location { get; set; }
            public List<string> Branches { get; set; }
            public string Type { get; set; }
            public string ActID { get; set; }
            public List<string> VendorIDs { get; set; }
            public List<string> Years { get; set; }
            public List<string> Periods { get; set; }
            public List<GetActsDetails> AssignCheckList { get; set; }
            public bool isclientbasedchecklist { get; set; }
            public string Frequency { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
        }

        public class GetActsDetails
        {
            public string ActID { get; set; }
            public string AuditID { get; set; }
            public int ChecklistMasterID { get; set; }
            public string ChecklistID { get; set; }
            public string SchedulePeriod { get; set; }
            public string VendorID { get; set; }
            public List<string> VendorIDs { get; set; }
            public List<string> Years { get; set; }
            public List<string> Periods { get; set; }
            public string FrequencyID { get; set; }
            public List<string> LocationID { get; set; }
            //public string Frequency { get; set; }
            //public string StartDate { get; set; }
            //public string EndDate { get; set; }


        }

        
    }

}