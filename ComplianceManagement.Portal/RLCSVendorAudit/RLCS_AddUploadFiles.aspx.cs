﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nelibur.ObjectMapper;
using System.Collections;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.IO;
using OfficeOpenXml;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web.Script.Serialization;
using System.Configuration;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_AddUploadFiles : System.Web.UI.Page
    {

        
        static long ActID = 0;
        //static long ScheduleOnId = 0;
        static string version = null;
        static long FileID = -1;
        static long ContractorID = 0;
        //static string State, location, Branch, StateOrCentral, StateFilter, YearFilter, PeriodFilter;
        static string State, location, Branch, StateOrCentral, StateFilter;

        protected void Page_Load(object sender, EventArgs e)
        {

            ActID = Convert.ToInt64(Request.QueryString["ActID"]);
            //ScheduleOnId = Convert.ToInt64(Request.QueryString["ScheduleOnID"]);
            ContractorID = Convert.ToInt64(Request.QueryString["ContractorID"]);
            State = Request.QueryString["State"];
            location = Request.QueryString["location"];
            Branch = Request.QueryString["Branch"];
            StateOrCentral = Request.QueryString["StateOrCentral"];
            StateFilter = Request.QueryString["StateFilter"];
            //YearFilter = Request.QueryString["YearFilter"];
            //PeriodFilter = Request.QueryString["PeriodFilter"];
            if (!IsPostBack)
            {
                //ActID = Convert.ToInt64(Request.QueryString["ActID"]);
               //docUpload.Attributes.Add("onclick", "document.getElementById('" + docUpload.ClientID + "').click();");
               
                BindGrid();
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            if (ViewState["datatable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["datatable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    drCurrentRow = dtCurrentTable.NewRow();
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["datatable"] = dtCurrentTable;

                    for (int i = 0; i < dtCurrentTable.Rows.Count-1; i++)
                    {
                        FileUpload fileUpload = (FileUpload)grdDocument.Rows[i].Cells[0].FindControl("checkListFileUpload");
                        TextBox txtValidDate = (TextBox)grdDocument.Rows[i].Cells[1].FindControl("txtValidity");

                        dtCurrentTable.Rows[i]["Uploadfile"] = fileUpload.FileBytes;
                        dtCurrentTable.Rows[i]["Validtill"] = txtValidDate.Text;
                    }

                    grdDocument.DataSource = dtCurrentTable;
                    grdDocument.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is Null");
            }

            setData();
        }

        private void setData()
        {
            int rowIndex = 0;
            if (ViewState["datatable"] != null)
            {
                DataTable dt = (DataTable)ViewState["datatable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FileUpload fileUpload = (FileUpload)grdDocument.Rows[i].Cells[0].FindControl("checkListFileUpload");
                        TextBox txtValidDate = (TextBox)grdDocument.Rows[i].Cells[1].FindControl("txtValidity");
                        if (i < dt.Rows.Count - 1)
                        {
                            //fileUpload = (FileUpload)dt.Rows[i]["Uploadfile"] as FileUpload;
                            //txtValidDate.Text = dt.Rows[i]["Validtill"].ToString();
                        }
                    }
                    rowIndex++;
                }
            }
        }

        public void BindGrid()
        {
            try
            {
                ShowFile.Text = "";
                if (ActID > 0 && ContractorID > 0)
                {
                    //long ChecklistID = Convert.ToInt64(Request.QueryString["CHID"].ToString());
                    //long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    long vrole = RoleManagement.GetByCode(AuthenticationHelper.Role).ID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentDataByUserID(ActID, ContractorID);
                        if (lstChecklistDetails.Count > 0)
                        {
                            ViewState["DocumentListCount"] = lstChecklistDetails.Count;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                            //chkDocument.Enabled = false;
                        }
                        else
                        {
                            ViewState["DocumentListCount"] = 0;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                            //chkDocument.Enabled = true;
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnSubmitDocument_Click(object sender, EventArgs e)
        {
            try
            {
                if (docUpload.HasFile && (!string.IsNullOrEmpty(txtDocumentValidity.Text)))
                {
                    if (btnSubmitDocument.Text == "Save")
                    {
                        version = "v1.0";
                    }
                    else
                    {
                        version = "v2.0";
                    }
                    
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    List<RLCSVendorFileData> files = new List<RLCSVendorFileData>();
                    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                    HttpFileCollection fileCollection = Request.Files;
                    var vrole = RoleManagement.GetByCode(AuthenticationHelper.Role).ID;
                    bool blankfileCount = true;
                    if (fileCollection.Count > 0)
                    {
                        string directoryPath = null;
                        
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\RLCSVendorDocuments\\" + AuthenticationHelper.UserID + "\\" + ActID.ToString() + "\\" + version; //AuditID.ToString() ActID+ "\\" + ChecklistID.ToString() + "\\" + ScheduleOnID + "\\" + version;
                        }
                        else
                        {
                            directoryPath = Server.MapPath("~/RLCSVendorDocuments/" + AuthenticationHelper.UserID + "/" + ActID.ToString() + "/"  + version); //AuditID.ToString() + "/" + ChecklistID.ToString() + "/" + ScheduleOnID + "/" + version);
                        }
                        DocumentManagement.CreateDirectory(directoryPath);
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection[i];

                            string[] keys = fileCollection.Keys[i].Split('$');
                            String fileName = "";

                            fileName = uploadfile.FileName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));

                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                            if (uploadfile.ContentLength > 0)
                            {
                                string filepathvalue = string.Empty;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                    filepathvalue = vale.Replace(@"\", "/");
                                }
                                else
                                {
                                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                }
                                RLCSVendorFileData file = new RLCSVendorFileData()
                                {
                                    ID = Convert.ToInt32(FileID != -1 ? FileID : 0),
                                    Name = fileName,
                                    FilePath = filepathvalue,
                                    FileKey = fileKey.ToString(),
                                    Version = version,
                                    CreatedOn = DateTime.Now,
                                    FileSize = uploadfile.ContentLength.ToString(),
                                    CreatedBy = Convert.ToInt32(vrole),
                                    ValidTill = Convert.ToDateTime(txtDocumentValidity.Text)
                                };
                                files.Add(file);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(uploadfile.FileName))
                                    blankfileCount = false;
                            }
                        }
                    }
                    bool flag = false;
                    if (blankfileCount)
                    {
                       
                        flag = RLCSVendorMastersManagement.FileUploadMaster(Convert.ToInt64(ActID), ContractorID, files, list, Filelist, State, location, Branch, StateOrCentral, StateFilter);

                        if (flag)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "File Upload Successfully.";
                            btnSubmitDocument.Text = "Save";
                            txtDocumentValidity.Text = "";
                            FileID = -1;


                            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            //{
                            //    List<int> VendorList = new List<int>();


                            //    long VendorIDforMaster = Convert.ToInt64(ContractorID);
                            //    var AuditObj = (from aduit in entities.RLCS_VendorAuditInstance
                            //                    where aduit.AVACOM_VendorID == VendorIDforMaster
                            //                    select aduit).FirstOrDefault();
                            //    int VendorIDnew = Convert.ToInt32(AuditObj.AVACOM_VendorID);
                            //    VendorList.Add(VendorIDnew);


                            //    List<long> obj1 = (from row in entities.RLCS_VendorAuditStepMapping
                            //                       where row.VendorAuditScheduleOnID == SchedulOnID
                            //                       && row.AuditID == objUpdateScheduleOn.AuditID
                            //                       && row.IsActive == true
                            //                       select row.CheckListID).ToList();
                            //    if (obj1.Count > 0)
                            //    {
                            //        foreach (var itemVendorID in VendorList)
                            //        {
                            //            var objmasterfiles = (from row in entities.RLCSMasterFileDatas
                            //                                  where obj1.Contains(row.ChecklistID) &&
                            //                                   row.ContractorID == itemVendorID
                            //                                  //&& row.ScheduleOnID == SchedulOnID
                            //                                  select row).ToList();
                            //            foreach (var item in objmasterfiles)
                            //            {
                            //                RLCSVendorFileDataMapping rlcsVendorFiledata = (from row in entities.RLCSVendorFileDataMappings
                            //                                                                where row.ScheduledOnID == SchedulOnID
                            //                                                                && row.ChecklistID == item.ChecklistID
                            //                                                                && row.FileID == item.FileID
                            //                                                                select row).FirstOrDefault();
                            //                if (rlcsVendorFiledata == null)
                            //                {
                            //                    RLCSVendorFileDataMapping rlcsVendorFiledataMapping = new RLCSVendorFileDataMapping();
                            //                    rlcsVendorFiledataMapping.ChecklistID = item.ChecklistID;
                            //                    rlcsVendorFiledataMapping.FileID = item.FileID;
                            //                    rlcsVendorFiledataMapping.FileType = 1;
                            //                    rlcsVendorFiledataMapping.ScheduledOnID = SchedulOnID;
                            //                    entities.RLCSVendorFileDataMappings.Add(rlcsVendorFiledataMapping);
                            //                    entities.SaveChanges();
                            //                }
                            //            }
                            //        }
                            //    }
                            //}

                        }
                        else
                        {
                            
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                           
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    }

                    BindGrid();
                }
                else
                {
                    if (docUpload.HasFile == false)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please attached at least one file.";
                    }
                    if (string.IsNullOrEmpty(txtDocumentValidity.Text))
                    {
                        validDateNull.IsValid = false;
                        validDateNull.ErrorMessage = "Please enter validity.";
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }
        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit Document"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            
                            var lstChecklistDetails = RLCSVendorMastersManagement.getDocumentDataByFileID(FileID);
                            string savePath = lstChecklistDetails[0].FilePath  + Server.HtmlDecode(lstChecklistDetails[0].FileKey);
                            txtDocumentValidity.Text = String.Format("{0:dd/MMM/yyyy}", lstChecklistDetails[0].ValidTill);// lstChecklistDetails[0].ValidTill.ToString("dd/MM/yyyy");
                           // docUpload.HasFile = lstChecklistDetails[0].FileKey;
                            ShowFile.Text = lstChecklistDetails[0].Name;
                            btnSubmitDocument.Text = "Update";
                        }
                    }
                }


                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (RLCSVendorMastersManagement.DeleteRLCSVendorFile(FileID, Convert.ToInt32(AuthenticationHelper.UserID)))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            BindGrid();
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=VendorAudit.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                        }
                        //BindGrid();
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {

                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    string CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    //lblMessage.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        protected void grdDocument_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdDocument.EditIndex = e.NewEditIndex;

            BindGrid();

        }

        protected void grdDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDocument.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ShowFile.Text = "";
            docUpload.Dispose();
            txtDocumentValidity.Text = "";
            btnSubmitDocument.Text = "Save";
            FileID = -1;
        }
    } 
}