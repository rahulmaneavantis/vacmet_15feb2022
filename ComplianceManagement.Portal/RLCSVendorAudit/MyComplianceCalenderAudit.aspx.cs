﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class MyComplianceCalenderAudit : System.Web.UI.Page
    {
        public static string CalendarDate, AuditStatus;

        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["AuditStatus"] != "undefined")
                {
                    AuditStatus = Convert.ToString(Request.QueryString["AuditStatus"]);
                }
                if (Request.QueryString["type"] != "undefined")
                {
                    try
                    {
                        string type = Convert.ToString(Request.QueryString["type"]);
                        GetCalender(type);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "fcal", "fcal('" + CalendarTodayOrNextDate.ToString("yyyy-MM-dd") + "');", true);
                    }
                    catch (Exception ex) { }
                }
            }
        }

        public DataTable fetchData(int customerid, int userid, string Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DataTable statutorydatatable = new DataTable();
                DataTable Internaldatatable = new DataTable();
                DataTable mgmtstatutorydatatable = new DataTable();
                DataTable mgmtInternaldatatable = new DataTable();

                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }


                #region Statutory
                entities.Database.CommandTimeout = 180;
                string role = AuthenticationHelper.Role;
                //string Status = Convert.ToInt32(AuditStatus);
                //var mgmtQueryStatutory = (from row in entities.SP_RLCS_ComplianceInstanceTransactionViewCustomerWiseManagement(796, 7613, "7613")
                //                          select row).ToList();
                var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(customerid, userid, role)
                                        select row).ToList();
                if (mgmtQueryStatutory.Count > 0)
                {

                    if (!string.IsNullOrEmpty(Request.QueryString["AuditStatus"]))
                    {
                        if (Request.QueryString["AuditStatus"] != "undefined" && Request.QueryString["AuditStatus"] != "null")
                        {
                            if (Request.QueryString["AuditStatus"].ToUpper().Trim() == "OPEN")
                            {
                                DateTime EndMonthDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                         DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

                                mgmtQueryStatutory = mgmtQueryStatutory.Where(a => (a.AuditStatusID == 1 || a.AuditStatusID == 2) && a.StartDate <= EndMonthDate).ToList();
                            }

                            //mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.StartDate).Select(entity => entity.FirstOrDefault()).ToList();

                        }
                    }


                    var Statutory = (from row in mgmtQueryStatutory
                                     select new
                                     {
                                         ScheduledOn = row.StartDate,
                                         ComplianceStatusID = row.AuditStatusID,
                                         RoleID = row.AuditID,
                                         ScheduledOnID = row.AuditScheduleOnID,
                                         CustomerBranchID = row.BranchID,
                                         EndDate=row.EndDate
                                     }).Distinct().ToList();


                    mgmtstatutorydatatable = Statutory.ToDataTable();
                }
                #endregion

                DataTable newtable = new DataTable();

                //management
                if (mgmtstatutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtstatutorydatatable);
                }

                return newtable;
            }
        }
        //public void GetCalender(string type)
        //{
        //    DateTime dt1 = DateTime.Now.Date;

        //    string month = dt1.Month.ToString();
        //    if (Convert.ToInt32(month) < 10)
        //    {
        //        month = "0" + month;
        //    }

        //    string year = dt1.Year.ToString();
        //    CalendarDate = year + "-" + month;
        //    List<Compliancecalendar> events = new List<Compliancecalendar>();
        //    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //    int userid = AuthenticationHelper.UserID;
        //    DataTable dt = fetchData(customerid, userid, type);
        //    var list = (from r in dt.AsEnumerable()
        //                select r["ScheduledOn"]).Distinct().ToList();

        //    var datetodayornext = (from r in dt.AsEnumerable()
        //                           orderby r.Field<DateTime>("ScheduledOn")
        //                           where r.Field<DateTime>("ScheduledOn") >= DateTime.Now.AddDays(-1)
        //                           select r["ScheduledOn"]).Distinct().FirstOrDefault();

        //    if (datetodayornext != null)
        //        CalendarTodayOrNextDate = Convert.ToDateTime(datetodayornext);
        //    else
        //        CalendarTodayOrNextDate = DateTime.Now;

        //    CalenderDateString = "";
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        Compliancecalendar _Event = new Compliancecalendar();
        //        string clor = "";
        //        int assignedCount = 0;
        //        DateTime date = Convert.ToDateTime(list[i]);
        //        //DateTime date = DateTime.Today.AddDays(7);
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (string.IsNullOrEmpty(AuditStatus))
        //            {
        //                var Assigned = (from dts in dt.AsEnumerable()
        //                                where dts.Field<DateTime>("ScheduledOn").Year == date.Year
        //                                && dts.Field<DateTime>("ScheduledOn").Month == date.Month
        //                                && dts.Field<DateTime>("ScheduledOn").Day == date.Day
        //                                select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

        //                var ClosedDelaycount = (from dts in dt.AsEnumerable()
        //                                        where //dts.Field<DateTime>("ScheduledOn").Year == date.Year
        //                                              //&& dts.Field<DateTime>("ScheduledOn").Month == date.Month
        //                                              // && dts.Field<DateTime>("ScheduledOn").Day == date.Day
        //                                      dts.Field<int>("ComplianceStatusID") == 5
        //                                        select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

        //                var Completed = (from dts in dt.AsEnumerable()
        //                                 where //dts.Field<DateTime>("ScheduledOn").Year == date.Year
        //                                       //&& dts.Field<DateTime>("ScheduledOn").Month == date.Month
        //                                       //&& dts.Field<DateTime>("ScheduledOn").Day == date.Day
        //                                 dts.Field<int>("ComplianceStatusID") == 4
        //                                 //|| dts.Field<int>("ComplianceStatusID") == 5)
        //                                 select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

        //                assignedCount = Assigned.Count;
        //                if (date > DateTime.Now)
        //                {
        //                    clor = "";
        //                    //if (Assigned.Count > Completed.Count)
        //                    //{
        //                    //    clor = "upcoming";
        //                    //}
        //                    // else 
        //                    if ((Assigned.Count) == (Completed.Count))
        //                    {
        //                        if (ClosedDelaycount.Count > 0)
        //                        {
        //                            clor = "delayed";
        //                        }
        //                        else
        //                        {
        //                            clor = "complete";
        //                        }
        //                    }
        //                }
        //                else
        //                {

        //                    if (Assigned.Count > Completed.Count)
        //                    {
        //                        clor = "overdue";
        //                    }
        //                    else if ((Assigned.Count) == (Completed.Count))
        //                    {
        //                        if (ClosedDelaycount.Count > 0)
        //                        {
        //                            clor = "delayed";
        //                        }
        //                        else
        //                        {
        //                            clor = "complete";
        //                        }
        //                    }
        //                }
        //            }


        //        }

        //        if (!String.IsNullOrEmpty(AuditStatus))
        //        {
        //            if (clor.Trim().ToUpper() == AuditStatus.Trim().ToUpper())
        //            {
        //                DateTime dtdate = Convert.ToDateTime(list[i]);
        //                var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        //                CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";
        //            }
        //        }
        //        else
        //        {
        //            DateTime dtdate = Convert.ToDateTime(list[i]);
        //            var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        //            CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";

        //        }
        //    }

        //    CalenderDateString = CalenderDateString.Trim(',');
        //}
        public void GetCalender(string type)
        {
            DateTime dt1 = DateTime.Now.Date;

            string month = dt1.Month.ToString();
            if (Convert.ToInt32(month) < 10)
            {
                month = "0" + month;
            }

            string year = dt1.Year.ToString();
            CalendarDate = year + "-" + month;
            List<Compliancecalendar> events = new List<Compliancecalendar>();
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;
            DataTable dt = fetchData(customerid, userid, type);
            var list = (from r in dt.AsEnumerable()
                        select r["ScheduledOn"]).ToList();

            var datetodayornext = (from r in dt.AsEnumerable()
                                   orderby r.Field<DateTime>("ScheduledOn")
                                   where r.Field<DateTime>("ScheduledOn") >= DateTime.Now.AddDays(-1)
                                   select r["ScheduledOn"]).Distinct().FirstOrDefault();

            if (datetodayornext != null)
                CalendarTodayOrNextDate = Convert.ToDateTime(datetodayornext);
            else
                CalendarTodayOrNextDate = DateTime.Now;

            CalenderDateString = "";
            for (int i = 0; i < list.Count; i++)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                string clor = "";
                int assignedCount = 0;
                DateTime date = Convert.ToDateTime(list[i]);
                //DateTime date = DateTime.Today.AddDays(7);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(AuditStatus))
                    {
                        if (AuditStatus.Trim().ToUpper() == "OPEN")
                        {
                            DateTime SD = Convert.ToDateTime(list[i]);
                            DateTime EndMonthDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                                DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                            // auditDetail.Where(e => e.StartDate <= EndMonthDate);
                            var Assigned = (from dts in dt.AsEnumerable()
                                            where dts.Field<DateTime>("ScheduledOn") <= EndMonthDate
                                            && (dts.Field<int>("ComplianceStatusID") == 1 || dts.Field<int>("ComplianceStatusID") == 2)
                                            && dts.Field<DateTime>("ScheduledOn") == SD
                                            select dts.Field<long>("ScheduledOnID")).ToList();


                           //var Assigned = auditDetail.Where(e => e.StartDate <= EndMonthDate && e.AuditStatusID == 1).ToList();
                           //var Assigned = (from dts in dt.AsEnumerable()
                           //                 where  dts.Field<int>("ComplianceStatusID") ==1
                           //                 select dts.Field<long>("ScheduledOnID")).Distinct().ToList();
                            assignedCount = Assigned.Count;
                        }
                        else if (AuditStatus.Trim().ToUpper() == "DELAYED")
                        {
                            var ClosedDelaycount = (from dts in dt.AsEnumerable()
                                                    where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                                          && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                                           && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                                  && dts.Field<int>("ComplianceStatusID") == 5
                                                    select dts.Field<long>("ScheduledOnID")).Distinct().ToList();
                            assignedCount = ClosedDelaycount.Count;
                        }
                        else if (AuditStatus.Trim().ToUpper() == "COMPLETE")
                        {
                            var Completed = (from dts in dt.AsEnumerable()
                                             where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                                   && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                                   && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                            && dts.Field<int>("ComplianceStatusID") == 4
                                             select dts.Field<long>("ScheduledOnID")).Distinct().ToList();
                            assignedCount = Completed.Count;
                        }
                        else if (AuditStatus.Trim().ToUpper() == "OVERDUE")
                        {
                            var Assigned = (from dts in dt.AsEnumerable()
                                            where (dts.Field<int>("ComplianceStatusID") == 1 || dts.Field<int>("ComplianceStatusID") == 2)
                                            &&  dts.Field<DateTime>("ScheduledOn") <= DateTime.Now 
                                            //&& dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                                   && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                                   && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                            select dts.Field<long>("ScheduledOnID")).Distinct().ToList();
                            assignedCount = Assigned.Count;
                        }
                        clor = AuditStatus.Trim().ToLower();
                        if (assignedCount >0)
                        { 
                        DateTime dtdate = Convert.ToDateTime(list[i]);
                        var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                            string chkAuditStatus = "undefined";
                            if (Request.QueryString["AuditStatus"] != "undefined")
                            {
                                chkAuditStatus = Convert.ToString(Request.QueryString["AuditStatus"]);
                            }

                            CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "','" + chkAuditStatus + "');\"},";
                        }
                    }
                    else
                    {
                        var Assigned = (from dts in dt.AsEnumerable()
                                        where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                        && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                        && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                        select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                        var ClosedDelaycount = (from dts in dt.AsEnumerable()
                                                where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                                      && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                                       && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                              && dts.Field<int>("ComplianceStatusID") == 5
                                                select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                        var Completed = (from dts in dt.AsEnumerable()
                                         where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                               && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                               && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                        && dts.Field<int>("ComplianceStatusID") == 4
                                         //|| dts.Field<int>("ComplianceStatusID") == 5)
                                         select dts.Field<long>("ScheduledOnID")).Distinct().ToList();
                        assignedCount = Assigned.Count;
                        if (date > DateTime.Now)
                        {
                            clor = "";
                            //if (Assigned.Count > Completed.Count)
                            //{
                            //    clor = "upcoming";
                            //}
                            // else 
                            if ((Assigned.Count) == (Completed.Count))
                            {
                                if (ClosedDelaycount.Count > 0)
                                {
                                    clor = "delayed";
                                }
                                else
                                {
                                    clor = "complete";
                                }
                            }
                        }
                        else
                        {

                            if (Assigned.Count > Completed.Count)
                            {
                                clor = "overdue";
                            }
                            else if ((Assigned.Count) == (Completed.Count))
                            {
                                if (ClosedDelaycount.Count > 0)
                                {
                                    clor = "delayed";
                                }
                                else
                                {
                                    clor = "complete";
                                }
                            }
                        }

                        DateTime dtdate = Convert.ToDateTime(list[i]);
                        var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                        CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";


                    }



                }
            }

            CalenderDateString = CalenderDateString.Trim(',');
        }
    }
}