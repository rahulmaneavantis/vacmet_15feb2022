﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSAuditChecklistSchedule_OLD : System.Web.UI.Page
    {
        protected static string user_Roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    user_Roles = AuthenticationHelper.Role;
                    BindCustomerFilter();
                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected void ddlVendorListBase_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {           
            var role = string.Empty;
            long customerID = -1;
            if (user_Roles.Contains("HVADM"))
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                    customerID = Convert.ToInt64(ddlFilterCustomer.SelectedValue);

                role = user_Roles.Trim();
            }
            else
            {
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                CustomerList1.Visible = false;
                customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                if (ddlFilterCustomer.Items.Count > 1)
                    ddlFilterCustomer.SelectedValue = Convert.ToString(customerID);
            }
            if (customerID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetVendorAuditAssignedLocationBranches((int)customerID, AuthenticationHelper.UserID, role)).ToList();
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                            BindLocationFilter(assignedbranchIDs, (int)customerID);
                            upDivLocation_Load(null, null);
                        }
                    }
                }
            }
        }       
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdChecklistDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }    
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                bindVendorList();
               
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }                    
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdChecklistDetails.PageIndex = chkSelectedPage - 1;
            grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();            
            //bindPageNumber();
            ShowGridDetail();
        }
        private void BindCustomerFilter()
        {
            var role = string.Empty;
            if (user_Roles.Contains("HVADM"))
            {
                role = user_Roles.Trim();
            }
            else
            {
                var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
            }
            var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);
            ddlFilterCustomer.DataTextField = "Name";
            ddlFilterCustomer.DataValueField = "ID";
            ddlFilterCustomer.DataSource = lstCustomers;
            ddlFilterCustomer.DataBind();
        }
        private void BindLocationFilter(List<int> assignedBranchList, int Customerid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Customerid);
                    tvFilterLocation.Nodes.Clear();
                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                    //tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindVendorList()
        {
            try
            {
                int branchID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.GetAllVendor(branchID);
                    ddlVendorListBase.DataTextField = "Name";
                    ddlVendorListBase.DataValueField = "ID";
                    ddlVendorListBase.DataSource = data;
                    ddlVendorListBase.DataBind();
                    ddlVendorListBase.Items.Insert(0, new ListItem("Select Vendor", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindGrid()
        {
            try
            {
                int branchID = -1;
                long vendorid = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlVendorListBase.SelectedValue))
                {
                    vendorid = Convert.ToInt32(ddlVendorListBase.SelectedValue);
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Session["TotalRows"] = null;
                    if (branchID != -1 && vendorid != -1)
                    {
                        var lstChecklistDetails = entities.SP_RLCS_GetChecklistDetail(vendorid, branchID).ToList();

                        var TempQueryAssignInstance = (from RVAI in entities.RLCS_VendorAuditInstance
                                                       join row1 in entities.RLCS_VendorAuditAssignment
                                                        on RVAI.ID equals row1.AuditId
                                                       join RVASM in entities.RLCS_VendorAuditStepMapping
                                                       on RVAI.ID equals RVASM.AuditID
                                                       where RVAI.AVACOM_BranchID == branchID
                                                        && RVAI.IsActive == false
                                                       select RVASM.CheckListID).ToList();
                        TempQueryAssignInstance = TempQueryAssignInstance.Distinct().ToList();                       
                        lstChecklistDetails.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));

                        
                        if (lstChecklistDetails.Count > 0)
                        {
                            grdChecklistDetails.DataSource = lstChecklistDetails;
                            grdChecklistDetails.DataBind();
                            Session["TotalRows"] = lstChecklistDetails.Count;
                        }
                        else
                        {
                            grdChecklistDetails.DataSource = lstChecklistDetails;
                            grdChecklistDetails.DataBind();
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }                   
                    upDivLocation_Load(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        #region Page Number-Bottom
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.SelectedValue = null;
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion
        
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool SaveFlag = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<Tuple<RLCS_VendorAuditInstance, RLCS_VendorAuditAssignment>> assignments = new List<Tuple<RLCS_VendorAuditInstance, RLCS_VendorAuditAssignment>>();
                    RLCS_VendorAuditInstance instance = new RLCS_VendorAuditInstance();
                    long branchID = -1;
                    long vendorid = -1;
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlVendorListBase.SelectedValue))
                    {
                        vendorid = Convert.ToInt32(ddlVendorListBase.SelectedValue);
                    }
                    instance = (from row in entities.RLCS_VendorAuditInstance
                                where row.AVACOM_VendorID == vendorid
                                && row.AVACOM_BranchID == branchID
                                select row).FirstOrDefault();
                    if (instance != null)
                    {
                        #region
                        var perid = instance.AVACOM_VendorID;
                        var revid = instance.AVACOM_AuditorID;
                        var auid = instance.AVACOM_AuditorID;
                        var lid = instance.AVACOM_BranchID;
                        if (perid != null && revid != null)
                        {
                            if (perid != -1 && revid != -1)
                            {
                                #region                                                                                                                                                    
                                RLCS_VendorAuditAssignment TempAssP = new RLCS_VendorAuditAssignment();
                                TempAssP.AuditId = Convert.ToInt64(auid);
                                TempAssP.RoleID = 3;
                                TempAssP.UserID = Convert.ToInt32(perid);
                                TempAssP.CreatedOn = DateTime.Now;
                                TempAssP.CustomerBranchID = Convert.ToInt32(lid);
                                assignments.Add(new Tuple<RLCS_VendorAuditInstance, RLCS_VendorAuditAssignment>(instance, TempAssP));

                                RLCS_VendorAuditAssignment TempAssR = new RLCS_VendorAuditAssignment();
                                TempAssR.AuditId = Convert.ToInt64(auid);
                                TempAssR.RoleID = 4;
                                TempAssR.UserID = Convert.ToInt32(revid);
                                TempAssR.CreatedOn = DateTime.Now;
                                TempAssR.CustomerBranchID = Convert.ToInt32(lid);
                                assignments.Add(new Tuple<RLCS_VendorAuditInstance, RLCS_VendorAuditAssignment>(instance, TempAssR));

                                var lstChecklistDetails = entities.SP_RLCS_GetChecklistDetail(vendorid, branchID).ToList();
                                var checklistIdlist = lstChecklistDetails.Select(entry => entry.ID).ToList();

                                RLCS_VendorScheduler.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, checklistIdlist.ToList(), branchID,-1);
                                #endregion
                            }
                        }
                        upDivLocation_Load(null, null);
                        #endregion
                    }
                    cvDuplicateEntry.IsValid = false;
                    if (SaveFlag)
                    {
                        cvDuplicateEntry.ErrorMessage = "Audit Save Sucessfully.";
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "Sorry, something went wrong. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}