﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSVendorAuditReassignAudit.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSVendorAuditReassignAudit" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">

    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../assets/fSelect.css" rel="stylesheet" />
    <link href="../../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>
    <script src="../assets/fSelect.js"></script>

    <script type="text/javascript">
        $(document).ready(function (e) {
            var QuryStringValues = getUrlVars();
            BindAuditDetailsTable(QuryStringValues);

            $("#btnReassign").on("click", function (e) {
                var auditdetails = {
                    AuditorID: $("#ddlAuditor").val(),
                    ScheduledOnID: $("#ddlAuditor").attr("scheduleid"),
                    AuditID: ""
                }

                $.ajax({
                    type: "POST",
                    url: "./RLCSVendorAuditReassignAudit.aspx/ReassignAudit",
                    data: JSON.stringify({ auditdetails }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "1")
                            alert("Audit Reassign to " + $("#ddlAuditor option:selected").text() + "");
                        else
                            alert("something wrong");
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });
            $("#btnBack").on("click", function (e) {
                window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
            });
        });

        function BindAuditDetailsTable(QuryStringValues) {
            var auditdetails = {
                CustomerID: "",
                UserID: QuryStringValues.UserID,
                Role: QuryStringValues.Role,
                Period: "",
                AuditID: QuryStringValues.AID,
                ScheduledOnID: QuryStringValues.AuditScheduleOnID
            }
            $.ajax({
                type: "POST",
                url: "./RLCSVendorAuditReassignAudit.aspx/BindAuditDetailsTable",
                data: JSON.stringify({ auditdetails }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var obj = JSON.parse(data.d);
                    var Row = "<tr><td>1</td><td>" + obj.Location + "</td><td>" + obj.Vendor + "</td><td>" + obj.SPOC + "</td><td>" + obj.StartDate + "</td><td>" + obj.EndDate + "</td><td>" + obj.Period + "</td><td><select id='ddlAuditor' Scheduleid='" + QuryStringValues.AuditScheduleOnID + "' class='form-control'></select></td></tr>";
                    $("#tblBody").append(Row);
                    BindAuditorList();
                },
                failure: function (data) {
                    alert(data);
                }
            });

        }

        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        function BindAuditorList() {
            $.ajax({
                type: "POST",
                url: "./RLCSVendorAuditReassignAudit.aspx/BindAuditorList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Auditor = JSON.parse(data.d);
                    $("#ddlAuditor").append($("<option></option>").val("-1").html("Select Auditor"));
                    $.each(Auditor, function (data, value) {
                        $("#ddlAuditor").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    //$('#ddlAuditor').attr("multiple", "multiple");
                    //$('#ddlAuditor').fSelect({
                    //});
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

      
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 0px"></div>
    <asp:UpdatePanel ID="Upvendorchecklist" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <table id="tblAuditRecord" class="table">
                            <thead>
                                <tr>
                                    <td>Sr. No</td>
                                    <td>Location</td>
                                    <td>Vendor</td>
                                    <td>SPOC</td>
                                    <td>Start Date</td>
                                    <td>End Date</td>
                                    <td>Period</td>
                                    <td>Auditor List</td>
                                </tr>
                            </thead>
                            <tbody id="tblBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-10">
                    &nbsp
                </div>
                <div class="col-lg-2 col-md-2">
                    <button id="btnReassign" class="btn btn-primary pull-leff" type="button">Reassign</button>
                    <button id="btnBack" class="btn btn-primary pull-right" type="button" style="margin-right: 19px;">Back</button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

