﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSUploadChecklist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindActMasterGrid();
                    bindActList();
                    bindStateList();
                    BindGrid();

                    if (ddlCentralOrState.SelectedValue == "Central")
                        ddlStateFilter.Enabled = false;
                    else
                    {
                        bindStateList();
                        ddlStateFilter.Enabled = true;
                    }

                    if (ddlStateFilterMaster.SelectedValue == "Central")
                        ddlStateFilter1.Enabled = false;
                    else
                    {
                        bindStateNewList();
                        ddlStateFilter1.Enabled = true;
                    }
                    bindCustomer();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #region bind StateDetails 
        private void bindStateList()
        {
            try
            {
                var StateList = RLCSVendorMastersManagement.getRLCSddlStateData();
                StateList = StateList.Where(x => !x.SM_Code.Equals("CENTRAL")).ToList();
                ddlStateFilter.DataTextField = "SM_Name";
                ddlStateFilter.DataValueField = "SM_Code";
                ddlStateFilter.DataSource = StateList;
                ddlStateFilter.DataBind();
                ddlStateFilter.Items.Insert(0, new ListItem("Select State", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindStateNewList()
        {
            try
            {
                var StateList = RLCSVendorMastersManagement.getRLCSddlStateData();
                StateList = StateList.Where(x => !x.SM_Code.Equals("CENTRAL")).ToList();

                ddlStateFilter1.DataTextField = "SM_Name";
                ddlStateFilter1.DataValueField = "SM_Code";
                ddlStateFilter1.DataSource = StateList;
                ddlStateFilter1.DataBind();
                ddlStateFilter1.Items.Insert(0, new ListItem("Select State", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region bind act detail 
        private void bindActList()
        {
            try
            {
                string stateorcentral = string.Empty;
                if (!string.IsNullOrEmpty(ddlCentralOrState.SelectedValue))
                {
                    stateorcentral = Convert.ToString(ddlCentralOrState.SelectedItem.Text);
                }
                var Actlist = RLCSVendorMastersManagement.getRLCSActlistData(stateorcentral);
                ddlActFilter.DataTextField = "AM_ActName";
                ddlActFilter.DataValueField = "AM_ActID";
                ddlActFilter.DataSource = Actlist;
                ddlActFilter.DataBind();
                ddlActFilter.Items.Insert(0, new ListItem("Select Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        public void bindCustomer()
        {
            string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
            if (CheckContractCustomer == "True")
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, "HVADM", AuthenticationHelper.CustomerID);
                
                ddlCustomer.DataSource = lstCustomers;
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataBind();
            }
            else
            {
                var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, "HVADM");

                ddlCustomer.DataSource = lstCustomers;
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataBind();
            }
        }

        public void BindGrid()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region new                   
                    var lstChecklistDetails = entities.SP_RLCS_VendorUploadCheckList().ToList();
                    if (ddlCustomer.SelectedValue != "")
                    {
                        lstChecklistDetails = lstChecklistDetails.Where(e => e.CustomerID == Convert.ToInt32(ddlCustomer.SelectedValue)).ToList();
                    }
                    else
                    {
                        string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                        if (CheckContractCustomer == "True")
                        {
                            lstChecklistDetails = lstChecklistDetails.Where(e => e.CustomerID == Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();
                        }
                    }
                    Session["TotalRows"] = null;
                    if (lstChecklistDetails.Count > 0)
                    {
                        string stateorcentral = string.Empty;
                        if (!string.IsNullOrEmpty(ddlCentralOrState.SelectedValue))
                        {
                            stateorcentral = Convert.ToString(ddlCentralOrState.SelectedItem.Text);
                            if (stateorcentral.ToUpper().Trim() == "Central".ToUpper().Trim())
                            {
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.SM_Name.ToUpper().Trim() == stateorcentral.ToUpper().Trim()).ToList();
                            }
                            else
                            {
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.SM_Name.ToUpper().Trim() != "Central".ToUpper().Trim()).ToList();
                            }
                        }
                        string ActId = string.Empty;
                        if (!string.IsNullOrEmpty(ddlActFilter.SelectedValue))
                        {
                            if (ddlActFilter.SelectedValue != "-1")
                            {
                                ActId = Convert.ToString(ddlActFilter.SelectedValue);
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.ActID == ActId).ToList();
                            }
                        }

                        string StateID = string.Empty;
                        if (!string.IsNullOrEmpty(ddlStateFilter.SelectedValue))
                        {
                            if (ddlStateFilter.SelectedValue != "-1")
                            {
                                StateID = Convert.ToString(ddlStateFilter.SelectedValue);
                                lstChecklistDetails = lstChecklistDetails.Where(x => x.StateID == StateID).ToList();
                            }
                        }


                        grdChecklistDetails.DataSource = lstChecklistDetails;
                        grdChecklistDetails.DataBind();
                        Session["TotalRows"] = lstChecklistDetails.Count;
                    }
                    else
                    {
                        grdChecklistDetails.DataSource = lstChecklistDetails;
                        grdChecklistDetails.DataBind();
                    }

                    lstChecklistDetails.Clear();
                    lstChecklistDetails = null;
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Page Number-Bottom
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

        //        BindGrid();

        //        int count = Convert.ToInt32(GetTotalPagesCount());
        //        if (count > 0)
        //        {
        //            int gridindex = grdChecklistDetails.PageIndex;
        //            string chkcindition = (gridindex + 1).ToString();
        //            DropDownListPageNo.SelectedValue = (chkcindition).ToString();
        //        }
        //        bindPageNumber();
        //        ShowGridDetail();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        //protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
        //    if (chkSelectedPage > 0)
        //        grdChecklistDetails.PageIndex = chkSelectedPage - 1;

        //    else
        //        grdChecklistDetails.PageIndex = 0;

        //    grdChecklistDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

        //    BindGrid(); ShowGridDetail();
        //}

        //private void bindPageNumber()
        //{
        //    try
        //    {
        //        int count = Convert.ToInt32(GetTotalPagesCount());

        //        if (DropDownListPageNo.Items.Count > 0)
        //        {
        //            DropDownListPageNo.Items.Clear();
        //            DropDownListPageNo.SelectedValue = null;
        //        }

        //        DropDownListPageNo.DataTextField = "ID";
        //        DropDownListPageNo.DataValueField = "ID";

        //        DropDownListPageNo.DataBind();
        //        for (int i = 1; i <= count; i++)
        //        {
        //            string chkPageID = i.ToString();
        //            DropDownListPageNo.Items.Add(chkPageID);
        //        }
        //        if (count > 0)
        //        {
        //            DropDownListPageNo.SelectedValue = ("1").ToString();
        //        }
        //        else if (count == 0)
        //        {
        //            DropDownListPageNo.Items.Add("0");
        //            DropDownListPageNo.SelectedValue = ("0").ToString();
        //        }
        //        ShowGridDetail();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void ShowGridDetail()
        //{
        //    if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
        //    {
        //        var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
        //        var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
        //        var EndRecord = 0;
        //        var TotalRecord = 0;
        //        var TotalValue = PageSize * PageNumber;

        //        TotalRecord = Convert.ToInt32(Session["TotalRows"]);
        //        if (TotalRecord < TotalValue)
        //        {
        //            EndRecord = TotalRecord;
        //        }
        //        else
        //        {
        //            EndRecord = TotalValue;
        //        }
        //        lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
        //        lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
        //        lblTotalRecord.Text = TotalRecord.ToString();
        //    }
        //    else
        //    {
        //        lblStartRecord.Text = "0 ";
        //        lblEndRecord.Text = "0 ";
        //        lblTotalRecord.Text = "0";
        //    }
        //}

        //private int GetTotalPagesCount()
        //{
        //    try
        //    {
        //        TotalRows.Value = "0";
        //        if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
        //        {
        //            TotalRows.Value = Convert.ToString(Session["TotalRows"]);
        //        }

        //        int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

        //        // total page item to be displyed
        //        int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

        //        // remaing no of pages
        //        if (pageItemRemain > 0)// set total No of pages
        //        {
        //            totalPages = totalPages + 1;
        //        }
        //        else
        //        {
        //            totalPages = totalPages + 0;
        //        }
        //        return totalPages;
        //    }
        //    catch (Exception ex)
        //    {
        //        return 0;
        //    }
        //}

        #endregion

        #region Common Methods
        public bool checkDuplicateMultipleDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, string ActIDText, string StateIDText, string NatureOfComplianceText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            string TextToCompareActId = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                            if ((TextToCompareActId.Trim()).Equals(ActIDText.Trim()))
                            {
                                string TextToCompareStateId = xlWorksheet.Cells[i, 3].Text.ToString().Trim();
                                if ((TextToCompareStateId.Trim()).Equals(StateIDText.Trim()))
                                {
                                    string TextToCompareNatureOfcompliance = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                                    if ((TextToCompareNatureOfcompliance.Trim()).Equals(NatureOfComplianceText.Trim()))
                                    {
                                        matchSuccess = true;
                                        i = lastRow; //exit from for Loop
                                    }
                                }
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        #endregion

        protected void lnkSampleFormat_Click(object sender, EventArgs e)
        {
            try
            {
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                string filePath = Server.MapPath("~/RLCSVendorAudit/SampleFormat/RLCSVendorAudit_Upload_Format.xlsx");

                if (!string.IsNullOrEmpty(filePath))
                {
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.AddHeader("Content-Disposition", "attachment;filename=RLCSVendorAudit_Upload_Format.xlsx");
                    byte[] data = req.DownloadData(filePath);
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    response.BinaryWrite(data);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "No Format Available for Download, Please Contact Admin for more detail";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/RLCSVendorAudit/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/RLCSVendorAudit/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {

                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "RLCSVendorCheckList");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RLCSVendorCheckList'.";
                            }

                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        public class customerlist
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;

                int uploadedVendorCount = 0;

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RLCSVendorCheckList"];

                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();

                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string ActId = string.Empty;
                    string ChecklistId = string.Empty;
                    string StateId = string.Empty;
                    string NatureOfCompliance = string.Empty;
                    string Risk = string.Empty;
                    string Description = string.Empty;
                    List<customerlist> lstCustomers = new List<customerlist>();
                    
                    string CheckContractCustomer = RLCSVendorMastersManagement.GetContractMasterStatus(AuthenticationHelper.CustomerID);
                    if (CheckContractCustomer == "True")
                    {
                       var data = RLCSVendorMastersManagement.GetAllClientWise(AuthenticationHelper.UserID, "HVADM", AuthenticationHelper.CustomerID);
                        if (data.Count > 0)
                        {
                            lstCustomers = (from row in data
                                                select new customerlist
                                                {
                                                    ID = row.ID,
                                                    Name = row.Name
                                                }).ToList();
                        }
                        
                    }
                    else
                    {
                        var data = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, "HVADM");
                        if (data.Count > 0)
                        {
                            lstCustomers = (from row in data
                                                select new customerlist
                                                {
                                                    ID = row.ID,
                                                    Name = row.Name
                                                }).ToList();
                        }
                    }

                    //var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, "HVADM");
                    #region Validations

                    for (int i = 2; i <= xlrow2; i++)
                    {

                        #region 1 ActId,ChecklistId,StateID and Nature Of Compliance

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim())
                            && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim())
                            && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim())
                            && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim())
                             && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim())
                            && !String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            ActId = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            ChecklistId = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            StateId = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            NatureOfCompliance = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();

                            bool actExist = RLCSVendorMastersManagement.ExistsActData(ActId);
                            bool stateExist = false;
                            if (StateId.Trim().ToUpper().Equals("CENTRAL"))
                            {
                                stateExist = true;
                            }
                            else
                            {
                                stateExist = RLCSVendorMastersManagement.ExistsStateData(StateId);
                            }

                            if (actExist && stateExist)
                            {
                                bool matchSuccess = false;
                                matchSuccess = checkDuplicateMultipleDataExistExcelSheet(xlWorksheet, i, ActId, StateId, NatureOfCompliance);
                                if (matchSuccess)
                                {
                                    errorMessage.Add("Nature of Compliance at Row - " + i + " Exists Multiple Times For Nature of Compliance in the Uploaded Excel Document");
                                }
                                bool existVendor = RLCSVendorMastersManagement.ExistsChecklistData(ActId, StateId, NatureOfCompliance);
                                if (existVendor)
                                {
                                    errorMessage.Add("Nature of Compliance already exists at row number - " + i + "");
                                }
                            }
                            else
                            {
                                if (!actExist)
                                {
                                    errorMessage.Add("ActID-" + ActId + " not exists at row number - " + i + "");
                                }
                                if (!stateExist)
                                {
                                    errorMessage.Add("State Name not exists at row number - " + i + "");
                                }
                            }
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 1].Text).Trim()))
                        {
                            errorMessage.Add("Required ActID at row number" + i + ".");
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 2].Text).Trim()))
                        {
                            errorMessage.Add("Required CheckListID at row number" + i + ".");
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 3].Text).Trim()))
                        {
                            errorMessage.Add("Required StateID at row number" + i + ".");
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 4].Text).Trim()))
                        {
                            errorMessage.Add("Required Nature of Compliance at row number" + i + ".");
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 5].Text).Trim()))
                        {
                            errorMessage.Add("Required Description at row number" + i + ".");
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 6].Text).Trim()))
                        {
                            errorMessage.Add("Required Risk at row number" + i + ".");
                        }
                        else
                        {
                            if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "HIGH"
                                || xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "MEDIUM"
                                || xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "LOW")
                            {

                            }
                            else
                            {
                                errorMessage.Add("Required Risk at row number" + i + ".");
                            }
                        }
                        if (String.IsNullOrEmpty((xlWorksheet.Cells[i, 10].Text).Trim()))
                        {
                            errorMessage.Add("Required Types of Compliance at row number" + i + ".");
                        }
                        if (!String.IsNullOrEmpty((xlWorksheet.Cells[i, 14].Text).Trim()))
                        {

                             string customername = lstCustomers.Where(f => f.Name == (xlWorksheet.Cells[i, 14].Text).Trim()).Select(n => n.Name).FirstOrDefault();

                            if (customername == null)
                                errorMessage.Add("Customer name is blank or not matching with value at " + i + ".");

                        }
                        #endregion
                    }

                    #endregion

                    string ContractTypeNameFlag = string.Empty;

                    int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);

                    if (errorMessage.Count <= 0)
                    {
                        #region Save                        
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            ActId = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            ChecklistId = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            StateId = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            NatureOfCompliance = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                            Description = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            //Risk = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                            int? customerid = null;
                            if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "HIGH")
                            {
                                Risk = "High";
                            }
                            else if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "MEDIUM")
                            {
                                Risk = "Medium";
                            }
                            else if (xlWorksheet.Cells[i, 6].Text.ToUpper().Trim() == "LOW")
                            {
                                Risk = "Low";
                            }
                            try
                            {
                                customerid = lstCustomers.Where(e => e.Name == (xlWorksheet.Cells[i, 14].Text).Trim()).FirstOrDefault().ID;
                            }
                            catch (Exception e) { }

                            RLCS_tbl_CheckListMaster objchklst = new RLCS_tbl_CheckListMaster()
                            {
                                ActID = ActId,
                                ChecklistID = ChecklistId,
                                StateID = StateId,
                                NatureOfCompliance = NatureOfCompliance,
                                Section = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim(),
                                Checklist_Rule = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim(),
                                Form = Convert.ToString(xlWorksheet.Cells[i, 9].Text).Trim(),
                                TypeOfCompliance = Convert.ToString(xlWorksheet.Cells[i, 10].Text).Trim(),
                                Frequency = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim(),
                                Type = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim(),
                                CreatedOn = DateTime.Now,
                                CreatedBy = UserID,
                                IsDeleted = false,
                                Risk = Risk,
                                Description = Description,
                                Consequences = Convert.ToString(xlWorksheet.Cells[i, 13].Text).Trim(),
                                customerid = customerid,

                            };
                            RLCSVendorMastersManagement.saveCheckListDetail(objchklst);
                            uploadedVendorCount++;
                            saveSuccess = true;
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }

                    if (saveSuccess)
                    {
                        if (uploadedVendorCount > 0)
                        {
                            BindGrid();
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = uploadedVendorCount + " Checklist Details Uploaded Successfully";
                            cvUploadUtilityPage.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdChecklistDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit_Checklist"))
                {
                    long ChecklistID = -1;
                    ChecklistID = Convert.ToInt64(e.CommandArgument);
                    int userID = AuthenticationHelper.UserID;
                    HiddenFieldchechecklistId.Value = "0";
                    HiddenFieldactid.Value = "0";
                    txtNatureOfCompliance.Text = "";
                    txtSection.Text = "";
                    txtCheckListRule.Text = "";
                    txtForm.Text = "";
                    txtTypeOfCompliance.Text = "";
                    txtFrequency.Text = "";
                    txtType.Text = "";
                    lblAct.Text = "";
                    lblState.Text = "";

                    if (ChecklistID > 0)
                    {
                        var getDetail = RLCSVendorMastersManagement.GetChecklistData(ChecklistID);
                        if (getDetail != null)
                        {
                            HiddenFieldchechecklistId.Value = ChecklistID.ToString();
                            HiddenFieldactid.Value = getDetail.ActID.ToString();
                            txtNatureOfCompliance.Text = getDetail.NatureOfCompliance;
                            txtSection.Text = getDetail.Section;
                            txtCheckListRule.Text = getDetail.Checklist_Rule;
                            txtForm.Text = getDetail.Form;
                            txtTypeOfCompliance.Text = getDetail.TypeOfCompliance;
                            txtFrequency.Text = getDetail.Frequency;
                            txtType.Text = getDetail.Type;
                            txtConsequences.Text = getDetail.Consequences;
                            txtDescription.Text = getDetail.Description;
                            if (!string.IsNullOrEmpty(getDetail.Risk))
                            {
                                if (getDetail.Risk.ToUpper().Trim() == "HIGH")
                                {
                                    ddlrisk.SelectedValue = "0";
                                }
                                if (getDetail.Risk.ToUpper().Trim() == "MEDIUM")
                                {
                                    ddlrisk.SelectedValue = "1";
                                }
                                if (getDetail.Risk.ToUpper().Trim() == "LOW")
                                {
                                    ddlrisk.SelectedValue = "2";
                                }
                            }
                            else
                            {
                                ddlrisk.SelectedValue = "-1";
                            }
                            var getactdetails = RLCSVendorMastersManagement.GetActData(getDetail.ActID);
                            var getstatedetails = RLCSVendorMastersManagement.GetStateData(getDetail.StateID);

                            lblAct.Text = getactdetails;
                            lblState.Text = getstatedetails;

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenpopup();", true);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnUpdateChecklist_Click(object sender, EventArgs e)
        {
            string ActID = string.Empty;
            long checklistId = -1;
            if (!string.IsNullOrEmpty(HiddenFieldchechecklistId.Value))
            {
                checklistId = Convert.ToInt64(HiddenFieldchechecklistId.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldactid.Value))
            {
                ActID = Convert.ToString(HiddenFieldactid.Value);
            }

            if (!string.IsNullOrEmpty(lblAct.Text)
                && !string.IsNullOrEmpty(lblAct.Text)
                && !string.IsNullOrEmpty(txtNatureOfCompliance.Text))
            {

                bool existVendor = RLCSVendorMastersManagement.ExistsChecklistDataforUpdate(checklistId, ActID, txtNatureOfCompliance.Text.Trim());
                if (!existVendor)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        RLCS_tbl_CheckListMaster objchechklist = (from row in entities.RLCS_tbl_CheckListMaster
                                                                  where row.Id == checklistId
                                                                  select row).FirstOrDefault();

                        if (objchechklist != null)
                        {
                            objchechklist.NatureOfCompliance = txtNatureOfCompliance.Text;
                            objchechklist.Section = txtSection.Text;
                            objchechklist.Checklist_Rule = txtCheckListRule.Text;
                            objchechklist.Form = txtForm.Text;
                            objchechklist.TypeOfCompliance = txtTypeOfCompliance.Text;
                            objchechklist.Frequency = txtFrequency.Text;
                            objchechklist.Type = txtType.Text;
                            objchechklist.Consequences = txtConsequences.Text;
                            objchechklist.Description = txtDescription.Text;
                            objchechklist.UpdatedOn = DateTime.Now;
                            objchechklist.UpdatedBy = Convert.ToInt64(AuthenticationHelper.UserID);
                            objchechklist.IsDeleted = false;
                            if (ddlrisk.SelectedValue != "-1")
                                objchechklist.Risk = Convert.ToString(ddlrisk.SelectedItem);
                            else
                                objchechklist.Risk = "";

                            entities.SaveChanges();
                        }
                    }
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "Update Successfully.";
                    BindGrid();
                    upUploadAuditChecklist.Update();
                }
                else
                {
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "Nature of Compliance Already exist.";
                }
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCustomer.SelectedIndex > 0)
            {
                BindGrid();
                upUploadAuditChecklist.Update();
            }
        }

        protected void ddlCentralOrState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCentralOrState.SelectedValue == "Central")
            {
                ddlStateFilter.Enabled = false;
                ddlStateFilter.SelectedValue = "-1";
            }
            else
            {
                bindStateList();
                ddlStateFilter.Enabled = true;
            }
            bindActList();
            BindGrid();
            upUploadAuditChecklist.Update();
        }
        protected void ddlStateFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            upUploadAuditChecklist.Update();
        }

        protected void ddlActFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            upUploadAuditChecklist.Update();
        }

        protected void btnActMaster_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenActMaster();", true);
        }
        public void BindActMasterGrid()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region new                   
                    var lstActMasterlistDetails = entities.RLCS_Act_Mapping.Where(x => x.IsDeleted == false).ToList();

                    if (!string.IsNullOrEmpty(ddlStateFilterMaster.SelectedValue))
                    {
                        if (ddlStateFilterMaster.SelectedValue == "Central")
                        {
                            lstActMasterlistDetails = lstActMasterlistDetails.Where(x => x.AM_StateID == "Central").ToList();
                        }
                        else
                        {
                            lstActMasterlistDetails = lstActMasterlistDetails.Where(x => x.AM_StateID != "Central").ToList();
                        }

                        string StateID = string.Empty;
                        if (!string.IsNullOrEmpty(ddlStateFilter1.SelectedValue))
                        {
                            if (ddlStateFilter1.SelectedValue != "-1")
                            {
                                StateID = Convert.ToString(ddlStateFilter1.SelectedValue);
                                lstActMasterlistDetails = lstActMasterlistDetails.Where(x => x.AM_StateID == StateID).ToList();
                            }
                        }
                    }

                    grdActMaster.DataSource = lstActMasterlistDetails;
                    grdActMaster.DataBind();

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlStateFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActMasterGrid();
        }

        protected void ddlStateFilterMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStateFilterMaster.SelectedValue == "Central")
            {
                ddlStateFilter1.Enabled = false;
                ddlStateFilter1.SelectedValue = "-1";
            }
            else
            {
                bindStateNewList();
                ddlStateFilter1.Enabled = true;
            }
            BindActMasterGrid();
        }
    }

}