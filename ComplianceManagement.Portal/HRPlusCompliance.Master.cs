﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSAPISettings;
using System.Net.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class HRPlusCompliance : System.Web.UI.MasterPage
    {

        public HRPlusCompliance()
        {
            response = null;
            _httpClient = HttpClientConfig._httpClient;
        }
        protected string LastLoginDate;
        protected string CustomerName;
        protected string userRole;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;
        HttpResponseMessage response;
        protected bool showMyWorkspace = false;
        protected bool vendorAuditApplicable = false;
        public List<RLCSMenu> LstMenuRLCSItems;
        public List<RLCSChildMenu> LstChildMenuDetails;
        protected List<long> ProductMappingDetails;
        private HttpClient _httpClient { set; get; }
        protected IList<Cust> lstUserAssignedCustomers;
        protected int? serviceproviderid;

        public class Cust
        {
            public int custID { get; internal set; }
            public string custName { get; internal set; }
            public string logoPath { get; internal set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    userRole = AuthenticationHelper.Role;

                    if (Session["AssignedRoles"] != null)
                    {
                        roles = (Session["AssignedRoles"]) as List<int>; ;
                    }
                    else
                    {
                        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                        Session["AssignedRoles"] = roles;
                    }

                    BindProductMapping();
                    BindUserAssignedCustomers();

                    if (!IsPostBack)
                    {
                        hdnProfileID.Value = AuthenticationHelper.ProfileID;
                        hdnAuthKey.Value = AuthenticationHelper.AuthKey;

                        customerid = Convert.ToInt32(AuthenticationHelper.CustomerID); //UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        userid = AuthenticationHelper.UserID;

                        //if (Session["vendorAuditApplicable"] != null)
                        //{
                        //    vendorAuditApplicable = Convert.ToBoolean(Session["vendorAuditApplicable"]);
                        //}
                        //else
                        //{
                        //    vendorAuditApplicable = RLCSManagement.CheckScopeApplicability(customerid, "SOW10");
                        //    Session["vendorAuditApplicable"] = vendorAuditApplicable;
                        //}

                        showMyWorkspace = true;
                        //if (Session["showMyWorkspace"] != null)
                        //{
                        //    showMyWorkspace = Convert.ToBoolean(Session["showMyWorkspace"]);
                        //}
                        //else
                        //{
                        //    showMyWorkspace = RLCS_Master_Management.CheckShowHideInputs(customerid, userid, 95);
                        //    Session["showMyWorkspace"] = showMyWorkspace;
                        //}

                        Page.Header.DataBind();

                        if (Session["LastLoginTime"] != null)
                        {
                            LastLoginDate = Session["LastLoginTime"].ToString();
                        }

                        if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            if (AuthenticationHelper.UserID != -1)
                            {
                                //var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                                //if (cname != null)
                                //{
                                //    CustomerName = cname;
                                //}
                            }
                        }

                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        if (LoggedUser != null)
                        {
                            if (LoggedUser.ImagePath != null)
                            {
                                ProfilePicTop.Src = LoggedUser.ImagePath;
                            }
                            else
                            {
                                ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                            }
                        }
                        else
                        {
                            ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                        }

                        string pageName = this.ContentPlaceHolder1.Page.GetType().FullName;

                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                        if (cname != null)
                        {
                            custName.InnerHtml = cname;
                        }
                    }

                    serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (serviceproviderid == 94)
                    {
                        LstMenuRLCSItems = new List<RLCSMenu>();
                        LstChildMenuDetails = new List<RLCSChildMenu>();
                        HttpResponseMessage response = null;
                        response = _httpClient.GetAsync("api/Masters/GetRLCSAvacomMenuDetails?Role=" + userRole + "&User=" + AuthenticationHelper.ProfileID).Result;
                        LstMenuRLCSItems = response.Content.ReadAsAsync<List<RLCSMenu>>().Result; 
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindProductMapping()
        {
            try
            {
                long customerID = -1;
                User loggedInUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (loggedInUser.CustomerID != null)
                    customerID = Convert.ToInt32(loggedInUser.CustomerID);
                else
                    customerID = Common.AuthenticationHelper.CustomerID;

                ProductMappingDetails = new List<long>();
                ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindUserAssignedCustomers()
        {
            try
            {
                int loggedInUserID = Convert.ToInt32((AuthenticationHelper.UserID));

                var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID);

                if (custmerlist.Count > 1)
                {
                    lstUserAssignedCustomers = new List<Cust>();

                    foreach (var item in custmerlist)
                    {
                        string path = string.Empty;
                        if (item.LogoPath != null)
                            path = item.LogoPath.Replace("~", "");

                        lstUserAssignedCustomers.Add(new Cust()
                        {
                            custID = item.ID,
                            custName = item.Name,
                            logoPath = string.IsNullOrEmpty(path) ? null : path
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClick_Click(object sender, EventArgs e)
        {
            int customerID = Convert.ToInt32(hfCustId.Value);
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", "alert('" + customerID + "')", true);

            ProductMappingStructure _obj = new ProductMappingStructure();
            if (_obj.ReAuthenticate_User(customerID))
                Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
        }
    }

    public class RLCSMenu
    {
        public string ParentId { get; set; }
        public string MenuName { get; set; }

        public string Id { get; set; }

        public string ParentMainMenu { get; set; }

        public List<RLCSChildMenu> LstChild { get; set; }

        public string LoggedInUserId { get; set; }

    }

    public class RLCSChildMenu
    {
        public string Id { get; set; }

        public string MenuName { get; set; }

        public string LoggedInUserId { get; set; }

    }
}