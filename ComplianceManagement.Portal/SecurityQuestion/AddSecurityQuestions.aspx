﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddSecurityQuestions.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion.AddSecurityQuestions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="Avantis - Development Team" />

    <title>Validate QA :: AVANTIS - Products that simplify</title>

    <!-- Bootstrap CSS -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" />
     
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="https://avacdn.azureedge.net/newcss/elegant-icons-style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>   
    <link href="https://avacdn.azureedge.net/newcss/style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/style-responsive.css" rel="stylesheet" />
    <link href="/newcss/ionicons.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

     <script type="text/javascript">
         var _paq = window._paq || [];
         /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
         _paq.push(['trackPageView']);
         _paq.push(['enableLinkTracking']);
         (function () {
             var u = "//analytics.avantis.co.in/";
             _paq.push(['setTrackerUrl', u + 'matomo.php']);
             _paq.push(['setSiteId', '1']);
             var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
             g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
         })();

        
         function settracknew(e, t, n, r) {
            
             try {
                 _paq.push(['trackEvent', e, t, n])
             } catch (t) { } return !0
         }

         function settracknewSucess() {
             settracknew('Login', 'SecurityQnA', 'Success', '');
         }
         function settracknewFailed() {
             settracknew('Login', 'SecurityQnA', 'Failed', '');
         }

        function fshowpass(obj)
        {
            if($('#txtAnswer1').attr('type')=='password'){
                $('#txtAnswer1').attr('type', 'text');
                $(obj).removeClass('ion-ios-eye');
                $(obj).addClass('ion-ios-eye-off');
            }else if($('#txtAnswer1').attr('type')=='text')
            {
                $('#txtAnswer1').attr('type', 'password');
                $(obj).removeClass('ion-ios-eye-off');
                $(obj).addClass('ion-ios-eye');
            }
        }
        function fshowpass2(obj) {
           
            if ($('#txtAnswar2').attr('type') == 'password') {
       
                $('#txtAnswar2').attr('type', 'text');
                $(obj).removeClass('ion-ios-eye');
                $(obj).addClass('ion-ios-eye-off');
            } else if ($('#txtAnswar2').attr('type') == 'text') {
                $('#txtAnswar2').attr('type', 'password');
                $(obj).removeClass('ion-ios-eye-off');
                $(obj).addClass('ion-ios-eye');
            }
        }
    </script>
</head>
<body>
    <div class="container">
        <form class="login-form" runat="server" name="login" id="Form1" autocomplete="off">
            <div class="col-md-12 login-form-head">
                <p class="login-img"> <a href="#">
                    <img src="../Images/avantil-logo.png" id="imgc" /></a>
                </p>
             
            </div>
            <div class="login-wrap">
                <div runat="server" id="divMasterQuestions" visible="false">
                    <div class="login-wrap">
                        <h1>Select new security questions & ans</h1>
                        <h3>These question will be used to verify your indentity
                        and help recover your password if you even forgot it. </h3>
                        <div class="clearfix" style="height: 10px"></div>
                        <asp:DropDownList ID="ddlQuestion1" CssClass="form-control input-lg m-bot15" runat="server"
                            OnSelectedIndexChanged="ddlQuestion1_SelectedIndexChanged" AutoPostBack="true" TabIndex="1">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Question." ControlToValidate="ddlQuestion1"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="SecurityQuestionValidationGroup"
                            Display="None" />
                        <div class="input-group" style="width: 100%">
                            <asp:TextBox ID="txtQuestionAnswer1" runat="server" class="form-control" TabIndex="2" placeholder="Enter Answer"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Answer 1 can not be empty." ControlToValidate="txtQuestionAnswer1"
                                runat="server" ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                           <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" Display = "None"  ValidationGroup="SecurityQuestionValidationGroup" ControlToValidate = "txtQuestionAnswer1" ValidationExpression = "^.*(?=.{3,}).*$" ErrorMessage="Answers must be minimum 3 character long">
                            </asp:RegularExpressionValidator> 
                        </div>
                        <asp:DropDownList ID="ddlQuestion2" CssClass="form-control input-lg m-bot15" runat="server"
                            OnSelectedIndexChanged="ddlQuestion2_SelectedIndexChanged" AutoPostBack="true" TabIndex="3">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Question." ControlToValidate="ddlQuestion2"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="SecurityQuestionValidationGroup"
                            Display="None" />
                        <div class="input-group" style="width: 100%">
                            <asp:TextBox ID="txtQuestionAnswer2" runat="server" class="form-control" TabIndex="4" placeholder="Enter Answer"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Answer 2 can not be empty." ControlToValidate="txtQuestionAnswer2"
                                runat="server" ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" Display = "None" ValidationGroup="SecurityQuestionValidationGroup" ControlToValidate = "txtQuestionAnswer2" ValidationExpression = "^.*(?=.{3,}).*$" ErrorMessage="Answers must be minimum 3 character long">
                            </asp:RegularExpressionValidator> 
                        </div>
                        <asp:DropDownList ID="ddlQuestion3" CssClass="form-control input-lg m-bot15" runat="server"
                            OnSelectedIndexChanged="ddlQuestion3_SelectedIndexChanged" AutoPostBack="true" TabIndex="5">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Question." ControlToValidate="ddlQuestion3"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="SecurityQuestionValidationGroup"
                            Display="None" />
                        <div class="input-group" style="width: 100%">
                            <asp:TextBox ID="txtQuestionAnswer3" runat="server" class="form-control" TabIndex="6" placeholder="Enter Answer"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Answer 3 can not be empty." ControlToValidate="txtQuestionAnswer3"
                                runat="server" ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" Display = "None"  ValidationGroup="SecurityQuestionValidationGroup" ControlToValidate = "txtQuestionAnswer3" ValidationExpression = "^.*(?=.{3,}).*$" ErrorMessage="Answers must be minimum 3 character long">
                            </asp:RegularExpressionValidator> 
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-6"> 
                                <asp:Button Text="Submit" runat="server" ID="btnSave" CssClass="btn btn-primary btn-lg btn-block"  CausesValidation="true" ValidationGroup="SecurityQuestionValidationGroup" OnClick="btnSave_Click" TabIndex="11" />
                            </div>
                            <div class="col-md-6">
                                <asp:Button Text="Clear" runat="server" ID="btnClear" OnClick="btnClear_Click" CssClass="btn btn-primary btn-lg btn-block" TabIndex="12"/>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px"></div>
                        <p>Kindly note answers are case-sensitive</p>
                        <p>Memorize your answers with exact spelling </p>
                        <p>Any 2 questions will be asked on your subsequent login </p>

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="SecurityQuestionValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="False"
                            ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                    </div>
                </div>
                <div id="QuestionAnswar" runat="server" visible="false">
                    <h1 style="font-size: 17px;">Validate answers for security questions</h1>
                    <h3>
                        <asp:Label ID="lblQuestion1ID" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblQuestion1" runat="server"></asp:Label>
                    </h3>
                    <div class="input-group" style="width: 100%">
                       
                           <asp:TextBox ID="txtAnswer1" runat="server" TextMode="Password" placeholder="Enter answer" MaxLength="100" class="form-control" TabIndex="1"></asp:TextBox>
                          <span class="input-group-addon"><i class="ion-ios-eye"  onclick="fshowpass(this);"  style="cursor:pointer;"></i>  </span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Answer can not be empty." ControlToValidate="txtAnswer1"
                            runat="server" ValidationGroup="AnswarValidationGroup" Display="None" />
                    </div>
                    <h3>
                        <asp:Label ID="lblQuestion2ID" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblQuestion2" runat="server" TabIndex="3"></asp:Label>
                    </h3>
                    <div class="input-group" style="width: 100%"> 
                        <asp:TextBox ID="txtAnswar2" runat="server" TextMode="Password" class="form-control"  MaxLength="100" placeholder="Enter answer" TabIndex="2"></asp:TextBox>
                          <span class="input-group-addon"  ><i class="ion-ios-eye" onclick="fshowpass2(this);"  style="cursor:pointer;"></i> </span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Answer can not be empty." ControlToValidate="txtAnswar2"
                            runat="server" ValidationGroup="AnswarValidationGroup" Display="None" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6" style="margin-left: -13px;">
                            <asp:Button Text="Validate" runat="server" ID="btnValidateQuestions"  TabIndex="7" class="btn btn-primary btn-lg btn-block"
                                ValidationGroup="AnswarValidationGroup" OnClick="btnValidateQuestions_Click" />
                        </div>
                        <div class="col-md-6" >
                            <asp:Button Text="Clear" runat="server" ID="btnValidateClear" TabIndex="3" OnClick="btnValidateClear_Click" CssClass="btn btn-primary btn-lg btn-block" />
                        </div>
                    </div>
                    <div class="clearfix" style="height: 15px"></div>
                    <p>Kindly note answers are case-sensitive</p>
                      <div style="margin-top:3%;">
                                  <asp:LinkButton ID="lnkLogOut" OnClientClick="javascript:return fclearcookie();" runat="server" >Or sign in as a different user</asp:LinkButton>
                    </div>
                    <%--<p style="display:none">
                        To reset your security information please
                    <asp:LinkButton ID="resetQuestions" runat="server" Text="click here." OnClientClick='ResetQuestionsAlert();'
                        OnClick="resetQuestions_Click" Visible="false"></asp:LinkButton>
                    </p>--%>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-block alert-danger fade in"
                        ValidationGroup="AnswarValidationGroup" />
                    <asp:CustomValidator ID="duEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                        ValidationGroup="AnswarValidationGroup" Display="None" />
                </div>
            </div>
              <asp:HiddenField runat="server" ID="cid" />
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <script>
     
        <%--if ($('#cid').val() == '914') {
                $('#imgc').attr('src', '../Images/zomatologo.jpg');
        }
      else if ('<%=LogoName%>' != null)
        {
            $('#imgc').attr('src', '<%=LogoName%>');
        }--%>

        $(document).ready(function () {
            
            if ($('#cid').val() == '914') {
                $('#imgc').attr('src', '../Images/zomatologo.jpg');
            }
            else if ('<%=LogoName%>' != null && '<%=LogoName%>' != '') {
               
                $('#imgc').attr('src', '<%=LogoName%>');
            }
            else {
                $('#imgc').attr('src', '../Images/avantil-logo.png');
            }

        });
        function fclearcookie() {
            window.location.href = "/logout.aspx"
            return false;
        }
        
    </script>
</body>
</html>