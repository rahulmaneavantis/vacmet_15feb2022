﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;
using System.Drawing;

namespace com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion
{
    public partial class ResetSecurityQuestions : System.Web.UI.Page
    {
        protected string LogoName;
        protected static int customerid;
        protected static int userid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CustomerID_new"] != null)
            {
                int customerID = Convert.ToInt32(Session["CustomerID_new"]);
                Customer objCust = UserManagement.GetCustomerforLogoCustomer(customerID);
                if (objCust != null)
                {
                    if (objCust.LogoPath != null)
                    {
                        var CustomerLogo = objCust.LogoPath;
                         LogoName = CustomerLogo.Replace("~", "..");
                    }
                }
            }
            if (!IsPostBack)
            {                
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    BindQuestionsFirst();                   
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindQuestions()
        {
            try
            {
                var Questions = UserManagement.GetSecurityQuestions();

                if (ddlQuestion1.SelectedIndex != -1 && ddlQuestion1.SelectedValue != "-1")
                {
                    Questions = Questions.Where(entry => entry.Question != ddlQuestion1.SelectedItem.Text).ToList();
                }
                if (ddlQuestion2.SelectedIndex != -1 && ddlQuestion2.SelectedValue != "-1")
                {
                    Questions = Questions.Where(entry => entry.Question != ddlQuestion2.SelectedItem.Text).ToList();
                }
                if (ddlQuestion3.SelectedIndex != -1 && ddlQuestion3.SelectedValue != "-1")
                {
                    Questions = Questions.Where(entry => entry.Question != ddlQuestion3.SelectedItem.Text).ToList();
                }
                if (!(ddlQuestion1.SelectedIndex != -1 && ddlQuestion1.SelectedValue != "-1"))
                {
                    ddlQuestion1.DataTextField = "Question";
                    ddlQuestion1.DataValueField = "ID";
                    ddlQuestion1.Items.Clear();
                    ddlQuestion1.DataSource = Questions;
                    ddlQuestion1.DataBind();
                    ddlQuestion1.Items.Insert(0, new ListItem("Select Question 1", "-1"));
                }
                if (!(ddlQuestion2.SelectedIndex != -1 && ddlQuestion2.SelectedValue != "-1"))
                {

                    ddlQuestion2.DataTextField = "Question";
                    ddlQuestion2.DataValueField = "ID";
                    ddlQuestion2.Items.Clear();
                    ddlQuestion2.DataSource = Questions;
                    ddlQuestion2.DataBind();
                    ddlQuestion2.Items.Insert(0, new ListItem("Select Question 2", "-1"));
                }
                if (!(ddlQuestion3.SelectedIndex != -1 && ddlQuestion3.SelectedValue != "-1"))
                {
                    ddlQuestion3.DataTextField = "Question";
                    ddlQuestion3.DataValueField = "ID";
                    ddlQuestion3.Items.Clear();
                    ddlQuestion3.DataSource = Questions;
                    ddlQuestion3.DataBind();
                    ddlQuestion3.Items.Insert(0, new ListItem("Select Question 3", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindQuestionsFirst()
        {
            try
            {
                var Questions = UserManagement.GetSecurityQuestions();
                ddlQuestion1.DataTextField = "Question";
                ddlQuestion1.DataValueField = "ID";
                ddlQuestion1.Items.Clear();
                ddlQuestion1.DataSource = Questions;
                ddlQuestion1.DataBind();
                ddlQuestion1.Items.Insert(0, new ListItem("Select Question 1", "-1"));

                ddlQuestion2.DataTextField = "Question";
                ddlQuestion2.DataValueField = "ID";
                ddlQuestion2.Items.Clear();
                ddlQuestion2.DataSource = Questions;
                ddlQuestion2.DataBind();
                ddlQuestion2.Items.Insert(0, new ListItem("Select Question 2", "-1"));

                ddlQuestion3.DataTextField = "Question";
                ddlQuestion3.DataValueField = "ID";
                ddlQuestion3.Items.Clear();
                ddlQuestion3.DataSource = Questions;
                ddlQuestion3.DataBind();
                ddlQuestion3.Items.Insert(0, new ListItem("Select Question 3", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlQuestion1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindQuestions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlQuestion2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindQuestions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlQuestion3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindQuestions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long userID = Convert.ToInt64(Session["userID"]);
                bool flag = true;
                bool flag1 = true;
                try
                {
                    // long userID = Convert.ToInt64(Session["userID"]);
                    flag = UserManagement.Delete(userID);
                    flag1 = UserManagementRisk.Delete(userID);
                }
                catch (Exception ex)
                {
                    flag = false;
                    flag1 = false;
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (flag && flag1)
                {

                    List<SecurityQuestionAnswar> questionList = new List<SecurityQuestionAnswar>();
                    SecurityQuestionAnswar question1 = new SecurityQuestionAnswar();
                    question1.SQID = Convert.ToInt32(ddlQuestion1.SelectedValue);
                    question1.Answar = txtQuestionAnswer1.Text.Trim();
                    question1.UserID = userID;
                    questionList.Add(question1);

                    SecurityQuestionAnswar question2 = new SecurityQuestionAnswar();
                    question2.SQID = Convert.ToInt32(ddlQuestion2.SelectedValue);
                    question2.Answar = txtQuestionAnswer2.Text.Trim();
                    question2.UserID = userID;
                    questionList.Add(question2);

                    SecurityQuestionAnswar question3 = new SecurityQuestionAnswar();
                    question3.SQID = Convert.ToInt32(ddlQuestion3.SelectedValue);
                    question3.Answar = txtQuestionAnswer3.Text.Trim();
                    question3.UserID = userID;
                    questionList.Add(question3);

                    List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk> questionListrisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk>();
                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question1risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
                    question1risk.SQID = Convert.ToInt32(ddlQuestion1.SelectedValue);
                    question1risk.Answar = txtQuestionAnswer1.Text.Trim();
                    question1risk.UserID = userID;
                    questionListrisk.Add(question1risk);

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question2risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
                    question2risk.SQID = Convert.ToInt32(ddlQuestion2.SelectedValue);
                    question2risk.Answar = txtQuestionAnswer2.Text.Trim();
                    question2risk.UserID = userID;
                    questionListrisk.Add(question2risk);

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question3risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
                    question3risk.SQID = Convert.ToInt32(ddlQuestion3.SelectedValue);
                    question3risk.Answar = txtQuestionAnswer3.Text.Trim();
                    question3risk.UserID = userID;
                    questionListrisk.Add(question3risk);

                    bool flagc = UserManagement.Create(questionList);
                    bool flagC1 = UserManagementRisk.Create(questionListrisk);
                    if (flagc && flagC1)
                    {
                        //Session["userID"] = null;
                        //Session.Abandon();
                        Session["QuestionBank"] = false;
                        Session["userID"] = userID;
                        Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        long userID = Convert.ToInt64(Session["userID"]);
        //        List<SecurityQuestionAnswar> questionList = new List<SecurityQuestionAnswar>();
        //        SecurityQuestionAnswar question1 = new SecurityQuestionAnswar();
        //        question1.SQID = Convert.ToInt32(ddlQuestion1.SelectedValue);
        //        question1.Answar = txtQuestionAnswer1.Text.Trim();
        //        question1.UserID = userID;
        //        questionList.Add(question1);

        //        SecurityQuestionAnswar question2 = new SecurityQuestionAnswar();
        //        question2.SQID = Convert.ToInt32(ddlQuestion2.SelectedValue);
        //        question2.Answar = txtQuestionAnswer2.Text.Trim();
        //        question2.UserID = userID;
        //        questionList.Add(question2);

        //        SecurityQuestionAnswar question3 = new SecurityQuestionAnswar();
        //        question3.SQID = Convert.ToInt32(ddlQuestion3.SelectedValue);
        //        question3.Answar = txtQuestionAnswer3.Text.Trim();
        //        question3.UserID = userID;
        //        questionList.Add(question3);

        //        List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk> questionListrisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk>();
        //        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question1risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
        //        question1risk.SQID = Convert.ToInt32(ddlQuestion1.SelectedValue);
        //        question1risk.Answar = txtQuestionAnswer1.Text.Trim();
        //        question1risk.UserID = userID;
        //        questionListrisk.Add(question1risk);

        //        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question2risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
        //        question2risk.SQID = Convert.ToInt32(ddlQuestion2.SelectedValue);
        //        question2risk.Answar = txtQuestionAnswer2.Text.Trim();
        //        question2risk.UserID = userID;
        //        questionListrisk.Add(question2risk);

        //        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk question3risk = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.SecurityQuestionAnswarRisk();
        //        question3risk.SQID = Convert.ToInt32(ddlQuestion3.SelectedValue);
        //        question3risk.Answar = txtQuestionAnswer3.Text.Trim();
        //        question3risk.UserID = userID;
        //        questionListrisk.Add(question3risk);

        //        bool flag = UserManagement.Create(questionList);
        //        bool flag1 = UserManagementRisk.Create(questionListrisk);
        //        if (flag && flag1)
        //        {
        //            //Session["userID"] = null;
        //            //Session.Abandon();
        //            Session["QuestionBank"] = false;
        //            Session["userID"] = userID;
        //            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlQuestion1.SelectedIndex = -1;
                txtQuestionAnswer1.Text = string.Empty;
                ddlQuestion2.SelectedIndex = -1;
                txtQuestionAnswer2.Text = string.Empty;
                ddlQuestion3.SelectedIndex = -1;
                txtQuestionAnswer3.Text = string.Empty;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}