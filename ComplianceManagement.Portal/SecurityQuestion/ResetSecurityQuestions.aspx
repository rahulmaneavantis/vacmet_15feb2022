﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetSecurityQuestions.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion.ResetSecurityQuestions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="FortuneCookie - Development Team" />

    <title>Reset password- QA :: AVANTIS - Products that simplify</title>

    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../NewCSS/style.css" rel="stylesheet" />
    <link href="../NewCSS/style-responsive.css" rel="stylesheet" />
      <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

     <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92029518-1', 'auto');
        ga('send', 'pageview');

        function settracknew(e, t, n, r) {
            try {
                ga('send', 'event', e, t, n + "#" + r)
            } catch (t) { } return !0
        }

        function settracknewnonInteraction(e, t, n, r) {
            ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
        }
          
    </script>
</head>
<body>
    <div class="container">
        <form runat="server" class="login-form" name="login" id="Form1" autocomplete="off">
            <asp:Panel ID="Panel1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />
                <div class="col-md-12 login-form-head">
                    <p class="login-img"><a href="#">
                        <img id="imgc" src="../Images/avantil-logo.png" /></a>
                    </p>
                </div>
                <div class="login-wrap">
                    <h2>Select new security questions & answers</h2>
                    <h3>These question will be used to verify your indentity
                     and help recover your password if you even forgot it. </h3>
                    <div class="clearfix" style="height: 10px"></div>
                    <asp:DropDownList ID="ddlQuestion1" CssClass="form-control input-lg m-bot15" runat="server"
                        OnSelectedIndexChanged="ddlQuestion1_SelectedIndexChanged" AutoPostBack="true" TabIndex="1">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Question." ControlToValidate="ddlQuestion1"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="SecurityQuestionValidationGroup"
                        Display="None" />
                    <div class="input-group" style="width: 100%">
                        <asp:TextBox ID="txtQuestionAnswer1" runat="server" class="form-control" TabIndex="2" placeholder="Enter Answer"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Answer 1 can not be empty." ControlToValidate="txtQuestionAnswer1"
                            runat="server" ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                          <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" Display = "None"  ValidationGroup="SecurityQuestionValidationGroup" ControlToValidate = "txtQuestionAnswer1" ValidationExpression = "^.*(?=.{3,}).*$" ErrorMessage="Answers must be minimum 3 character long">
                            </asp:RegularExpressionValidator> 
                    </div>
                    <asp:DropDownList ID="ddlQuestion2" CssClass="form-control input-lg m-bot15" runat="server"
                        OnSelectedIndexChanged="ddlQuestion2_SelectedIndexChanged" AutoPostBack="true" TabIndex="3">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Question." ControlToValidate="ddlQuestion2"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="SecurityQuestionValidationGroup"
                        Display="None" />
                    <div class="input-group" style="width: 100%">
                        <asp:TextBox ID="txtQuestionAnswer2" runat="server" class="form-control" TabIndex="4" placeholder="Enter Answer"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Answer 2 can not be empty." ControlToValidate="txtQuestionAnswer2"
                            runat="server" ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                           <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" Display = "None" ValidationGroup="SecurityQuestionValidationGroup" ControlToValidate = "txtQuestionAnswer2" ValidationExpression = "^.*(?=.{3,}).*$" ErrorMessage="Answers must be minimum 3 character long">
                            </asp:RegularExpressionValidator> 
                    </div>
                    <asp:DropDownList ID="ddlQuestion3" CssClass="form-control input-lg m-bot15" runat="server"
                        OnSelectedIndexChanged="ddlQuestion3_SelectedIndexChanged" AutoPostBack="true" TabIndex="5">
                    </asp:DropDownList>
                    <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Question." ControlToValidate="ddlQuestion3"
                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="SecurityQuestionValidationGroup"
                        Display="None" />
                    <div class="input-group" style="width: 100%">
                        <asp:TextBox ID="txtQuestionAnswer3" runat="server" class="form-control" TabIndex="6" placeholder="Enter Answer"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Answer 3 can not be empty." ControlToValidate="txtQuestionAnswer3"
                            runat="server" ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                           <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" Display = "None"  ValidationGroup="SecurityQuestionValidationGroup" ControlToValidate = "txtQuestionAnswer3" ValidationExpression = "^.*(?=.{3,}).*$" ErrorMessage="Answers must be minimum 3 character long">
                            </asp:RegularExpressionValidator> 
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <asp:Button class="btn btn-primary btn-lg btn-block" Text="Submit" runat="server" ID="btnSave"
                                ValidationGroup="SecurityQuestionValidationGroup" OnClick="btnSave_Click" TabIndex="11" />
                        </div>

                        <div class="col-md-6">
                            <asp:Button class="btn btn-primary btn-lg btn-block" Text="Clear" runat="server" ID="btnClear" OnClick="btnClear_Click" />
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px"></div>
                    <p>Kindly note answer are case-sensitive</p>
                    <p>Memorize your answer with exact spelling </p>
                    <p>Any 2 questions will be asked on your subsequent login </p>
                    <p onclick="back()" style="cursor:pointer">Click here to go back </p>
                    <div class="clearfix" style="height: 10px"></div>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                        ValidationGroup="SecurityQuestionValidationGroup"/>
                    <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                        ValidationGroup="SecurityQuestionValidationGroup" Display="None" />
                </div>
            </asp:Panel>
        </form>
    </div>
    <script>
      <%-- if('<%=LogoName%>' != null)
            {                                  
            $('#imgc').attr('src', '<%=LogoName%>');
            }--%>
        function back() {
            window.history.back();
        };

        $(document).ready(function () {
           
            if ($('#cid').val() == '914') {
                $('#imgc').attr('src', '../Images/zomatologo.jpg');
            }
            else if ('<%=LogoName%>' != null && '<%=LogoName%>' != '') {
               
                 $('#imgc').attr('src', '<%=LogoName%>');
             }
             else {
                $('#imgc').attr('src', '../Images/avantisLogo.png');
               
             }

         });

    </script>
</body>
</html>
