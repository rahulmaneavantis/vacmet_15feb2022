﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Security;
using System.Configuration;
using System.Drawing;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void SetResetPasswordRandomQuestion()
        {
            try
            {

                User user = UserManagement.GetByUsername(txtResetPasswordUserID.Text.Trim());
                if (user != null)
                {
                    ViewState["userID"] = null;
                    ViewState["userID"] = user.ID;
                    var randomQuestion = UserManagement.GetRandomQuestions(user.ID);
                    if (randomQuestion.Count > 0)
                    {
                        tblResetPasswordQuestion.Visible = true;
                        divResetPasswordSubmit.Visible = false;
                        lblRPQustion1ID.Text = Convert.ToString(randomQuestion[0].ID);
                        lblRPQustion1.Text = randomQuestion[0].Question;
                        lblRPQustion2ID.Text = Convert.ToString(randomQuestion[1].ID);
                        lblRPQustion2.Text = randomQuestion[1].Question;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "You are the guest user or you did not set your security questions. Please contact your company admin to unlock the account or reset password.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Enter valid emailID.";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GenerateTempPasswordSendMailBackLogin()
        {
            try
            {
                if (txtResetPasswordUserID.Text.Trim().Contains('@'))
                {
                    if (!UserManagement.EmailIDExists(txtResetPasswordUserID.Text.Trim()))
                    {
                        #region If EmailID 
                        User user = UserManagement.GetByUsername(txtResetPasswordUserID.Text.Trim());
                        if (user != null)
                        {
                            ViewState["userID"] = null;
                            ViewState["userID"] = user.ID;

                            mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(user.ID));

                            string passwordText = Util.CreateRandomPassword(10);
                            string encryptedPwd = Util.CalculateAESHash(passwordText);
                            user.Password = encryptedPwd;
                            string message = SendNotificationEmail(user, passwordText);

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                            bool result = ChangePassword(user);
                            bool result1 = ChangePassword(mstuser);

                            //Added on 20072020 for TL_AVACOM
                            if (user.ID != null)
                            {
                                bool isRLCSUser = UserManagement.IsRLCSUser(Convert.ToInt32(user.ID));
                                if (isRLCSUser)
                                {
                                    UserManagement.ChangePassword_RLCS(Convert.ToInt32(user.ID), encryptedPwd, "A");
                                }
                            }

                            if (result && result1)
                            {
                                UserManagement.WrongAttemptCountUpdate(user.ID);
                                //Send Email on User Mail Id.

                                if(user.CustomerID == 35) //Badve Group
                                {
                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);
                                }
                                else
                                {
                                    string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);
                                }



                                Session.Clear();
                                Session["userID"] = null;
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                                Session.RemoveAll();
                                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                                FormsAuthentication.SignOut();
                                Response.Redirect("~/Login.aspx?flag=Y", false);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Enter valid email/ email is not registered with us.";
                        }
                        #endregion
                    }
                    else
                    {
                        #region If EmailID 
                        User user = UserManagement.GetByUsername(txtResetPasswordUserID.Text.Trim());
                        if (user != null)
                        {
                            ViewState["userID"] = null;
                            ViewState["userID"] = user.ID;

                            mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(user.ID));
                            RLCS_User_Mapping rlcsuser = UserManagement.GetRLCSByID(txtResetPasswordUserID.Text.Trim());                         
                            string passwordText = Util.CreateRandomPassword(10);                           
                            string Newpassword = Util.CalculateAESHash(passwordText);

                            user.Password = Newpassword;
                            mstuser.Password = Newpassword;
                            rlcsuser.Password = Newpassword;

                            string message = SendNotificationEmail(user, passwordText);
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                            bool result = ChangePassword(user);
                            bool result1 = ChangePassword(mstuser);

                            bool result2 = false;
                            //bool result2 = UserManagement.RLCSChangePassword(rlcsuser);

                            //Added on 20072020 for TL_AVACOM
                            if (user.ID != null)
                            {
                                bool isRLCSUser = UserManagement.IsRLCSUser(Convert.ToInt32(user.ID));
                                if (isRLCSUser)
                                {
                                    result2 = UserManagement.ChangePassword_RLCS(Convert.ToInt32(user.ID), Newpassword, "A");
                                }
                            }

                            if (result && result1 && result2)
                            {
                                UserManagement.WrongAttemptCountUpdate(user.ID);
                                //Send Email on User Mail Id.
                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                //SendGridEmailManager.SendGridMail(SenderEmailAddress, ReplyEmailAddressName, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);

                                Session.Clear();
                                Session.Abandon();
                                Session.RemoveAll();

                                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                                FormsAuthentication.SignOut();
                                Response.Redirect("~/Login.aspx?flag=Y", false);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Enter valid email/ email is not registered with us.";
                        }
                        #endregion
                    }
                }
                else
                {
                    #region If NO EmailID 
                    RLCS_User_Mapping user = UserManagement.GetRLCSByID(txtResetPasswordUserID.Text.Trim());                   
                    if (user != null)
                    {
                        ViewState["userID"] = null;
                        ViewState["userID"] = user.AVACOM_UserID;

                       
                        string passwordText = Util.CreateRandomPassword(10);
                        user.Password = Util.CalculateAESHash(passwordText);

                        string message = RLCSSendNotificationEmail(user, passwordText);
                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        bool result = UserManagement.RLCSChangePassword(user);                        
                        if (result)
                        {
                            //Send Email on User Mail Id.

                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);
                            //string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                            //SendGridEmailManager.SendGridMail(SenderEmailAddress, ReplyEmailAddressName, new List<String>(new String[] { user.Email }), null, null, "Temporary Password", message);

                          
                            Session.Clear();
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                            FormsAuthentication.SignOut();
                            Response.Redirect("~/Login.aspx?flag=Y", false);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Enter valid email/ email is not registered with us.";
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public bool ChangePassword(User user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);                        
                        User userToUpdate = new User();

                        userToUpdate = (from entry in entities.Users
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.EnType = "A";
                       userToUpdate.ChangePasswordFlag = false;

                        entities.SaveChanges();

                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        //entities.Connection.Close();
                        return false;
                    }
                }
            }
        }

        public static bool ChangePassword(mst_User user)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        mst_User userToUpdate = new mst_User();
                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.ChangePasswordFlag = false;
                        userToUpdate.EnType = "A";
                        entities.SaveChanges();                        
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                if (user != null)
                {
                    int customerID = -1;
                    string ReplyEmailAddressName = "";
                    if (user.RoleID == 1 || user.RoleID == 12 || user.RoleID == 24 || user.RoleID == 25 || user.RoleID == 26)
                    {
                        ReplyEmailAddressName = "Avantis";
                    }                   
                    else
                    {
                        customerID = UserManagement.GetByID(Convert.ToInt32(ViewState["userID"])).CustomerID ?? 0;
                        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    }                  
                    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string portalurl = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                    if (Urloutput != null)
                    {
                        portalurl = Urloutput.URL;
                    }
                    else
                    {
                        portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                                           .Replace("@Username", user.Email)
                                           .Replace("@User", username)
                                           .Replace("@PortalURL", Convert.ToString(portalurl))
                                           .Replace("@Password", passwordText)
                                           .Replace("@From", ReplyEmailAddressName)
                                           .Replace("@URL", Convert.ToString(portalurl));

                    //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                    //                        .Replace("@Username", user.Email)
                    //                        .Replace("@User", username)
                    //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                    //                        .Replace("@Password", passwordText)
                    //                        .Replace("@From", ReplyEmailAddressName)
                    //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                    return message;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }
        private string RLCSSendNotificationEmail(RLCS_User_Mapping user, string passwordText)
        {
            try
            {
                if (user != null)
                {
                    string ReplyEmailAddressName = "";
                    ReplyEmailAddressName = "Avantis";
                    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string portalurl = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                    if (Urloutput != null)
                    {
                        portalurl = Urloutput.URL;
                    }
                    else
                    {
                        portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                                        .Replace("@Username", user.UserID)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(portalurl))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(portalurl));
                    //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ForgotPassword_New
                    //                        .Replace("@Username", user.UserID)
                    //                        .Replace("@User", username)
                    //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                    //                        .Replace("@Password", passwordText)
                    //                        .Replace("@From", ReplyEmailAddressName)
                    //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                    return message;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }
        protected void btnProceed_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateTempPasswordSendMailBackLogin();

                //SetResetPasswordRandomQuestion();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                txtRPQustion1.BorderColor = Color.Empty;
                txtRPQustion2.BorderColor = Color.Empty;
                txtRPQustion2.BorderColor = Color.Empty;
                if (Convert.ToString(ViewState["userID"]) != null)
                {
                    if (Convert.ToString(ViewState["userID"]) != "")
                    {
                        long userID = Convert.ToInt64(ViewState["userID"]);
                        List<SecurityQuestionAnswar> questionList = new List<SecurityQuestionAnswar>();
                        SecurityQuestionAnswar question1 = new SecurityQuestionAnswar();
                        question1.SQID = Convert.ToInt32(lblRPQustion1ID.Text.Trim());
                        question1.Answar = txtRPQustion1.Text.Trim();
                        question1.UserID = userID;
                        questionList.Add(question1);

                        SecurityQuestionAnswar question2 = new SecurityQuestionAnswar();
                        question2.SQID = Convert.ToInt32(lblRPQustion2ID.Text);
                        question2.Answar = txtRPQustion2.Text.Trim();
                        question2.UserID = userID;
                        questionList.Add(question2);

                        int flag = UserManagement.ValidateQuestions(questionList);

                        if (flag == 0)
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(userID));
                            mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(userID));
                            string passwordText = Util.CreateRandomPassword(10);
                            user.Password = Util.CalculateAESHash(passwordText);
                            mstuser.Password = Util.CalculateAESHash(passwordText);
                            // added by sudarshan for comman notification
                            string ReplyEmailAddressName = "Avantis";
                            string portalurl = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                            if (Urloutput != null)
                            {
                                portalurl = Urloutput.URL;
                            }
                            else
                            {
                                portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(portalurl), ReplyEmailAddressName);
                            //string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                            bool result = UserManagement.ChangePassword(user);
                            bool result1 = UserManagementRisk.ChangePassword(mstuser);
                            if (result && result1)
                            {
                                UserManagement.WrongAttemptCountUpdate(user.ID);
                                string scriptText = "alert('Password Reset Successfully!!!! A new mail generated with new password. Please Login with your new login credentials.'); window.location='" + Request.ApplicationPath + "/Login.aspx'";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", "settracknewForForgotPass();", true);
                                Session["userID"] = null;
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertMessage", "settracknewForFailed();", true);

                            }
                        }
                        else
                        {
                            if (flag == 1)
                                txtRPQustion1.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                            else if (flag == 2)
                                txtRPQustion2.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                            else
                                txtRPQustion2.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please enter valid answers.";
                        }
                    }
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnResetPasswordClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtRPQustion1.Text = string.Empty;
                txtRPQustion2.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnklogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/login.aspx", false);
        }
    }
}