﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CachePage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CachePage" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="Button1" runat="server" Text="Set" OnClick="Button1_Click" /></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblUserID" runat="server" Text="UserID"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Refresh" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
