﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Text;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_CustomerDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSummary1.CssClass = "alert alert-danger";

            if (!IsPostBack)
            {
                ViewState["CorpID"] = null;
                                
                BindCustomerStatus();
                BindLocationType();                
                BindServiceProvider();
                BindDistributors();
                BindProductType();

                ddlComplianceProductType_SelectedIndexChanged(sender, e);

                int customerID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    customerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                }

                if (customerID != 0)
                {
                    int complianceProductType = 0;
                    complianceProductType = RLCS_Master_Management.GetComplianceProductType(customerID);
                    ViewState["ComplianceProductType"] = complianceProductType;

                    if (complianceProductType >= 1)
                        ViewState["CorpID"] = RLCS_Master_Management.GetCorporateIDByCustID(customerID);
                    if (string.IsNullOrEmpty(ViewState["CorpID"].ToString()))
                        txtCorpID.Text = GetCorpID(tbxName.Text.Trim());

                    EditCustomerInformation(customerID);
                }
                else if (customerID == 0)
                {
                    ViewState["Mode"] = 0;
                }

                //Allow Access to Create Sub-Distributor
                int loggedInUserCustomerID = 0;

                if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["DistID"]))
                    {
                        loggedInUserCustomerID = Convert.ToInt32(Request.QueryString["DistID"]);
                    }
                    //loggedInUserCustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                    loggedInUserCustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var custRecord = CustomerManagement.GetByID(loggedInUserCustomerID);

                if (custRecord != null)
                {
                    bool canCreateSubDist = RLCS_Master_Management.CanCreateSubDistributors(custRecord);

                    if (!canCreateSubDist && AuthenticationHelper.Role != "IMPT")
                    {
                        //if (rblCustomerDistributor.Items.FindByValue("C") != null)
                        //{
                        //    rblCustomerDistributor.ClearSelection();
                        //    rblCustomerDistributor.Items.FindByValue("C").Selected = true;
                        //}

                        if (rblCustomerDistributor.Items.FindByValue("D") != null)
                            rblCustomerDistributor.Items.FindByValue("D").Enabled = false;
                    }

                    //FixedPrice or Payment
                    if (ViewState["Mode"] != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {                            
                            bool isPaymentModeCust = RLCS_Master_Management.IsPaymentModeCustomer(custRecord);

                            if (isPaymentModeCust)
                            {
                                if (rblPayment.Items.FindByValue("1") != null)
                                {
                                    rblPayment.ClearSelection();
                                    rblPayment.Items.FindByValue("1").Selected = true;
                                }
                            }
                        }
                    }
                }

                if (loggedInUserCustomerID == customerID && AuthenticationHelper.Role != "IMPT")
                {
                    rblSubDistributor.Enabled = false;
                    divSubDist.Visible = false;
                }
                else
                {
                    rblSubDistributor.Enabled = true;
                    divSubDist.Visible = true;
                }

                rblCustomerDistributor_CheckedChanged(null, null);
            }            
        }
        public void BindProductType()
        {
            int customerID;
            try
            {
                lstBoxProduct.DataTextField = "ProductName";
                lstBoxProduct.DataValueField = "ProductID";
                if (Request.QueryString["DistID"] != null)
                {
                    customerID = Convert.ToInt32(Request.QueryString["DistID"]);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                //   lstBoxProduct.DataSource = CommanClass.FillProduct();
                lstBoxProduct.DataSource = CommanClass.GetAllProductMapping(customerID);
                lstBoxProduct.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";
                ddlLocationType.Items.Clear();
                ddlLocationType.DataSource = ProcessManagement.FillLocationType();
                ddlLocationType.DataBind();
                ddlLocationType.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindServiceProvider()
        {
            try
            {
                int serviceProviderID = 95;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlSPName.DataTextField = "Name";
                ddlSPName.DataValueField = "ID";
                ddlSPName.Items.Clear();
                ddlSPName.DataSource = CustomerManagement.FillServiceProvider(serviceProviderID);
                ddlSPName.DataBind();

                if (ddlSPName.Items.FindByValue(serviceProviderID.ToString()) != null)
                    ddlSPName.Items.FindByValue(serviceProviderID.ToString()).Selected = true;
                else
                    ddlSPName.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDistributors()
        {
            try
            {
                int distbutorID = 95; //95=Avantis
                if (AuthenticationHelper.Role == "SPADM")
                {
                    distbutorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distbutorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    distbutorID = RLCS_Master_Management.Get_DistributorID(Convert.ToInt32(AuthenticationHelper.CustomerID));                    
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["DistID"]))
                    {
                        distbutorID = Convert.ToInt32(Request.QueryString["DistID"]);
                    }                    
                }

                ddlDistributor.DataTextField = "Name";
                ddlDistributor.DataValueField = "ID";
                ddlDistributor.Items.Clear();

                ddlDistributor.DataSource = CustomerManagement.Fill_ServiceProviders_Distributors(1, -1, distbutorID);
                //ddlDistributor.DataSource = CustomerManagement.FillServiceProvider(distbutorID);
                ddlDistributor.DataBind();

                if (ddlDistributor.Items.FindByValue(distbutorID.ToString()) != null)
                    ddlDistributor.Items.FindByValue(distbutorID.ToString()).Selected = true;
                else
                    ddlDistributor.Items.Insert(0, new ListItem("Select", "-1"));

                if (ddlDistributor.Items.Count == 1 && ddlDistributor.SelectedValue != "-1")
                    divDistributor.Visible = false;
                else
                    divDistributor.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditCustomerInformation(int customerID)
        {
            try
            {
                lblErrorMassage.Text = "";
                ViewState["Mode"] = 1;
                ViewState["CustomerID"] = customerID;

                Customer customer = CustomerManagement.GetByID(customerID);
                tbxName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxBuyerName.Text = customer.BuyerName;
                tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                tbxBuyerEmail.Text = customer.BuyerEmail;
                txtStartDate.Text = customer.StartDate != null ? customer.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                txtEndDate.Text = customer.EndDate != null ? customer.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                txtDiskSpace.Text = customer.DiskSpace;

                ImgLogo.ImageUrl = string.IsNullOrEmpty(customer.LogoPath) ? "" : customer.LogoPath;
                imgDiv.Visible = string.IsNullOrEmpty(customer.LogoPath) ? false : true;

                if (customer.LocationType != null)
                {
                    ddlLocationType.SelectedValue = Convert.ToString(customer.LocationType);
                }
                if (customer.IComplianceApplicable != null)
                {
                    if (ddlComplianceApplicable.Items.FindByValue(Convert.ToString(customer.IComplianceApplicable)) != null)
                    {
                        ddlComplianceApplicable.ClearSelection();
                        ddlComplianceApplicable.Items.FindByValue(Convert.ToString(customer.IComplianceApplicable)).Selected = true;
                    }                    
                }
                if (customer.TaskApplicable != null)
                {
                    if (ddlTaskApplicable.Items.FindByValue(Convert.ToString(customer.TaskApplicable)) != null)
                    {
                        ddlTaskApplicable.ClearSelection();
                        ddlTaskApplicable.Items.FindByValue(Convert.ToString(customer.TaskApplicable)).Selected = true;
                    }                    
                }
                if (customer.Status != null)
                {
                    if (ddlCustomerStatus.Items.FindByValue(Convert.ToString(customer.Status)) != null)
                    {
                        ddlCustomerStatus.ClearSelection();
                        ddlCustomerStatus.Items.FindByValue(Convert.ToString(customer.Status)).Selected = true;
                    }                    
                }

                if (customer.IsServiceProvider == true)
                {
                    chkSp.Checked = true;
                    //showHideServiceProvider();
                    customer.ServiceProviderID = null;
                }
                else
                {
                    BindServiceProvider();
                    chkSp.Checked = false;
                    //showHideServiceProvider();
                    if (ddlSPName.Items.FindByValue(Convert.ToString(customer.ServiceProviderID)) != null)
                    {
                        ddlSPName.ClearSelection();
                        ddlSPName.Items.FindByValue(Convert.ToString(customer.ServiceProviderID)).Selected = true;
                    }                    
                }

                rblCustomerDistributor.ClearSelection();
                ViewState["Type"] = customer.IsDistributor;
                if (customer.IsDistributor == true)
                {
                    if (rblCustomerDistributor.Items.FindByValue("D") != null)
                        rblCustomerDistributor.Items.FindByValue("D").Selected = true;
                }
                else
                {
                    if (rblCustomerDistributor.Items.FindByValue("C") != null)
                        rblCustomerDistributor.Items.FindByValue("C").Selected = true;
                }

                if (customer.CanCreateSubDist != null)
                {
                    rblSubDistributor.ClearSelection();
                    if (customer.CanCreateSubDist == true)
                    {
                        if (rblSubDistributor.Items.FindByValue("1") != null)
                            rblSubDistributor.Items.FindByValue("1").Selected = true;
                    }
                    else
                    {
                        if (rblSubDistributor.Items.FindByValue("0") != null)
                            rblSubDistributor.Items.FindByValue("0").Selected = true;
                    }
                }

                rblCustomerDistributor_CheckedChanged(null, null);

                if (customer.IsPayment != null)
                {
                    rblPayment.ClearSelection();
                    if (customer.IsPayment == true)
                    {
                        if (rblPayment.Items.FindByValue("1") != null)
                        {
                            rblPayment.ClearSelection();
                            rblPayment.Items.FindByValue("1").Selected = true;
                        }
                    }
                    else
                    {
                        if (rblPayment.Items.FindByValue("0") != null)
                        {
                            rblPayment.ClearSelection();
                            rblPayment.Items.FindByValue("0").Selected = true;
                        }                        
                    }
                }
                
                if (customer.ComplianceProductType != null)
                {
                    if (ddlComplianceProductType.Items.FindByValue(Convert.ToString(customer.ComplianceProductType)) != null)
                    {
                        ddlComplianceProductType.ClearSelection();
                        ddlComplianceProductType.Items.FindByValue(Convert.ToString(customer.ComplianceProductType)).Selected = true;
                    }
                }

                if (ddlComplianceProductType.SelectedValue != "" || !string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                {
                    if (Convert.ToInt32(ddlComplianceProductType.SelectedValue) > 0)
                    {
                        ddlComplianceProductType_SelectedIndexChanged(null, null);

                        //GET Corporate Record
                        var RLCSCorporateRecord = RLCS_Master_Management.Get_Customer_CorporateClient_Mapping(customerID);

                        if (RLCSCorporateRecord != null)
                        {
                            txtCorpID.Text = RLCSCorporateRecord.CO_CorporateID;

                            if (!string.IsNullOrEmpty(txtCorpID.Text))
                            {
                                txtCorpID.Enabled = false;
                                btnCheckCorpID.Enabled = false;
                                ViewState["CorpID"] = txtCorpID.Text;
                                lblCheckCorporate.Visible = false;
                                btnSave.Enabled = true;
                            }

                            txtPAN.Text = RLCSCorporateRecord.CO_PAN;
                            txtTAN.Text = RLCSCorporateRecord.CO_TAN;

                            if (RLCSCorporateRecord.CO_Country != null)
                            {
                                if (ddlCountry.Items.FindByValue(RLCSCorporateRecord.CO_Country) != null)
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(RLCSCorporateRecord.CO_Country);
                                    //ddlCountry.Items.FindByValue(RLCSCorporateRecord.CO_Country).Selected = true;

                                    ddlCountry_SelectedIndexChanged(null, null);
                                }
                            }

                            if (RLCSCorporateRecord.CO_State != null)
                            {
                                if (ddlState.Items.FindByValue(RLCSCorporateRecord.CO_State) != null)
                                {
                                    ddlState.ClearSelection();
                                    ddlState.Items.FindByValue(RLCSCorporateRecord.CO_State).Selected = true;
                                    //ddlState.SelectedValue = Convert.ToString(RLCSCorporateRecord.CO_State);

                                    ddlState_SelectedIndexChanged(null, null);

                                    if (RLCSCorporateRecord.CO_City != null)
                                    {
                                        if (ddlCity.Items.FindByText(RLCSCorporateRecord.CO_City) != null)
                                        {
                                            ddlCity.ClearSelection();
                                            ddlCity.Items.FindByText(RLCSCorporateRecord.CO_City).Selected = true;
                                            //ddlCity.SelectedItem.Text = Convert.ToString(RLCSCorporateRecord.CO_City);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    txtCorpID.Text = txtPAN.Text = txtTAN.Text = string.Empty;
                    ddlCountry.ClearSelection();
                    ddlState.ClearSelection();
                    ddlCity.ClearSelection();
                }
                //Product Mapping
                if (lstBoxProduct.Items.Count > 0)
                {
                    lstBoxProduct.ClearSelection();
                }

                List<long> ProductMappingDetails = new List<long>();
                ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));

                if (ProductMappingDetails != null)
                {
                    if (ProductMappingDetails.Count > 0)
                    {
                        foreach (var eacProductID in ProductMappingDetails)
                        {
                            if (lstBoxProduct.Items.FindByValue(eacProductID.ToString()) != null)
                                lstBoxProduct.Items.FindByValue(eacProductID.ToString()).Selected = true;
                        }
                    }
                }

                upCustomers.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddCustomer()
        {
            try
            {
                lblErrorMassage.Text = string.Empty;
                ViewState["Mode"] = 0;
                chkSp.Checked = false;

                tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = txtStartDate.Text = txtEndDate.Text = txtDiskSpace.Text = txtCorpID.Text = txtPAN.Text = txtTAN.Text = string.Empty;

                ddlCustomerStatus.SelectedIndex = -1;

                ddlComplianceProductType.ClearSelection();
                ddlCountry.ClearSelection();
                ddlState.ClearSelection();
                ddlCity.ClearSelection();

                imgDiv.Visible = false;

                //ddlCountry.Items.Clear();
                //ddlState.Items.Clear();
                //ddlCity.Items.Clear();

                txtCorpID.Enabled = true;
                btnCheckCorpID.Enabled = true;

                ddlComplianceProductType_SelectedIndexChanged(null, null);

                upCustomers.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplianceApplicable_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCorpID.Text))
                {
                    int avacom_CustomerID = 0;
                    bool saveSuccess = false;

                    Customer customer = new Customer();
                    ///Hr Product Entry 22APRIL2021                  
                    customer.CreatedBy = AuthenticationHelper.UserID;
                    customer.CreatedFrom = 9;
                    //END
                    customer.Name = tbxName.Text;
                    customer.Address = tbxAddress.Text;
                    customer.BuyerName = tbxBuyerName.Text;
                    customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                    customer.BuyerEmail = tbxBuyerEmail.Text;
                                        
                    string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                    string enddate = Request[txtEndDate.UniqueID].ToString().Trim();

                    if (strtdate != "" && enddate != "")
                    {
                        customer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        customer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    customer.DiskSpace = txtDiskSpace.Text;

                    if (ViewState["Mode"] != null)
                    {
                        if ((int)ViewState["Mode"] == 1)
                        {
                            customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                            avacom_CustomerID = Convert.ToInt32(ViewState["CustomerID"]);
                        }
                    }

                    if (CustomerManagement.Exists(customer))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    if (chkSp.Checked == false)
                    {
                        if (string.IsNullOrEmpty(ddlSPName.SelectedValue) || ddlSPName.SelectedValue == "-1") //if (ddlSPName.SelectedIndex == 0)
                        {
                            cvDuplicateEntry.ErrorMessage = "Please Select Service Provider Name.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                    }

                    customer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                    customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                    customer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                    customer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);

                    //Service Provider
                    customer.IsServiceProvider = chkSp.Checked;
                    //if(Convert.ToInt32(ddlSPName.SelectedValue)==-1)
                    //{
                    //    customer.ServiceProviderID = null;
                    //}
                    //else
                    //{
                    //    customer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                    //}

                    if (chkSp.Checked == true)
                    {
                        customer.IsServiceProvider = true;
                        customer.ServiceProviderID = null;
                    }
                    else
                    {
                        customer.IsServiceProvider = false;
                        if (Request.QueryString["DistID"]!=null)
                        {
                            customer.ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(Request.QueryString["DistID"])); //Convert.ToInt32(ddlSPName.SelectedValue);
                        }
                        else
                        {
                            customer.ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID)); //Convert.ToInt32(ddlSPName.SelectedValue);
                        }
                        
                    }

                    if (!string.IsNullOrEmpty(rblCustomerDistributor.SelectedValue))
                    {
                        if (rblCustomerDistributor.SelectedValue.Trim().ToUpper().Equals("C"))
                        {
                            customer.IsDistributor = false;
                        }
                        else if (rblCustomerDistributor.SelectedValue.Trim().ToUpper().Equals("D"))
                        {
                            customer.IsDistributor = true;
                        }
                    }

                    if (!string.IsNullOrEmpty(ddlDistributor.SelectedValue) && ddlDistributor.SelectedValue != "-1")
                    {
                        customer.ParentID = Convert.ToInt32(ddlDistributor.SelectedValue);
                    }

                    if (!string.IsNullOrEmpty(rblSubDistributor.SelectedValue))
                    {
                        customer.CanCreateSubDist = Convert.ToBoolean(Convert.ToInt32(rblSubDistributor.SelectedValue));                        
                    }

                    if (!string.IsNullOrEmpty(rblPayment.SelectedValue))
                    {
                        customer.IsPayment = Convert.ToBoolean(Convert.ToInt32(rblPayment.SelectedValue));
                    }

                    bool hrComplianceCustomer = false;
                    //ComplianceProductType 
                    //0--Compliance--  
                    //1 -HR Only Compliance(HR Compliance RLCS Generated) 
                    //2 -HR+Compliance(HR Compliance RLCS Generated)
                    if (ddlComplianceProductType.SelectedValue != "" || !string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                    {
                        customer.ComplianceProductType = Convert.ToInt32(ddlComplianceProductType.SelectedValue);
                        if (customer.ComplianceProductType > 0)
                        {
                            hrComplianceCustomer = true;
                        }
                    }
                    else
                        customer.ComplianceProductType = 0;

                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer mstCustomer = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer();
                    ///Hr Product Entry 22APRIL2021                  
                    customer.CreatedBy = AuthenticationHelper.UserID;
                    customer.CreatedFrom = 9;
                    //END
                    mstCustomer.Name = tbxName.Text;
                    mstCustomer.Address = tbxAddress.Text;
                    mstCustomer.BuyerName = tbxBuyerName.Text;
                    mstCustomer.BuyerContactNumber = tbxBuyerContactNo.Text;
                    mstCustomer.BuyerEmail = tbxBuyerEmail.Text;

                    if (strtdate != "" && enddate != "")
                    {
                        mstCustomer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        mstCustomer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if ((int)ViewState["Mode"] == 1)
                    {
                        mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                    }

                    if (CustomerManagementRisk.Exists(mstCustomer))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    mstCustomer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                    mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                    mstCustomer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                    mstCustomer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);

                    //Service Provider
                    mstCustomer.IsServiceProvider = customer.IsServiceProvider;
                    mstCustomer.ServiceProviderID = customer.ServiceProviderID;

                    //Distributor
                    mstCustomer.IsDistributor = customer.IsDistributor;
                    mstCustomer.ParentID = customer.ParentID;
                    mstCustomer.CanCreateSubDist = customer.CanCreateSubDist;
                    mstCustomer.IsPayment = customer.IsPayment;

                    //ComplianceProductType
                    mstCustomer.ComplianceProductType = customer.ComplianceProductType;

                    if ((int)ViewState["Mode"] == 0)
                    {
                        bool resultDataChk = false;
                        int resultData = 0;

                        resultData = CustomerManagement.Create(customer);
                        if (resultData > 0)
                        {
                            resultDataChk = CustomerManagementRisk.Create(mstCustomer);
                            if (resultDataChk == false)
                            {
                                CustomerManagement.deleteCustReset(resultData);
                            }
                            else
                            {
                                avacom_CustomerID = resultData;
                                saveSuccess = true;
                            }
                        }

                        try
                        {                            
                            string ReplyEmailAddressName = "Avantis";
                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                                               .Replace("@NewCustomer", customer.Name)
                                               .Replace("@LoginUser", AuthenticationHelper.User)
                                               .Replace("@PortalURL", Convert.ToString(portalURL))
                                               .Replace("@From", ReplyEmailAddressName)
                                               .Replace("@URL", Convert.ToString(portalURL));
                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                            //                    .Replace("@NewCustomer", customer.Name)
                            //                    .Replace("@LoginUser", AuthenticationHelper.User)
                            //                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                            //                    .Replace("@From", ReplyEmailAddressName)
                            //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();
                            //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer account created.", message);
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        bool updateDetails = false;
                        if (ViewState["Type"] != null)
                        {
                            if (Convert.ToBoolean(ViewState["Type"])) //Previous Type - Distributor or Customer
                            {
                                if (Convert.ToBoolean(ViewState["Type"]) != customer.IsDistributor)
                                {
                                    int distCustCount = RLCS_Master_Management.CheckDistributorsCustomerCount(customer.ID);
                                    if (distCustCount > 0)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "This Professional Firm(Distributor) has created/assigned to -" + distCustCount + "- Customers, so can not able to update Type";

                                        updateDetails = false;
                                    }
                                    else
                                        updateDetails = true;
                                }
                                else
                                    updateDetails = true;
                            }
                            else
                                updateDetails = true;
                        }
                        else
                            updateDetails = true;

                        if (updateDetails)
                        {
                            CustomerManagement.Update(customer);
                            CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016
                            saveSuccess = true;
                        }
                    }

                    //Product Mapping by Vishal
                    if (saveSuccess)
                    {
                        if (lstBoxProduct.Items.Count > 0)
                        {
                            Business.ComplianceManagement.UpdateProductMapping(Convert.ToInt64(avacom_CustomerID));
                            foreach (ListItem eachProduct in lstBoxProduct.Items)
                            {
                                if (eachProduct.Selected)
                                {
                                    if (Convert.ToInt32(eachProduct.Value) != 0)
                                    {
                                        Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                        {
                                            CustomerID = mstCustomer.ID,
                                            ProductID = Convert.ToInt32(eachProduct.Value),
                                            IsActive = false,
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = AuthenticationHelper.UserID
                                        };

                                        if (productmapping.ProductID == 3 || productmapping.ProductID == 4)
                                        {
                                            //add if audit product
                                            //ProcessManagement.CreateProcessSubprocessPredefined(avacom_CustomerID, AuthenticationHelper.UserID);
                                        }

                                        Business.ComplianceManagement.CreateProductMapping(productmapping);
                                    }
                                }
                            }
                        }
                    }

                    //Create or Update Customer/Corporate in RLCS DB
                    if (hrComplianceCustomer && saveSuccess && avacom_CustomerID != 0)
                    {
                        RLCS_Customer_Corporate_Mapping newRecord = new RLCS_Customer_Corporate_Mapping()
                        {
                            AVACOM_CustomerID = avacom_CustomerID,
                            CO_Name = customer.Name,
                            CO_IsAventisCorporate=customer.ComplianceProductType,
                            CO_Version = 1,
                        };

                        newRecord.CO_CorporateID = txtCorpID.Text.Trim();

                        if (!string.IsNullOrEmpty(txtPAN.Text.Trim()))
                            newRecord.CO_PAN = txtPAN.Text.Trim().ToUpper();

                        if (!string.IsNullOrEmpty(txtTAN.Text.Trim()))
                            newRecord.CO_TAN = txtTAN.Text.Trim().ToUpper();

                        if (ddlCountry.SelectedValue != "" && ddlCountry.SelectedValue != "-1")
                        {
                            newRecord.CO_Country = ddlCountry.SelectedValue;
                        }

                        if (ddlState.SelectedValue != "" && ddlState.SelectedValue != "-1")
                        {
                            newRecord.CO_State = ddlState.SelectedValue;
                        }

                        if (ddlCity.SelectedValue != "" && ddlCity.SelectedValue != "-1")
                        {
                            newRecord.CO_City = ddlCity.SelectedItem.Text;
                            newRecord.CO_Pincode = ddlCity.SelectedValue;
                        }

                        if (ddlCustomerStatus.SelectedValue != "")
                        {
                            if (ddlCustomerStatus.SelectedItem.Text.Trim().ToUpper().Equals("INACTIVE"))
                                newRecord.CO_Status = "I";
                            else if (ddlCustomerStatus.SelectedItem.Text.Trim().ToUpper().Equals("ACTIVE"))
                                newRecord.CO_Status = "A";
                        }

                        saveSuccess = RLCS_Master_Management.CreateUpdate_Customer_CorporateClient_Mapping(newRecord);

                        if (saveSuccess)
                        {
                            Model_CorporateClientMaster corpRecord = new Model_CorporateClientMaster()
                            {
                                CorporateId = newRecord.CO_CorporateID,
                                CorporateName = newRecord.CO_Name,
                                CO_ModifiedBy = "1",
                                CorporateFlag = customer.ComplianceProductType,
                            };

                            string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                            rlcsAPIURL += "AventisIntegration/CreateUpdateCorporateMaster";

                            string responseData = WebAPIUtility.POST_Corporate(rlcsAPIURL, corpRecord);

                            if (!string.IsNullOrEmpty(responseData))
                            {
                                var apiResponse = JsonConvert.DeserializeObject<RLCS_API_Response>(responseData);

                                if (apiResponse != null)
                                {
                                    saveSuccess = Convert.ToBoolean(apiResponse.StatusCode);

                                    if (saveSuccess)
                                    {
                                        RLCS_ClientsManagement.Update_ProcessedStatus_Corporate(avacom_CustomerID, corpRecord.CorporateId, true);
                                    }
                                }
                            }

                            com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping productmapping = new com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping()
                            {
                                CustomerID = avacom_CustomerID,
                                ProductID = 1, //Compliance
                                IsActive = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now
                            };

                            ProductMapping_Risk productmappingrisk = new ProductMapping_Risk()
                            {
                                CustomerID = avacom_CustomerID,
                                ProductID = 1, //Compliance
                                IsActive = false,
                                CreatedBy = 1,
                                CreatedOn = DateTime.Now
                            };

                            RLCS_Master_Management.CreateUpdate_ProductMapping_ComplianceDB(productmapping);
                            RLCS_Master_Management.CreateUpdate_ProductMapping_AuditDB(productmappingrisk);

                            //if (customer.ComplianceProductType == 2 || customer.ComplianceProductType == 4)
                            //{
                            //    Business.Data.ProductMapping HRproductmapping = new Business.Data.ProductMapping()
                            //    {
                            //        CustomerID = avacom_CustomerID,
                            //        ProductID = 9,
                            //        IsActive = false,
                            //        CreatedOn = DateTime.Now,
                            //        CreatedBy = AuthenticationHelper.UserID
                            //    };
                            //    RLCS_Master_Management.CreateUpdate_ProductMapping_ComplianceDB(HRproductmapping);
                            //}
							
                            //UPlOAD  CUSTOMER LOGO
                            if (fuCustLogoUpload.HasFile)
                            {
                                string fileName = Path.GetFileName(fuCustLogoUpload.PostedFile.FileName);
                                string FolderPath = "~/CustomerLogos/" + avacom_CustomerID + "/" + customer.Name;

                                string filepath = FolderPath + "/" + fileName;

                                if (!Directory.Exists(MapPath(FolderPath)))
                                {
                                    Directory.CreateDirectory(Server.MapPath(FolderPath));
                                }

                                if (!File.Exists(MapPath(filepath)))
                                {
                                    fuCustLogoUpload.PostedFile.SaveAs(Server.MapPath(filepath));
                                }

                                var LogoPath = filepath;
                                customer.LogoPath = LogoPath;

                                CustomerManagement.UpdateCustomerPhoto(Convert.ToInt32(avacom_CustomerID), filepath);
                                CustomerManagementRisk.UpdateCustomerPhoto(Convert.ToInt32(avacom_CustomerID), filepath);
                            }
                        }
                    }

                    if (saveSuccess)
                    {
                        EditCustomerInformation(avacom_CustomerID);

                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Details Save Successfully";
                        ValidationSummary1.CssClass = "alert alert-success";
                        //added by tejashri for close popup window after save record..
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                        //added by tejashri for close popup window after save record..
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Required CorporateID";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    ScriptManager.RegisterStartupScript(upCustomers, upCustomers.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rblCustomerDistributor_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(rblCustomerDistributor.SelectedValue))
                {
                    if (rblCustomerDistributor.SelectedValue == "D")
                    {
                        divSubDist.Visible = true;

                        if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT")
                            divPayment.Visible = true;
                        else
                            divPayment.Visible = false;
                    }
                    else if (rblCustomerDistributor.SelectedValue == "C")
                    {
                        divSubDist.Visible = false;
                        divPayment.Visible = false;

                        if (rblSubDistributor.Items.FindByValue("0") != null)
                        {
                            rblSubDistributor.ClearSelection();
                            rblSubDistributor.Items.FindByValue("0").Selected = true;
                        }                            
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplianceProductType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bool showControls = false;

                if (ddlComplianceProductType.SelectedValue != "" || !string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                {
                    if (Convert.ToInt32(ddlComplianceProductType.SelectedValue) > 0)
                    {
                        showControls = true;

                        BindCountry();
                        BindState();

                        if (ViewState["CorpID"] != null)
                        {
                            if (!string.IsNullOrEmpty(ViewState["CorpID"].ToString()))
                            {
                                if (ViewState["Mode"].ToString() == "1")
                                {
                                    lblCheckCorporate.Visible = false;
                                }
                                else
                                {
                                    btnSave.Enabled = false;
                                    lblCheckCorporate.Visible = true;
                                }
                            }
                            else
                                tbxName_TextChanged(sender, e);
                        }
                        else
                        {
                            btnSave.Enabled = false;
                            lblCheckCorporate.Visible = true;
                            tbxName_TextChanged(sender, e);
                        }
                    }
                }

                divCorpID.Visible = showControls;
                divPAN.Visible = showControls;
                divTAN.Visible = showControls;
                divCountry.Visible = showControls;
                divState.Visible = showControls;
                divCity.Visible = showControls;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCountry()
        {
            try
            {
                ddlCountry.Items.Clear();

                ddlCountry.DataTextField = "Name";
                ddlCountry.DataValueField = "ID";

                ddlCountry.DataSource = RLCS_Master_Management.FillCountry();
                ddlCountry.DataBind();

                if (ddlCountry.Items.FindByText("India") != null)
                {
                    ddlCountry.ClearSelection();
                    ddlCountry.Items.FindByText("India").Selected = true;

                    ddlCountry_SelectedIndexChanged(null, null);
                }

                //ddlCountry.Items.Insert(0, new ListItem("<Select Country>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindState();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindState()
        {
            try
            {
                if (ddlCountry.SelectedValue == "001")
                {
                    ddlState.Items.Clear();

                    ddlState.DataTextField = "Name";
                    ddlState.DataValueField = "ID";

                    ddlState.DataSource = RLCS_Master_Management.FillStates();
                    ddlState.DataBind();

                    //ddlState.Items.Insert(0, new ListItem("<Select State>", "-1"));
                }
                else
                {
                    ddlState.Items.Clear();
                    //ddlState.Items.Insert(0, new ListItem("<Select State>", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCity();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCity()
        {
            try
            {
                if (ddlState.SelectedValue != "" || !string.IsNullOrEmpty(ddlState.SelectedValue) || ddlState.SelectedValue != "-1")
                {
                    ddlCity.Items.Clear();

                    ddlCity.DataTextField = "Name";
                    ddlCity.DataValueField = "ID";
                    if (!String.IsNullOrEmpty(ddlState.SelectedValue))
                    {
                        ddlCity.DataSource = RLCS_Master_Management.FillLocationCityByStateID(Convert.ToInt32(ddlState.SelectedValue));

                    }
                    ddlCity.DataBind();

                    //ddlCity.Items.Insert(0, new ListItem("<Select City>", "-1"));
                }
                else
                {
                    ddlCity.Items.Clear();
                    //ddlCity.Items.Insert(0, new ListItem("<Select City>", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0)
                    {
                        txtCorpID.Text = GetCorpID(tbxName.Text.Trim());

                        btnCheckCorpID.Enabled = true;
                        ViewState["CorpID"] = txtCorpID.Text;
                        lblCheckCorporate.Visible = true;
                        btnSave.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string GetCorpID(string corpName)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                foreach (char c in corpName)
                {
                    if (result.Length < 5)
                    {
                        if (Char.IsLetterOrDigit(c))
                            result.Append(c);
                    }
                }

                return "AVA" + result.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                return result.ToString();
            }
        }

        protected void btnCheckCorpID_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCorpID.Text))
                {
                    string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];

                    rlcsAPIURL += "AventisIntegration/CheckCorporateIdExists?CorporateId=" + txtCorpID.Text.Trim();

                    string responseData = WebAPIUtility.Invoke("GET", rlcsAPIURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        bool corpExists = Convert.ToBoolean(responseData.Trim());

                        if (corpExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Corporate with Same CorporateID already exists, Please choose other";
                            btnSave.Enabled = false;
                        }
                        else
                        {
                            corpExists = RLCS_Master_Management.Exists_CorporateID(txtCorpID.Text.Trim());
                            bool clientExists = RLCS_Master_Management.Exists_ClientID(txtCorpID.Text.Trim());
                            if (corpExists)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Corporate with Same CorporateID already exists, Please choose other";
                                btnSave.Enabled = false;
                            }
                            else if (clientExists)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Client with Same CorporateID already exists, Please choose other";
                                btnSave.Enabled = false;
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                lblCheckCorporate.Visible = false;                                

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "CorporateID Available, Please proceed further";
                                ValidationSummary1.CssClass = "alert alert-success";
                            }
                        }

                        upCustomers.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}