﻿using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_GraphDetails : System.Web.UI.Page
    {
        static string tlConnect_url = ConfigurationManager.AppSettings["TLConnect_API_URL"].ToString();
                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string gridToBind = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["Grid"]))
                {
                    gridToBind = Request.QueryString["Grid"];

                    divddlActivityStatus.Visible = false;
                    divddlYear.Visible = false;
                    divddlMonth.Visible = false;

                    if (!string.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        if (ddlActivityStatus.Items.FindByValue(Request.QueryString["Status"]) != null)
                        {
                            divddlActivityStatus.Visible = true;

                            ddlActivityStatus.ClearSelection();
                            ddlActivityStatus.Items.FindByValue(Request.QueryString["Status"]).Selected = true;
                        }
                    }

                    if(!string.IsNullOrEmpty(gridToBind))
                    {
                        if (gridToBind.Trim().ToUpper().Equals("AS"))
                        {
                            BindGrid_ActivityStatus();
                            grdActivityList.Visible = true;
                        }
                        else if(gridToBind.Trim().ToUpper().Equals("LCS"))
                        {
                            BindGrid_LocationWiseComplianceStatus();
                            grdLocationWiseComplianceStatusList.Visible = true;
                        }
                        else if (gridToBind.Trim().ToUpper().Equals("REGISTER")|| gridToBind.Trim().ToUpper().Equals("RETURN") || gridToBind.Trim().ToUpper().Equals("CHALLAN"))
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["Month"]))
                            {
                                divddlMonth.Visible = true;

                                string monthValue = string.Empty;
                                monthValue = Request.QueryString["Month"];

                                if (Convert.ToInt32(monthValue) < 10)
                                    monthValue = "0" + monthValue;

                                if (ddlMonth.Items.FindByValue(monthValue) != null)
                                {
                                    ddlMonth.ClearSelection();
                                    ddlMonth.Items.FindByValue(monthValue).Selected = true;
                                }
                            }

                            if (!string.IsNullOrEmpty(Request.QueryString["Year"]))
                            {
                                divddlYear.Visible = true;
                                
                                if (ddlYear.Items.FindByValue(Request.QueryString["Year"]) != null)
                                {
                                    ddlYear.ClearSelection();
                                    ddlYear.Items.FindByValue(Request.QueryString["Year"]).Selected = true;
                                }
                            }

                            if (gridToBind.Trim().ToUpper().Equals("REGISTER"))
                            {
                                BindGrid_Registers_Return_Challans("REGISTER");
                                grdRegisters.Visible = true;
                            }
                            else if (gridToBind.Trim().ToUpper().Equals("RETURN"))
                            {
                                BindGrid_Registers_Return_Challans("RETURN");
                                grdReturns.Visible = true;
                            }
                            else if (gridToBind.Trim().ToUpper().Equals("CHALLAN"))
                            {
                                BindGrid_Registers_Return_Challans("CHALLAN");
                                grdChallans.Visible = true;
                            }
                        }                       

                        bindPageNumber();
                        ShowGridDetail(); //#FF7473 -Red #FFE26B-Yellow
                    }
                }
            }
        }

        public void BindGrid_ActivityStatus()
        {
            try
            {                   
                string activityStatus = "DT"; //Due Today

                if (!string.IsNullOrEmpty(ddlActivityStatus.SelectedValue))
                {
                    activityStatus = ddlActivityStatus.SelectedValue;
                }

                var lstActivities = GetActivityList(activityStatus, tlConnect_url);

                if (lstActivities.Count > 0)
                {
                    grdActivityList.DataSource = lstActivities;
                    grdActivityList.DataBind();

                    Session["TotalRows"] = lstActivities.Count;
                }
                else
                {
                    grdActivityList.DataSource = null;
                    grdActivityList.DataBind();
                }

                lstActivities.Clear();
                lstActivities = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindGrid_LocationWiseComplianceStatus()
        {
            try
            {
                string clientID = "DUMGD0RG";
                string profileID = "HVZCE4TNUGIZFYX";

                var lstComplianceStatusList = GetLocationWiseComplianceStatusList(clientID, profileID, tlConnect_url);

                if (lstComplianceStatusList.Count > 0)
                {
                    grdLocationWiseComplianceStatusList.DataSource = lstComplianceStatusList;
                    grdLocationWiseComplianceStatusList.DataBind();

                    Session["TotalRows"] = lstComplianceStatusList.Count;
                }
                else
                {
                    grdLocationWiseComplianceStatusList.DataSource = null;
                    grdLocationWiseComplianceStatusList.DataBind();
                }

                lstComplianceStatusList.Clear();
                lstComplianceStatusList = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindGrid_Registers_Return_Challans(string docType)
        {
            try
            {
                string month = string.Empty;
                string year = string.Empty;

                if (!string.IsNullOrEmpty(ddlMonth.SelectedValue))
                {
                    month = ddlMonth.SelectedValue;
                }

                if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
                {
                    year = ddlYear.SelectedValue;
                }

                var lstRegisterReturnChallan = GetRegisterReturnChallanList(month, year, docType, tlConnect_url);

                //if (lstRegisterReturnChallan.Count > 0)
                //{
                    if (docType.Equals("REGISTER"))
                    {
                        grdRegisters.DataSource = lstRegisterReturnChallan;
                        grdRegisters.DataBind();
                    }
                    else if (docType.Equals("CHALLAN"))
                    {
                        grdChallans.DataSource = lstRegisterReturnChallan;
                        grdChallans.DataBind();
                    }
                    else if (docType.Equals("RETURN"))
                    {
                        grdReturns.DataSource = lstRegisterReturnChallan;
                        grdReturns.DataBind();
                    }                

                    Session["TotalRows"] = lstRegisterReturnChallan.Count;
                //}
                //else
                //{
                //    grdRegisters.DataSource = null;
                //    grdRegisters.DataBind();

                //    grdChallans.DataSource = null;
                //    grdChallans.DataBind();

                //    grdReturns.DataSource = null;
                //    grdReturns.DataBind();
                //}                

                lstRegisterReturnChallan.Clear();
                lstRegisterReturnChallan = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvGraphDetail.IsValid = false;
                cvGraphDetail.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public List<ActivityDetails> GetActivityList(string statusParameter, string requestUrl)
        {
            List<ActivityDetails> lstActivities = new List<ActivityDetails>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    requestUrl += "Dashboard/GetClientDashboardDueDetails?ClientId=DDYRG&DetailsFor=" + statusParameter + "&profileID=HVZCE4TNUGIZFYX";

                    string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        lstActivities = JsonConvert.DeserializeObject<List<ActivityDetails>>(responseData);                        
                    }
                }

                return lstActivities;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstActivities;
            }
        }

        public List<LocationWiseComplianceStatus> GetLocationWiseComplianceStatusList(string clientID, string profileID, string requestUrl)
        {
            List<LocationWiseComplianceStatus> lstLocationWiseComplianceStatus = new List<LocationWiseComplianceStatus>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    requestUrl += "Dashboard/GetComplianceClientIDBy_ProfileID?ClientId=" + clientID + "&profileID=" + profileID;

                    string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var _obj = JsonConvert.DeserializeObject<DetailedComplianceStatus>(responseData);

                        if (_obj != null)
                            lstLocationWiseComplianceStatus = _obj.Map;
                    }
                }

                return lstLocationWiseComplianceStatus;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstLocationWiseComplianceStatus;
            }
        }

        public List<RegisterReturnChallanDetail> GetRegisterReturnChallanList(string month, string year, string docType, string requestUrl)
        {
            List<RegisterReturnChallanDetail> lstRegisters = new List<RegisterReturnChallanDetail>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (docType.Equals("REGISTER"))
                        requestUrl += "DownloadRegisters/GetRegisterDetails?ClientId=ALL&month=" + month + "&year=" + year + "&state=All&branch=All&profileID=HVZCE4TNUGIZFYX";
                    else if (docType.Equals("CHALLAN"))
                        requestUrl += "DownloadChallan/GetRegisterDetails?ClientId=ALL&month=" + month + "&year=" + year + "&state=All&branch=All&profileID=HVZCE4TNUGIZFYX";
                    else if (docType.Equals("RETURN"))
                        requestUrl += "DownloadReturns/GetRegisterDetails?ClientId=ALL&month=" + month + "&year=" + year + "&state=All&branch=All&profileID=HVZCE4TNUGIZFYX";

                    string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        lstRegisters = JsonConvert.DeserializeObject<List<RegisterReturnChallanDetail>>(responseData);
                    }
                }

                return lstRegisters;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstRegisters;
            }
        }
        
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                string gridToBind = Request.QueryString["Grid"];

                if (!string.IsNullOrEmpty(gridToBind))
                {
                    if (gridToBind.Trim().ToUpper().Equals("AS"))
                    {
                        PageSize = Convert.ToInt32(grdActivityList.PageSize);
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("LCS"))
                    {
                        PageSize = Convert.ToInt32(grdLocationWiseComplianceStatusList.PageSize);
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("REGISTER"))
                    {
                        PageSize = Convert.ToInt32(grdRegisters.PageSize);
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("RETURN"))
                    {
                        PageSize = Convert.ToInt32(grdReturns.PageSize);
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("CHALLAN"))
                    {
                        PageSize = Convert.ToInt32(grdChallans.PageSize);
                    }

                    var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                    var EndRecord = 0;
                    var TotalRecord = 0;
                    var TotalValue = PageSize * PageNumber;

                    TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                    if (TotalRecord < TotalValue)
                    {
                        EndRecord = TotalRecord;
                    }
                    else
                    {
                        EndRecord = TotalValue;
                    }

                    if (TotalRecord != 0)
                        lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                    else
                        lblStartRecord.Text = "0";

                    lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                    lblTotalRecord.Text = TotalRecord.ToString();
                }
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {      
                int gridindex = 0;
                string gridToBind = Request.QueryString["Grid"];

                if (!string.IsNullOrEmpty(gridToBind))
                {
                    if (gridToBind.Trim().ToUpper().Equals("AS"))
                    {
                        grdActivityList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        gridindex = grdActivityList.PageIndex;
                        BindGrid_ActivityStatus();
                    }
                    else if(gridToBind.Trim().ToUpper().Equals("LCS"))
                    {
                        grdLocationWiseComplianceStatusList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        gridindex = grdLocationWiseComplianceStatusList.PageIndex;
                        BindGrid_LocationWiseComplianceStatus();
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("REGISTER"))
                    {
                        grdRegisters.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        gridindex = grdRegisters.PageIndex;
                        BindGrid_Registers_Return_Challans("REGISTER");
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("RETURN"))
                    {
                        grdReturns.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        gridindex = grdReturns.PageIndex;
                        BindGrid_Registers_Return_Challans("RETURN");
                    }
                    else if (gridToBind.Trim().ToUpper().Equals("CHALLAN"))
                    {
                        grdChallans.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        gridindex = grdChallans.PageIndex;
                        BindGrid_Registers_Return_Challans("CHALLAN");
                    }

                    bindPageNumber();

                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {                        
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }

                    ShowGridDetail();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

            string gridToBind = Request.QueryString["Grid"];

            if (!string.IsNullOrEmpty(gridToBind))
            {
                if (gridToBind.Trim().ToUpper().Equals("AS"))
                {
                    grdActivityList.PageIndex = chkSelectedPage - 1;
                    grdActivityList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                    BindGrid_ActivityStatus();
                }
                else if (gridToBind.Trim().ToUpper().Equals("LCS"))
                {
                    grdLocationWiseComplianceStatusList.PageIndex = chkSelectedPage - 1;
                    grdLocationWiseComplianceStatusList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                    BindGrid_LocationWiseComplianceStatus();
                }
                else if (gridToBind.Trim().ToUpper().Equals("REGISTER"))
                {
                    grdRegisters.PageIndex = chkSelectedPage - 1;
                    grdRegisters.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                    BindGrid_Registers_Return_Challans("REGISTER");
                }
                else if (gridToBind.Trim().ToUpper().Equals("RETURN"))
                {
                    grdReturns.PageIndex = chkSelectedPage - 1;
                    grdReturns.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                    BindGrid_Registers_Return_Challans("RETURN");
                }
                else if (gridToBind.Trim().ToUpper().Equals("CHALLAN"))
                {
                    grdChallans.PageIndex = chkSelectedPage - 1;
                    grdChallans.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                    BindGrid_Registers_Return_Challans("CHALLAN");
                }

                ShowGridDetail();
            }
        }

        public bool ShowHideDownloadButton(string buttonText)
        {
            bool showButton = false;
            try
            {
                if(!string.IsNullOrEmpty(buttonText))
                {
                    if(!buttonText.Trim().ToUpper().Equals("NO DATA"))
                    {
                        showButton= true;
                    }
                }

                return showButton;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return showButton;
            }
        }
    }
}