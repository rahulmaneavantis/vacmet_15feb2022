﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.TLPages;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_DFMSPOCDashboard : System.Web.UI.Page
    {
        protected static DateTime FromFinancialYearSummery;
        protected static DateTime ToFinancialYearSummery;

        protected static int customerID;

        protected static string tlConnect_url = String.Empty;
        protected static string rlcs_API_url;
        protected static string userProfileID;
        protected static string userProfileID_Encrypted;
        protected static string authKey;
        protected static string profileID;
        protected static string userID;
        protected static string Encrypted_userID;
        protected static int ProductType;
        protected static int? serviceproviderid;
        protected string CustomerWiseComplianceStatusChart;
        protected string CustomerWiseComplianceSeries;

        protected string SPOCWiseComplianceStatusChart;
        protected string SPOCWiseComplianceSeries;
        protected static string RLCS_WEB_URL;
        protected static bool IsOneTimeDashboardUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            userID = Convert.ToString(AuthenticationHelper.UserID);
            ProductType = Convert.ToInt32(AuthenticationHelper.ComplianceProductType);
            rlcs_API_url = ConfigurationManager.AppSettings["RLCSAPIURL"].ToString();
            RLCS_WEB_URL = ConfigurationManager.AppSettings["RLCS_WEB_URL"].ToString();
            txtPeriodList.Enabled = false;
            if (!IsPostBack)
            {
                IsOneTimeDashboardUser = CheckIfOneTimeDashboardUser(userID);
                serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                Session["masterpage"] = "HRPlusSPOCMGR";
                try
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        HiddenField home = (HiddenField)Master.FindControl("Ishome");
                        home.Value = "true";

                        int loggedInUserID = Convert.ToInt32((AuthenticationHelper.UserID));

                        //var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID);
                        var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID, AuthenticationHelper.Role);

                        if (custmerlist.Count == 1 && AuthenticationHelper.Role == "HMGR" && custmerlist.FirstOrDefault().IsDistributor == false)
                        {
                            HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                        }
                        else if (custmerlist.Count == 1 && AuthenticationHelper.Role != "DADMN" && AuthenticationHelper.Role != "HMGR")
                        {
                            HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                        }
                        else
                        {
                            var userRecord = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));

                            if (userRecord != null)
                            {
                                if (userRecord.CustomerID != null)
                                {
                                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    if (customerID != userRecord.CustomerID)
                                    {
                                        ProductMappingStructure _obj = new ProductMappingStructure();

                                        //customerID --- User's CustomerID not Selected CustomerID
                                        _obj.ReAuthenticate_User(Convert.ToInt32(userRecord.CustomerID));

                                        Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", true);
                                    }
                                }
                            }

                            string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                            if (!string.IsNullOrEmpty(TLConnectKey))
                            {
                                userProfileID = string.Empty;
                                userProfileID = AuthenticationHelper.ProfileID;
                                if (!string.IsNullOrEmpty(userProfileID))
                                {
                                    userProfileID_Encrypted = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                                    Encrypted_userID = RLCSEncryption.encrypt(userID.Trim());
                                }

                                authKey = AuthenticationHelper.AuthKey;

                                if (AuthenticationHelper.Role != "HMGR")
                                {
                                    liCustomerWise.Visible = false;
                                    liSPOCWise.Visible = false;
                                }
                                else
                                {
                                    liCustomerWise.Visible = true;
                                    liSPOCWise.Visible = true;
                                }

                                lnkShowCustomerWiseDashboard_Click(sender, e);
                            }
                        }
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private bool CheckIfOneTimeDashboardUser(string userID)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var UserList = entities.SP_GetOneTimeUsersCount(Convert.ToInt32(userID));
                    if (UserList.Count() > 0) return true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public void BindUserAssignedCustomers()
        {
            try
            {

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomers(List<SP_RLCS_GetAssignedCustomers_Result> lstMyAssignedCustomers)
        {
            try
            {
                ddlCustomers.Items.Clear();
                ddlCustomers.DataTextField = "CustomerName";
                ddlCustomers.DataValueField = "CustomerID";

                ddlCustomers.DataSource = lstMyAssignedCustomers;
                ddlCustomers.DataBind();

                ddlCustomers.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSPOCs(List<SP_RLCS_GetAssignedSPOC_Result> lstMySPOCs)
        {
            try
            {
                ddlSPOCs.Items.Clear();

                ddlSPOCs.DataTextField = "UserName";
                ddlSPOCs.DataValueField = "UserID";

                ddlSPOCs.DataSource = lstMySPOCs;
                ddlSPOCs.DataBind();

                ddlSPOCs.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (MainView.ActiveViewIndex == 0)
                    LoadDashboardCustomerWise(null);
                else
                    LoadDashboardSPOCWise(null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkShowCustomerWiseDashboard_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 0;
            int loggedInUserID = AuthenticationHelper.UserID;

            int customerID = -1;
            int serviceProviderID = -1;
            int distributorID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "SPADM")
            {
                serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            liCustomerWise.Attributes.Add("class", "active");
            liSPOCWise.Attributes.Add("class", "");

            var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(loggedInUserID, distributorID, AuthenticationHelper.Role);
            //var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID);
            BindCustomers(lstMyAssignedCustomers);

            if (AuthenticationHelper.Role != "HMGR")
            {
                divFilterCustomer.Visible = true;
                divFilterSPOC.Visible = false;
                //ddlSPOCs.Visible = false;
            }
            else
            {
                divFilterCustomer.Visible = false;
                divFilterSPOC.Visible = true;
                //ddlSPOCs.Visible = true;

                var lstMyAssignedSPOCs = UserCustomerMappingManagement.Get_AssignedSPOC(loggedInUserID, distributorID, AuthenticationHelper.Role);
                BindSPOCs(lstMyAssignedSPOCs);
            }

            LoadDashboardCustomerWise(lstMyAssignedCustomers);
        }

        protected void lnkSPOCWiseDashboard_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 1;
            int loggedInUserID = AuthenticationHelper.UserID;

            int customerID = -1;
            int serviceProviderID = -1;
            int distributorID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "SPADM")
            {
                serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            liCustomerWise.Attributes.Add("class", "");
            liSPOCWise.Attributes.Add("class", "active");

            divFilterCustomer.Visible = false;
            //divFilterSPOC.Visible = true;
            ddlSPOCs.Visible = true;

            var lstMyAssignedSPOCs = UserCustomerMappingManagement.Get_AssignedSPOC(loggedInUserID, distributorID, AuthenticationHelper.Role);
            BindSPOCs(lstMyAssignedSPOCs);

            LoadDashboardSPOCWise(null);
        }



        protected void SetTopBoxesCounts(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList, List<SP_RLCS_GetAssignedCustomers_Result> custmerlist) /*UserAllCustomer*/
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (custmerlist.Count > 0)
                    {
                        divCustomerCount.InnerText = Convert.ToString(custmerlist.Count);
                    }
                    else
                    {
                        divCustomerCount.InnerText = "0";
                    }

                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                    if (!string.IsNullOrEmpty(TLConnectKey))
                    {
                        var userPID = AuthenticationHelper.ProfileID;
                        entities.Database.CommandTimeout = 180;
                        divLocationCount.InnerText = Convert.ToString(MastertransactionsQueryList.Select(row => row.CustomerBranchID).Distinct().Count());

                        //var userAssignedBranchList = (entities.SP_RLCS_GetAssignedBranch(AuthenticationHelper.UserID, userPID)).ToList();
                        //if (userAssignedBranchList.Count > 0)
                        //{
                        //    divLocationCount.InnerText = Convert.ToString(userAssignedBranchList.Count);
                        //}
                        //else
                        //{
                        //    divLocationCount.InnerText = "0";
                        //}
                    }
                    else
                    {
                        divLocationCount.InnerText = "0";
                    }

                    if (MastertransactionsQueryList.Count > 0)
                    {
                        //Upcoming                                     
                        divUpcomingCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "Upcoming").Count());

                        //DueToday                                     
                        divOverdueCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "Overdue").Count());

                        //Review                                     
                        divPendingForReviewCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "PendingForReview").Count());

                        //Action
                        divPendingActionCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "PendingForAction").Count());
                    }
                    else
                    {
                        divUpcomingCount.InnerText = "0";
                        divOverdueCount.InnerText = "0";
                        divPendingForReviewCount.InnerText = "0";
                        divPendingActionCount.InnerText = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Customer Wise

        private void LoadDashboardCustomerWise(List<SP_RLCS_GetAssignedCustomers_Result> lstMyAssignedCustomers)
        {
            int customerID = -1;
            int serviceProviderID = -1;
            int distributorID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "SPADM")
            {
                serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            List<int> lstSelectedCustomers = new List<int>();

            if (lstMyAssignedCustomers == null)
                lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);
            //lstMyAssignedCustomers = UserCustomerMappingManagement.Get_UserCustomerMapping(AuthenticationHelper.UserID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MastertransactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();
                if (customerID == 94)
                {
                    var CustomersList = (from row in entities.Customers
                                         join row1 in entities.UserCustomerMappings
                                         on row.ID equals row1.CustomerID
                                         where row1.UserID == AuthenticationHelper.UserID
                                         select row).ToList();

                   

                    if (CustomersList != null)
                    {
                        MastertransactionsQuery = CustomersList.Select(a => new SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result { CustomerID = Convert.ToInt32(a.ID), CustomerName = a.Name, ActID = 1, ComplianceID = 1, ComplianceInstanceID = 1, ComplianceStatusID = 1, ScheduledOnID = "1", RoleID = 3, UserID = AuthenticationHelper.UserID }).ToList();
                    }
                }
                else
                {
                    MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, AuthenticationHelper.Role)
                                       .Where(entry => entry.IsActive == true
                                           && entry.IsUpcomingNotDeleted == true
                                           && lstMyAssignedCustomers.Select(row => row.CustomerID).Contains(entry.CustomerID))).ToList();
                }



                BindYears(MastertransactionsQuery);

                SetTopBoxesCounts(MastertransactionsQuery, lstMyAssignedCustomers);

                if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && ddlCustomers.SelectedValue != "-1")
                {
                    lstSelectedCustomers.Add(Convert.ToInt32(ddlCustomers.SelectedValue));
                }

                if (lstSelectedCustomers.Count > 0)
                {
                    lstMyAssignedCustomers = lstMyAssignedCustomers.Where(row => lstSelectedCustomers.Contains(row.CustomerID)).ToList();
                }

                MastertransactionsQuery = GetFilteredData(MastertransactionsQuery);

                GetComplianceStatusCount_CustomerWise(MastertransactionsQuery, lstMyAssignedCustomers);
            }
        }

        public void GetComplianceStatusCount_CustomerWise(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MasterRecords, List<SP_RLCS_GetAssignedCustomers_Result> custmerlist)
        {
            try
            {
                CustomerWiseComplianceStatusChart = String.Empty;

                string strListofCustomers = String.Empty;
                string newDivName = String.Empty;
                string newTopDivName = String.Empty;

                List<Tuple<String, String>> GraphName = new List<Tuple<String, String>>();
                GraphName.Add(new Tuple<String, String>("REG", "Registers"));
                GraphName.Add(new Tuple<String, String>("RET", "Returns"));
                GraphName.Add(new Tuple<String, String>("CHA", "Challans"));

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (custmerlist.Count > 0)
                    {
                        List<long> lstComplianceIDs = new List<long>();

                        var registerComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                                     where row.AVACOM_ComplianceID != null
                                                     select (long)row.AVACOM_ComplianceID).ToList();

                        var returnComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                                   where row.AVACOM_ComplianceID != null
                                                   select (long)row.AVACOM_ComplianceID).ToList();

                        var challanComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                                    where row.AVACOM_ComplianceID != null
                                                    select (long)row.AVACOM_ComplianceID).ToList();

                        //var masterRegistersRecords = (from row in MasterRecords
                        //                              join row1 in entities.RLCS_Register_Compliance_Mapping
                        //                              on row.ComplianceID equals row1.AVACOM_ComplianceID
                        //                              select row).ToList();

                        //var masterReturnsRecords = (from row in MasterRecords
                        //                            join row1 in entities.RLCS_Returns_Compliance_Mapping
                        //                            on row.ComplianceID equals row1.AVACOM_ComplianceID
                        //                            select row).ToList();

                        //var masterChallansRecords = (from row in MasterRecords
                        //                             join row1 in entities.RLCS_Challan_Compliance_Mapping
                        //                             on row.ComplianceID equals row1.AVACOM_ComplianceID
                        //                             select row).ToList();

                        long UpcomingCount = 0;
                        long OverdueCount = 0;
                        long PendingForReviewCount = 0;

                        long totalcount = 0;
                        DateTime now = DateTime.Now.Date;
                        DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                        int Count = 1;
                        StringBuilder divstr = new StringBuilder();

                        string selectedFreq = ddlFrequency.SelectedValue;
                        string selectedPeriod = string.Join(",", GetSelectedPeriod());
                        string selectedYear = ddlYear.SelectedValue;

                        List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> customerWiseRecords = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

                        custmerlist.ForEach(EachCustomer =>
                        {
                            customerWiseRecords = (from row in MasterRecords
                                                   where row.CustomerID == EachCustomer.CustomerID
                                                   select row).ToList();

                            if (customerWiseRecords.Count > 0)
                            {
                                UpcomingCount = 0;
                                OverdueCount = 0;
                                PendingForReviewCount = 0;

                                UpcomingCount = (from row in customerWiseRecords
                                                 where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                OverdueCount = (from row in customerWiseRecords
                                                where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                                 && row.PerformerScheduledOn < now
                                                select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                PendingForReviewCount = (from row in customerWiseRecords
                                                         where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                totalcount = UpcomingCount + OverdueCount + PendingForReviewCount;

                                if (totalcount != 0)
                                {
                                    strListofCustomers += "'" + EachCustomer.CustomerName + "',";
                                    newDivName = "divGraphCustomerID_" + EachCustomer.CustomerID;

                                    divstr.Append(@"<div class='widget col-md-12 colpadding0' style='width: 98%;'>" +
                                                    "<h3><b>" + EachCustomer.CustomerName + "</b>" +
                                                    "<span class='k-icon k-icon-15 closeButton' id='" + EachCustomer.CustomerID + "'><i class='fa fa-angle-down' style='color:black;'></i></span>" +
                                                    "<span class='k-icon k-i-window-maximize k-icon-15 maxButton' data-toggle='tooltip' data-placement='bottom' title='Assigned Compliance'  onclick=" + String.Format("ShowMyCompliances('All','-1','{0}','-1','{1}','{2}','{3}','{4}','{5}')", EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "></span>" +
                                                 "</h3>");

                                    divstr.Append(@"<div id='ContentPlaceHolder1_divContainerCustomer_" + EachCustomer.CustomerID + "' class='col-md-12 colpadding0 customerDetails'>" +
                                                   "<div class='row' style='width: 98%; display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */'>" +
                                                      "<div class='col-lg-1 col-md-1 col-sm-12 col-xs-12 colpadding0' style='width: 10%;'>" +
                                                           "<div id='" + newDivName + "' class='col-md-12 colpadding0' runat='server'>" +
                                                          "</div>" +
                                                      "</div>");

                                    string chartString = "var container_" + newDivName + "=Highcharts.chart('" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',height: 100}," +
                                       "title: {text: '',style: { fontWeight: '300',fontSize: '12px'}},legend: { itemDistance:0, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                       "plotOptions: {pie: {size: '150%', showInLegend: false, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 5," +
                                       "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                                       //"series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>'},data: [{name: 'Upcoming',y: " + UpcomingCount + ",color: '#008000',drilldown: 'pass',},{name: 'Overdue',y: " + OverdueCount + ",color: '#FF0000',drilldown: 'fail',},{name: 'PendingForReview',y: " + PendingForReviewCount + ",color:'#0000FF',drilldown: 'nottested',}],}]});";
                                       "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>'},data: [{name: 'Upcoming',y: " + UpcomingCount + ",color: '#1FD7E1', " +
                                       "events:{click: function(e) {" + String.Format("ShowMyCompliances('All','U','{0}','-1','{1}','{2}','{3}','{4}','{5}')", EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "},},}," +
                                       "{name: 'Overdue',y: " + OverdueCount + ",color: '#FF7473'," +
                                       "events:{click: function(e) {" + String.Format("ShowMyCompliances('All','O','{0}','-1','{1}','{2}','{3}','{4}','{5}')", EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "},},}," +
                                       "{name: 'PendingForReview',y: " + PendingForReviewCount + ",color:'#FFC952'," +
                                       "events:{click: function(e) {" + String.Format("ShowMyCompliances('All','PR','{0}','-1','{1}','{2}','{3}','{4}','{5}')", EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "},},}],}]});";

                                    //CustomerWiseComplianceStatusChart += CustomerWiseComplianceSeries;
                                    CustomerWiseComplianceStatusChart += chartString;

                                    divstr.Append(@"<div class='col-lg-1 col-md-1 col-sm-12 col-xs-12 colpadding0' style='width: 88%;'>");

                                    List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> filteredRecords = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

                                    GraphName.ForEach(eachType =>
                                    {
                                        if (eachType.Item1 == "REG")
                                        {
                                            filteredRecords = (from row in customerWiseRecords
                                                               where registerComplianceIDs.Contains(row.ComplianceID)
                                                               select row).ToList();
                                        }
                                        else if (eachType.Item1 == "RET")
                                        {
                                            filteredRecords = (from row in customerWiseRecords
                                                               where returnComplianceIDs.Contains(row.ComplianceID)
                                                               select row).ToList();
                                        }
                                        else if (eachType.Item1 == "CHA")
                                        {
                                            filteredRecords = (from row in customerWiseRecords
                                                               where challanComplianceIDs.Contains(row.ComplianceID)
                                                               select row).ToList();
                                        }

                                        UpcomingCount = 0;
                                        OverdueCount = 0;
                                        PendingForReviewCount = 0;

                                        UpcomingCount = (from row in filteredRecords
                                                         where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                        OverdueCount = (from row in filteredRecords
                                                        where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                                         && row.PerformerScheduledOn < now
                                                        select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                        PendingForReviewCount = (from row in filteredRecords
                                                                 where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();


                                        StringBuilder divUpcomingCountstr = new StringBuilder();
                                        StringBuilder divOverdueCountstr = new StringBuilder();
                                        StringBuilder divPendingForReviewCountstr = new StringBuilder();
                                        if (UpcomingCount == 0)
                                        {
                                            divUpcomingCountstr.Append("<div class='count' style='cursor: default' onclick=" + String.Format("ShowMyCompliances('{0}','U','{1}','-1','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + UpcomingCount + "</div>" + "<div class='desc' style='cursor: default'>Upcoming</div>");
                                        }
                                        else
                                        {
                                            divUpcomingCountstr.Append("<div class='count' onclick=" + String.Format("ShowMyCompliances('{0}','U','{1}','-1','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + UpcomingCount + "</div>" + "<div class='desc'>Upcoming</div>");
                                        }
                                        if (OverdueCount == 0)
                                        {
                                            divOverdueCountstr.Append("<div class='count' style='cursor: default' onclick=" + String.Format("ShowMyCompliances('{0}','O','{1}','-1','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + OverdueCount + "</div>" + "<div class='desc' style='cursor: default'>Overdue</div>");
                                        }
                                        else
                                        {
                                            divOverdueCountstr.Append("<div class='count' onclick=" + String.Format("ShowMyCompliances('{0}','O','{1}','-1','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + OverdueCount + "</div>" + "<div class='desc'>Overdue</div>");
                                        }
                                        if (PendingForReviewCount == 0)
                                        {
                                            divPendingForReviewCountstr.Append("<div class='count' style='cursor: default' onclick=" + String.Format("ShowMyCompliances('{0}','PR','{1}','-1','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + PendingForReviewCount + "</div>" + "<div class='desc' style='cursor: default'>Pending Review</div>");
                                        }
                                        else
                                        {
                                            divPendingForReviewCountstr.Append("<div class='count' onclick=" + String.Format("ShowMyCompliances('{0}','PR','{1}','-1','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachCustomer.CustomerID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + PendingForReviewCount + "</div>" + "<div class='desc'>Pending Review</div>");
                                        }

                                        divstr.Append(@"<div class='col-lg-4 col-md-4 col-sm-12 col-xs-12 colpadding0'>" +
                                                                                                     "<div class='info-box white-bg' style='height: 100px !important;'>" +
                                                                                                         "<div class='title'>" + eachType.Item2 + "</div>" +
                                                                                                         "<div class='col-md-4 colpadding0 borderright'>" +
                                                                                                      " " + divUpcomingCountstr + "" +
                                                                                                         "</div>" +
                                                                                                        "<div class='col-md-4 colpadding0 borderright'>" +
                                                                                                         "" + divOverdueCountstr + "" +
                                                                                                         "</div>" +
                                                                                                        "<div class='col-md-4 colpadding0'>" +
                                                                                                        "" + divPendingForReviewCountstr + "" +
                                                                                                         "</div>" +
                                                                                                     "</div>" +
                                                                                                 "</div>");


                                    });

                                    divstr.Append(@"</div>");

                                    if (RLCS_ComplianceManagement.CheckComplianceAssignment(EachCustomer.CustomerID, AuthenticationHelper.UserID))
                                    {
                                        divstr.Append(@"<div class='col-lg-1 col-md-1 col-sm-12 col-xs-12 colpadding0 text-center' style='width: 2%; align-items: center; display: flex;'>" +
                                            "<div>" +
                                                "<a onclick='return CallSelectIndexEvent(" + EachCustomer.CustomerID + ")'>" +
                                                    "<img src='../Images/proceed.png' alt='Proceed' data-toggle='tooltip' data-placement='bottom' title='Proceed' /></a>" +
                                            "</div>" +
                                        "</div>");
                                    }
                                    else
                                    {
                                        divstr.Append(@"<div class='col-lg-1 col-md-1 col-sm-12 col-xs-12 colpadding0 text-center' style='width: 2%; align-items: center; display: flex;'>" +
                                            "<div>" +
                                            "</div>" +
                                        "</div>");
                                    }

                                    divstr.Append(@"</div></div></div>");
                                    divstr.Append(@"<div class='clearfix'></div>");
                                }
                            }
                            Count++;
                        });

                        divContainer_CustomerWiseSummary.InnerHtml = divstr.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region SPOC Wise

        private void LoadDashboardSPOCWise(List<SP_RLCS_GetAssignedSPOC_Result> lstMyAssignedSPOCs)
        {
            int customerID = -1;
            int serviceProviderID = -1;
            int distributorID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "SPADM")
            {
                serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            string selectedMonth = string.Empty;
            string selectedYear = string.Empty;

            if (lstMyAssignedSPOCs == null)
                lstMyAssignedSPOCs = UserCustomerMappingManagement.Get_AssignedSPOC(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                //var MastertransactionsQuery = (entities.SP_RLCS_UserAllCustomerComplianceInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)
                //.Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true)).ToList();

                var MastertransactionsQuery = (entities.SP_RLCS_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, customerID, distributorID, serviceProviderID, AuthenticationHelper.Role)
                                        .Where(entry => entry.IsActive == true
                                            && entry.IsUpcomingNotDeleted == true)).ToList();

                MastertransactionsQuery = GetFilteredData(MastertransactionsQuery);

                GetComplianceStatusCount_SPOCWise(MastertransactionsQuery, lstMyAssignedSPOCs);
            }
        }

        public void GetComplianceStatusCount_SPOCWise(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> masterRecords, List<SP_RLCS_GetAssignedSPOC_Result> spoclist)
        {
            try
            {
                SPOCWiseComplianceStatusChart = String.Empty;

                string strListofCustomers = String.Empty;
                string newDivName = String.Empty;
                string newTopDivName = String.Empty;

                List<Tuple<String, String>> GraphName = new List<Tuple<String, String>>();
                GraphName.Add(new Tuple<String, String>("REG", "Registers"));
                GraphName.Add(new Tuple<String, String>("RET", "Returns"));
                GraphName.Add(new Tuple<String, String>("CHA", "Challans"));

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (spoclist.Count > 0)
                    {
                        List<long> lstComplianceIDs = new List<long>();
                        var registerComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                                     where row.AVACOM_ComplianceID != null
                                                     select (long)row.AVACOM_ComplianceID).ToList();

                        var returnComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                                   where row.AVACOM_ComplianceID != null
                                                   select (long)row.AVACOM_ComplianceID).ToList();

                        var challanComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                                    where row.AVACOM_ComplianceID != null
                                                    select (long)row.AVACOM_ComplianceID).ToList();

                        long UpcomingCount = 0;
                        long OverdueCount = 0;
                        long PendingForReviewCount = 0;

                        long totalcount = 0;
                        DateTime now = DateTime.Now.Date;
                        DateTime nextOneMonth = DateTime.Now.AddMonths(1);

                        string selectedFreq = ddlFrequency.SelectedValue;
                        string selectedPeriod = string.Join(",", GetSelectedPeriod());
                        string selectedYear = ddlYear.SelectedValue;

                        int Count = 1;
                        StringBuilder divstr = new StringBuilder();

                        List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> spocWiseRecords = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

                        spoclist.ForEach(EachSPOC =>
                        {
                            spocWiseRecords = (from row in masterRecords
                                               where row.UserID == EachSPOC.UserID
                                               select row).ToList();

                            if (spocWiseRecords.Count > 0)
                            {
                                UpcomingCount = 0;
                                OverdueCount = 0;
                                PendingForReviewCount = 0;

                                UpcomingCount = (from row in spocWiseRecords
                                                 where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                                 && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                OverdueCount = (from row in spocWiseRecords
                                                where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                                 && row.PerformerScheduledOn < now
                                                select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                PendingForReviewCount = (from row in spocWiseRecords
                                                         where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                totalcount = UpcomingCount + OverdueCount + PendingForReviewCount;

                                if (totalcount != 0)
                                {
                                    strListofCustomers += "'" + EachSPOC.UserName + "',";
                                    newDivName = "divGraphSPOCID_" + EachSPOC.UserID;

                                    divstr.Append(@"<div class='widget col-md-12 colpadding0' style='width: 98%;'>" +
                                                    "<h3><b>" + EachSPOC.UserName + "</b>" +
                                                    "<span class='k-icon k-i-close k-icon-15 closeButton'></span>" +
                                                    "<span class='k-icon k-i-window-maximize k-icon-15 maxButton' onclick='" + String.Format("return ShowMyCompliances('All','-1','-1','{0}','{1}','{2}','{3}','{4}','{5}')", EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "' data-role='tooltip'></span>" +
                                                 "</h3>");

                                    divstr.Append(@"<div id='ContentPlaceHolder1_divContainerSPOC_" + EachSPOC.UserID + "' class='col-md-12 colpadding0'>" +
                                                   "<div class='row' style='width: 98%; display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */'>" +
                                                      "<div class='col-lg-1 col-md-1 col-sm-12 col-xs-12 colpadding0' style='width: 10%;'>" +
                                                           "<div id='" + newDivName + "' class='col-md-12 colpadding0' runat='server'>" +
                                                          "</div>" +
                                                      "</div>");

                                    string chartString = "var container_" + newDivName + "=Highcharts.chart('" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',height: 100}," +
                                       "title: {text: '',style: { fontWeight: '300',fontSize: '12px'}},legend: { itemDistance:0, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                       "plotOptions: {pie: {size: '150%', showInLegend: false, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 5," +
                                       "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                                       //"series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>'},data: [{name: 'Upcoming',y: " + UpcomingCount + ",color: '#008000',drilldown: 'pass',},{name: 'Overdue',y: " + OverdueCount + ",color: '#FF0000',drilldown: 'fail',},{name: 'PendingForReview',y: " + PendingForReviewCount + ",color:'#0000FF',drilldown: 'nottested',}],}]});";
                                       "series: [{name: 'Status',tooltip:{pointFormat: '{series.name}: <b>{point.y}</b>'},data: [{name: 'Upcoming',y: " + UpcomingCount + ",color: '#1FD7E1', " +
                                       "events:{click: function(e) {" + String.Format("ShowMyCompliances('All','U','-1','{0}','{1}','{2}','{3}','{4}','{5}')", EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "},},}," +
                                       "{name: 'Overdue',y: " + OverdueCount + ",color: '#FF7473'," +
                                       "events:{click: function(e) {" + String.Format("ShowMyCompliances('All','O','-1','{0}','{1}','{2}','{3}','{4}','{5}')", EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "},},}," +
                                       "{name: 'PendingForReview',y: " + PendingForReviewCount + ",color:'#FFC952'," +
                                       "events:{click: function(e) {" + String.Format("ShowMyCompliances('All','PR','-1','{0}','{1}','{2}','{3}','{4}','{5}')", EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + "},},}],}]});";

                                    //CustomerWiseComplianceStatusChart += CustomerWiseComplianceSeries;
                                    SPOCWiseComplianceStatusChart += chartString;

                                    divstr.Append(@"<div class='col-lg-1 col-md-1 col-sm-12 col-xs-12 colpadding0' style='width: 88%;'>");

                                    List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> filteredRecords = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

                                    GraphName.ForEach(eachType =>
                                    {
                                        if (eachType.Item1 == "REG")
                                        {
                                            filteredRecords = (from row in spocWiseRecords
                                                               where registerComplianceIDs.Contains(row.ComplianceID)
                                                               select row).ToList();
                                        }
                                        else if (eachType.Item1 == "RET")
                                        {
                                            filteredRecords = (from row in spocWiseRecords
                                                               where returnComplianceIDs.Contains(row.ComplianceID)
                                                               select row).ToList();
                                        }
                                        else if (eachType.Item1 == "CHA")
                                        {
                                            filteredRecords = (from row in spocWiseRecords
                                                               where challanComplianceIDs.Contains(row.ComplianceID)
                                                               select row).ToList();
                                        }

                                        UpcomingCount = 0;
                                        OverdueCount = 0;
                                        PendingForReviewCount = 0;

                                        UpcomingCount = (from row in filteredRecords
                                                         where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                        OverdueCount = (from row in filteredRecords
                                                        where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                                         && row.PerformerScheduledOn < now
                                                        select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                        PendingForReviewCount = (from row in filteredRecords
                                                                 where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                                                 select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList().Count();

                                        divstr.Append(@"<div class='col-lg-4 col-md-4 col-sm-12 col-xs-12 colpadding0'>" +
                                                         "<div class='info-box white-bg' style='height: 100px !important;'>" +
                                                             "<div class='title'>" + eachType.Item2 + "</div>" +
                                                             "<div class='col-md-4 colpadding0 borderright'>" +
                                                                 "<div class='count' onclick=" + String.Format("ShowMyCompliances('{0}','U','-1','{1}','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + UpcomingCount + "</div>" +
                                                                 "<div class='desc'>Upcoming</div>" +
                                                             "</div>" +

                                                             "<div class='col-md-4 colpadding0 borderright'>" +
                                                             "<div class='count' onclick=" + String.Format("ShowMyCompliances('{0}','O','-1','{1}','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + OverdueCount + "</div>" +
                                                                 "<div class='desc'>Overdue</div>" +
                                                             "</div>" +

                                                             "<div class='col-md-4 colpadding0'>" +
                                                             "<div class='count' onclick=" + String.Format("ShowMyCompliances('{0}','PR','-1','{1}','{2}','{3}','{4}','{5}','{6}')", eachType.Item1, EachSPOC.UserID, selectedFreq, selectedPeriod, selectedYear, txtStartPeriod.Text, txtEndPeriod.Text) + ">" + PendingForReviewCount + "</div>" +
                                                                 "<div class='desc'>Pending Review</div>" +
                                                             "</div>" +
                                                         "</div>" +
                                                     "</div>");
                                    });

                                    divstr.Append(@"</div>");

                                    divstr.Append(@"</div></div></div>");
                                    divstr.Append(@"<div class='clearfix'></div>");
                                }
                            }
                            Count++;
                        });

                        divContainer_SPOCWiseSummary.InnerHtml = divstr.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardData_ComplianceDisplayCount(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, string filter)
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

            if (!string.IsNullOrEmpty(filter))
            {
                DateTime now = DateTime.Now.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                DateTime nextTenDays = DateTime.Now.AddDays(10);

                switch (filter)
                {
                    case "Upcoming":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Overdue":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                              && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case "PendingForReview":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case "PendingForAction":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 20)
                                             select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();

                        break;

                    case "DueToday":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn == now
                                             select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "HighRisk":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn < nextTenDays
                                             select row).ToList().GroupBy(entity => new { entity.ComplianceSchedules, entity.ScheduledOnID }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
            }

            return transactionsQuery.ToList();
        }

        public List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> GetFilteredData(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery)
        {
            List<int> lstSelectedCustomers = new List<int>();
            if (!string.IsNullOrEmpty(ddlCustomers.SelectedValue) && ddlCustomers.SelectedValue != "-1")
            {
                lstSelectedCustomers.Add(Convert.ToInt32(ddlCustomers.SelectedValue));
            }

            if (lstSelectedCustomers.Count > 0)
            {
                MastertransactionsQuery = MastertransactionsQuery.Where(row => lstSelectedCustomers.Contains(row.CustomerID)).ToList();
            }

            List<long> lstSelectedSPOCs = new List<long>();
            if (!string.IsNullOrEmpty(ddlSPOCs.SelectedValue) && ddlSPOCs.SelectedValue != "-1")
            {
                lstSelectedSPOCs.Add(Convert.ToInt32(ddlSPOCs.SelectedValue));
            }

            if (lstSelectedSPOCs.Count > 0)
            {
                MastertransactionsQuery = MastertransactionsQuery.Where(row => lstSelectedSPOCs.Contains(row.UserID)).ToList();
            }

            #region Date Period Filter
            try
            {
                DateTime? startDate = null;
                DateTime? endDate = null;

                if (!string.IsNullOrEmpty(txtStartPeriod.Text) && !string.IsNullOrEmpty(txtEndPeriod.Text))
                {
                    string[] startPeriod = txtStartPeriod.Text.Split('-');
                    if (startPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(startPeriod[1]);

                            startDate = new DateTime(year, month, 1).Date;
                        }
                    }

                    string[] endPeriod = txtEndPeriod.Text.Split('-');
                    if (endPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(endPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(endPeriod[1]);

                            endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(txtStartPeriod.Text) && string.IsNullOrEmpty(txtEndPeriod.Text))
                {
                    string[] startPeriod = txtStartPeriod.Text.Split('-');
                    if (startPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(startPeriod[1]);

                            startDate = new DateTime(year, month, 1).Date;
                            endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                        }
                    }
                }
                else if (string.IsNullOrEmpty(txtStartPeriod.Text) && !string.IsNullOrEmpty(txtEndPeriod.Text))
                {
                    string[] startPeriod = txtStartPeriod.Text.Split('-');
                    if (startPeriod.Length > 1)
                    {
                        string monthValue = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMonthIndexFromAbbreviatedMonth(startPeriod[0]);

                        if (!string.IsNullOrEmpty(monthValue))
                        {
                            int month = Convert.ToInt32(monthValue);
                            int year = Convert.ToInt32(startPeriod[1]);

                            startDate = new DateTime(year, month, 1).Date;
                            endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                        }
                    }
                }

                if (startDate != null && endDate != null)
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.PerformerScheduledOn.Date >= startDate && row.PerformerScheduledOn.Date <= endDate).ToList();
            }
            catch (Exception ex)
            {

            }

            #endregion

            #region Period Filter

            if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
            {
                List<string> lstPeriod = new List<string>();
                lstPeriod = GetSelectedPeriod();

                string frequencySelected = string.Empty;
                frequencySelected = ddlFrequency.SelectedValue;

                if (frequencySelected.Equals("BiAnnual"))
                    frequencySelected = "TwoYearly";

                if (ddlFrequency.SelectedValue != "-1" && lstPeriod.Count > 0)
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Frequency.Equals(frequencySelected) && lstPeriod.Contains(row.RLCS_PayrollMonth)).ToList();
                }
                else if (ddlFrequency.SelectedValue != "-1")
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.Frequency.Equals(frequencySelected)).ToList();
                }
            }

            if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
            {
                if (ddlYear.SelectedValue != "-1")
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(row => row.RLCS_PayrollYear.Equals(ddlYear.SelectedValue)).ToList();
                }
            }

            #endregion

            return MastertransactionsQuery;
        }

        public List<string> GetSelectedPeriod()
        {
            List<string> lstPeriod = new List<string>();
            try
            {
                foreach (RepeaterItem aItem in rptPeriodList.Items)
                {
                    try
                    {
                        CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
                        if (chkPeriod != null && chkPeriod.Checked)
                        {
                            Label lblPeriodName = (Label)aItem.FindControl("lblPeriodName");
                            if (lblPeriodName != null && !string.IsNullOrEmpty(lblPeriodName.Text))
                            {
                                if (!string.IsNullOrEmpty(lblPeriodName.Text.Trim()))
                                {
                                    Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
                                    if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
                                    {
                                        if (ddlFrequency.SelectedValue == "Monthly")
                                            lstPeriod.Add(lblPeriodID.Text);
                                        else if (ddlFrequency.SelectedValue == "Quarterly")
                                        {
                                            if (lblPeriodName.Text == "Q1")
                                            {
                                                lstPeriod.Add("03");//CY
                                                lstPeriod.Add("06");//FY
                                            }
                                            else if (lblPeriodName.Text == "Q2")
                                            {
                                                lstPeriod.Add("06");//CY
                                                lstPeriod.Add("09");//FY
                                            }
                                            else if (lblPeriodName.Text == "Q3")
                                            {
                                                lstPeriod.Add("09");//CY
                                                lstPeriod.Add("12");//FY
                                            }
                                            else if (lblPeriodName.Text == "Q4")
                                            {
                                                lstPeriod.Add("12");//CY
                                                lstPeriod.Add("03");//FY
                                            }
                                        }
                                        else if (ddlFrequency.SelectedValue == "HalfYearly")
                                        {
                                            if (lblPeriodName.Text == "HY1")
                                            {
                                                lstPeriod.Add("06");//CY
                                                lstPeriod.Add("09");//FY
                                            }
                                            else if (lblPeriodName.Text == "HY2")
                                            {
                                                lstPeriod.Add("12");//CY
                                                lstPeriod.Add("03");//FY
                                            }
                                        }
                                        else if (ddlFrequency.SelectedValue == "Annual")
                                        {
                                            lstPeriod.Add("12");//CY
                                            lstPeriod.Add("03");//FY
                                        }
                                        else if (ddlFrequency.SelectedValue == "BiAnnual" || ddlFrequency.SelectedValue == "Biennial")
                                        {
                                            lstPeriod.Add("12");//CY
                                            lstPeriod.Add("03");//FY
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                }

                return lstPeriod;
            }
            catch (Exception ex)
            {
                return lstPeriod;
            }
        }

        public void ClearSelectedPeriod()
        {
            try
            {
                foreach (RepeaterItem aItem in rptPeriodList.Items)
                {
                    try
                    {
                        CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
                        if (chkPeriod != null && chkPeriod.Checked)
                        {
                            chkPeriod.Checked = false;
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlCustomers.SelectedValue = "-1";
                ddlSPOCs.SelectedValue = "-1";
                txtStartPeriod.Text = "";
                txtEndPeriod.Text = "";

                ddlFrequency.ClearSelection();

                ddlYear.ClearSelection();

                if (MainView.ActiveViewIndex == 0)
                    LoadDashboardCustomerWise(null);
                else
                    LoadDashboardSPOCWise(null);
            }
            catch (Exception ex) { LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name); }
        }

        protected void BindYears(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQueryList)
        {
            try
            {
                var lstYears = MastertransactionsQueryList.Where(row => row.RLCS_PayrollYear != null).Select(row => row.RLCS_PayrollYear).Distinct().OrderByDescending(row => row).ToList();

                ddlYear.Items.Clear();

                ddlYear.DataSource = lstYears;
                ddlYear.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPeriod(string selectedFreq)
        {
            try
            {
                if (!string.IsNullOrEmpty(selectedFreq))
                {
                    List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

                    tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "Annual"));
                    //tupleList.Add(new Tuple<string, string, string>("Biennial", "Biennial", "Biennial"));
                    tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "BiAnnual"));

                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "HY1"));
                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "HY2"));

                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "Q1"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "Q2"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "Q3"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "Q4"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "Octomber", "10"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

                    DataTable dtReturnPeriods = new DataTable();
                    dtReturnPeriods.Columns.Add("ID");
                    dtReturnPeriods.Columns.Add("Name");

                    var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            DataRow drReturnPeriods = dtReturnPeriods.NewRow();
                            drReturnPeriods["ID"] = item.Item3;
                            drReturnPeriods["Name"] = item.Item2;
                            dtReturnPeriods.Rows.Add(drReturnPeriods);
                        }
                        txtPeriodList.Enabled = true;
                        rptPeriodList.DataSource = dtReturnPeriods;
                        rptPeriodList.DataBind();
                    }
                    else
                    {
                        txtPeriodList.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                {
                    BindPeriod(ddlFrequency.SelectedValue);
                    ddlYear.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upFreqFilter_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}