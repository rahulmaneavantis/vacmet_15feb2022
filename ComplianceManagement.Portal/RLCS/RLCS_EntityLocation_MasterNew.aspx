﻿<%@ Page Title="Entity/Location/Branch" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EntityLocation_MasterNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EntityLocation_MasterNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

      
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    
    <style type="text/css">
            .k-grid-content
        {
            min-height:394px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

          .myKendoCustomClass {
            z-index:999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>
 
    <title></title>


     <script type="text/javascript">
         $(document).ready(function () {
            $('#divRLCSCustomerBranchesDialog').hide();
            $('#divRLCSCustomerLocationDialog').hide();
            fhead('Masters/ Entity-Location-Branch');
            
             var dataSource = new kendo.data.TreeListDataSource({
                 transport: {
                     read: {
                         url: '<% =avacomRLCSAPI_URL%>/GetCustomerBranchDetail?customerID=<% =CustId%>',
                         beforeSend: function (request) {
                             request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                             request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                             request.setRequestHeader('Content-Type', 'application/json');
                         },
                         dataType: 'json',
                     }
                 },
                 schema: {
                     data: function (response) {
                         return response.Result;
                     },
                     model: {
                         id: "ID",
                         parentId: "ParentID",
                         fields: {
                             ID: { type: "number", nullable: false },
                             ParentID: { field: "ParentID", nullable: true }
                         },
                         expanded: true
                     }
                 }
             });

             $("#treelist").kendoTreeList({
                 dataSource: dataSource,
                  sortable: true,
                filterable: true,
                columnMenu: true,
                 columns: [
                     { field: "CM_ClientID", expandable: true, title: "ClientID" },
                     { field: "CM_City", title: "Location Code" },
                     { field: "CM_State", title: "State Code" },
                     {
                         field: "SM_Name", title: "State"
                     },
                     { field: "LM_Name", title: "Location" },
                     { field: "Name", title: "Name" },
                     { field: "CM_EstablishmentType", title: "Establishment Type", hidden: true },
                     {
                        command: [
                            { name: "View", text: "", iconClass: ".k-icon k-i-eye", className: "ob-overviewMain" }
                        ], title: "Action", lock: true,// width: 150,                        
                    }
                 ]
             });
         });

         $(document).on("click", "#treelist tbody tr .ob-overviewMain", function (e) {
             var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
             
             if (item.BranchType == "E") {
                 OpenRLCSEntityClientPopup(item.ID);
             }
             if (item.BranchType == "B") {
                  OpenRLCSLocationBranchPopup(item.ID);
             }
             return false;
         });


         function OpenRLCSEntityClientPopup(ID) {
             $('#divRLCSCustomerBranchesDialog').show();

             var myWindowAdv = $("#divRLCSCustomerBranchesDialog");

             function onClose() {

             }

             myWindowAdv.kendoWindow({
                 width: "80%",
                 height: '80%',
                 title: "Entity/Client Details-HR+ Compliance",
                 visible: false,
                 actions: [
                     //"Pin",
                     "Close"
                 ],
                 close: onClose
             });

             myWindowAdv.data("kendoWindow").center().open();
             $('#iframeRLCSEntityClient').attr('src', "/Setup/CreateClientBasicSetup?Id=" + ID + "&getMaster=Yes");

             return false;
         }

           function OpenRLCSLocationBranchPopup(ID) {
            $('#divRLCSCustomerLocationDialog').show();

            var myWindowAdv = $("#divRLCSCustomerLocationDialog");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "87%",
                height: "90%",
                title: "Branch Details-HR+ Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=Yes");

            return false;
        }
            </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div id="treelist"></div>
      <div id="divRLCSCustomerBranchesDialog">
        <iframe id="iframeRLCSEntityClient" style="width: 100%; height: 100%; border: none">
              <div class="chart-loading"></div>
        </iframe>
    </div>
    <div id="divRLCSCustomerLocationDialog">
        <iframe id="iframeRLCSLocationBranch" style="width: 100%; height: 100%; border: none">
              <div class="chart-loading"></div>
        </iframe>
    </div>
</asp:Content>
