﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCSMIS : System.Web.UI.Page
	{
		protected static string CId;
		protected static string UId;
		protected static string ProfileID;
		protected static string MISPath;
		protected static string avacomAPIUrl;
		protected static string RLCS_API_URL;
		private HttpResponseMessage response;


		private HttpClient _httpClient { set; get; }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				RLCS_API_URL = ConfigurationManager.AppSettings["RLCSAPIURL_Portal"];
				avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
				CId = Convert.ToString(AuthenticationHelper.CustomerID);
				UId = Convert.ToString(AuthenticationHelper.UserID);
				ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
				if (!string.IsNullOrEmpty(Request.QueryString["Flag"]) && Request.QueryString["Flag"] == "D")
				{
					if (!string.IsNullOrEmpty(Request.QueryString["MISPaths"]))
					{
						MISPath = Request.QueryString["MISPaths"].ToString();
						bool response = GetMISDocuments(MISPath);
						if (!response)
							ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Document Not avaliable');", true);
					}

				}
			}
		}

		protected bool GetMISDocuments(string MISPath)
		{
			MISPath=MISPath.Replace("|", "'");
			string filename = String.Empty;
			string contentType = String.Empty;
			bool documentSuccess = true;
			string baseAddress = ConfigurationManager.AppSettings["RLCS_MIS_Path"].ToString();
			string filePath = baseAddress + MISPath;
			_httpClient = new HttpClient();
			_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL_Portal"]);
			_httpClient.Timeout = new TimeSpan(0, 15, 0);
			_httpClient.DefaultRequestHeaders.Accept.Clear();
			_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			_httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
			_httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
			_httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

			try
			{
				HttpResponse res = HttpContext.Current.Response;
				response = _httpClient.GetAsync("Masters/Generate?FilePath=" + filePath).Result;
				var statuscode = Convert.ToInt32(response.StatusCode);
				if (statuscode == 200)
				{
					var byteFile = response.Content.ReadAsByteArrayAsync().Result;
					if (byteFile.Length > 0)
					{

						string extension = Path.GetExtension(MISPath);
						contentType = "application/" + extension.ToString().Replace(".", "").ToLower() + "";
						filename = "MIS" + extension;

						res.Buffer = true;
						res.ClearContent();
						res.ClearHeaders();
						res.Clear();
						res.ContentType = contentType;
						res.AddHeader("content-disposition", "attachment; filename=" + "MIS" + "-Document" +extension);
						res.BinaryWrite(byteFile);
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest();

					}



				}
			}
			catch (Exception ex)
			{ documentSuccess = false; }








			//try
			//{
			//	if (File.Exists(filePath))
			//	{
			//		string extension = Path.GetExtension(filePath);
			//		string contentType = "application/" + extension.ToString().Replace(".", "").ToLower() + "";
			//		HttpResponse response = HttpContext.Current.Response;
			//		response.Buffer = true;
			//		response.ClearContent();
			//		response.ClearHeaders();
			//		response.Clear();
			//		response.ContentType = contentType;
			//		response.AddHeader("content-disposition", "attachment; filename=" + "MIS" + "-Document-" + DateTime.Now.ToString("ddMMyy") + extension);
			//		response.TransmitFile(filePath);
			//		response.Flush(); // Sends all currently buffered output to the client.
			//		response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
			//		HttpContext.Current.ApplicationInstance.CompleteRequest();
			//		response.End();
			//	}
			//	else
			//	{
			//		documentSuccess = false;
			//	}
			//}
			//catch (Exception ex)
			//{
			//	documentSuccess = false;
			//}
			return documentSuccess;
		}
	}
}