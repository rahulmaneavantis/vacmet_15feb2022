﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_VendorAuditDashborad_Client.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_VendorAuditDashborad_Client" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        

        <title>MY Vendor DASHBOARD :: AVANTIS - Products that simplify</title>        

        <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>    
        <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
        <style type="text/css">
            .highcharts-background {
                width: auto;
            }
            .Dashboard-white-widget {
                background: none;
            }
           

            .bx-wrapper .bx-controls-direction a {
                position: absolute;
                top: 20%;
                outline: 0;
                width: 20px;
                height: 35px;
                text-indent: -9999px;
                z-index: 999;
            }

            .bx-wrapper .bx-prev {
                left: -4.5%;
            }

            .bx-wrapper .bx-next {
                right: -4.5%;
            }

            .downloadinage {
                background: url(../Images/regulatorydownload.png) no-repeat 6px center;
                width: 100px;
                height: 100px;
                display: block;
            }

            #UpDetailView {
                margin-top: 25px;
                margin-left: 20px;
            }

            .windowgrading {
                min-height: 150px;
                max-height: 200px;
            }

            /*.tile_stats_count {
                transition: color 1s ease-out;
                color: navy;
            }*/

            .tile_stats_count:hover {
                /*font-size:22px;
                   margin-bottom:23px;*/
                /*color: white;*/
                /*background-color:grey;*/
            }

            .bg-aqua {
                background-color: #00c0ef !important;
                color: #fff !important;
            }

            .bg-green {
                background-color: #00a65a !important;
            }

            .bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green, .bg-navy, .bg-teal, .bg-olive, .bg-lime, .bg-orange, .bg-fuchsia, .bg-purple, .bg-maroon, .bg-black, .bg-red-active, .bg-yellow-active, .bg-aqua-active, .bg-blue-active, .bg-light-blue-active, .bg-green-active, .bg-navy-active, .bg-teal-active, .bg-olive-active, .bg-lime-active, .bg-orange-active, .bg-fuchsia-active, .bg-purple-active, .bg-maroon-active, .bg-black-active {
                color: #fff !important;
            }

            .bg-yellow {
                background-color: #f39c12 !important;
            }

            /*:after, :before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            :after, :before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }*/

            .small-box {
                border-radius: 2px;
                position: relative;
                display: block;
                margin-bottom: 20px;
                box-shadow: 0 1px 1px rgba(0,0,0,0.1);
            }

                .small-box:hover {
                    text-decoration: none;
                    color: #f9f9f9;
                    /* transition: all .3s linear; */
                    /*height:123px;
    width:155px;*/
                }

                .small-box > .inner {
                    padding: 10px;
                }

            .icon {
                -webkit-transition: all .3s linear;
                -o-transition: all .3s linear;
                transition: all .3s linear;
                position: absolute;
                top: -10px;
                right: 10px;
                z-index: 0;
                font-size: 96px;
                color: rgba(0,0,0,0.15);
            }

            .small-box > .small-box-footer {
                position: relative;
                text-align: center;
                padding: 3px 0;
                color: #fff;
                color: rgba(255,255,255,0.8);
                display: block;
                z-index: 10;
                background: rgba(0,0,0,0.1);
                text-decoration: none;
            }

            .divCount:hover {
                /*color: #FF7473;*/
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
            }

            .entity-bg {
                color: #fff;
                background: #2b80b7;
            }

            .compliances-bg {
                color: #fff;
                background: #64B973;
            }

            .dueToday-bg {
                color: #fff;
                background: #8F45AD;
            }

            .upcoming-bg {
                color: #fff;
                background: #004586;
            }

            .overdue-bg {
                color: #fff;
                background: #FF7473;
                /*#FF0000;*/
            }

            @media (min-width: 768px) {
                .five-cols .col-md-1,
                .five-cols .col-sm-1,
                .five-cols .col-lg-1 {
                    width: 100%;
                }
            }

            @media (min-width: 992px) {
                .five-cols .col-md-1,
                .five-cols .col-sm-1,
                .five-cols .col-lg-1 {
                    width: 20%;
                }
            }

            @media (min-width: 1200px) {
                .five-cols .col-md-1,
                .five-cols .col-sm-1,
                .five-cols .col-lg-1 {
                    width: 20%;
                }
            }

            #max:hover {
                z-index: 10;
            }

            .closeButton:hover, .maxButton:hover {
                -moz-box-shadow: 0 0 0 15px #f4f4f4;
                -webkit-box-shadow: 0 0 0 15px #f4f4f4;
                box-shadow: 0 0 0 15px #f4f4f4;
                -moz-transition: -moz-box-shadow .2s;
                -webkit-transition: -webkit-box-shadow .2s;
                transition: box-shadow .2s;
            }
        </style>

        <style>
            .mr10 {
                margin-left: 15px;
            }

            .ml5 {
                margin-left: 10px;
            }

            .map {
            }

            .columnchart {
            }

            #example, #example1 {
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .dash-head {
                width: 970px;
                height: 80px;
                /*background: url('../content/web/sortable/dashboard-head.png') no-repeat 50% 50% #222222;*/
            }

            .panel-wrap {
                display: table;
                /* width: 968px;
                  background-color: #f5f5f5;
                    border: 1px solid #e5e5e5;*/
            }

            #sidebar2 #sidebar3 {
                display: table-cell;
                /*padding: 20px 0 20px 20px;
                    width: 220px;*/
                vertical-align: top;
            }

            .widget.placeholder {
                opacity: 0.4;
                border: 1px dashed #a6a6a6;
            }

            /* WIDGETS */
            .widget {
                /*margin: 0 0 20px;*/
                padding: 0;
                background-color: #ffffff;
                /*border: 1px solid #e7e7e7;*/
                border-radius: 3px;
                cursor: move;
            }

                .widget:hover {
                    background-color: #fcfcfc;
                    border-color: #cccccc;
                }

                .widget div {
                    /*padding: 10px;
                    min-height: 50px;*/
                }

                .widget h3 {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 17px;
                    padding: 10px 10px;
                    /*text-transform: uppercase;*/
                    /*border-bottom: 1px solid #e7e7e7;*/
                }

                    .widget h3 span {
                        float: right;
                    }

                        .widget h3 span:hover {
                            cursor: pointer;
                            background-color: #e7e7e7;
                            border-radius: 20px;
                        }

            /* PROFILE */
            .profile-photo {
                /*width: 80px;
                    height: 80px;*/
                /*margin: 10px auto;*/
                border-radius: 100px;
                border: 1px solid #e7e7e7;
                background: url('../content/web/Customers/ISLAT.jpg') no-repeat 50% 50%;
            }

            #profile div {
                /*text-align: center;*/
            }

            #profile #profile1 h4 {
                /*width: auto;
                    margin: 0 0 5px;
                    font-size: 1.2em;*/
                color: #1f97f7;
            }

            #profile p {
                /*margin: 0 0 10px;*/
            }

            /* BLOGS & NEWS */
            /*#blogs,
            #news,#profile1,#teammates1,#teammates2,#teammates3,#teammates4,#teammates5 {
                margin-bottom:15px;
              margin-right:15px;
            }*/


            #CheckSummary #RiskSummary #PerformanceSummary #FreqSummary h4,
            #blogs h4,
            #news h4 {
                width: auto;
                font-size: 1.4em;
                color: #1f97f7;
                font-weight: normal;
            }

            .blog-info {
                font-size: .9em;
                color: #787878;
            }

            #sidebar2 #sidebar3 #blogs h4 {
                font-size: 1em;
            }

            #sidebar2 #sidebar3 #blogs p {
                display: none;
            }

            #sidebar2 #blogs .blog-info {
                display: block;
            }

            #sidebar4 {
                /*margin-left:15px;*/
            }

            #mainsummery #news h4 {
                font-size: 1.2em;
                line-height: 1.4em;
                height: 40px;
            }

                #mainsummery #news h4 span {
                    display: block;
                    float: left;
                    width: 100px;
                    height: 40px;
                    color: #000;
                }
           
            #CheckSummary, #RiskSummary, #PerformanceSummary, #FreqSummary {
                margin-left: 15px;
                margin-bottom: 15px;
            }
            /* TEAMMATES */
            /*.team-mate:after {
                content: ".";
                display: block;
                height: 0;
                line-height: 0;
                clear: both;
                visibility: hidden;
            }*/
            #CheckSummary #RiskSummary #PerformanceSummary #FreqSummary .team-mate h4 {
                font-size: 1.4em;
                font-weight: normal;
                /*margin-top: 12px;*/
            }

            .team-mate p {
            }

            .team-mate img {
                float: left;
                margin: 0 15px 0 0;
                border: 1px solid #e7e7e7;
                border-radius: 60px;
            }


        </style>

        <style type="text/css">
            b, strong {
                font-weight: 400;
            }

            div.k-window {
                z-index: 27;
            }



           

            .tile_count .tile_stats_count {
                padding: 0px 0px 0px 20px;
                position: relative;
            }

            .tile_count .tile_stats_count, ul.quick-list li {
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .body {
                color: #eee;
            }
           
            *, :after, :before {
                box-sizing: border-box;
            }

            .info-box:hover {
                /*color: #FF7473;*/
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }



            .TMB {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 20% !important;
            }

            .TMB1 {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 19.5% !important;
            }

            .TMBImg {
                padding-left: 0px !important;
                padding-right: 0px !important;
                text-align: center;
                /*margin-left: -7%;
                margin-right: -3%;*/
            }



            .info-box {
                min-height: 65px !important;
                width: 90%;
                margin-left: 10px;
            }

                .info-box .titleMD {
                    font-size: 14px;
                }

                .info-box .countMD {
                    font-size: 18px;
                }

            .div-location {
                min-height: 52px;
                padding-top: 0px;
            }

            .Dashboard-white-widget {
                padding: 5px 10px 0px !important;
            }


            .TMBImg > img {
                width: 37px;
            }


            div.panel {
                margin-bottom: 12px;
            }

            .panel .panel-heading .panel-actions {
                height: 25px !important;
            }

            hr {
                margin-bottom: 8px;
            }

            .panel .panel-heading h2 {
                font-size: 18px;
            }

            td > label {
                padding: 6px;
            }

            

            span.input-group-addon {
                padding: 0px;
            }

            td > label {
                padding: 3px 4px 0 4px;
                margin-top: -1%;
            }

            .nav-tabs > li > a {
                color: #333 !important;
            }

            /*.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #1fd9e1 !important;
            }*/
        </style>

        <style type="text/css">
            

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

        </style>




    <script type="text/javascript">
        $(document).ready(function () {
            MonthlyCompliancePieChart();
            RiskColumnChart();
            StatusPieChart();
            FrequancySummaryChart();
        });

         function FrequancySummaryChart() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'divSummaryPieChart',
                    type: 'pie',
                    events: {
                        load: function (event) {
                            //When is chart ready?
                            $(document).resize();
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: 'Click on graph to view details',
                    //  text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<br>{point.percentage:.0f} %',
                            distance: -50,
                            filter: {
                                property: 'percentage',
                                operator: '>',
                                value: 4
                            }
                        },
                    },
                    series: {
                        point: {
                            events: {
                                legendItemClick: function () {
                                    return false; // <== returning false will cancel the default action
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.0f}%</b>'
                },
                series: [{
                    data: [
                                     <%=seriesData_GraphFrequancyPie%>
                    ],
                    size: '100%',
                    showInLegend: true,
                    events: {
                        click: function (event) {
                            OpenMonthlyData("MonthlyPie");
                        }
                    },
                    allowPointSelect: false,
                    point: {
                        events: {
                            legendItemClick: function (e) {
                                e.preventDefault();
                            }
                        }
                    }
                }]
            },

                function (chart) { // on complete
                    var textX = chart.plotLeft + (chart.plotWidth * 0.5);
                    var textY = chart.plotTop + (chart.plotHeight * 0.5);

                    var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
                    span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
                    span += '<span style="font-size: 16px">1000</span>';
                    span += '</span>';

                    $("#addText").append(span);
                    span = $('#pieChartInfoText');
                    span.css('left', textX + (span.width() * -0.5));
                    span.css('top', textY + (span.height() * -0.5));
                });
        }

        function MonthlyCompliancePieChart() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'divMonthlyCompliancePieChart',
                    type: 'pie',
                    events: {
                        load: function (event) {
                            //When is chart ready?
                            $(document).resize();
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: 'Click on graph to view details',
                    //  text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<br>{point.percentage:.0f} %',
                            distance: -50,
                            filter: {
                                property: 'percentage',
                                operator: '>',
                                value: 4
                            }
                        },
                    },
                    series: {
                        point: {
                            events: {
                                legendItemClick: function () {
                                    return false; // <== returning false will cancel the default action
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.0f}%</b>'
                },
                series: [{
                    data: [
                                     <%=seriesData_GraphMonthlyCompliancePie%>
                    ],
                    size: '100%',
                    showInLegend: true,
                    events: {
                        click: function (event) {
                            OpenMonthlyData("MonthlyPie");
                        }
                    },
                    allowPointSelect: false,
                    point: {
                        events: {
                            legendItemClick: function (e) {
                                e.preventDefault();
                            }
                        }
                    }
                }]
            },

                function (chart) { // on complete
                    var textX = chart.plotLeft + (chart.plotWidth * 0.5);
                    var textY = chart.plotTop + (chart.plotHeight * 0.5);

                    var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
                    span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
                    span += '<span style="font-size: 16px">1000</span>';
                    span += '</span>';

                    $("#addText").append(span);
                    span = $('#pieChartInfoText');
                    span.css('left', textX + (span.width() * -0.5));
                    span.css('top', textY + (span.height() * -0.5));
                });
        }
         function RiskColumnChart(){
            var perRiskStackedColumnChart = Highcharts.chart('perRiskStackedColumnChartDiv', {
                chart: {
                    type: 'column',
                    events: {
                        load: function(event) {
                            
                            $(document).resize(); 
                        }
                    }
                },
                title: {
                    text: 'Per Risk',
                    style: {
                        // "font-family": 'Helvetica',
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Click on graph to view details',
                    //  text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                xAxis: {
                    categories: ['High', 'Medium', 'Low']
                },
                yAxis: {
                    title: {
                        text: '',                     
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: 'gray'
                        }
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: 'white',
                            style:{
                              
                                textShadow:false,
                            }
                        },
                        cursor: 'pointer'
                    },
                },
                <%=perRiskChart%>
                });

                return false;
        }
       
              function StatusPieChart(){
            var perStatusPieChart = Highcharts.chart('perStatusPieChartDiv', {
                chart: {
                    type: 'pie',
                    events: {
                        load: function(event) {                            
                            $(document).resize(); 
                        },
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            // this.subtitle.update({ text: 'Click on graph to view documents' });
                            //this.subtitle.update({ text: 'Completion Status - Overall Risk' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Status' });
                            //this.subtitle.update({ text: 'Click on graph to drilldown' });
                            //this.subtitle.update({ text: 'Completion Status - Overall' });
                        }
                    },
                },
                subtitle: {
                    text: 'Click on graph to view details',
                    //  text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                title: {
                    text: 'Per Status',
                    style: {
                        display: 'none'
                    },
                },
                xAxis: {
                    type: 'category',
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        events: {
                            click: function (event) {
                                    
                                Detail('GraphMonthly','DetailsMonthly','liGraphMonthlky','liDetailsMonthly');

                            }
                        },
                            
                        dataLabels: {
                            enabled: true,
                            //format: '{y} <br>{point.name}',
                            format: '{y}',
                            distance: 5,
                        },
                        showInLegend: true,
                    },
                  
                },
                legend: {
                    itemDistance: 2,
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    formatter: function () {
                        if (this.series.name == 'Status')
                            return 'Click to Drilldown';	// text before drilldown
                        else
                            return 'Click to View Documents';		// text after drilldown
                    }
                },
                <%=perFunctionPieChart%>                
            });
            return false;

        }
           function Detail(Graph,Details,liGraph,liDetails){                         
                $('#' +Graph).hide();
                $('#' +Details).show();
                $('#'+liGraph).removeClass('active');
                $('#' +liDetails).addClass('active');
                return false;
            }
         
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section id="container-fluid" class="">
        <section class="">
            
    <div class="row col-md-12 tile_count" id="divTabs" runat="server">
        <div id="entitycountDivBox" runat="server" class="five-cols col-md-1 TMB">
            <div class="info-box bluenew-bg">
                <div class="div-location" style="cursor: pointer;">
                    <div class="col-md-5 TMBImg">
                        <img src="../Images/Upcoming-new.png" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Open Audit</div>
                        <div id="divOpenAuditCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>

        <div id="compliancecountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box compliances-bg">
                <div class="div-location" style="cursor: pointer">
                    <div class="col-md-5 TMBImg">
                        <img src="../Images/Compliances-new.png" />
                    </div>
                    <div class="col-md-7 colpadding0" style="margin-left: -4%;">
                        <div class="titleMD" style="text-align: left;">Closed Audit</div>
                        <div id="divClosedAuditCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>


        <div id="duetodayCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box dueToday-bg">
                <div class="div-location" style="cursor: pointer">
                    <div class="col-md-5 TMBImg">
                        <img src="../Images/Due Today.png" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Cancelled Audit</div>
                        <div id="divCancelledAuditCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>

        <div id="upcomingCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box upcoming-bg">
                <div class="div-location" style="cursor: pointer">
                    <div class="col-md-5 TMBImg">
                        <img src="../Images/User-Profile-pic.png" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Vendor</div>
                        <div id="divVendorCount" runat="server" class="countMD" style="text-align: left;">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>

        <div id="OverdueCountDivBox" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box overdue-bg">
                <div class="div-location" style="cursor: pointer">
                    <div class="col-md-5 TMBImg">
                        <img src="../Images/User-Profile-pic.png" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Auditor</div>
                        <div id="divAuditorCount" runat="server" class="countMD" style="text-align: left;">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>

    </div>

    <div class="row">
        <div id="example" class="">
            <div class="panel-wrap hidden-on-narrow col-md-12 pl0 colpadding0" style="padding: 0">
                <div id="sidebar2">
                        <div id="CheckSummary" class="widget col-md-4 colpadding0" style="width: 31.5%;">
                                <h3><b>OverAll Summary</b>
                                    </h3>
                              <div class="col-md-12 colpadding0">
                                    <div id="divMonthlyCompliancePieChart" class="col-md-12 colpadding0" style="height: 280px;">                                      
                                    </div>
                                </div>
                            </div>

                         <div id="RiskSummary" class="widget col-md-4 colpadding0" style="width: 31.5%;">
                                <h3>
                                    <b>Risk Summary</b>                                   
                                </h3>

                                <div class="col-md-12 colpadding0">                                 
                                    <div id="perRiskStackedColumnChartDiv" class="col-md-12 colpadding0" style="height: 280px;">
                                    </div>
                                </div>
                            </div>
                
                      <div id="PerformanceSummary" class="widget col-md-4 colpadding0" style="width: 31.5%">
                                <h3><b>Performance Summary </b>                                  
                                </h3>
                                 <div class="col-md-12 colpadding0">
                                   <div id="perStatusPieChartDiv" class="col-md-12 colpadding0" style="height: 280px;">                              
                                    </div>
                                </div>
                            </div>
                </div>
                   <div id="sidebar4">
                          <div id="FreqSummary" class="widget col-md-4 colpadding0" style="width: 31.5%">
                                <h3><b>Frequancy Summary</b>
                                   </h3>
                              <div class="col-md-12 colpadding0">
                                    <div id="divSummaryPieChart" class="col-md-12 colpadding0" style="height: 280px;width:auto;">                                      
                                    </div>
                                </div>
                            </div>
                       </div>
            </div>
        </div>
    </div>
            </section>
         </section>
</asp:Content>
