﻿<%@ Page Title="My Documents" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyDocuments.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyDocuments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <style type="text/css">
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
    
    <script type="text/x-kendo-template" id="template"> 
       
    <div class=row style="padding-bottom: 4px;">
            <div class="toolbar">               
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width:242px;">            
                <%--<input id="dropdownlistComplianceType" data-placeholder="Type"> --%>                  
                <%--<input id="dropdownlistRisk" data-placeholder="Risk">    --%>              
                <input id="dropdownlistStatus" data-placeholder="Status">
                <input id="dropdownlistTypePastdata" data-placeholder="Status">                
               <%-- <button id="export" onclick="exportReport()"  class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;"></button>--%>
                <button id="AdavanceSearch" style="height: 23px;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
            </div>
    </div> 
           
         <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 17%;;">
                        </div>
                        <div class="col-md-2" style="width: 15%;">
                            <div id="dvdropdownEventName" style="display:none;"><input id="dropdownEventName" data-placeholder="Event Name" style="width:175px;"></div>          
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 0px;">
                           <div id="dvdropdownEventNature" style="display:none;"><input id="dropdownEventNature" data-placeholder="Event Nature"></div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%;padding-left: 22px;">                             
                             <button id="ClearfilterMain" style="float: right; margin-left: 1%;display:none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>                                         
                             <button id="dvbtndownloadDocumentMain" style="float: right;display:none;" onclick="selectedDocumentMain(event)">Download</button>                                                                         
                        </div>

                    </div>
                </div>
                             
       
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filtersstoryboard">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filtertype">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filterrisk">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;" Id="filterstatus">&nbsp;</div>

    </script>

    <script id="fileTemplate" type="text/x-kendo-template">      
            <span class='k-progress'></span>
            <div class='file-wrapper'>  
            #=GetFileExtType(FileName)# #=FileName # 
         
    </script>

    <script id="fileExtensionTemplate" type="text/x-kendo-template">
      
            #=FileName.split('.').pop() # 
         
    </script>

    <script type="text/javascript">

        function GetFileExtType(value) {

            if (value.split('.').pop() == "pdf" || value.split('.').pop() == "PDF" || value.split('.').pop() == "Pdf") {
                return "<span class='k-icon k-i-file-pdf k-i-pdf'></span>";
            }
            else if (value.split('.').pop() == "doc" || value.split('.').pop() == "docx" || value.split('.').pop() == "DOC" || value.split('.').pop() == "DOCX") {
                return "<span class='k-icon k-i-file-word k-i-file-doc k-i-word k-i-doc'></span>";
            }

            else if (value.split('.').pop() == "xls" || value.split('.').pop() == "xlsx" || value.split('.').pop() == "XLS" || value.split('.').pop() == "XLSX") {
                return "<span class='k-icon k-i-file-excel k-i-file-xls k-i-excel k-i-xls'></span>";
            }
            else if (value.split('.').pop() == "ppt" || value.split('.').pop() == "pptx" || value.split('.').pop() == "PPT" || value.split('.').pop() == "PPTX") {
                return "<span class='k-icon k-i-file-ppt k-i-ppt'></span>";
            }
            else if (value.split('.').pop() == "msg" || value.split('.').pop() == "MSG") {
                return "<span class='k-icon k-i-email k-i-envelop k-i-letter'></span>";
            }
            else if (value.split('.').pop() == "txt") {
                return "<span class='k-icon k-i-file-txt k-i-txt'></span>";
            }
            else if (value.split('.').pop() == "jpg" || value.split('.').pop() == "JPG" || value.split('.').pop() == "jpeg" || value.split('.').pop() == "JPEG" || value.split('.').pop() == "png" || value.split('.').pop() == "PNG" || value.split('.').pop() == "tif" || value.split('.').pop() == "TIF" || value.split('.').pop() == "tiff" || value.split('.').pop() == "TIFF" || value.split('.').pop() == "bmp" || value.split('.').pop() == "BMP" || value.split('.').pop() == "gif" || value.split('.').pop() == "GIF") {
                return "<span class='k-icon k-i-image k-i-photo'></span>";
            }
            else {
                return "";
            }
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {
            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });

            $("#Startdatepicker").kendoDatePicker({
                change: onChange
            });

            function onChange() {

                $('#filterStartDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#filterStartDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterStartDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterStartDate').append('Start Date:&nbsp;');
                    $('#filterStartDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');

                }

                DateFilterCustom();
            }

            function DateFilterCustom() {

                $('input[id=chkAll]').prop('checked', false);
                $('#dvbtndownloadDocument').css('display', 'none');

                var filter = { logic: "and", filters: [] };
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    filter.filters.push({
                        field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    filter.filters.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')//kendo.parseDate(this.value(), 'MM/dd/yyyy')
                    });
                }
                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                dataSource.filter(filter);
            }

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange1
            });

            function onChange1() {
                $('#filterLastDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterLastDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterLastDate').append('End Date&nbsp;&nbsp;:&nbsp;');

                    $('#filterLastDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                } 
                DateFilterCustom();
            }

            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({
                dataSource: {
                    //type: "odata",
                    transport: {

                        read: {
                            url: "http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaIntFlag=SAT&FY=",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        } 
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                           alert("Called");
                            return response.Result;
                        }
                    }
                },

                height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                change: onChange,
                columns: [
                    {
                        template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' value=#=ScheduledOnID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",
                        width: "3%;"//, lock: true
                    },
                    { hidden: true, field: "Risk", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    {
                        hidden: true, field: "FileName", title: "Document Name",
                        template: kendo.template($('#fileTemplate').html()), width: "29.7%"
                    },
                    { hidden: true, field: "Version", title: "Version" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Description',
                        width: "43.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        //  width: 120,
                        // format: "{0:dd-MMM-yyyy}",
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        hidden: true, field: "VersionDate", title: "Uploaded Date", type: "date",
                        template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #"
                        //format: "{0:dd-MMM-yyyy}"
                    },
                    {
                        hidden: true, field: "FileName", title: "Type",
                        template: kendo.template($('#fileExtensionTemplate').html())
                    },
                    //{ hidden: true, field: "", title: "Type" },
                    { hidden: true, field: "", title: "Uploaded By" },
                    { hidden: true, field: "", title: "Size" },
                    { hidden: true, field: "ActID", title: "" },
                    { hidden: true, field: "UserID", title: "UserID" },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" }
                        ], title: "Action", lock: true,// width: 150,

                    }
                ]
            });

            $("#grid1").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            
            var grid = $("#grid").kendoGrid({

                dataSource: {
                    transport: {
                        read: {
                            url: "http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaIntFlag=SAT&FY=",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }

                         //read: "http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StatusFlag=1&FY="
                     },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            alert("Called");
                            return response.Result;
                        }
                    }
                },

                toolbar: kendo.template($("#template").html()),
                height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        //field: "ID", title: " ",
                        template: "<input name='sel_chkbxMain' id='sel_chkbxMain' type='checkbox' value=#=ScheduledOnID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAllMain' />",
                        width: "3%;"//, lock: true
                    },
                    //{ hidden: true, field: "ID", title: "ID" },
                    { hidden: true, field: "Risk", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        field: "ShortDescription", title: 'Description',
                        width: "32.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        //  width: 120,
                        //format: "{0:dd-MMM-yyyy}",
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,// width: 150,
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });           

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "SAT" },
                    { text: "Internal", value: "INT" },
                    //{ text: "Event Based", value: "1" },
                    //{ text: "Statutory CheckList", value: "2" },
                    //{ text: "Internal CheckList", value: "3" }
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                }
            });
            
            function DataBindDaynamicKendoGriddMain() {
                alert("DataBindDaynamicKendoGriddMain");
                $('input[id=chkAllMain]').prop('checked', false);
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                //$("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'none');

                var dataSource = new kendo.data.DataSource({
                    transport: {

                        read: {
                            url: 'http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaIntFlag=SAT&FY=',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        } 

                        //read: "http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StatusFlag=1&FY="
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                           alert("Called");
                            return response.Result;
                        }
                    }
                });
                alert("Grid BInd");
            var grid = $('#grid').data("kendoGrid");
            dataSource.read();
            alert("before read");
            grid.setDataSource(dataSource);

            debugger;
            
            var dataSource12 = new kendo.data.HierarchicalDataSource({
                severFiltering: true,
                transport: {
                    read: {
                        url: 'http://localhost:18695/api/GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>',
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        //dataType: 'json',
                    },
                },
                    
                    schema: {
                        data: function (response) {
                            alert(response.Result);
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                alert("Tree Bind");
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        filter.filters.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },

                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Pending For Review", value: "Pending For Review" }
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All", value: "All" }
                ]
            });


            function DataBindDaynamicKendoGrid() {

                <%if (RoleFlag == 1)%>
                <%{%>
                $("#dropdownUser").data("kendoDropDownTree").value([]);
                <%}%>
                $('input[id=chkAll]').prop('checked', false);

                $("#grid1").data("kendoGrid").dataSource.filter({});

                $("#dropdowntree1").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
                $("#Startdatepicker").data("kendoDatePicker").value(null);
                $("#Lastdatepicker").data("kendoDatePicker").value(null);
                $('#filterStartDate').html('');
                $('#filterLastDate').html('');
                $('#filterStartDate').css('display', 'none');
                $('#filterLastDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#dvbtndownloadDocument').css('display', 'none');

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                    read: {
                        url: 'http://localhost:18695/api/GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>',
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        //dataType: 'json',
                    },
                },
                        schema: {
                            data: function (response) {
                                alert(response.Result);
                                return response.Result;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });

                    dataSource12.read();
                    $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
                

                if ($("#dropdownFY").val() == "0") {
                    var dataSource = new kendo.data.DataSource({
                        transport: {

                            read: {
                                url: 'http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaIntFlag=SAT&FY=' + $("#dropdownFY").val() + '',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }

                            //read: "http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StatusFlag=1&FY="
                        },
                        pageSize: 10,
                        schema: {
                            data: function (response) {
                                alert("Called");
                                return response.Result;
                            }
                        },
                        filterable: true,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }
                else {
                    var dataSource = new kendo.data.DataSource({
                        transport: {

                            read: {
                                url: 'http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=All&StaIntFlag=SAT&FY=' + $("#dropdownFY").val() + '',
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            } 

                            //read: "http://localhost:18695/api/GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StatusFlag=1&FY="
                        },
                        pageSize: 10,
                        schema: {
                            data: function (response) {
                                alert("Called");
                                return response.Result;
                            }
                        }                    
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }
            }

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    debugger;
                    DataBindDaynamicKendoGriddMain();
                },
                index: 1,
                dataSource: [
                    { text: "All", value: "All" },
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" }                    
                ]
            });
            

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },

                checkAll: true,

                autoWidth: true,
                checkAllTemplate: "Select All",

                dataTextField: "Name",
                dataValueField: "ID",

                change: function (e) {

                    var filter = { logic: "or", filters: [] };

                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                    read: {
                        url: 'http://localhost:18695/api/GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>',
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        //dataType: 'json',
                    },
                },
                    schema: {
                        data: function (response) {
                            
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/State/Location/Branch",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {

                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                    read: {
                        url: 'http://localhost:18695/api/GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>',
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        //dataType: 'json',
                    },
                },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    DataBindDaynamicKendoGrid();

                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2016", value: "2016" },
                    { text: "2017", value: "2017" },
                    { text: "2018", value: "2018" },
                    { text: "2019", value: "2019" }
                ]
            });
            
            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {

                    var filter = { logic: "or", filters: [] };

                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>"
                    },
                }
            });

            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });
                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1')
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Pending For Review", value: "Pending For Review" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({

                autoWidth: true,

                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Statutory", value: "SAT" },
                    { text: "Internal", value: "INT" },
                ]
            });



            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID, item.TaskTransactionID)
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-editMain", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID, item.TaskTransactionID);
                $("#divAdvanceSearchModel").data("kendoWindow").close();
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDownloadOverviewpup(item.ScheduledOnID, item.TaskTransactionID)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                e.preventDefault();

                return true;
            });


            $(document).on("click", "#chkAll", function (e) {

                if ($('input[id=chkAll]').prop('checked')) {

                    $('input[name="sel_chkbx"]').each(function (i, e) {
                        e.click();
                    });
                }
                else {

                    $('input[name="sel_chkbx"]').attr("checked", false);
                }
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#chkAllMain", function (e) {

                if ($('input[id=chkAllMain]').prop('checked')) {
                    $('input[name="sel_chkbxMain"]').each(function (i, e) {
                        e.click();
                    });

                }
                else {

                    $('input[name="sel_chkbxMain"]').attr("checked", false);

                }
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbx", function (e) {
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbxMain", function (e) {
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });
        });

     function selectedDocument(e) {

         if (($('input[name="sel_chkbx"]:checked').length) == 0) {

             return;
         }
         var checkboxlist = [];
         $('input[name="sel_chkbx"]').each(function (i, e) {
             if ($(e).is(':checked')) {
                 checkboxlist.push(e.value);
             }
         });
         console.log(checkboxlist.join(","));

         $('#downloadfile').attr('src', "../Common/TaskDownloadDoc.aspx?TaskScheduleOnID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType1").val());
         return false;
     }
     function selectedDocumentMain(e) {

         e.preventDefault();
         if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {

             return;
         }
         var checkboxlist = [];
         $('input[name="sel_chkbxMain"]').each(function (i, e) {
             if ($(e).is(':checked')) {
                 checkboxlist.push(e.value);
             }
         });

         console.log(checkboxlist.join(","));

         $('#downloadfile').attr('src', "../Common/TaskDownloadDoc.aspx?TaskScheduleOnID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType").val());
         return false;
     }

     function ClearAllFilterMain(e) {

         //e.preventDefault();
         $("#dropdowntree").data("kendoDropDownTree").value([]);
         $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
         $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
         $('#ClearfilterMain').css('display', 'none');

         $('#dvbtndownloadDocumentMain').css('display', 'none');

         $("#dropdownEventName").data("kendoDropDownList").select(0);
         $("#dropdownEventNature").data("kendoDropDownList").select(0);

         $("#grid").data("kendoGrid").dataSource.filter({});

         $('input[id=chkAllMain]').prop('checked', false);
     }

     function ClearAllFilter(e) {


         $("#dropdowntree1").data("kendoDropDownTree").value([]);

         $("#dropdownUser").data("kendoDropDownTree").value([]);
         $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
         $("#Startdatepicker").data("kendoDatePicker").value(null);
         $("#Lastdatepicker").data("kendoDatePicker").value(null);
         $('#filterStartDate').html('');
         $('#filterLastDate').html('');
         $('#filterStartDate').css('display', 'none');
         $('#filterLastDate').css('display', 'none');
         $('#Clearfilter').css('display', 'none');

         $('#dvbtndownloadDocument').css('display', 'none');

         $("#grid1").data("kendoGrid").dataSource.filter({});

         $('input[id=chkAll]').prop('checked', false);
     }

     function fcloseStory(obj) {

         var DataId = $(obj).attr('data-Id');
         var dataKId = $(obj).attr('data-K-Id');
         var seq = $(obj).attr('data-seq');
         var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
         $(deepspan).trigger('click');
         var upperli = $('#' + dataKId);
         $(upperli).remove();

         //for rebind if any pending filter is present (Main Grid)
         fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
         fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
         fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
         fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
         fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

         CheckFilterClearorNot();

         CheckFilterClearorNotMain();
     };

     function CheckFilterClearorNotMain() {
         if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
             ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
             $('#ClearfilterMain').css('display', 'none');
         }
     }

     function CheckFilterClearorNot() {
         if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
             ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
             ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
             $('#Clearfilter').css('display', 'none');
         }
     }

     function fCreateStoryBoard(Id, div, filtername) {

         var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
         $('#' + div).html('');
         $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
         $('#' + div).css('display', 'block');

         if (div == 'filtersstoryboard') {
             $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
             $('#ClearfilterMain').css('display', 'block');
         }
         else if (div == 'filtertype') {
             $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
         }

         else if (div == 'filterstatus') {
             $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
             $('#ClearfilterMain').css('display', 'block');
         }
         else if (div == 'filterpstData1') {
             $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterCategory') {
             $('#' + div).append('Category&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterAct') {
             $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterCompSubType') {
             $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterCompType') {
             $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filtersstoryboard1') {
             $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filtertype1') {
             $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterrisk1') {
             $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterFY') {
             $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterUser') {
             $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }
         else if (div == 'filterstatus1') {
             $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
             $('#Clearfilter').css('display', 'block');
         }

         for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
             var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
             $(button).css('display', 'none');
             $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
             var buttontest = $($(button).find('span')[0]).text();
             if (buttontest.length > 10) {
                 buttontest = buttontest.substring(0, 10).concat("...");
             }
             $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
         }

         if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
             $('#' + div).css('display', 'none');
             $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

         }


         CheckFilterClearorNot();

         CheckFilterClearorNotMain();
     }

     function OpenAdvanceSearch(e) {

         var myWindowAdv = $("#divAdvanceSearchModel");

         function onClose() {

         }

         myWindowAdv.kendoWindow({
             width: "85%",
             height: "85%",
             title: "Advanced Search",
             visible: false,
             actions: [
                 //"Pin",
                 //"Minimize",
                 "Maximize",
                 "Close"
             ],
             close: onClose
         });

         myWindowAdv.data("kendoWindow").center().open();
         e.preventDefault();
         return false;
     }

     function OpenAdvanceSearchFilter(e) {
         $('#divAdvanceSearchFilterModel').modal('show');
         e.preventDefault();
         return false;
     }

     function ChangeView() {

         $("#grid1").data("kendoGrid").showColumn(0);//Id
         $("#grid1").data("kendoGrid").hideColumn(1);//CId
         $("#grid1").data("kendoGrid").hideColumn(2);//FileName
         $("#grid1").data("kendoGrid").hideColumn(3);//V
         $("#grid1").data("kendoGrid").showColumn(4);//Location
         $("#grid1").data("kendoGrid").showColumn(5);//Task Description
         $("#grid1").data("kendoGrid").showColumn(6);//SD
         $("#grid1").data("kendoGrid").showColumn(7);//ForMonth
         $("#grid1").data("kendoGrid").showColumn(8);//Status
         $("#grid1").data("kendoGrid").hideColumn(9);//VD
         $("#grid1").data("kendoGrid").hideColumn(10);//type
         $("#grid1").data("kendoGrid").hideColumn(11);//Uploaded by
         $("#grid1").data("kendoGrid").hideColumn(12);//Size
         $("#grid1").data("kendoGrid").hideColumn(13);//UserId           
     }

     function ChangeListView() {
         $("#grid1").data("kendoGrid").showColumn(0);//Id
         $("#grid1").data("kendoGrid").hideColumn(1);//CId
         $("#grid1").data("kendoGrid").showColumn(2);//FileName
         $("#grid1").data("kendoGrid").showColumn(3);//V
         $("#grid1").data("kendoGrid").hideColumn(4);//Location
         $("#grid1").data("kendoGrid").hideColumn(5);//Task Description
         $("#grid1").data("kendoGrid").hideColumn(6);//SD
         $("#grid1").data("kendoGrid").showColumn(7);//ForMonth
         $("#grid1").data("kendoGrid").hideColumn(8);//Status
         $("#grid1").data("kendoGrid").showColumn(9);//VD
         $("#grid1").data("kendoGrid").showColumn(10);//type
         $("#grid1").data("kendoGrid").hideColumn(11);//Uploaded by
         $("#grid1").data("kendoGrid").hideColumn(12);//Size
         $("#grid1").data("kendoGrid").hideColumn(13);//UserId
     }

     function exportReport() {
         $("#grid").getKendoGrid().saveAsExcel();
         return false;
     };

     function OpenDocumentOverviewpup(Taskscheduledonid, Tasktransactionid) {

         $('#divOverView').modal('show');
         $('#OverViews').attr('width', '1150px');
         $('#OverViews').attr('height', '600px');
         $('.modal-dialog').css('width', '1200px');
         $('#OverViews').attr('src', "../Common/TaskDocumentOverview.aspx?TaskScheduleID=" + Taskscheduledonid + "&TaskTransactionID=" + Tasktransactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());
     }

     function OpenDownloadOverviewpup(scheduledonid, transactionid) {

         $('#divDownloadView').modal('show');
         $('#DownloadViews').attr('width', '401px');
         $('#DownloadViews').attr('height', 'auto');
         $('.modal-dialog').css('width', '437px');
         $('.modal-dialog').css('height', '207px');

         $('#DownloadViews').attr('src', "../Common/TaskDownloadOverview.aspx?TaskScheduleID=" + scheduledonid + "&TaskTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());

     }

     $("#newModelClose").on("click", function () {
         myWindow3.close();
     });

     function CloseClearPopup() {
         $('#OverViews1').attr('src', "../Common/blank.html");
     }

     function CloseClearOV() {
         $('#OverViews').attr('src', "../Common/blank.html");
     }

     function CloseClearDV() {
         $('#DownloadViews').attr('src', "../Common/blank.html");
     }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Documents/ Compliance Documents');
            setactivemenu('TaskDocument');
            //fmaters();
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="example">
        <div id="grid" style="border: none;"></div>
        <div>

            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 360px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divAdvanceSearchModel" style="padding-top: 5px;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>

                    </div>
                </div>
                <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                            <input id="dropdowntree1" data-placeholder="Entity/State/Location/Branch" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                            <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownPastData" style="width: 100%;" />
                        </div>
                        <%--   <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                        </div>--%>
                        <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                            <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                            <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 115%;" />
                        </div>

                    </div>
                </div>


                <div class="row" style="margin-left: -9px; margin-top: 7px; margin-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                        <%--   <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                            <input id="dropdownPastData" style="width: 100%;" />
                        <%--</div>--%>
                        <%--<div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 20%; padding-left: 9px;">
                            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                        </div>--%>
                        <div class="col-md-2" style="width: 15.4%; padding-left: 0px;">
                            <%-- <input id="dropdownComplianceSubType" style="width: 100%;" />--%>
                            <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                        </div>
                        <%-- <div class="col-md-4" id="dvdropdownACT" style="width: 31.3%; padding-left: 0px;">
                            <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                        </div>--%>
                        <div class="col-md-2" style="width: 11.6%; padding-left: 0px;" id="dvdropdownlistStatus1">
                            <%if (RoleFlag == 1)%>
                            <%{%>
                            <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                            <%}%>
                        </div>

                    </div>
                </div>


                <div class="row" style="padding-bottom: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 16.6%;">
                        </div>
                        <div class="col-md-2" style="width: 14.3%;">
                            <div id="dvdropdownEventName1" style="display: none;">
                                <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 196px;">
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 10%;">
                            <div id="dvdropdownEventNature1" style="display: none;">
                                <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 166px;">
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%; padding-left: 105px;">
                            <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>
                        </div>

                        <%--   <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>--%>
                    </div>
                </div>



                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterCompType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterCategory">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterAct">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterCompSubType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterStartDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterLastDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboard1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filtertype1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterrisk1">&nbsp;</div>

                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterpstData1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterUser">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterFY">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterstatus1">&nbsp;</div>


                <div id="grid1"></div>
            </div>
            
            <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
        </div>
    </div>
</asp:Content>
