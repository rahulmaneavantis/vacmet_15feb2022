﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using OfficeOpenXml;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class Avacom_LeaveMappingBulkUpload : System.Web.UI.Page
    {
        List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();
        ErrorMessage objError = null;
        protected static string Client_ID = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            spanErrors.Visible = false;
            if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]))
            {
                Client_ID = Request.QueryString["ClientID"];
            }
            else
            { Client_ID = String.Empty; }

        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Server.MapPath("~/Attendance_Mapping/SampleFile")))
            {
                Directory.CreateDirectory(Server.MapPath("~/Attendance_Mapping/SampleFile"));
            }
            string filePath = Server.MapPath("~/Attendance_Mapping/SampleFile/SampleFileLeaveMapping.xlsx");
            DownloadFile(filePath);
        }
        private void DownloadFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/plain";
                    Response.Flush();
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";
            if (LeaveMappingFileUpload.HasFile && Path.GetExtension(LeaveMappingFileUpload.FileName).ToLower() == ".xlsx")
            {

                try
                {
                    if (!Directory.Exists(Server.MapPath("~/Attendance_Mapping/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Attendance_Mapping/Uploaded/"));
                    }

                    string filename = Path.GetFileName(LeaveMappingFileUpload.FileName);
                    LeaveMappingFileUpload.SaveAs(Server.MapPath("~/Attendance_Mapping/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Attendance_Mapping/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool matchSuccess = checkSheetExist(xlWorkbook, "SampleFileLeaveMapping");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);

                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'SampleFIleLeaveMapping'.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }
        public static bool checkSheetExist(ExcelPackage xlWorkbook, string sheetNameUploaded)
        {
            try
            {
                bool matchFlag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (sheet.Name.Trim().Equals(sheetNameUploaded))
                    {
                        matchFlag = true;
                    }
                } //End ForEach
                return matchFlag;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                int updateCount = 0;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["SampleFIleLeaveMapping"];
                //   List<Client_LeaveMapping> data = new List<Client_LeaveMapping>();
                List<RLCS_LeaveTypeMaster_Mapping> data = new List<RLCS_LeaveTypeMaster_Mapping>();

                if (xlWorksheet != null)
                {
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string ErrorFileName = "ErrorFile_Avacom_LeaveTypeMapping" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                    hdFileName.Value = ErrorFileName;
                    List<String> clientIDList = new List<string>();

                    for (int i = 2; i <= xlrow2; i++)
                    {

                        string AvacomLeaveType = xlWorksheet.Cells[i, 1].Text.Trim();
                        string ClientCode = xlWorksheet.Cells[i, 2].Text.Trim();


                        bool isValidLeaveType = checkValidLeaveType(AvacomLeaveType);
                        if (!isValidLeaveType)
                        {
                            objError = new ErrorMessage();
                            objError.ErrorDescription = "Invalid Avacom_LeaveType => " + AvacomLeaveType;
                            objError.RowNum = i.ToString();
                            lstErrorMessage.Add(objError);
                        }




                        if (lstErrorMessage.Count == 0)
                        {

                            RLCS_LeaveTypeMaster_Mapping _mappingData = new RLCS_LeaveTypeMaster_Mapping()
                            {
                                AVACOM_LeaveType = AvacomLeaveType,
                                LeaveType = ClientCode,
                                ClientID = Client_ID
                            };
                            using (ComplianceDBEntities db = new ComplianceDBEntities())
                            {
                                try
                                {
                                    var existingRecord = (from r in db.RLCS_LeaveTypeMaster_Mapping
                                                          where r.AVACOM_LeaveType == _mappingData.AVACOM_LeaveType && r.ClientID == _mappingData.ClientID
                                                          select r
                                                      ).ToList();


                                    if (existingRecord.Count > 0)
                                    {
                                        existingRecord.First().LeaveType = _mappingData.LeaveType;
                                        updateCount = db.SaveChanges();
                                    }
                                    else
                                        data.Add(_mappingData);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    if (lstErrorMessage.Count == 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            entities.RLCS_LeaveTypeMaster_Mapping.AddRange(data);
                            entities.SaveChanges();
                            // bindGridview();
                            cvUploadUtilityPage.IsValid = false;
                            cvUploadUtilityPage.ErrorMessage = "Upload Successfully!";
                        }

                    }
                    else
                    {

                        GenerateAvacom_LeaveMappingErrorCSV(lstErrorMessage, ErrorFileName);
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Uploaded data Contains some error plz Download Error file to check errors.";

                    }

                }
            }
            catch (Exception ex)
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";

            }
        }
        public bool checkValidLeaveType(string leaveType)
        {
            bool flag = false;

            using (ComplianceDBEntities db = new ComplianceDBEntities())
            {
                flag = db.RLCS_LeaveTypeMaster.Where(x => x.LeaveType == leaveType).Any();

            }
            return flag;
        }
        public void GenerateAvacom_LeaveMappingErrorCSV(List<ErrorMessage> detailView, string FileName)
        {
            spanErrors.Visible = true;

            try
            {

                if (!Directory.Exists(Server.MapPath("~/Leave_Mapping/TempEmpError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Leave_Mapping/TempEmpError/"));
                }

                string FilePath = Server.MapPath("~/Leave_Mapping/TempEmpError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {

                        string content = "Error Description,Row No";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {

                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }
        }
        protected void btnDownloadError_Click(object sender, EventArgs e)
        {
            try
            {
                string filePath = Server.MapPath("~/Leave_Mapping/TempEmpError/" + hdFileName.Value);
                DownloadFile(filePath);
            }
            catch (Exception ex)
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Internal server Error";
            }
        }


    }
    public class ErrorMessage
    {
        public string ErrorDescription { get; set; }
        public string RowNum { get; set; }
    }
}