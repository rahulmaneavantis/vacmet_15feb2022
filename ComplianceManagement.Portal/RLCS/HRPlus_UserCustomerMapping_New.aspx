﻿<%@ Page Title="User-Customer Mapping :: Setup/Onboarding" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_UserCustomerMapping_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_UserCustomerMapping_New" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
            width: 225px;
        }

        .chosen-container {
            width: 50%;
        }
        .btn{
            font-weight: 400;
            font-size:14px;
        }

        .panel-heading .nav > li > a {
            font-size: 17px;
        }
    </style>
    <style>
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }

                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }

        .mt10 {
            margin-top: 10px;
        }
    </style>
    <!--
    <script type="text/javascript">
       $(document).ready(function () {
           $("#addnewbtn").kendoTooltip({ content: "Assign User/Manager to Customer" }).data("kendoTooltip");
           $("#backbtn").kendoTooltip({ content: "Back to User Master" }).data("kendoTooltip");

           $(".k-i-edit").kendoTooltip(

               {
                   content: "Edit"
               });
           $(".k-i-delete").kendoTooltip({ content: "Delete" });

           

           
           /* 
           $("#deletebtn").kendoTooltip({
               filter: "#ContentPlaceHolder1_grdUserCustomerMapping_lbtnDelete_1",
               content: function (e) {
                   return "Delete Mapping";
               }
           });
           */

        });
    </script>
       -->

    <script type="text/javascript">

     
        function OpenPopupUCM(recordID) {
            var myWindowAdv = $("#divUserCustomerMapping");

            function onClose() {
                //$(this.element).empty();
                document.getElementById('<%= lnkBtnRefresh.ClientID %>').click();
            }

            function onOpen(e) {
                kendo.ui.progress(e.sender.element, true);
            }

            myWindowAdv.kendoWindow({
                width: "35%",
                height: "60%",
                title: "Assign Customer to User",
                content: "../RLCS/UserCustomerMappingPage.aspx?ID=" + recordID,
                iframe: "true",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose,
                open: onOpen,
            });

            myWindowAdv.data("kendoWindow").center().open();
            //$('#idloader9').show();
            // $('#showOverdueDetails').attr('src', "../RLCS/RLCS_OverdueCompliances.aspx");
        }
/*
        $(document).ready(function () {
            $("#addnewbtn").kendoTooltip({ content: "Assign User/Manager to Customer" }).data("kendoTooltip");
            $("#backbtn").kendoTooltip({ content: "Back to User Master" }).data("kendoTooltip");

            $(".k-i-edit").kendoTooltip(

                {
                    content: "Edit"
                });
            $(".k-i-delete").kendoTooltip({ content: "Delete" });


*/


            /* 
            $("#deletebtn").kendoTooltip({
                filter: "#ContentPlaceHolder1_grdUserCustomerMapping_lbtnDelete_1",
                content: function (e) {
                    return "Delete Mapping";
                }
            });
            

        });
        */


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row col-md-12 colpadding0">
        <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
            <ul class="nav nav-tabs">
                <li>
                    <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkTabEntityBranch" runat="server" PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx">Entity-Branch</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity/Branch Assignment</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                </li>
            </ul>
        </header>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 colpadding0">
            <asp:ValidationSummary ID="vsUCMPage" runat="server" CssClass="alert alert-danger" ValidationGroup="UCMPageValidationGroup" />
            <asp:CustomValidator ID="cvUCMPage" runat="server" EnableClientScript="False"
                ValidationGroup="UCMPageValidationGroup" Display="None" />
            <asp:Label runat="server" ID="Label1" Style="color: Red"></asp:Label>
        </div>

        <asp:UpdatePanel ID="upUserCustomerList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="mt10 col-md-12 colpadding0">
                    <div class="col-md-3 colpadding0" runat="server" id="divCustomer">
                        <%--<label for="ddlCustomerPage" class="control-label">Customer</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlCustomerPage" Width="95%" AllowSingleDeselect="false"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPage_SelectedIndexChanged">
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-3 colpadding0" runat="server" id="divFilterUsers">
                        <%--<label for="ddlUserPage" class="control-label">User</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlUserPage" AllowSingleDeselect="false" Width="95%"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUserPage_SelectedIndexChanged">
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-3 colpadding0" runat="server" id="div1">
                        <%--<label for="ddlMgrPage" class="control-label">Manager</label>--%>
                        <asp:DropDownListChosen runat="server" ID="ddlMgrPage" AllowSingleDeselect="false" Width="95%"
                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlMgrPage_SelectedIndexChanged">
                        </asp:DropDownListChosen>
                    </div>

                    <div class="col-md-3 colpadding0" style="text-align: right;">
                        <%--<label for="lnkbtnBack" class="hidden-label"></label>--%>
                        <asp:LinkButton Text="Refresh" class="btn btn-primary" runat="server" ID="lnkBtnRefresh" OnClick="btnRefresh_Click" Style="display: none" />
                       <div id="addnewbtn" style="float: left;">
                         <asp:LinkButton Text="Add New" class="btn btn-primary" runat="server" ID="lnkbtnAddNew"
                            OnClientClick="return OpenPopupUCM(0);" data-toggle="tooltip" data-placement="bottom"  />
                           </div>
                        <div id="backbtn" style="float: left;margin-left: 10px;">
                        <asp:LinkButton Text="Back" class="btn btn-primary" runat="server" ID="lnkbtnBack" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx"
                            data-toggle="tooltip" data-placement="bottom"   ToolTip="Back" />
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 colpadding0">
                    <div class="tab-content">
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        <div runat="server" id="performerdocuments" class="tab-pane active">
                            <asp:GridView runat="server" ID="grdUserCustomerMapping" AutoGenerateColumns="false" GridLines="None"
                                AllowSorting="true" OnRowCreated="grdUserCustomerMapping_RowCreated" CssClass="table"
                                CellPadding="4" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdUserCustomerMapping_Sorting"
                                Font-Size="12px" OnPageIndexChanging="grdUserCustomerMapping_PageIndexChanging" ShowHeaderWhenEmpty="true" OnRowCommand="grdUserCustomerMapping_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="SR.No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CustomerName" HeaderText="Customer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="CustomerName" />
                                    <asp:BoundField DataField="UserName" HeaderText="User" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                                    <asp:BoundField DataField="ManagerName" HeaderText="Manager" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ManagerName" />

                                    <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Right" HeaderText="Action" HeaderStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                           <div id="editbtn"  style="float: left;"> 
                                               <asp:LinkButton runat="server" CommandName="EDIT_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnEdit" > <img src="../../Images/edit_icon.png" alt="Edit" title="Edit Mapping" /></asp:LinkButton>
                                               </div>
                                               <div id="deletebtn"  style="float: left;margin-left: 5px;">
                                            <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                                OnClientClick="return confirm('Are you certain you want to delete this Mapping?');"  ><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Mapping" /></asp:LinkButton> 
                                       </div>
                                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No User Customer Mapping record(s) found or all records are filtered out
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div id="record1" runat="server" class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div runat="server" id="DivRecordsScrum">
                                    <p style="padding-right: 0px !Important; color: #666; text-align: -webkit-left;">
                                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                           
                                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                           
                                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" runat="server" id="DivnextScrum" style="margin-bottom: 20px">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                       
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1" style="padding-left: 0px;">
                        <label for="ddlPageSize" class="hidden-label">Show</label>
                        <asp:DropDownList runat="server" Visible="false" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                            class="form-control">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="modal fade" id="divAssignEntitiesDialogpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog w35per">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" style="font-size: large;">
                        Assign Customer to User</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
            </div>
        </div>
    </div>

    <div id="divUserCustomerMapping" style="overflow: hidden">
    </div>

    <script type="text/javascript">
        function CloseCustomerAssignPopUp() {
            $('#divAssignUserCustomerDialog').dialog('close');

            $("#BodyContent_lblCustomerPopUP").selectedIndex = -1;
            $("#BodyContent_lblCustomerPopUP").find("option").attr("selected", false);
            //$("select[id*=BodyContent_lblCustomerPopUP]").attr("disabled", false);

        }
    </script>
</asp:Content>

