﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_CustomerCorporateList_New : System.Web.UI.Page
    {
        protected static int custID = -1;

        protected static int IsSPDist;
        protected static int spID = -1;
        protected static int distID = -1;
        protected int userID;

        protected static string avacomRLCSAPIURL;        
        protected static string ProfileID;
        protected static string RoleCode;
        protected static int loggedInUserCustID = -1;
        protected int? serviceproviderid;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            custID = -1;
            spID = -1;
            distID = -1;
            loggedInUserCustID = -1;

            RoleCode = AuthenticationHelper.Role;
            userID = Convert.ToInt32(AuthenticationHelper.UserID);
            avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
           
            if (!string.IsNullOrEmpty(AuthenticationHelper.ProfileID))
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            else
                ProfileID = Convert.ToString(AuthenticationHelper.UserID);

            if (AuthenticationHelper.Role == "SPADM")
            {
                loggedInUserCustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 0;
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                loggedInUserCustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 1;
            }
            else if (AuthenticationHelper.Role == "IMPT")
            {                
                IsSPDist = 2;
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                loggedInUserCustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                custID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 3;
            }

            serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));

            if (AuthenticationHelper.Role == "HEXCT" && serviceproviderid == 94)
            {
                lnkBtnComAssign.Visible = false;
                lnkBtnComAct.Visible = false;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }
    }
}