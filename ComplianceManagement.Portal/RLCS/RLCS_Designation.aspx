<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_Designation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_Designation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <style>
        .form-control {
            margin-bottom: 8px;
            height: 31px;
            font-family: sans-serif;
        }

        .m-10 {
            /*margin-left: 10px;*/
            padding-left: 24px;
        }
    </style>

       <script>
       
        function ClosePopupWindowAfterSave() {
            console.log("Closepopupwindowaftersave: called.");
            
            setTimeout(function () {
                if (window.parent.$("#divRLCSDesignationAddDialog") != null && window.parent.$("#divRLCSDesignationAddDialog") != undefined)
                    window.parent.$("#divRLCSDesignationAddDialog").data("kendoWindow").close();
            }, 3000);
        }
    </script>
    
</head>
<body style="background: white">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upDesignation" runat="server" UpdateMode="Conditional" onLoad="upDesignation_Load" >
            <ContentTemplate>
                <div class="row col-xs-12 col-sm-12 col-md-12">
                    <asp:ValidationSummary ID="vsUserDetails" runat="server" CssClass="alert alert-danger" ValidationGroup="UserValidationGroup" />
                    <asp:ValidationSummary ID="submission" runat="server" CssClass="alert alert-success" ValidationGroup="submissionValidationGroup" />
                    <asp:CustomValidator ID="designation_Exist" runat="server" EnableClientScript="False"
                        ErrorMessage="Designation already exists." ValidationGroup="UserValidationGroup" Display="None" />
                    <asp:CustomValidator ID="submit" runat="server" EnableClientScript="False"
                        ErrorMessage="Saved Successfully" ValidationGroup="submissionValidationGroup" Display="None" />
                </div>
                   <div style="margin-bottom: 7px; margin-top: 30px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Designation</label>
                    </div>

                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:TextBox runat="server" ID="tbxDesignation" CssClass="form-control"
                            MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Required Designation"
                            ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                            ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid designation."
                            ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>

<%--                 <div style="margin-bottom: 7px" class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label class="control-label">
                            Status</label>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <asp:DropDownListChosen runat="server" ID="ddlStatus" CssClass="form-control" Width="100%" AllowSingleDeselect="false"  AutoPostBack="false">
                            <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="In-Active" Value="0"></asp:ListItem>
                        </asp:DropDownListChosen>                        
                    </div>
                </div>--%>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: 20px">
                        <asp:Button Text="save" id="btn_Save" runat="server" CssClass="btn btn-primary float-right" OnClick="btn_Save_Click" />
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: 20px">
                        <asp:Button Text="Cancel" ID="btn_Cancle" runat="server" CssClass="btn btn-primary" OnClick="btn_Cancle_Click"/>
                    </div>
                </div>
                <div style="margin-bottom: 7px;   float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                </div>
            </ContentTemplate>

        </asp:UpdatePanel>
    </form>
</body>
</html>
