﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_MISDownload : System.Web.UI.Page
	{
		List<string> _misPath = new List<string>();
		private HttpResponseMessage response;
		private HttpClient _httpClient { set; get; }
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(Request.QueryString["MISPaths"]))
				{
					string[] commandArg = Request.QueryString["MISPaths"].ToString().Split(',');
					string flag = Request.QueryString["Flag"].ToString();
					if (commandArg.Length > 0)
					{
						foreach (var item in commandArg)
						{
							//var path= ;
							_misPath.Add(item.Replace("|", "'"));
						}
					}
					bool r= GetMIS_Documents(_misPath, flag);
					if (!r)
						ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Something went Wrong');", true);
					
				}
			}
			catch (Exception ex)
			{

			}
		}

		protected bool GetMIS_Documents(List<string> _misPath, string flag)
		{
			bool documentSuccess = true;
			{
				string baseAddress = ConfigurationManager.AppSettings["RLCS_MIS_Path"].ToString();
				_httpClient = new HttpClient();
				_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL_Portal"]);
				_httpClient.Timeout = new TimeSpan(0, 15, 0);
				_httpClient.DefaultRequestHeaders.Accept.Clear();
				_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				_httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
				_httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
				_httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

				using (ZipFile MISDocument = new ZipFile())
				{
					foreach (var obj in _misPath)
					{ string filepath = baseAddress + obj;
						string filename = String.Empty;
						string contentType = String.Empty;
						try
						{
							response = _httpClient.GetAsync("Masters/Generate?FilePath=" + filepath).Result;
							var statuscode = Convert.ToInt32(response.StatusCode);
							if (statuscode == 200)
							{
								var byteFile = response.Content.ReadAsByteArrayAsync().Result;
								if (byteFile.Length > 0)
								{

									string extension = Path.GetExtension(obj);
									contentType = "application/" + extension.ToString().Replace(".", "").ToLower() + "";
									filename = "MIS" + extension;
									MISDocument.AddEntry(obj, byteFile);
								}
							}
						}
						catch (Exception ex)
						{ documentSuccess = false; }
					}

					if (documentSuccess)
					{
						HttpResponse res = HttpContext.Current.Response;
						var zipMs = new MemoryStream();
						WebClient req = new WebClient();
						MISDocument.Save(zipMs);
						zipMs.Position = 0;

						byte[] Filedata = zipMs.ToArray();
						res.Buffer = true;
						res.ClearContent();
						res.ClearHeaders();
						res.Clear();
						res.ContentType = "application/zip";
						res.AddHeader("content-disposition", "attachment; filename=" + "MIS" + "-Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
						res.BinaryWrite(Filedata);
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest();
					}
				}
			}
			//{
			//	bool documentSuccess = false;
			//	string baseAddress = ConfigurationManager.AppSettings["RLCS_MIS_Path"].ToString();
			//	string filePath = string.Empty;
			//	string directory = string.Empty;

			//	using (ZipFile MISDocument = new ZipFile())
			//	{
			//		try
			//		{
			//			foreach (var obj in _misPath)
			//			{
			//				//string[] s = obj.Split('\\');

			//				//if (flag == "B")
			//				//{
			//				//	string[] paths = { "MIS", s[1], s[2], s[4], s[5] };
			//				//	 directory = Path.Combine(paths);
			//				//}
			//				//else {
			//				//	string[] paths = { "MIS", s[1], s[2], s[3]};
			//				//	directory = Path.Combine(paths);

			//				//}


			//				filePath = baseAddress + obj;
			//				if (File.Exists(filePath))
			//				{
			//					MISDocument.AddFile(filePath);
			//				}
			//			}
			//			documentSuccess = true;
			//		}
			//		catch (Exception ex)
			//		{
			//			documentSuccess = false;
			//		}
			//		if (documentSuccess)
			//		{
			//			var zipMs = new MemoryStream();
			//			WebClient req = new WebClient();
			//			HttpResponse response = HttpContext.Current.Response;
			//			MISDocument.Save(zipMs);
			//			zipMs.Position = 0;

			//			byte[] Filedata = zipMs.ToArray();
			//			response.Buffer = true;
			//			response.ClearContent();
			//			response.ClearHeaders();
			//			response.Clear();
			//			response.ContentType = "application/zip";
			//			response.AddHeader("content-disposition", "attachment; filename=" + "MIS" + "-Documents-" + DateTime.Now.ToString("ddMMyy") + ".zip");
			//			response.BinaryWrite(Filedata);
			//			HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
			//			HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
			//			HttpContext.Current.ApplicationInstance.CompleteRequest();
			//		}
			//	}
			//}
			return documentSuccess;
		}
	}
}
