﻿<%@ Page Title="Daily Updates" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_DailyUpdateList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_DailyUpdateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">  
                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>                    
                    <%--<p style="color: #999; margin-top: 10px; margin-left: 5px;">Entries</p>--%>
                </div>
        <div style="margin-bottom: 4px">          
        <asp:GridView runat="server" ID="grdDailyUpdateList" AutoGenerateColumns="false" PageSize="5" AllowPaging="true" AutoPostBack="true"
            CssClass="table" GridLines="none"  OnPageIndexChanging="grdDailyUpdateList_PageIndexChanging"
            Width="100%"  DataKeyNames="ID">            
                <columns>
                     <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                <asp:BoundField DataField="Title" HeaderText="Title" ItemStyle-Height="20px" HeaderStyle-Height="20px" />  <asp:TemplateField  Visible="false">
        <HeaderTemplate>Description</HeaderTemplate>
        <ItemTemplate>
            <%# Eval ("Description").ToString ().Length > 60 ? Eval ("Description").ToString ().Substring (0, 60) + "..." : Eval("Description").ToString ()%>
        </ItemTemplate>
    </asp:TemplateField>
                   
               <asp:TemplateField HeaderText = "View" ItemStyle-Width="30">
                 <ItemTemplate>
                     <a  onclick="fmodaldailyupdate(this)" data-title="<%#Eval("Title")%>" data-desc='<%#Eval("Description").ToString ().Replace("'","\"")%>' data-date="<%#Eval("CreatedDate")%>" style="height:65px;width:60px; cursor:pointer;" ><img src="../Images/view-icon-new.png" /></a>
                 </ItemTemplate>    
                 </asp:TemplateField> 
                <%--<asp:TemplateField HeaderText = "View" ItemStyle-Width="30">
            <ItemTemplate>
                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Id", "~/MailDetails.aspx?Id={0}") %>'
                    Text="View" />
            </ItemTemplate>
        </asp:TemplateField>--%>
                    </columns>    
                     <RowStyle CssClass="clsROWgrid"   />
                <HeaderStyle CssClass="clsheadergrid"    />
             <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>         
            </asp:GridView>  
                    </div>   
                    <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnDownload" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="ddlType" />--%>
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 900px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    
                </div>
                <div class="modal-body" id="dailyupdatecontent">
                      <h2 id="dailyupdatedate" style="margin-top:1px">Daily Updates: November 3, 2016</h2>
                    <h3 id="dailyupdatedateInner">Daily Updates: November 3,
                    </h3>
                    <p id="dailytitle"></p>
                    <div id="contents" style="color: #666666;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function timeconvert1(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
    (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }
        function getmonths1(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }
        function fmodaldailyupdate(obj)
        { 
            var title = $(obj).attr('data-title');
            var desc = $(obj).attr('data-desc');
            var date = $(obj).attr('data-date');
            var objDate = new Date(date);
            var mon_I = getmonths1(objDate);            $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
            $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
            $("#contents").html(desc);
            $("#dailytitle").html(title);            $('#NewsModal').modal('show');
        }
        $(document).ready(function () {
         
            fhead('Daily Updates');
        });
    </script>
</asp:Content>
