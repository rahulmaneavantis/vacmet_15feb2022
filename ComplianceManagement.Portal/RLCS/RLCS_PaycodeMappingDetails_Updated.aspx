﻿<%@ Page Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_PaycodeMappingDetails_Updated.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_PaycodeMappingDetails_Updated" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />
    <style type="text/css">
        .k-textbox > input {
            text-align: left;
        }
        .k-tooltip {
            margin-top: 5px;
        }
        .k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
            color: #2e2e2e;
            background-color: #d8d6d6 !important;
        }
        .k-i-plus-outline {
            margin-top: -4px;
        }
        .k-grid, .k-listview {
            margin-top: 10px;
        }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        .k-tooltip-content {
            width: max-content;
        }
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px;
            line-height: normal;
        }
        .k-calendar-container {
            background-color: white;
            width: 217px;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: -7px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2px;
            margin-bottom:0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
             min-width: 19px;
              min-height: 25px;
              border-radius: 25px;
              background-color: transparent;
              border-color: transparent;
              border-color: transparent;
              margin-left:0px;
              padding-right:0px;
              padding-left:0px;
        }
         .k-button.k-state-active, .k-button:active{
            color:black;
        }
        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
            padding-right:18px;
        }

        div.k-window-content{
            overflow:hidden;
        }
        .k-grid-pager {
           
             margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

           .btn {
         height:36px;
         font-weight:400;
         font-size:14px;
         }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
           .k-filter-row th, .k-grid-header th[data-index='8'] {
            text-align:center;
            color:#535b6a;
        }  
           .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
               .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
           .k-dropdown-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input {
    padding-top: 5px;
    border-radius: 3px 0 0 3px;
}
             label.k-label:hover{
              color:#1fd9e1;
          }
    </style>

    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }

                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
    .alert-danger {	
        color: #5c9566;	
        background-color: #dff8e3;	
        border-color: #d6e9c6;	
        }	
    .alert {	
        /*padding: 15px;	
        margin-bottom: 20px;*/	
        border: 1px solid transparent;	
        border-radius: 4px;	
    }
</style>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#divRLCSPaycodeUploadDialog').hide();
            $('#divRLCSPaycodeAddDialog').hide();
            
            GetDataPaycodeDetailsWeb();
            SetGridTooltip();
            //Search 
            
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            //END
        });
        function onClose() {
            debugger;
            document.getElementById('<%= lnkbtnRefresh.ClientID %>').click();
        }
        function OpenAddEmployeePopup() {
            debugger;
            var Client_ = $("#ContentPlaceHolder1_ddlClientList").val();
            var Client = '<%= ClientID%>';

            if (Client_ != undefined || Client_ != null) {
                $('#divRLCSPaycodeAddDialog').show();
                var myWindowAdv = $("#divRLCSPaycodeAddDialog");
                myWindowAdv.kendoWindow({
                    width: "90%",
                    height: '92%',
                    title: "Paycode Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/RLCS/RLCS_PaycodeMappingDetail_Dynamic.aspx?ClientID=" + Client_ );
            }
            return false;
        }

        function EditEmployeePopup(item) {
            debugger
            var clientid = item.CPMD_ClientID;
            var id = item.ID;

            if (clientid != undefined || clientid != null) {
                $('#divRLCSPaycodeAddDialog').show();
                var myWindowAdv = $("#divRLCSPaycodeAddDialog");

                myWindowAdv.kendoWindow({
                    width: "90%",
                    height: '60%',
                    title: "Paycode Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/RLCS/RLCS_PaycodeMappingDetail_Dynamic.aspx?ClientID=" + clientid + "&ID=" + id + "&Edit=YES");
            }
            return false;
        }

        function OpenUploadWindow() {
            debugger;
            var Client_ = $("#ContentPlaceHolder1_ddlClientList").val();
            Client = '<%=ClientID%>';
            $('#divRLCSPaycodeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeUpload').attr('src', "/Setup/UploadPaycodeFile?Client=" + Client_);
            var myWindowAdv = $("#divRLCSPaycodeUploadDialog");
            myWindowAdv.kendoWindow({
                width: "50%",
                height: "50%",
                title: "Upload Paycode Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;

        }

        function ddlChangeClient() {
            
            GetDataPaycodeDetailsWeb();
           
        }
       
        function GetDataPaycodeDetailsWeb() {
            debugger
            var Client = $("#ContentPlaceHolder1_ddlClientList").val();
            jQuery.ajax({
                type: "POST",
                url: "RLCS_PaycodeMappingDetails_Updated.aspx/BindPaycodeDetailsWeb", //It calls our web method  
                contentType: "application/json; charset=utf-8",
                data: "{'ClientID':'" + Client + "'}",
                dataType: "Json",
                success: function (data) {                    
                    var dataArray = data.d;                    
                    BindGrid(dataArray);                   
                },
                error: function (d) {
                }
            });
        }

        function DeletePaycodeDetails(item) {
            var record = item.ID;
            var client = item.CPMD_ClientID;
            debugger
            jQuery.ajax({
                type: "POST",
                url: "../RLCS/RLCS_PaycodeMappingDetails_Updated.aspx/DeletePaycodeDetailsWeb", //It calls our web method  
                contentType: "application/json; charset=utf-8",
                data: "{ 'recordID': '" + record + "','clientID':'" + client + "' }",
                dataType: "Json",
                success: function (data) {
                    var dataArray = data.d;
                    UpdatePaycodeDetails(dataArray)
                },
                error: function (d) {
                }
            });
        }

        function UpdatePaycodeDetails() {
            var Client = '<%= ClientID%>';
            debugger
            jQuery.ajax({
                type: "POST",
                url: "../RLCS/RLCS_PaycodeMappingDetails_Updated.aspx/UpdatePaycodeDetailsWeb", //It calls our web method  
                contentType: "application/json; charset=utf-8",
                data: "{ 'clientID':'" + Client + "' }",
                dataType: "Json",
                success: function (data) {
                    GetDataPaycodeDetailsWeb();
                },
                error: function (d) {
                }
            });
        }

        function BindGrid(dataArray) {
            debugger
            
            var gridview = $("#gridPaycode").kendoGrid({
                dataSource: dataArray,
                schema: {
                    model: {
                        fields: {
                            CPMD_Sequence_Order: { type: "number" },
                            CPMD_Header: { type: "string" },
                            CPMD_PayCode: { type: "string" },
                            CPMD_Deduction_Type: { type: "string" },
                            CPMD_appl_ESI: { type: "string" },
                            CPMD_appl_PF: { type: "string" },
                            CPMD_Appl_PT: { type: "string" },
                            CPMD_Appl_LWF: { type: "string" },
                        }
                    }
                },
                // editable: "inline",
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh:false,
                    pageSizes:true,
                    pageSize:10,
                    buttonCount: 3,
                    change: function (e) {
                    }
                },
               
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                   {
                       title: "Header<br/> Name",
                        field: "CPMD_Header",
                        width: "10%;",
                        
                        attributes: {
                                //border-width: 0px 1px 1px 0px
                            style: 'white-space: nowrap;text-align:left;'
                        },filterable: { multi: true, search: true },

                    },
                    {
                        field: "CPMD_Deduction_Type", title: 'Paycode<br/> Type',    
                        width: "8%",
                        attributes: {
                            //border-width: 0px 1px 1px 0px;
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CPMD_PayCode", title: 'Paycode',
                        width: "10%",
                        filterable: {
                            extra: false,
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "CPMD_Sequence_Order", title: 'Seq.<br/>Order',
                        width: "7%",
                        attributes: {
                        //border-width: 0px 1px 1px 0px;'
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false, 
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CPMD_appl_ESI", title: 'ESI <br/>Applicable',
                        width: "8%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CPMD_appl_PF", title: 'PF <br/>Applicable',
                        width: "8%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CPMD_Appl_PT", title: 'PT <br/>Applicable',
                        width: "8%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CPMD_Appl_LWF", title: 'LWF<br/> Applicable',
                        width: "8%",
                        attributes: {
                            //border-width: 0px 1px 1px 0px;
                            style: 'white-space: nowrap;'
                        },
                        //template: '#:checkCustomerActive(Status)#',
                        filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command:
                            [
                          { name: "edit", text: "", iconClass: ".k-icon k-i-pencil k-icon k-edit", className: "k-grid-edit" },
                          { name: "delete", text: "", iconClass: ".k-icon k-i-delete k-icon", className: "k-grid-delete" },
                            ],
                        title: "Action",
                        lock: true, width: "8%;",
                        attributes: {
                            //border-width: 0px 1px 1px 0px
                            style: 'white-space: nowrap;'
                        },
                    }
                    
                ],
                dataBound: function () {
                    var grid = this;
                    var trs = this.tbody.find('tr').each(function () {
                        var item = grid.dataItem($(this));
                        if (item.CPMD_PayCode == "CLIENTID") {
                            $(this).find('.k-grid-delete').hide();
                        }
                        if (item.CPMD_PayCode == "EMP_ID") {
                            $(this).find('.k-grid-delete').hide();
                        }
                        if (item.CPMD_PayCode == "LOP") {
                            $(this).find('.k-grid-delete').hide();
                        }
                        if (item.CPMD_PayCode == "NETPAY") {
                            $(this).find('.k-grid-delete').hide();
                        }
                        if (item.CPMD_PayCode == "TOTALDEDUCTION") {
                            $(this).find('.k-grid-delete').hide();
                        }
                        if (item.CPMD_PayCode == "TOTALEARNING") {
                            $(this).find('.k-grid-delete').hide();
                        }
                        if (item.CPMD_PayCode == "WORKINGDAYS") {
                            $(this).find('.k-grid-delete').hide();
                        }

                    });
                }
                
            });

            $('#filter').on('input', function (e) {
                debugger;
                var grid = $('#gridPaycode').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field) {
                        //var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        if (x.field == "CPMD_Header" || x.field == "CPMD_Deduction_Type" || x.field == "CPMD_PayCode") {
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: e.target.value
                            })
                        }
                        else if (x.field == "CPMD_Sequence_Order") {
                           // if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                           // }

                        }
                    }
                });
                grid.dataSource.filter(filter);
            });
           
        }


        //function Edit(e) {
        $(document).on("click", ".k-grid-edit", function (e) {
            debugger
            var item = $("#gridPaycode").data("kendoGrid").dataItem($(this).closest("tr"));
            
            EditEmployeePopup(item)
        });


        $(document).on("click", ".k-grid-delete", function (e) {
            debugger
            var item = $("#gridPaycode").data("kendoGrid").dataItem($(this).closest("tr"));
            var recordID = item.ID;
            var clientID = item.CPMD_ClientID;
            if (item != null && item != undefined) {
                
                window.kendoCustomConfirm("Are you certain you want to delete this Paycode?", "Confirm").then(function () {
                            debugger
                             DeletePaycodeDetails(item)
                        }, function () {
                            // For Cancel
                        });
                }
            }
        );
                <%--var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                if(item!=null && item!=undefined){
                    var cust = item.ID;
                    if(cust!=null && cust!=undefined){
                        window.kendoCustomConfirm("This will also delete all the sub-entities/branch associated with customer. Are you certain you want to delete this customer?","Confirm").then(function () {
                            
                            $.ajax({
                                type: 'POST',
                                url: "<%=avacomRLCSAPIURL%>DeleteCustomer?CustomerID=" + cust,
                                dataType: "json",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    xhr.setRequestHeader('Content-Type', 'application/json');
                                },
                                success: function (result) {
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
                                    popupNotification.show("Deleted Successfully", "error");
                                },
                                error: function (result) {
                   
                                }
                            });
                        }, function () {
                            // For Cancel
                        });  
                    }
                }

                return false;--%>



        //$(document).ready(function () {
            
          
        //});


        function SetGridTooltip() {
            debugger
            $("#gridPaycode").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });

            $("#gridPaycode").kendoTooltip({
                filter: ".k-grid-delete",
                content: function (e) {
                    return "Delete";
                }
            });
            $("#ContentPlaceHolder1_btnAddEmployee").kendoTooltip({
                content: function (e) {
                    return "Add New Paycode";
                }
            });
            $("#ContentPlaceHolder1_btnUploadEmployee").kendoTooltip({
                content: function (e) {
                    return "Upload Paycode";
                }
            });

            $("#btnAddEmployee").kendoTooltip({ content: "Add New" });
            
            $("#btnUploadEmployee").kendoTooltip({ content: "Upload Paycode Header" });
        }

      
        
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:LinkButton Text="Refresh" runat="server" ID="lnkbtnRefresh" OnClick="btnRefresh_Click" CssClass="k-hidden" />
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>
                        <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    

    <asp:UpdatePanel ID="upEmpMasterCADMN" runat="server" UpdateMode="Conditional" OnLoad="upEmpMasterCADMN_Load">
        <ContentTemplate>
            <asp:HiddenField ID="hdnClientID" runat="server" Value="" />
            <div style="width: 100%; margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                  <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                                       
                                        <asp:CustomValidator ID="cvDuplicate" runat="server" EnableClientScript="False"
                                            ValidationGroup="DocGenValidationGroup" Display="None" />
            </div>
            <div style="width: 100%">
                <table width="100%">
                    <tr>
                        <td style="width: 20%; display:none;">
                            <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                        runat="server" Style="text-decoration: none; color: Black" />
                                </ItemTemplate>
                                <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                                <SeparatorStyle Font-Size="12" />
                                <SeparatorTemplate>
                                    &gt;
                                </SeparatorTemplate>
                            </asp:DataList>
                        </td>
                        <td style="width: 30%;">
                            <div id="divCustomerfilter" runat="server">
                                <div style="width: 80%; float: left;">
                                    <asp:DropDownListChosen runat="server" ID="ddlClientList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="form-control" AutoPostBack="true" onchange="ddlChangeClient()"  NoResultsText="No results match." Width="350px"
                                         AllowSingleDeselect="true" />
                                </div>
                            </div>
                        </td>
                        <td style="width: 30%;">
                            <div style="width: 80%; float: left;">
                                <%--<asp:TextBox runat="server" ID="tbxFilter" Width="100%" MaxLength="50" placeholder="Filter" CssClass="form-control" /> --%>   
                                <input id='filter' class='k-textbox' placeholder="Search" CssClass="form-control" style="width: -webkit-fill-available;" autocomplete="off"/>                           
                            </div>                            
                        </td>
                        <td style="width: 10%;">
                            <button id="btnAddEmployee" runat="server" onclick="return OpenAddEmployeePopup()" style="font-weight:400;height:36px" class="btn btn-primary k-btnUpload"><span class="k-icon k-i-plus-outline"></span>Add New</button>
                            <%--<asp:LinkButton runat="server" ID="btnAddEmployee" class="btn btn-primary" CssClass="k-button" ToolTip="Paycode Add" OnClientClick="return OpenAddEmployeePopup()"><span class="k-icon k-i-plus-outline"></span>Add New</asp:LinkButton>--%>
                        </td>
                        <td style="width: 10%;">
                            <%--<asp:LinkButton Text="Upload" runat="server" ID="btnUploadEmployee" OnClientClick="return OpenUploadWindow()" CssClass="k-button" />--%>
                            <button id="btnUploadEmployee" runat="server" onclick="return OpenUploadWindow()" style="font-weight:400;height:36px" class="btn btn-primary k-btnUpload">Upload</button>
                        </td>
                    </tr>

                </table>
            </div>
        </ContentTemplate>
        <Triggers>
             <asp:PostBackTrigger ControlID="ddlClientList" />
            
        </Triggers>
    </asp:UpdatePanel>
  
    <div id="gridPaycode"></div>

    <div id="divRLCSPaycodeUploadDialog">
        <div class="chart-loading colpadding0" id="loader"></div>
        <iframe id="iframeUpload" style="width: 100%; height: 400px; border: none"></iframe>
    </div>
    <div id="divRLCSPaycodeAddDialog">
        <div class="chart-loading colpadding0" id="loader1"></div>
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none;"></iframe>
    </div>
     <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" <%--style="border: none;"--%>>
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
       
</asp:Content>
