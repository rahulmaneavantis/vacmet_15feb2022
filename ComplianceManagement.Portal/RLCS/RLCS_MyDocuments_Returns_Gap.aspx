﻿<%@ Page Title="Returns :: My Documents" Language="C#" AutoEventWireup="true" MasterPageFile="~/RLCSCompliance.Master" CodeBehind="RLCS_MyDocuments_Returns_Gap.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyDocuments_Returns_Gap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    <title></title>

    <style>
       .k-grid-content { min-height:250px; }       
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>
    
    <script type="text/jscript">
        $(document).ready(function () {


              function startChange() {
                debugger;
                $("#Enddatepicker").data("kendoDatePicker").value(null);

                var dt = new Date('<% =newReturnsMonth%>/31/<% =newReturnsYear%>');
                //var dt = new Date("05/31/2018");
                var dtnew = new Date("05/30/2050");
                var startDate = start.value(),
                    endDate = end.value();
                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());

                    end.min(startDate);                    
                    //if ((startDate.getMonth() >= 05 && startDate.getFullYear() == 2018) || startDate.getFullYear() > 2018) {
                    if ((startDate.getMonth() >= <% =newReturnsMonth%> && startDate.getFullYear() == <% =newReturnsYear%>) || startDate.getFullYear() > <% =newReturnsYear%>) {
                        end.max(new Date(dtnew));
                    }
                    else {
                        end.max(new Date(dt));
                    }
                    //endDate.setDate(startDate.getDate());
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                var y = startDate.getFullYear();
                if (y == 2020) {
                    y = 0;
                }
				else if (y == 2019) {
                    y = 1;
                }
                else if (y == 2018) {
                    y = 2;
                }
                else if (y == 2017) {
                    y = 3;
                }
                else if (y == 2016) {
                    y = 4;
                }
                $("#ddlMonth").data("kendoDropDownList").select(parseInt(startDate.getMonth()));
                $("#ddlYear").data("kendoDropDownList").select(y);
                dropDownFilter();
            }

            function endChange() {

                var endDate = end.value(),
                    startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    //start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    //start.max(endDate);
                    end.min(endDate);
                }

                dropDownFilter();
            }

            var start = $("#Startdatepicker").kendoDatePicker({
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMMM yyyy",
                // specifies that DateInput is used for masking the input element
                value: new Date(),
                dateInput: true,
                change: startChange
            }).data("kendoDatePicker");

            var end = $("#Enddatepicker").kendoDatePicker({
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMMM yyyy",
                // specifies that DateInput is used for masking the input element
              //  value: new Date(),
                dateInput: true,
                change: endChange
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());


            setactivemenu('leftnoticesmenu');
            	try{
                ApiTrack_Activity("Returns", "pageView", null);
                }catch(e){}
            fhead('My Documents/ Returns');
        });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {       
                debugger;
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup(item.RecordID,"SOW05","View");
            //analytics("Returns","Download",$('#CName').val(),1);
             try{
             ApiTrack_Activity('Returns','Download',item.RecordID);
        }catch(e){}
            return true;
        });

        $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup(item.RecordID,"SOW05","View");
           // analytics("Returns","Overview",$('#CName').val(),1);
                try{
                ApiTrack_Activity('Returns','Overview',item.RecordID);
           }catch(e){}
            return true;
        });

        function OpenDownloadOverviewpup(RecordID,DocType,Event) {         
            $('#DownloadViews').attr('src', "RLCS_MyRegistrations.aspx?RLCSRecordID=" + RecordID + "&RLCSDockType=" + DocType+"&Event="+Event);   
             
        }

        function OpenDocumentOverviewpup(RecordID,DocType,Event) {
          
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?RecordID=" + RecordID + "&DocType="+DocType+"&Event="+Event)
        }

        $(document).on("click", "#grid1 tbody tr .ob-download", function (e) {
          debugger;
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup1(item.LM_TaskID, item.LM_State, item.LM_Location, item.AVACOM_BranchID, item.LM_PayrollMonth, item.LM_PayrollYear, item.ScheduleList,"W");
            analytics('Returns','DocView',$('#CName').val(),1);
                try{
                ApiTrack_Activity('Returns','DocView',item.RecordID);
                }catch(e){}
            return true;
        });

        $(document).on("click", "#grid1 tbody tr .ob-downloadcompliance", function (e) {
            debugger;
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup1(item.LM_TaskID, item.LM_State, item.LM_Location, item.AVACOM_BranchID, item.LM_PayrollMonth, item.LM_PayrollYear, item.ScheduleList, "C");
            analytics('Returns', 'DocView', $('#CName').val(), 1);
            try {
                ApiTrack_Activity('Returns', 'DocView', item.RecordID);
            } catch (e) { }
            return true;
        });

        function OpenDownloadOverviewpup1(LM_TaskID,LM_State,LM_Location,AVACOM_BranchID,LM_PayrollMonth,LM_PayrollYear, ScheduleList,FileType) {
          debugger;
            $('#DownloadViews').attr('src', "RLCS_MyDocuments_Returns_Gap.aspx?LM_TaskID=" + LM_TaskID + "&State=" + LM_State + "&Location=" + LM_Location + "&BranchID=" + AVACOM_BranchID + "&Month=" + LM_PayrollMonth + "&Year=" + LM_PayrollYear + "&ScheduleList=" + ScheduleList + "&FileType=" + FileType);
        }

        function OpenDocumentOverviewpup1(ScheduleList,Type,Flag) {   
        debugger;
          
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '1200px');
            var ScopeID = "SOW05";
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?ScheduleList=" + ScheduleList + "&FileType=" + Flag + "&ScopeID=" + ScopeID);
        }

        $(document).on("click", "#grid1 tbody tr .ob-overview", function (e) {
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup1(item.ScheduleList,"View","W");
            analytics('Returns','OverView',$('#CName').val(),1);
                try{
                ApiTrack_Activity('Returns','OverView',item.RecordID);
                }catch(e){}
            return true;
        });

        $(document).on("click", "#grid1 tbody tr .ob-overviewcompliance", function (e) {
            var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup1(item.ScheduleList, "View","C");
            analytics('Returns', 'OverView', $('#CName').val(), 1);
            try {
                ApiTrack_Activity('Returns', 'OverView', item.RecordID);
            } catch (e) { }
            return true;
        });

        

       // function OpenDocumentOverviewpup1(LM_TaskID,LM_State,LM_Location,AVACOM_BranchID,LM_PayrollMonth,LM_PayrollYear) {
         
       //     $('#divViewDocument').modal('show');
       //     $('.modal-dialog').css('width', '1200px');
        //     $('#OverViews').attr('src', "RLCS_MyDocuments_Returns_Gap.aspx?LM_TaskID=" +LM_TaskID+ "&State=" +LM_State+ "&Location=" + LM_Location+"&BranchID="+AVACOM_BranchID+ "&Month=" + LM_PayrollMonth+"&Year="+LM_PayrollYear);
       // }

        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");           
        }

        function fcloseStory(obj) {
          
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);            
            $(upperli).remove();
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            //fCreateStoryBoard('ddlMonth', 'filtermonth', 'mon');
            //fCreateStoryBoard('ddlYear', 'filteryear', 'yer');
            //CheckFilterClearorNot();
            CheckFilterClearorNotMain();
        };

        function BindLocationFilter() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/State/Location/Branch",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    var month =<% =newReturnsMonth%>;
                    var year =<% =newReturnsYear%>;
                    debugger;
                    if (month < $("#ddlMonth").val() && year >= $("#ddlYear").val()) {

                        var filter = { logic: "or", filters: [] };
                        var values = this.value();

                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {

                        var filter = { logic: "or", filters: [] };
                        var values = this.value();

                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#chkAll_Main').removeAttr('checked');                       
                    $('#chkAll').removeAttr('checked');
                    $('#dvbtndownloadDocumentMain').css('display', 'none');   
                },        
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =Path%>GetAssignedEntitiesLocationsList?customerId=<% =CustId%>&userId=<% =UserId%>&profileID=<% =ProfileID%>",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }                  
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }
     
        function BindFirstGrid() {
            $('#grid1').hide();
            $('#grid').show();

            $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<% =Path%>GetDocuments_BranchList_ReturnChallan_Historical?customerID=<% =CustId%>&userID=<% =UserId%>&scopeID=SOW05&MonthId=&YearID=&profileID=<% =ProfileID%>&FromDate='+ $("#Startdatepicker").val()+ '&EndDate='+ $("#Enddatepicker").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        total: function (response) {
                            return response.Result.length;
                        }
                    }
                },
                //toolbar: kendo.template($("#template").html()),
                //height: 471,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                   // pageSizes: true,
                    //pageSizes: [ 10 , 25, 50 ],
                    change: function (e) {                      
                        $('#chkAll').removeAttr('checked');
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },                
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                  {
                        template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' value=#=RecordID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",
                        width: "3%;"//, lock: true
                    },
                   
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                         width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EntityClientName", title: 'Entity/Client',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "SM_Name", title: 'State',
                        width: "13.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LM_Name", title: 'Location',
                        width: "13.7%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "AVACOM_BranchName", title: 'Branch',
                        width: "18%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: false, field: "MonthName", title: "Month" },
                    { hidden: false, field: "PayrollYear", title: "Year" },
                    {
                        command: [
                            { name:"editLable1", text: "No Document" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,width: "12.7%;"
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                    
                    // Selects all delete buttons
                    $("#grid tbody tr .k-grid-edit1").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });

                      $("#grid tbody tr .k-grid-edit2").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });

                    $("#grid tbody tr .k-grid-editLable1").each(function () {
                      
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Available') {
                            $(this).remove();
                        }                    
                    });
                }
            });

            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
                         
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",                  
                content: function (e) {         
                   var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest('tr'));                                   
                   return dataItem.DocStatus;                
                }
            });

              $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",                  
                content: function (e) {                                                             
                   return "View";                
                }
            });
        }

          $(document).on("click", "#sel_chkbx", function (e) {
             
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

        $(document).on("click", "#chkAll", function (e) {  
          
                if ($('input[id=chkAll]').prop('checked')) {

                    $('input[name="sel_chkbx"]').each(function (i, e) {
                        e.click();
                    });
                }
                else {                   
                    $('input[name="sel_chkbx"]').attr("checked", false);
                }
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

          function selectedDocument(e) {
              debugger;
                 e.preventDefault();
              
            if (($('input[name="sel_chkbx"]:checked').length) == 0 && ($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
                return;
            }
            var checkboxlist = [];

            if (($('input[name="sel_chkbx"]:checked').length) > 0)
            {
                $('input[name="sel_chkbx"]').each(function (i, e) {
                    if ($(e).is(':checked')) {
                        checkboxlist.push(e.value);
                    }
                });

              
              
                $('#DownloadViews').attr('src', "RLCS_MyRegistrations.aspx?RLCSRecordID=" + checkboxlist.join(",") + "&RLCSDockType=SOW05&Event=Download");   
                analytics('Returns','DocView',$('#CName').val(),1);
            }

            if (($('input[name="sel_chkbx_Main"]:checked').length) > 0)
            {
                $('input[name="sel_chkbx_Main"]').each(function (i, e) {
                    if ($(e).is(':checked')) {
                        checkboxlist.push(e.value);
                    }
                });

                $('#DownloadViews').attr('src', "../RLCS/RLCSDownloadDoc.aspx?RLCSIds=" + checkboxlist.join(",") + "&IsFlag=Return");
              
            }
              
            return false;
        }


         function BindSecondGrid(){
             var customerID =<% =CustId%>;
             
             $('#grid').hide();
             $('#grid1').show();

                $("#grid1").kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                //url: "<% =Path%>GetDocuments_BranchList_Returns",
                                url: '<% =Path%>GetDocuments_BranchList_Returns?customerID=<% =CustId%>&userID=<% =UserId%>&MonthId=' + $("#ddlMonth").val() + '&YearID=' + $("#ddlYear").val() + '&profileID=<% =ProfileID%>&FromDate='+ $("#Startdatepicker").val()+ '&EndDate='+ $("#Enddatepicker").val(),                         
                                type: 'Get',                               
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },

                                dataType: 'json'
                            }
                        },
                        pageSize:10,
                        schema: {
                            data: function (response) {
                                return response.Result;
                            },
                            total: function (response) {
                                return response.Result.length;
                            }
                        }
                    },

                    //height: 471,
                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: {
                        refresh: true,                      
                        change: function (e) {
                            $('#chkAll_Main').removeAttr('checked');
                            $('#dvbtndownloadDocumentMain').css('display', 'none');
                        },                     
                        buttonCount: 3
                    },
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    selectable: true,
                    columns: [
                      {
                        template: "<input name='sel_chkbx_Main' id='sel_chkbx_Main' type='checkbox' value='#=LM_TaskID#-#=LM_State#-#=LM_Location#-#=AVACOM_BranchID#-#=LM_PayrollMonth#-#=LM_PayrollYear#' >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll_Main' />",
                        width: "3%;"//, lock: true
                    },
                         {
                             title: "Sr.No",
                             field: "rowNumber",
                             template: "<span class='row-number'></span>",
                             width: "5%;",
                             attributes: {
                                 style: 'white-space: nowrap;'

                             },
                             filterable: {
                                 extra: false,
                                 operators: {
                                     string: {
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 }
                             }
                         }, 
                          
                            {
                                hidden:true,
                                title: "AVACOM_BranchID",
                                field: "AVACOM_BranchID",
                                //template: "<span class='row-number'></span>",
                                width: 70,
                         
                            },
                              {
                                  hidden:true,
                                  title: "RecordID",
                                  field: "RecordID",
                                  //template: "<span class='row-number'></span>",
                                  width: 70,
                              }, 
                          {
                              field: "Entity",
                              title: "Entity/Client",
                                width: "25%;",
                              attributes: {
                                  style: 'white-space: nowrap;'

                              },
                              filterable: {
                                  extra: false,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },
                          {
                              field: "StateName",
                              title: "State",
                              width: "15%",
                              attributes: {
                                  style: 'white-space: nowrap;'

                              },
                              filterable: {
                                  extra: false,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },
                          {
                              field: "LocationName",
                              title: "Location",
                              width: "15%",
                              attributes: {
                                  style: 'white-space: nowrap;'

                              },
                              filterable: {
                                  extra: false,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },
                        {
                            hidden: true,
                              field: "ScheduleList",
                              title: "ScheduleList",
                              attributes: {
                                  style: 'white-space: nowrap;'

                              },
                              filterable: {
                                  extra: false,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },

                        { hidden: false, field: "LM_PayrollMonth", title: "Month",width: "10%" },
                        { hidden: false, field: "LM_PayrollYear", title: "Year", width: "10%" },
                  
                            {// template: kendo.template('<span class="file-icon img-file"></span>').html()
                              command: [
                               { name:"editLable2", text: "No Document", className:"ob-lable" },
                                  { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download",width:70},
                                   { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                          
                              ], title: "Working", lock: true,width: 150,

                        },
                        {// template: kendo.template('<span class="file-icon img-file"></span>').html()
                            command: [
                                { name: "editLable2", text: "No Document", className: "ob-lable" },
                                { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-downloadcompliance", width: 70 },
                                { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewcompliance" }

                            ], title: "Compliance", lock: true, width: 150,

                        }

                    ],
                    dataBound: function () {
                        var rows = this.items();
                        $(rows).each(function () {
                            var index = $(this).index() + 1 
                            + ($("#grid1").data("kendoGrid").dataSource.pageSize() * ($("#grid1").data("kendoGrid").dataSource.page() - 1));;
                            var rowLabel = $(this).find(".row-number");
                            $(rowLabel).html(index);
                        });


                         // Selects all delete buttons
                    $("#grid1 tbody tr .k-grid-edit1").each(function () {
                        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }
                    });

                    $("#grid1 tbody tr .k-grid-edit2").each(function () {
                        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }
                    });


                    $("#grid1 tbody tr .k-grid-editLable2").each(function () {
                        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Available') {
                            $(this).remove();
                        }
                    });
                    }
             });

                $("#grid1").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
             }).data("kendoTooltip");

            }

        $(document).on("click", "#sel_chkbx_Main", function (e) {

            if (($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
                $('#dvbtndownloadDocumentMain').css('display', 'none');
            }
            else {
                $('#dvbtndownloadDocumentMain').css('display', 'block');
            }
            return true;
        });

        $(document).on("click", "#chkAll_Main", function (e) {
          
            if ($('input[id=chkAll_Main]').prop('checked')) {

                $('input[name="sel_chkbx_Main"]').each(function (i, e) {
                    e.click();
                });
            }
            else {                   
                $('input[name="sel_chkbx_Main"]').attr("checked", false);
            }
            if (($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
                $('#dvbtndownloadDocumentMain').css('display', 'none');
            }
            else {
                $('#dvbtndownloadDocumentMain').css('display', 'block');
            }
            return true;
        });     


        function BindDropDownMonth(){
            $("#ddlMonth").kendoDropDownList({
                placeholder: "Month",
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                  
                    dropDownFilter();
                    $("#dropdowntree").data("kendoDropDownTree").value([]);
                },
                //change: function (e) {            
                //    var month = $("#ddlMonth").val();
                //    var year = $("#ddlYear").val();

                //    if (month > 9 && year >= 2018) {
                //        var grid = $('#grid').data("kendoGrid");
                //        grid.setDataSource(null);
                //        BindSecondGrid();

                //        monthYearFilter();

                //        //var values = this.value().trim();
                //        //if (values != "" && values != null) {
                //        //    var filter = { logic: "or", filters: [] };
                //        //    filter.filters.push({
                //        //        field: "PayrollMonth", operator: "eq", value: parseInt(values)
                //        //    });
                //        //    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                //        //    dataSource.filter(filter);
                //        //}

                //        //BindSecondGrid();
                //        //BindSecondGridMonth();
                //        //BindChallansType();
                //    } else if (year < 2018 || (month <= 9 && year <= 2018)) {
                //        BindFirstGrid();

                //        monthYearFilter();

                //        //var values = this.value().trim();
                //        //if (values != "" && values != null) {
                //        //    var filter = { logic: "or", filters: [] };
                //        //    filter.filters.push({
                //        //        field: "PayrollMonth", operator: "eq", value: parseInt(values)
                //        //    });
                //        //    var dataSource = $("#grid").data("kendoGrid").dataSource;
                //        //    dataSource.filter(filter);
                //        //}
                //    } else {
                //        //ClearAllFilter();
                //    }
                //},
                dataSource: [
                        { text: "January", value: "01" },
                        { text: "February", value: "02" },
                        { text: "March", value: "03" },
                        { text: "April", value: "04" },
                        { text: "May", value: "05" },
                        { text: "June", value: "06" },
                        { text: "July", value: "07" },
                        { text: "August", value: "08" },
                        { text: "September", value: "09" },
                        { text: "October", value: "10" },
                        { text: "November", value: "11" },
                        { text: "December", value: "12" }
                ]
            });
        }

        function BindDropDownYear(){
            $("#ddlYear").kendoDropDownList({
                placeholder: "Year",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                index: 1,
                change: function (e) {
                  
                    dropDownFilter();
                    $("#dropdowntree").data("kendoDropDownTree").value([]);
                },
                dataSource: [
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },                     
                    { text: "2016", value: "2016" }
                ]
            });
        }

        function dropDownFilter() {
           debugger;
            $('#ClearfilterMain').css('display', 'none');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $('input[name="sel_chkbx"]').attr("checked", false);
            $('input[name="sel_chkbx_Main"]').attr("checked", false);

            var month = $("#ddlMonth").val();
            var year = $("#ddlYear").val();
                        
          
            if (month >= <% =newReturnsMonth%> && (year >= <% =newReturnsYear%> || year > <% =newReturnsYear%>)) {
                 var grid = $('#grid').data("kendoGrid");

            if (grid != undefined || grid != null)
                $('#grid1').empty();
                        //grid.setDataSource(null);


              var grid2 = $('#grid').data("kendoGrid");

                    if (grid2 != undefined || grid2 != null)
                        $('#grid').empty();

                $('#grid').hide();
                $('#grid1').show();

                BindSecondGrid();
              
            } else if (year < <% =newReturnsYear%> || (month < <% =newReturnsMonth%> && year <= <% =newReturnsYear%>)) {
                var grid = $('#grid').data("kendoGrid");

                    if (grid != undefined || grid != null)
                        $('#grid').empty();


              var grid2 = $('#grid').data("kendoGrid");

                    if (grid2 != undefined || grid2 != null)
                        $('#grid').empty();


                $('#grid1').hide();
                $('#grid').show();

                BindFirstGrid();
           
            } else {        
                //ClearAllFilter();
            }
        }

        function monthYearFilter_Grid_Historical(){
            if ($('#ddlMonth') != null && $('#ddlYear') != null) {
                var filter = { logic: "and", filters: [] };
                filter.filters.push({
                    field: "PayrollMonth", operator: "eq", value: parseInt($('#ddlMonth').val())
                });
                filter.filters.push({
                    field: "PayrollYear", operator: "eq", value: parseInt($('#ddlYear').val())                                
                });
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.filter(filter);
            }
        }

        function monthYearFilter_Grid(){
            if ($('#ddlMonth') != null && $('#ddlYear') != null) {
                var filter = { logic: "and", filters: [] };
                filter.filters.push({
                    field: "PayrollMonth", operator: "eq", value: parseInt($('#ddlMonth').val())
                });
                filter.filters.push({
                    field: "PayrollYear", operator: "eq", value: parseInt($('#ddlYear').val())                                
                });
                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                dataSource.filter(filter);
            }
        }
        
        $(document).ready(function (){

            var d = new Date();
            var n = d.getMonth();
            var y = d.getFullYear();

            BindLocationFilter();
            BindDropDownMonth();
            BindDropDownYear();

            if (n == 0) {
                y = y - 1;
                n = 12;
            }

            if (y == 2020) {
                y = 0;
            }   
            else if (y == 2019) {
                y = 1;
            }            
            else if (y == 2018) {
                y = 2;
            }
            else if (y == 2017) {
                y = 3;
            }
            else if (y == 2016) {
                y = 4;
            }
          
            $("#ddlMonth").data("kendoDropDownList").select(parseInt(n-1));
            $("#ddlYear").data("kendoDropDownList").select(y);

            dropDownFilter();            

            var rowNumber = 0;

            function resetRowNumber(e) {
                rowNumber = 0;
            }

            function renderNumber(data) {
                return ++rowNumber;
            }

            function renderRecordNumber(data) {
              
                var page = parseInt($("#grid").data("kendoGrid").dataSource.page()) - 1;
                var pagesize = $("#grid").data("kendoGrid").dataSource.pageSize();
                return parseInt(rowNumber + (parseInt(page) * parseInt(pagesize)));
            }

            var record = 0;
            var customerID =<% =CustId%>;
        });

        function ClearAllFilterMain(e) {
            e.preventDefault();
            $("#dropdowntree").data("kendoDropDownTree").value([]);
           
            $('#ClearfilterMain').css('display', 'none'); 
            
            dropDownFilter();
        }

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddlMonth').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddlYear').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {
         
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
         
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            $('#Clearfilter').css('display', 'none');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                //  $('#' + div).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');                
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }                
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;white-space: nowrap;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }



            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
               
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }
    </script>

    <script type="text/javascript">
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
       
        function exportReport() {
            $("#grid").getKendoGrid().saveAsExcel();
            return false;
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-4 colpadding0">
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 98%;" />
            </div>

            <div class="col-md-2 colpadding0" style="display:none;">
                <input id="ddlMonth" style="width: 98%" data-placeholder="Month" />
            </div>

            <div class="col-md-2 colpadding0" style="display:none;">
                <input id="ddlYear" style="width: 98%" data-placeholder="Year" />
            </div>
             <div class="col-md-2" style="width: 20%; padding-left: 0px;">
                      <input id="Startdatepicker" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 100%;"/>                   
                </div>
                <div class="col-md-2" style="width: 20%; padding-left: 0px;">
                      <input id="Enddatepicker" placeholder="End Date" CssClass="clsROWgrid" title="Enddatepicker" style="width: 100%;"/>                   
                </div>
            <div class="col-md-2 colpadding0">
                <button id="ClearfilterMain" style="float: right; display: none;" onclick="ClearAllFilterMain(event)">
                    <span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter(s)</button>
            </div>

            <div class="col-md-2 colpadding0">
                <button id="dvbtndownloadDocumentMain" style="display: none; float: right;" onclick="selectedDocument(event)">Download</button>
            </div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                    <%-- <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color:black;" id="filtersstoryboard">&nbsp;</div>--%>
                    <%--<div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>--%>
                </div>
            </div>
            <div id="grid1" style="border: none; display: none"></div>
        </div>
    </div>

     <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div class="col-md-12 colpadding0">
                                <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 360px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            //fhead('My Documents / Compliance Documents');
            //setactivemenu('ComplianceDocumentList');
            //fmaters()
        });
    </script>
</asp:Content>
