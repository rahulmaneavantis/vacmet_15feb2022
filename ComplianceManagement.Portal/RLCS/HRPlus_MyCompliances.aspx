﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_MyCompliances.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_MyCompliances" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>

	<link href="../NewCSS/stylenew.css" rel="stylesheet" />
	<link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

	<link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
	<link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

	<script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>

	<link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
	<link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
	<link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

	<script src="/Newjs/bootstrap-datepicker.min.js"></script>
	<link href="/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />

	<style type="text/css">
		.ui-widget-header {
			border: 0px !important;
			background: inherit;
			font-size: 20px;
			color: #666666;
			font-weight: normal;
			padding-top: 0px;
			margin-top: 5px;
		}

		.table > tbody > tr > th {
			vertical-align: bottom;
			border-bottom: 2px solid #dddddd;
		}

			.table > tbody > tr > th > a {
				vertical-align: bottom;
				color: #666666 !important;
				text-decoration: none !important;
				border-bottom: 2px solid #dddddd;
			}

		.table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
			padding: 5px;
			line-height: 1.428571429;
			vertical-align: middle;
		}

		.table tr th {
			color: #666666;
			font-size: 15px;
			font-weight: normal;
			font-family: 'Roboto',sans-serif;
		}

		.clsheadergrid {
			padding: 8px !important;
			line-height: 1.428571429 !important;
			vertical-align: bottom !important;
			border-bottom: 2px solid #dddddd !important;
			color: #666666;
			font-size: 15px;
			font-weight: normal;
			font-family: 'Roboto',sans-serif;
			text-align: left;
		}

		.clsROWgrid {
			padding: 8px !important;
			line-height: 1.428571429 !important;
			vertical-align: top !important;
			border-top: 1px solid #dddddd !important;
			color: #666666 !important;
			font-size: 14px !important;
			font-family: 'Roboto', sans-serif !important;
		}

		.m-10 {
			margin-top: 10px;
			margin-left: 10px;
		}

		.btn-search {
			background: #1fd9e1;
			float: right;
		}

		.chosen-results {
			max-height: 150px !important;
		}

		.table, pre.prettyprint {
			margin-bottom: 0px;
		}

		.k-loading-image {
			display: none;
		}

		.chosen-container-single .chosen-single {
			box-shadow: none !important;
		}

		#gridCompliances {
			table-layout: fixed;
			text-overflow: ellipsis;
			width: 100%;
			overflow: hidden;
			white-space: nowrap;
		}

		td {
			max-width: 100px;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}

		.action {
			width: 50px;
		}

		.compliance {
			width: 404px;
		}

		.Top-Mar {
			margin-top: 6px;
		}

		.tvFilterLocation {
			position: absolute;
			z-index: 50;
			background: white;
			border: 1px solid rgb(195, 185, 185);
			height: 200px;
			width: 100%;
			display: block;
			overflow: scroll;
		}
	</style>
	<style>
		.k-loading-mask {
			width: 0px;
			height: 0px;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			bindFromToMonthPicker();
			initializeJQueryUIDeptDDL();
			$('input[type="submit"]').click(function () { bindFromToMonthPicker(); });
		});

		function SetDatePicker() {
			bindFromToMonthPicker();
			$("#txtStartPeriod").datepicker('setDate', null);
			$("#txtEndPeriod").datepicker('setDate', null);
		}
	    //Perioddropdown
		$(document).mouseup(function (e) {
		    var container = $("#divFilterLocation");

		    // if the target of the click isn't the container nor a descendant of the container
		    if (!container.is(e.target) && container.has(e.target).length === 0) {
		        container.hide();
		    }
		});

		$(document).mouseup(function (e) {
		    var container = $("#dvPeriod");

		    // if the target of the click isn't the container nor a descendant of the container
		    if (!container.is(e.target) && container.has(e.target).length === 0) {
		        container.hide();
		    }
		});

		function bindFromToMonthPicker() {
			$('.fromMonthPicker').datepicker({
				autoclose: true,
				minViewMode: 1,
				format: 'M-yyyy'
			}).on('changeDate', function (selected) {
				if (selected.date != undefined) {
					startDate = new Date(selected.date.valueOf());
					startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
					$('.toMonthPicker').datepicker('setStartDate', startDate);
				}
			});

			$('.toMonthPicker').datepicker({
				autoclose: true,
				minViewMode: 1,
				format: 'M-yyyy'
			}).on('changeDate', function (selected) {
				if (selected.date != undefined) {
					FromEndDate = new Date(selected.date.valueOf());
					FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
					$('.fromMonthPicker').datepicker('setEndDate', FromEndDate);
				}
			});

			return false;
		}

		function initializeJQueryUI(textBoxID, divID) {
			$("#" + textBoxID).unbind('click');

			$("#" + textBoxID).click(function () {
				$("#" + divID).toggle("blind", null, 500, function () { });
			});
		}

		function checkAllPeriods(cb) {
			var ctrls = document.getElementsByTagName('input');
			for (var i = 0; i < ctrls.length; i++) {
				var cbox = ctrls[i];
				if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod") > -1) {
					cbox.checked = cb.checked;
				}
			}
		}

		function UncheckPeriodHeader() {
			var rowCheckBox = $("#RepeaterTable input[id*='chkPeriod']");
			var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkPeriod']:checked");
			var rowCheckBoxHeader = $("#RepeaterTable input[id*='PeriodSelectAll']");
			if (rowCheckBox.length == rowCheckBoxSelected.length) {
				rowCheckBoxHeader[0].checked = true;
			} else {
				rowCheckBoxHeader[0].checked = false;
			}
			//$('#BodyContent_udcInputForm_txtReturnList').val(rowCheckBoxSelected.length + ' ' + 'item selected');
		}

		function initializeJQueryUIDeptDDL() {

			$("#<%= txtPeriodList.ClientID %>").unbind('click');
			$("#<%= txtPeriodList.ClientID %>").click(function () {
				$("#dvPeriod").toggle("blind", null, 100, function () { });
			});
		}

		

		function ShowComplianceDialog(InstanceID, ComplianceSchedules, Type, Code, changeStatus, flag) {
			$('#divMyCompliances').show();
			var myWindowAdv = $("#divMyCompliances");
			function onClose() {
				$(this.element).empty();
			}

			function onOpen(e) {
				kendo.ui.progress(e.sender.element, true);
			}
			myWindowAdv.kendoWindow({
				width: "95%",
				height: "90%",

				content: "../RLCS/RLCS_ComplianceStatusTransaction.aspx?instanceID=" + InstanceID + "&scheduleOnID=" + ComplianceSchedules + "&Type=" + Type + "&Code=" + Code + "&changestatus=" + changeStatus + "&flag=" + '<%=filter%>',
				iframe: true,
				visible: false,
				actions: ["Close"],
				open: onOpen,
				close: onClose
			});

			myWindowAdv.data("kendoWindow").center().open();

			return false;
		}

		function CallParentMethod(complianceType, returnRegisterChallanID, actID, complianceID, branchID, month, year) {
		    bindFromToMonthPicker();
			window.parent.GotoComplianceInputOutputPage(complianceType, returnRegisterChallanID, actID, complianceID, branchID, month, year);
		}


	</script>
	<script type="text/javascript">
		function ShowChallanBranchdiv() {
			var headertitle = 'Branch Code Mapping';
			$('#divBranchCodeDetails').show();

			var myWindowAdv = $("#divBranchCodeDetails");
			function onClose() {
				$(this.element).empty();
			}
			function onOpen(e) {
				kendo.ui.progress(e.sender.element, true);
			}

			myWindowAdv.kendoWindow({
				width: "75%",
				height: "65%",
				title: "Branch Code Mapping",
				content: "../RLCS/Challan_BranchCodeMapping.aspx",
				iframe: true,
				visible: false,
				actions: [
					"Pin",
					"Close"
				],
				//open: onOpen,
				close: onClose
			});

			myWindowAdv.data("kendoWindow").center().open();

			return false;

		}
	</script>
</head>
<body>
	<form id="form2" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

		<asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<asp:UpdateProgress ID="updateProgress" runat="server">
					<ProgressTemplate>
						<div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
							<asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
								AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
						</div>
					</ProgressTemplate>
				</asp:UpdateProgress>

				<div class="col-xs-12 col-sm-12 col-md-12">
					<asp:ValidationSummary runat="server" ID="vsCompliances" CssClass="alert alert-block alert-danger alert-dismissable"
						ValidationGroup="myCompliancesValidationGroup" />
					<asp:CustomValidator ID="cvCompliances" runat="server" EnableClientScript="False"
						ValidationGroup="myCompliancesValidationGroup" Display="None" CssClass="alert alert-block alert-danger fade in" />
				</div>

				<div class="row col-md-12 col-sm-12">
					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 7%; display: none">
						<label for="ddlPageSize" class="control-label" hidden="hidden">Show</label>&nbsp;
                       
                        <asp:DropDownListChosen runat="server" ID="ddlPageSize" class="form-control" Width="100%"
							AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
							<asp:ListItem Text="10" />
							<asp:ListItem Text="20" />
							<asp:ListItem Text="50" Selected="True" />
						</asp:DropDownListChosen>
					</div>

					<asp:UpdatePanel ID="upCustomers" runat="server" OnLoad="upCustomers_Load">
						<ContentTemplate>
							<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
								<label for="ddlCustomers" class="control-label" hidden="hidden">Customer</label>&nbsp;                       
                                <asp:DropDownListChosen runat="server" ID="ddlCustomers" class="form-control" Width="100%" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true">
								</asp:DropDownListChosen>
							</div>

							<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
								<label for="tbxFilterLocation" class="control-label" hidden="hidden">Entity/ Branch</label>&nbsp;                       
                               <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 32px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" CssClass="txtbox" autocomplete="off" />
								<div id="divFilterLocation" style="position: absolute; z-index: 10; overflow-y: auto; background: white; border: 1px solid rgb(195, 185, 185); height: 200px; width: 100%; display: block;">
									<asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" CssClass="tvFilterLocation" NodeStyle-ForeColor="#8e8e93"
										Style="overflow: auto; margin-top: -20px; background-color: #ffffff; color: #8e8e93 !important; max-height: 200px;" ShowLines="true"
										OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
									</asp:TreeView>
								</div>
							</div>

							<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 20%">
								<label for="ddlCustomers" class="control-label" hidden="hidden">Assigned User</label>&nbsp;                       
                                <asp:DropDownListChosen runat="server" ID="ddlSPOCs" class="form-control" Width="100%">
								</asp:DropDownListChosen>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>

					<asp:UpdatePanel ID="upFreqFilter" runat="server" OnLoad="upFreqFilter_Load">
						<ContentTemplate>
							<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 10%">
								<label for="ddlFrequency" class="control-label" hidden="hidden">Frequency</label>&nbsp;
                               
                                <asp:DropDownListChosen ID="ddlFrequency" runat="server" Width="100%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
									AutoPostBack="true" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged">
									<asp:ListItem Text="Frequency" Value="-1"></asp:ListItem>
									<asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
									<asp:ListItem Text="Quarterly" Value="Quarterly"></asp:ListItem>
									<asp:ListItem Text="Half Yearly" Value="Half Yearly"></asp:ListItem>
									<asp:ListItem Text="Annual" Value="Annual"></asp:ListItem>
									<asp:ListItem Text="BiAnnual" Value="BiAnnual"></asp:ListItem>
								</asp:DropDownListChosen>
							</div>

							<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 10%">
								<label for="txtPeriodList" class="control-label" hidden="hidden">Period</label>&nbsp;
                                       
                                <asp:TextBox runat="server" ID="txtPeriodList" placeholder="Period" Style="padding: 10px; margin: 0px; height: 32px; width: 100%;" CssClass="txtbox form-control" />
								<div  style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: auto;" id="dvPeriod" class="dvDeptHideshow form-control">
									<div style="overflow-y: auto; height: 200px;">
										<asp:Repeater ID="rptPeriodList" runat="server">
											<HeaderTemplate>
												<table class="detailstable FadeOutOnEdit" id="RepeaterTable">
													<tr>
														<td style="width: 100px;" colspan="2">
															<asp:CheckBox ID="PeriodSelectAll" Text="Select All" runat="server" onclick="checkAllPeriods(this)" />
														</td>
													</tr>
											</HeaderTemplate>
											<ItemTemplate>
												<tr>
													<td style="width: 20px;">
														<asp:CheckBox ID="chkPeriod" runat="server" onclick="UncheckPeriodHeader();" /></td>
													<td style="width: 200px;">
														<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 118px; padding-bottom: 5px;">
															<asp:Label ID="lblPeriodID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
															<asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
														</div>
													</td>
												</tr>
											</ItemTemplate>
											<FooterTemplate>
												</table>
											</FooterTemplate>
										</asp:Repeater>
									</div>
								</div>
							</div>

							<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 10%">
								<label for="ddlYear" class="control-label" hidden="hidden">Year</label>&nbsp;
                                       
                                <asp:DropDownListChosen ID="ddlYear" runat="server" Width="100%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
									AutoPostBack="false">
								</asp:DropDownListChosen>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>

					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 5%; float: right;">
						<label for="lbtnExportExcel" class="control-label" hidden="hidden">Export</label>&nbsp;
                        <asp:LinkButton runat="server" CssClass="btn" ID="lbtnExportExcel" Style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
							data-toggle="tooltip" data-placement="bottom" Title="Export to Excel" ToolTip="Export to Excel" OnClick="lbtnExportExcel_Click">                                                  
						</asp:LinkButton>
					</div>
				</div>

				<div class="clearfix"></div>

				<div class="row col-md-12 col-sm-12">
					<div class="col-xs-1 col-sm-1 col-md-1" style="padding-left: 0px; width: 10%">
						<label for="ddlComType" class="control-label" hidden="hidden">Type</label>&nbsp;
                       
                        <asp:DropDownListChosen AutoPostBack="true" runat="server" ID="ddlComType" class="form-control" Width="100%" OnSelectedIndexChanged="ddlComType_SelectedIndexChanged">
							<asp:ListItem Text="Type" Value="-1" Selected="True" />
							<asp:ListItem Text="Register" Value="REG" />
							<asp:ListItem Text="Return" Value="RET" />
							<asp:ListItem Text="Challan" Value="CHA" />
						</asp:DropDownListChosen>
					</div>
					<div id="divchallantype" class="col-xs-1 col-sm-1 col-md-1 ESIComType" runat="server" style="padding-left: 0px; width: 11%;">
						<label for="ddlESIComType" class="control-label" hidden="hidden">Challan Type</label>&nbsp;
                       
                        <asp:DropDownListChosen runat="server" ID="ddlESIComType" class="form-control" Width="100%" >
							<asp:ListItem Text="Challan Type" Value="" Selected="True" />
							<asp:ListItem Text="ESI" Value="ESI" />
							<asp:ListItem Text="EPF" Value="EPF" />
							<asp:ListItem Text="PT" Value="PT" /> 
							
						</asp:DropDownListChosen>
					</div>

					<div class="col-xs-1 col-sm-1 col-md-1 colpadding0" style="width: 30%">
						<div class="col-xs-4 col-sm-4 col-md-4" style="padding-left: 0px;">
							<label for="txtStartPeriod" class="control-label" hidden="hidden">From</label>&nbsp;                           
                            <asp:TextBox runat="server" Height="32px" placeholder="Start Month" autoComplete="off" Style="padding-left: 5px; text-align: center;" class="form-control fromMonthPicker" ID="txtStartPeriod" />
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4" style="padding-left: 0px;">
							<label for="txtEndPeriod" class="control-label" hidden="hidden">To</label>&nbsp;                           
                            <asp:TextBox runat="server" Height="32px" placeholder="End Month" autoComplete="off" Style="padding-left: 5px; margin-left: 0px; text-align: center;" class="form-control toMonthPicker" ID="txtEndPeriod" />
						</div>
						
					
				      </div>

				<div class="col-xs-1 col-sm-1 col-md-1" style="display: none; padding-left: 0px; width: 25%">
						<label for="ddlAct" class="control-label" hidden="hidden">Act</label>
						<asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control" Width="100%">
						</asp:DropDownListChosen>
					</div>

					<div class="col-xs-1 col-sm-1 col-md-1" style="display: none;">
						<label for="ddlFType" class="control-label" hidden="hidden">Status</label>
						<asp:DropDownListChosen runat="server" ID="ddlFType" class="form-control" Width="100%">
							<asp:ListItem Text="All" Value="-1" Selected="True" />
							<asp:ListItem Text="Upcoming" Value="U" />
							<asp:ListItem Text="Overdue" Value="O" />
							<asp:ListItem Text="PendingForReview" Value="PR" />
							<asp:ListItem Text="PendingForAction" Value="PA" />
							<asp:ListItem Text="NotCompleted" Value="NC" />
							<asp:ListItem Text="ClosedTimely" Value="CT" />
							<asp:ListItem Text="ClosedDelayed" Value="CD" />
						</asp:DropDownListChosen>
					</div>

					<div class="col-xs-1 col-sm-1 col-md-1" style="display: none;">
						<asp:DropDownListChosen runat="server" ID="ddlRisk" class="form-control" Width="100%" Visible="false">
							<asp:ListItem Text="Risk" Value="-1" Selected="True" />
							<asp:ListItem Text="High" Value="0" />
							<asp:ListItem Text="Medium" Value="1" />
							<asp:ListItem Text="Low" Value="2" />
						</asp:DropDownListChosen>
					</div>
					<%--<div class="col-xs-1 col-sm-1 col-md-1"  style="width: 15%;" onclick="ShowChallanBranchdiv()">
								<asp:Button ID="btnBranchCodeMapping1" CausesValidation="false" class="btn btn-primary" Font-Size="15px" runat="server" Text="View Branch-Code Mapping" />
							</div>--%>

					<div class="col-xs-1 col-sm-1 col-md-1" style="width: 33%; margin-top:15px; float: right; text-align: right;">
						<div class="col-xs-1 col-sm-1 col-md-1" onclick="ShowChallanBranchdiv()" style="margin-left:-75px;">	
						<asp:Button  ID="btnBranchCodeMapping" CausesValidation="false" class="btn btn-primary" Font-Size="15px" runat="server" Text="View Branch-Code Mapping" />
						</div>
							<label for="txtStartPeriod" class="control-label" hidden="hidden">Filter</label>&nbsp;
                        <div style="width: 188px; float: right">
							<asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" Font-Size="15px" runat="server" Text="Apply" OnClick="btnSearch_Click" />
							<%--  <asp:Button ID="btnClear" CausesValidation="false" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClear_Click" /> --%>
							<asp:LinkButton runat="server" CausesValidation="false" class="btn btn-primary" OnClick="btnClear_Click"><span aria-hidden="true" class="k-icon k-i-filter-clear"> </span> <font size="3"> Clear Filter</font></asp:LinkButton>

						</div>
					</div>
				</div>

				<div class="row col-md-12 col-sm-12 AdvanceSearchScrum" style="margin-bottom: 0px;">
					<div id="divAdvSearch" runat="server" visible="false">
						<p>
							<asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
						</p>
						<p>
							<asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>
						</p>
					</div>
				</div>

				<div class="row col-md-12">
					<asp:GridView runat="server" ID="gridCompliances" AutoGenerateColumns="false" AllowSorting="true" GridLines="None" OnRowDataBound="gridCompliances_RowDataBound"
						CssClass="table" AllowPaging="true" PageSize="50" ShowHeaderWhenEmpty="true" EnableSortingAndPagingCallbacks="true" OnRowCommand="gridCompliances_RowCommand">
						<Columns>
							<%-- <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

							<asp:TemplateField HeaderText="Customer">
								<ItemTemplate>
									<asp:Label ID="lblCustomerName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Branch/Code">
								<ItemTemplate>
									<asp:Label ID="lblBranchName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Compliance" HeaderStyle-CssClass="compliance" ItemStyle-CssClass="compliance">
								<ItemTemplate>
									<asp:Label ID="lblCompliance" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Form">
								<ItemTemplate>
									<asp:Label ID="lblForm" runat="server" data-toggle="tooltip" data-placement="bottom"
										Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Frequency">
								<ItemTemplate>
									<asp:Label ID="lblFreq" runat="server" data-toggle="tooltip" data-placement="bottom"
										Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Period">
								<ItemTemplate>
									<asp:Label ID="lblPeriod" runat="server" data-toggle="tooltip" data-placement="bottom"
										Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Due Date">
								<ItemTemplate>
									<asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom"
										Text='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>'
										ToolTip='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Status" Visible="false">
								<ItemTemplate>
									<asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceStatusName") %>' ToolTip='<%# Eval("ComplianceStatusName") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="CanChangeStatus" Visible="false">
								<ItemTemplate>
									<asp:Label ID="lblCanChangeStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CanChangeStatus") %>' ToolTip='<%# Eval("CanChangeStatus") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" ItemStyle-CssClass="action" HeaderStyle-CssClass="action">
								<ItemTemplate>
									<%--  <a id="hlShowMyWorkspace" visible="false" data-toggle="tooltip" data-placement="left" tooltip="Review Compliance" onclick="ShowComplianceDialog5('<%#Eval("ComplianceInstanceID")%>','<%#Eval("ComplianceSchedules")%>')"  style="height:65px;width:60px; cursor:pointer;" ><img src="../../Images/edit_icon_new.png" /></a>--%>
									<asp:HyperLink ID="hlShowMyWorkspace" Visible="false" Text="<img src='../../Images/edit_icon_new.png' />"  NavigateUrl='<%# String.Format("javascript: ShowComplianceDialog({0},\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\")",Eval("ComplianceInstanceID"),Eval("ComplianceSchedules"),Eval("DocType"),Eval("ScheduledOnID"),Eval("CanChangeStatus"),Eval("FilterStatus")) %>' runat="server"
										data-toggle="tooltip" data-placement="left" ToolTip="Review Compliance"></asp:HyperLink>
									

									<asp:LinkButton ID="lnkbtn_GotoIOM" runat="server" Visible="false" CommandName="GOTO_IO" CommandArgument='<%# Eval("ComplianceSchedules")+"|"+Eval("CustomerID")+"|"+Eval("CustomerBranchID")+"|"+ Eval("ActID")+"|"+ Eval("ComplianceID")+"|"+Eval("RLCS_PayrollMonth")+"|"+Eval("RLCS_PayrollYear")+"" %>'
										data-toggle="tooltip" data-placement="left" ToolTip="Select & Proceed"><img src="../../Images/edit_icon_new.png" alt="Select & Proceed" title="Select & Proceed" /></asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>

						<RowStyle CssClass="clsROWgrid" />
						<HeaderStyle CssClass="clsheadergrid" />

						<PagerTemplate>
							<table style="display: none">
								<tr>
									<td>
										<asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
									</td>
								</tr>
							</table>
						</PagerTemplate>

						<EmptyDataTemplate>
							No Records Found.                       
                       
						</EmptyDataTemplate>
					</asp:GridView>
				</div>

				<div class="clearfix"></div>

				<div class="col-md-12 col-sm-12 colpadding0" style="margin-top: 10px;">
					<div class="col-md-6 col-sm-6">
						<div id="DivRecordsScrum" runat="server">
							<p style="padding-right: 0px !Important;">
								<asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
								<asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>-<asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
							</p>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 text-right">
						<div class="table-paging">
							<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />
							<div class="table-paging-text">
								<p>
									<asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                    <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
								</p>
							</div>
							<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
							<asp:HiddenField ID="TotalRows" runat="server" Value="0" />
						</div>
					</div>
				</div>

				<div id="divMyCompliances" style="height: 96%">
				</div>
					<div id="divBranchCodeDetails" style="height: 96%">
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="lbtnExportExcel" />
			</Triggers>
		</asp:UpdatePanel>
		<%--<div id="divBranchCodeDetails" style="height: 95%"></div>--%>

		<script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

		<script>
			$(document).ready(function () {
				$(document).tooltip({ selector: '[data-toggle="tooltip"]' });
				// $('#updateProgress1').hide();

				if ($(window.parent.document).find("#divMyCompliances").children().first().hasClass("k-loading-mask"))
					$(window.parent.document).find("#divMyCompliances").children().first().hide();

				if ($(window.parent.document).find("#divBranchCodeDetails").children().first().hasClass("k-loading-mask"))
					$(window.parent.document).find("#divBranchCodeDetails").children().first().hide();

				InitializeBranchesFilter();
				HideTreeViewFilter();
				var CT = $("#ddlComType").val();
				if (CT != "CHA")
                    $(".ESIComType").css("display", "none");
                $("#btnSearch").click(function () {

					var ComplianceType = $("#ddlComType").val();
                    if (ComplianceType == "CHA") {
                        //$("#ddlESIComType").show();
                        $(".ESIComType").css("display", "block");
                    } else {
                        // $("#ddlESIComType").hide();
                        $(".ESIComType").css("display", "none");
                    }
                });
			});

			function InitializeBranchesFilter() {
				initializeJQueryUI("<%=tbxFilterLocation.ClientID %>", 'divFilterLocation');
			}

			function HideTreeViewFilter() {
				$("<%=tbxFilterLocation.ClientID %>").hide("blind", null, 500, function () { });
			}

			function initializeJQueryUI(textBoxID, divID) {
				$("#" + textBoxID).unbind('click');

				$("#" + textBoxID).click(function () {
					$("#" + divID).toggle("blind", null, 500, function () { });
				});
			}
		</script>
	</form>
</body>
</html>
