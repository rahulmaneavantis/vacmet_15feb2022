﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using System.Text;
using System.Text.RegularExpressions;
using StackExchange.Redis;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_ManagementDashboard : System.Web.UI.Page
    {
        protected static DateTime FromFinancialYearSummery;
        protected static DateTime ToFinancialYearSummery;
        protected string Performername;
        protected string Reviewername;
        protected string InternalPerformername;
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected static string perFunctionChart;
        protected static string perFunctionPieChart;
        protected static string perFunctionPieNotcompleted;
        protected static string perPenaltyStatusPieChart;
        protected static string perRiskChart;
        protected static string perdeptChartzomato;
        protected static string perdeptChartCatzomato;
        protected static string perImprisonmentPenaltyChart;
        protected static string perFunctionHIGHPenalty;
        protected static string perFunctionMEDIUMPenalty;
        protected static string perFunctionLOWPenalty;
        protected static string perFunctionCRITICALPenalty;
        protected static int customerID;
        protected static int BID;
        protected static int graphshowhide;
        protected static string IsSatutoryInternal;
        protected static bool IsApprover = false;
        public static List<long> Branchlist = new List<long>();
        public static List<long> BranchlistGrading = new List<long>();
        bool IsPenaltyVisible = true;
        protected List<Int32> roles;
        protected static string perFunctionChartDEPT;
        bool IsNotCompiled = false;
        protected static string UserInformation;
        protected static string ProcessWiseObservationStatusChart;
        public string Penaltytextchange;
        protected static string customeridstring;
        protected static string RegulatoryAuthKey;
        protected static string UserAuthKey;
        protected static string AvacomRLCSApiURL;
        protected static string rlcs_API_url;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var plainBytes = Encoding.UTF8.GetBytes(Session["Email"].ToString());
                UserInformation = Convert.ToBase64String(CryptographyHandler.Encrypt(plainBytes, CryptographyHandler.GetRijndaelManaged("avantis")));
            }
            catch
            {

            }
            if (!IsPostBack)
            {
                rlcs_API_url = ConfigurationManager.AppSettings["RLCSAPIURL"].ToString();
                RegulatoryAuthKey = ConfigurationManager.AppSettings["RegulatoryAuth"].ToString();
                UserAuthKey = ConfigurationManager.AppSettings["X-User-Id-1"].ToString();
                AvacomRLCSApiURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"].ToString();

                try
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        if (Session["User_comp_Roles"] != null)
                        {
                            roles = Session["User_comp_Roles"] as List<int>;
                        }
                        else
                        {
                            roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                            Session["User_comp_Roles"] = roles;
                        }

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            if (AuthenticationHelper.Role.Equals("HMGR"))
                            {
                                IsApprover = false;
                            }
                            if (AuthenticationHelper.Role == "HMGR")
                            {

                                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                                home.Value = "true";
                                BindUserColors();
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                //fldsCalender.Visible = true;
                               
                                Branchlist.Clear();
                                BranchlistGrading.Clear();
                                BindLocationFilter();
                                rbFinancialYearFunctionSummery_SelectedIndexChanged(sender, e);
                                
                                    //PenaltyCriteria.Visible = false;
                                    entitycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    locationcount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    //functioncount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    compliancecount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    usercount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    //penaltycount.Visible = false;
                                
                                BindLocationCount(customerID, IsPenaltyVisible);
                               
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                                

                                int UnreadNotifications = Business.ComplianceManagement.GetUnreadUserNotification(AuthenticationHelper.UserID);

                                if (UnreadNotifications > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divNotification", "openNotificationModal();", true);
                                }
                                divTabs.Visible = true;
                                ComplianceCalender.Visible = true;
                                DivOverDueCompliance.Visible = true;
                                fldrbFinancialYearFunctionSummery.Visible = true;
                                
                                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                                string cashTimeval = string.Empty;
                                if (objlocal == "Local")
                                {
                                    cashTimeval = "MHRA_CHE" + AuthenticationHelper.UserID;
                                }
                                else
                                {
                                    cashTimeval = "MHRACHE" + AuthenticationHelper.UserID;
                                }
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                                    TimeSpan span = DateTime.Now - ss;
                                    if (span.Hours == 0)
                                    {
                                        Label1.Text = "Last updated within the last hour";
                                    }
                                    else
                                    {
                                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                                    }
                                }
                                else
                                {
                                    Label1.Text = "Last updated within the last hour";
                                }
                            }
                            else
                            {
                                //added by rahul on 12 June 2018 Url Sequrity
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                                FormsAuthentication.RedirectToLoginPage();
                            }
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                catch (Exception ex)
                {
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "MHRA_CHE" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "MHRACHE" + AuthenticationHelper.UserID;
                    }
                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {
                        Label1.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                    }
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        
        #region Common Function   

        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        
        private void BindLocationCount(int customerID, bool IsPenaltyVisible)
        {
            try
            {
                
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                string cashinternalval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MHRA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MHRS_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MHRACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MHRSPSD" + AuthenticationHelper.UserID;
                    
                }

                bool IsApproverInternal = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    // STT Change- Add Status
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();                    
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    if (ddlStatus.SelectedItem.Text == "Statutory")
                    {
                        #region Statutory
                        IsSatutoryInternal = "Statutory";
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {
                            try
                            {
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "HMGR")).ToList();
                                }
                                try
                                {
                                    StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                    if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                    {
                                        StackExchangeRedisExtensions.Remove(cashTimeval);
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                    else
                                    {
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryval);
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Statutory Error exception :" + cashstatutoryval);

                            }
                                GetManagementCompliancesSummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                
                        }
                        else
                        {
                            try
                            {
                                    if (IsApprover == false)
                                    {
                                       
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "HMGR")).ToList();
                                    }

                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryval);
                                    }

                                
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Statutory Error exception :" + cashstatutoryval);
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "HMGR")).ToList();
                                }
                            }
                           
                                GetManagementCompliancesSummary(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                
                        }
                        #endregion
                    }
                    
                    #region Top Count
                    if (ddlStatus.SelectedItem.Text == "Statutory")
                    {                    
                        long statutorycategorycount = 0;
                        long statutorycompliancecount = 0;
                        long statutorylocationcount = 0;
                        long statutoryentitycount = 0;
                        long statutoryucount = 0;
                        #region Statutory
                        if (IsApprover == false)
                        {
                            #region Management
                            entities.Database.CommandTimeout = 360;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "S"
                                                && row.userRole == "HMGR"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                List<SP_ManagementDashboardManagerComplianceCount_Result> ManagerComplianceCount = new List<SP_ManagementDashboardManagerComplianceCount_Result>();
                                entities.Database.CommandTimeout = 360;
                                var ManagerCompliancedetails = (entities.SP_ManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                ManagerComplianceCount = (from row in ManagerCompliancedetails
                                                          select row).Distinct().ToList();
                                if (ManagerComplianceCount.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ManagerComplianceCount = ManagerComplianceCount.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        statutorycategorycount = ManagerComplianceCount.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ManagerComplianceCount.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ManagerComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        statutorycategorycount = ManagerComplianceCount.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ManagerComplianceCount.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ManagerComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    statutoryentitycount = (entities.sp_Entitycount((int)AuthenticationHelper.UserID, "SAT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    statutoryucount = (from row in entities.Users
                                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                                       && row.IsActive == true && row.IsDeleted == false
                                                       select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    //divFunctionCount.InnerText = Convert.ToString(statutorycategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(statutoryentitycount);
                                    divLocationCount.InnerText = Convert.ToString(statutorylocationcount);
                                    divUsersCount.InnerText = Convert.ToString(statutoryucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    //divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                //divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                            }
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
        #region Top DropDown Selected Index Change

        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);
                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                    criticalcolor.Value = Cmd.Critical;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (customerID == -1)
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                    {
                        Branchlist.Clear();
                        GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        BID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        Branchlist.ToList();
                    }
                    else
                    {
                        BindLocationFilter();
                    }
                    BindLocationCount(customerID, IsPenaltyVisible);
                    //BindGradingReportSummaryTree();
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                string cashinternalval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MHRA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MHRS_PSD" + AuthenticationHelper.UserID;                    
                }
                else
                {
                    cashTimeval = "MHRACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MHRSPSD" + AuthenticationHelper.UserID;
                }
                try
                {
                    StackExchangeRedisExtensions.Remove(cashstatutoryval);
                    StackExchangeRedisExtensions.Remove(cashinternalval);
                    StackExchangeRedisExtensions.Remove(cashTimeval);
                }
                catch (Exception)
                {
                    LogLibrary.WriteErrorLog("Remove btnRefresh_Click Error exception :" + cashstatutoryval);
                }
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == customerID.ToString())
                        {
                            IsPenaltyVisible = false;
                        }
                    }
                }
                BindLocationCount(customerID, IsPenaltyVisible);

                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                {
                    //CacheHelper.Get<DateTime>(cashTimeval, out ss);                                  
                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {
                        //Label1.Text = "last updated :" + ss.ToString("dd-MMM-yyyy hh:mm:ss tt");
                        Label1.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                    }
                }
                else
                {
                    Label1.Text = "Last updated within the last hour";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region  Function Report

        private void BindLocationFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);
                    tvFilterLocation.Nodes.Clear();
                    string isstatutoryinternal = "";
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    var LocationList = CustomerBranchManagement.RLCS_GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);

                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    //BindLocationFilterPenalty(bracnhes, LocationList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";
                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }
                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }

        }
        #endregion



        #region Statutory
       public void GetManagementCompliancesSummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                //string tempperImprisonmentPenaltyChart = perImprisonmentPenaltyChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        //perImprisonmentPenaltyChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                        //perImprisonmentPenaltyChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    //perImprisonmentPenaltyChart = string.Empty;
                }

                bool auditor = false;
               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;

                    
                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                               //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.RLCS_GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.RLCS_GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                    }

                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";



                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;


                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;



                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['In Time', " + CompletedCountLow + "], " +
                                " ['After due date'," + AfterDueDatecountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;


                            //CRITICAL
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;



                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['In Time', " + CompletedCountCritical + "], " +
                                " ['After due date'," + AfterDueDatecountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;

                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;


                    #region Previous Working
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";
                    #endregion

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    // Not Completed - Critical

                    "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                      " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                       "}}},{  " +

                    // Not Completed - High
                    "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                     " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                    "}}},{" +

                    // Not Completed - Medium
                    " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                    "}}},{  " +

                    // Not Completed - Low

                    "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                      " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +



                    "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +

                    // After Due Date - Critical
                    "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - High

                    "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                    " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - Medium
                    "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                    "}}},{   " +
                    // After Due Date - Low
                    "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +





                    "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +

                    // In Time - Critical
                    "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                     "}}},{  " +

                    // In Time - High
                    "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                    " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                    "}}},{  " +
                    // In Time - Medium
                    "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                    "}}},{  " +
                    // In Time - Low
                    "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +



                    "}}}]}]";
                    
                    
                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                            //perImprisonmentPenaltyChart = tempperImprisonmentPenaltyChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        #endregion
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                //Top
                DateTime TopStartdate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopStartdate))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", string.Format("initializeDatePickerTopStartdate(new Date({0}, {1}, {2}));", TopStartdate.Year, TopStartdate.Month - 1, TopStartdate.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", "initializeDatePickerTopStartdate(null);", true);
                }

                DateTime TopEnddate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopEnddate))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", string.Format("initializeDatePickerTopEnddate(new Date({0}, {1}, {2}));", TopEnddate.Year, TopEnddate.Month - 1, TopEnddate.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", "initializeDatePickerTopEnddate(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rbFinancialYearFunctionSummery_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbFinancialYearFunctionSummery.SelectedValue.Equals("0"))
                {
                    if (DateTime.Today.Month > 3)
                    {
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (rbFinancialYearFunctionSummery.SelectedValue.Equals("1"))
                {
                    if (DateTime.Today.Month > 3)
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-2);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (rbFinancialYearFunctionSummery.SelectedValue.Equals("2"))
                {
                    string dtfrom = Convert.ToString("01-01-1900");
                    string dtto = Convert.ToString("01-01-1900");
                    FromFinancialYearSummery = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                InternalPerformername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
        protected void lnkShowTLDashboard_Click(object sender, EventArgs e)
        {
            try
            {
                var userRecord = RLCSManagement.GetRLCSUserRecord(Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.ProfileID);

                if (userRecord != null)
                {
                    if (userRecord.AVACOM_UserRole != null)
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        ProductMappingStructure _obj = new ProductMappingStructure();

                        //customerID --- User's CustomerID not Selected CustomerID
                        _obj.ReAuthenticate_User(customerID, userRecord.AVACOM_UserRole);
                        Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlperiod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int period = Convert.ToInt32(ddlperiod.SelectedValue);

                if (period == 0) //Current YTD
                {
                    if (DateTime.Today.Month > 3)
                    {
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (period == 1) //Current + Previous YTD
                {
                    if (DateTime.Today.Month > 3)
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-2);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (period == 2) //All
                {
                    string dtfrom = Convert.ToString("01-01-1900");
                    string dtto = Convert.ToString("01-01-1900");
                    FromFinancialYearSummery = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else if (period == 3)// Last Month
                {
                    FromFinancialYearSummery = DateTime.Now.AddDays(-30);
                    ToFinancialYearSummery = DateTime.Now;
                }
                else if (period == 4)//Last 3 Month
                {
                    FromFinancialYearSummery = DateTime.Now.AddDays(-90);
                    ToFinancialYearSummery = DateTime.Now;
                }
                else if (period == 5)//Last 6 Month
                {
                    FromFinancialYearSummery = DateTime.Now.AddDays(-180);
                    ToFinancialYearSummery = DateTime.Now;
                }
                else if (period == 6)//This Month
                {
                    DateTime now = DateTime.Now;
                    FromFinancialYearSummery = new DateTime(now.Year, now.Month, 1);
                    ToFinancialYearSummery = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}