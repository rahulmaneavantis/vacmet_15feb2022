﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class Challan_BranchCodeMapping : System.Web.UI.Page
	{
		public static List<int> locationList = new List<int>();
		List<BranchCodeMappingDetails_Result> _emptyObject = new List<BranchCodeMappingDetails_Result>();
		protected void Page_Load(object sender, EventArgs e)
		{
			btnSearch1.Enabled = false;
			if (!IsPostBack)
			{
				BindCustomers();
				//BindLocationFilter();
				divddlCustomer_SelectedIndexChanged(sender, e);
			}


		}
		public void BindChallanCode()
		{
			//string entityID = string.Empty;
			try
			{
				int CustID = Convert.ToInt32(divddlCustomers.SelectedValue);
				string entityID = clientDropDown.SelectedValue;
				string challanType = dropDownChallanType.SelectedValue;
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var _challanCode = entities.SP_GetChallanCodeDetails(CustID, entityID, challanType).ToList();
					DropDownListCode.Items.Clear();
					DropDownListCode.DataTextField = "Code";
					DropDownListCode.DataValueField = "Value";
					DropDownListCode.DataSource = _challanCode;
					DropDownListCode.DataBind();
				}
			}
			catch (Exception e)
			{ }


		}
		//public void BindLocationFilter()
		//{
		//	try
		//	{
		//		using (ComplianceDBEntities entities = new ComplianceDBEntities())
		//		{
		//			int customerID = -1;

		//			if (!string.IsNullOrEmpty(divddlCustomers.SelectedValue))
		//				customerID = Convert.ToInt32(divddlCustomers.SelectedValue);
		//			else
		//				customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

		//			List<int> assignedbranchIDs = new List<int>();

		//			entities.Database.CommandTimeout = 180;
		//			var userAssignedBranchList = (entities.SP_GetManagerAssignedBranch(AuthenticationHelper.UserID)).ToList();

		//			if (userAssignedBranchList != null)
		//			{
		//				if (userAssignedBranchList.Count > 0)
		//				{
		//					assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
		//				}

		//				var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

		//				CtvFilterLocation.Nodes.Clear();

		//				TreeNode node = new TreeNode("Entity/Branch", "-1");
		//				node.Selected = true;
		//				CtvFilterLocation.Nodes.Add(node);

		//				foreach (var item in bracnhes)
		//				{
		//					node = new TreeNode(item.Name, item.ID.ToString());
		//					node.SelectAction = TreeNodeSelectAction.Expand;
		//					CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedbranchIDs);
		//					CtvFilterLocation.Nodes.Add(node);
		//				}

		//				CtvFilterLocation.CollapseAll();
		//				CtvFilterLocation_SelectedNodeChanged(null, null);
		//			}
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
		//	}
		//}
		//protected void CtvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
		//{
		//	try
		//	{
		//		gridBranchMappingDetails.Visible = false;
		//		DropDownListCode.Items.Clear();
		//		CtbxFilterLocation.Text = CtvFilterLocation.SelectedNode != null ? CtvFilterLocation.SelectedNode.Text : "Entity/Branch";
		//		if (!string.IsNullOrEmpty(divddlCustomers.SelectedValue) && divddlCustomers.SelectedValue != "-1" && !string.IsNullOrEmpty(CtvFilterLocation.SelectedValue) && CtvFilterLocation.SelectedValue != "-1" && !string.IsNullOrEmpty(dropDownChallanType.SelectedValue) && dropDownChallanType.SelectedValue != "-1")
		//		{
		//			BindChallanCode();
		//		}

		//	}
		//	catch (Exception ex)
		//	{
		//		LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
		//	}
		//}
		protected void clientDropDown_SelectedNodeChanged(object sender, EventArgs e)
		{

			try
			{
				gridBranchMappingDetails.Visible = false;
				DropDownListCode.Items.Clear();
				if (!string.IsNullOrEmpty(divddlCustomers.SelectedValue) && divddlCustomers.SelectedValue != "-1" && !string.IsNullOrEmpty(clientDropDown.SelectedValue) && clientDropDown.SelectedValue != "-1" && !string.IsNullOrEmpty(dropDownChallanType.SelectedValue) && dropDownChallanType.SelectedValue != "-1")
				{
					BindChallanCode();
				}

			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}

		}
		//protected void upCCustomers_Load(object sender, EventArgs e)
		//{
		//	try
		//	{
		//		ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'CdivFilterLocation');", CtbxFilterLocation.ClientID), true);
		//		ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#CdivFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
		//	}
		//	catch (Exception ex)
		//	{
		//		LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
		//	}
		//}
		private void BindCustomers()
		{
			try
			{
				int customerID = -1;
				int serviceProviderID = -1;
				int distributorID = -1;

				if (AuthenticationHelper.Role == "CADMN")
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "SPADM")
				{
					serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else if (AuthenticationHelper.Role == "DADMN")
				{
					distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}
				else
				{
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				}

				divddlCustomers.Items.Clear();
				divddlCustomers.DataTextField = "CustomerName";
				divddlCustomers.DataValueField = "CustomerID";
				var lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);
				divddlCustomers.DataSource = lstMyAssignedCustomers;
				divddlCustomers.DataBind();

				divddlCustomers.Items.Insert(0, new ListItem("Customer", "-1"));
				divddlCustomers.SelectedIndex = -1;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		public void BindClientList(int CustomerID )	
		{
			try
			{
				List<object> EntityList = new List<object>();
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{

					EntityList = (
							  from RCRM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
							  join CB in entities.CustomerBranches on RCRM.AVACOM_BranchID equals CB.ID
							  where RCRM.BranchType == "E"
							  && RCRM.CM_Status == "A"
							  && RCRM.IsProcessed == true
							  && RCRM.AVACOM_CustomerID == CustomerID
							  && CB.IsDeleted == false
							  orderby RCRM.CM_ClientName ascending
							  select new
							  {
								  ID = RCRM.CM_ClientID,
								  Name = RCRM.CM_ClientName
							  }).Distinct().ToList<object>();
					clientDropDown.Items.Clear();
					clientDropDown.DataTextField = "Name";
					clientDropDown.DataValueField = "ID";
					clientDropDown.DataSource = EntityList;
					clientDropDown.DataBind();
					clientDropDown.Items.Insert(0, new ListItem("Entity", "-1"));
					clientDropDown.SelectedIndex = -1;
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		protected void divddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				gridBranchMappingDetails.Visible = false;
				//BindLocation();
				if(divddlCustomers.SelectedValue != "-1")
				BindClientList(Convert.ToInt32(divddlCustomers.SelectedValue));
				DropDownListCode.Items.Clear();
				if (!string.IsNullOrEmpty(divddlCustomers.SelectedValue) && divddlCustomers.SelectedValue != "-1" && !string.IsNullOrEmpty(clientDropDown.SelectedValue) && clientDropDown.SelectedValue != "-1" && !string.IsNullOrEmpty(dropDownChallanType.SelectedValue) && dropDownChallanType.SelectedValue != "-1")
				{
					BindChallanCode();
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		protected void dropDownChallanType_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				DropDownListCode.Items.Clear();
				if (!string.IsNullOrEmpty(divddlCustomers.SelectedValue) && divddlCustomers.SelectedValue != "-1" && !string.IsNullOrEmpty(clientDropDown.SelectedValue) && clientDropDown.SelectedValue != "-1" && !string.IsNullOrEmpty(dropDownChallanType.SelectedValue) && dropDownChallanType.SelectedValue != "-1")
				{

					BindChallanCode();
				}
				gridBranchMappingDetails.Visible = false;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		protected void DropDownListCode_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(DropDownListCode.SelectedValue) && DropDownListCode.SelectedValue != "-1")
				{
					btnSearch1.Enabled = true;
				}
				else { btnSearch1.Enabled = false; }
				gridBranchMappingDetails.Visible = false;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void bindGridonApplyclick(object sender, EventArgs e)
		{
			try
			{
				//string entityID = string.Empty;
				if (!string.IsNullOrEmpty(divddlCustomers.SelectedValue) && divddlCustomers.SelectedValue != "-1" && !string.IsNullOrEmpty(clientDropDown.SelectedValue) && clientDropDown.SelectedValue != "-1" && !string.IsNullOrEmpty(dropDownChallanType.SelectedValue) && dropDownChallanType.SelectedValue != "-1" && !string.IsNullOrEmpty(DropDownListCode.SelectedValue) && DropDownListCode.SelectedValue != "-1")
				{ }

				int CustID = Convert.ToInt32(divddlCustomers.SelectedValue);
				string entityID = clientDropDown.SelectedValue;
				string challanType = dropDownChallanType.SelectedValue;
				string _code = DropDownListCode.SelectedValue;

				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{

					var _data = entities.BranchCodeMappingDetails(CustID, entityID, challanType, _code).ToList();

					gridBranchMappingDetails.DataSource = _data;
					gridBranchMappingDetails.DataBind();
					gridBranchMappingDetails.Visible = true;

				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}

		}
	}
}