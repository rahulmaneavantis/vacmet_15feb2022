<%@ Page Title="My Compliance Documents" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyComplianceDocuments.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyComplianceDocuments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <%-- <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>
    <script type="text/javascript">
        document.write('<style type="text/css">body{display:none}</style>');
        jQuery(function ($) {
            $('body').css('display', 'block');
        });
    </script>
    <style type="text/css">
        .lst-top {
            margin-top: 5px !important;
        }

        .k-textbox > input {
            text-align: left;
        }

        .k-grid-header th.k-with-icon .k-link {
            color: #535b6a;
        }

        .k-check-all {
            zoom: 1.13;
            font-size: 13px;
        }

        .k-label input[type="checkbox"] {
            zoom: 1.2;
        }

        .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
            cursor: pointer;
            background-color: transparent !important;
            border-color: transparent;
            color: #1fd9e1;
        }

        .k-checkbox-label:hover {
            color: #1fd9e1;
        }

        label.k-label:hover {
            color: #1fd9e1;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: -1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid .k-button {
            margin: 0 .16em;
            margin-top: 3px;
        }

        .k-grid-content {
            min-height: auto !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
            padding-left: 23.2px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            /*border-width: 0 1px 1px 0px;*/
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 5px 0px -3px;
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
            margin-top: -4px;
            padding-top: 0px;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left: 2.7px;
            margin-bottom: 0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            padding-left: 0px;
            padding-right: 0px;
            color: black;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            padding-top: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            /*border-width: 0 0px 1px 0px;*/
            text-align: center;
            padding-left: 0px;
            padding-right: 10px;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width: 0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 1px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: #535b6a;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
            width: 200px;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            width: max-content;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -5px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            background-color: white;
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100%;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            border-width: 0 0px 0 0;
        }

        .k-grid, .k-listview {
            margin-top: 44px;
        }

        .k-autocomplete, .k-dropdown-wrap, .k-multiselect-wrap, .k-numeric-wrap, .k-picker-wrap, .k-textbox {
            border-width: 1px;
            border-style: solid;
            height: 33px;
        }

        .k-state-default > .k-select {
            margin-top: 0px;
            border-color: #c5c5c5;
        }

        .k-dropdown-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .k-grid-header .k-header > .k-link, .k-header, .k-treemap-title {
            color: rgba(0,0,0,0.5);
        }

        .k-tooltip-content {
            width: max-content !important;
        }

        .k-multiselect-wrap .k-input {
            padding-top: 5px;
            display: block !important;
        }

        .k-state-focused:hover {
            text-decoration: none;
            background-color: transparent;
            border-color: transparent;
        }

        .btn {
            height: 31px;
        }

        .k-item, .k-in {
            /* width: 90px; */
            border-color: transparent !important;
            box-sizing: border-box;
            box-shadow: none;
        }

        .k-button {
            margin-top: 2px;
        }

        .k-menu .k-menu-group .k-separator, .k-menu-scroll-wrapper.vertical .k-menu-group .k-separator, .k-menu-scroll-wrapper.vertical .k-separator, .k-popups-wrapper.vertical .k-menu-group .k-separator, .k-popups-wrapper.vertical .k-separator, ul.k-menu-vertical .k-separator {
            color: #535b6a;
            border-color: #ceced2 !important;
        }

        .hoverCircle:hover {
            -moz-box-shadow: 0 0 0 12px #E9EAEA;
            -webkit-box-shadow: 0 0 0 12px #E9EAEA;
            box-shadow: 0 0 0 12px #E9EAEA;
            -moz-transition: -moz-box-shadow .3s;
            -webkit-transition: -webkit-box-shadow .3s;
            transition: box-shadow .3s;
            border-radius: 50%;
            background-color: #E9EAEA;
        }

        .btn {
            font-weight: 400;
            font-size: 14px;
        }

        .k-grid, .k-listview {
            margin-top: 10px;
        }
    </style>
    <%-- <style type="text/css">
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
        .k-autocomplete.k-state-default, .k-dropdown-wrap.k-state-default, .k-numeric-wrap.k-state-default, .k-picker-wrap.k-state-default {
            background-color:white;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            margin-top:0px;
                  }
        .k-grid td {
            line-height: 1.0em;
        }

        /*For grid Button Next first last previous (formatting)*/
        .k-i-arrow-60-right:before{
            content:"\e005";
            padding-left:2px;
            padding-bottom:5px;
        }

        .k-i-arrow-end-right:before{
            content:"\e009";
            padding-left:0px;
            padding-bottom:5px;
        }

        .k-i-arrow-60-left:before
        {
            content: "\e007";
            padding-bottom: 5px
        }
        .k-i-arrow-end-left:before{
            content: "\e00b";
            padding-bottom: 5px;
        }
           /*For grid Button Next first last previous (formatting)*/

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
            padding-bottom:0px;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 9px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }
        .k-combobox-clearable .k-input, .k-dropdowntree-clearable .k-dropdown-wrap .k-input, .k-dropdowntree-clearable .k-multiselect-wrap, .k-multiselect-clearable .k-multiselect-wrap {
            padding-right: 2em;
            padding-bottom: 0px;
            height: 27px;
        }
        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            border-radius: 30px;
            background-color:initial;
            border: 0px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }
       
        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 32%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }
        .k-grid-content,.k-auto-scrollable{
           height:auto !important;
        }
        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-grid-header th{
            height:15px;
        }
       
      .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            padding-top: 1px;
            padding-bottom: 1px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        } 

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
        #AdavanceSearch{
            border: .5px solid #dcdcdf;
            background-color: #f5f5f6;
        }
        .clearfix{
            height: 9px !important;
        }
        .container{
            max-width:99% !important;
        }
       
        .sidebar-menu{
            margin-top: 53px !important;
        }
        .iframe1 {
    max-height: 500px; /* or whatever */
}
   
        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 0px 1px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 30px;
            vertical-align: middle;
        }
     .k-input, .k-multiselect-wrap, .k-textbox > input, input.k-textbox, input.k-textbox:hover, textarea.k-textbox, textarea.k-textbox:hover {
    background-color:white;
    color: #515967;
}
     .k-icon, .k-tool-icon {
    position: relative;
    display: inline-block;
    overflow: hidden;
    width: 1em;
    height: 1em;
    text-align: center;
    vertical-align: top;
    background-image: none;
    font: 16px/1 WebComponentsIcons;
    speak: none;
    font-variant: normal;
    text-transform: none;
    text-indent: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: inherit;
}
     .hoverCircle:hover {
    -moz-box-shadow: 0 0 0 12px #E9EAEA;
    -webkit-box-shadow: 0 0 0 12px #E9EAEA;
    box-shadow: 0 0 0 12px #E9EAEA;
    -moz-transition: -moz-box-shadow .3s;
    -webkit-transition: -webkit-box-shadow .3s;
    transition: box-shadow .3s;
    border-radius: 50%;
    background-color: #E9EAEA;
}
        .k-block, .k-content, .k-dropdown .k-input, .k-popup, .k-toolbar, .k-widget {
            color: #515967;
            width: auto;
            height: 28px;
        }

.k-picker-wrap .k-icon {
    cursor: pointer;
    bottom: 6px;
}   
    </style>--%>
    <title></title>


    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>


    <script id="fileTemplate" type="text/x-kendo-template">
                 
            <span class='k-progress'></span>
            <div class='file-wrapper'> 
            #=GetFileExtType(FileName)# #=FileName # 
           <%-- <span class='k-icon k-i-file-txt k-i-txt'></span>#=FileName# --%>

    </script>


    <script id="getrisk" type="text/x-kendo-template">          
            #=GetRiskType(Risk)#        
    </script>


    <script id="fileExtensionTemplate" type="text/x-kendo-template">
      
            #=FileName.split('.').pop() # 
         
    </script>

    <script type="text/javascript">  
        function ApplyFilter() {

            var finalSelectedfilter = { logic: "and", filters: [] };

            var selectedLocations = $("#dropdowntree").data("kendoDropDownTree")._values;
            if (selectedLocations.length > 0) {
                var locFilter = { logic: "or", filters: [] };

                $.each(selectedLocations, function (i, v) {
                    locFilter.filters.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(locFilter);
            }


            var selectedYrs = $("#dropdownlistYear").data("kendoDropDownList").value();
            if (selectedYrs != "-1") {
                var locFilterr = { logic: "or", filters: [] };

                var fruits = [selectedYrs];
                $.each(fruits, function (i, v) {
                    locFilterr.filters.push({
                        field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(locFilterr);
            }

            var selectedMonths = $("#dropdownlistMonth").data("kendoDropDownList").value();
            if (selectedMonths != "-1") {
                var monthFilter = { logic: "or", filters: [] };

                var fruits = [selectedMonths];
                $.each(fruits, function (i, v) {
                    monthFilter.filters.push({
                        field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(monthFilter);
            }

            var selectedActs = $("#dropdownlistAct").data("kendoDropDownList").value();
            if ((selectedActs != "-1") && (selectedActs != "")) {
                var actFilter = { logic: "or", filters: [] };

                var fruits = [selectedActs];
                $.each(fruits, function (i, v) {
                    actFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(actFilter);
            }
            var selectedPEIDs = $("#ddlPrincipleEmployer").data("kendoDropDownList").value();
            if ((selectedPEIDs != "-1") && (selectedPEIDs != "")) {
                var PEFilter = { logic: "or", filters: [] };

                var fruits = [selectedPEIDs];
                $.each(fruits, function (i, v) {
                    PEFilter.filters.push({
                        field: "PEID", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(PEFilter);
            }


            var selectedStatus = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            if (selectedStatus.length > 0) {
                var statusFilter = { logic: "or", filters: [] };

                $.each(selectedStatus, function (i, v) {
                    statusFilter.filters.push({
                        field: "Status", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(statusFilter);
            }


            var selectedType = $("#ddltype").data("kendoDropDownTree")._values;
            if (selectedType.length > 0) {
                var typeFilter = { logic: "or", filters: [] };

                $.each(selectedType, function (i, v) {
                    typeFilter.filters.push({
                        field: "DocType", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(typeFilter);
            }



            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);
                $('#ClearfilterMain').css('display', 'block');

				/*if ($("#dropdowntree").data("kendoDropDownTree") != null && $("#dropdowntree").data("kendoDropDownTree") != undefined)
				{
					var dataSource = $("#grid").data("kendoGrid").dataSource;
					dataSource.filter(finalSelectedfilter);
					//$('#ClearfilterMain').css('display', 'block');                      
				}*/

            }
            else {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                dataSource.filter({});
                $('#ClearfilterMain').css('display', 'none');
				/*if ($("#dropdowntree").data("kendoDropDownTree") != null && $("#dropdowntree").data("kendoDropDownTree") != undefined) {
					var dataSource = $("#grid").data("kendoGrid").dataSource;
					dataSource.filter({});
					//$('#ClearfilterMain').css('display', 'none');
				}*/
            }
        }

        function GetRiskType(value) {
            if (value == '0') {
                return "High";
            }
            else if (value == '1') {
                return "Medium";
            }
            else if (value == '2') {
                return "Low";
            }
            else {
                return "";
            }
        }



        function GetFileExtType(value) {

            if (value.split('.').pop() == "pdf" || value.split('.').pop() == "PDF" || value.split('.').pop() == "Pdf") {
                return "<span class='k-icon k-i-file-pdf k-i-pdf'></span>";
            }
            else if (value.split('.').pop() == "doc" || value.split('.').pop() == "docx" || value.split('.').pop() == "DOC" || value.split('.').pop() == "DOCX") {
                return "<span class='k-icon k-i-file-word k-i-file-doc k-i-word k-i-doc'></span>";
            }

            else if (value.split('.').pop() == "xls" || value.split('.').pop() == "xlsx" || value.split('.').pop() == "XLS" || value.split('.').pop() == "XLSX") {
                return "<span class='k-icon k-i-file-excel k-i-file-xls k-i-excel k-i-xls'></span>";
            }
            else if (value.split('.').pop() == "ppt" || value.split('.').pop() == "pptx" || value.split('.').pop() == "PPT" || value.split('.').pop() == "PPTX") {
                return "<span class='k-icon k-i-file-ppt k-i-ppt'></span>";
            }
            else if (value.split('.').pop() == "msg" || value.split('.').pop() == "MSG") {
                return "<span class='k-icon k-i-email k-i-envelop k-i-letter'></span>";
            }
            else if (value.split('.').pop() == "txt") {
                return "<span class='k-icon k-i-file-txt k-i-txt'></span>";
            }
            else if (value.split('.').pop() == "jpg" || value.split('.').pop() == "JPG" || value.split('.').pop() == "jpeg" || value.split('.').pop() == "JPEG" || value.split('.').pop() == "png" || value.split('.').pop() == "PNG" || value.split('.').pop() == "tif" || value.split('.').pop() == "TIF" || value.split('.').pop() == "tiff" || value.split('.').pop() == "TIFF" || value.split('.').pop() == "bmp" || value.split('.').pop() == "BMP" || value.split('.').pop() == "gif" || value.split('.').pop() == "GIF") {
                return "<span class='k-icon k-i-image k-i-photo'></span>";
            }
            else {
                return "";
            }
        }


        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {

            $('#dropdownFY').change(function () {

                $('#Clearfilter').css('display', 'block');

            });

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "90%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });

            $("#Startdatepicker").kendoDatePicker({
                change: onChange
            });
            function onChange() {

                $('#filterStartDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#filterStartDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterStartDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterStartDate').append('Start Date:&nbsp;');
                    $('#filterStartDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');

                }
                DateFilterCustom();
            }

            function DateFilterCustom() {

                $('input[id=chkAll]').prop('checked', false);
                $('#dvbtndownloadDocument').css('display', 'none');

                if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                    || $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                    || $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                    || $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {
                    setCommonAllfilter();
                }
                else {
                    var filter = { logic: "and", filters: [] };
                    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        filter.filters.push({
                            field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                        });
                    }
                    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        filter.filters.push({
                            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')//kendo.parseDate(this.value(), 'MM/dd/yyyy')
                        });
                    }
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);
                }
            }

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange1
            });
            function onChange1() {

                $('#filterLastDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterLastDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterLastDate').append('End Date&nbsp;&nbsp;:&nbsp;');

                    $('#filterLastDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                }
                DateFilterCustom();
            }



            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({

                dataSource: {
                    //type: "odata",
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>',
                            <%--url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=',--%>
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },

                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        total: function (response) {

                            return response.Result.length;
                        }
                    }
                },

                //height: 453,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: false,
                    pageSize: 10,
                    pageSizes: true,
                    buttonCount: 3,

                    change: function (e) {
                        $('#chkAll').removeAttr('checked');
                        $('#dvbtndownloadDocument').css('display', 'none');
                    },
                },
                reorderable: true,

                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                change: onChange,
                columns: [
                    {
                        template: function (dataItem) {
                            // return "<strong>" + kendo.htmlEncode(dataItem.name) + "</strong>";
                            return "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' flag='" + kendo.htmlEncode(dataItem.IsStatuoryOREventBasedORCheckList) + "' value='" + kendo.htmlEncode(dataItem.ScheduledOnID) + " '>"
                        },
                        // template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' flag=#=IsStatuoryOREventBasedORCheckList# value=#=ScheduledOnID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAll' />",

                        width: "3%;",//, lock: true
                        attributes: {
                            style: 'white-space: nowrap;text-align:center'
                            //border-width:0px 0px 1px 1px;'
                        },


                    },
                    {
                        hidden: true, field: "Risk", title: "Risk", menu: false,
                        template: kendo.template($('#getrisk').html())
                    },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID", menu: false, },
                    {
                        hidden: true, field: "FileName", title: "Document Name", menu: false,
                        attributes: {
                            style: 'white-space: nowrap; '
                        },
                        template: kendo.template($('#fileTemplate').html()), width: "29.7%",
                        filterable: { multi: true, search: true }
                    },
                    { hidden: true, field: "Version", title: "Version", menu: false, filterable: { multi: true, search: true }, },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            search: true,
                            multi: true,
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortForm", title: 'Compliance',
                        width: "43.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name', menu: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature', menu: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        //  width: 120,
                        format: "{0:dd-MMM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                            //border-width:0px 0px 1px 1px;
                        },
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {

                            search: true,
                            multi: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: 'date',
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period',
                        attributes: {
                            style: 'white-space: nowrap; '
                            //border-width:0px 0px 1px 1px;
                        },
                        filterable: {
                            extra: false,
                            search: true,
                            multi: true,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap; '
                            //border-width:0px 0px 1px 1px;
                        },
                        filterable: {
                            search: true,
                            multi: true,
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        hidden: true, field: "VersionDate", title: "Uploaded Date", menu: false, type: "date",
                        template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        format: "{0:dd-MMM-yyyy}",
                        type: 'date',
                        filterable: {
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    type: 'date',
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        hidden: true, field: "FileName", title: "Type", attributes: {
                            style: 'white-space: nowrap; '
                        }, menu: false,
                        template: kendo.template($('#fileExtensionTemplate').html()),
                        filterable: { multi: true, search: true }
                    },
                    //{ hidden: true, field: "", title: "Type" },
                    { hidden: true, field: "", title: "Uploaded By", menu: false, filterable: { multi: true, search: true } },
                    { hidden: true, field: "", title: "Size", menu: false, filterable: { multi: true, search: true } },
                    { hidden: true, field: "ActID", title: "", menu: false, filterable: { multi: true, search: true } },
                    { hidden: true, field: "UserID", title: "UserID", menu: false, filterable: { multi: true, search: true } },
                    { hidden: true, field: "PEID", title: "PEID", menu: false, filterable: { multi: true, search: true } },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain " }
                            //hoverCircle
                        ], title: "Action", lock: true,
                        attributes: {
                            style: 'text-align:center;'
                            //border-width:0px 1px 1px 1px;
                        },
                        // width: 150,

                    }
                ]
            });

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });
            //function definition

            $("#grid1").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom-left",
                content: function (e) {

                    var content = e.target.context.textContent;
                    if ($.trim(content) != "") {
                        return '<div style="width: ' + $.trim(content).length * .6 + 'em; max-width: auto">' + $.trim(content) + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            var grid = $("#grid").kendoGrid({
                dataSource: null,
                // toolbar: kendo.template($("#template").html()),
                //height:513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    //pageSize: 10,
                    pageSizes: true,
                    refresh: false,
                    buttonCount: 3,

                    change: function (e) {
                        //$('#chkAllMain').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'block');
                    },
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                    {
                        template: function (dataItem) {
                            // return "<strong>" + kendo.htmlEncode(dataItem.name) + "</strong>";
                            return "<input name='sel_chkbxMain' id='sel_chkbxMain' type='checkbox' flag='" + kendo.htmlEncode(dataItem.IsStatuoryOREventBasedORCheckList) + "' value='" + kendo.htmlEncode(dataItem.ScheduledOnID) + " '>"
                        },
                        //field: "ID", title: " ",
                        //template: "<input name='sel_chkbxMain' id='sel_chkbxMain' type='checkbox' flag=#=ScheduledOnID# value=#=ScheduledOnID# >",
                        filterable: false, sortable: false,
                        headerTemplate: "<input type='checkbox' id='chkAllMain' />",
                        width: "5%;",//, lock: true
                        attributes: {
                            style: "text-align:center;"
                        },
                    },
                    //{ hidden: true, field: "ID", title: "ID" },
                    {
                        hidden: true, field: "Risk", title: "Risk", menu: false,
                        template: kendo.template($('#getrisk').html())

                    },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID", menu: false, },
                    {
                        field: "Branch", title: 'Location',
                        width: "14.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            search: true,
                            extra: false,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceID", title: 'Comp.Id', width: "9%;",
                        filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortForm", title: 'Description',
                        width: "22%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "Name", title: 'ACT', menu: false,
                        width: "10.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name', menu: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature', menu: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        width: "15%",
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            search: true,
                            extra: false,
                            multi: true,
                            operators: {
                                string: {
                                    type: 'date',
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,
                            search: true,
                            multi: true,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            search: true,
                            extra: false,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "PEID", title: "PEID", menu: false, filterable: { multi: true, search: true } },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true, attributes: { style: "text-align: center;" }, className: "k-abc",
                        // width: 150,
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });
            $("#dvbtndownloadDocumentMain").kendoTooltip({
                //filter: ".k-grid-edit4",
                position: "bottom-right",
                content: function (e) {
                    return "Download Selected";
                }
            });
            $("#AdavanceSearch").kendoTooltip({
                //filter: ".k-grid-edit2",
                position: "bottom-right",
                content: function (e) {
                    return "Advanced Search";
                }
            });
            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 40em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Statutory CheckList", value: "2" },
                    //{ text: "Statutory", value: "-1" },                    
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                }
            });
            function DataBindDaynamicKendoGriddMain() {

                $('input[id=chkAllMain]').prop('checked', false);
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                //$("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'none');

                var gridClear = $('#grid').data("kendoGrid");
                if (gridClear != undefined || gridClear != null)
                    gridClear.setDataSource(null);

                var dataSource = new kendo.data.DataSource({
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>',
                              <%--url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaOrInt=SAT&StaIntFlag=' + $("#dropdownlistComplianceType").val() + '&FY=',--%>
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        total: function (response) {

                            return response.Result.length;
                        }
                    }
                });

                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            //dataType: 'json',
                        },
                    },

                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });

                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);

                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0);
                if ($("#dropdownlistComplianceType").val() == 1) {
                    $('#dvdropdownEventNature').css('display', 'block');
                    $('#dvdropdownEventName').css('display', 'block');

                    $("#grid").data("kendoGrid").showColumn(5);
                    $("#grid").data("kendoGrid").showColumn(6);
                    $("#grid").data("kendoGrid").hideColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(8);
                }
                else {
                    $("#grid").data("kendoGrid").showColumn(8);
                    $("#grid").data("kendoGrid").showColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(5);
                    $("#grid").data("kendoGrid").hideColumn(6);
                }
            }

            function DataBindDaynamicKendoGriddMainMonth() {

                $('input[id=chkAllMain]').prop('checked', false);
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                //$("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'block');

                var gridClear = $('#grid').data("kendoGrid");
                if (gridClear != undefined || gridClear != null)
                    gridClear.setDataSource(null);

                var dataSource = new kendo.data.DataSource({
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=All&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&MY=' + $("#dropdownlistMonth").val() + '&YY=' + $("#dropdownlistYear").val() + '&ActName=' + $("#dropdownlistAct").val() + '',
                            <%--url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaOrInt=SAT&StaIntFlag=' + $("#dropdownlistComplianceType").val() + '&FY=',--%>
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        total: function (response) {

                            return response.Result.length;
                        }
                    }
                });

                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            //dataType: 'json',
                        },
                    },

                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });

                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);

                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0);
                if ($("#dropdownlistComplianceType").val() == 1) {
                    $('#dvdropdownEventNature').css('display', 'block');
                    $('#dvdropdownEventName').css('display', 'block');

                    $("#grid").data("kendoGrid").showColumn(5);
                    $("#grid").data("kendoGrid").showColumn(6);
                    $("#grid").data("kendoGrid").hideColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(8);
                }
                else {
                    $("#grid").data("kendoGrid").showColumn(8);
                    $("#grid").data("kendoGrid").showColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(5);
                    $("#grid").data("kendoGrid").hideColumn(6);
                }
            }

            function DataBindDaynamicKendoGriddMainForYear() {

                $('input[id=chkAllMain]').prop('checked', false);
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                //$("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'block');

                var gridClear = $('#grid').data("kendoGrid");
                if (gridClear != undefined || gridClear != null)
                    gridClear.setDataSource(null);

                var dataSource = new kendo.data.DataSource({
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=All&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&MY=' + $("#dropdownlistMonth").val() + '&YY=' + $("#dropdownlistYear").val() + '&ActName=' + $("#dropdownlistAct").val() + '',
                            <%--url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaOrInt=SAT&StaIntFlag=' + $("#dropdownlistComplianceType").val() + '&FY=',--%>
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        total: function (response) {

                            return response.Result.length;
                        }
                    }
                });

                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            //dataType: 'json',
                        },
                    },

                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });

                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);

                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0);
                if ($("#dropdownlistComplianceType").val() == 1) {
                    $('#dvdropdownEventNature').css('display', 'block');
                    $('#dvdropdownEventName').css('display', 'block');

                    $("#grid").data("kendoGrid").showColumn(5);
                    $("#grid").data("kendoGrid").showColumn(6);
                    $("#grid").data("kendoGrid").hideColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(8);
                }
                else {
                    $("#grid").data("kendoGrid").showColumn(8);
                    $("#grid").data("kendoGrid").showColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(5);
                    $("#grid").data("kendoGrid").hideColumn(6);
                }
            }

            function DataBindDaynamicKendoGriddMainAct() {

                $('input[id=chkAllMain]').prop('checked', false);
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'block');

                var gridClear = $('#grid').data("kendoGrid");
                if (gridClear != undefined || gridClear != null)
                    gridClear.setDataSource(null);

                var dataSource = new kendo.data.DataSource({
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=All&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&MY=' + $("#dropdownlistMonth").val() + '&YY=' + $("#dropdownlistYear").val() + '&ActName=' + $("#dropdownlistAct").val() + '',
                            <%--url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaOrInt=SAT&StaIntFlag=' + $("#dropdownlistComplianceType").val() + '&FY=',--%>
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        total: function (response) {

                            return response.Result.length;
                        }
                    }
                });

                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            //dataType: 'json',
                        },
                    },

                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });

                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);

                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0);
                if ($("#dropdownlistComplianceType").val() == 1) {
                    $('#dvdropdownEventNature').css('display', 'block');
                    $('#dvdropdownEventName').css('display', 'block');

                    $("#grid").data("kendoGrid").showColumn(5);
                    $("#grid").data("kendoGrid").showColumn(6);
                    $("#grid").data("kendoGrid").hideColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(8);
                }
                else {
                    $("#grid").data("kendoGrid").showColumn(8);
                    $("#grid").data("kendoGrid").showColumn(3);
                    $("#grid").data("kendoGrid").hideColumn(5);
                    $("#grid").data("kendoGrid").hideColumn(6);
                }
            }

            //$("#dropdownlistRisk").kendoDropDownTree({
            //    placeholder: "Risk",
            //    checkboxes: true,
            //    checkAll: true,
            //    visible:false,
            //    autoClose: true,
            //    checkAllTemplate: "Select All",
            //    autoWidth: true,
            //    dataTextField: "text",
            //    dataValueField: "value",

            //    change: function () {

            //        if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
            //            || $("#dropdowntree").data("kendoDropDownTree")._values.length > 0
            //            || $("#ddltype").data("kendoDropDownTree")._values.length > 0) {
            //            setCommonAllfilterMain();
            //        }
            //        else {
            //            var filter = { logic: "or", filters: [] };
            //            //  values is an array containing values to be searched
            //            var values = this.value();
            //            $.each(values, function (i, v) {
            //                filter.filters.push({
            //                    field: "Risk", operator: "eq", value: parseInt(v)
            //                });
            //            });

            //            var dataSource = $("#grid").data("kendoGrid").dataSource;
            //            dataSource.filter(filter);
            //        }
            //        fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

            //        $('input[id=chkAllMain]').prop('checked', false);  
            //        $('#dvbtndownloadDocumentMain').css('display', 'block');
            //    },
            //    dataSource: [
            //        { text: "High", value: "0" },
            //        { text: "Medium", value: "1" },
            //        { text: "Low", value: "2" }
            //    ]
            //});



            //        $("#ddltype").kendoDropDownTree({
            //          placeholder: "Document Type",
            //          checkboxes: false,
            //          checkAll: true,
            //          autoClose: true,
            //         // checkAllTemplate: "Select All",
            //          autoWidth: true,
            //          dataTextField: "text",
            //          dataValueField: "value",
            //          change: function ()
            //          {
            //              if ($("#ddltype").val() == "Challan") {
            //$("#ddlChallanType").data("kendoDropDownList").enable(true);
            //              }
            //              else {
            //$("#ddlChallanType").data("kendoDropDownList").enable(false);
            //              }



            //              /*
            //                if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
            //                    //|| $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
            //                    || $("#dropdowntree").data("kendoDropDownTree")._values.length > 0
            //                    || $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
            //                    || $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
            //                    || $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {
            //                    setCommonAllfilterMain();
            //                }
            //                else {

            //                    var filter = { logic: "or", filters: [] };
            //                    var values = this.value();
            //                    $.each(values, function (i, v) {
            //                        filter.filters.push({
            //                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
            //                            field: "DocType", operator: "eq", value: v
            //                        });
            //                    });

            //                    var dataSource = $("#grid").data("kendoGrid").dataSource;
            //                    dataSource.filter(filter);
            //                }*/
            //                ApplyFilter();

            //                fCreateStoryBoard('ddltype', 'filterdoctype', 'doctype');
            //              $('input[id=chkAllMain]').prop('checked', false);  
            //              $('#dvbtndownloadDocumentMain').css('display', 'block');

            //          },
            //            dataSource: [                  
            //            { text: "Return", value: "Return" },
            //              { text: "Register", value: "Register" },
            //              { text: "Challan", value: "Challan" }

            //              //{ text: "Returns", value: "0" },
            //              //{ text: "Register", value: "7" },
            //              //{ text: "Challans", value: "18" }
            //          ]
            //      });



            $("#ddltype1").kendoDropDownTree({
                placeholder: "Document Type",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    // var filter = { logic: "or", filters: [] };
                    //var values = this.value();
                    //$.each(values, function (i, v) {                      
                    //    filter.filters.push({
                    //        field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    //fCreateStoryBoard('ddltype1', 'filterdoctype1', 'doctype1');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);  
                    if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        || $("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                        || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                        setCommonAllfilter();
                    }
                    //if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                    // || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                    //     || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                    //      setCommonAllfilter();
                    //  }
                    else {
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    fCreateStoryBoard('ddltype1', 'filterdoctype1', 'doctype1');
                },
                dataSource: [
                    { text: "Return", value: "Return" },
                    { text: "Register", value: "Register" },
                    { text: "Challan", value: "Challan" }

                    //{ text: "Returns", value: "0" },
                    //{ text: "Register", value: "7" },
                    //{ text: "Challans", value: "18" }
                ]
            });


            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    ApplyFilter();

					/*if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0                        
						|| $("#ddltype").data("kendoDropDownTree")._values.length > 0
						|| $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
						|| $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
						|| $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {
						setCommonAllfilterMain();
					}
					else {
						var filter = { logic: "or", filters: [] };
						var values = this.value();
						$.each(values, function (i, v) {
							//add after u get column for filter in API
							filter.filters.push({
								field: "Status", operator: "eq", value: v
							});
						});

						var dataSource = $("#grid").data("kendoGrid").dataSource;
						dataSource.filter(filter);
					}*/

                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                },
                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Pending For Review", value: "Pending For Review" }
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                },
                index: 0,
                dataSource: [
                    { text: "All", value: "All" },
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },

                ]
            });


            function DataBindDaynamicKendoGrid() {

                $('input[id=chkAll]').prop('checked', false);

                $("#grid1").data("kendoGrid").dataSource.filter({});

                $("#dropdowntree1").data("kendoDropDownTree").value([]);
                //$("#dropdownlistAct").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
                //$("#dropdownUser").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
                $("#Startdatepicker").data("kendoDatePicker").value(null);
                $("#Lastdatepicker").data("kendoDatePicker").value(null);
                $('#filterStartDate').html('');
                $('#filterLastDate').html('');
                $('#filterStartDate').css('display', 'none');
                $('#filterLastDate').css('display', 'none');
                $('#Clearfilter').css('display', 'block');
                $('#dvbtndownloadDocument').css('display', 'block');

                //$("#dvdropdownACT").css('display', 'block');

                var gridClear = $('#grid1').data("kendoGrid");
                if (gridClear != undefined || gridClear != null)
                    gridClear.setDataSource(null);

                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            //dataType: 'json',
                        },
                    },

                    schema: {
                        data: function (response) {

                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);

                if ($("#dropdownlistComplianceType1").val() == 1)//event based
                {
                    $('#dvdropdownEventNature1').css('display', 'block');
                    $('#dvdropdownEventName1').css('display', 'block');

                    $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                    $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                    $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                    $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
                }
                else {
                    $('#dvdropdownEventNature1').css('display', 'none');
                    $('#dvdropdownEventName1').css('display', 'none');

                    $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
                    $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature

                    $("#grid1").data("kendoGrid").showColumn(5);//Branch
                    $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
                }

                if ($("#dropdownFY").val() == "0") {
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownPastData").val() + '&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>',
                            <%--url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StaOrInt=SAT&StaIntFlag=' + $("#dropdownlistComplianceType1").val() + '&FY=',--%>
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            }
                        },
                        pageSize: 10,
                        schema: {
                            data: function (response) {

                                return response.Result;
                            },
                            total: function (response) {
                                return response.Result.length;
                            }
                        },
                        filterable: true,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }
                else {
                    var dataSource = new kendo.data.DataSource({
                        transport: {

                            read: {
                                url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=' + $("#dropdownPastData").val() + '&StaOrInt=SAT&StaIntFlag=5&FY=' + $("#dropdownFY").val() + '&profileID=<% =ProfileID%>',
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            }
                        },
                        pageSize: 10,
                        schema: {
                            data: function (response) {

                                return response.Result;
                            },
                            total: function (response) {
                                return response.Result.length;
                            }
                        }
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }
            }

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                },
                index: 0,
                dataSource: [
                    { text: "All", value: "All" },
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },

                ]
            });

            $("#dropdownlistMonth").kendoDropDownList({
                placeholder: "Month",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    ApplyFilter();
					/*if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
					   || $("#dropdowntree").data("kendoDropDownTree")._values.length > 0
					   || $("#ddltype").data("kendoDropDownTree")._values.length > 0
					   //|| $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
						|| $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
						|| $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {
						setCommonAllfilterMain();
					}
					else {
						var filter = { logic: "or", filters: [] };
						var values = this.value();
						if (values != "-1") {
							var fruits = [values];
							$.each(fruits, function (i, v) {
								filter.filters.push({
									field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
								});
							});
						}
						var dataSource = $("#grid").data("kendoGrid").dataSource;
						dataSource.filter(filter);
					}*/

                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                },
                dataSource: [
                    { text: "Month", value: "-1" },
                    { text: "January", value: "01" },
                    { text: "February", value: "02" },
                    { text: "March", value: "03" },
                    { text: "April", value: "04" },
                    { text: "May", value: "05" },
                    { text: "June", value: "06" },
                    { text: "July", value: "07" },
                    { text: "August", value: "08" },
                    { text: "September", value: "09" },
                    { text: "October", value: "10" },
                    { text: "November", value: "11" },
                    { text: "December", value: "12" }
                ]
            });

            $("#dropdownlistYear").kendoDropDownList({
                placeholder: "Year",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    ApplyFilter();
					/*if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
					   || $("#dropdowntree").data("kendoDropDownTree")._values.length > 0
					   || $("#ddltype").data("kendoDropDownTree")._values.length > 0
						//|| $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
						|| $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
					   || $("#dropdownlistAct").data("kendoDropDownList").text() != "Act"){
						setCommonAllfilterMain();
					}
					else
					{
						var filter = { logic: "or", filters: [] };
						var values = this.value();
						if (values != "-1") {
							var fruits = [values];
							$.each(fruits, function (i, v) {
								filter.filters.push({
									field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
								});
							});
						}
						var dataSource = $("#grid").data("kendoGrid").dataSource;
						dataSource.filter(filter);
					}*/

                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                },
                dataSource: [
                    { text: "Year", value: "-1" },
                    { text: "2021", value: "2021" },
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                    { text: "2015", value: "2015" },
                    { text: "2014", value: "2014" },
                    { text: "2013", value: "2013" },
                    { text: "2012", value: "2012" },
                    { text: "2011", value: "2011" },
                    { text: "2010", value: "2010" },
                    { text: "2009", value: "2009" }
                ]
            });
            //PE
            var BranchID = "";

            $("#ddlPrincipleEmployer").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                dataTextField: "PEName",
                dataValueField: "PEID",
                optionLabel: "Select PE",
                change: function (e) {
                    ApplyFilter();
                },
                dataSource: {
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetPrincipleEmployer?customerID=<% =CustId%>&ClientID=' + BranchID,

                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                },

                dataBound: function (e) {
                    e.sender.list.width("150");
                },
            });
            //END
            function GetPE() {
                var selectedActs = $("#dropdownlistAct").data("kendoDropDownList").value();
                if ((selectedActs != "-1") && (selectedActs != "")) {
                    //GET Act CLRA  GetActType
                    debugger;
                    $.ajax({
                        type: "POST",
                        url: "../RLCS/RLCS_MyComplianceDocuments.aspx/GetActType",
                        data: "{ 'ActID': '" + selectedActs + "' }",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            debugger;
                            if (data.d == "CLRA") {
                                $("#CLRAdiv").show();

                            }
                            else {
                                $("#CLRAdiv").hide();
                            }
                        },
                        failure: function (response) {

                        }
                    });

                }
            }
            $("#dropdownlistAct").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                dataTextField: "ACTNAME",
                dataValueField: "ACTID",
                optionLabel: "Act",
                change: function (e) {
                    ApplyFilter();
					/* if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
						//|| $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
						|| $("#ddltype").data("kendoDropDownTree")._values.length > 0
						|| $("#dropdowntree").data("kendoDropDownTree")._values.length > 0
						|| $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
						|| $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {
						setCommonAllfilterMain();
					}
					else {
						var filter = { logic: "or", filters: [] };
						var values = this.value();
						if (values != "" && values != null) {
							var fruits = [values];
							$.each(fruits, function (i, v) {
								filter.filters.push({
									field: "ActID", operator: "eq", value: parseInt(v)
								});
							});
						}

						var dataSource = $("#grid").data("kendoGrid").dataSource;
						dataSource.filter(filter);
					}*/
                },
                dataSource: {
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetApplicableActs?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&IsAvantis=true',

                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                },

                dataBound: function (e) {
                    e.sender.list.width("1000");
                },
            });

            $("#dropdownEventName").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    //e.preventDefault();

                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + $("#dropdownEventName").val() + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read:"<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });

            var eval = 0;

            if ($("#dropdownEventName").val() != '') {
                eval = $("#dropdownEventName").val()
            }

            $("#dropdownEventNature").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {

                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + eval + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            var eval1 = 0;

            if ($("#dropdownEventName").val() != '') {
                eval1 = $("#dropdownEventName").val()
            }

            $("#dropdownEventName1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {

                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + eval1 + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature1").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read:"<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });


            $("#dropdownEventNature1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {

                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + $("#dropdownEventName1").val() + '&RoleId=<% =RoleID%>'
                    }
                }
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                // autoClose: false,

                autoWidth: true,
                checkAllTemplate: "Select All",

                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {

                    ApplyFilter();

					/*if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
					    
						|| $("#ddltype").data("kendoDropDownTree")._values.length > 0
						|| $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
						|| $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
						|| $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {
						setCommonAllfilterMain();
					}
						else {
							var filter = { logic: "or", filters: [] };
							var values = this.value();
							$.each(values, function (i, v) {
								filter.filters.push({
									field: "CustomerBranchID", operator: "eq", value: parseInt(v)
								});
							});

							var dataSource = $("#grid").data("kendoGrid").dataSource;
							dataSource.filter(filter);
						}*/
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                },
                dataSource: null
            });









            function setCommonAllfilterMain() {

                if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    //&& $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#dropdownlistAct").data("kendoDropDownList").text() != "Act"
                    && $("#ddlPrincipleEmployer").data("kendoDropDownList").text() != "Select PE") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    ////risk Details
                    // var Riskdetails = [];
                    //var list1 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                    // $.each(list1, function (i, v) {
                    //     Riskdetails.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    // });

                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //Act Details  
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }
                    //PrincipleEmployer Details  
                    var PEdetails = [];
                    var listp = $("#ddlPrincipleEmployer").data("kendoDropDownList").value();
                    if (listp != "-1") {
                        var fruits = [listp];
                        $.each(fruits, function (i, v) {
                            PEdetails.push({
                                field: "PEID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }
                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: PEdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });
                    //Act Details  
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }
                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    //Act Details  
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            }
                        ]
                    });

                }


                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });



                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    //Act Details fixed 
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if ((list1 != "-1") && (list1 != "")) {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    //&& $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    ////risk Details
                    // var Riskdetails = [];
                    //var list1 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                    // $.each(list1, function (i, v) {
                    //     Riskdetails.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    //&& $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
                ) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    ////risk Details
                    // var Riskdetails = [];
                    //var list1 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                    // $.each(list1, function (i, v) {
                    //     Riskdetails.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});


                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //Act Details fixed 
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if ((list1 != "-1") && (list1 != "")) {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            }
                        ]
                    });

                }

                else if ($("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistAct").data("kendoDropDownList").text() != "Select Act") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //Act Details  
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }


                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: typedetails
                            }

                        ]
                    });

                }

                else if ($("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {


                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });


                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });

                }


                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });

                }


                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#ddltype").data("kendoDropDownTree")._values.length > 0) {


                    //Status details
                    var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                    var Statusdetails = [];
                    $.each(list, function (i, v) {
                        Statusdetails.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });


                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month") {


                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                    //location details
                    var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                    var locationsdetails = [];
                    $.each(list, function (i, v) {
                        locationsdetails.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });


                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                        ]
                    });

                }

                else if ($("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            }
                        ]
                    });

                }

                else if ($("#ddltype").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month") {

                    //type Details
                    var typedetails = [];
                    var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                    $.each(list12, function (i, v) {
                        typedetails.push({
                            //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                            field: "DocType", operator: "eq", value: v
                        });
                    });

                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: typedetails
                            },
                            {
                                logic: "or",
                                filters: Monthdetails
                            }
                        ]
                    });

                }

                else if ($("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
                    && $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                    //month Details  
                    var Monthdetails = [];
                    var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Monthdetails.push({
                                field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //year Details  
                    var Yeardetails = [];
                    var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                    if (list1 != "-1") {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Yeardetails.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }

                    //Act Details fixed 
                    var Actdetails = [];
                    var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                    if ((list1 != "-1") && (list1 != "")) {
                        var fruits = [list1];
                        $.each(fruits, function (i, v) {
                            Actdetails.push({
                                field: "ActID", operator: "eq", value: parseInt(v)
                            });
                        });
                    }



                    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Monthdetails
                            },
                            {
                                logic: "or",
                                filters: Yeardetails
                            },
                        ]
                    });

                }



                else {

                    if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });
                    }

                    if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                        //Status details
                        var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });


                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                }
                            ]
                        });
                    }

                    //if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    ////risk Details
                    //var Riskdetails = [];
                    //var list1 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                    //$.each(list1, function (i, v) {
                    //    Riskdetails.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});

                    //    var dataSource = $("#grid").data("kendoGrid").dataSource;

                    //    dataSource.filter({
                    //        logic: "and",
                    //        filters: [
                    //            {
                    //                logic: "or",
                    //                filters: Riskdetails
                    //            }
                    //        ]
                    //    });

                    //}

                    if ($("#ddltype").data("kendoDropDownTree")._values.length > 0) {


                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });
                    }
                    if ($("#dropdownlistMonth").data("kendoDropDownList").text() != "Month") {

                        //month Details  
                        var Monthdetails = [];
                        var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
                        if (list1 != "-1") {
                            var fruits = [list1];
                            $.each(fruits, function (i, v) {
                                Monthdetails.push({
                                    field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
                                });
                            });
                        }

                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Monthdetails
                                }
                            ]
                        });
                    }
                    if ($("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

                        //year Details  
                        var Yeardetails = [];
                        var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
                        if (list1 != "-1") {
                            var fruits = [list1];
                            $.each(fruits, function (i, v) {
                                Yeardetails.push({
                                    field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                                });
                            });
                        }

                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Yeardetails
                                }
                            ]
                        });
                    }
                    if ($("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {

                        //Act Details  
                        var Actdetails = [];
                        var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
                        //fixed
                        if (list1 != "-1") {
                            var lst = [list1];
                            $.each(lst, function (i, v) {
                                Actdetails.push({
                                    field: "ActID", operator: "eq", value: parseInt(v)
                                });
                            });
                        }

                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Actdetails
                                }
                            ]
                        });
                    }
                    if ($("#dropdownlistAct").data("kendoDropDownList").text() != "Select PE") {

                        //PrincipleEmployer Details  
                        var PEdetails = [];
                        var listp = $("#ddlPrincipleEmployer").data("kendoDropDownList").value();
                        //fixed
                        if (listp != "-1") {
                            var lst = [listp];
                            $.each(lst, function (i, v) {
                                PEdetails.push({
                                    field: "PEID", operator: "eq", value: parseInt(v)
                                });
                            });
                        }

                        var dataSource = $("#grid").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: PEdetails
                                }
                            ]
                        });
                    }
                }
            }

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAllTemplate: "Select All",
                //checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {

                    if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        || $("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                        || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                        setCommonAllfilter();
                    }
                    else {
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');

                    //var filter = { logic: "or", filters: [] };
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    //fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'block');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsList?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            //dataType: 'json',
                        },
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                },

                dataBound: function (e) {
                    placeholder: "Entity/Sub-Entity/Location1"
                }
            });





            function setCommonAllfilter() {



                if (($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                    || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                    if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Act Details
                        var Actdetails = [];
                        var list1 = $("#dropdownlistAct").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Actdetails.push({
                                field: "ACTNAME", operator: "eq", value: parseInt(v)
                            });
                        });


                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }




                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });



                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });



                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });


                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }


                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }



                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });


                        //date details
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //date details
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }


                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                }
                            ]
                        });

                    }

                    else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });
                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        //datefilter
                        var datedetails = [];
                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            datedetails.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "and",
                                    filters: datedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }


                    else {

                        if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0) {

                            //location details
                            var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                            var locationsdetails = [];
                            $.each(list, function (i, v) {
                                locationsdetails.push({
                                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                                });
                            });


                            //datefilter
                            var datedetails = [];
                            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }
                            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "and",
                                        filters: datedetails
                                    },
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }
                                ]
                            });


                        }

                        else if ($("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {


                            //Risk details
                            var list = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                            var Riskdetails = [];
                            $.each(list, function (i, v) {
                                Riskdetails.push({
                                    field: "Risk", operator: "eq", value: parseInt(v)
                                });
                            });


                            //datefilter
                            var datedetails = [];
                            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }
                            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "and",
                                        filters: datedetails
                                    },
                                    {
                                        logic: "or",
                                        filters: Riskdetails
                                    }
                                ]
                            });
                        }

                        else if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {


                            //Status details
                            var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                            var Statusdetails = [];
                            $.each(list, function (i, v) {
                                Statusdetails.push({
                                    field: "Status", operator: "eq", value: v
                                });
                            });


                            //datefilter
                            var datedetails = [];
                            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }
                            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "and",
                                        filters: datedetails
                                    },
                                    {
                                        logic: "or",
                                        filters: Statusdetails
                                    }
                                ]
                            });
                        }

                        else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0) {


                            //type Details
                            var typedetails = [];
                            var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                            $.each(list12, function (i, v) {
                                typedetails.push({
                                    //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                    field: "DocType", operator: "eq", value: v
                                });
                            });


                            //datefilter
                            var datedetails = [];
                            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }
                            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                                datedetails.push({
                                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                                });
                            }

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "and",
                                        filters: datedetails
                                    },
                                    {
                                        logic: "or",
                                        filters: typedetails
                                    }
                                ]
                            });
                        }

                    }
                }
                else//without date filter
                {
                    if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });



                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //datefilter

                        //    var datedetails = [];
                        //if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        //    datedetails.push({
                        //        field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                        //    });
                        //}
                        //if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        //    datedetails.push({
                        //        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                        //    });
                        //}




                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                //{
                                //    logic: "and",
                                //    filters: datedetails
                                //},
                                {
                                    logic: "or",
                                    filters: typedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                                //  {
                                //    logic: "and",
                                //    filters: datedetails
                                //}
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });



                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });



                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: typedetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: typedetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });


                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });


                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }

                    else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });
                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#ddltype1").data("kendoDropDownTree")._values.length > 0) {


                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });


                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {

                        //location details
                        var list = $("#dropdowntree1").data("kendoDropDownTree")._values;
                        var locationsdetails = [];
                        $.each(list, function (i, v) {
                            locationsdetails.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: locationsdetails
                                }
                            ]
                        });

                    }

                    else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {

                        //type Details
                        var typedetails = [];
                        var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                        $.each(list12, function (i, v) {
                            typedetails.push({
                                //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                field: "DocType", operator: "eq", value: v
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: typedetails
                                }
                            ]
                        });

                    }

                    else if ($("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        && $("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {

                        //risk Details
                        var Riskdetails = [];
                        var list1 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                        $.each(list1, function (i, v) {
                            Riskdetails.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        //Status details
                        var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                        var Statusdetails = [];
                        $.each(list, function (i, v) {
                            Statusdetails.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;

                        dataSource.filter({
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: Statusdetails
                                },
                                {
                                    logic: "or",
                                    filters: Riskdetails
                                }
                            ]
                        });

                    }

                    else {

                        if ($("#dropdowntree1").data("kendoDropDownTree")._values.length > 0) {

                            var filter = { logic: "or", filters: [] };
                            var values = $("#dropdowntree1").data("kendoDropDownTree")._values;
                            $.each(values, function (i, v) {
                                filter.filters.push({
                                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                                });
                            });

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }

                        else if ($("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0) {


                            //Risk details
                            var list = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                            var Riskdetails = [];
                            $.each(list, function (i, v) {
                                Riskdetails.push({
                                    field: "Risk", operator: "eq", value: parseInt(v)
                                });
                            });

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: Riskdetails
                                    }
                                ]
                            });
                        }

                        else if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {


                            //Status details
                            var list = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                            var Statusdetails = [];
                            $.each(list, function (i, v) {
                                Statusdetails.push({
                                    field: "Status", operator: "eq", value: v
                                });
                            });


                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: Statusdetails
                                    }
                                ]
                            });
                        }

                        else if ($("#ddltype1").data("kendoDropDownTree")._values.length > 0) {


                            //type Details
                            var typedetails = [];
                            var list12 = $("#ddltype1").data("kendoDropDownTree")._values;
                            $.each(list12, function (i, v) {
                                typedetails.push({
                                    //field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
                                    field: "DocType", operator: "eq", value: v
                                });
                            });

                            var dataSource = $("#grid1").data("kendoGrid").dataSource;

                            dataSource.filter({
                                logic: "and",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: typedetails
                                    }
                                ]
                            });
                        }

                    }
                }
            }



            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {


                    if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        || $("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                        || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                        setCommonAllfilter();
                    }
                    else {
                        var filter = { logic: "or", filters: [] };
                        //  values is an array containing values to be searched
                        var values = this.value();
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

                    //var filter = { logic: "or", filters: [] };
                    ////  values is an array containing values to be searched
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    //fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
                    //CheckFilterClearorNot();
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'block');
                },
                dataSource: [
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Year",

                change: function () {
                    //DataBindDaynamicKendoGrid();
                    //var filter = { logic: "or", filters: [] };
                    ////  values is an array containing values to be searched
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    //fCreateStoryBoard('dropdownFY', 'filterFY', 'FY1');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    if (values != "") {
                        var fruits = [values];
                        $.each(fruits, function (i, v) {
                            filter.filters.push({
                                field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
                            });
                        });
                    }
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);
                },
                dataSource: [
                    //{ text: "Year", value: "0" },
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                    { text: "2015", value: "2015" },

                ]
            });


            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {

                    var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>"
                    },
                    //schema: {
                    //    data: function (response) {                          
                    //        return response[0].dataSource;
                    //    },
                    //      model: {
                    //        children: "items"
                    //    }                      
                    //}
                }
                //dataSource: [
                //    { text: "User1", value: "0" },
                //    { text: "User2", value: "1" },
                //    { text: "User3", value: "2" }
                //]
            });

            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {


                    if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        || $("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                        || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                        setCommonAllfilter();
                    }
                    else {
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();
                        $.each(values, function (i, v) {
                            //add after u get column for filter in API
                            filter.filters.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }

                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');

                    //var filter = { logic: "or", filters: [] };
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    //add after u get column for filter in API
                    //    filter.filters.push({
                    //        field: "Status", operator: "eq", value: v
                    //    });
                    //});
                    //
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'block');
                },
                dataSource: [
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Pending For Review", value: "Pending For Review" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({

                autoWidth: true,

                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Statutory CheckList", value: "2" },
                ]
            });

            $("#dropdownType").kendoDropDownTree({
                placeholder: "Select Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "TypeName",
                dataValueField: "TypId",
                //optionLabel: "All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        //filter.filters.push({
                        //    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        //});
                    });
                    fCreateStoryBoard('dropdownType', 'filterCompType', 'CompType');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Data/BindTypesAll?CId=<% =CId%>"
                    }
                }
            });

            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Select Category",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "CategoryName",
                dataValueField: "CategoryId",
                //optionLabel: "All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        //filter.filters.push({
                        //    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        //});
                    });
                    fCreateStoryBoard('dropdownCategory', 'filterCategory', 'Category');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Data/BindCategoriesAll?CId=<% =CId%>"
                    }
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                //autoWidth: true,
                dataTextField: "ACTNAME",
                dataValueField: "ACTID",
                optionLabel: "Select Act",
                change: function (e) {

                    if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdowntree1").data("kendoDropDownTree")._values.length > 0
                        || $("#ddltype1").data("kendoDropDownTree")._values.length > 0
                        || $("#dropdownlistRisk1").data("kendoDropDownTree")._values.length > 0
                        || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                        || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {
                        setCommonAllfilter();
                    }
                    else {
                        ClearAllFilter();
                    }

                    //var values = this.value();
                    //if (values != "" && values != null) {
                    //    var filter = { logic: "or", filters: [] };
                    //    filter.filters.push({
                    //        field: "ActID", operator: "eq", value: parseInt(values)
                    //    });
                    //    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //    dataSource.filter(filter);
                    //}
                    //else {
                    //    ClearAllFilter();
                    //}
                },
                dataSource: {
                    transport: {

                        read: {
                            url: '<% =avacomRLCSAPI_URL%>GetApplicableActs?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&IsAvantis=true',

                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                },
              <%--  dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>"
                    }
                }--%>
            });

            $("#dropdownComplianceSubType").kendoDropDownTree({
                placeholder: "Compliance Sub Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        //filter.filters.push({
                        //    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        //});
                    });
                    fCreateStoryBoard('dropdownComplianceSubType', 'filterCompSubType', 'CompSubType');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Data/BindComplianceSubTypeList?UId=<% =UserId%>"
                    }
                }
            });

            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });
            $('#dvbtndownloadDocumentMain').css('display', 'block');
            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID, item.ID)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDownloadOverviewpup(item.ScheduledOnID, item.ID, item.IsStatuoryOREventBasedORCheckList)
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                e.preventDefault();

                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                //myWindowAdv.close();
                // $("#divAdvanceSearchModel").close();
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID);
                $("#divAdvanceSearchModel").data("kendoWindow").close();
                return true;
            });


            $(document).on("click", "#chkAll", function (e) {
                if ($('input[id=chkAll]').prop('checked')) {

                    $('input[name="sel_chkbx"]').each(function (i, e) {
                        e.click();
                    });
                }
                else {
                    $('input[name="sel_chkbx"]').attr("checked", false);
                }
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#chkAllMain", function (e) {
                if ($('input[id=chkAllMain]').prop('checked')) {
                    $('input[name="sel_chkbxMain"]').each(function (i, e) {
                        e.click();
                    });

                    // $('input[name="sel_chkbxMain"]').attr("checked", true);
                }
                else {

                    $('input[name="sel_chkbxMain"]').attr("checked", false);

                    //$('input[name="sel_chkbxMain"]').each(function (i, e) {
                    //    //e.prop("checked", false);
                    //    e.attr("checked", false);
                    //});
                }
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbx", function (e) {
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    $('#dvbtndownloadDocument').css('display', 'none');
                }
                else {
                    $('#dvbtndownloadDocument').css('display', 'block');
                }
                return true;
            });

            $(document).on("click", "#sel_chkbxMain", function (e) {
                if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                else {
                    $('#dvbtndownloadDocumentMain').css('display', 'block');
                }
                return true;
            });
        });

        function selectedDocument(e) {

            if (($('input[name="sel_chkbx"]:checked').length) == 0) {

                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbx"]').each(function (i, e) {
                if ($(e).is(':checked')) {

                    var ISStatutoryflag = 2;
                    if ($(e).attr('flag') !== '') {
                        if ($(e).attr('flag') === 'Statutory CheckList') {
                            ISStatutoryflag = 2;
                        } else if ($(e).attr('flag') === 'Statutory') {
                            ISStatutoryflag = -1;
                        }
                    }
                    checkboxlist.push(e.value + '/' + ISStatutoryflag);
                }
            });
            console.log(checkboxlist.join(","));


            $('#downloadfile').attr('src', "../ComplianceDocument/HRDownloadDoc.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType1").val());
            return false;
        }
        function selectedDocumentMain(e) {

            e.preventDefault();
            if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                alert("Please select atleast one record to download.");
                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbxMain"]').each(function (i, e) {
                if ($(e).is(':checked')) {

                    var ISStatutoryflag = 2;
                    if ($(e).attr('flag') !== '') {
                        if ($(e).attr('flag') === 'Statutory CheckList') {
                            ISStatutoryflag = 2;
                        } else if ($(e).attr('flag') === 'Statutory') {
                            ISStatutoryflag = -1;
                        }
                    }

                    checkboxlist.push(e.value + '/' + ISStatutoryflag);

                }
            });
            console.log(checkboxlist.join(","));

            $('#downloadfile').attr('src', "../ComplianceDocument/HRDownloadDoc.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType1").val());
            return false;
        }

        function ClearAllFilterMain(e) {

            e.preventDefault();
            $("#dropdowntree").data("kendoDropDownTree").value([]);

            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $("#ddltype").data("kendoDropDownTree").value([]);

            $("#dropdownlistMonth").data("kendoDropDownList").select(0);

            $("#dropdownlistYear").data("kendoDropDownList").select(0);
            $("#dropdownlistAct").data("kendoDropDownList").select(0);

            $('#dvbtndownloadDocumentMain').css('display', 'none');

            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);

            $("#grid").data("kendoGrid").dataSource.filter({});

            $('input[id=chkAllMain]').prop('checked', false);

            $('#ClearfilterMain').css('display', 'none');

        }

        function ClearAllFilter(e) {

            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);

            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            //$("#dropdownACT").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            //$("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#dropdownPastData").data("kendoDropDownList").select(0);

            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#ddltype1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#Clearfilter').css('display', 'none');

            $('#dvbtndownloadDocument').css('display', 'none');

            $("#grid1").data("kendoGrid").dataSource.filter({});


            $('input[id=chkAll]').prop('checked', false);
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            //for rebind if any pending filter is present (ADV Grid)
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            //fCreateStoryBoard('dropdownACT', 'filterAct', 'Act');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
            fCreateStoryBoard('ddltype', 'filterdoctype', 'doctype');
            fCreateStoryBoard('ddltype1', 'filterdoctype1', 'doctype1');

            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddltype').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddltype1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownFY').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownPastData').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownlistComplianceType1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#Clearfilter').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard               
            }
            else if (div == 'filterdoctype') {
                $('#' + div).append('DocumentType&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;');//Dashboard     
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterdoctype1') {
                $('#' + div).append('DocumentType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard     
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>&nbsp;');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }

        function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }

        function ChangeView() {
            $('#grid1').css('display', 'block');

            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").hideColumn(3);//FileName
            $("#grid1").data("kendoGrid").hideColumn(4);//V
            $("#grid1").data("kendoGrid").showColumn(5);//Branch
            $("#grid1").data("kendoGrid").showColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
            $("#grid1").data("kendoGrid").showColumn(9);//Scheduleon
            $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
            $("#grid1").data("kendoGrid").showColumn(11);//Status
            $("#grid1").data("kendoGrid").hideColumn(12);//VD
            $("#grid1").data("kendoGrid").hideColumn(13);//type
            $("#grid1").data("kendoGrid").hideColumn(14);//Uploaded Date
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            //$("#grid1").data("kendoGrid").hideColumn(16);//Size

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
            }

        }

        function ChangeListView() {
            $('#grid1').css('display', 'block');
            //  $('#grid2').css('display', 'none');
            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").showColumn(3);//FileName
            $("#grid1").data("kendoGrid").showColumn(4);//V
            $("#grid1").data("kendoGrid").hideColumn(5);//Branch
            $("#grid1").data("kendoGrid").hideColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Scheduleon
            $("#grid1").data("kendoGrid").hideColumn(8);//ForMonth
            $("#grid1").data("kendoGrid").hideColumn(9);//Status
            $("#grid1").data("kendoGrid").showColumn(10);//VD
            $("#grid1").data("kendoGrid").hideColumn(11);//type
            $("#grid1").data("kendoGrid").showColumn(12);//Uploaded Date
            $("#grid1").data("kendoGrid").showColumn(13);//Size
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
        }

        function ChangeAuditQView() {
            $('#grid1').css('display', 'none');
            //  $('#grid2').css('display', 'block');            
        }

        function exportReport() {
            $("#grid").getKendoGrid().saveAsExcel();
            return false;
        };

        function OpenOverViewpup(scheduledonid, instanceid) {

            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1150px');
            $('#OverViews1').attr('height', '500px');
            $('.modal-dialog').css('width', '1200px');

            if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {

                $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViews1').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        }

        function OpenDocumentOverviewpup(scheduledonid, transactionid) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '500px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/DocumentOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());
        }

        function OpenDownloadOverviewpup(scheduledonid, transactionid, type) {
            //$('#divDownloadView').modal('show');
            //$('#DownloadViews').attr('width', '401px');
            //$('#DownloadViews').attr('height', 'auto');
            //$('.modal-dialog').css('width', '437px');
            //$('.modal-dialog').css('height', '207px');

            var ISStatutoryflag = 2;

            if (type !== '') {
                if (type === 'Statutory CheckList') {
                    ISStatutoryflag = 2;
                } else if (type === 'Statutory') {
                    ISStatutoryflag = -1;
                }
            }

            $('#divDownloadView').modal('show');
            $('#DownloadViews').attr('width', '401px');
            // $('#DownloadViews').attr('height', 'auto');
            $('.modal-dialog').css('width', '437px');
            $('.modal-dialog').css('height', '300px');
            $('#DownloadViews').attr('src', "../Common/HRPlusDownloadOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + ISStatutoryflag);

        }

        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");
        }

        function CloseClearOV() {
            $('#OverViews').attr('src', "../Common/blank.html");
        }

        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");
        }
        function BindChallanType() {
            $("#ddlChallanType").kendoDropDownList({
                placeholder: "Challan Type",
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    $('#ClearfilterMain').css('display', 'block');
                    dropDownFilter();
                    $("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]);
                    //$('#ClearfilterMain').css('display', 'none');
                    //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    //$('input[name="sel_chkbx"]').attr("checked", false);
                    //$('input[name="sel_chkbx_Main"]').attr("checked", false);

                    <%-- var month = $("#ddlMonth").val();
                        var year = $("#ddlYear").val();

                        //if (month > 5 && year >= 2018) {  change by amol
                            if (month > <% =newRegistersMonth%> && year >= <% =newRegistersYear%>) {
                            var grid = $('#grid').data("kendoGrid");
                            grid.setDataSource(null);

                            BindSecondGrid();

                            $('#divddlChallanType').css('display', 'block');

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }

                            //BindSecondGrid();
                            //BindSecondGridMonth();
                            //BindChallansType();
                        } else if (year < <% =newRegistersYear%> || (month <= <% =newRegistersMonth%> && year <= <% =newRegistersYear%>)) {

                            $('#divddlChallanType').css('display', 'none');

                            BindFirstGrid();

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }
                        } else {
                            ClearAllFilter();
                        }--%>
                },
                dataSource: [
                    { text: "ESI", value: "ESI" },
                    { text: "EPF", value: "EPF" },
                    { text: "PT", value: "PT" },
                ]
            });
        }
        function Bind_ddltype(actType) {
            debugger;
            var _dllData = [
                { text: "Return", value: "Return" },
                { text: "Register", value: "Register" },
                { text: "Challan", value: "Challan" }
            ]
            $("#ddltype").kendoDropDownTree({
                placeholder: "Document Type",
                checkboxes: false,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    debugger;
                    ApplyFilter();
                    BindActGroupType($("#ddltype").val());
                    $('#ClearfilterMain').css('display', 'block');

                    if ($("#ddltype").val() == "Challan") {
                        $("#ddlChallanType").data("kendoDropDownList").enable(true);
                    }
                    else {
                        $("#ddlChallanType").data("kendoDropDownList").enable(false);
                    }

                    //if ($("#ddltype").val() == "Challan") {
                    //	BindEntities("Challan");
                    //	$('#divLocation').css('display', 'none');
                    //	$('#divEntity').css('display', 'inline');
                    //	$("#ddlChallanType").data("kendoDropDownList").enable(true);
                    //}
                    //else {
                    //	$("#ddlChallanType").data("kendoDropDownList").enable(false);
                    //	$('#divEntity').css('display', 'none');
                    //	$('#divLocation').css('display', 'inline');
                    //}

                    // ApplyFilter();
                    fCreateStoryBoard('ddltype', 'filterdoctype', 'doctype');
                    // $('input[id=chkAllMain]').prop('checked', false);
                    //$('#dvbtndownloadDocumentMain').css('display', 'block');

                },
                dataSource: _dllData

            });
            $("#ddltype").data("kendoDropDownTree").value("Register");

            if ($("#ddltype").data("kendoDropDownTree").value() != "Challan") {
                $("#ddlChallanType").data("kendoDropDownList").enable(false);
            }



        }
        function BindActGroupType(groupType) {

            var _dllActGroupData = [
                { text: "S&E/Factory", value: "S&E/Factory" },
                { text: "CLRA", value: "CLRA" }
            ]
            if (groupType == "Challan") {
                _dllActGroupData.length = _dllActGroupData.length - 1;
            }

            $("#ddlActGroup").kendoDropDownList({
                //checkAll: true,
                autoClose: true,
                //checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    debugger;
                    bindKendoData($("#ddlActGroup").val());
                    bindEntityLocationTree($("#ddlActGroup").val());
                    if ($("#ddltype").val() != "Challan") {
                        $("#ddlChallanType").data("kendoDropDownList").enable(false);
                    }
                    else {
                        $("#ddlChallanType").data("kendoDropDownList").enable(true);
                    }

                    //if ($("#ddltype").val() != "Challan" && $("#ddlActGroup").val() == "CLRA") {
                    //	BindEntities("PE");
                    //	$("#ddlChallanType").data("kendoDropDownList").enable(false);
                    //	$('#divEntity').css('display', 'inline');
                    //	$('#divLocation').css('display', 'none');
                    //}
                    //else {
                    //	$("#ddlChallanType").data("kendoDropDownList").enable(false);
                    //	$('#divEntity').css('display', 'none');
                    //	$('#divLocation').css('display', 'inline');
                    //}

                },
                dataSource: _dllActGroupData
            });
            $("#ddlActGroup").data("kendoDropDownList").value("S&E/Factory");

        }
		<%--function BindEntities(flag) {
			debugger;
			$("#ddlEntityList").kendoDropDownList({
				dataTextField: "AVACOM_BranchName",
				dataValueField: "CM_ClientID",
				optionLabel: "Select Entity",
				change: function (e) {
					$('#ClearfilterMain').css('display', 'block');
				},
				dataSource: {
					transport: {
						read: {
							url: "<% =avacomRLCSAPI_URL%>GetAssignedEntitiesORPrincipleEmployer?customerID=<% =CId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&flag=" + flag,
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							return response.Result;
						}
					}
				}
			});
		}--%>


        function bindKendoData(acttype) {
            debugger;
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<% =avacomRLCSAPI_URL%>GetMyDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&ActType=' + $("#ddlActGroup").val(),
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        dataType: 'json',
                    }
                },
                schema: {

                    data: function (response) {
                        if (response.Result != null) {
                            return response.Result;
                        }
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    },
                },

            });
            var grid = $("#grid").data("kendoGrid");
            grid.setDataSource(dataSource);
        }
        function bindEntityLocationTree(actType) {
            var dataSource = new kendo.data.HierarchicalDataSource({
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsListCLRA?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&ActGroup=' + $("#ddlActGroup").val(),
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        //dataType: 'json',
                    },
                },
                schema: {
                    data: function (response) {

                        return response.Result;
                    },
                    model: {
                        children: "Children"
                    }
                }
            })
            var ddl = $("#dropdowntree").data("kendoDropDownTree");
            ddl.setDataSource(dataSource);


        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div style="width: 99%;">
            <input id="ddltype" data-placeholder="type" style="width: 142px;" />
            <input id="ddlChallanType" data-placeholder="Challan Type" style="width: 79px;" />
            <input id="ddlActGroup" data-placeholder="type" style="width: 90px;" />
            <input id="dropdownlistMonth" data-placeholder="Month" style="width: 111px;" />
            <input id="dropdownlistYear" data-placeholder="Year" style="width: 102px;" />
            <input id="dropdownlistStatus" data-placeholder="Status" style="width: 93px; padding: 0px;" />
            <input id="dropdownlistAct" data-placeholder="Act" style="width: 222px;" />
            <div id="divLocation" style="display: inline;">
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 240px;" />
            </div>
            <%-- <div id="divEntity" style="display: inline;">
			     <input id="ddlEntityList" data-placeholder="Entity" style="width: 243px; display:none" />
		        </div>--%>
            <button id="dvbtndownloadDocumentMain" style="float: right; height: 30px; width: 23px; border-radius: 34px; border: 0px; background-color: transparent; padding-left: 13px;" data-placement="bottom"><span class="k-icon k-i-download k-state-border-down hoverCircle" onclick="selectedDocumentMain(event)"></span></button>
            <button id="AdavanceSearch" style="float: right; height: 30px; width: 21px; border-radius: 34px; border: 0px; background-color: transparent" data-placement="bottom" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-search hoverCircle " onclick="javascript:return false;"></span></button>
            <div id="CLRAdiv" class="lst-top" style="display: none;">
                <input id="ddlPrincipleEmployer" data-placeholder="Select PE" style="width: 150px;" />
            </div>
        </div>


        <div class="row">
            <div class="col-md-12" style="margin-top: 20px;">
                <div class="col-md-2" style="width: 17%;">
                </div>
                <div class="col-md-2" style="width: 15%;">
                    <div id="dvdropdownEventName" style="display: none;">
                        <input id="dropdownEventName" data-placeholder="Event Name" style="width: 175px;">
                    </div>
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 0px;">
                    <div id="dvdropdownEventNature" style="display: none;">
                        <input id="dropdownEventNature" data-placeholder="Event Nature">
                    </div>
                </div>
                <div class="col-md-2" style="width: 3%;">
                </div>


            </div>

            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterdoctype">&nbsp;</div>

            <div id="grid" <%--style="border: none;"--%>></div>
            <div>
                <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 360px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">

                                <iframe id="DownloadViews" src="about:blank" width="300px" height="210px" frameborder="0"></iframe>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="divAdvanceSearchModel" style="padding-top: 5px;">

                    <div class="row">
                        <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;">
                            <button id="primaryTextButton1" class="btn btn-primary" onclick="ChangeView()">Grid View</button>
                            <button id="primaryTextButton" class="btn btn-primary" onclick="ChangeListView()">List View</button>
                            <%-- <button id="primaryTextButton2" onclick="ChangeAuditQView()">IKEA Audit</button>--%>
                        </div>
                    </div>
                    <div class="row" style="margin-left: -9px;">
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                                <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;" />
                            </div>
                            <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                                <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                            </div>
                            <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                                <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                            </div>

                            <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                                <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                            </div>
                            <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                                <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 100%;" />
                            </div>
                            <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                                <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 115%;" />
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-left: -9px; margin-top: 7px; margin-bottom: 5px;">
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                                <input id="dropdownPastData" style="width: 100%;" />
                            </div>
                            <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px; display: none;">
                                <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%; display: none;" />
                                <input id="dropdownlistComplianceType" data-placeholder="Type" style="display: none;" />
                            </div>
                            <div class="col-md-2" id="dvdropdownlistComplianceType" style="width: 15.3%; padding-left: 0px;">
                                <input id="ddltype1" data-placeholder="type" style="width: 100%;" />
                            </div>
                            <div class="col-md-2" style="width: 13%; padding-left: 0px; display: none;">
                                <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                            </div>
                            <div class="col-md-4" id="dvdropdownACT" style="width: 29.3%; padding-left: 0px; display: none;">
                                <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                            </div>
                            <div class="col-md-2" style="width: 13.4%; padding-left: 0px;" id="dvdropdownlistStatus1">
                                <%if (RoleFlag == 1)%>
                                <%{%>
                                <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                                <%}%>
                            </div>

                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 5px;">
                        <div class="col-md-12">

                            <div class="col-md-2" style="width: 14.3%; margin-left: -31px;">
                                <div id="dvdropdownEventName1" style="display: none;">
                                    <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 196px;">
                                </div>
                            </div>
                            <div class="col-md-4" style="margin-left: 28px;">
                                <div id="dvdropdownEventNature1" style="display: none;">
                                    <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 166px;">
                                </div>
                            </div>
                            <%--<div class="col-md-2" style="">
                        </div>
                        <div class="col-md-3" style="">
                        </div>--%>
                            <div class="col-md-8" style="width: 37%; padding-left: 119px;">
                                <button id="Clearfilter" class=" btn-primary btn" style="float: right; margin-left: 1%; display: none; height: 30px;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                                <button id="dvbtndownloadDocument" class=" btn-primary btn" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>
                            </div>

                            <%--   <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>--%>
                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterCompType">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterCategory">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterAct">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterCompSubType">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterStartDate">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterLastDate">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filtersstoryboard1">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filtertype1">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterrisk1">&nbsp;</div>

                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterpstData1">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterUser">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterFY">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterstatus1">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterdoctype1">&nbsp;</div>
                    <div id="grid1"></div>
                </div>

                <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            fhead('My Documents/ Compliance Documents');
            setactivemenu('ComplianceDocumentList');
            BindChallanType();
            Bind_ddltype("S&E/Factory");
            bindKendoData("SEA");
            bindEntityLocationTree("SEA");
            // fmaters()
        });

    </script>
    <style>
        .k-filter-row th, .k-grid-header th[data-index='11'] {
            border-width: 1px 0px 0px 0px;
            /*background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color:#535b6a;*/
            text-align: center;
            color: #535b6a;
        }

        .k-filter-row th, .k-grid-header th[data-index='0'] {
            /*border-width: 1px 0px 0px 0px;*/
            /*background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color:#535b6a;
            height: 30px;*/
            text-align: center;
        }

        .k-filter-row th, .k-grid-header th[data-index='18'] {
            text-align: center;
        }

        .k-list-container.k-popup-dropdowntree .k-widget {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding: 0px;
        }

        .k-list-container.k-popup-dropdowntree .k-check-all {
            margin: 10px 14px 3px;
        }

        .k-radio, input.k-checkbox {
            margin-left: -5px;
        }
        /*.k-list-optionlabel, .k-popup .k-item {
    margin-left: -6px;
    cursor: default;
}
        .k-grid .k-hierarchy-cell .k-icon, .k-scheduler-table .k-icon, .k-treeview .k-icon {
    margin-left: 0px;
    background-color: transparent;
    border-radius: 4px;
}*/
    </style>
</asp:Content>
