﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSAPISettings
{
    public static class HttpClientConfig
    {
        public static HttpClient _httpClient { set; get; }

        static HttpClientConfig()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RLCS_API_URL"]))
            {
                _httpClient = new HttpClient();
                _httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCS_API_URL"]);
                _httpClient.Timeout = new TimeSpan(0, 15, 0);
                _httpClient.DefaultRequestHeaders.Accept.Clear();
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                _httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
                _httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
                _httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            }
        }
    }
}