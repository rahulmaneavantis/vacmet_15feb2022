﻿<%@ Page Title="My Reports" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyReports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyReports" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta name="author" content="AVANTIS - Development Team" />

    <title>My Reports :: AVANTIS - Products that simplify</title>

    <!-- Offline-->
    <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../newjs/spectrum.js"></script>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <%--<link href="../NewCSS/Kendouicss.css" rel="stylesheet" />--%>

    <style type="text/css">
        .p-r-5 {
            padding-left: 5px !important;
            padding-right: 5px !important;
            /*margin-right:2% !important;*/
        }

        .chosen-container {
            position: relative;
            /* display: inline-block; */
            vertical-align: middle;
            font-size: 13px;
            zoom: 1;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        .control-label {
            margin-top: 5px !important;
            color: #999 !important;
        }

        .m-l {
            margin-left: 10px !important;
        }

        .p-t {
            padding-top: 10px !important;
        }

        .loadingfile {
            position: absolute;
            top: 200px;
            left: 533px;
            z-index: 2;
            width: 100%;
            display: none;
        }
    </style>

    <script>
        $(document).ready(function () {
         ApiTrack_Activity("My Reports", "pageView", null);
        });
        function downloadMIS(path) {
            //$('#dwnfile').attr('src', path);
            $('.loadingfile').show();
            $.ajax({
                url: path,
                dataType: "jsonp",
                timeout: 5000,

                success: function (response) {
                    $('.loadingfile').hide();
                },
                error: function (parsedjson) {
                    if (parsedjson != null && parsedjson.statusText != "success") {
                        $('.loadingfile').hide();
                        $('#fileNot').modal('show');
                        setTimeout(function () { $('#fileNot').modal('hide'); }, 3000);
                    } else {
                        $('#dwnfile').attr('src', path);
                        $('.loadingfile').hide();
                    }
                }
            });
        }

        function clearMonthlySummary() {
            $('#divMonthlyCompliancePieChart').css('display', 'none');
            $('#divMonthlyComplianceColumnChart').css('display', 'none');
        }

        function ClearOboradingSummary() {
            $('#divonBoardingCompliancePieChart').css('display', 'none');
            $('#divonBoardingComplianceColumnChart').css('display', 'none');
        }

        $(document).ready(function () {

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/State/Location/Branch",
                checkboxes: {
                    checkChildren: true
                },

                //checkboxes: true,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>GetAssignedEntitiesLocationsList?customerId=<% =customerID%>&userId=<% =userID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#ddlIlstforMonth").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Month",
                change: function (e) {

                    //var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    if (values != "" && values != null) {

                        var Year = $("#ddlIlstforYear").val();
                        // var location = $("#dropdowntree").data("kendoDropDownTree").dataItem(e.node);$("#s1 option:selected").val()

                        $('#DownloadViews').attr('src', "RLCS_MyReports.aspx?Month=" + values + "&Year=" + Year);
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                },
                dataSource: [
                    { text: "--Select--", value: "0" },
                    { text: "JANUARY", value: "1" },
                    { text: "FEBUARY", value: "2" },
                    { text: "MARCH", value: "3" },
                    { text: "APRIL", value: "4" },
                    { text: "MAY", value: "5" },
                    { text: "JUNE", value: "6" },
                    { text: "JULY", value: "7" },
                    { text: "AUGUST", value: "8" },
                    { text: "SEPTEMBER", value: "9" },
                    { text: "OCTOBER", value: "10" },
                    { text: "NOVEMBER", value: "11" },
                    { text: "December", value: "12" },
                ],
            });

            $("#ddlIlstforYear").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: {
                    text: "Select Year",
                    value: 0
                },

                change: function (e) {
                    //
                    //var filter = { logic: "or", filters: [] };
                    var values = this.value();

                    if (values != "" && values != null) {
                        var Month = $("#ddlIlstforMonth").val();
                        $('#DownloadViews').attr('src', "RLCS_MyReports.aspx?Month=" + Month + "&Year=" + values);
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                },
                dataSource: [
                    { text: "--Select--", value: "0" },
                    { text: "2016", value: "2016" },
                    { text: "2017", value: "2017" },
                    { text: "2018", value: "2018" },
                ],
            });


        });
    function fCreateStoryBoard(Id, div, filtername) {
        var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
        $('#' + div).html('');

        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
        $('#' + div).css('display', 'block');

        $('#Clearfilter').css('display', 'none');

        if (div == 'filtersstoryboard') {
            $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        }
        else if (div == 'filtertype') {
            $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            //  $('#' + div).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        }
        else if (div == 'filterrisk') {
            $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        }
        else if (div == 'filterstatus') {
            $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        }
        else if (div == 'filterpstData1') {
            $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterCategory') {
            $('#' + div).append('Category&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterAct') {
            $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterCompSubType') {
            $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterCompType') {
            $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filtersstoryboard1') {
            $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filtertype1') {
            $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterrisk1') {
            $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterFY') {
            $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterUser') {
            $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterstatus1') {
            $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }

        for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
            var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
            $(button).css('display', 'none');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
            var buttontest = $($(button).find('span')[0]).text();
            $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;white-space: nowrap;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
        }



        if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
            //
            $('#' + div).css('display', 'none');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

        }

    }
    function fcloseStory(obj) {

        var DataId = $(obj).attr('data-Id');
        var dataKId = $(obj).attr('data-K-Id');
        var seq = $(obj).attr('data-seq');
        var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
        $(deepspan).trigger('click');
        var upperli = $('#' + dataKId);
        $(upperli).remove();
    };
    </script>
    <script>
        //$(document).on.click(function (event) {
        //    
        //    if (event.target.id == "") {
        //        var idvid = $(event.target).closest('div');
        //        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation1') > -1) {
        //            $("#divFilterLocation").show();
        //        } else {
        //            $("#divFilterLocation").hide();
        //        }
        //    }
        //    else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
        //        $("#divFilterLocation").hide();
        //    } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
        //        $("#divFilterLocation").show();
        //    } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
        //        $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

        //        $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
        //            $("#divFilterLocation").toggle("blind", null, 500, function () { });
        //        });

        //    }
        //});
        $(document).on("click", "#DivFiltersMonthly1", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation1') > -1) {
                    $("#divFilterLocation1").show();
                } else {
                    $("#divFilterLocation1").hide();
                }
            }
            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation1") {
                $("#divFilterLocation1").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation1') > -1) {
                $("#divFilterLocation1").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation1") {
                $("#ContentPlaceHolder1_tbxFilterLocation1").unbind('click');

                $("#ContentPlaceHolder1_tbxFilterLocation1").click(function () {
                    $("#divFilterLocation1").toggle("blind", null, 500, function () { });
                });

            }
        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="modal fade" id="fileNot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 360px; height: 200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="fvideostop()">×</button>
                </div>
                <div class="modal-body">
                    <h1 id="filenotfoundmessage"><img src="../Images/Returns.png" />&nbsp;&nbsp;&nbsp;File not found</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="loadingfile">
        <img src="../images/processing.gif" />
    </div>

    <iframe src="about:blank" id="dwnfile" style="display: none;"></iframe>
    <div style="margin-bottom: 7px">
        <asp:ValidationSummary ID="VSReportValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
            ValidationGroup="VSReportValidationGroup" />
        <asp:CustomValidator ID="cvReportvalidation" runat="server" Visible="false" EnableClientScript="False"
            ValidationGroup="VSReportValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
    </div>
    <div>
        
        <div class="row" id="DivFiltersMonthly1" style="padding-left:1%">
            <div class="col-md-4 colpadding0" id="divFilterLocation12">
                <asp:TextBox runat="server" ID="tbxFilterLocation1" PlaceHolder="Select Entity/State/Location/Branch" autocomplete="off" CssClass="clsDropDownTextBox" />
                <div style="margin-left: 1px; position: absolute; z-index: 10; margin-top: -20px; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation1">
                    <asp:TreeView runat="server" ID="tvFilterLocation1" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                        Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                        OnSelectedNodeChanged="tvFilterLocation1_SelectedNodeChanged">
                    </asp:TreeView>
                </div>
            </div>             
            
            <div class="col-md-2 colpadding0" id="Monthdd">
                <%--<input id="ddlIlstforMonth" style="width: 100%" />--%>
                <asp:DropDownListChosen runat="server" ID="ddlMonth" AutoPostBack="True" Width="98%" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged"
                    class="form-control" DataPlaceHolder="Month">
                    <asp:ListItem Selected="True" Enabled="true" Text="--Select--" Value="0" />
                    <asp:ListItem Text="January" Value="1" />
                    <asp:ListItem Text="February" Value="2" />
                    <asp:ListItem Text="March" Value="3" />
                    <asp:ListItem Text="April" Value="4" />
                    <asp:ListItem Text="May" Value="5" />
                    <asp:ListItem Text="June" Value="6" />
                    <asp:ListItem Text="July" Value="7" />
                    <asp:ListItem Text="August" Value="8" />
                    <asp:ListItem Text="September" Value="9" />
                    <asp:ListItem Text="October" Value="10" />
                    <asp:ListItem Text="November" Value="11" />
                    <asp:ListItem Text="December" Value="12" />
                </asp:DropDownListChosen>
            </div>

            <div class="col-md-2 colpadding0" id="Yeardd">
                <%-- <input id="ddlIlstforYear" style="width: 100%" />--%>
                <asp:DropDownListChosen runat="server" ID="ddlYear" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"
                    class="form-control" Width="98%" DataPlaceHolder="Year">
                    <asp:ListItem Text="--Select--" Value="0" Selected="True" Enabled="true" />
                    <asp:ListItem Text="2017" Value="2017" />
                    <asp:ListItem Text="2018" Value="2018" />
                    <asp:ListItem Text="2019" Value="2019" />
                    <asp:ListItem Text="2020" Value="2020" />
                    <asp:ListItem Text="2021" Value="2021" />
                </asp:DropDownListChosen>
            </div>
            
        </div>

        <div class="row Dashboard-white-widget dashboard p-t" id="Boarding" runat="server">
            <div class="col-lg-12 col-md-12 p-r-5">
                <div class="panel panel-default">
                    <div id="DivFilters">
                        <div id="collapseonBoardingSummary" style="display: none;" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3" style="padding-left: 0px; padding-right: 0px; width: 25%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" CssClass="txtbox" autocomplete="off" />
                                        <div style="margin-left: 1px; display: none; position: absolute; z-index: 10" id="divFilterLocation">
                                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="239px" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="divonBoardingCompliancePieChart" class="col-md-5" style="width: 34.5%"></div>
                                <div id="divonBoardingComplianceColumnChart" class="col-md-7" style="width: 65.5%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="Compliances" runat="server">
            <div class="dashboard p-t">
                <div class="col-lg-12 col-md-12 p-r-5">
                    <div class="panel panel-default">
                        <%-- <div class="panel-heading m-l">
                        <%--<a data-toggle="collapse" data-parent="#accordion" href="#DivFiltersMonthly">
                            <h2>Compliance Summary </h2>
                        </a>
                        <div class="panel-actions">
                         <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#DivFiltersMonthly"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('FunctionSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>--%>
                        <div id="DivFiltersMonthly">
                            <div id="collapseFunctionSummary" class="panel-collapse collapse in">
                                <div class="panel-body">

                                    <%-- <div class="row">
                                    <div class="k-header k-grid-toolbar">
                                        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>
                                    </div>
                                </div>--%>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="divMonthlyCompliancePieChart" class="col-md-5" style="width: 34.5%"></div>
                                            <div id="divMonthlyComplianceColumnChart" class="col-md-7" style="width: 65.5%"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript">

       <%-- $(document).ready(function(){
            $("#divMonthlyComplianceColumnChart").click( 
    function(evt){
        
        var location= window["<%=tvFilterLocationMonthly.ClientID%>" + "_Data"];
        var selectedNode = document.getElementById(location.selectedNodeID.value);
        var value = selectedNode.href.substring(selectedNode.href.indexOf(",") + 3, selectedNode.href.length - 2);
        var text = selectedNode.innerHTML;
        alert(text);

    });  
        })--%>
            //<!-- Graph-Monthly Compliance-Column Chart -->
            $(document).ready(function () {
                setactivemenu('leftreportsmenu');

           <%-- var reportType='<%=showReportType%>';--%>
                var reportType = ReadQuerySt('ReportType');

                if (reportType != null) {

                    if (reportType == "B") {
                        // fhead('Onboarding Compliance Summary');
                        fhead('My Reports/ Onboarding Compliance Summary');
                        $('#ContentPlaceHolder1_Compliances').hide();
                        $('#ContentPlaceHolder1_Boarding').show();
                        BindOnBoardingPie();
                        BindOnBoardingBar();
                        $('#Yeardd').hide();
                        $('#Monthdd').hide();
                    } else {
                        //fhead('Monthly Compliance Summary');
                        fhead('My Reports/ Monthly Compliance Summary');
                        $('#ContentPlaceHolder1_Boarding').hide();
                        $('#ContentPlaceHolder1_Compliances').show();
                        BindMonthlyPie();
                        BindMonthlyBar();
                    }
                }

                function BindMonthlyPie() {

                    var chart = new Highcharts.Chart({
                        chart: {
                            renderTo: 'divMonthlyCompliancePieChart',
                            type: 'pie',
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Client Compliance Summary'
                        },
                        plotOptions: {
                            pie: {
                                //innerSize: '40%',
                                //  dataLabels: { enabled: true, format: '{y}', distance: 2 }
                                allowPointSelect: true,
                                cursor: 'pointer',

                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b><br>{point.percentage:.0f} %',
                                    distance: -50,
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                },

                            }
                        },
                        //tooltip: {
                        //    formatter: function () {
                        //        return '<b>' + this.point.name + '</b>: ' + this.y;
                        //    }
                        //},
                        tooltip: {
                            pointFormat: '<b>{point.percentage:.0f}%</b>'
                        },
                        //colors: ['#62a8ea', '#f96868'],
                        series:
                            [{
                                data: [ <%= seriesData_GraphMonthlyCompliancePie%>],
                            size: '100%',
                            showInLegend: true,
                            allowPointSelect: false,
                            point: {
                                events: {
                                    legendItemClick: function (e) {
                                        e.preventDefault();
                                    }
                                }
                            }
                        }]

                });
                }

                function BindMonthlyBar() {
                    $(function () {
                        // Chart Global options
                        Highcharts.setOptions({
                            credits: {
                                text: '',
                                href: 'https://teamleaseregtech.com',
                            },
                            lang: {
                                drillUpText: "< Back",
                            },

                        });

                        var DeptWiseColumnChart = Highcharts.chart('divMonthlyComplianceColumnChart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Location Compliance Summary'
                            },
                            subtitle: {
                                text: ''
                            },

                      <%= seriesData_GraphMonthlyComplianceCategory%>

                        yAxis: {
                            min: 0,
                            title: {
                                text: ''
                            },
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'grey'
                                }
                            }

                        },
                        legend: {
                            align: 'right',
                            x: -30,
                            verticalAlign: 'top',
                            y: 25,
                            floating: true,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: false

                        },
                        credits: {
                            enabled: false
                        },
                        //tooltip: {
                        //    headerFormat: '<b>{point.x}</b><br/>',
                        //    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                        //},
                        tooltip: {
                            backgroundColor: 'rgba(247,247,247,1)',
                            //pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                            pointFormat: '<span style="color:{series.color}">{series.name}</span>: {point.percentage:.0f}%<br/>',
                            shared: true
                        },

                        plotOptions: {
                            column: {
                                stacking: 'percent',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b><br>{point.percentage:.0f} %',
                                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                                },
                                filter: {
                                    property: 'percentage',
                                    operator: '>',
                                    value: 4
                                },
                                allowPointSelect: false,
                                cursor: 'pointer',
                            },
                            series: {
                                events: {
                                    legendItemClick: function (e) {
                                        e.preventDefault();
                                    }
                                }
                            },
                        },
                        legend: {
                            itemDistance: 2,
                        },

                        series: [<%=seriesData_GraphMonthlyComplianceColumn%>]
                    });
                });
            }

           <%-- function BindMonthlyBar() {
                var DeptWiseColumnChart =
Highcharts.chart('divMonthlyComplianceColumnChart', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },

        <%= seriesData_GraphMonthlyComplianceCategory%>

    yAxis: {
        title: {
            text: ''
        },
        stackLabels: {
            enabled: false,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'grey'
            }
        }
    },
    colors: ['#62a8ea', '#f96868'],
    //legend: {
    //    enabled: false,
    //    //enabled: true,
    //    //itemDistance: 0,
    //    //itemStyle: {
    //    //    textshadow: false,
    //    //},
    //},
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.y}</b><br/>',
        pointFormat: '{series.name}: {point.y:.2f} %<br/>Total: {point.stackTotal:.2f} %'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b><br>{point.percentage:.0f} %',
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
            },

            point: {
                events: {
                    click: function() {
                        
                    }
                }
            }
        }
        //series: {
        //    borderWidth: 0,
        //    dataLabels: {
        //        enabled: true,
        //        format: '{point.y}',
        //        style: {
        //            textShadow: false,
        //            // color:'red',
        //        }
        //    },
        //},
    },
    legend: {
        itemDistance: 2,
    },

    tooltip: {
        //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{series.name}</span>: <b>{point.percentage:.1f}</b><br/>'
    },
    series: [<%=seriesData_GraphMonthlyComplianceColumn%>]
    //series: [{
    //    name: 'Complied',
    //    data: dataset1
    //},
    //    {
    //        name: 'Not Complied',
    //        data: dataset2
    //    }]
});--%>

                //}

                function BindOnBoardingPie() {


                    var chart = new Highcharts.Chart({
                        chart: {
                            renderTo: 'divonBoardingCompliancePieChart',
                            type: 'pie',
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Client Health Check Summary'
                        },
                        plotOptions: {
                            pie: {
                                //innerSize: '40%',
                                //  dataLabels: { enabled: true, format: '{y}', distance: 2 }
                                allowPointSelect: true,
                                cursor: 'pointer',

                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b><br>{point.percentage:.0f} %',
                                    distance: -50,
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                }
                            }
                        },
                        tooltip: {
                            pointFormat: '<b>{point.percentage:.0f}%</b>'
                        },
                        //tooltip: {
                        //    formatter: function () {
                        //        return '<b>' + this.point.name + '</b>: ' + this.y;
                        //    }
                        //},
                        colors: ['#62a8ea', '#f96868'],

                        series:
                                                [{
                                                    data: [
                         <%=seriesData_GraphBoardingCompliancePie%>
                                                ],
                                                size: '100%',
                                                showInLegend: true,
                                                allowPointSelect: false,
                                                point: {
                                                    events: {
                                                        legendItemClick: function (e) {
                                                            e.preventDefault();
                                                        }
                                                    }
                                                }
                                            }]
                });
            }



                function BindOnBoardingBar() {

                    var DeptWiseColumnChart =
                        Highcharts.chart('divonBoardingComplianceColumnChart', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Location Health Check Summary'
                            },
                            subtitle: {
                                text: ''
                            },

        <%= seriesData_GraphBoardingComplianceCategory%>

                            yAxis: {
                                 min: 0,
                                title: {
                                    text: ''
                                },
                                stackLabels: {
                                    enabled: false,
                                    style: {
                                        fontWeight: 'bold',
                                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'grey'
                                    }
                                }
                            },

                            colors: ['#62a8ea', '#f96868'],
                            //legend: {
                            //    enabled: false,
                            //    //enabled: true,
                            //    //itemDistance: 0,
                            //    //itemStyle: {
                            //    //    textshadow: false,
                            //    //},
                            //},
                            legend: {
                                align: 'right',
                                x: -30,
                                verticalAlign: 'top',
                                y: 25,
                                floating: true,
                                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                borderColor: '#CCC',
                                borderWidth: 1,
                                shadow: false
                            },
                            tooltip: {
                                backgroundColor: 'rgba(247,247,247,1)',
                                //pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                                pointFormat: '<span style="color:{series.color}">{series.name}</span>: {point.percentage:.0f}%<br/>',
                                shared: true
                            },
                            //tooltip: {
                            //    headerFormat: '<b>{point.y}</b><br/>',
                            //    pointFormat: '{series.name}: {point.y:.2f} %<br/>Total: {point.stackTotal:.2f} %'
                            //},
                            //tooltip: {
                            //    //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            //    pointFormat: '<span style="color:{point.color}">{series.name}</span>: <b>{point.percentage:.1f}</b><br/>'
                            //},
                            credits: {
                                enabled: false
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'percent',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b><br>{point.percentage:.0f} %',
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                                    },
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                },
                                series: {
                                    events: {
                                        legendItemClick: function (e) {
                                            e.preventDefault();
                                        }
                                    }
                                },
                            },
                            legend: {
                                itemDistance: 2,
                            },
                            
                            series: [<%=seriesData_GraphMonthlyComplianceColumn%>]
                            //series: [{
                            //    name: 'Complied',
                            //    data: dataset1
                            //},
                            //    {
                            //        name: 'Not Complied',
                            //        data: dataset2
                            //    }]
                        });

}


                //$("#DivFilters").click(function (event) {
                //    if (event.target.id == "") {
                //        var idvid = $(event.target).closest('div');
                //        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                //            $("#divFilterLocation").show();

                //        } else {
                //            $("#divFilterLocation").hide();
                //        }
                //    }
                //    else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                //        $("#divFilterLocation").hide();

                //    } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                //        $("#divFilterLocation").show();
                //    } else if (event.target.id == "ContentPlaceHolder2_tbxFilterLocation") {
                //        $("#ContentPlaceHolder2_tbxFilterLocation").unbind('click');

                //        $("#ContentPlaceHolder2_tbxFilterLocation").click(function () {
                //            $("#divFilterLocation").toggle("blind", null, 500, function () { });
                //        });

                //    }
                //});

                //$("#DivFiltersMonthly").click(function (event) {
                //    if (event.target.id == "") {
                //        var idvid = $(event.target).closest('div');
                //        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocationMonthly') > -1) {

                //            $("#divFilterLocationMonthly").show();
                //        } else {
                //            $("#divFilterLocationMonthly").hide();
                //        }
                //    }
                //    else if (event.target.id != "ContentPlaceHolder2_tbxFilterLocationMonthly") {
                //        $("#divFilterLocationMonthly").hide();
                //    } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocationMonthly') > -1) {
                //        $("#divFilterLocationMonthly").show();
                //    } else if (event.target.id == "ContentPlaceHolder2_tbxFilterLocationMonthly") {
                //        $("#ContentPlaceHolder2_tbxFilterLocationMonthly").unbind('click');

                //        $("#ContentPlaceHolder2_tbxFilterLocationMonthly").click(function () {
                //            $("#divFilterLocationMonthly").toggle("blind", null, 500, function () { });
                //        });

                //    }
                //});
            });

$(document).ready(function () {
    $(function () {
        // Chart Global options
        Highcharts.setOptions({
            credits: {
                text: '',
                href: 'https://teamleaseregtech.com',
            },
            lang: {
                drillUpText: "< Back",
            },
        });
    });
});
$('#divFilterLocation1').hide();
//<!-- Graph-Monthly Compliance-Column Chart END-->
        </script>

        <script type="text/javascript">

            $(".chosen-container").css({ 'display': '' });
            //$("#Boarding").hide();
            //$('.chosen-container').removeAttr('display','inline-block');
        </script>
        <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 360px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
