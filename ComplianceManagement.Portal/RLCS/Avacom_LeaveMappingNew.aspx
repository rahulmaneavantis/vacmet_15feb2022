﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="Avacom_LeaveMappingNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.Avacom_LeaveMappingNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss_V1.css" rel="stylesheet" />
    <title></title>
    <style type="text/css">
        .k-textbox > input {
            text-align: left;
        }

        .btn {
            font-size: 14px;
            height: 36px;
        }

        label.k-label:hover {
            color: #1fd9e1;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 0;
        }

        .k-grid tbody td {
            padding: 5px;
        }

        .k-grid-content {
            min-height: auto !important;
        }

        .k-grid tbody .k-button {
            min-width: 25px;
            min-height: 25px;
            background-color: transparent;
            border: none;
            margin-left: 0px;
            margin-right: -7px;
            padding-left: 0px;
            padding-right: 15px;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .k-grid, .k-listview {
            margin-top: 10px;
        }

            .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
                -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
                box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            }

        .k-tooltip-content {
            width: max-content;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px;
            line-height: normal;
        }

        .k-calendar-container {
            background-color: white;
            width: 217px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top: 0px;
        }
        .k-pager-input, .k-pager-numbers li {
            margin-left: 0px;
            float: left;
        }

        .k-widget.k-grid .k-pager-numbers {
            position: relative;
        }



        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }


        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-menu .k-item .k-item, .k-menu-scroll-wrapper .k-item .k-item, .k-menu-scroll-wrapper.vertical > .k-item, .k-popups-wrapper .k-item .k-item, .k-popups-wrapper.vertical > .k-item, ul.k-menu-vertical > .k-item {
            display: block;
            float: none;
            margin-left: 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
            color: #515967;
            padding-top: 5px;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -1px;
            margin-left: 2px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width: 0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }
       

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-i-filter-clear {
            margin-top: -4px;
            margin-right: 3px;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height: 34px;
        }

        .k-multiselect-wrap, .k-floatwrap {
            height: 34px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
            margin-left: -3px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .alert {
            margin-top: 2px;
            padding-bottom: 0px;
            padding-top: 0px;
            border: 1px solid transparent;
        }

        .alert-danger {
            background-color: #ffe0e6;
            border-color: #ffd0e1;
            margin-right: -13px;
            margin-left: -13px;
            color: #ff2d55;
        }

        .k-listbox .k-item, .k-popup .k-list .k-item {
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            margin-left: 0px;
        }


        ul li {
            list-style: none;
            margin-left: 20px;
        }
        /*.k-pager-numbers .k-link, .k-treeview .k-in {
            border-color: transparent;
            margin-left: -19px;
        }*/

        #divinline {
            display: inline-grid;
        }
    </style>
    <script type="text/javascript">
        var AVLeave;
        var ddlClientValue;
        $(document).ready(function () {
            fhead('Masters/Avacom_LeaveType');
            $("#ModalAttendanceMapping").modal("hide");
            $('#btnUploadLeaveDetails').hide();
            $('#ClearfilterMain').hide();
            $('#txtSearch').css('visibility', 'hidden');
            debugger;
            Bind_Clients();

            BindFirstGrid();
            $("#divMsg").hide();
            $("#btnSave").click(function (e) {
                debugger;
                var DetailsObj = {
                    clientCode: $("#txtClientCode").val(),
                    LeaveType: AVLeave,
                    clientID: $("#ddlClientList").data("kendoDropDownList").value()

                }
                if (DetailsObj.clientCode == "") {
                    $("#divMsg").show();
                    $("#spanMsg").show();
                    $("#divMsgPanel").removeClass("alert-success");
                    $("#divMsgPanel").addClass("alert-danger");
                    $("#spanMsg").html("Client Code is empty.");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/RLCS/Avacom_LeaveMappingNew.aspx/SaveLeaveMappingDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                $("#divMsg").show();
                                debugger;
                                $("#spanMsg").show();
                                $("#spanMsg").html("Details Save Successfully");
                                $("#divMsgPanel").removeClass("alert-danger");
                                $("#divMsgPanel").addClass("alert-success");
                                $("#btnSave").attr("disabled", true);
                                setTimeout(function () {
                                    $("#ModalAttendanceMapping").modal("hide");
                                    $("#btnSave").attr("disabled", false);
                                }, 3000);

                                BindFirstGrid();

                            }
                            else {
                                $("#divMsg").show();
                                $("#spanMsg").show();
                                $("#spanMsg").html("Details Saved Successfully.");
                                $("#divMsgPanel").removeClass("alert-danger");
                                $("#divMsgPanel").addClass("alert-success");
                                $("#btnSave").attr("disabled", true);
                                setTimeout(function () {
                                    $("#ModalAttendanceMapping").modal("hide");
                                    $("#btnSave").attr("disabled", false);
                                }, 3000);

                                BindFirstGrid();
                            }
                        },
                        failure: function (data) {
                            alert(data);
                        },
                        error: function (error) {

                        }
                    });
                }
            });

            debugger;
            $("#btnUploadLeaveDetails").kendoTooltip({
                content: function (e) {
                    return "Bulk Upload";
                }
            });

            //code for search filter in kendo [txtSearch] is textbox ID and grid is kendoGrid ID
            $("#txtSearch").on('input', function (e) {
                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;
                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field == "Leave_Description" || x.field == "LeaveType" || x.field == "ClientCode") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
            });
        });

        function OpenUploadBulkLeaveDetailsPopup() {
            $('#divLeaveDetailsUploadDialogBulk').show();
            var clientID = $("#ddlClientList").data("kendoDropDownList").value();
            var myWindowAdv = $("#divLeaveDetailsUploadDialogBulk");

            myWindowAdv.kendoWindow({
                width: "55%",
                height: '35%',

                content: "../RLCS/Avacom_LeaveMappingBulkUpload.aspx?ClientID=" + clientID,
                iframe: true,
                title: "Upload",
                visible: false,
                actions: [
                    "Close"
                ],
                close: onClose,
                open: onOpen
            });

            myWindowAdv.data("kendoWindow").center().open();
            return false;
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, false);
        }

        function onClose() {
            $("#txtSearch").val('');
             BindFirstGrid();
        }

        function BindFirstGrid() {

            debugger;
            //Empty grid
            var grid = $('#grid').data("kendoGrid");
            //if (grid != undefined || grid != null)
                $('#grid').empty();


            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<%=Path%>GetAvacom_LeaveTypeGridData?clientID=' + ddlClientValue,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    batch: true,
                    schema: {
                        data: function (response) {
                            if (response != null || response != undefined)
                                return response.Result;
                        },
                        total: function (response) {
                            if (response != null || response != undefined)
                                return response.Result.length;
                        }
                    }
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: false,
                    buttonCount: 3,

                    pageSizes: true,
                    pageSize: 10

                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        field: "LeaveType", title: 'Avacom Leave Type',
                        width: "35%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "Leave_Description", title: 'Description',
                        width: "35%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "ClientCode", title: 'Client Code',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        hidden: true, field: "ID", menu: false
                    },
                       {
                           command:
                               [
                                   { name: "edit", text: "", imageClass: "k-i-edit", iconClass: "k-icon k-i-edit", className: "ob-download" },

                               ],
                           title: "Action", lock: true, width: "10%;"
                       }
                ]
            });
            //Code for tooltip in grid Data
            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            //Code for tooltip in Kendo coloum header
            //$("#grid").kendoTooltip({
            //    filter: "th", //this filter selects the second column's cells
            //    position: "down",
            //    content: function (e) {
            //        var content = e.target.context.textContent;
            //        if (content != "") {
            //            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
            //        }
            //        else
            //            e.preventDefault();
            //    }
            //}).data("kendoTooltip");

            //code for tooltip in kendo action 
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View";
                }
            });
            $("#ClearfilterMain").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Clear Filter";
                }
            });
        }

        //Action button edit code in kendo grid
        $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {
            $("#divMsg").hide();
            $("#spanMsg").html("");
            $('input[type=text]').val('');
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));


            var AVLeaveType = item.LeaveType;
            AVLeave = AVLeaveType;
            $("#txtAvacom_LeaveType").val(AVLeaveType);
            $("#ModalAttendanceMapping").modal("show");



            return false;
        });

        function Bind_Clients() {
            $("#ddlClientList").kendoDropDownList({

                autoWidth: true,
                dataTextField: "Name",
                //filter: "contains", //startswith //endswith//contains
                dataValueField: "ID",
                index: 0,
                optionLabel: "Select Client",
                change: function (e) {
                    debugger;
                    if ($("#ddlClientList").val() != '' && $("#ddlClientList").val() != null && $("#ddlClientList").val() != undefined) {
                        ddlClientValue = $("#ddlClientList").val();

                    }
                    var ddltext = $("#ddlClientList").data("kendoDropDownList").text();
                    if (ddltext == "Select Client") {
                        ddlClientValue = "";
                        $('#txtSearch').css('visibility', 'hidden');
                        $('#ClearfilterMain').hide();
                        $('#btnUploadLeaveDetails').hide();
                        $("#grid").data("kendoGrid").dataSource.data([]);
                    }
                    else {
                        $('#txtSearch').css('visibility', 'visible');
                        $('#btnUploadLeaveDetails').show();
                        $('#ClearfilterMain').show();
                        BindFirstGrid();
                    }
                    

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<%=Path%>GetClientListData?CustomerID=<%=CustId%>',

                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            ddlClientValue = response.Result[0].ID;
                            return response.Result;
                        }
                    }
                }
            });
           }
           function ClearAllFilterMain(e) {

               e.preventDefault();
               $("#ddlClientList").data("kendoDropDownList").value("Select Client");
               $("#txtSearch").val('');
               $("#grid").data("kendoGrid").dataSource.data([]);
               $('#btnUploadLeaveDetails').hide();
               $('#ClearfilterMain').css('display', 'none');
           }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>

                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <%--<div id="divinline" style="margin-top: 10px;">
        <asp:DropDownList runat="server" ID="ddlClientList" OnSelectedIndexChanged="ddlClientList_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 35px; width: 390px;"
            CssClass="form-control" AutoPostBack="true" NoResultsText="No results match."
            AllowSingleDeselect="true" />
    </div>--%>
    <div id="divBranches1" style="width: 20%; float: left; margin-top: 5px;">
        <input id="ddlClientList" style="width: 94%; margin-right: 0.2%;" />
    </div>
    <div id="searchtype" runat="server" style="display: inline-grid; padding-left: 50px; margin-top: 5px;">
        <input id='txtSearch' type="text" class='k-textbox' placeholder="Type to Search" style="width: 140%; height: 36px" onkeydown="return (event.keyCode!=13);" autocomplete="off" />
    </div>
    <div style="display: inline-grid; padding-left: 115px; margin-top: 5px;">
        <button id="ClearfilterMain" class="btn btn-primary" style="height: 33px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span><font size="2" style="zoom: 1.1">Clear Filter</font></button>
    </div>
    <div id="bulkUploadBtn" runat="server" style="display: inline-grid; padding-left: 100px; margin-top: 5px;">
        <button id="btnUploadLeaveDetails" class="btn btn-primary" onclick="return OpenUploadBulkLeaveDetailsPopup()"><span class="k-icon k-i-upload"></span></button>
    </div>


    <%--For Kendo Grid--%>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <div id="grid">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div class="modal  modal-fullscreen" id="ModalAttendanceMapping" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" id="divValidation" style="height: 240px; width: 710px; margin-left: -100px; overflow-y: auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Attendance Mapping</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" id="divMsg" style="margin-bottom: -18px;">
                            <div class="col-md-12">
                                <div id="divMsgPanel" class="alert" style="width: 619px; height: 40px; border-radius: 7px; padding-top: 8px; margin-left: 0px;">
                                    <strong></strong><span id="spanMsg" style="font-size: 16px;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Avacom LeaveType<span style="color: red; margin-top: 9px;"></span></label>
                                <input type="text" id="txtAvacom_LeaveType" style="margin-top: 10px;" class="form-control" readonly="true " />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black; margin-top: 0px;">Client Code<span style="color: red;">*</span></label>
                                <input type="text" id="txtClientCode" class="form-control" autocomplete="off" style="margin-top: 10px;" />
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <button id="btnSave" type="button" value="Submit" style="margin-left: 288px; margin-top: 15px;" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="divLeaveDetailsUploadDialogBulk">
        <iframe id="iframeUploadBulk" style="width: 0px; height: 0px; border: none"></iframe>
    </div>



</asp:Content>
