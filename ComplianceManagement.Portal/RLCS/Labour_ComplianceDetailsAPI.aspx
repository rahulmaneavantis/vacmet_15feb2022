﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Labour_ComplianceDetailsAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.Labour_ComplianceDetailsAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <script src="../Scripts/KendoPage/Labour_ComplianceDetail.js"></script>
    <title></title>
   <style type="text/css">
       .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }
        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }
           input[type=checkbox], input[type=radio] {
    margin: 4px 4px 0;
    line-height: normal;
}

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: -4px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }
 
        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }
        
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus
        {
           margin-left: -18px;
           cursor: pointer;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
        var perorrev = "";
        var record = 0;
        var total = 0;
        function BindGrid() {
            var grid = $("#grid").data("kendoGrid");
            if (grid != undefined || grid != null) {
                $("#grid").empty();
            }
            if ($("#dropdownlistUserRole").val() == "3") {

                perorrev = "Performer";

            }
            else if ($("#dropdownlistUserRole").val() == "4") {
                perorrev = "Reviewer";
            }
            else {
                perorrev = "Performer/Reviewer";
            }

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/Labour_DashboradComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&bid=<% =BID%>&catid=<% =catid%>&StatusFlag=<% =StatusFlagID%>&IsApprover=<% =isapprover%>&Isdept=<% =IsDeptHead%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }                        
                    },
                    schema: {
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Statustory;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].DeptStatustory;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 3) {
                                return response[0].DeptInternal;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Statustory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].DeptStatustory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 3) {
                                return response[0].DeptInternal.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All',5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = 0;
                    total = this.dataSource._total;
                 
                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                        this.dataSource.pageSize(10)
                    }
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
               
                },
                columnMenu: true,
                filterMenuInit: function (e) {
                    if (e.field == "Name") {
                        e.container.css({ "max-width": "60%", });
                        e.container.find(".k-multicheck-wrap").css("overflow-x", "auto");
                    } else if (e.field == "ShortDescription") {
                        e.container.css({ "max-width": "60%", });
                        e.container.find(".k-multicheck-wrap").css("overflow-x", "auto");
                    } else if (e.field == "Section") {
                        e.container.css({ "max-width": "60%", });
                        e.container.find(".k-multicheck-wrap").css("overflow-x", "auto");
                    }
                },
                columns: [
                    {
                        field: "Branch", title: 'Location',
                        width: "27%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    <% if (StatusFlagID == -1)
        {%>   {
                        field: "Name", title: 'Act',
                        width: "24%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "Section", title: 'Section',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    <%}%>
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "34%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Frequency", title: 'Frequency',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     { hidden: true, field: "InternalComplianceTypeName", title: "Internal Type", filterable: { multi: true, search: true }, width: "10%" },
                   
                    {
                        field: "User", title: perorrev,
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]
            });
            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            //window.parent.forchild($("body").height()+50);
        }

        $(document).ready(function () {
            //window.parent.forchild($("body").height());
            BindGrid();

            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterGrid();
                },
                index: 0,
                dataSource: [
                    { text: "Role", value: "-1" },
                    { text: "Performer", value: "3" },
                    { text: "Reviewer", value: "4" }
                ]
            });
             <% if (StatusFlagID == -1)
        {%>  
            $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function')
                },
                dataSource: {
                    severFiltering: true,

                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>"                       
                    }
                }
            });
            <%}
        else
        {%> 
            $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function')
                },
                dataSource: {
                    severFiltering: true,

                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>"                       
                    }
                }
            });
            <%}%>
            //
            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownACT', 'filterAct', 'act')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {                
<%--                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',--%>
                             url: '<% =Path%>Data/BindComplianceWiseActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=HMGR',
                             dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
                    }
                } ,
                dataBound: function (e) {
                    e.sender.list.width("557");
                }
            });
               
            $("#dropdownFrequency").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Name",
                optionLabel: "Select Frequency",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownFrequency', 'filterCompType', 'frequency')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindFrequency',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindFrequency"
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("200");
                }
            });
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
               // filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    //window.parent.forchild($("body").height()+50);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }                        
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            //$(document).ready(function () {
            //    ComplianceType();
            //    if ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) {
            //        if ($("#dropdownType").val() == "S") {
            //            $('#dvdropdowntype').css('display', 'none');
            //        }
            //        else {
            //            $('#dvdropdowntype').css('display', 'block');
            //        }
            //    }
            //    else {
            //        $('#dvdropdowntype').css('display', 'block');
            //    }
            //    BindGrid();

            //});

            //function ClearAllFilterMain(e) {
            //    alert(1);
            //    debugger;
            //    $("#dropdowntree").data("kendoDropDownTree").value([]);
            //    $("#dropdownlistUserRole").data("kendoDropDownTree").value([]);
            //    $('#ClearfilterMain').css('display', 'none');
            //    $("#grid").data("kendoGrid").dataSource.filter({});
            //    e.preventDefault();
            //}
        });
        //function ClearAllFilterMain(e) {
        //        $("#dropdowntree").data("kendoDropDownTree").value([]);
        //        $("#dropdownlistUserRole").data("kendoDropDownTree").value([]);
        //        $('#ClearfilterMain').css('display', 'none');
        //        $("#grid").data("kendoGrid").dataSource.filter({});
        //        e.preventDefault();
        //    }
        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            //$("#dropdownlistUserRole").data("kendoDropDownList").value([]);
            $("#dropdownfunction").data("kendoDropDownList").select(0);
  if ($("#dropdownACT").val() != undefined || $("#dropdownACT").val()!= null) {
            $("#dropdownACT").data("kendoDropDownList").select(0);
}
  if ($("#dropdownFrequency").val() != undefined || $("#dropdownFrequency").val()!= null) {
            $("#dropdownFrequency").data("kendoDropDownList").select(0);
}
          //  $('#ClearfilterMain').css('display', 'block');
            $("#grid").data("kendoGrid").dataSource.filter({});
          // BindGrid();
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
            // window.parent.forchild($("body").height() + 100);
            e.preventDefault();
        }
        function FilterGrid() {



            var IsDept = document.getElementById('IsDeptId').value;

            var IsSI = document.getElementById('IsSIId').value;
            //alert(IsDept);

            //location details
            //var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            //var locationsdetails = [];
            //$.each(list1, function (i, v) {
            //    locationsdetails.push({
            //        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
            //    });
            //});
            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }


            //Role Details
            var Roledetails = [];
            if ($("#dropdownlistUserRole").val() != "-1") {
                Roledetails.push({
                    field: "RoleID", operator: "eq", value: parseInt($("#dropdownlistUserRole").val())
                });
            }

            //Frequency Details
            var Frequencydetails = [];
            if ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined) {
                Frequencydetails.push({
                    field: "Frequency", operator: "eq", value: parseInt($("#dropdownFrequency").val())
                });
            }

            //Act Details
            var Actdetails = [];

           
            debugger;

            var dataSource = $("#grid").data("kendoGrid").dataSource;

            
            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locationsdetails.length > 0
                            || Roledetails.length > 0
                //|| Functiondetails.length > 0
                            || Actdetails.length > 0
                            || Frequencydetails.length > 0
                            || ($("#dropdownlistUserRole").val() != $("#dropdownlistUserRole").val() != "Role" && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != undefined)
                            || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                            || ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined)
                            || ($("#dropdownfunction").val() != undefined && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != "")
                            || ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined)
                            || datedetails.length > 0) {

                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) {
                    var TypeFilter = { logic: "or", filters: [] };
                    TypeFilter.filters.push({
                        field: "InternalComplianceTypeID", operator: "eq", value: $("#dropdownType").val()
                    });
                    finalSelectedfilter.filters.push(TypeFilter);
                }
                if ($("#dropdownlistUserRole").val() != undefined && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != "-1" && $("#dropdownlistUserRole").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "RoleID", operator: "eq", value: $("#dropdownlistUserRole").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }

                if ($("#dropdownfunction").val() != undefined && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != "-1" && $("#dropdownfunction").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };

                    if (IsDept == 1) {
                        if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
                            Functiondetails.push({
                                field: "DepartmentID", operator: "eq", value: parseInt($("#dropdownfunction").val())
                            });
                        }
                    }
                    else {
                        if (IsSI == 0) {
                            SeqFilter.filters.push({
                                field: "InternalComplianceCategoryID", operator: "eq", value: $("#dropdownfunction").val()
                            });
                        }
                        else {
                            SeqFilter.filters.push({
                                field: "ComplianceCategoryId", operator: "eq", value: $("#dropdownfunction").val()
                            });
                        }
                    }
                    finalSelectedfilter.filters.push(SeqFilter);
                }
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "-1" && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };

                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });

                    finalSelectedfilter.filters.push(ActFilter);
                }

                if ($("#dropdownFrequency").val() != undefined && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != "-1" && $("#dropdownFrequency").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "Frequency", operator: "eq", value: $("#dropdownFrequency").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }


                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                dataSource.pageSize(total);
            }
        }
        function exportReport(e) {
            e.preventDefault();
            debugger;
            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var CustomerName = document.getElementById('CustName').value;

            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Role Details
            var Roledetails = [];
            if ($("#dropdownlistUserRole").val() != "" && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != "-1" && $("#dropdownlistUserRole").val() != undefined) {
                Roledetails.push(parseInt($("#dropdownlistUserRole").val()));
            }

            //function details
            var functiondetails = [];
            if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != "-1" && $("#dropdownfunction").val() != undefined) {
                functiondetails.push(parseInt($("#dropdownfunction").val()));
            }

            //Act Details
            var Actdetails = [];
            if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "-1" && $("#dropdownACT").val() != undefined) {
                Actdetails.push(parseInt($("#dropdownACT").val()));
            }


            //Frequency Details
            var Frequencydetails = [];
            if ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != "-1" && $("#dropdownFrequency").val() != undefined) {
                Frequencydetails.push($("#dropdownFrequency").val());
            }
            var sflag =<% =StatusFlagID%>;
            var did =<% =IsDeptHead%>;
            var isapp =<% =isapprover%>;

            $.ajax({
                type: "GET",
                url: '' + PathName + '//ExportReport/ReportMgmt',
                data: {
                    StatusFlag: sflag, Isdept: did, isapprover: isapp, CName: CustomerName, UserId: UId, CustomerID: customerId,
                    location: JSON.stringify(locationsdetails),

                    Role: JSON.stringify(Roledetails), Function: JSON.stringify(functiondetails),
                    actDetail: JSON.stringify(Actdetails),
                    FrequencyDetail: JSON.stringify(Frequencydetails),
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }
    </script>
</head>
<body style="overflow-x: hidden;">
    <form>
        <div>
            <h1 id="pagetype" style="height: 30px; background-color: #f8f8f8; margin-top: 0px; color: #666; margin-bottom: 12px; font-size: 19px; padding-left: 5px; padding-top: 5px; font-weight: bold;">Compliances</h1>
        </div>

        <div id="example">
            <div class="row" style="padding-bottom: 10px;">
                <div class="toolbar">
                    <div class="row">
                        <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 270px;margin-right: 10px;" />
                        <input id="dropdownlistUserRole" data-placeholder="Role" style="width: 190px;margin-right: 10px;" />
                        <%--<input id="dropdownType" style="width: 15%; margin-right: 0.8%;" />--%>
                        <% if (StatusFlagID == 0)
                        {%>
                        <input id="dropdownType" style="width: 15%;margin-right: 10px;" />
                        <%}%>
                        <input id="dropdownfunction" style="width: 223px;margin-right: 10px;" />
                        <% if (StatusFlagID == -1)
                        {%>
                        <input id="dropdownACT" style="width: 304px;margin-right: 10px;" />
                        <%}%>
                        <input id="IsDeptId" type="hidden" value="<% =IsDeptHead%>" />
                        <input id="IsSIId" type="hidden" value="<% =StatusFlagID%>" />
                        <input id="dropdownFrequency" style="width: 200px;" />
                        <button id="export" onclick="exportReport(event)" style="height: 30px;margin-left: 1093px;margin-top: 4px;" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                        <button id="ClearfilterMain" style="margin-left: 10px;height: 30px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-bottom: 7px; font-size: 12px; display: none; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterrole">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterstatus">&nbsp;</div>

            <div id="grid" style="margin-top: -4px;margin-bottom:10px;"></div>

            <input id="CustName" type="hidden" value="<% =CustomerName%>" />
            <input id="UId" type="hidden" value="<% =UId%>" />
            <input id="Path" type="hidden" value="<% =Path%>" />
            <input id="CustomerId" type="hidden" value="<% =CustId%>" />


        </div>
        <script type="text/javascript">
            function fhead(Compliances) {
                $('#pagetype').html(val);
                //  $('#sppagetype').html(val)   
            }
        </script>
    </form>
</body>
</html>
