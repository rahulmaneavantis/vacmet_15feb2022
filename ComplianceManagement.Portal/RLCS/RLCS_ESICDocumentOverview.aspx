﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_ESICDocumentOverview.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_ESICDocumentOverview" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <div class="row colpadding0">
            <div class="col-md-1 colpadding0">
                <table width="100%">
                    <thead>
                        <tr>
                            <td valign="top">
                                <%--<asp:Repeater runat="server" ID="Repeater1" OnItemCommand="Repeater1_ItemCommand" OnItemDataBound="Repeater1_ItemDataBound">--%>
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                            <td>
                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("Name")%>'
                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Name")%>'
                                                    Text='<%# Eval("Name").ToString().Substring(0,6) +"..." %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>

                    </thead>
                </table>
            </div>
            <div class="col-md-11">
                <tr> <asp:Label ID="lblDocuments" style="color:red" runat="server"></asp:Label></tr>
                <GleamTech:DocumentViewer runat="server" Width="100%" ID="DocumentViewer1" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="500px"
                    DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" />

            </div>
        </div>
        </div>
    </form>
</body>
</html>
