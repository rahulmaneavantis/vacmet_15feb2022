using System;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Linq;
using System.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_DesignationUpdate : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            string designation = Request.QueryString["des"];

            if (!IsPostBack)
            {
                tbxDesignation.Text = designation;
            }

        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string inputStatus = ddlStatus.Text;
            string inputDesignation = tbxDesignation.Text;
            string gridStatus = "1";
            string gridDesignation = Request.QueryString["des"];
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.Data.RLCS_Designation_Master
                record = entities.RLCS_Designation_Master.SingleOrDefault(a => a.ID == Id);
                var existingDesignationCount = entities.RLCS_Designation_Master.Count(a => a.Long_designation == inputDesignation);
                if (inputStatus == gridStatus && inputDesignation == gridDesignation)
                {
                    submit.IsValid = false;
                    btn_Update.Enabled = false;
                    record.UpdatedOn = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                }
                else if (inputStatus == "0" && inputDesignation == gridDesignation)
                {
                    submit.IsValid = false;
                    btn_Update.Enabled = false;
                    record.Status = "I";
                    record.UpdatedOn = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                }
                else if (inputStatus == "1" && inputDesignation != gridDesignation && existingDesignationCount == 0)
                {
                    submit.IsValid = false;
                    btn_Update.Enabled = false;
                    record.Long_designation = inputDesignation;
                    record.UpdatedOn = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                }
                else if (inputStatus == "0" && inputDesignation != gridDesignation && existingDesignationCount == 0)
                {
                    submit.IsValid = false;
                    btn_Update.Enabled = false;
                    record.Long_designation = inputDesignation;
                    record.Status = "I";
                    record.UpdatedOn = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
                } else
                {
                    designation_Exist.IsValid = false;
                }
                entities.SaveChanges();
            };
        }

        protected void btn_Cancle_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);
        }
    }
}