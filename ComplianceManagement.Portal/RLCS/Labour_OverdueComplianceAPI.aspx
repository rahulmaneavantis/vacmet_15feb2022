﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Labour_OverdueComplianceAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.Labour_OverdueComplianceAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white; margin-right: -18px">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script src="../Scripts/KendoPage/Labour_OverdueCompliance.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>


    <style type="text/css">
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }
        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 2px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
         input[type=checkbox], input[type=radio] {
    margin: 4px 5px 0px;
    margin-top: 1px\9;
    line-height: normal;
       }

         .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }
    </style>

    <script type="text/javascript">
        var record = 0;
        var total = 0;
        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                },

                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/Labour_GetMGMTOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=0&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val() + '&Isdept=0&IsApprover=<% =isapprover%>&RiskId=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                         
                    },
                    schema: {
                        model: {
                            fields: {
                                ComplianceID: { type: "string", },
                              //\\  InternalComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" }
                            }
                        },
                        data: function (response) {
                            return response[0].SList;
                        },
                        total: function (response) {
                            return response[0].SList.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                dataBound: function (e) {
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                        this.dataSource.pageSize(10)
                    }
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                pageable: {
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                    { hidden: true, field: "ShortForm", title: "Short&nbsp;Form", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Branch", title: "Location", filterable: { multi: true, search: true }, width: "10%" },
                    {
                        field: "ActName", title: 'Act&nbsp;Name',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "16%"
                    },
             { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, search: true }, width: "10%" },

                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Performer", title: 'Performer',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        //    field: "ScheduledOn", title: 'Due&nbsp;Date',
                        //    type: "date",    
                        //    format: "{0:dd-MM-yyyy}",

                        //},    
                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "16%"
                    },

                    {
                        field: "OverdueBy", title: 'Overdue By (Days)',
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "18%"
                    },
                    {
                        field: "ForMonth", title: 'Period',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "16%"
                    },
                    {
                        command: [
                            {
                                name: "edit2", text: "", title: "Action", iconClass: "k-icon k-i-eye", className: "ob-edit",
                            }
                        ], title: "Action", width: "7%;",
                        headerAttributes: {
                            style: "border-right: solid 1px #ceced2;text-align:center;"
                        }
                    }
                ]
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td[role=gridcell]", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "th[data-role=droptarget]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=7]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=8]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=9]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=10]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=11]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=12]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#grid').kendoTooltip({
                filter: "td[colspan=13]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });
            window.parent.forchild($("body").height() + 70);
        }

        function OpenOverViewpupMain(scheduledonid, instanceid) {
            $('#divApiOverView').modal('show');
            $('#APIOverView').attr('width', '1150px');
            $('#APIOverView').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#APIOverView').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            window.parent.forchild($("body").height() + 70);
        }

        function BindGridApply(e) {
            BindGrid();
            FilterGridStatutoryNew();
            e.preventDefault();
        }

        $(document).ready(function () {
            window.parent.forchild($("body").height() + 70);
            $("#Startdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy"
            });

            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy"
            });
            $("#txtSearchComplianceID").on('input', function (e) {
                FilterGridStatutoryNew();
            });

            BindGrid();

            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY").val() != "0") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" }
                ]
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGridStatutoryNew();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });
            //$("#dropdownlistRisk").data("kendoDropDownTree").value('0');
            $("#ClearfilterMain").show();

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterGridStatutoryNew();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        
                    }
                }
            });

            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    FilterGridStatutory();
                    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>"
                    }
                }
            });


            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGridStatutoryNew();
                    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceWiseActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=HMGR',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }                        
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("775");
                }
            });

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    FilterGridStatutoryNew();
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                    window.parent.forchild($("body").height() + 15);

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=MGMT"
                    },
                }
            });

             $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
               // filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGridStatutoryNew();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    //window.parent.forchild($("body").height()+50);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }                        
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });


        });
        function FilterGridStatutoryNew() {
            debugger
            var locationsdetails = [];
            //location details
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }

            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            //user Details
            var userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }
            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                });
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locationsdetails.length > 0
               || Riskdetails.length > 0
                || $("#txtSearchComplianceID").val() != ""
               || ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined)
               || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                  || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
               || datedetails.length > 0

                || userdetails.length > 0) {

                if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {

                    var SequenceFilter = { logic: "or", filters: [] };
                    SequenceFilter.filters.push({
                        field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                    });
                    finalSelectedfilter.filters.push(SequenceFilter);
                }

                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }

                if (Riskdetails.length > 0) {
                    var RiskFilter = { logic: "or", filters: [] };

                    $.each(Riskdetails, function (i, v) {

                        RiskFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(RiskFilter);
                }

                if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
                    if ($("#dropdownfunction").val() != 0) {

                        var FunctionFilter = { logic: "or", filters: [] };

                        FunctionFilter.filters.push({
                            field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                        });

                        finalSelectedfilter.filters.push(FunctionFilter);
                    }
                }

                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }

                if (datedetails.length > 0) {
                    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        var DateFilter = { logic: "or", filters: [] };
                        DateFilter.filters.push({
                            field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                        });

                        finalSelectedfilter.filters.push(DateFilter);
                    }

                    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        var DateFilter = { logic: "or", filters: [] };
                        DateFilter.filters.push({
                            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                        });

                        finalSelectedfilter.filters.push(DateFilter);
                    }
                }

                if (userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };

                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }
                if ($("#txtSearchComplianceID").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                dataSource.pageSize(total);
            }
        }
        function ClearAllFilterMain1(e) {
            $("#txtSearchComplianceID").val('');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownfunction").data("kendoDropDownList").select(0);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
            <%{%>
            $("#dropdownSequence").data("kendoDropDownList").select(0);
            <%}%>
            $("#dropdownPastData").data("kendoDropDownList").select(4);
            $("#Startdatepicker").val('');
            $("#Lastdatepicker").val('');
            $("#grid").data("kendoGrid").dataSource.filter({});
            BindGrid();
            window.parent.forchild($("body").height() + 100);
            e.preventDefault();
        }

    </script>



    <title></title>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
</head>
<body style="overflow-x: hidden;">
    <form>

        <div style="width: 99%">
            <h1 style="height: 30px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; padding-top: 5px; padding-left: 5px; color: #666; margin-bottom: 12px;">Overdue Compliance</h1>
        </div>

        <div id="example">
            <div style="margin: 0.5% 0 0.5%;">
                <input id="CustName" type="hidden" value="<% =CustomerName%>" />
                <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
                <input id="dropdowntree" style="width: 25%; margin-right: 0.8%;" />
                <input id="dropdownFY" style="width: 17%; margin-right: 0.8%;" />
                <input id="dropdownlistRisk" style="width: 17%; margin-right: 0.8%;" />
                <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" style="width: 17.5%; margin-right: 0.8%;" />
                <input id="Lastdatepicker" placeholder="End Date" style="width: 17.5%;" />
            </div>

            <div style="margin: 0.5% 0 0.5%;">
                <input id="dropdownACT" style="width: 25%; margin-right: 0.8%;" />
                <input id="dropdownPastData" style="width: 17%; margin-right: 0.8%;" />
                <input id="dropdownfunction" style="width: 17%; margin-right: 0.8%;" />
                <input id="dropdownUser" style="width: 17.5%; margin-right: 0.8%;" />
                 <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 17.4%;margin-right:10px" />

             </div>
            <div style="margin: 0.5% 0 0.5%;">

                <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
                <%{%>
                <input id="dropdownSequence" style="width: 25%; margin-right: 0.8%;" />
                <%}%>
                     <button id="export" onclick="exportReport(event)" style="height: 30px;float:right;margin-left:28px;margin-right:18px"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button id="Applyfilter" style="height: 30px; margin-left: 8px;float:right;margin-right:-19px;margin-left:1px;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button id="ClearfilterMain" style="display: none; float: right;float: right;height: 30px;margin-right: 10px;" onclick="ClearAllFilterMain1(event)"><span class="k-icon k-i-filter-clear"></span>Clear</button>
          
             
            </div>

            <div class="row" style="clear: both; height: 15px;"></div>
            <div class="row" style="padding-bottom: 15px; font-size: 12px; display: none; font-weight: bold; padding-top: 3px; margin-top: -15px;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; padding-top: 0px; margin-top: -11px; font-weight: bold;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-top: 5px;" id="filterUser">&nbsp;</div>

            <div id="grid" style="width: 99%; margin-bottom: 10px"></div>

        </div>
        <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1220px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

