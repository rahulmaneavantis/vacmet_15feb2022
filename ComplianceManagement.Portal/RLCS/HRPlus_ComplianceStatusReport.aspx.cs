﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using paytm;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_ComplianceStatusReport : System.Web.UI.Page
    {
        protected static string AVACOM_RLCS_API_URL;

        protected static int customerID;
        protected static int loggedInUserID;
        protected static string loggedUserRole;
        protected static string loggedUserProfileID;

        protected static int distID = -1;
        protected static int spID = -1;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string custWise = Request.QueryString["CustWise"];
            if (!string.IsNullOrEmpty(custWise))            
                this.MasterPageFile = "~/HRPlusCompliance.Master";                          
            else
                this.MasterPageFile = "~/HRPlusSPOCMGR.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AVACOM_RLCS_API_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            loggedInUserID = AuthenticationHelper.UserID;
            loggedUserRole = AuthenticationHelper.Role;
            loggedUserProfileID = AuthenticationHelper.ProfileID;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            if (AuthenticationHelper.Role == "SPADM")
            {
                spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            if (!IsPostBack)
            {
                try
                {

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }

        }
    }
}