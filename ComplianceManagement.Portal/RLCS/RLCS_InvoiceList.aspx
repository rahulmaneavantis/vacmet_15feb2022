﻿<%@ Page Title="My Invoice(s)" Language="C#" AutoEventWireup="true" CodeBehind="RLCS_InvoiceList.aspx.cs" MasterPageFile="~/RLCSCompliance.Master" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_InvoiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <%--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    <style>
           .loadingfile {
            position: absolute;
            top: 200px;
            left: 533px;
            z-index: 2;
            width: 100%;
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.loadingfile').hide();
            setactivemenu('leftnoticesmenu');
            fhead('My Invoices');
            ApiTrack_Activity("My Invoices","pageView",null);
        });

        function BindDDLMonth() {
            $("#ddlIlstforMonth").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                width: 80,
                //optionLabel: "Select Month",
                index: 0,
                change: function (e) {
                    debugger;
                    //var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    if (values != "" && values != null) {
                        bindGridval($("#ddlIlstforClient").val(), $("#ddlIlstfortotal").val(), values, $("#ddlIlstforYear").val());
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                },
                dataSource: [
                    { text: "January", value: "1" },
                    { text: "February", value: "2" },
                    { text: "March", value: "3" },
                    { text: "April", value: "4" },
                    { text: "May", value: "5" },
                    { text: "June", value: "6" },
                    { text: "July", value: "7" },
                    { text: "August", value: "8" },
                    { text: "September", value: "9" },
                    { text: "October", value: "10" },
                    { text: "November", value: "11" },
                    { text: "December", value: "12" },
                ],
            });
        }

        function BindDDLYear() {
            $("#ddlIlstforYear").width(150).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                index: 0,
               
                //optionLabel: {
                //    text: "Select Year",
                //    value: 0
                //},

                change: function (e) {
                    debugger;
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();

                    if (values != "" && values != null) {
                        //bindGridval($("#ddlIlstforClient").val(), 0, 0, values);
                        bindGridval($("#ddlIlstforClient").val(), $("#ddlIlstfortotal").val(), $("#ddlIlstforMonth").val(), values);
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                },
                dataSource: [
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                ],
            });
            
            //$("#ddlIlstforYear").data("kendoDropDownList").list.width(10);
        }

        function BindInvoiceType() {
            $("#ddlIlstfortotal").width(170).kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                //optionLabel: {
                //    text: "Select Type",
                //    value: 0
                //},
                index: 0,
                change: function (e) {
                    debugger;
                    var values = this.value();

                    if (values != "" && values != null) {
                        //bindGridval($("#ddlIlstforClient").val(), values, 0, 0);
                        bindGridval($("#ddlIlstforClient").val(), values, $("#ddlIlstforMonth").val(), $("#ddlIlstforYear").val());
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                },
                dataSource: [
                    { text: "Total", value: "T" },
                    { text: "Current", value: "C" },
                    { text: "Outstanding", value: "O" },
                ],
            });

        }

        function BindEntities() {
            $("#ddlIlstforClient").width(350).kendoDropDownList({
                dataTextField: "AVACOM_BranchName",
                dataValueField: "CM_ClientID",
                
                //optionLabel: "Select Entity",
                change: function (e) {
                    var values = this.value();
                    if (values != "" && values != null) {
                        //bindGridval(values, 0, 0, 0);
                        bindGridval(values, $("#ddlIlstfortotal").val(), $("#ddlIlstforMonth").val(), $("#ddlIlstforYear").val());
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                },
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =avacomAPIUrl%>GetAssignedEntities?customerID=<% =CId%>&userID=<% =UId%>&profileID=<% =ProfileID%>",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            //if (response.Result != null && response.Result != undefined) {
                            //    bindGridval(response.Result[0].CM_ClientID, $("#ddlIlstfortotal").val(), $("#ddlIlstforMonth").val(), $("#ddlIlstforYear").val());
                            //}
                            return response.Result;
                        }
                    }
                }
            });


        }


        $(document).on("click", "#grid tbody tr .ob-download", function (e) {
            $('.loadingfile').show();
            var client = $('#ddlIlstforClient').val();
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var path = '';
            var month = getMonthFromString(item.MonthName);
            var year = item.YearName;
            var invoiceno = item.RIM_InvoiceNo;
            debugger;
            if (client != undefined && month != undefined && year != undefined && invoiceno != undefined)
            downloadMIS(client, month,year,invoiceno);
            downloadInvoice(client, month, year, invoiceno);
                      /// return true;
        });
        function getMonthFromString(mon) {
            if(mon !=undefined)
            return new Date(Date.parse(mon + " 1, 2012")).getMonth() + 1
        }
        function downloadMIS(client, month, year, invoiceno) {
            var path = '<%= commonPath%>' + client + '&Month='+month+'&Year='+year+'&InvoiceNumber='+invoiceno
            //$('#dwnfile').attr('src', path);
            $('.loadingfile').show();
            $.ajax({
                url: path,
                dataType: "jsonp",
                timeout: 5000,

                success: function (response) {
                    $('.loadingfile').hide();
                },
                error: function (parsedjson) {
                    if (parsedjson != null && parsedjson.statusText != "success") {
                        $('.loadingfile').hide();
                        alert("No Data found for this selection");
                        //$('#fileNot').modal('show');
                        setTimeout(function () { $('#fileNot').modal('hide'); }, 3000);
                    } else {
                        debugger;
                        $('#dwnfile').attr('src', path);
                        $('.loadingfile').hide();
                    }
                }
            });
        }
       
        //$(document).ready(function () {

        function bindGridval(ClientID, Total, Month, Year) {

            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                //dataSource: datagrid,
                dataSource: {
                    //  type: "odata",
                    transport: {
                        read: {
                            <%--url: "<% =TLConnectAPIUrl%>Dashboard/GetInvoiceDetailsClick?ClientId=" + ClientID + "&details=" + Total + "&month=" + Month + "&year=" + Year + "&UserID=",--%>
                            url: "<% =TLConnectAPIUrl%>Dashboard/GetInvoiceDetailsClick?ClientId=" + ClientID + "&details=" + Total + "&month=&year=&UserID=",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', '5CB407DE-AC17-4C5E-B7D9-CA97BC');
                                xhr.setRequestHeader('X-User-Id-1', 'E43SC3fopmDfizK5kv2chQ==');
                            },
                            dataType: "json"
                        }
                    },
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        },
                    }
                },

                //toolbar: kendo.template($("#template").html()),
                height: 380,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    refresh: true,
                    //pageSizes: true,
                    buttonCount: 3
                },
                columns: [
                     {
                         title: "Sr.No",
                         field: "rowNumber",
                         template: "<span class='row-number'></span>",
                         width: 70
                     },
                    {
                        field: "ClientID", title: 'Entity',
                        //width: 130,
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "MonthName", title: 'Month',
                        width: 135,
                        //attributes: {
                        //    style: 'white-space: nowrap;'
                        //},
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "YearName", title: 'Year',
                        // type: "date",
                        //  width: 120,
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "RIM_InvoiceNo", title: 'Invoice No',
                        filterable: {
                            extra: false,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "RIM_Invoice_Date", title: 'Invoice Date',
                        //   width: 130,
                        //attributes: {
                        //    style: 'white-space: nowrap '
                        //},
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
            {
                field: "RIM_Invoice_Amount", title: 'Amount',
                //   width: 130,
                //attributes: {
                //    style: 'white-space: nowrap '
                //},
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains"
                        }
                    }
                }
            },
              {
                  command: [

                      { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },

                  ], title: "Action", lock: true, width: "12.7%;"
              }
                ], dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                        + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
        }

        $(document).ready(function () {

            //BindDDLMonth();
           // BindDDLYear();

            BindEntities();
            BindInvoiceType();

            bindGridval($("#ddlIlstforClient").val(), "T", $("#ddlIlstforMonth").val(), $("#ddlIlstforYear").val());
        });

        //MonthdropDown
        function fCreateStoryBoard(Id, div, filtername) {           
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            // debugger;
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            $('#Clearfilter').css('display', 'none');

            if (div == 'filterlstforclient') {
                $('#' + div).append('Client&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                //  $('#' + div).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;white-space: nowrap;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }
            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                // debugger;
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            }
        }
        //});
    </script>

     <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="loadingfile">
        <img src="../images/processing.gif" />
    </div>

    <iframe src="about:blank" id="dwnfile" style="display: none;"></iframe>
    <div class="row colpadding0">
        <div class="col-md-4" style="padding-left: 0">
            <input id="ddlIlstforClient" style="width: 100%" />
        </div>
        <div class="col-md-2">
            <input id="ddlIlstfortotal" style="width: 100%" />
        </div>
        <div class="col-md-2" style="display: none;">
            <input id="ddlIlstforMonth" style="width: 100%" />
        </div>
        <div class="col-md-2" style="padding-right: 0; display: none;">
            <input id="ddlIlstforYear" style="width: 100%" />
        </div>
    </div>
    
   <%-- <div style="display: none">
        <iframe id="DownloadViews" src="about:blank" width="100px" height="150px" frameborder="0"></iframe>
    </div>--%>
    <div id="grid" style="border: none; margin-top: 5px"></div>
</asp:Content>
