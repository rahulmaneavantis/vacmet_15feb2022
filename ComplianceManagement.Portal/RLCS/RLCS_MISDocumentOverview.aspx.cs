﻿using GleamTech.DocumentUltimate.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_MISDocumentOverview : System.Web.UI.Page
	{
		private HttpClient _httpClient { set; get; }
		HttpResponseMessage response;
		public RLCS_MISDocumentOverview()
		{
			_httpClient = new HttpClient();
			_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL_Portal"]);
			_httpClient.Timeout = new TimeSpan(0, 15, 0);
			_httpClient.DefaultRequestHeaders.Accept.Clear();
			_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			_httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
			_httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
			_httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(Request.QueryString["DocumentPath"]))
			{
				try
				{
					string MISPath = Request.QueryString["DocumentPath"].ToString();
					string baseAddress = ConfigurationManager.AppSettings["RLCS_MIS_Path"].ToString();
					string filePath = baseAddress + MISPath;
					filePath = filePath.Replace("|", "'");
					//string filePath = "C:\\Temp\\ManualPath\\MIS\\AVATRANS\\08_2021\\Tamil Nadu\\Chennai_TIPL_EKA_0002\\GreyTipDocumentation.xls";
					string Folder = "~/TempFiles/RLCS";
					string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
					string DateFolder = Folder + "/" + File;
					string extension = System.IO.Path.GetExtension(filePath);
					if (extension != ".zip")
					{
						if (filePath != "")
						{
							response = _httpClient.GetAsync("Masters/Generate?FilePath=" + filePath).Result;
							var statuscode = Convert.ToInt32(response.StatusCode);
							if (statuscode == 200)
							{
								var byteFile = response.Content.ReadAsByteArrayAsync().Result;
								if (byteFile.Length > 0)
								{
									if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
									{
										DocumentInfo _info = new DocumentInfo("MIS_Document" + extension);


										DocumentViewer1.DocumentSource = new DocumentSource(_info, byteFile);

										//DocumentViewer1.Document = ConfigurationManager.AppSettings["DriveUrl"] + "/" + Convert.ToString(filePath);
									}
									else
									{
										DocumentInfo _info = new DocumentInfo("MIS_Document" + extension);
										DocumentViewer1.DocumentSource = new DocumentSource(_info, byteFile);
										//DocumentViewer1.Document = Convert.ToString(filePath);
									}
								}
							}
							else
							{
								lblDocuments.Text = "Sorry, No document available to preview.";
								DocumentViewer1.Visible = false;
							}

						}
						else
						{
							lblDocuments.Text = "Sorry, No document available to preview.";
							DocumentViewer1.Visible = false;
						}

					}
					else
					{
						lblDocuments.Text = "Zip file cannot be viewed here. Please use the download option.";
						DocumentViewer1.Visible = false;
					}

				}
				catch (Exception ex)
				{ }
			}
			else
			{
				lblDocuments.Text = "Sorry, No document available to preview.";
				DocumentViewer1.Visible = false;
			}

		}
		protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{

		}
		protected void Repeater1_ItemCommand(object sender, RepeaterItemEventArgs e)
		{

		}
	}
}