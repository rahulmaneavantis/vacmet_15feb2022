﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_MyPayments : System.Web.UI.Page
    {
        protected static int custID = -1;
        protected static string avacomRLCSAPIURL;

        protected static int customerID;
        protected static int loggedInUserID;
        protected static string loggedUserRole;
        protected static string loggedUserProfileID;

        protected static int distID = -1;
        protected static int spID = -1;

        protected static string custName;

        protected void Page_Load(object sender, EventArgs e)
        {
            avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

            loggedInUserID = Convert.ToInt32(AuthenticationHelper.UserID);
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            loggedUserRole = AuthenticationHelper.Role;
            loggedUserProfileID = AuthenticationHelper.ProfileID;

            vsPayments.CssClass = "alert alert-danger";

            if (AuthenticationHelper.Role == "SPADM")
            {
                spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
            {
                custID = Convert.ToInt32(Request.QueryString["CustID"]);

                if (custID != Convert.ToInt32(AuthenticationHelper.CustomerID))
                    btnAddAmount.Visible = false;
                else
                    btnAddAmount.Visible = true;
            }
            else
                custID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            if (!IsPostBack)
            {
                custName = CustomerManagement.CustomerGetByIDName(custID);

                ShowBalance(custID);
                MainView.ActiveViewIndex = 0;
                lnkPayments_Click(sender, e);
            }
        }

        protected void btnAddAmount_Click(object sender, EventArgs e)
        {
            try
            {
                if (custID != 0 && custID != -1)
                    Response.Redirect("~/RLCS/HRPlus_PaymentCheckout.aspx?CustID=" + custID, false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnCheckTxnStatus_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustomerID.Value != null)
                {
                    int customerID = Convert.ToInt32(hdnCustomerID.Value);
                    string orderID = Convert.ToString(hdnOrderID.Value);
                    int loggedInUserID = AuthenticationHelper.UserID;

                    //Re-Check Transaction Status
                    PaymentManagement.GetTxnStatus_Paytm(customerID, orderID, loggedInUserID);

                    var paymentDetails = PaymentManagement.GetPaymentOrderDetailsByOrderID(orderID);

                    if (paymentDetails != null)
                    {
                        string resultMessage = string.Empty;
                        bool txnStatus = false;
                        if (paymentDetails.StatusID == 0)
                        {
                            resultMessage = orderID + "- Current Status is-" + paymentDetails.Status + ", Please check after some time"; //"Failed"
                        }
                        else if (paymentDetails.StatusID == 1)
                        {
                            resultMessage = orderID + "- Current Status is-" + paymentDetails.Status; //"SUCCESS"
                            txnStatus = true;
                        }
                        else if (paymentDetails.StatusID == 2)
                        {
                            resultMessage = orderID + "- Current Status is-" + paymentDetails.Status; //"Failed"
                        }
                        else if (paymentDetails.StatusID == 3)
                        {
                            resultMessage = orderID + "- Current Status is-" + paymentDetails.Status + ", Please check after some time"; //"Failed"
                        }
                        else
                        {
                            resultMessage = orderID + "- Current Status is-" + paymentDetails.Status + ", Please check after some time"; //"Failed"
                        }

                        lnkPayments_Click(sender, e);

                        cvPayments.IsValid = false;
                        cvPayments.ErrorMessage = resultMessage;

                        if (txnStatus)
                            vsPayments.CssClass = "alert alert-success";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void lnkPayments_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 0;

            liPayment.Attributes.Add("class", "active");
            liDeduction.Attributes.Add("class", "");
            liBusiness.Attributes.Add("class", "");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "showPayTab", "ShowPaymentTab();", true);
        }

        protected void lnkDeductions_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 1;

            liPayment.Attributes.Add("class", "");
            liDeduction.Attributes.Add("class", "active");
            liBusiness.Attributes.Add("class", "");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "showDeductTab", "ShowDeductionsTab();", true);
        }

        protected void lnkConsolidatedBusiness_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 2;

            liPayment.Attributes.Add("class", "");
            liDeduction.Attributes.Add("class", "");
            liBusiness.Attributes.Add("class", "active");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "showBusTab", "ShowBusinessTab();", true);
        }

        public void ShowBalance(int custID)
        {
            try
            {
                if (custID != 0 && custID != -1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var balanceRecord = entities.SP_RLCS_CheckBalance(custID).FirstOrDefault();

                        if (balanceRecord != null)
                            if (balanceRecord.Balance != null)
                            {
                                string amtBalance = string.Empty;

                                try
                                {
                                    decimal decBalance = Convert.ToDecimal(balanceRecord.Balance);
                                    amtBalance = decBalance.ToString("C", new CultureInfo("EN-in"));
                                }
                                catch(Exception ex)
                                {

                                }

                                lblBalance.Text = amtBalance;
                            }
                    }
                }        
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}