﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_EmployeeMaster : System.Web.UI.Page
    {
        protected static int CustId;
       
        protected static string Path;
        protected static string Path1;
        protected static string ProfileID;
        protected int UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            Path1 = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL1"];

            if (!string.IsNullOrEmpty(AuthenticationHelper.ProfileID))
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            else
                ProfileID = Convert.ToString(AuthenticationHelper.UserID);
        }
    }
}