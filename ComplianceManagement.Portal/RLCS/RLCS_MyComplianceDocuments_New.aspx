﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyComplianceDocuments_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyComplianceDocuments_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
	<link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

	<script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
	<script type="text/javascript" src="../Newjs/jszip.min.js"></script>
	<style type="text/css">
		#ClearfilterMain {
			margin-right: -10px;
			display: block;
			margin-top: 10px;
			float: right !important;
			height: 36px;
			font-size: 14px;
			font-weight: 400;
		}

		.k-multiselect-wrap .k-input {
			display: block !important;
		}

		.k-treeview .k-content, .k-treeview .k-item > .k-group, .k-treeview > .k-group {
			padding: 0;
			background: 0 0;
			list-style-type: none;
			position: relative;
			margin: -2px 1px 0px -16px;
		}

		.k-treeview .k-i-collapse, .k-treeview .k-i-expand, .k-treeview .k-i-minus, .k-treeview .k-i-plus {
			margin-left: -6px;
			margin-right: -10px;
		}

		.k-list-container.k-popup-dropdowntree .k-check-all {
			margin: 10px 10px -6px 7px;
		}

		.k-check-all {
			zoom: 1.11;
			font-size: 13px;
			margin-left: 8px;
		}

		.lst-top {
			margin-top: 5px !important;
		}

		label.k-label:hover {
			color: #1fd9e1;
		}

		.k-grid-content {
			min-height: auto !important;
		}

		.k-dropdown-wrap-hover, .k-state-default-hover, .k-state-hover, .k-state-hover:hover {
			color: #2e2e2e;
			background-color: #d8d6d6 !important;
		}

		.k-multiselect-wrap .k-input {
			padding-top: 6px;
		}

		.k-filter-row th, .k-grid-header th[data-title='Action'] {
			text-align: center;
		}

		.k-grid tbody .k-button {
			min-width: 19px !important;
			min-height: 25px;
			background-color: transparent;
			border: none;
			margin-left: 4px;
			margin-right: 4px;
			padding-left: 2px;
			padding-right: 2px;
			color: black;
		}

		.panel-heading .nav > li > a {
			font-size: 16px;
			margin-left: 0.5em;
			margin-right: 0.5em;
		}

		.panel-heading .nav > li > a {
			font-size: 16px;
			margin-left: 0.5em;
			margin-right: 0.5em;
		}

		.panel-heading .nav > li:hover {
			color: white;
			background-color: #1fd9e1;
			border-top-left-radius: 10px;
			border-top-right-radius: 10px;
			margin-left: 0.5em;
			margin-right: 0.5em;
		}

		.panel-heading .nav > li {
			margin-left: 5px !important;
			margin-right: 5px !important;
		}

		.panel-heading .nav {
			background-color: #f8f8f8;
			border: none;
			font-size: 11px;
			margin: 0px 0px 0px 0;
			border-radius: 10px;
		}

			.panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
				color: white;
				background-color: #1fd9e1;
				border-top-left-radius: 10px;
				border-top-right-radius: 10px;
				margin-left: 0.5em;
				margin-right: 0.5em;
			}

		.k-grid, .k-listview {
			margin-top: 10px;
		}

			.k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
				-webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
				box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
			}

		.k-tooltip-content {
			width: max-content;
		}

		input[type=checkbox], input[type=radio] {
			margin: 4px 6px 0;
			margin-top: 1px\9;
			line-height: normal;
		}

		.k-calendar-container {
			background-color: white;
			width: 217px;
		}

		.div.k-grid-footer, div.k-grid-header {
			border-top-width: 1px;
			margin-right: 0px;
			margin-top: 0px;
		}


		.k-grid-footer-wrap, .k-grid-header-wrap {
			position: relative;
			width: 100%;
			overflow: hidden;
			border-style: solid;
			border-width: 0 1px 0 0;
			zoom: 1;
		}


		html {
			color: #666666;
			font-size: 15px;
			font-weight: normal;
			font-family: 'Roboto',sans-serif;
		}

		.k-checkbox-label, .k-radio-label {
			display: inline;
		}

		.myKendoCustomClass {
			z-index: 999 !important;
		}

		.k-header .k-grid-toolbar {
			background: white;
			float: left;
			width: 100%;
		}

		.k-calendar .k-alt, .k-calendar th, .k-dropzone-hovered, .k-footer-template td, .k-grid-footer, .k-group, .k-group-footer td, .k-grouping-header, .k-pager-wrap, .k-popup, .k-toolbar, .k-widget .k-status {
			background-color: #f5f5f5;
			/*width: 98.5%;*/
			width: auto !important;
		}

		.k-grid td {
			line-height: 2.0em;
			border-bottom-width: 1px;
			background-color: white;
			border-width: 0 1px 1px 0px;
		}

		.k-i-more-vertical:before {
			content: "\e006";
		}

		.k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
			background-color: #1fd9e1;
			background-image: none;
			background-color: white;
		}

		k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
			color: #000000;
			border-color: #1fd9e1;
			background-color: white;
		}

		#grid .k-grid-toolbar {
			background: white;
		}

		.k-pager-wrap > .k-link > .k-icon {
			margin-top: 0px;
			margin-left: 2.7px;
			color: inherit;
		}

		.toolbar {
			float: left;
		}

		html .k-grid tr:hover {
			background: white;
		}

		html .k-grid tr.k-alt:hover {
			background: white;
		}


		.k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
			margin-right: 0px;
			margin-right: 0px;
			margin-left: 0px;
			margin-left: 0px;
		}

		.k-auto-scrollable {
			overflow: hidden;
		}

		.k-grid-header {
			padding-right: 0px !important;
			margin-right: 2px;
		}

		.k-filter-menu .k-button {
			width: 27%;
		}

		.k-label input[type="checkbox"] {
			margin: 0px 5px 0 !important;
		}

		.k-filter-row th, .k-grid-header th.k-header {
			font-size: 15px;
			background: #f8f8f8;
			font-family: 'Roboto',sans-serif;
			color: #2b2b2b;
			font-weight: 400;
		}

		.k-primary {
			border-color: #1fd9e1;
			background-color: #1fd9e1;
		}

		.k-pager-wrap {
			background-color: white;
			color: #2b2b2b;
		}

		td.k-command-cell {
			border-width: 0 0px 1px 0px;
			text-align: center;
		}

		.k-grid-pager {
			margin-top: 0px;
			border-width: 0px 1px 1px 1px;
		}

		span.k-icon.k-i-calendar {
			margin-top: 6px;
		}

		.col-md-2 {
			width: 20%;
		}

		.k-filter-row th, .k-grid-header th.k-header {
			border-width: 0px 0px 1px 0px;
			background: #E9EAEA;
			font-weight: bold;
			margin-right: 18px;
			font-family: 'Roboto',sans-serif;
			font-size: 15px;
			color: rgba(0, 0, 0, 0.5);
			height: 20px;
			vertical-align: middle;
		}

		.k-dropdown-wrap.k-state-default {
			background-color: white;
			height: 34px;
		}

		.k-multiselect-wrap, .k-floatwrap {
			height: 34px;
		}

		.k-popup.k-calendar-container, .k-popup.k-list-container {
			background-color: white;
		}

		.k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
			-webkit-box-shadow: inset 0 0 3px 1px white;
			box-shadow: inset 0 0 3px 1px white;
		}

		label.k-label {
			font-family: roboto,sans-serif !important;
			color: #515967;
			/* font-stretch: 100%; */
			font-style: normal;
			font-weight: 400;
			min-width: max-content !important;
			white-space: pre-wrap;
		}

		.k-multicheck-wrap .k-item {
			line-height: 1.2em;
			font-size: 14px;
			margin-bottom: 5px;
		}

		label {
			display: flex;
			margin-bottom: 0px;
		}

		.k-state-default > .k-select {
			border-color: #ceced2;
			margin-top: 3px;
		}

		.k-grid-norecords {
			width: 100%;
			height: 100%;
			text-align: center;
			margin-top: 2%;
		}

		.k-grouping-header {
			font-style: italic;
			background-color: white;
		}

		.k-grid-toolbar {
			background: white;
			border: none;
		}

		.k-grid table {
			width: 100.5%;
			margin: -1px;
		}

		.k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
			color: #515967;
			padding-top: 5px;
		}

		.k-tooltip {
			margin-top: 5px;
		}
	</style>
	<script>
		function DisplaysK(val) {
			debugger;
			if (val == "success") {
				$('#updateProgress').hide();
				$("#updateProgress").html();
			}
			else
				$('#updateProgress').show();
			//setTimeout(function () { $("#updateProgress").hide(); $("#updateProgress").html(); }, 5000);
		};
	</script>
	<script type="text/javascript">
		$(document).ready(function () {


			$("#challangrid").hide();
			$("#PEtreelist").hide();
			//alert(index);
			// var dropdownlist = $("#dropdownlistMonth").data("kendoDropDownList");
			//dropdownlist.select(1);
			//dropdownlist.trigger("change");
			//$("#dropdownlistMonth").data("kendoDropDownList").select('1');

			//Bind ComplianceType
			$("#dropdownlistComplianceType").kendoDropDownList({
				placeholder: "Compliance Type",
				dataTextField: "text",
				dataValueField: "value",
				checkboxes: true,
				checkAll: true,
				autoClose: true,
				dataSource: [
					{ text: "Statutory", value: "-1" },
					{ text: "Statutory CheckList", value: "2" },
				],
				index: 0,
				change: function (e) {
					//DataBindDaynamickendoTreeListdMain();
				}
			});
			var data = [];
			$("#ddlActGroup").kendoDropDownList({
				//checkAll: true,
				autoClose: true,
				//checkAllTemplate: "Select All",
				autoWidth: true,
				dataTextField: "text",
				dataValueField: "value",
				change: function () {
					debugger;
					if ($("#ddltype").val() != "Challan" && $("#ddlActGroup").val() == "CLRA") {
						BindEntities("PE");
						$("#ddlChallanType").data("kendoDropDownList").enable(false);
						$('#divEntity').css('display', 'inline');
						$('#divLocation').css('display', 'none');
					}
					else {
						$("#ddlChallanType").data("kendoDropDownList").enable(false);
						$('#divEntity').css('display', 'none');
						$('#divLocation').css('display', 'inline');
					}

				},
				dataSource: data
			});

          <%-- var dataSource = new kendo.data.TreeListDataSource({

                transport: {
                    read: {

                        url: '<% =avacomRLCSAPI_URL%>GetAll_RLCS_ComplianceDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&DocType=' + $("#ddltype").val() + '&ChallanType=' + $("#ddlChallanType").val(),
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        dataType: 'json',
                    }
                },
                schema: {
                    data: function (response) {
                        if (response.Result != null)
                            return response.Result;
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    },
                    model: {
                        id: "BranchSrID",
                        parentId: "BranchParentSrID",
                        fields: {
                            BranchParentSrID: { field: "BranchParentSrID", nullable: true },
                            BranchSrID: { field: "BranchSrID", type: "number" }
                        },
                        expanded: false
                    },
                },

            });--%>




			var dataSource = null;

			$("#challangrid").kendoGrid({
				dataSource: dataSource,
				editable: true,
				sortable: true,
				filterable: true,
				reorderable: true,
				resizeable: true,
				multi: true,
				selectable: true,
				pageable: {
					pageSizes: ['All', 5, 10, 20],
				},

				// toolbar: [{ name: "create", text: " " }],
				columnMenu: true,
				columns: [
					{ hidden: true, field: "Type", menu: false, },
					{ hidden: true, field: "ClientId", menu: false, },
					{ hidden: true, field: "IsStatuoryOREventBasedORCheckList", menu: false, },
					{ hidden: true, field: "ScheduledOnID", menu: false, },
					{ hidden: true, field: "ChallanType", menu: false, },
					{
						field: "ClientName", expandable: true, title: "Client Name", width: "28.7%;",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "Branch", expandable: true, title: "Location/Code", width: "18.7%;",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ShortForm", title: "Description", width: "24.7%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},

					{
						hidden: true,
						field: "Name", title: 'ACT', menu: false,
						width: "32.7%",
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "EventName", title: 'Event Name', menu: false,
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "EventNature", title: 'Event Nature', menu: false,
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ScheduledOn", title: 'Due Date',
						type: "date",
						//  width: 120,
						format: "{0:dd-MMM-yyyy}",
						attributes: {
							style: 'white-space: nowrap;'
							//border-width:0px 0px 1px 1px;
						},
						template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
						filterable: {

							search: true,
							multi: true,
							extra: false,
							operators: {
								string: {
									type: 'date',
									format: "{0:dd-MMM-yyyy}",
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ForMonth", title: "Period", width: "10%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "Status", title: "Status", width: "25.7%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},

					{ field: "CM_EstablishmentType", title: "Establishment Type", hidden: true, menu: false, },
					{
						title: "Action",
						command: [
							{
								name: "editLable1", text: " ", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit"
							},
							{ name: "edit1", text: " ", iconClass: "k-icon k-i-download", className: "ob-download" },
							{ name: "edit2", text: " ", iconClass: "k-icon k-i-eye", className: "ob-overview" },

						], lock: true, width: "13%",
					}
				]
				,
			});
			var challangrid = $("#challangrid").data("kendoGrid");
			var emptyDataSource = new kendo.data.DataSource({
				data: []
			});
			challangrid.setDataSource(emptyDataSource);

			$("#challangrid").kendoTooltip({
				filter: ".ob-edit",
				content: function (e) {
					return "View";
				}
			});
			$("#challangrid").kendoTooltip({
				filter: ".ob-overview",
				content: function (e) {
					return "Overview";
				}
			});
			$("#challangrid").kendoTooltip({
				filter: ".ob-download",
				content: function (e) {
					return "Download";
				}
			});

			$(document).on("click", "#challangrid tbody tr .ob-download", function (e) {
				//debugger;            
				var item = $("#challangrid").data("kendoGrid").dataItem($(this).closest("tr"));
				OpenDownloadOverviewpup(item.ScheduleIds, item.ID, item.IsStatuoryOREventBasedORCheckList)
				return true;
			});

			$(document).on("click", "#challangrid tbody tr .ob-edit", function (e) {
				//debugger;            
				var item = $("#challangrid").data("kendoGrid").dataItem($(this).closest("tr"));
				OpenDocumentOverviewpup(item.ScheduleIds, item.ID)
				return true;
			});
			$(document).on("click", "#challangrid tbody tr .ob-overview", function (e) {
				var item = $("#challangrid").data("kendoGrid").dataItem($(this).closest("tr"));
				debugger;
				OpenOverViewpup(item.ScheduleIds, item.ComplianceInstanceID);
				return true;
			});


			$("#treelist").kendoTreeList({
				dataSource: dataSource,
				editable: true,
				sortable: true,
				filterable: true,
				reorderable: true,
				resizeable: true,
				multi: true,
				selectable: true,
				pageable: {
					pageSizes: ['All', 5, 10, 20],
				},
				dataBinding: function () {
					var total = this.dataSource._pristineTotal;
					if (this.dataSource.pageSize() == undefined) {
						this.dataSource.pageSize(total);
						record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
					}
					else {
						record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
					}
				},
				// toolbar: [{ name: "create", text: " " }],
				columnMenu: true,
				columns: [
					{ hidden: true, field: "Type", menu: false, },
					{ hidden: true, field: "IsStatuoryOREventBasedORCheckList", menu: false, },
					{ hidden: true, field: "ScheduledOnID", menu: false, },
					{
						field: "Branch", expandable: true, title: "Location", width: "18.7%;",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ComplianceID", title: 'Comp.Id', width: "10%;",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ShortForm", title: "Description", width: "28.7%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "Name", title: 'ACT', menu: false,
						width: "32.7%",
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "EventName", title: 'Event Name', menu: false,
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "EventNature", title: 'Event Nature', menu: false,
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ScheduledOn", title: 'Due Date',
						type: "date",
						//  width: 120,
						format: "{0:dd-MMM-yyyy}",
						attributes: {
							style: 'white-space: nowrap;'
							//border-width:0px 0px 1px 1px;
						},
						template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
						filterable: {

							search: true,
							multi: true,
							extra: false,
							operators: {
								string: {
									type: 'date',
									format: "{0:dd-MMM-yyyy}",
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ForMonth", title: "Period", width: "12%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "Status", title: "Status", width: "15%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},

					{ field: "CM_EstablishmentType", title: "Establishment Type", hidden: true, menu: false, },
					{
						title: "Action",
						command: [
							{ name: "editLable1", text: " ", className: "ob-lable", imageClass: "k-i-hyperlink-open", className: "ob-edit" },
							{ name: "edit1", text: " ", iconClass: "k-icon k-i-download", imageClass: "k-i-download", className: "ob-download" },
							{ name: "edit2", text: " ", iconClass: "k-icon k-i-overview", imageClass: "k-i-eye", className: "ob-overview" },

						], lock: true, width: "13%",
					}
				]
				, dataBound: onDataBound,

			});
			var treelist = $("#treelist").data("kendoTreeList");
			var emptyDataSource = new kendo.data.DataSource({
				data: []
			});
			treelist.setDataSource(emptyDataSource);

			$("#treelist").kendoTooltip({
				filter: ".ob-edit",
				content: function (e) {
					return "View";
				}
			});
			$("#treelist").kendoTooltip({
				filter: ".ob-overview",
				content: function (e) {
					return "Overview";
				}
			});
			$("#treelist").kendoTooltip({
				filter: ".ob-download",
				content: function (e) {
					return "Download";
				}
			});
			/*   var treelist = $("#treelist").data("kendoTreeList");
			   var emptyDataSource = new kendo.data.DataSource({
				   data: []
			   });
			   treelist.setDataSource(emptyDataSource);*/


			$("#PEtreelist").kendoTreeList({
				dataSource: dataSource,
				editable: true,
				sortable: true,
				filterable: true,
				reorderable: true,
				resizeable: true,
				multi: true,
				selectable: true,
				pageable: {
					pageSizes: ['All', 5, 10, 20],
				},
				dataBinding: function () {
					var total = this.dataSource._pristineTotal;
					if (this.dataSource.pageSize() == undefined) {
						this.dataSource.pageSize(total);
						record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
					}
					else {
						record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
					}
				},
				// toolbar: [{ name: "create", text: " " }],
				columnMenu: true,
				columns: [
					{ hidden: true, field: "Type", menu: false, },
					{ hidden: true, field: "IsStatuoryOREventBasedORCheckList", menu: false, },
					{ hidden: true, field: "ScheduledOnID", menu: false, },
					{ hidden: true, field: "PEID", menu: false, },
					{
						field: "Branch", expandable: true, title: "Location", width: "10%;",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ComplianceID", title: 'Comp.Id', width: "10%;",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "PENAme", title: "Principle Employer", width: "15%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ShortDescription", title: "Description", width: "15%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "RequiredForms", title: "Forms", width: "10%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "Name", title: 'ACT', menu: false,
						//width: "32.7%",
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "EventName", title: 'Event Name', menu: false,
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						hidden: true,
						field: "EventNature", title: 'Event Nature', menu: false,
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ScheduledOn", title: 'Due Date', width: "10%",
						type: "date",
						//  width: 120,
						format: "{0:dd-MMM-yyyy}",
						attributes: {
							style: 'white-space: nowrap;'
							//border-width:0px 0px 1px 1px;
						},
						template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
						filterable: {

							search: true,
							multi: true,
							extra: false,
							operators: {
								string: {
									type: 'date',
									format: "{0:dd-MMM-yyyy}",
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "ForMonth", title: "Period", width: "10%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "Status", title: "Status", width: "10%",
						filterable: {
							extra: true,
							search: true,
							multi: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},

					{ field: "CM_EstablishmentType", title: "Establishment Type", hidden: true, menu: false, },
					{
						title: "Action",
						command: [
							{ name: "editLable1", text: " ", className: "ob-lable", imageClass: "k-i-hyperlink-open", className: "ob-edit" },
							{ name: "edit1", text: " ", iconClass: "k-icon k-i-download", imageClass: "k-i-download", className: "ob-download" },
							{ name: "edit2", text: " ", iconClass: "k-icon k-i-overview", imageClass: "k-i-eye", className: "ob-overview" },

						], lock: true, width: "10%",
					}
				]
				, dataBound: onDataBound,

			});
			var PEtreelist = $("#PEtreelist").data("kendoTreeList");
			var emptyDataSource = new kendo.data.DataSource({
				data: []
			});
			PEtreelist.setDataSource(emptyDataSource);

			$("#PEtreelist").kendoTooltip({
				filter: ".ob-edit",
				content: function (e) {
					return "View";
				}
			});
			$("#PEtreelist").kendoTooltip({
				filter: ".ob-overview",
				content: function (e) {
					return "Overview";
				}
			});
			$("#PEtreelist").kendoTooltip({
				filter: ".ob-download",
				content: function (e) {
					return "Download";
				}
			});
		});


		function onDataBound(e) {
			var data = this.dataSource.view();
			for (var i = 0; i < data.length; i++) {
				var uid = data[i].uid;
				var row = this.table.find("tr[data-uid='" + uid + "']");
				if (data[i].BranchParentSrID == null) {
					row.find(".ob-overview").hide();
					row.find(".ob-edit").hide();
				}
				else {
					row.find(".ob-overview").show();
					row.find(".ob-edit").show();
				}
			}

		}

		$(document).on("click", "#PEtreelist tbody tr .ob-download", function (e) {
			debugger;
			var item = $("#PEtreelist").data("kendoTreeList").dataItem($(this).closest("tr"));

			if (item.BranchParentSrID == null) {

				DownloadDocuments_AfterJune(item.CustomerBranchID, item.CM_EstablishmentType, item.CM_State, $("#dropdownlistMonth").data("kendoDropDownList").value(), $("#dropdownlistYear").data("kendoDropDownList").value(), $("#ddlChallanType").val(), "2", $("#ddltype").val(), item.Branch);
				return true;
			}
			if (item.BranchParentSrID != null) {
				OpenDownloadOverviewpup(item.ScheduledOnID, item.ID, item.IsStatuoryOREventBasedORCheckList)
				return true;
			}
			return false;
		});

		$(document).on("click", "#treelist tbody tr .ob-download", function (e) {
			//debugger;            
			var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));

			if (item.BranchParentSrID == null) {

				DownloadDocuments_AfterJune(item.CustomerBranchID, item.CM_EstablishmentType, item.CM_State, $("#dropdownlistMonth").data("kendoDropDownList").value(), $("#dropdownlistYear").data("kendoDropDownList").value(), $("#ddlChallanType").val(), "2", $("#ddltype").val(), item.Branch);
				return true;
			}
			if (item.BranchParentSrID != null) {
				OpenDownloadOverviewpup(item.ScheduledOnID, item.ID, item.IsStatuoryOREventBasedORCheckList)
				return true;
			}
			return false;
		});

		function DownloadDocuments_AfterJune(Customer_BranchID, EstablishmentType, State, Month, Year, Type, FileTypeId, DocumentType, Branch) {

			$('#DownloadViews').attr('src', "RLCS_MyComplianceDocuments_New.aspx?Customer_BranchID=" + Customer_BranchID + "&EstablishmentType=" + EstablishmentType + "&State=" + State + "&Month=" + Month + "&Year=" + Year + "&Type=" + Type + "&FileTypeId=" + FileTypeId + "&DocumentType=" + DocumentType + "&Branch=" + Branch);

		}

		$(document).on("click", "#treelist tbody tr .ob-edit", function (e) {
			//debugger;            
			var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
			if (item.BranchParentSrID != null) {
				OpenDocumentOverviewpup(item.ScheduledOnID, item.ID)
				return true;
			}
			return false;
		});
		$(document).on("click", "#treelist tbody tr .ob-overview", function (e) {
			var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
			debugger;
			if (item.BranchParentSrID != null) {
				OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID);
			}
			return true;
		});

		$(document).on("click", "#PEtreelist tbody tr .ob-edit", function (e) {
			//debugger;            
			var item = $("#PEtreelist").data("kendoTreeList").dataItem($(this).closest("tr"));
			if (item.BranchParentSrID != null) {
				OpenDocumentOverviewpup(item.ScheduledOnID, item.ID)
				return true;
			}
			return false;
		});
		$(document).on("click", "#PEtreelist tbody tr .ob-overview", function (e) {
			var item = $("#PEtreelist").data("kendoTreeList").dataItem($(this).closest("tr"));
			debugger;
			if (item.BranchParentSrID != null) {
				OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID);
			}
			return true;
		});
		//Redirect Pages
		function OpenDocumentOverviewpup(scheduledonid, transactionid) {
			$('#divOverView').modal('show');
			$('#OverViews').attr('width', '1150px');
			$('#OverViews').attr('height', '500px');
			$('.modal-dialog').css('width', '1200px');
			$('#OverViews').attr('src', "../Common/DocumentOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + $("#dropdownlistComplianceType").val());
		}
		function OpenDownloadOverviewpup(scheduledonid, transactionid, type) {
			var ISStatutoryflag = 2;

			if (type !== '') {
				if (type === 'Statutory CheckList') {
					ISStatutoryflag = 2;
				} else if (type === 'Statutory') {
					ISStatutoryflag = -1;
				}
			}
			$('#divDownloadView').modal('show');
			$('#DownloadViews').attr('width', '401px');
			$('.modal-dialog').css('width', '437px');
			$('.modal-dialog').css('height', '300px');
			$('#DownloadViews').attr('src', "../Common/HRPlusDownloadOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid + "&ISStatutoryflag=" + ISStatutoryflag);

		}


		function OpenOverViewpup(scheduledonid, instanceid) {

			$('#divOverView1').modal('show');
			$('#OverViews1').attr('width', '1150px');
			$('#OverViews1').attr('height', '500px');
			$('.modal-dialog').css('width', '1200px');

			if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {

				$('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
			}
			else {
				$('#OverViews1').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
			}
		}
		//Close Functions
		function CloseClearOV() {
			$('#OverViews').attr('src', "../Common/blank.html");
		}

		function CloseClearDV() {
			$('#DownloadViews').attr('src', "../Common/blank.html");
		}


		function BindChallanType() {
			$("#ddlChallanType").kendoDropDownList({
				placeholder: "Challan Type",
				autoClose: true,
				autoWidth: true,
				dataTextField: "text",
				dataValueField: "value",
				index: 0,
				change: function (e) {
					$('#ClearfilterMain').css('display', 'block');
					dropDownFilter();
					$("#ddlEntitySubEntityLocation").data("kendoDropDownTree").value([]);
                    //$('#ClearfilterMain').css('display', 'none');
                    //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    //$('input[name="sel_chkbx"]').attr("checked", false);
                    //$('input[name="sel_chkbx_Main"]').attr("checked", false);

                    <%-- var month = $("#ddlMonth").val();
                        var year = $("#ddlYear").val();

                        //if (month > 5 && year >= 2018) {  change by amol
                            if (month > <% =newRegistersMonth%> && year >= <% =newRegistersYear%>) {
                            var grid = $('#grid').data("kendoGrid");
                            grid.setDataSource(null);

                            BindSecondGrid();

                            $('#divddlChallanType').css('display', 'block');

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }

                            //BindSecondGrid();
                            //BindSecondGridMonth();
                            //BindChallansType();
                        } else if (year < <% =newRegistersYear%> || (month <= <% =newRegistersMonth%> && year <= <% =newRegistersYear%>)) {

                            $('#divddlChallanType').css('display', 'none');

                            BindFirstGrid();

                            var values = this.value().trim();
                            if (values != "" && values != null) {
                                var filter = { logic: "or", filters: [] };
                                filter.filters.push({
                                    field: "PayrollMonth", operator: "eq", value: parseInt(values)
                                });
                                var dataSource = $("#grid").data("kendoGrid").dataSource;
                                dataSource.filter(filter);
                            }
                        } else {
                            ClearAllFilter();
                        }--%>
				},
				dataSource: [
					{ text: "ESI", value: "ESI" },
					{ text: "EPF", value: "EPF" },
					{ text: "PT", value: "PT" },
				]
			});
		}

    </script>

	<script type="text/javascript">

		$(document).ready(function () {

			$("#dropdowntree").kendoDropDownTree({
				placeholder: "Entity/Sub-Entity/Location",
				checkboxes: {
					checkChildren: true
				},
				checkAll: true,
				autoWidth: true,
				checkAllTemplate: "Select All",

				dataTextField: "Name",
				//dataTextField: "ID",
				dataValueField: "ID",
				change: function (e) {
					//ApplyFilter();
					fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
					//$('input[id=chkAllMain]').prop('checked', false);
					//$('#dvbtndownloadDocumentMain').css('display', 'block');
				},
				dataSource: {
					severFiltering: true,
					transport: {
						read: {
							url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsListCLRA?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&ActGroup=S&E/Factory',
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
						},
					},
					schema: {
						data: function (response) {

							return response.Result;
						},
						model: {
							children: "Children"
						}
					}
				}
			});

			//  $("#dropdowntree").data("kendoDropDownTree").value("Select All");
			//  var dropdowntree = $("#dropdowntree").data("kendoDropDownTree");
			// dropdowntree.value(["1"]);
			// $("#dropdowntree .k-checkbox-wrapper input").prop("checked", true).trigger("change");



			$("#dropdownlistMonth").kendoDropDownList({
				placeholder: "Month",
				checkboxes: true,
				checkAll: true,
				autoClose: true,
				checkAllTemplate: "Select All",
				autoWidth: true,
				dataTextField: "text",
				dataValueField: "value",
				//   index: 3,
				change: function () {
					// ApplyFilter();
					//$('#dvbtndownloadDocumentMain').css('display', 'block');
				},
				dataSource: [
					{ text: "Month", value: "-1" },
					{ text: "January", value: "01" },
					{ text: "February", value: "02" },
					{ text: "March", value: "03" },
					{ text: "April", value: "04" },
					{ text: "May", value: "05" },
					{ text: "June", value: "06" },
					{ text: "July", value: "07" },
					{ text: "August", value: "08" },
					{ text: "September", value: "09" },
					{ text: "October", value: "10" },
					{ text: "November", value: "11" },
					{ text: "December", value: "12" }
				]
			});
			var d = new Date();
			var $n = parseInt(d.getMonth());
			var dropdownlist = $("#dropdownlistMonth").data("kendoDropDownList");
			dropdownlist.select(dropdownlist.ul.children().eq($n + 1));

			var _year = new Date().getFullYear();

			var arr = [];
			for (var i = _year; i > _year - 5; i--) {
				arr.push(i);
			}
			$("#dropdownlistYear").kendoDropDownList({
				placeholder: "Year",
				checkboxes: true,
				checkAll: true,
				autoClose: true,
				checkAllTemplate: "Select All",
				autoWidth: true,
				dataTextField: "text",
				dataValueField: "value",
				change: function () {
					// ApplyFilter();
					// $('#dvbtndownloadDocumentMain').css('display', 'block');
				},
				//dataSource: arr
				dataSource: [
					{ text: "Year", value: "-1" },
                    { text: "2022", value: "2022" },
                    { text: "2021", value: "2021" },
					{ text: "2020", value: "2020" },
					{ text: "2019", value: "2019" },
					{ text: "2018", value: "2018" },
					{ text: "2017", value: "2017" },
					{ text: "2016", value: "2016" },
					{ text: "2015", value: "2015" },
					{ text: "2014", value: "2014" }
				]
			});
			var d = new Date();
			var $n = parseInt(d.getFullYear());
			$("#dropdownlistYear").data('kendoDropDownList').value($n);


			//PE
			var BranchID = "";

			$("#ddlPrincipleEmployer").kendoDropDownList({
				filter: "startswith",
				autoClose: false,
				dataTextField: "PEName",
				dataValueField: "PEID",
				optionLabel: "Select PE",
				change: function (e) {
					//  ApplyFilter();
				},
				dataSource: {
					transport: {

						read: {
							url: '<% =avacomRLCSAPI_URL%>GetPrincipleEmployer?customerID=<% =CustId%>&ClientID=' + BranchID,

							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							return response.Result;
						}
					}
				},

				dataBound: function (e) {
					e.sender.list.width("150");
				},
			});
			//END
			function GetPE() {
				var selectedActs = $("#dropdownlistAct").data("kendoDropDownList").value();
				if ((selectedActs != "-1") && (selectedActs != "")) {
					//GET Act CLRA  GetActType
					debugger;
					$.ajax({
						type: "POST",
						url: "../RLCS/RLCS_MyComplianceDocuments_New.aspx/GetActType",
						data: "{ 'ActID': '" + selectedActs + "' }",
						contentType: "application/json; charset=utf-8",
						success: function (data) {
							debugger;
							if (data.d == "CLRA") {
								$("#CLRAdiv").show();

							}
							else {
								$("#CLRAdiv").hide();
							}
						},
						failure: function (response) {

						}
					});

				}
			}

			$("#dropdownlistStatus").kendoDropDownTree({
				placeholder: "Status",
				checkboxes: true,
				checkAll: true,
				autoClose: true,
				autoWidth: true,
				checkAllTemplate: "Select All",
				change: function (e) {
					//   ApplyFilter();
					fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');

					//$('input[id=chkAllMain]').prop('checked', false);
					//$('#dvbtndownloadDocumentMain').css('display', 'block');
				},
				dataSource: [
					{ text: "Closed-Delayed", value: "Closed-Delayed" },
					{ text: "Closed-Timely", value: "Closed-Timely" },
					{ text: "Rejected", value: "Rejected" },
					{ text: "Pending For Review", value: "Pending For Review" }
				]
			});

			//register selected by default after  #dropdownlistAct ending

			$("#dropdownlistAct").kendoDropDownList({
				filter: "startswith",
				autoClose: false,
				dataTextField: "ACTNAME",
				dataValueField: "ACTID",
				optionLabel: "Act",
				change: function (e) {
					// ApplyFilter();
					GetPE();
				},
				dataSource: {
					transport: {

						read: {
							url: '<% =avacomRLCSAPI_URL%>GetApplicableActs?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&IsAvantis=true',

							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							return response.Result;
						}
					}
				},

				dataBound: function (e) {
					e.sender.list.width("1000");
				},
			});


			BindChallanType();



			$('#ClearfilterMain').css('display', 'block');

		});

		function BindActGroupType(groupType) {

			var _dllActGroupData = [
				{ text: "S&E/Factory", value: "S&E/Factory" },
				{ text: "CLRA", value: "CLRA" }
			]
			if (groupType == "Challan") {
				_dllActGroupData.length = _dllActGroupData.length - 1;
			}

			$("#ddlActGroup").data("kendoDropDownList").setDataSource(_dllActGroupData);
			$("#ddlActGroup").data("kendoDropDownList").value("S&E/Factory");

		}
		function Bind_ddltype(actType) {
			debugger;
			var _dllData = [
				{ text: "Return", value: "Return" },
				{ text: "Register", value: "Register" },
				{ text: "Challan", value: "Challan" }
			]
			$("#ddltype").kendoDropDownTree({
				placeholder: "Document Type",
				checkboxes: false,
				checkAll: true,
				autoClose: true,
				checkAllTemplate: "Select All",
				autoWidth: true,
				dataTextField: "text",
				dataValueField: "value",
				change: function () {
					debugger;
					BindActGroupType($("#ddltype").val());
					$('#ClearfilterMain').css('display', 'block');
					
					if ($("#ddltype").val() == "Challan") {
						BindEntities("Challan");
						$('#divLocation').css('display', 'none');
						$('#divEntity').css('display', 'inline');
						$("#ddlChallanType").data("kendoDropDownList").enable(true);
					}
					else {
						$("#ddlChallanType").data("kendoDropDownList").enable(false);
						$('#divEntity').css('display', 'none');
						$('#divLocation').css('display', 'inline');
					}

					// ApplyFilter();
					fCreateStoryBoard('ddltype', 'filterdoctype', 'doctype');
					// $('input[id=chkAllMain]').prop('checked', false);
					//$('#dvbtndownloadDocumentMain').css('display', 'block');

				},
				dataSource: _dllData

			});
			$("#ddltype").data("kendoDropDownTree").value("Register");

			if ($("#ddltype").data("kendoDropDownTree").value() != "Challan") {
				$("#ddlChallanType").data("kendoDropDownList").enable(false);
			}



		}

		function BindEntityLocationSate(actType) {
			debugger;
			var dataSource = new kendo.data.HierarchicalDataSource({
				transport: {
					read: {
						url: '<% =avacomRLCSAPI_URL%>GetAssignedEntitiesLocationsListCLRA?customerID=<% =CustId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&ActGroup=' + actType,
						beforeSend: function (request) {
							request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
							request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
							request.setRequestHeader('Content-Type', 'application/json');
						},
					},
				},
				schema: {
					data: function (response) {

						return response.Result;
					},
					model: {
						children: "Children"
					}
				}
			})

			//$("#dropdowntree").kendoDropDownTree({
			var ddl = $("#dropdowntree").data("kendoDropDownTree");
			ddl.setDataSource(dataSource);
		}

		function ApplyFilter(e) {
			debugger;
			e.preventDefault();
			DisplaysK("abc");

			if ($("#ddlActGroup").val() == "CLRA") {
				var PEID = $("#ddlEntityList").data("kendoDropDownList").value();
				if (PEID == "")
					PEID = 0;
				$("#challangrid").hide();
				$("#treelist").hide();
				$("#PEtreelist").show();
				var dataSource = new kendo.data.TreeListDataSource({

					transport: {
						read: {

							url: '<% =avacomRLCSAPI_URL%>GetAll_RLCS_ComplianceDocuments_PE?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&DocType=' + $("#ddltype").val() + '&PEID=' + PEID,
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							if (response.Result != null) {
								DisplaysK("success");
								return response.Result;
							}

						},
						total: function (response) {
							if (response.Result != null && response.Result != undefined) {
								if (response.Result.length > 0) {

								}
								return response.Result.length;
							}
						},
						model: {
							id: "BranchSrID",
							parentId: "BranchParentSrID",
							fields: {
								BranchParentSrID: { field: "BranchParentSrID", nullable: true },
								BranchSrID: { field: "BranchSrID", type: "number" }
							},
							expanded: false
						},
					},

				});
				var PEtreelist = $("#PEtreelist").data("kendoTreeList");
				PEtreelist.setDataSource(dataSource);
			}
			else if ($("#ddltype").val() == "Challan") {

				$("#treelist").hide();
				$("#PEtreelist").hide();
				$("#challangrid").show();

				var dataSourceC = new kendo.data.DataSource({
					transport: {
						read: {
							//+ '&ClientID=' + $("#ddlEntityList").val()
							url: '<% =avacomRLCSAPI_URL%>GetAll_RLCS_ComplianceDocuments_Challan?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&DocType=' + $("#ddltype").val() + '&ChallanType=' + $("#ddlChallanType").val() + '&ClientID=' + $("#ddlEntityList").val(),
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {

						data: function (response) {
							if (response.Result != null) {
								DisplaysK("success");
								return response.Result;
							}
						},
						total: function (response) {
							if (response.Result != null && response.Result != undefined) {
								if (response.Result.length > 0) {

								}
								return response.Result.length;
							}
						},
					},

				});
				var challangrid = $("#challangrid").data("kendoGrid");
				challangrid.setDataSource(dataSourceC);

			}
			else {
				$("#challangrid").hide();
				$("#PEtreelist").hide();
				$("#treelist").show();
				var dataSource = new kendo.data.TreeListDataSource({

					transport: {
						read: {

							url: '<% =avacomRLCSAPI_URL%>GetAll_RLCS_ComplianceDocuments?userID=<% =UId%>&customerID=<% =CustId%>&MonthId=<% =IsMonthID%>&StaOrInt=SAT&StaIntFlag=5&FY=&profileID=<% =ProfileID%>&DocType=' + $("#ddltype").val() + '&ChallanType=' + $("#ddlChallanType").val(),
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							if (response.Result != null) {
								DisplaysK("success");
								return response.Result;
							}
						},
						total: function (response) {
							if (response.Result != null && response.Result != undefined) {
								if (response.Result.length > 0) {

								}
								return response.Result.length;
							}
						},
						model: {
							id: "BranchSrID",
							parentId: "BranchParentSrID",
							fields: {
								BranchParentSrID: { field: "BranchParentSrID", nullable: true },
								BranchSrID: { field: "BranchSrID", type: "number" }
							},
							expanded: false
						},
					},

				});
				var treelist = $("#treelist").data("kendoTreeList");
				treelist.setDataSource(dataSource);
			}

			//code to filter the kendo data baised on DDL values
			var finalSelectedfilter = { logic: "and", filters: [] };

			//var selectedLocations = $("#dropdowntreedropdowntree").data("kendoDropDownTree")._values;
			//alert(selectedLocations);
			//if (selectedLocations.length > 0) {
			//	var locFilter = { logic: "or", filters: [] };

			//	$.each(selectedLocations, function (i, v) {
			//		//alert(i);
			//		//alert(v);
			//		locFilter.filters.push({
			//			field: "CustomerBranchID", operator: "eq", value: parseInt(v)
			//		});
			//	});

			//	finalSelectedfilter.filters.push(locFilter);
			//}


			var selectedYrs = $("#dropdownlistYear").data("kendoDropDownList").value();
			if (selectedYrs != "-1") {
				var locFilterr = { logic: "or", filters: [] };

				var fruits = [selectedYrs];
				$.each(fruits, function (i, v) {
					locFilterr.filters.push({
						field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
					});
				});

				finalSelectedfilter.filters.push(locFilterr);
			}

			var selectedMonths = $("#dropdownlistMonth").data("kendoDropDownList").value();
			if (selectedMonths != "-1") {
				var monthFilter = { logic: "or", filters: [] };

				var fruits = [selectedMonths];
				$.each(fruits, function (i, v) {
					monthFilter.filters.push({
						field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
					});
				});

				finalSelectedfilter.filters.push(monthFilter);
			}

			var selectedActs = $("#dropdownlistAct").data("kendoDropDownList").value();
			if ((selectedActs != "-1") && (selectedActs != "")) {
				var actFilter = { logic: "or", filters: [] };

				var fruits = [selectedActs];
				$.each(fruits, function (i, v) {
					actFilter.filters.push({
						field: "ActID", operator: "eq", value: parseInt(v)
					});
				});

				finalSelectedfilter.filters.push(actFilter);
			}
			if ($("#ddlActGroup").val() == "CLRA") {
				var selectedPEIDs = $("#ddlEntityList").data("kendoDropDownList").value();

				debugger;
				if ((selectedPEIDs != "-1") && (selectedPEIDs != "")) {
					var PEFilter = { logic: "or", filters: [] };

					var fruits = [selectedPEIDs];
					$.each(fruits, function (i, v) {
						PEFilter.filters.push({
							field: "PEID", operator: "eq", value: parseInt(v)
						});
					});

					finalSelectedfilter.filters.push(PEFilter);

				}
			}


			var selectedStatus = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
			if (selectedStatus.length > 0) {
				var statusFilter = { logic: "or", filters: [] };

				$.each(selectedStatus, function (i, v) {
					statusFilter.filters.push({
						field: "Status", operator: "eq", value: v
					});
				});

				finalSelectedfilter.filters.push(statusFilter);
			}


			var selectedType = $("#ddltype").data("kendoDropDownTree")._values;
			if (selectedType.length > 0) {
				var typeFilter = { logic: "or", filters: [] };

				$.each(selectedType, function (i, v) {
					typeFilter.filters.push({
						field: "DocType", operator: "eq", value: v
					});
				});

				finalSelectedfilter.filters.push(typeFilter);
			}

			if ($("#ddltype").data("kendoDropDownTree").text() == "Challan") {
				debugger;
				var entityname = $("#ddlEntityList").data("kendoDropDownList").value();
				if (entityname != "") {
					var entityFilter = { logic: "or", filters: [] };
					var entityArray = [entityname];
					$.each(entityArray, function (i, v) {
						entityFilter.filters.push({
							field: "ClientId", operator: "eq", value: v
						});
					});
					
					finalSelectedfilter.filters.push(entityFilter);
					
				}

				var challanType = $("#ddlChallanType").data("kendoDropDownList").value();

				if (challanType != "-1") {
					var challanFilter = { logic: "or", filters: [] };

					var fruits = [challanType];
					$.each(fruits, function (i, v) {
						challanFilter.filters.push({
							field: "ChallanType", operator: "eq", value: v
						});
					});

					finalSelectedfilter.filters.push(challanFilter);
				}

			}
			
			if (finalSelectedfilter.filters.length > 0) {
				debugger
				var dataSource = [];

				if ($("#ddlActGroup").data("kendoDropDownList").value() == "CLRA") {

					dataSource = $("#PEtreelist").data("kendoTreeList").dataSource;
					dataSource.filter(finalSelectedfilter);
				}
				else if ($("#ddltype").data("kendoDropDownTree").text() == "Challan") {
					dataSource = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter(finalSelectedfilter);
				}
				else {
					dataSource = $("#treelist").data("kendoTreeList").dataSource;
					dataSource.filter(finalSelectedfilter);
				}

				$('#ClearfilterMain').css('display', 'block');
			}
			else {
				debugger
				if ($("#ddlActGroup").data("kendoDropDownList").value() == "CLRA") {
					dataSource = $("#PEtreelist").data("kendoTreeList").dataSource;
					dataSource.filter({});
				}
				else if ($("#ddltype").data("kendoDropDownTree").text() == "Challan") {
					dataSource = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({});
				}
				else {
					dataSource = $("#treelist").data("kendoTreeList").dataSource;
					dataSource.filter({});
				}
				$('#ClearfilterMain').css('display', 'none');
			}
		}

		function setCommonAllfilterMain() {

			if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#dropdownlistAct").data("kendoDropDownList").text() != "Act"
				&& $("#ddlPrincipleEmployer").data("kendoDropDownList").text() != "Select PE") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//Act Details  
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}
				//PrincipleEmployer Details  
				var PEdetails = [];
				var listp = $("#ddlPrincipleEmployer").data("kendoDropDownList").value();
				if (listp != "-1") {
					var fruits = [listp];
					$.each(fruits, function (i, v) {
						PEdetails.push({
							field: "PEID", operator: "eq", value: parseInt(v)
						});
					});
				}
				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});


				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Actdetails
						},
						{
							logic: "or",
							filters: PEdetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Actdetails
						},
						{
							logic: "or",
							filters: PEdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});
				//Act Details  
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				//Act Details  
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}


				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

			}


			else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});



				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				//Act Details fixed 
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if ((list1 != "-1") && (list1 != "")) {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}


				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: Actdetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: Actdetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});


				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: typedetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				//&& $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
				//&& $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
			) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//Act Details fixed 
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if ((list1 != "-1") && (list1 != "")) {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}


				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Yeardetails
						}
					]
				});

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Yeardetails
						}
					]
				});

			}

			else if ($("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						}
					]
				});
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistAct").data("kendoDropDownList").text() != "Select Act") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//Act Details  
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}


				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Actdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;

				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: typedetails
						}

					]
				});

				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: typedetails
						}

					]
				});

			}

			else if ($("#dropdownlistYear").data("kendoDropDownList").text() != "Year"
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {


				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});

				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Statusdetails
						}
					]
				});
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Yeardetails
						},
						{
							logic: "or",
							filters: Statusdetails
						}
					]
				});

			}


			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});

				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Statusdetails
						},
						{
							logic: "or",
							filters: locationsdetails
						}
					]
				});

			}


			else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
				&& $("#ddltype").data("kendoDropDownTree")._values.length > 0) {


				//Status details
				var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
				var Statusdetails = [];
				$.each(list, function (i, v) {
					Statusdetails.push({
						field: "Status", operator: "eq", value: v
					});
				});


				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Statusdetails
						}
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Statusdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month") {


				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						}
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Monthdetails
						}
					]
				});

			}

			else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

				//location details
				var list = $("#dropdowntree").data("kendoDropDownTree")._values;
				var locationsdetails = [];
				$.each(list, function (i, v) {
					locationsdetails.push({
						field: "CustomerBranchID", operator: "eq", value: parseInt(v)
					});
				});


				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: locationsdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
					]
				});

			}

			else if ($("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Yeardetails
						}
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Yeardetails
						}
					]
				});

			}

			else if ($("#ddltype").data("kendoDropDownTree")._values.length > 0
				&& $("#dropdownlistMonth").data("kendoDropDownList").text() != "Month") {

				//type Details
				var typedetails = [];
				var list12 = $("#ddltype").data("kendoDropDownTree")._values;
				$.each(list12, function (i, v) {
					typedetails.push({
						//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
						field: "DocType", operator: "eq", value: v
					});
				});

				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Monthdetails
						}
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: typedetails
						},
						{
							logic: "or",
							filters: Monthdetails
						}
					]
				});

			}

			else if ($("#dropdownlistMonth").data("kendoDropDownList").text() != "Month"
				&& $("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

				//month Details  
				var Monthdetails = [];
				var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Monthdetails.push({
							field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
						});
					});
				}

				//year Details  
				var Yeardetails = [];
				var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
				if (list1 != "-1") {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Yeardetails.push({
							field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
						});
					});
				}

				//Act Details fixed 
				var Actdetails = [];
				var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
				if ((list1 != "-1") && (list1 != "")) {
					var fruits = [list1];
					$.each(fruits, function (i, v) {
						Actdetails.push({
							field: "ActID", operator: "eq", value: parseInt(v)
						});
					});
				}



				var dataSource = $("#treelist").data("kendoTreeList").dataSource;
				var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
				dataSource.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
					]
				});
				dataSourceC.filter({
					logic: "and",
					filters: [
						{
							logic: "or",
							filters: Monthdetails
						},
						{
							logic: "or",
							filters: Yeardetails
						},
					]
				});
			}



			else {

				if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

					//location details
					var list = $("#dropdowntree").data("kendoDropDownTree")._values;
					var locationsdetails = [];
					$.each(list, function (i, v) {
						locationsdetails.push({
							field: "CustomerBranchID", operator: "eq", value: parseInt(v)
						});
					});

					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: locationsdetails
							}
						]
					});
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: locationsdetails
							}
						]
					});
				}

				if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

					//Status details
					var list = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
					var Statusdetails = [];
					$.each(list, function (i, v) {
						Statusdetails.push({
							field: "Status", operator: "eq", value: v
						});
					});

					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Statusdetails
							}
						]
					});
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Statusdetails
							}
						]
					});
				}
				if ($("#ddltype").data("kendoDropDownTree")._values.length > 0) {


					//type Details
					var typedetails = [];
					var list12 = $("#ddltype").data("kendoDropDownTree")._values;
					$.each(list12, function (i, v) {
						typedetails.push({
							//field: "NatureOfCompliance", operator: "eq", value: parseInt(v)
							field: "DocType", operator: "eq", value: v
						});
					});

					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: typedetails
							}
						]
					});
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: typedetails
							}
						]
					});
				}
				if ($("#dropdownlistMonth").data("kendoDropDownList").text() != "Month") {

					//month Details  
					var Monthdetails = [];
					var list1 = $("#dropdownlistMonth").data("kendoDropDownList").value();
					if (list1 != "-1") {
						var fruits = [list1];
						$.each(fruits, function (i, v) {
							Monthdetails.push({
								field: "RLCS_PayrollMonth", operator: "eq", value: parseInt(v)
							});
						});
					}

					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Monthdetails
							}
						]
					});
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Monthdetails
							}
						]
					});
				}
				if ($("#dropdownlistYear").data("kendoDropDownList").text() != "Year") {

					//year Details  
					var Yeardetails = [];
					var list1 = $("#dropdownlistYear").data("kendoDropDownList").value();
					if (list1 != "-1") {
						var fruits = [list1];
						$.each(fruits, function (i, v) {
							Yeardetails.push({
								field: "RLCS_PayrollYear", operator: "eq", value: parseInt(v)
							});
						});
					}

					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Yeardetails
							}
						]
					});
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Yeardetails
							}
						]
					});
				}
				if ($("#dropdownlistAct").data("kendoDropDownList").text() != "Act") {

					//Act Details  
					var Actdetails = [];
					var list1 = $("#dropdownlistAct").data("kendoDropDownList").value();
					//fixed
					if (list1 != "-1") {
						var lst = [list1];
						$.each(lst, function (i, v) {
							Actdetails.push({
								field: "ActID", operator: "eq", value: parseInt(v)
							});
						});
					}

					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Actdetails
							}
						]
					});
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: Actdetails
							}
						]
					});
				}
				if ($("#dropdownlistAct").data("kendoDropDownList").text() != "Select PE") {

					//PrincipleEmployer Details  
					var PEdetails = [];
					var listp = $("#ddlPrincipleEmployer").data("kendoDropDownList").value();
					//fixed
					if (listp != "-1") {
						var lst = [listp];
						$.each(lst, function (i, v) {
							PEdetails.push({
								field: "PEID", operator: "eq", value: parseInt(v)
							});
						});
					}

					var dataSource = $("#treelist").data("kendoTreeList").dataSource;
					var dataSourceC = $("#challangrid").data("kendoGrid").dataSource;
					dataSource.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: PEdetails
							}
						]
					});
					dataSourceC.filter({
						logic: "and",
						filters: [
							{
								logic: "or",
								filters: PEdetails
							}
						]
					});
				}
			}
		}
		function fCreateStoryBoard(Id, div, filtername) {

			var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
			$('#' + div).html('');
			$($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
			$('#' + div).css('display', 'block');

			if (div == 'filtersstoryboard') {
				$('#' + div).append('Location&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard
				$('#ClearfilterMain').css('display', 'block');
			}
			else if (div == 'filtertype') {
				$('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard               
			}
			else if (div == 'filterdoctype') {
				$('#' + div).append('DocumentType&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;');//Dashboard     
				$('#ClearfilterMain').css('display', 'block');
			}
			else if (div == 'filterrisk') {
				$('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard
				$('#ClearfilterMain').css('display', 'block');
			}
			else if (div == 'filterstatus') {
				$('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;');//Dashboard
				$('#ClearfilterMain').css('display', 'block');
			}
			else if (div == 'filterpstData1') {
				$('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterCategory') {
				$('#' + div).append('Category&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterAct') {
				$('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterCompSubType') {
				$('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterCompType') {
				$('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filtersstoryboard1') {
				$('#' + div).append('Location&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filtertype1') {
				$('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterrisk1') {
				$('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterdoctype1') {
				$('#' + div).append('DocumentType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');//Dashboard     
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterFY') {
				$('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterUser') {
				$('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}
			else if (div == 'filterstatus1') {
				$('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;');
				$('#Clearfilter').css('display', 'block');
			}

			for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
				var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
				$(button).css('display', 'none');
				$($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
				var buttontest = $($(button).find('span')[0]).text();
				if (buttontest.length > 10) {
					buttontest = buttontest.substring(0, 10).concat("...");
				}
				$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>&nbsp;');
			}

			if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
				$('#' + div).css('display', 'none');
				$($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

			}
			CheckFilterClearorNot();
			CheckFilterClearorNotMain();
		}
		function CheckFilterClearorNotMain() {
			if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#ddltype').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
				$('#ClearfilterMain').css('display', 'none');
			}
		}
		function CheckFilterClearorNot() {
			if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				//($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#ddltype1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				//($($($('#dropdownFY').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				//($($($('#dropdownPastData').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				//($($($('#dropdownlistComplianceType1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
				($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
				$('#Clearfilter').css('display', 'none');
			}
		}
		function fcloseStory(obj) {

			var DataId = $(obj).attr('data-Id');
			var dataKId = $(obj).attr('data-K-Id');
			var seq = $(obj).attr('data-seq');
			var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
			$(deepspan).trigger('click');
			var upperli = $('#' + dataKId);
			$(upperli).remove();

			//for rebind if any pending filter is present (Main Grid)
			fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
			fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
			fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
			//for rebind if any pending filter is present (ADV Grid)
			fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
			fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
			fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
			//fCreateStoryBoard('dropdownACT', 'filterAct', 'Act');
			fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
			fCreateStoryBoard('ddltype', 'filterdoctype', 'doctype');
			fCreateStoryBoard('ddltype1', 'filterdoctype1', 'doctype1');

			CheckFilterClearorNot();

			CheckFilterClearorNotMain();
		};
		function ClearAllFilterMain(e) {

			e.preventDefault();
			$("#dropdowntree").data("kendoDropDownTree").value([]);

			$("#dropdownlistStatus").data("kendoDropDownTree").value([]);
			$("#ddltype").data("kendoDropDownTree").value([]);
			$("#dropdownlistYear").data("kendoDropDownList").select(0);
			$("#dropdownlistMonth").data("kendoDropDownList").select(0);
			$("#dropdownlistAct").data("kendoDropDownList").select(0);


			$("#ddlChallanType").data("kendoDropDownList").select(0);


			//  $('#dvbtndownloadDocumentMain').css('display', 'none');

			// $("#dropdownEventName").data("kendoDropDownList").select(0);
			//$("#dropdownEventNature").data("kendoDropDownList").select(0);

			//  $("#treelist").data("kendoTreeList").dataSource.filter({});
			var PEtreelist = $("#PEtreelist").data("kendoTreeList");
			var treelist = $("#treelist").data("kendoTreeList");
			var challangrid = $("#challangrid").data("kendoGrid");
			var emptyDataSource = new kendo.data.DataSource({
				data: []
			});
			treelist.setDataSource(emptyDataSource);
			PEtreelist.setDataSource(emptyDataSource);
			challangrid.setDataSource(emptyDataSource);
			$('#ClearfilterMain').css('display', 'none');
		}
		function BindEntities(flag) {
			debugger;
			$("#ddlEntityList").kendoDropDownList({
				dataTextField: "AVACOM_BranchName",
				dataValueField: "CM_ClientID",
				optionLabel: "Select Entity",
				change: function (e) {
					$('#ClearfilterMain').css('display', 'block');
				},
				dataSource: {
					transport: {
						read: {
							url: "<% =avacomRLCSAPI_URL%>GetAssignedEntitiesORPrincipleEmployer?customerID=<% =CId%>&userID=<% =UId%>&profileID=<% =ProfileID%>&flag=" + flag,
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							return response.Result;
						}
					}
				}
			});
		}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div style="width: 99%; display: inline;">

		<input id="ddltype" data-placeholder="type" style="width: 130px;" />
		<input id="ddlChallanType" data-placeholder="Challan Type" style="width: 79px;" />
		<input id="ddlActGroup" data-placeholder="type" style="width: 90px;" />
		<input id="dropdownlistMonth" data-placeholder="Month" style="width: 111px;" />
		<input id="dropdownlistYear" data-placeholder="Year" style="width: 102px;" />
		<input id="dropdownlistStatus" data-placeholder="Status" style="width: 93px; padding: 0px;" />
		<input id="dropdownlistAct" data-placeholder="Act" style="width: 222px;" />
		<div id="divLocation" style="display: inline;">
			<input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 243px;" />
		</div>
		<div id="divEntity" style="display: inline;">
			<input id="ddlEntityList" data-placeholder="Entity" style="width: 243px;" />
		</div>
		<button id="Applyfiltermain" class=" btn-primary btn pull-right" style="margin-left: 25px; height: 36px; font-size: 14px; font-weight: 400; margin-top: 10px;" onclick="ApplyFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply Filter</button>

		<button id="ClearfilterMain" class=" btn-primary btn pull-right" style="margin-left: 25px; display: none; height: 36px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
		<button id="dvbtndownloadDocumentMain" style="float: right; height: 30px; width: 23px; border-radius: 34px; border: 0px; background-color: transparent; padding-left: 13px; display: none;" data-placement="bottom"><span class="k-icon k-i-download k-state-border-down hoverCircle" onclick="selectedDocumentMain(event)"></span></button>
		<button id="AdavanceSearch" style="float: right; height: 30px; width: 21px; border-radius: 34px; border: 0px; background-color: transparent; display: none;" data-placement="bottom" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-search hoverCircle " onclick="javascript:return false;"></span></button>
		<div id="CLRAdiv" class="lst-top" style="display: none;">
			<input id="ddlPrincipleEmployer" data-placeholder="Select PE" style="width: 150px;" />
		</div>
	</div>
	<div style="margin-top: 5px;">
		<div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filtersstoryboard">&nbsp;</div>
		<div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filtertype">&nbsp;</div>
		<div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterrisk">&nbsp;</div>
		<div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterstatus">&nbsp;</div>
		<div class="row" style="padding-bottom: 4px; font-size: 14px; display: none; color: #535b6a;" id="filterdoctype">&nbsp;</div>
	</div>
	<div class="row col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
		<div id="treelist"></div>
	</div>
	<div class="row col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
		<div id="challangrid"></div>
	</div>
	<div class="row col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
		<div id="PEtreelist"></div>
	</div>
	<div>
		<div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px; display: none;">
			<input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%; display: none;" />
			<input id="dropdownlistComplianceType" data-placeholder="Type" style="display: none;" />
		</div>
		<div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			<div class="modal-dialog" style="width: 1150px;">
				<div class="modal-content" style="width: 100%;">
					<div class="modal-header" style="border-bottom: none;">
						<button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			<div class="modal-dialog" style="width: 1150px;">
				<div class="modal-content" style="width: 100%;">
					<div class="modal-header" style="border-bottom: none;">
						<button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			<div class="modal-dialog" style="width: 360px;">
				<div class="modal-content" style="width: 100%;">
					<div class="modal-header" style="border-bottom: none;">
						<button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">

						<iframe id="DownloadViews" src="about:blank" width="300px" height="210px" frameborder="0"></iframe>

					</div>
				</div>
			</div>
		</div>
		<iframe id="downloadfile" style="display: none" src="about:blank" width="0" height="0"></iframe>
	</div>
	<script>
		$(document).ready(function () {
			//BindEntities();
			Bind_ddltype("S&E/Factory");
			BindEntityLocationSate("S&E/Factory");
			fhead('My Documents/ Compliance Documents');
			setactivemenu('ComplianceDocumentList');
		});
	</script>
</asp:Content>
