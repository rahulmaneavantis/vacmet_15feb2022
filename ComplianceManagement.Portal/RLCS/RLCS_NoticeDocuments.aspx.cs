﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSLocation;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_NoticeDocuments : System.Web.UI.Page
    {
        public static string LitigatinoDocPath = "";
        protected bool flag;
        public string CaseNoticeflag;
        private long CustomerID = AuthenticationHelper.CustomerID;
        public RLCS_LocationDetails rlcslocationdetails = new RLCS_LocationDetails();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                flag = false;

                BindParty();
                BindDepartment();
                //applyCSStoFileTag_ListItems();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                    userAssignedBranchList = rlcslocationdetails.GetuserAssignedBranchList(userAssignedBranchList);
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                            BindLocationFilter(assignedbranchIDs);
                        }
                    }
                }

                BindGrid();
                //BindFileTags();
                bindPageNumber();
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";
            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();
            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            ddlPartyPage.DataTextField = "Name";
            ddlPartyPage.DataValueField = "ID";
            ddlPartyPage.DataSource = obj;
            ddlPartyPage.DataBind();
            ddlPartyPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlStatus.SelectedValue == "1")
                    {
                        isstatutoryinternal = "I";
                    }

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    upDivLocation_Load(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        //{
        //    try
        //    {
        //        treetoBind.Nodes.Clear();
        //        NameValueHierarchy branch = null;
        //        if (branchList.Count > 0)
        //        {
        //            branch = branchList[0];
        //        }

        //        treeTxtBox.Text = "Select Entity/Branch/Location";
        //        List<TreeNode> nodes = new List<TreeNode>();
        //        BindBranchesHierarchy(null, branch, nodes);
        //        foreach (TreeNode item in nodes)
        //        {
        //            treetoBind.Nodes.Add(item);
        //        }
        //        treetoBind.CollapseAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvErrorNoticePage.IsValid = false;
        //        cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        //{
        //    try
        //    {
        //        if (nvp != null)
        //        {
        //            foreach (var item in nvp.Children)
        //            {
        //                TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //                BindBranchesHierarchy(node, item, nodes);
        //                if (parent == null)
        //                {
        //                    nodes.Add(node);
        //                }
        //                else
        //                {
        //                    parent.ChildNodes.Add(node);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvErrorNoticePage.IsValid = false;
        //        cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        public void BindGrid()
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            int branchID = -1;
            int partyID = -1;
            int deptID = -1;
            string noticeStatus = string.Empty;
            string noticeType = string.Empty;
            var type = string.Empty;

            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                noticeStatus = ddlStatus.SelectedValue;
            }
            //if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            //{
            //    noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);
            //}

            if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
            {
                noticeType = ddlNoticeTypePage.SelectedValue;
            }

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
            {
                branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
            {
                partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
            {
                deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
            }

            var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

            List<ListItem> selectedItems = RLCSManagement.GetSelectedItems(lstBoxFileTags);
            var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

            //var DocList = RLCSManagement.GetAllDocumentListofNotice(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType, selectedFileTags);
            var DocList = RLCSManagement.GetInspectionNoticeList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, branchList, noticeStatus, noticeType);

            if (DocList.Count > 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            userAssignedBranchList = rlcslocationdetails.GetuserAssignedBranchList(userAssignedBranchList);
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                            if (assignedbranchIDs.Count > 0)
                                DocList = DocList.Where(Entry => assignedbranchIDs.Contains(Entry.CustomerBranchID)).ToList();

                        }
                    }
                }
            }

            if (DocList.Count > 0)
                DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

            string SortExpr = string.Empty;
            string CheckDirection = string.Empty;

            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                {
                    CheckDirection = Convert.ToString(ViewState["Direction"]);

                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (CheckDirection == "Ascending")
                    {
                        DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                    }
                    else
                    {
                        CheckDirection = "Descending";
                        DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                    }
                }
            }

            Session["TotalRows"] = null;
            if (DocList.Count > 0)
            {
                grdMyDocument.DataSource = DocList;
                Session["TotalRows"] = DocList.Count;
                grdMyDocument.DataBind();
            }
            else
            {
                grdMyDocument.DataSource = DocList;
                grdMyDocument.DataBind();
            }

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdMyDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ShowGridDetail();
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdMyDocument.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;
                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["Direction"] = "Ascending";
            ViewState["SortExpression"] = null;
            BindGrid();
            bindPageNumber();
            applyCSStoFileTag_ListItems();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }
        protected void grdMyDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                List<string> Doctypes = new List<string>();
                Doctypes.Add("N");
                Doctypes.Add("NR");
                Doctypes.Add("NT");
                List<string> Docfiles = new List<string>();

                if (e.CommandName == "Download")
                {
                    GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<tbl_LitigationFileData> CMPDocuments = new List<tbl_LitigationFileData>();
                    int flagcheckdoc = 0;
                    if (CMPDocuments != null)
                    {
                        using (ZipFile LitigationZip = new ZipFile())
                        {
                            CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(commandArg[0]), Doctypes);

                            if (CMPDocuments.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        flagcheckdoc = 1;
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("ddMMyyyy");
                                        if (file.EnType == "M")
                                        {
                                            LitigationZip.AddEntry("NoticeDocument" + "/" + Dates + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            LitigationZip.AddEntry("NoticeDocument" + "/" + Dates + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                                if (flagcheckdoc == 1)
                                {
                                    var zipMs = new MemoryStream();
                                    LitigationZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=NoticeDocument.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                                }
                                else {
                                    cvErrorNoticePage.IsValid = false;
                                    cvErrorNoticePage.ErrorMessage = "No Document available to Download.";
                                }
                            }
                            else
                            {
                                cvErrorNoticePage.IsValid = false;
                                cvErrorNoticePage.ErrorMessage = "No Document available to Download.";
                            }
                        }
                    }
                }
                else if (e.CommandName == "View")
                {
                    GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    DropDownList ddlDocumentFile = (DropDownList)grdMyDocument.Rows[RowIndex].FindControl("ddlDocumentFile");
                    
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    Session["NoticeInstanceID"] = commandArg[0];
                    List<tbl_LitigationFileData> CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(commandArg[0]), Doctypes);

                    if (CMPDocuments.Count > 0)
                    {
                        List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = Convert.ToInt64(commandArg[0]);
                            entityData.DocTypeInstanceID = Convert.ToInt64(commandArg[2]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptLitigationVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles/Litigation";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    LitigatinoDocPath = FileName;
                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    lblMessage.Text = "No Document available to preview";
                                    //cvErrorNoticePage.IsValid = false;
                                    //cvErrorNoticePage.ErrorMessage = "No Document available to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                    }
                }
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            applyCSStoFileTag_ListItems();
        }

        protected void rptLitigationVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                long NoticeInstanceID = Convert.ToInt64(Session["NoticeInstanceID"]);
                List<string> Doctypes = new List<string>();
                Doctypes.Add("N");
                Doctypes.Add("NR");
                Doctypes.Add("NT");

                var CheckType = string.Empty;
                
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<tbl_LitigationFileData> LitigationFileData = new List<tbl_LitigationFileData>();
                List<tbl_LitigationFileData> LitigationDocument = new List<tbl_LitigationFileData>();
                
                    LitigationDocument = LitigationDocumentManagement.GetDocuments(NoticeInstanceID, Doctypes);
                if (commandArgs[1].Equals("1.0"))
                {
                    LitigationFileData = LitigationDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (LitigationFileData.Count <= 0)
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    LitigationFileData = LitigationDocument.Where(entry => entry.FileName == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("View"))
                {
                    if (LitigationFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in LitigationFileData)
                        {
                            if (Path.GetExtension(file.FileName).Trim().ToUpper() != ".ZIP")
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    i++;
                                    LitigatinoDocPath = FileName;

                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                }
                            }
                            else
                            {
                                lblMessage.Text = "Zip file can not be preview, Please use download option";
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                break;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptLitigationVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage;
            if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedItem.ToString()))
            {
                chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            }
            else
            {
                chkSelectedPage = 1;
            }
            grdMyDocument.PageIndex = chkSelectedPage - 1;
            grdMyDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
            ShowGridDetail();
            applyCSStoFileTag_ListItems();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }

        protected void grdMyDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void ClearDropDownValue()
        {
            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                ddlStatus.Items.Clear();
            }

            if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
            {

                ddlNoticeTypePage.Items.Clear();
            }

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
            {
                tvFilterLocation.Nodes.Clear();
            }

            if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
            {
                ddlPartyPage.Items.Clear();
            }

            if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
            {
                ddlDeptPage.Items.Clear();
            }
        }
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                BindFileTags();
                bindPageNumber();
                applyCSStoFileTag_ListItems();
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                // tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdMyDocument_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int caseStatus = -1;
                string caseType = string.Empty;
                var type = string.Empty;

                grdMyDocument.DataSource = null;
                grdMyDocument.DataBind();
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    caseStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    caseType = ddlNoticeTypePage.SelectedItem.Text;
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                List<ListItem> selectedItems = RLCSManagement.GetSelectedItems(lstBoxFileTags);
                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                var DocList = RLCSManagement.GetAllDocumentListofNotice(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType, selectedFileTags);
                DocList = rlcslocationdetails.GetuserAssignedBranchList(DocList);
                if (DocList.Count > 0)
                    DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdMyDocument.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdMyDocument.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (DocList.Count > 0)
                {
                    grdMyDocument.DataSource = DocList;
                    Session["TotalRows"] = DocList.Count;
                    grdMyDocument.DataBind();
                }
                else
                {
                    grdMyDocument.DataSource = DocList;
                    grdMyDocument.DataBind();
                }

                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdMyDocument_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();

                List<ListItem> lstSelectedTags = new List<ListItem>();

                foreach (ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = RLCSManagement.ReArrange_FileTags(lstBoxFileTags);

                lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindFileTags()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                List<sp_LitigationNoticeFileTag_Result> lstTags = new List<sp_LitigationNoticeFileTag_Result>();
                List<string> lstTagsTask = new List<string>();
                lstTags = RLCSManagement.GetDistinctFileTagsNoticeMyDocument(customerID, 0, AuthenticationHelper.UserID, AuthenticationHelper.Role);
                lstBoxFileTags.Items.Clear();
                if (lstTags.Count > 0)
                {
                    foreach (var item in lstTags)
                    {
                        lstBoxFileTags.Items.Add(new ListItem(item.FileTag, item.FileTag));
                    }
                }
                if (lstTags.Count > 0)
                    outerDivFileTags.Visible = true;
                else
                    outerDivFileTags.Visible = false;
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void applyCSStoFileTag_ListItems()
        {
            foreach (ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "item label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "item label label-info");
            }
        }
        private void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }
        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            ddlStatus.ClearSelection();
            ddlNoticeTypePage.ClearSelection();
            ddlPartyPage.ClearSelection();
            ddlDeptPage.ClearSelection();
            ddlNoticeTypePage.ClearSelection();
            ddlPartyPage.ClearSelection();
            ClearTreeViewSelection(tvFilterLocation);
            applyCSStoFileTag_ListItems();
        }
    }
}