﻿<%@ Page Title="Generate Document :: My Workspace" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_DocumentGeneration.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_DocumentGeneration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
        $(document).ready(function () {            
            fhead('My Workspace/Generate Documents');
        });
         
        $(document).on("click", function (event) {
        debugger;
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tvFilterLocation.ClientID %>') {
                $('<%= tvFilterLocation.ClientID %>').unbind('click');

                $('<%= tvFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }


            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation_Return.ClientID %>') > -1) {
                    $("#divFilterLocation_Return").show();
                } else {
                    $("#divFilterLocation_Return").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation_Return.ClientID %>') > -1) {
                $("#divFilterLocation_Return").show();
            } else if (event.target.id != '<%= tbxFilterLocation_Return.ClientID %>') {
                $("#divFilterLocation_Return").hide();
            } else if (event.target.id == '<%= tbxFilterLocation_Return.ClientID %>') {
                $("#divFilterLocation_Return").show();
            } else if (event.target.id == '<%= tvFilterLocation_Return.ClientID %>') {
                $('<%= tvFilterLocation_Return.ClientID %>').unbind('click');

                $('<%= tvFilterLocation_Return.ClientID %>').click(function () {
                    $("#divFilterLocation_Return").toggle("blind", null, 500, function () { });
                });
            }
        });

function initializeJQueryUI(textBoxID, divID) {
    alert(textBoxID + "-" + divID);
    $("#" + textBoxID).unbind('click');

    $("#" + textBoxID).click(function () {
        $("#" + divID).toggle("blind", null, 500, function () { });
    });
}

function returnAdvanceSearch() {
    $("#returnType").toggle();
    $("#returnAct").toggle();
    $("#returnFrequency").toggle();
    $("#returnPeriod").toggle();
    $("#returnClearfix").toggle();
};

function hideDivBranch() {
    $('#divFilterLocation').hide("blind", null, 0, function () { });
    $('#divFilterLocation_Return').hide("blind", null, 0, function () { });
}

    function disableCombobox() {

        $(".custom-combobox").attr('disabled', 'disabled');
    }

    function checkAllReturns(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkReturn") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }

    function checkAllPeriods(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }

    function checkAllCodes(cb) {
        debugger;
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkReturnCode") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }

    $(document).ready(function () {
        $("#divFilterLocation").hide();
        $("#btnRepeater").click(function () {
            $("#dvDept").toggle("blind", null, 500, function () { });
            $("#dvPeriod").toggle("blind", null, 500, function () { });
            $("#dvCode").toggle("blind", null, 500, function () { });
        });
    });
 
    function initializeJQueryUIDeptDDL() {
        debugger;
        $("#<%= txtReturnList.ClientID %>").unbind('click');
        $("#<%= txtReturnList.ClientID %>").click(function () {
            $("#dvDept").toggle("blind", null, 100, function () { });
        });

        $("#<%= txtPeriodList.ClientID %>").unbind('click');
        $("#<%= txtPeriodList.ClientID %>").click(function () {
            $("#dvPeriod").toggle("blind", null, 100, function () { });
        });

          $("#<%= txtCodeList.ClientID %>").click(function () {
              $("#dvCode").toggle("blind", null, 100, function () { });
        });
    }

    function UncheckHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkReturn']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkReturn']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='DepartmentSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {
            rowCheckBoxHeader[0].checked = false;
        }
        //$('#BodyContent_udcInputForm_txtReturnList').val(rowCheckBoxSelected.length + ' ' + 'item selected');
    }

    function UncheckPeriodHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkPeriod']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkPeriod']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='PeriodSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {
            rowCheckBoxHeader[0].checked = false;
        }
        //$('#BodyContent_udcInputForm_txtReturnList').val(rowCheckBoxSelected.length + ' ' + 'item selected');
    }
    function UncheckCodeHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkCode']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkReturnCode']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='CodeSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {
            rowCheckBoxHeader[0].checked = false;
        }
        //$('#BodyContent_udcInputForm_txtReturnList').val(rowCheckBoxSelected.length + ' ' + 'item selected');
    }

    </script>

    <style>
        .alert {
            margin-bottom: 0px;
        }
        .chosen-results {
            max-height: 250px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocGenPopup" runat="server" OnLoad="upDocGenPopup_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget" style="padding: 5px 10px 10px;">
                <div class="panel-body" style="padding: 0px;">
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                        <asp:ValidationSummary ID="vsDocGen1" runat="server" class="alert alert-block alert-success fade in" ValidationGroup="success" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="DocGenValidationGroup" Display="None" />
                         <asp:CustomValidator ID="cvDuplicateEntrysucess" runat="server" EnableClientScript="False"
                            ValidationGroup="success" Display="None" />
                    </div>

                    <div class="col-md-12 colpadding0">
                        <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 15px 0px;">
                            <ul class="nav nav-tabs">
                                <li class="active" id="liRegister" runat="server">
                                    <asp:LinkButton ID="lnkTabRegister" OnClick="lnkTabRegister_Click" runat="server">Register</asp:LinkButton>
                                </li>
                                <li class="" id="liChallan" runat="server">
                                    <asp:LinkButton ID="lnkTabChallan" OnClick="lnkTabChallan_Click" runat="server">Challan</asp:LinkButton>
                                </li>
                                <li class="" id="liReturn" runat="server">
                                    <asp:LinkButton ID="lnkTabReturn" OnClick="lnkTabReturn_Click" runat="server">Return</asp:LinkButton>
                                </li>
                            </ul>
                        </header>
                    </div>

                    <div class="clearfix"></div>

                    <asp:MultiView ID="MainView" runat="server">
                        <asp:View ID="RegisterView" runat="server">
                            <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                                <div class="col-md-4 colpadding0" style="width: 30%">
                                    <label for="tbxFilterLocation" class="filter-label">Branch</label>
                                    <asp:TextBox runat="server" AutoComplete="off" ID="tbxFilterLocation"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 95%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: 90%; margin-top: -20px;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="105%" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <label for="ddlMonthRegister" class="filter-label">Month</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlMonthRegister" class="form-control" Width="90%" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="January" Value="01" />
                                        <asp:ListItem Text="February" Value="02" />
                                        <asp:ListItem Text="March" Value="03" />
                                        <asp:ListItem Text="April" Value="04" />
                                        <asp:ListItem Text="May" Value="05" />
                                        <asp:ListItem Text="June" Value="06" />
                                        <asp:ListItem Text="July" Value="07" />
                                        <asp:ListItem Text="August" Value="08" />
                                        <asp:ListItem Text="September" Value="09" />
                                        <asp:ListItem Text="October" Value="10" />
                                        <asp:ListItem Text="November" Value="11" />
                                        <asp:ListItem Text="December" Value="12" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <label for="ddlYearRegister" class="filter-label">Year</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlYearRegister" class="form-control" Width="90%" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="2020" Value="2020" />
                                        <asp:ListItem Text="2019" Value="2019" />
                                        <asp:ListItem Text="2018" Value="2018" />
                                        <asp:ListItem Text="2017" Value="2017" />
                                        <asp:ListItem Text="2016" Value="2016" />
                                        <asp:ListItem Text="2015" Value="2015" />
                                        <asp:ListItem Text="2014" Value="2014" />
                                        <asp:ListItem Text="2013" Value="2013" />
                                        <asp:ListItem Text="2012" Value="2012" />
                                        <asp:ListItem Text="2011" Value="2011" />
                                        <asp:ListItem Text="2010" Value="2010" />
                                        <asp:ListItem Text="2009" Value="2009" />
                                        <asp:ListItem Text="2008" Value="2008" />
                                        <asp:ListItem Text="2007" Value="2007" />
                                        <asp:ListItem Text="2006" Value="2006" />
                                        <asp:ListItem Text="2005" Value="2005" />
                                        <asp:ListItem Text="2004" Value="2004" />
                                        <asp:ListItem Text="2003" Value="2003" />
                                        <asp:ListItem Text="2002" Value="2002" />
                                        <asp:ListItem Text="2001" Value="2001" />
                                        <asp:ListItem Text="2000" Value="2000" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0" style="width: 13%">
                                    <label for="ddlRegType" class="filter-label">Type</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlRegType" class="form-control" Width="90%"
                                        OnSelectedIndexChanged="ddlRegType_SelectedIndexChanged" AutoPostBack="true" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="Consolidated" Value="C" Selected="True" />
                                        <asp:ListItem Text="Individual" Value="IN" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0" style="width: 25%">
                                    <label for="ddlRegisterName" class="filter-label">Register</label>
                                    <asp:DropDownListChosen ID="ddlRegisterName" runat="server" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 6%">
                                    <label for="btnFilter" class="hidden-label">Apply</label>
                                    <asp:Button ID="btnGenerateRegister" CausesValidation="false" class="btn btn-primary" runat="server"
                                        Text="Generate" OnClick="btnGenerateRegister_Click" />
                                </div>
                            </div>
                        </asp:View>

                        <asp:View ID="ChallanView" runat="server">
                            <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <label for="ddlChallanType" class="filter-label">Type</label>
                                    <asp:DropDownListChosen ID="ddlChallanType" runat="server" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                        OnSelectedIndexChanged="ddlChallanType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="PF Working" Value="PF_Working_New" />
                                        <asp:ListItem Text="ESI" Value="ESI" />
                                        <asp:ListItem Text="PT" Value="PT" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0" style="width: 25%">
                                    <label for="ddlEntityClient" class="filter-label">Entity/Client</label>
                                    <asp:DropDownListChosen ID="ddlEntityClient" runat="server" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                        OnSelectedIndexChanged="ddlChallanType_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <label for="ddlMonthChallan" class="filter-label">Month</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlMonthChallan" class="form-control" Width="90%" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="January" Value="01" />
                                        <asp:ListItem Text="February" Value="02" />
                                        <asp:ListItem Text="March" Value="03" />
                                        <asp:ListItem Text="April" Value="04" />
                                        <asp:ListItem Text="May" Value="05" />
                                        <asp:ListItem Text="June" Value="06" />
                                        <asp:ListItem Text="July" Value="07" />
                                        <asp:ListItem Text="August" Value="08" />
                                        <asp:ListItem Text="September" Value="09" />
                                        <asp:ListItem Text="October" Value="10" />
                                        <asp:ListItem Text="November" Value="11" />
                                        <asp:ListItem Text="December" Value="12" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <label for="ddlYearChallan" class="filter-label">Year</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlYearChallan" class="form-control" Width="90%" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="2020" Value="2020" />
                                        <asp:ListItem Text="2019" Value="2019" />
                                        <asp:ListItem Text="2018" Value="2018" />
                                        <asp:ListItem Text="2017" Value="2017" />
                                        <asp:ListItem Text="2016" Value="2016" />
                                        <asp:ListItem Text="2015" Value="2015" />
                                        <asp:ListItem Text="2014" Value="2014" />
                                        <asp:ListItem Text="2013" Value="2013" />
                                        <asp:ListItem Text="2012" Value="2012" />
                                        <asp:ListItem Text="2011" Value="2011" />
                                        <asp:ListItem Text="2010" Value="2010" />
                                        <asp:ListItem Text="2009" Value="2009" />
                                        <asp:ListItem Text="2008" Value="2008" />
                                        <asp:ListItem Text="2007" Value="2007" />
                                        <asp:ListItem Text="2006" Value="2006" />
                                        <asp:ListItem Text="2005" Value="2005" />
                                        <asp:ListItem Text="2004" Value="2004" />
                                        <asp:ListItem Text="2003" Value="2003" />
                                        <asp:ListItem Text="2002" Value="2002" />
                                        <asp:ListItem Text="2001" Value="2001" />
                                        <asp:ListItem Text="2000" Value="2000" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0" style="width: 25%">
                                    <label for="ddlChallanCode" class="filter-label">Code</label>
                                    <asp:DropDownListChosen ID="ddlChallanCode" runat="server" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 6%">
                                    <label for="btnFilter" class="hidden-label">Apply</label>
                                    <asp:Button ID="btnGenerateChallan" CausesValidation="false" class="btn btn-primary" runat="server"
                                        Text="Generate" OnClick="btnGenerateChallan_Click" />
                                </div>
                            </div>
                        </asp:View>

                        <asp:View ID="ReturnView" runat="server">
                            <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                                 <div class="col-md-1 colpadding0" style="width: 13%">
                                    <label for="ddlChallanType" class="filter-label">Type</label>
                                    <asp:DropDownListChosen ID="ddlReturnChallanType" runat="server" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                        OnSelectedIndexChanged="ddlReturnChallanType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="-Select-" Value="-1" />
                                            <asp:ListItem Text="PF Working" Value="EPF" />
                                            <asp:ListItem Text="ESI" Value="ESI" />
<%--                                            <asp:ListItem Text="PT" Value="PT" />--%>
                                            <asp:ListItem Text="Other" Value="Other" />
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0" id="ReturnBranch" runat="server">
                                    <label for="tbxFilterLocation" class="filter-label">Branch</label>
                                    <asp:TextBox runat="server" AutoComplete="off" ID="tbxFilterLocation_Return"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 95%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: 90%; margin-top: -20px;" id="divFilterLocation_Return">
                                        <asp:TreeView runat="server" ID="tvFilterLocation_Return" SelectedNodeStyle-Font-Bold="true" Width="105%" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocation_Return_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>

                                 <div class="col-md-3 colpadding0" style="width: 25%" id="ReturnEntity" runat="server">
                                    <label for="ddlEntityClient" class="filter-label">Entity/Client</label>
                                    <asp:DropDownListChosen ID="ddlreturnEntity" runat="server" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select"
                                        OnSelectedIndexChanged="ddlreturnEntity_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <label for="ddlFrequency" class="filter-label">Frequency</label>
                                    <asp:DropDownListChosen ID="ddlFrequency" runat="server" Width="90%" class="form-control"  AllowSingleDeselect="false" DataPlaceHolder="Select"
                                        OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownListChosen>
                                    
                                </div>

                                <div class="col-md-1 colpadding0">
                                    <label for="ddlReturnYear" class="filter-label">Year</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlYearReturn" class="form-control" Width="90%" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="2020" Value="2020" />
                                        <asp:ListItem Text="2019" Value="2019" />
                                        <asp:ListItem Text="2018" Value="2018" />
                                        <asp:ListItem Text="2017" Value="2017" />
                                        <asp:ListItem Text="2016" Value="2016" />
                                        <asp:ListItem Text="2015" Value="2015" />
                                        <asp:ListItem Text="2014" Value="2014" />
                                        <asp:ListItem Text="2013" Value="2013" />
                                        <asp:ListItem Text="2012" Value="2012" />
                                        <asp:ListItem Text="2011" Value="2011" />
                                        <asp:ListItem Text="2010" Value="2010" />
                                        <asp:ListItem Text="2009" Value="2009" />                                        
                                        <asp:ListItem Text="2008" Value="2008" />
                                        <asp:ListItem Text="2007" Value="2007" />
                                        <asp:ListItem Text="2006" Value="2006" />
                                        <asp:ListItem Text="2005" Value="2005" />
                                        <asp:ListItem Text="2004" Value="2004" />
                                        <asp:ListItem Text="2003" Value="2003" />
                                        <asp:ListItem Text="2002" Value="2002" />
                                        <asp:ListItem Text="2001" Value="2001" />
                                        <asp:ListItem Text="2000" Value="2000" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <label for="ddlPeriod" class="filter-label">Period</label>
                                    <asp:TextBox runat="server" ID="txtPeriodList" Style="padding: 0px; margin: 0px; height: 32px; width: 90%;" CssClass="txtbox form-control" />
                                    <div style="display:none; position:absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px;width: 89%;" id="dvPeriod" class="dvDeptHideshow form-control">
                                        <asp:Repeater ID="rptPeriodList" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width: 100px;" colspan="2">
                                                            <asp:CheckBox ID="PeriodSelectAll" Text="Select All" runat="server" onclick="checkAllPeriods(this)" />
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkPeriod" runat="server" onclick="UncheckPeriodHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 118px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblPeriodID" runat="server" style="display:none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <label for="ddlReturnList" class="filter-label"">Return</label>
                                    <asp:TextBox runat="server" ID="txtReturnList" Style="padding: 0px; margin: 0px; height: 32px; width: 90%;" CssClass="txtbox form-control" />
                                    <div style="display:none; position:absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width:89%;" id="dvDept" class="dvDeptHideshow form-control">
                                    
                                        <asp:Repeater ID="rptReturnList" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width: 100px;" colspan="2">
                                                            <asp:CheckBox ID="DepartmentSelectAll" Text="Select All" runat="server" onclick="checkAllReturns(this)" />
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkReturn" runat="server" onclick="UncheckHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 193px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblReturnID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblReturnName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <div class="col-md-2 colpadding0" id="DivCode" runat="server">
                                    <label for="ddlReturnChekCode" class="filter-label"">Code</label>
                                    <asp:TextBox runat="server" ID="txtCodeList" Style="padding: 0px; margin: 0px; height: 32px; width: 90%;" CssClass="txtbox form-control" />
                                    <div style="display:none; position:absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width:89%;" id="dvCode" class="dvDeptHideshow form-control">
                                    
                                        <asp:Repeater ID="rptCode" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width: 100px;" colspan="2">
                                                            <asp:CheckBox ID="CodeSelectAll" Text="Select All" runat="server" onclick="checkAllCodes(this)" />
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkReturnCode" runat="server" onclick="UncheckCodeHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 193px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblcode" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblcodeName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="col-md-1 colpadding0">
                                    <label for="btnFilter" class="hidden-label">Apply</label>
                                    <asp:Button ID="btnGenerateReturn" CausesValidation="false" class="btn btn-primary" runat="server"
                                        Text="Generate" OnClick="btnGenerateReturn_Click" />
                                </div>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
