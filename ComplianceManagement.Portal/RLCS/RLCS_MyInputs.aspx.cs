﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyInputs : System.Web.UI.Page
    {
        protected static string custID_Encrypted;
        protected static string userID_Encrypted;
        public int JSONID;
        public string ComplianceID;
        public string ComplianceType;
        public string ReturnRegisterChallanID;
        public string MonthId;
        public string Year;
        public string InputID;
        public string BranchID;
        public string ClientId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                string custID = Convert.ToString(AuthenticationHelper.CustomerID);
                string userID = Convert.ToString(AuthenticationHelper.UserID);


              

                if (!string.IsNullOrEmpty(custID) && !string.IsNullOrEmpty(userID))
                {
                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                    custID_Encrypted = custID;
                    userID_Encrypted = userID;

                    //custID_Encrypted = CryptographyHandler.encrypt(custID.Trim(), TLConnectKey);
                    //custID_Encrypted = CryptographyHandler.encrypt_RLCS_PHP(custID.Trim());
                    //userID_Encrypted = CryptographyHandler.encrypt(userID.Trim(), TLConnectKey);
                }
                //if (Request.QueryString["ComplianceID"] != null && Request.QueryString["JSONID"] != null)
                if(Request.QueryString["JSONID"] != null)
                {
                    JSONID = Convert.ToInt32(Request.QueryString["JSONID"]);
                    //ComplianceID = Convert.ToString(Request.QueryString["ComplianceID"]);
                    ComplianceType = Convert.ToString(Request.QueryString["ComplianceType"]);
                    ///ReturnRegisterChallanID = Convert.ToString(Request.QueryString["ReturnRegisterChallanID"]);
                    MonthId = Convert.ToString(Request.QueryString["MonthId"]);
                    Year = Convert.ToString(Request.QueryString["Year"]);
                    //InputID = Convert.ToString(Request.QueryString["InputID"]);
                    //BranchID = Convert.ToString(Request.QueryString["BranchID"]);
                    ClientId = Convert.ToString(Request.QueryString["ClientID"]);
                }
            }
            else
            {
                //added by rahul on 12 June 2018 Url Sequrity
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }
    }
}