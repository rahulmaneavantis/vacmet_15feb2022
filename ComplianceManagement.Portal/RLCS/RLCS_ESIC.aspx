﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_ESIC.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_ESIC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
	<link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

	<script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
	<script type="text/javascript" src="../Newjs/jszip.min.js"></script>
	<link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

	<style>
		.astrick:after {
			content: " *";
			color: red;
			display: inline;
		}

		.col-md-4 {
			width: 30%;
		}

		.col-md-3 {
			width: 24%;
		}

		.col-md-1 {
			width: 10.333333%;
		}

		.col-md-2 {
			/* width: 16.666666666666664%; */
		}
		#grid th.k-header:nth-child(10) { text-align:center; }
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			fhead('My Documents/ Employee ESIC Cards');
			$("#Applyfiltermain").addClass('disabled');
			function pickerfun() {
				applyButtonEnable($("#ddlIlstforClient").val(), $("#ddl_Subcode").val(), $("#Startdatepicker").val());
			}
			var start = $("#Startdatepicker").kendoDatePicker({
				start: "year",
				// defines when the calendar should return date
				depth: "year",
				// display month and year in the input
				format: "MMMM yyyy",
				// specifies that DateInput is used for masking the input element
				value: new Date(),
				dateInput: true,
				width: 150,
				change: function () {
					applyButtonEnable($("#ddlIlstforClient").val(), $("#ddl_Subcode").val(), $("#Startdatepicker").val());
				},
			}).data("kendoDatePicker");

			BindFirstGrid();
			BindEntities();
		});

		//Action button edit code in kendo grid
		$(document).on("click", "#grid tbody tr .k-grid-edit1", function (e) {
			debugger;
			var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
			DownloadDocuments(item.ClientId, item.SubCode, item.EmpId, item.DOJ, item.IPNumber, item.RequestDate);
			return false;
		});

		function DownloadDocuments(ClientId, SubCode, empid, DOJ, IpNumber, RequestDate) {
			debugger;
			$('#DownloadViews').attr('src', "RLCS_ESIC.aspx?ClientID=" + ClientId + "&SubCode=" + SubCode + "&EmployeeID=" + empid + "&DOJ=" + DOJ + "&ESICNumber=" + IpNumber + "&RequestDate=" + RequestDate + "&Type=D");
		}
		function excelExport(e) {
			$('#DownloadViews').attr('src', "RLCS_ESIC.aspx?ClientID=" + $("#ddlIlstforClient").val() + "&SubCode=" + $("#ddl_Subcode").val() + "&FromDate=" + $("#Startdatepicker").val() + "&Todate=" + $("#Startdatepicker").val() + "&Type=E");
			return false;
		}
		function ClearAllFilterMain(e) {
			e.preventDefault();
			$("#ddlIlstforClient").data("kendoDropDownList").select(0);
			$("#Applyfiltermain").addClass('disabled');
			$("#ddl_Subcode").data("kendoDropDownList").select(0);
			var start = $("#Startdatepicker").kendoDatePicker({ dateInput: true }).data("kendoDatePicker");
			start.value(null);
			start.trigger("change");
			var grid = $("#grid").data("kendoGrid");
			var emptyDataSource = new kendo.data.DataSource({
				data: []
			});
			grid.setDataSource(emptyDataSource);
			$('#ClearfilterMain').css('display', 'none');
		}

		function ApplyFilter(e) {
			debugger;
			var datepicker = $("#Startdatepicker").data("kendoDatePicker").value();
			var month = (datepicker.getMonth() + 1).toString();
			if (month.length == 1) {
				month = '0' + month;
			}
			var year = datepicker.getFullYear().toString();
			var datevalue = "01" + "-" + month + "-" + year;
			e.preventDefault();
			var dataSource = new kendo.data.DataSource({
				transport: {
					read: {
						url: '<% =RLCSAPIURL%>Masters/GetESICDetails?EntityId=' + $("#ddlIlstforClient").val() + '&SubCode=' + $("#ddl_Subcode").val() + '&FromDate=' + datevalue + '&ToDate=' + datevalue,
						dataType: "json",
					}
				},
				pageSize: 10,
				schema: {
					data: function (response) {
						return response;
					},
					total: function (response) {
						return response.length;

					}
				},
			});

			var grid = $("#grid").data("kendoGrid");
			grid.setDataSource(dataSource);
		}
		function applyButtonEnable(entiry, subcode, fromdate) {
			debugger;
			if (entiry.val != "" && subcode != "" && fromdate != "") {
				$("#Applyfiltermain").removeClass('disabled');
			}
			else { $("#Applyfiltermain").addClass('disabled'); }
		}

		function BindFirstGrid() {

			$("#grid").show();
			var dataSource = null;

			var grid = $("#grid").kendoGrid({
				dataSource: dataSource,
				sortable: true,
				filterable: true,
				columnMenu: true,
				//pageable: {
				//    refresh: true
				//},
				pageable: {
					pageSizes: ['All', 5, 10, 20],
					refresh: true,
					change: function (e) {
						$('#chkAll_Main').removeAttr('checked');
						//$('#dvbtndownloadDocumentMain').css('display', 'none');
					},
					pageSizes: true,
					buttonCount: 3
				},
				batch: true,
				pageSize: 10,
				reorderable: true,
				resizable: true,
				multi: true,
				selectable: true,
				columns: [
					{
						template: "<input name='sel_chkbx_Main' id='sel_chkbx_Main' type='checkbox' value='#=ClientId#-#=SubCode#-#=DOJ#-#=EmpId#-#=IPNumber#-#=RequestDate#' >",
						filterable: false, sortable: false,
						headerTemplate: "<input type='checkbox' id='chkAll_Main'/>",
						width: "4%;"//, lock: true
					},
					{
						hidden: true,
						field: "RequestDate", title: 'RequestDate',
					},
					{
						hidden: true,
						field: "ClientId", title: 'ClientID',
					},
					{
						hidden: true,
						field: "SubCode", title: 'SubCode',
					},
					{
						field: "EmpId", title: ' Employee Code',
						width: "18.7%;",
						attributes: {
							style: 'white-space: nowrap;'

						}, filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "EmpName", title: 'Employee Name',
						width: "28.7%",
						attributes: {
							style: 'white-space: nowrap;'
						},
						filterable: {
							multi: true,
							extra: false,
							search: true,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "DOJ", title: 'Employee DOJ', width: "18.7%", filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						field: "IPNumber", title: 'ESIC Number', width: "18.7%",
						attributes: {
							style: 'white-space: nowrap '
						},
						filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{

						field: "CardGeneratedDate", title: 'Card Generated Date', width: "22.7%",
						attributes: {
							style: 'white-space: nowrap '
						},
						filterable: {
							extra: false,
							operators: {
								string: {
									eq: "Is equal to",
									neq: "Is not equal to",
									contains: "Contains"
								}
							}
						}
					},
					{
						command: [
							{ name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
							{ name: "edit3", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
						], title: "Action", lock: true, width: "10.7%;",
					} 
				],
				dataBound: function () {
					var rows = this.items();
					$(rows).each(function () {
						var index = $(this).index() + 1
							+ ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
						var rowLabel = $(this).find(".row-number");
						$(rowLabel).html(index);
					});
				}
			});


			$("#grid").kendoTooltip({
				filter: "td:nth-child(5)", //this filter selects the second column's cells
				position: "down",
				content: function (e) {
					var content = e.target.context.textContent;
					return content;
				}
			}).data("kendoTooltip");

			$("#grid").kendoTooltip({
				filter: ".k-grid-edit1",
				content: function (e) {
					return "Download";
				}
			});

			$("#grid").kendoTooltip({
				filter: ".k-grid-edit2",
				content: function (e) {
					return "View";
				}
			});

			//Code for tooltip in grid Data
			$("#grid").kendoTooltip({
				filter: "td", //this filter selects the second column's cells
				position: "down",
				content: function (e) {
					var content = e.target.context.textContent;
					if (content != "") {
						return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
					}
					else
						e.preventDefault();
				}
			}).data("kendoTooltip");


		}
		$(document).on("click", "#chkAll_Main", function (e) {


			if ($('input[id=chkAll_Main]').prop('checked')) {
				$('#dvbtndownloadDocumentMain').css('visibility', 'visible');
			//	$("#dvbtndownloadDocumentMain").css('display', 'inline');
				$('input[name="sel_chkbx_Main"]').each(function (i, e) {
					e.click();
				});
			}
			else {
				$('#dvbtndownloadDocumentMain').css('visibility', 'hidden');
			//	$("#dvbtndownloadDocumentMain").css('display', 'none');
				$('input[name="sel_chkbx_Main"]').attr("checked", false);
			}

			return true;
		});

		$(document).on("click", "#sel_chkbx_Main", function (e) {
			if (($('input[name="sel_chkbx_Main"]:checked').length) > 1) {
				$('#dvbtndownloadDocumentMain').css('visibility', 'visible');
				//$("#dvbtndownloadDocumentMain").css('display', 'inline');
			}
			else {
				$('#dvbtndownloadDocumentMain').css('visibility', 'hidden');
				
				//$("#dvbtndownloadDocumentMain").css('display', 'none');
			}

		});
		$("#grid").kendoTooltip({
			filter: ".k-grid-edit3",
			content: function (e) {
				return "View";
			}
		});

		$(document).on("click", "#grid tbody tr .ob-overview", function (e) {
			debugger;
			var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
			ViewDocuments(item.ClientId, item.SubCode, item.DOJ, item.EmpId, item.IPNumber, item.RequestDate);
			return true;
		});

		function ViewDocuments(ClientId, SubCode, DOJ, EmpId, IPNumber, RequestDate) {
			$('#divViewDocument').modal('show');
			$('.modal-dialog').css('width', '90%');
			$('#OverViews').attr('src', "RLCS_ESICDocumentOverview.aspx?ClientId=" + ClientId + "&SubCode=" + SubCode + "&DOJ=" + DOJ + "&EmployeeID=" + EmpId + "&ESICNumber=" + IPNumber + "&RequestDate=" + RequestDate);
		}

		function BindEntities() {

			$("#ddlIlstforClient").width(265).kendoDropDownList({
				dataTextField: "AVACOM_BranchName",
				dataValueField: "CM_ClientID",
				optionLabel: "Select Entity",
				change: function (e) {
					$('#ClearfilterMain').css('display', 'block');
					applyButtonEnable($("#ddlIlstforClient").val(), $("#ddl_Subcode").val(), $("#Startdatepicker").val());
					var values = this.value();
					if (values != "" && values != null) {
						BindsubCode();
					}
					else {
						//$("#grid").data("kendoGrid").dataSource.filter({});
					}
				},
				dataSource: {
					transport: {
						read: {
							url: "<% =avacomAPIUrl%>GetAssignedEntities?customerID=<% =CId%>&userID=<% =UId%>&profileID=<% =ProfileID%>",
							beforeSend: function (request) {
								request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
								request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
								request.setRequestHeader('Content-Type', 'application/json');
							},
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							debugger;
							if (response.Result != null && response.Result != undefined) {
								console.log($("#ddlIlstforClient").val());
								BindsubCode();
							}
							return response.Result;
						}
					}
				}
			});
		}
		function BindsubCode() {
			debugger;
			var entityval = $("#ddlIlstforClient").val();
			$("#ddl_Subcode").width(200).kendoDropDownList({
				dataTextField: "Text",
				dataValueField: "Value",
				optionLabel: "Select Code",
				change: function (e) {
					$('#ClearfilterMain').css('display', 'block');
					applyButtonEnable($("#ddlIlstforClient").val(), $("#ddl_Subcode").val(), $("#Startdatepicker").val());
					var values = this.value();
					if (values != "" && values != null) {
					}
					else {
						//$("#grid").data("kendoGrid").dataSource.filter({});
					}
				},
				dataSource: {
					transport: {
						read: {
							url: "<% =RLCSAPIURL%>Masters/GetSubCodesBasedonEntity?EntityId=" + entityval,
							dataType: 'json',
						}
					},
					schema: {
						data: function (response) {
							debugger;
							return response;
						}
					}
				}
			});
		}

		function selectedDocument(e) {
			e.preventDefault();

			if (($('input[name="sel_chkbx_Main"]:checked').length) == 0) {
				return;
			}
			var checkboxlist = [];

			if (($('input[name="sel_chkbx_Main"]:checked').length) > 0) {
				$('input[name="sel_chkbx_Main"]').each(function (i, e) {
					if ($(e).is(':checked')) {
						checkboxlist.push(e.value);
					}
				});

				$('#DownloadViews').attr('src', "RLCS_ESICDownload.aspx?RLCSIds=" + checkboxlist.join(","));
			}
			console.log(checkboxlist.join(","));
			return false;
		}

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<div class="astrick" style="display: inline">
		<input id="ddlIlstforClient" />
	</div>

	<div class=" astrick" style="display: inline; padding-left: 15px">
		<input id="ddl_Subcode" />
	</div>
	<div class=" astrick" style="display: inline; padding-left: 15px">
		<input id="Startdatepicker" placeholder="Start Date" title="startdatepicker" />
	</div>

	<div style="display: inline">
			<button id="dvbtndownloadDocumentMain" class=" btn-primary btn" data-toggle='tooltip' data-placement='bottom' title='Download All ESIC cards' style="visibility:hidden; margin-left: 135px;"  onclick="selectedDocument(event)">Download</button>
	</div>
	<div style="display:inline">
		<button id="Applyfiltermain" class="btn-primary btn" style="height: 36px; font-size: 14px; font-weight: 400;margin-left: 15px;" onclick="ApplyFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
		<button id="btnExcel" class="btn btn-primary pull-right " data-placement="bottom" style="height: 36px;" onclick="return excelExport(event)"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
	</div>

	

	<div id="grid" style="border: none; margin-top: 10px;"></div>
	<div class="modal-body" style="display: none;">
		<iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
	</div>

	<div>
		<div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			<div class="modal-dialog" style="width: 70%;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body" style="height: 570px;">
						<div style="width: 100%;">
							<div class="col-md-12 colpadding0">
								<asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
								<fieldset style="height: 550px; width: 100%;">
									<iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
