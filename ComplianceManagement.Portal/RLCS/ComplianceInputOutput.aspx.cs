using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class ComplianceInputOutput : System.Web.UI.Page
	{
		protected static string AVACOM_RLCS_API_URL;
		protected static string avacomAPIUrl;
		protected static string TLConnectAPIUrl;
		protected static int customerID;
		protected static int BranchID;
		protected static string authKey;
		protected static string userProfileID_Encrypted;
		public int LoginUserID;
		public static List<int> branchList = new List<int>();
		public static List<int> locationList = new List<int>();
		public string ComplianceID_;
		public string ComplianceType_;
		public string ReturnRegisterChallanID_;
		public string MonthID_;
		public int Year_;
		public string ActID_;
		public string BranchID_;
		//public string ActGroup;
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!IsPostBack)
				{

					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
					LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);

					avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

					initialLoad();
					//BindDDLYear();
					//BindDDLYearOutput();



					if (Request.QueryString["ComplianceType"] != null && Request.QueryString["ComplianceID"] != null && Request.QueryString["BranchID"] != null && Request.QueryString["Month"] != null && Request.QueryString["Year"] != null)
					{
						ComplianceID_ = Convert.ToString(Request.QueryString["ComplianceID"]);
						ComplianceType_ = Convert.ToString(Request.QueryString["ComplianceType"]);
						MonthID_ = Convert.ToString(Request.QueryString["Month"]);
						Year_ = Convert.ToInt32(Request.QueryString["Year"]);
						ActID_ = Convert.ToString(Request.QueryString["ActID"]);
						BranchID_ = Convert.ToString(Request.QueryString["BranchID"]);
						hdnOutComplianceID.Value = Convert.ToString(ComplianceID_);
						hdnOutComplianceType.Value = Convert.ToString(ComplianceType_);
						hdnOutMonth.Value = Convert.ToString(MonthID_);
						hdnOutYear.Value = Convert.ToString(Year_);
						hdnOutBranchId.Value = Convert.ToString(BranchID_);
						hdnOutActID.Value = Convert.ToString(ActID_);
						fillAllOutTab();
						var clientID = "";
						var IsFromDashboard = false;
						if (Request.QueryString["IsFromDashboard"] != null)
						{
							IsFromDashboard = Convert.ToBoolean(Request.QueryString["IsFromDashboard"]);
						}
						using (ComplianceDBEntities entities = new ComplianceDBEntities())
						{
							var LstBranchCount = entities.sp_GetEntityCountFromBranchIds(BranchID_,"").ToList();
							clientID = LstBranchCount[0].ClientId;
							hdnClientId.Value = clientID;
						}
						if (IsFromDashboard == true)
						{
							ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "SubmitClickOnRedirection('" + customerID + "','" + LoginUserID + "','" + clientID + "');", true);
						}
						//btnOutSubmit_Click(null, null);
					}
				}
				ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
				ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
			}
			catch (Exception)
			{
				throw;
			}

		}

		protected void initialLoad()
		{
			using (ComplianceDBEntities entities = new ComplianceDBEntities())
			{
				var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
				if (userAssignedBranchList != null)
				{
					if (userAssignedBranchList.Count > 0)
					{
						List<int> assignedbranchIDs = new List<int>();
						assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

						BindLocationFilter(assignedbranchIDs);

						BindLocationFilterOutput(assignedbranchIDs);
						complianceRegisterChallan.Visible = false;

					}
				}
			}

		}
		public void BindDDLYear()
		{
			try
			{
				List<YearCalander> _objlst = new List<YearCalander>();
				int currentYear = Convert.ToInt32(DateTime.Now.Year);
				for (int i = currentYear; i >= currentYear - 4; i--)
				{
					YearCalander obj = new YearCalander();
					obj.Text = i.ToString();
					obj.Value = i.ToString();
					_objlst.Add(obj);
				}
				if (_objlst != null && _objlst.Count > 0)
				{
					ddlYear.DataTextField = "Text";
					ddlYear.DataValueField = "Value";
					ddlYear.DataSource = _objlst;
					ddlYear.DataBind();
					ddlYear.Items.Insert(0, new ListItem("Year", "0"));
					ddlYear.SelectedIndex = 0;
				}
			}
			catch (Exception ex)
			{ }

		}
		public class YearCalander
		{
			public string Text { get; set; }
			public string Value { get; set; }
		}

		public void BindDDLYearOutput()
		{
			try
			{
				List<YearCalander> _objlst = new List<YearCalander>();
				int currentYear = Convert.ToInt32(DateTime.Now.Year);
				for (int i = currentYear; i >= currentYear - 4; i--)
				{
					YearCalander obj = new YearCalander();
					obj.Text = i.ToString();
					obj.Value = i.ToString();
					_objlst.Add(obj);
				}
				if (_objlst != null && _objlst.Count > 0)
				{
					ddlOutYear.DataTextField = "Text";
					ddlOutYear.DataValueField = "Value";
					ddlOutYear.DataSource = _objlst;
					ddlOutYear.DataBind();
					ddlOutYear.Items.Insert(0, new ListItem("Year", "0"));
					ddlOutYear.SelectedIndex = 0;
				}
			}
			catch (Exception ex)
			{ }
		}
		public void fillAllOutTab()
		{
			try
			{
				if (!string.IsNullOrEmpty(hdnOutComplianceType.Value))
				{
					ddlOutputComplianceType.SelectedValue = Convert.ToString(hdnOutComplianceType.Value);
					ddlOutputComplianceType_SelectedIndexChanged(null, null);

					foreach (RepeaterItem item1 in rptAct.Items)
					{
						CheckBox chkAct = (CheckBox)item1.FindControl("chkAct");
						Label lblActID = (Label)item1.FindControl("lblActID");

						if (Convert.ToInt32(hdnOutActID.Value) == (Convert.ToInt32(lblActID.Text)))
						{
							chkAct.Checked = true;
						}
					}
					btnComplianceGet_Click(null, null);
					if (hdnOutComplianceType.Value == "Return")
					{
						ddlComplianceName.SelectedValue = hdnOutComplianceID.Value;
						ddlComplianceName_SelectedIndexChanged(null, null);
					}
					else
					{
						foreach (RepeaterItem Item in rptCompliance.Items)
						{
							CheckBox chkcompliance = (CheckBox)Item.FindControl("chkcompliance");
							Label lblcomplianceID = (Label)Item.FindControl("lblcomplianceID");
							if ((Convert.ToString(hdnOutComplianceID.Value)).Trim() == (Convert.ToString(lblcomplianceID.Text)).Trim())
							{
								chkcompliance.Checked = true;
							}
						}
					}
					btnFrequencyGet_Click(null, null);
					foreach (RepeaterItem item1 in rptPeriod1.Items)
					{
						CheckBox chkPeriod1 = (CheckBox)item1.FindControl("chkPeriod1");
						Label lblPeriodID1 = (Label)item1.FindControl("lblPeriodID1");

						if ((Convert.ToString(hdnOutMonth.Value)).Trim() == (Convert.ToString(lblPeriodID1.Text)).Trim())
						{
							chkPeriod1.Checked = true;
						}
					}
					ddlOutYear.SelectedValue = Convert.ToString(hdnOutYear.Value);
					foreach (TreeNode node in tvOutputBranch.Nodes)
					{
						string NodeText = node.Text;
						string NodeValue = node.Value;
						SelectNodeByValue(node, hdnOutBranchId.Value);
					}

				}
			}
			catch (Exception)
			{
			}
		}
		protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
		{
			foreach (TreeNode n in Node.ChildNodes)
			{
				if (n.Value == ValueToSelect)
				{
					n.Select();
					n.Checked = true;
				}
				else
				{
					SelectNodeByValue(n, ValueToSelect);
				}
			}
		}
		protected void upDocGenPopup_Load(object sender, EventArgs e)
		{
			try
			{
				ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
				ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationOutput');", tbxOutputBranch.ClientID), true);
				ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				cvDuplicateEntry.IsValid = false;
				cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
			}
		}
		private void BindPeriod_Return(string selectedFreq, string Type)
		{
			try
			{
				if (!string.IsNullOrEmpty(selectedFreq))
				{
					List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

					tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "12"));
					tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "01,02,03,04,05,06,07,08,09,10,11,12"));

					tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "06"));
					tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "12"));

					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "03"));
					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "06"));
					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "09"));
					tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "12"));

					tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

					tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

					tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "October", "10"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
					tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

					DataTable dtReturnPeriods = new DataTable();
					dtReturnPeriods.Columns.Add("ID");
					dtReturnPeriods.Columns.Add("Name");

					var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
					if (lst.Count > 0)
					{
						foreach (var item in lst)
						{
							DataRow drReturnPeriods = dtReturnPeriods.NewRow();
							drReturnPeriods["ID"] = item.Item3;
							drReturnPeriods["Name"] = item.Item2;
							dtReturnPeriods.Rows.Add(drReturnPeriods);
						}
						if (Type == "I")
						{
							rptPeriodList.DataSource = dtReturnPeriods;
							rptPeriodList.DataBind();

						}
						else if (Type == "O")
						{
							rptPeriod1.DataSource = dtReturnPeriods;
							rptPeriod1.DataBind();
						}

					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		public void Clear()
		{
			// txtFileInputList.Text = "";
			ddlYear.SelectedIndex = -1;
			txtFileInputList.Text = "";
		}
		protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				int? serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));

				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				Clear();
				DataTable dtFileInput = new DataTable();
				dtFileInput.Columns.Add("ID");
				dtFileInput.Columns.Add("Name");

				int ddlvalue = -1;
				List<int> lstFrequency = new List<int>();
				if (ddlFileType.SelectedValue != "-1")
				{
					if (ddlFileType.SelectedValue == "Monthly")   //File type Monthly
					{
						ddlvalue = 0;
						lstFrequency.Add(ddlvalue);
					}
					else if (ddlFileType.SelectedValue == "Annual") //File type Yearly
					{
						ddlvalue = 3;
						lstFrequency.Add(ddlvalue);
					}
					else if (ddlFileType.SelectedValue == "Other")
					{
						foreach (RepeaterItem item1 in rptFileInput.Items)
						{
							CheckBox chkFileInput = (System.Web.UI.WebControls.CheckBox)item1.FindControl("chkFileInput");
						}
						////   1=Quaterly//2=Half//4 =4Monthly//5 = BiAnnual   /////
						//ddlvalue = 1;
						lstFrequency.Add(ddlvalue);
						int[] FreqArr = new int[4] { 1, 2, 4, 5 };
						for (int i = 0; i < FreqArr.Length; i++)
						{
							lstFrequency.Add(FreqArr[i]);
						}

					}
					#region MyRegion InputOutput Page File
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						var listFileInputOutput = (from PP in entities.page_parameter
												   join CIM in entities.ComplianceInputMappings on PP.id equals CIM.InputID
												   join CI in entities.ComplianceInstances on (long)CIM.ComplianceID equals CI.ComplianceId
												   join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
												   where lstFrequency.Contains((int)PP.Frequency)
												   && PP.page_name != "" && PP.FileType == 1 && CA.UserID == (long)LoginUserID
												   select new
												   {
													   Id = PP.id,
													   Name = PP.page_name,
													   Priority = PP.Priority
												   }).Distinct().OrderBy(x => x.Priority).ToList();


						if (serviceproviderid != 94 && listFileInputOutput.Exists(x => x.Id == 46))
						{
							var challanfileid = listFileInputOutput.Single(x => x.Id == 46);
							listFileInputOutput.Remove(challanfileid);
						}
						if (serviceproviderid != 94 && listFileInputOutput.Exists(x => x.Id == 47))
						{
							var challanfileid = listFileInputOutput.Single(x => x.Id == 47);
							listFileInputOutput.Remove(challanfileid);
						}
						if (serviceproviderid != 94 && listFileInputOutput.Exists(x => x.Id == 48))
						{
							var challanfileid = listFileInputOutput.Single(x => x.Id == 48);
							listFileInputOutput.Remove(challanfileid);
						}

						if (listFileInputOutput.Count > 0)
						{
							foreach (var item in listFileInputOutput)
							{
								DataRow drFileInput = dtFileInput.NewRow();
								drFileInput["ID"] = item.Id;
								drFileInput["Name"] = item.Name;
								dtFileInput.Rows.Add(drFileInput);
							}

							rptFileInput.DataSource = dtFileInput;
							rptFileInput.DataBind();
							if (ddlFileType.SelectedValue == "Other")
							{

								foreach (RepeaterItem item1 in rptFileInput.Items)
								{
									CheckBox chkFileInput = (CheckBox)item1.FindControl("chkFileInput");
									chkFileInput.Visible = false; //must uncomment

									HtmlTableCell cell = (HtmlTableCell)item1.FindControl("tdChkFiles");
									cell.Visible = false;

									Label lblFileInputNm = (Label)item1.FindControl("lblFileInputName");
									lblFileInputNm.Visible = true;

								}
							}
							else
							{
								foreach (RepeaterItem item1 in rptFileInput.Items)
								{
									Label lblFileInputNm = (Label)item1.FindControl("lblFileInputName");
									lblFileInputNm.Visible = false;
								}
							}

						}
						else
						{
							rptFileInput.DataSource = dtFileInput;
							rptFileInput.DataBind();
						}
					}
					#endregion

					BindPeriod_Return(ddlFileType.SelectedValue, "I");
					FillDocumentType();
				}
			}
			catch (Exception ex)
			{
			}
		}
		protected void btnFileInput_Click(object sender, EventArgs e)
		{
			FillDocumentType();
		}

		private void FillDocumentType()
		{
			try
			{
				int? serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
				List<int> lstInputId = new List<int>();
				if (ddlFileType.SelectedValue == "Other")
				{
					int InputId = Convert.ToInt32(hdnInputId.Value);
					lstInputId.Add(InputId);
				}
				else
				{
					string InputId1 = "";
					int Counts = 0;
					foreach (RepeaterItem Item in rptFileInput.Items)
					{
						CheckBox chkFileInput = (CheckBox)Item.FindControl("chkFileInput");

						if (chkFileInput != null && chkFileInput.Checked)
						{
							Label lblFileInputID = (Label)Item.FindControl("lblFileInputID");
							Label lblFileInputName = (Label)Item.FindControl("lblFileInputName");

							int TestID = (Convert.ToInt32(Item.ID));
							lstInputId.Add(Convert.ToInt32(lblFileInputID.Text));

							// txtFileInputList.Text += lblFileInputName.Text + ",";
							InputId1 += (lblFileInputName.Text) + ",";
							if (InputId1.ToString().Contains(","))
							{
								Counts++;
							}
							else
							{
								Counts = 0;
							}
						}

					}

					if (InputId1.Length > 0 && Counts == 1)
					{
						txtFileInputList.Attributes.Add("placeholder", "  " + InputId1.ToString().Remove(InputId1.Length - 1, 1));
					}
					else if (Counts > 1)
					{
						txtFileInputList.Attributes.Add("placeholder", "  Selected multiple Files");
					}
					else
					{
						txtFileInputList.Attributes.Add("placeholder", "     All Files");
					}
				}

				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				string ActGroup = ddlActGroup.SelectedValue;
				// txtFileInputList.Text.TrimEnd(',');
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var DocumentType = (from CIM in entities.ComplianceInputMappings
										join CI in entities.ComplianceInstances on (long)CIM.ComplianceID equals CI.ComplianceId
										join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
										where lstInputId.Contains((int)CIM.InputID) && CA.UserID == (long)LoginUserID
										select new
										{
											Id = CIM.DocumentType,
											Name = CIM.DocumentType,
											//ActGroupId = CIM.AVACOM_ActGroup,
											//ActGroupName = CIM.AVACOM_ActGroup
										}).Distinct().ToList();

					if (ActGroup == "CLRA")
					{
						DocumentType = DocumentType.Except(DocumentType.Where(a => a.Name == "Challan")).ToList();
					}

					if (serviceproviderid == 94 && !lstInputId.Contains(46))
					{
						if (DocumentType.Exists(x => x.Name == "Challan"))
						{
							var challanfileid = DocumentType.Single(x => x.Name == "Challan");
							DocumentType.Remove(challanfileid);
						}
					}

					ddlComplianceType.DataValueField = "Id";
					ddlComplianceType.DataTextField = "Name";

					ddlComplianceType.DataSource = DocumentType;
					ddlComplianceType.DataBind();

					//ddlActGroup.DataValueField = "ActGroupId";
					//ddlActGroup.DataTextField = "ActGroupName";

					//ddlActGroup.DataSource = DocumentType;
					//ddlActGroup.DataBind();

					if (serviceproviderid != 94)
					{
						ddlComplianceType.Items.Insert(0, "All Compliances");
					}
				}
			}
			catch (Exception ex)
			{
			}
		}



		protected void btnGetSelectedLbl_Click(object sender, EventArgs e)
		{
			string InputId = "";
			int Counts = 0;
			hdnSubmitbtn.Value = "";

			foreach (RepeaterItem Item in rptFileInput.Items)
			{
				CheckBox chkFileInput = (CheckBox)Item.FindControl("chkFileInput");

				if (chkFileInput != null && chkFileInput.Checked)
				{
					Label lblFileInputID = (Label)Item.FindControl("lblFileInputID");
					Label lblFileInputName = (Label)Item.FindControl("lblFileInputName");
					InputId += (lblFileInputName.Text) + ",";
					if (InputId.ToString().Contains(","))
					{
						Counts++;
					}
					else
					{
						Counts = 0;
					}

				}
			}
			if (InputId.Length > 0 && Counts == 1)
			{
				txtFileInputList.Attributes.Add("placeholder", "  " + InputId.ToString().Remove(InputId.Length - 1, 1));
			}
			else if (Counts > 1)
			{
				txtFileInputList.Attributes.Add("placeholder", "  Selected multiple Files");
			}
			else
			{
				txtFileInputList.Attributes.Add("placeholder", "     All Files");
			}


		}


		//protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
		//{
		//    tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
		//}

		private void BindLocationFilter(List<int> assignedBranchList)
		{
			try
			{
				List<NameValueHierarchy> branches = new List<NameValueHierarchy>();
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					int customerID = -1;
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
					if (ddlActGroup.SelectedValue == "CLRA")
					{
						var  _obj=entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Where(x => x != null && x.AVACOM_CustomerID == customerID).Select(x => x.AVACOM_BranchID);
						assignedBranchList=_obj.Where(v => v != null).Select(x => x.Value).ToList();
					
						branches = RLCS_ClientsManagement.GetAllHierarchyCLRA(customerID);
					}
					else
					{
						branches = RLCS_ClientsManagement.GetAllHierarchy(customerID);
					}
					tvFilterLocation.Nodes.Clear();
					TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
					node.Selected = true;
					tvFilterLocation.Nodes.Add(node);
					foreach (var item in branches)
					{
						node = new TreeNode(item.Name, item.ID.ToString());
						node.SelectAction = TreeNodeSelectAction.Expand;
						CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
						tvFilterLocation.Nodes.Add(node);
					}
					tvFilterLocation.CollapseAll();
					//tvFilterLocation_SelectedNodeChanged(null, null);
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}
		private void BindLocationFilterOutput(List<int> assignedBranchList)
		{
			try
			{
				List<NameValueHierarchy> branches = new List<NameValueHierarchy>();
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					int customerID = -1;
					customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);


					if (ddlActGroupOutput.SelectedValue == "CLRA")
					{
					
						var _obj = entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Where(x => x != null && x.AVACOM_CustomerID == customerID).Select(x => x.AVACOM_BranchID);
						assignedBranchList = _obj.Where(v => v != null).Select(x => x.Value).ToList();

						branches = RLCS_ClientsManagement.GetAllHierarchyCLRA(customerID);
					}
					else
					{
						branches = RLCS_ClientsManagement.GetAllHierarchy(customerID);
					}
					LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
					tvOutputBranch.Nodes.Clear();
					TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
					node.Selected = true;
					tvOutputBranch.Nodes.Add(node);
					foreach (var item in branches)
					{
						node = new TreeNode(item.Name, item.ID.ToString());
						node.SelectAction = TreeNodeSelectAction.Expand;
						CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
						tvOutputBranch.Nodes.Add(node);
					}
					tvOutputBranch.CollapseAll();
					// tvOutputBranch_SelectedNodeChanged(null, null);
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

		protected void btnsubmit_Click(object sender, EventArgs e)
		{
			try
			{
				string InputId = "";
				//txtFileInputList.Text = "";
				if (ddlFileType.SelectedValue == "Other")
				{
					InputId = string.IsNullOrEmpty(Convert.ToString(hdnInputId.Value)) ? "" : Convert.ToString(hdnInputId.Value);
				}
				else
				{

					string InputId1 = "";
					int Counts = 0;

					foreach (RepeaterItem Item in rptFileInput.Items)
					{
						CheckBox chkFileInput = (CheckBox)Item.FindControl("chkFileInput");

						if (chkFileInput != null && chkFileInput.Checked)
						{
							Label lblFileInputID = (Label)Item.FindControl("lblFileInputID");
							Label lblFileInputName = (Label)Item.FindControl("lblFileInputName");
							InputId += (lblFileInputID.Text) + ",";
							//  txtFileInputList.Text += lblFileInputName.Text + ",";

							InputId1 += (lblFileInputName.Text) + ",";
							if (InputId1.ToString().Contains(","))
							{
								Counts++;
							}
							else
							{
								Counts = 0;
							}

						}
					}
					if (InputId1.Length > 0 && Counts == 1)
					{
						txtFileInputList.Attributes.Add("placeholder", "  " + InputId1.ToString().Remove(InputId1.Length - 1, 1));
					}
					else if (Counts > 1)
					{
						txtFileInputList.Attributes.Add("placeholder", "  Selected multiple Files");
					}
					else
					{
						txtFileInputList.Attributes.Add("placeholder", "     All Files");
					}
					if (InputId.Length > 0)
					{
						InputId = InputId.Remove(InputId.Length - 1);
					}
				}
				string MonthId = "";
				foreach (RepeaterItem Item in rptPeriodList.Items)
				{
					CheckBox chkPeriod = (CheckBox)Item.FindControl("chkPeriod");

					if (chkPeriod != null && chkPeriod.Checked)
					{
						Label lblPeriodID = (Label)Item.FindControl("lblPeriodID");
						MonthId += (lblPeriodID.Text) + ",";
					}
				}
				if (MonthId.Length > 0)
				{
					MonthId = MonthId.Remove(MonthId.Length - 1);
				}

				hdnSubmitbtn.Value = "";
				int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				hdnCustID.Value = Convert.ToString(AuthenticationHelper.CustomerID);

				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				hdnLoginUserID.Value = Convert.ToString(LoginUserID);
				string BranchID = "";
				hdnBranchId.Value = "";
				locationList.Clear();

				for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
				{
					RetrieveNodes(this.tvFilterLocation.Nodes[i]);
				}
				if (locationList.Count > 0)
				{
					foreach (var item in locationList)
					{
						BranchID += Convert.ToString(item) + ",";
					}
				}
				if (!string.IsNullOrEmpty(BranchID))
				{
					BranchID = BranchID.Remove(BranchID.Length - 1);

					hdnBranchId.Value = Convert.ToString(BranchID);
				}

				if (String.IsNullOrEmpty(ddlFileType.SelectedValue) || ddlYear.SelectedValue == "0" || String.IsNullOrEmpty(MonthId) || BranchID == "" || InputId.Length == 0)
				{
					if (String.IsNullOrEmpty(ddlFileType.SelectedValue))
					{
						InputTypeValidator.IsValid = false;
						InputTypeValidator.ErrorMessage = "Please Select File Input Type";
						InputTypeValidator.ValidationGroup = "DocGenValidationGroup";
						cvDuplicateEntrysucess.Visible = false;
					}
					if (InputId.Length == 0)
					{
						InputFileValidator.IsValid = false;
						InputFileValidator.ErrorMessage = "Please Select Input File Selection";
						InputFileValidator.ValidationGroup = "DocGenValidationGroup";
						cvDuplicateEntrysucess.Visible = false;
					}
					if (String.IsNullOrEmpty(MonthId))
					{
						PeriodValidator.IsValid = false;
						PeriodValidator.ErrorMessage = "Please Select Period";
						PeriodValidator.ValidationGroup = "DocGenValidationGroup";
						cvDuplicateEntrysucess.Visible = false;
					}
					if (ddlYear.SelectedValue == "0")
					{
						YearValidator.IsValid = false;
						YearValidator.ErrorMessage = "Please Select Year";
						YearValidator.ValidationGroup = "DocGenValidationGroup";
						cvDuplicateEntrysucess.Visible = false;
					}
					if (String.IsNullOrEmpty(BranchID))
					{
						EntityValidator.IsValid = false;
						EntityValidator.ErrorMessage = "Please Select Entity";
						EntityValidator.ValidationGroup = "DocGenValidationGroup";
						cvDuplicateEntrysucess.Visible = false;
					}
				}

				else
				{
					hdnInputId.Value = Convert.ToString(InputId);
					string ActGroup = string.Empty;
					ActGroup = ddlActGroup.SelectedValue;

					hdnMonth.Value = Convert.ToString(MonthId);

					string ComplianceType = "";
					try
					{
						if (ddlComplianceType.SelectedItem.Text != "")
						{
							ComplianceType = ddlComplianceType.SelectedItem.Text;
							if (ComplianceType == "All Compliances")
							{
								ComplianceType = "All";
							}
						}
					}
					catch (Exception ex)
					{
						ComplianceType = "All";
					}
					hdnComplianceType.Value = string.IsNullOrEmpty(Convert.ToString(ComplianceType)) ? "All" : Convert.ToString(ComplianceType);

					//string ActGroup = "";
					//hdnActGroup.Value = ActGroup;
					string ComplianceID = "";
					hdnComplianceID.Value = Convert.ToString(ComplianceID);
					//string ReturnRegisterChallanID = "";
					//hdnReturnRegisterChallanID.Value = Convert.ToString(ReturnRegisterChallanID);
					//string DocumentType = "";
					//hdnDocumentType.Value = string.IsNullOrEmpty(Convert.ToString(DocumentType)) ? "" : Convert.ToString(DocumentType);
					//string Period = "";
					//hdnPeriod.Value = string.IsNullOrEmpty(Convert.ToString(Period)) ? "" : Convert.ToString(Period);
					string Year = "";
					if (ddlYear.SelectedValue != "0")
					{
						Year = Convert.ToString(ddlYear.SelectedValue);
						hdnYear.Value = string.IsNullOrEmpty(Convert.ToString(Year)) ? "2019" : Convert.ToString(Year);
					}
					hdnSubmitbtn.Value = "Submit";

					hdnClientId.Value = "";
					List<sp_GetEntityCountFromBranchIds_Result> LstBranchCount = new List<sp_GetEntityCountFromBranchIds_Result>();

					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						//string ActGroup = ddlActGroup.SelectedValue;
						LstBranchCount = entities.sp_GetEntityCountFromBranchIds(BranchID, ActGroup).ToList();
						//LstEntities = (from L in CustomerBranch join LB in locationList on L.CustomerBranchID equals LB select L).ToList();
					}
					if (LstBranchCount.Count > 1)
					{
						cvDuplicateEntry.IsValid = false;
						cvDuplicateEntry.ErrorMessage = "Please Select Single Entity Details";
						cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
						cvDuplicateEntrysucess.Visible = false;
					}
					else
					{
						string ClientID = LstBranchCount[0].ClientId;
						hdnClientId.Value = ClientID;
						Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindTreeListGrid1('" + CustomerID + "','" + ComplianceType + "','" + ComplianceID + "','" + MonthId + "','" + Year + "','" + LoginUserID + "','" + ClientID + "',''" + 0 + ", '" + ActGroup + "');", true);
					}
					// Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindTreeListGrid1('802', '16754', '', 'ALL', '', '0', '0', '', '01', 2019, '')", true);
					//" +CustomerID+ "," +BranchID+ "," +InputId+ "," +ComplianceType+ "," + ActGroup+ "," +ComplianceID+ "," +ReturnRegisterChallanID+ "," +DocumentType+ "," +Month+ "," +Year+ "," +Period;
				}
			}
			catch (Exception ex)
			{

			}
		}


		protected void btnFileSelectnOther_Click(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(hdnInputId.Value))
				{
					string Frequency = "";
					int InputId = Convert.ToInt32(hdnInputId.Value);
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						var Frequency_ID = (from PP in entities.page_parameter
											where PP.id == InputId && PP.FileType == 1
											select PP.Frequency).Distinct().FirstOrDefault();
						if (Frequency_ID != null)
						{
							List<Tuple<int, string>> FreqTList = new List<Tuple<int, string>>();
							FreqTList.Add(new Tuple<int, string>(1, "Quarterly"));
							FreqTList.Add(new Tuple<int, string>(2, "Half Yearly"));
							FreqTList.Add(new Tuple<int, string>(4, "4 Monthly"));
							FreqTList.Add(new Tuple<int, string>(5, "BiAnnual"));
							var lst = FreqTList.FindAll(m => m.Item1 == Frequency_ID);
							if (lst.Count > 0)
							{


								BindPeriod_Return(Convert.ToString(lst[0].Item2), "I");


							}

						}

					}
				}
			}
			catch (Exception ex)
			{
			}
		}

		//protected void tvOutputBranch_SelectedNodeChanged(object sender, EventArgs e)
		//{
		//    tbxOutputBranch.Text = tvOutputBranch.SelectedNode.Text;
		//}

		protected void ddlOutputComplianceType_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				List<int> userAssignedBranchListIds = new List<int>();
				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
					foreach (var item in userAssignedBranchList)
					{
						userAssignedBranchListIds.Add(item.ID);
					}
				}

				#region MyRegion InputOutput Page File
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					// txtActNameList.Text = "";
					if (ddlOutputComplianceType.SelectedIndex != 0)
					{
						if (ddlOutputComplianceType.SelectedValue == "Register")
						{
							ddlActGroupOutput.Enabled = true;
							complianceRegisterChallan.Visible = true;
							complianceReturn.Visible = false;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Register_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}
						if (ddlOutputComplianceType.SelectedValue == "Challan")
						{
							ddlActGroupOutput.SelectedValue = "S&E/Factory";
							ddlActGroupOutput.Enabled = false;
							complianceRegisterChallan.Visible = true;
							complianceReturn.Visible = false;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Challan_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 && RR.AVACOM_ActGroup != "CLRA"
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}
						if (ddlOutputComplianceType.SelectedValue == "Return")
						{
							ddlActGroupOutput.Enabled = true;
							complianceRegisterChallan.Visible = false;
							complianceReturn.Visible = true;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Returns_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}
					}
					else
					{
						rptAct.DataSource = null;
						rptAct.DataBind();


					}
				}
			}
			catch (Exception ex)
			{
			}
			#endregion
		}

		protected void ddlActGroup_SelectedIndexChangedoutput(object sender, EventArgs e)
		{
			initialLoad();
			try
			{
				List<int> userAssignedBranchListIds = new List<int>();
				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
					foreach (var item in userAssignedBranchList)
					{
						userAssignedBranchListIds.Add(item.ID);
					}
				}

				#region MyRegion InputOutput Page File
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					// txtActNameList.Text = "";
					if (ddlActGroupOutput.SelectedValue == "S&E/Factory")
					{
						if (ddlOutputComplianceType.SelectedValue == "Register")
						{

							complianceRegisterChallan.Visible = true;
							complianceReturn.Visible = false;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Register_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}
						if (ddlOutputComplianceType.SelectedValue == "Challan")
						{
							ddlActGroupOutput.SelectedValue = "S&E/Factory";
							ddlActGroupOutput.Enabled = false;
							complianceRegisterChallan.Visible = true;
							complianceReturn.Visible = false;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Challan_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}
						if (ddlOutputComplianceType.SelectedValue == "Return")
						{
							ddlActGroupOutput.Enabled = true;
							complianceRegisterChallan.Visible = false;
							complianceReturn.Visible = true;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Returns_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}

						//else
						//{
						//    rptAct.DataSource = null;
						//    rptAct.DataBind();


						//}

					}

					else
					{
						if (ddlOutputComplianceType.SelectedValue == "Register")
						{

							complianceRegisterChallan.Visible = true;
							complianceReturn.Visible = false;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Register_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 && RR.AVACOM_ActType == "CLRA"
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}

						if (ddlOutputComplianceType.SelectedValue == "Return")
						{
							ddlActGroupOutput.Enabled = true;
							complianceRegisterChallan.Visible = false;
							complianceReturn.Visible = true;
							var listActNameOutput = (from A in entities.Acts
													 join C in entities.Compliances on A.ID equals C.ActID
													 join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
													 join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
													 join RR in entities.RLCS_Returns_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
													 join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
													 where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false
													 && RR.AVACOM_ActGroup == "CLRA"
													 select new
													 {
														 Id = A.ID,
														 Name = A.Name
													 }).Distinct().ToList();
							if (listActNameOutput.Count > 0)
							{

								rptAct.DataSource = listActNameOutput;
								rptAct.DataBind();
							}
							else
							{
								rptAct.DataSource = null;
								rptAct.DataBind();


							}
						}

						//else
						//{
						//    rptAct.DataSource = null;
						//    rptAct.DataBind();


						//}
					}




				}
			}
			catch (Exception ex)
			{
			}
			#endregion
		}

		protected void btnComplianceGet_Click(object sender, EventArgs e)
		{
			try
			{
				DataTable dtCompliance = new DataTable();
				dtCompliance.Columns.Add("ID");
				dtCompliance.Columns.Add("Name");
				List<int> userAssignedBranchListIds = new List<int>();
				List<int> lstActIds = new List<int>();
				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
					foreach (var item in userAssignedBranchList)
					{
						userAssignedBranchListIds.Add(item.ID);
					}
				}
				foreach (RepeaterItem Item in rptAct.Items)
				{
					CheckBox chkAct = (CheckBox)Item.FindControl("chkAct");

					if (chkAct != null && chkAct.Checked)
					{
						Label lblActID = (Label)Item.FindControl("lblActID");
						Label lblActName = (Label)Item.FindControl("lblActName");

						lstActIds.Add(Convert.ToInt32(lblActID.Text));

						// txtActNameList.Text += lblActName.Text + ",";
					}

				}
				if (lstActIds.Count > 0)
				{
					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						var listComplianceName = (from A in entities.Acts
												  join C in entities.Compliances on A.ID equals C.ActID
												  join CM in entities.ComplianceInputMappings on C.ID equals (long)CM.ComplianceID
												  join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
												  join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
												  join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
												  where userAssignedBranchListIds.Contains((int)CB.ID) && lstActIds.Contains((int)A.ID) && CA.UserID == (long)LoginUserID && C.IsDeleted == false && CM.DocumentType == ddlOutputComplianceType.SelectedValue
												  select new
												  {
													  Id = C.ID,
													  Name = C.ShortForm
												  }).Distinct().ToList();
						if (listComplianceName.Count > 0)
						{
							foreach (var item in listComplianceName)
							{
								DataRow drCompliance = dtCompliance.NewRow();
								drCompliance["ID"] = item.Id;
								drCompliance["Name"] = item.Name;
								dtCompliance.Rows.Add(drCompliance);
							}
							if (ddlOutputComplianceType.SelectedValue == "Return")
							{
								ddlComplianceName.DataValueField = "Id";
								ddlComplianceName.DataTextField = "Name";
								ddlComplianceName.DataSource = dtCompliance;
								ddlComplianceName.DataBind();
								ddlComplianceName.Items.Insert(0, "Compliance");
							}
							else
							{
								rptCompliance.DataSource = dtCompliance;
								rptCompliance.DataBind();
							}
						}
						else
						{
							ddlComplianceName.DataSource = null;
							ddlComplianceName.DataBind();
							ddlComplianceName.Items.Insert(0, "Compliance");

							rptCompliance.DataSource = null;
							rptCompliance.DataBind();
						}
					}
				}
				else
				{
					//Vali
				}
			}
			catch (Exception ex)
			{
			}
		}

		protected void ddlComplianceName_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (ddlComplianceName.SelectedIndex != 0)
			{
				int ComplianceID = Convert.ToInt32(ddlComplianceName.SelectedValue);
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					var Frequency_ID = (from C in entities.Compliances
										where C.ID == ComplianceID && C.IsDeleted == false
										select C.Frequency).Distinct().FirstOrDefault();
					if (Frequency_ID != null)
					{
						List<Tuple<int, string>> FreqTList = new List<Tuple<int, string>>();
						FreqTList.Add(new Tuple<int, string>(0, "Monthly"));
						FreqTList.Add(new Tuple<int, string>(1, "Quarterly"));
						FreqTList.Add(new Tuple<int, string>(2, "Half Yearly"));
						FreqTList.Add(new Tuple<int, string>(3, "Annual"));
						FreqTList.Add(new Tuple<int, string>(4, "Four Monthly"));
						FreqTList.Add(new Tuple<int, string>(5, "BiAnnual"));
						var lst = FreqTList.FindAll(m => m.Item1 == Frequency_ID);
						if (lst.Count > 0)
						{
							BindPeriod_Return(Convert.ToString(lst[0].Item2), "O");
						}

					}
				}
			}
		}

		protected void btnOutSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				hdnSubmitbtn.Value = "";
				//string ActGroup = "";
				string InputId = "";
				//string ReturnRegisterChallanID = "";
				//string DocumentType = "";
				//string Period = "";
				string MonthId = "";
				string Year = "";
				string ComplianceType = "";
				string ActID = "";
				int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
				hdnOutCustID.Value = Convert.ToString(AuthenticationHelper.CustomerID);
				LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
				hdnLoginUserID.Value = Convert.ToString(LoginUserID);

				string BranchID = "";

				if (!string.IsNullOrEmpty(BranchID_))
				{
					BranchID = hdnOutBranchId.Value;
				}
				else
				{
					hdnOutBranchId.Value = "";
					locationList.Clear();

					for (int i = 0; i < this.tvOutputBranch.Nodes.Count; i++)
					{
						RetrieveNodes(this.tvOutputBranch.Nodes[i]);
					}
					if (locationList.Count > 0)
					{
						foreach (var item in locationList)
						{
							BranchID += Convert.ToString(item) + ",";
						}
					}
					if (!string.IsNullOrEmpty(BranchID))
					{
						BranchID = BranchID.Remove(BranchID.Length - 1);

						hdnOutBranchId.Value = Convert.ToString(BranchID);
					}
				}

				foreach (RepeaterItem Item in rptAct.Items)
				{
					CheckBox chkAct = (CheckBox)Item.FindControl("chkAct");

					if (chkAct != null && chkAct.Checked)
					{
						Label lblActID = (Label)Item.FindControl("lblActID");
						ActID += (lblActID.Text) + ",";
					}
				}
				if (ActID.Length > 0)
				{
					ActID = ActID.Remove(ActID.Length - 1);
					hdnOutActID.Value = ActID;
				}
				foreach (RepeaterItem Item in rptPeriod1.Items)
				{
					CheckBox chkPeriod1 = (CheckBox)Item.FindControl("chkPeriod1");

					if (chkPeriod1 != null && chkPeriod1.Checked)
					{
						Label lblPeriodID1 = (Label)Item.FindControl("lblPeriodID1");
						MonthId += (lblPeriodID1.Text) + ",";
					}
				}
				if (MonthId.Length > 0)
				{
					MonthId = MonthId.Remove(MonthId.Length - 1);
					hdnOutMonth.Value = MonthId;
				}
				string ComplianceID = "";
				if (ddlComplianceName.SelectedValue != "Compliance" || ddlComplianceName.SelectedValue != "")
				{
					ComplianceID = ddlComplianceName.SelectedValue;
					hdnOutComplianceID.Value = ComplianceID;
				}
				if (ddlOutputComplianceType.SelectedValue == "Return")
				{
					ComplianceID = ddlComplianceName.SelectedValue;
					hdnOutComplianceID.Value = ComplianceID;
				}
				else
				{
					List<int> lstComplianceID = new List<int>();
					foreach (RepeaterItem Item in rptCompliance.Items)
					{
						CheckBox chkcompliance = (CheckBox)Item.FindControl("chkcompliance");

						if (chkcompliance != null && chkcompliance.Checked)
						{
							Label lblcomplianceID = (Label)Item.FindControl("lblcomplianceID");
							if (!string.IsNullOrEmpty(lblcomplianceID.Text))
							{
								ComplianceID += lblcomplianceID.Text + ',';

							}

						}
					}

					if (ComplianceID.Length > 0)
					{
						ComplianceID = ComplianceID.Remove(ComplianceID.Length - 1);
						hdnOutComplianceID.Value = ComplianceID;
					}
				}

				if (ddlOutYear.SelectedValue != "0")
				{
					Year = ddlOutYear.SelectedValue;
					hdnOutYear.Value = Year;
				}

				if (ddlOutputComplianceType.SelectedValue == "Compliance Type" || ActID.Length == 0 || BranchID == "" || ComplianceID == "" || string.IsNullOrEmpty(Year) || MonthId.Length == 0)
				{
					if (ddlOutputComplianceType.SelectedValue == "Compliance Type")
					{
						ComplianceTypeValidator.IsValid = false;
						ComplianceTypeValidator.ErrorMessage = "Compliance Type Selection not Found, Please Select Compliance Type";
						ComplianceTypeValidator.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}

					if (ActID.Length == 0)
					{
						ActNameValidator.IsValid = false;
						ActNameValidator.ErrorMessage = "Act Selection not Found, Please Select Act";
						ActNameValidator.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}

					if (ComplianceID == "0" || ComplianceID == "")
					{
						ComplianceValidator.IsValid = false;
						ComplianceValidator.ErrorMessage = "Compliance Selection not Found, Please Select Compliance";
						ComplianceValidator.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}

					if (MonthId.Length == 0)
					{
						OutPeriodValidator.IsValid = false;
						OutPeriodValidator.ErrorMessage = "Period Selection not Found, Please Select Period";
						OutPeriodValidator.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}

					if (string.IsNullOrEmpty(Year))
					{
						OutYearValidator.IsValid = false;
						OutYearValidator.ErrorMessage = "Year Selection not Found, Please Select Year";
						OutYearValidator.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}

					if (BranchID == "")
					{
						OutEntityValidator.IsValid = false;
						OutEntityValidator.ErrorMessage = "Entity Selection Not Found, Please Select Entity";
						OutEntityValidator.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}
				}
				else
				{
					BranchID = String.IsNullOrEmpty(BranchID) ? "" : BranchID;
					hdnClientId.Value = "";
					List<sp_GetEntityCountFromBranchIds_Result> LstBranchCount = new List<sp_GetEntityCountFromBranchIds_Result>();

					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						LstBranchCount = entities.sp_GetEntityCountFromBranchIds(BranchID,ddlActGroup.SelectedValue).ToList();
						//LstEntities = (from L in CustomerBranch join LB in locationList on L.CustomerBranchID equals LB select L).ToList();
					}
					if (LstBranchCount.Count > 1)
					{
						cvDuplicateEntryOut.IsValid = false;
						cvDuplicateEntryOut.ErrorMessage = "Please Select Single Entity Details";
						cvDuplicateEntryOut.ValidationGroup = "DocGenValidationGroup1";
						cvDuplicateEntrysucessOut.Visible = false;
					}
					else
					{
						string ClientID = LstBranchCount[0].ClientId;
						hdnClientId.Value = ClientID;


						ComplianceType = ddlOutputComplianceType.SelectedValue;
						hdnOutComplianceType.Value = ComplianceType;

						hdnSubmitbtn.Value = "OutSubmit";

						Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "bindKendoGridOut('" + CustomerID + "','" + ComplianceType + "','" + ComplianceID + "','" + MonthId + "','" + Year + "','" + LoginUserID + "','" + ClientID + "');", true);
						//ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "SubmitClickOnRedirection();", true);
					}
				}
			}
			catch (Exception ex)
			{


			}
		}
		protected void RetrieveNodes(TreeNode node)
		{
			try
			{
				if (node.Checked)
				{
					if (!locationList.Contains(Convert.ToInt32(node.Value)))
					{
						locationList.Add(Convert.ToInt32(node.Value));
					}
					if (node.ChildNodes.Count != 0)
					{
						for (int i = 0; i < node.ChildNodes.Count; i++)
						{
							RetrieveNodes(node.ChildNodes[i]);
						}
					}
				}
				else
				{
					foreach (TreeNode tn in node.ChildNodes)
					{
						if (tn.Checked)
						{
							if (!locationList.Contains(Convert.ToInt32(tn.Value)))
							{
								locationList.Add(Convert.ToInt32(tn.Value));
							}
						}
						if (tn.ChildNodes.Count != 0)
						{
							for (int i = 0; i < tn.ChildNodes.Count; i++)
							{
								RetrieveNodes(tn.ChildNodes[i]);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
			}
		}
		protected void ChkBoxClear(TreeNode node)
		{

			if (node.Checked)
			{
				node.Checked = false;
			}
			foreach (TreeNode tn in node.ChildNodes)
			{

				if (tn.Checked)
				{
					tn.Checked = false;
				}

				if (tn.ChildNodes.Count != 0)
				{
					for (int i = 0; i < tn.ChildNodes.Count; i++)
					{
						ChkBoxClear(tn.ChildNodes[i]);
					}
				}
			}
		}
		protected void btnClear_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < this.tvOutputBranch.Nodes.Count; i++)
			{
				ChkBoxClear(this.tvOutputBranch.Nodes[i]);
			}
		}
		protected void btnClear1_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
			{
				ChkBoxClear(this.tvFilterLocation.Nodes[i]);
			}
		}
		protected void btnlocation1_Click(object sender, EventArgs e)
		{

		}

		protected void btnhdnClear1_Click(object sender, EventArgs e)
		{
			try
			{
				DataTable DocumentType = new DataTable();
				DocumentType = null;
				ddlFileType.SelectedIndex = -1;
				//txtFileInputList.Text = "";
				rptFileInput.DataSource = null;
				rptFileInput.DataBind();
				rptPeriodList.DataSource = null;
				rptPeriodList.DataBind();
				ddlYear.SelectedIndex = -1;
				ddlComplianceType.DataSource = DocumentType;
				ddlComplianceType.DataBind();
				ddlComplianceType.SelectedIndex = -1;
				btnClear1_Click(sender, e);
				btnComplianceGet_Click(sender, e);
				hdnBranchId.Value = "";
				hdnInputId.Value = "";
				hdnComplianceID.Value = "";
				hdnActGroup.Value = "";
				hdnComplianceType.Value = "";
				hdnReturnRegisterChallanID.Value = "";
				hdnDocumentType.Value = "";
				hdnMonth.Value = "";
				hdnYear.Value = "";
				hdnPeriod.Value = "";

			}
			catch (Exception ex)
			{
			}

		}

		protected void btnhdnClear_Click(object sender, EventArgs e)
		{
			try
			{
				DataTable ComplianceName = new DataTable();
				ComplianceName = null;
				ddlOutputComplianceType.SelectedIndex = -1;
				//  txtActNameList.Text = "";
				rptAct.DataSource = null;
				rptAct.DataBind();
				rptPeriod1.DataSource = null;
				rptPeriod1.DataBind();
				ddlOutYear.SelectedIndex = -1;

				ddlComplianceName.DataSource = null;
				ddlComplianceName.DataBind();

				ddlComplianceName.SelectedIndex = -1;

				hdnOutBranchId.Value = "";
				hdnOutInputId.Value = "";
				hdnOutComplianceID.Value = "";
				hdnOutActID.Value = "";
				hdnOutComplianceType.Value = "";
				hdnOutReturnRegisterChallanID.Value = "";
				hdnOutDocumentType.Value = "";
				hdnOutMonth.Value = "";
				hdnOutYear.Value = "";
				hdnOutPeriod.Value = "";
			}
			catch (Exception ex)
			{
			}
		}

		public class InputOutputScheduleID
		{
			public string ScheduleOnID { get; set; }
			public int ReturnRegisterChallanID { get; set; }
		}
		[WebMethod]
		public static string JSONInsert(string IOJson, string ParameterID)
		{
			//XmlDocument doc = JsonConvert.DeserializeXmlNode(IOJson);
			string ID = "0";
			try
			{
				var IOJsonNew = IOJson;
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					tbl_IOJSON Record1 = new tbl_IOJSON()
					{
						log = Convert.ToString(IOJsonNew),
						ParameterID = Convert.ToString(ParameterID)
					};
					// Record1.log = IOJson;
					entities.tbl_IOJSON.Add(Record1);
					entities.SaveChanges();
					ID = Convert.ToString(Record1.ID);

				}

				JavaScriptSerializer json_serializer = new JavaScriptSerializer();
				var ID_list = JsonConvert.DeserializeObject<List<InputOutputScheduleID>>(IOJson);
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					foreach (var item in ID_list)
					{
						tbl_ScheduleOnIDDetails lstIDs = new tbl_ScheduleOnIDDetails()
						{
							JSONID = Convert.ToInt64(ID),
							ScheduleOnId = item.ScheduleOnID,
							ReturnRegisterChallanID = item.ReturnRegisterChallanID,
							Created_Date = DateTime.Now
						};
						entities.tbl_ScheduleOnIDDetails.Add(lstIDs);
						entities.SaveChanges();
					}
				}

				return ID;
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
				return ID;

			}
		}

		protected void btnFrequencyGet_Click(object sender, EventArgs e)
		{
			List<int> lstComplianceID = new List<int>();
			foreach (RepeaterItem Item in rptCompliance.Items)
			{
				CheckBox chkcompliance = (CheckBox)Item.FindControl("chkcompliance");

				if (chkcompliance != null && chkcompliance.Checked)
				{
					Label lblcomplianceID = (Label)Item.FindControl("lblcomplianceID");
					if (!string.IsNullOrEmpty(lblcomplianceID.Text))
					{
						lstComplianceID.Add(Convert.ToInt32(lblcomplianceID.Text));
					}

				}
			}
			if (lstComplianceID.Count > 0)
			{
				using (ComplianceDBEntities entities = new ComplianceDBEntities())
				{
					int IDC = Convert.ToInt32(lstComplianceID[0]);
					var Frequency_ID = (from C in entities.Compliances
										where C.ID == IDC && C.IsDeleted == false
										select C.Frequency).Distinct().FirstOrDefault();
					if (Frequency_ID != null)
					{
						List<Tuple<int, string>> FreqTList = new List<Tuple<int, string>>();
						FreqTList.Add(new Tuple<int, string>(0, "Monthly"));
						FreqTList.Add(new Tuple<int, string>(1, "Quarterly"));
						FreqTList.Add(new Tuple<int, string>(2, "Half Yearly"));
						FreqTList.Add(new Tuple<int, string>(3, "Annual"));
						FreqTList.Add(new Tuple<int, string>(4, "Four Monthly"));
						FreqTList.Add(new Tuple<int, string>(5, "BiAnnual"));
						var lst = FreqTList.FindAll(m => m.Item1 == Frequency_ID);
						if (lst.Count > 0)
						{
							BindPeriod_Return(Convert.ToString(lst[0].Item2), "O");
						}

					}

				}
			}
		}

		protected void ddlActGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			initialLoad();
			FillDocumentType();
		}
	}
}
