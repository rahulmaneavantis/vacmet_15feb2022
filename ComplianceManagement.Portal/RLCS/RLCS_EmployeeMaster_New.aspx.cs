﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.Services;
using System.Web.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_EmployeeMaster_New : System.Web.UI.Page
    {
        protected static string avacomRLCSAPIURL; 
        protected static string ProfileID;
        protected int userID;
        protected string userRole;
        protected static string RoleCode;

        protected static int custID = -1;
        protected static int IsSPDist;
        protected static int spID = -1;
        protected static int distID = -1;

        protected int? serviceproviderid;

        protected void Page_Load(object sender, EventArgs e)
        {
            custID = -1;
            spID = -1;
            distID = -1;

            userID = Convert.ToInt32(AuthenticationHelper.UserID);
            userRole = AuthenticationHelper.Role;
            RoleCode = AuthenticationHelper.Role;

            avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];            

             serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
            if (!string.IsNullOrEmpty(AuthenticationHelper.ProfileID))
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            else
                ProfileID = Convert.ToString(AuthenticationHelper.UserID);
            
            if (AuthenticationHelper.Role == "SPADM")
            {
                spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 0;
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                custID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 1;
            }
            else if (AuthenticationHelper.Role == "IMPT")
            {
                IsSPDist = 2;
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                custID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 3;
            }

            serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
            if (AuthenticationHelper.Role == "HEXCT" && serviceproviderid == 94)
            {
                lnkBtnComAssign.Visible = false;
                lnkBtnComAct.Visible = false;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }

        [System.Web.Services.WebMethod]
        public static int? GetServiceProvider(int ID)  
        {
            int? serviceproviderid = CustomerManagement.GetServiceProviderID(ID);
            return serviceproviderid;          
        }

    }
}