﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Reflection;
using System.Text;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_PayCodeHeaderBulkUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                spanErrors.Visible = false;
            }            
        }

        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            hdFileName.Value = "";
            span2.Visible = false;
            if (HeaderUpload.HasFile && Path.GetExtension(HeaderUpload.FileName).ToLower() == ".xlsx")
            {
                try
                {
                    if (!Directory.Exists(Server.MapPath("~/Uploaded/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Uploaded/"));
                    }

                    string filename = Path.GetFileName(HeaderUpload.FileName);
                    HeaderUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool matchSuccess = ContractCommonMethods.checkSheetExist(xlWorkbook, "PaycodeHeaderBulkUplod");
                            if (matchSuccess)
                            {
                                ProcessChecklistData(xlWorkbook);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'PaycodeHeaderBulkUplod'.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            else
            {
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Please upload excel file (.xlsx)";
            }
        }

        private void ProcessChecklistData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["PaycodeHeaderBulkUplod"];
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    if (xlWorksheet != null)
                    {
                        List<ErrorMessage> lstErrorMessage = new List<ErrorMessage>();
                        int xlrow2Col = xlWorksheet.Dimension.End.Column;
                        int xlrowCount = xlWorksheet.Dimension.End.Row;
                        bool headerPresent = false;
                        string ErrorFileName = "ErrorFile_DesignationUpload_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                        hdFileName.Value = ErrorFileName;

                        #region Validations

                        ErrorMessage objError = null;
                        string[] arrPaycode = { };
                        //string clientid = Convert.ToString(hdClientId.Value);
                        if (xlrowCount > 1)
                        {
                            span2.Visible = true;
                        }
                        else
                        {
                            for (int i = 2; i <= xlrow2Col; i++)
                            {
                                string HeaderName = xlWorksheet.Cells[1, i].Text.Trim();
                                if (String.IsNullOrEmpty((xlWorksheet.Cells[1, i].Text).Trim()))
                                {
                                    objError = new ErrorMessage();
                                    objError.ErrorDescription = "Required Header_Name at Row ";
                                    objError.RowNum = i.ToString();
                                    lstErrorMessage.Add(objError);
                                }
                                if (!String.IsNullOrEmpty((xlWorksheet.Cells[1, i].Text).Trim()))
                                {
                                    try
                                    {
                                        DataTable tblHeader = null;
                                        if (Session["HeaderName"] != null)
                                        {
                                            tblHeader = Session["HeaderName"] as DataTable;
                                        }
                                        string HearderExcel = (xlWorksheet.Cells[1, i].Text).Trim().ToUpper();
                                        headerPresent = tblHeader.AsEnumerable().Any(row => HearderExcel == row.Field<String>("HeaderName").ToUpper());
                                        if (arrPaycode.Contains(HearderExcel))
                                        {
                                            headerPresent = true;
                                        }
                                        if (!string.IsNullOrEmpty(HearderExcel))
                                        {
                                            Array.Resize(ref arrPaycode, arrPaycode.Length + 1);
                                            arrPaycode[arrPaycode.GetUpperBound(0)] = HearderExcel;
                                        }
                                        
                                        if (headerPresent == true)
                                        {
                                            objError = new ErrorMessage();
                                            objError.ErrorDescription = (xlWorksheet.Cells[1, i].Text).Trim() + " Paycode already exist.";
                                            objError.RowNum = i.ToString();
                                            lstErrorMessage.Add(objError);
                                        }
                                    }
                                    catch (Exception ex1)
                                    {
                                        Exception ex2 = new Exception("Datatable Error-"+ ex1);                                      
                                        LoggerMessage.InsertLog(ex2, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        cvUploadUtilityPage.IsValid = false;
                                        cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
                                    }
                                    
                                }
                            }
                        }
                        if (span2.Visible == false)
                        {
                            if (lstErrorMessage.Count == 0)
                            {
                                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                                for (int i = 2; i <= xlrow2Col; i++)
                                {
                                    string HeaderName = xlWorksheet.Cells[1, i].Text.Trim();
                                    StandardColumnModel objColumn = new StandardColumnModel();
                                    objColumn.CPMD_Standard_Column = HeaderName;
                                    lst.Add(objColumn);
                                }
                                Session["HeaderList"] = lst;
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = " Header Name Uploaded Successfully";
                                Session["HeaderName"] = null;
                                vsUploadUtility.CssClass = "alert alert-success";
                            }
                            else
                            {
                                Session["HeaderList"] = null;
                                GenerateDesignationUploadErrorCSV(lstErrorMessage, ErrorFileName);
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void GenerateDesignationUploadErrorCSV(List<ErrorMessage> detailView, string FileName)
        {
            try
            {
                spanErrors.Visible = true;

                if (!Directory.Exists(Server.MapPath("~/TempPayCodeHeaderError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/TempPayCodeHeaderError/"));
                }

                string FilePath = Server.MapPath("~/TempPayCodeHeaderError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {
                        string content = "Error Description,Row No";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {
                                content = data.ErrorDescription + "," + data.RowNum;
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }

        }
        protected void btnDownloadError_Click(object sender, EventArgs e)
        {
            string filePath = Server.MapPath("~/TempPayCodeHeaderError/" + hdFileName.Value);
            DownloadFile(filePath);
        }
        private void DownloadFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "text/plain";
                    Response.Flush();
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public class ErrorMessage
        {
            public string ErrorDescription { get; set; }
            public string RowNum { get; set; }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Server.MapPath("~/RLCSDocuments/")))
            {
                Directory.CreateDirectory(Server.MapPath("~/RLCSDocuments/"));
            }
            string filePath = Server.MapPath("~/RLCSDocuments/SamplePayCodeHeader.xlsx");
            DownloadFile(filePath);
        }
    }
}