﻿<%@ Page Title="Applicability Assessment :: Avantis" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_CheckApplicability.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_CheckApplicability" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
     <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <title></title>
   <style type="text/css">
          .k-grid tbody .k-button {
          min-width: 22px;
          min-height: 22px;
          border-radius: 35px;
          background-color: transparent;
          border-color: transparent;
          color:black;
    }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        input[type=checkbox], input[type=radio]{
                margin: 4px 5px 0px -3px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: auto !important;
        }
       
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .k-input, .k-textbox > input{
            text-align:left;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            /*border-width: 0 1px 1px 0px ;*/
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:1.4px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            /*border-width: 0 0px 1px 0px;*/
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }
         .k-filter-row th, .k-grid-header th[data-index='5'] {
            text-align:center;
        }
        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: #535b6a;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:32px;
            padding-top:3px;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
     .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
    -webkit-box-shadow: inset 0 0 3px 1px white;
    box-shadow:inset 0 0 3px 1px white;
}
        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            background: white;
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.1%;
            margin:-1px;
            margin-top:-7px;
        }
        label.k-label:hover{
              color:#1fd9e1;
          }
    .k-i-arrow-60-left:before {
            content: "\e007";
            margin-left: -2.7px;
            margin-top: 0.4px;
        }
         .btn {
             padding: 5px 8px;
             font-weight:400;
             font-size:14px;
         }
         .k-toolbar{
             margin-top:5px;
         }
         .k-tooltip{
             margin-top:5px;
         }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
           <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">
        var record=0;
        $(document).ready(function () {
            fhead('Masters/Applicability Assessment');
            $('#divShowApplicability').hide();
            BindGrid();
        });

        function BindGrid() {            
            var gridview = $("#gridListing").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetCheckApplicabilityCustomers?custID=' + <%=custID%>+'&SPID=' + <%=spID%>+'&distID=' + <%=distID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {  
                            debugger;
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    }
                },
                scrollable:false,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh:false,
                    buttonCount: 3,
                    pageSizes:true,
                    pageSize:10,
                    change: function (e) {

                    }
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page()-1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID",menu:false, },
                    //{
                    //    title: "Sr.No.",
                    //    lock:true,
                    //    field: "rowNumber",
                    //    template: "#= ++record #",
                    //    //template: "<span class='row-number'></span>",
                    //    width: "6%;",
                    //    filterable:false,
                       
                    //    attributes: {
                    //        style: 'white-space: nowrap;text-align:left;'
                    //    },
                    //},
                    {
                        field: "Name", title: 'Company',
                        width: "25%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            multi: true, 
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerName", title: 'Contact Person',
                        width: "21%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerEmail", title: 'Email',
                        width: "23%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerContactNumber", title: 'Contact Number',
                        width: "17%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        title: "Action", lock: true, width: "13%;",
                        attributes: {
                            style: 'text-align: center;'
                        },
                        command:
                            [
                               { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", className: "ob-download removeBorderLeft", click: Edit },
                            ],
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index()
                            + 1
                            + ($("#gridListing").data("kendoGrid").dataSource.pageSize() * ($("#gridListing").data("kendoGrid").dataSource.page() - 1));

                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });

                    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                },
                //columnHide: function(e) {
                //    // Add the fix when first column hides
                //    if(!$('.k-grid-header .k-header:last-child').is(':visible')){
                //        // Fix the header
                //        $('.k-grid-header .k-header:visible').eq(0).addClass('removeBorderRight');

                //        // Fix the rows
                //        $('.k-grid tr').each(function (){
                //            $(this).find('td:visible').eq(0).addClass('removeBorderRight');
                //        });
                //    }
                //}, 
                //columnShow: function(e) {
                //    // If first column has shown up
                //    if($('.k-grid-header .k-header:last-child').is(':visible')){
                //        // remove the fix from all elements that has it
                //        $('.removeBorderLeft').removeClass('removeBorderLeft');
                //    }
                //}
            
                
            });
            $("#gridListing").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#gridListing").kendoTooltip({
                filter: "td:nth-child(2)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridListing").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Name;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridListing").kendoTooltip({
                filter: "td:nth-child(3)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridListing").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.BuyerName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridListing").kendoTooltip({
                filter: "td:nth-child(4)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridListing").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.BuyerEmail;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridListing").kendoTooltip({
                filter: "td:nth-child(5)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridListing").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.BuyerContactNumber;
                    return content;
                }
            }).data("kendoTooltip");


        }
               function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            //$(this.element).empty();
            $('#gridListing').data('kendoGrid').dataSource.read();
        }

        function onRowBound(e) {
            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
            $(".k-grid-delete").find("span").addClass("k-icon k-delete");
            $(".k-grid-add").find("span").addClass("k-i-plus-outline");
        }

        function OpenCheckApplicabilityPopup(ID) {            
            $('#divShowApplicability').show();

            var myWindowAdv = $("#divShowApplicability");
            
            myWindowAdv.kendoWindow({
                width: "95%",
                height: '95%',
                title: "Applicability Assessment",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                //open: onOpen
            });
            
            $('#iframeApplicability').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeApplicability').attr('src', "/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=" + ID+"&Source=Home");            
            return false;
        }

        function Edit(e) {            
            var d = e.data;
            var grid = $("#gridListing").data("kendoGrid");

            var tr = $(e.target).closest("tr");    // get the current table row (tr)
            var item = this.dataItem(tr);
            var recordID = item.ID;

            $('#divShowApplicability').show();
            var myWindowAdv = $("#divShowApplicability");

            myWindowAdv.kendoWindow({                
                width: "95%",
                height: '95%',
                //content: '/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=' + recordID,
                title: "Applicability Assessment",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                //open: onOpen
            });

            $('#iframeApplicability').attr('src', 'about:blank');
            var windowWidget = $("#divShowApplicability").data("kendoWindow");
            
            myWindowAdv.data("kendoWindow").center().open();

            $('#iframeApplicability').attr('src', "/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=" + recordID+"&Source=Home");            
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar text-right">
            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddContract" OnClientClick="return OpenCheckApplicabilityPopup(0);"
                data-toggle="tooltip" data-placement="bottom" ToolTip="">
                    <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;Check Now
            </asp:LinkButton>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="gridListing" <%--style="border: none;"--%>>
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divShowApplicability">
        <iframe id="iframeApplicability" style="width: 100%; height: 98%; border: none;"></iframe>
    </div>
</asp:Content>
