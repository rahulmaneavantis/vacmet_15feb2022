﻿<%@ Page Title="Compliance Assignment Report" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_ComplianceAssignmentReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ComplianceAssignmentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <script src="../Scripts/KendoPage/HRPlus_AssignmentReport.js"></script>

  
       <style type="text/css">
           /*.k-treeview .k-item {
    display: block;
    border-width: 0;
    margin: 0;
    padding: 0px 0 0 0px;
}*/ .k-textbox > input{
                               text-align:left;
                           }
           #ddlCustomers-list {
           overflow-x:hidden;
           overflow-y:scroll;
           }
           .k-list-scroller {
           overflow-y:visible !important;
           overflow-x:initial;
           }
            .k-check-all{
              zoom:1.111;
              font-size:13px;
          }
            .k-label input[type="checkbox"] {
    zoom: 1.2;
    margin: 0px 5px 0 !important;
}
           .k-i-excel{
            margin-top:-3px;
           }
           .k-multiselect-wrap .k-input {
               padding-top:6px;
           }
           .k-autocomplete .k-input, .k-dropdown-wrap .k-input, .k-multiselect-wrap .k-input, .k-numeric-wrap .k-input, .k-picker-wrap .k-input, .k-selectbox .k-input, .k-textbox > input{
               padding-top:5px;
           }
           .k-grid, .k-listview {
               margin-top: 10px;
               
           }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
        
              .k-tooltip-content{
         width: max-content;
        
         }
        input[type=checkbox], input[type=radio] {
            margin: 4px 7px 0px;
            margin-top: 1px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:-7px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }
        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
             color: #515967;
             padding-top: 5px;
       }
        .k-grid-content {
            min-height: auto !important;
        }
        
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .btn{
            font-weight:400 !important;
            height:36px;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
            color:rgba(0,0,0,0.5);
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color:rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
           
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }
        label.k-label:hover{
            color:#1fd9e1;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-list-container.k-popup-dropdowntree {
             padding: 0;
             background-color: white;
             overflow-y: scroll;
      }
        .k-checkbox-label{
         margin-left: -18px;
}
           .k-treeview .k-state-hover, .k-treeview .k-state-hover:hover {
    cursor: pointer;
    background-color: transparent;
    border-color: transparent;
    color:#1fd9e1;
}
      .k-checkbox-label:hover {
         color:#1fd9e1;
}
     .k-list-container.k-popup-dropdowntree .k-treeview {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0px;
    overflow:visible;
}
   
   .k-list-container.k-popup-dropdowntree .k-check-all {
    margin: 10px 15px 0;
}
   /*#ddlUsers .k-list-filter {
    display: none;
}*/
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
            <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        //empty datasource
        var emptyDataSource = new kendo.data.DataSource({
            data: []
        });

        function Bind_Customer() {
            $("#ddlCustomers").kendoDropDownList({
                optionLabel: "Select Customer",
                filter: "contains",
                //autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "CustomerName",
                dataValueField: "CustomerID",
                change: ChangeCustomer,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =AVACOM_RLCS_API_URL%>GetMyAssignedCustomers?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {                            
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_EntityBranch() {            
            var customerID = -1;

            if ($('#ddlTreeEntityBranch').data('kendoDropDownTree')) {
                $('#ddlTreeEntityBranch').data('kendoDropDownTree').destroy();
                $('#divBranches').empty();
                $('#divBranches').append('<input id="ddlTreeEntityBranch" data-placeholder="Entity/Branch" style="width: 98%;" />');
            }
            var customerID = -1;
            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();
            }

            if (customerID !== -1) {
                $("#ddlTreeEntityBranch").kendoDropDownTree({
                    placeholder: "Entity/Branch",
                    checkboxes: {
                        checkChildren: true
                    },
                    //checkboxes: true,
                    checkAll: true,
                    autoWidth: true,
                    checkAllTemplate: "Select All",
                    tagMode: "single",
                    dataTextField: "Name",
                    dataValueField: "ID",
                    change: ApplyFilter,
                    dataSource: {
                        transport: {
                            read: {
                                url: '<%=AVACOM_RLCS_API_URL%>GetAllEntitiesLocationsList?customerID=' + customerID,
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            }
                        },
                        schema: {
                            data: function (response) {
                                return response.Result;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    }
                });
            } else {
                $("#ddlTreeEntityBranch").kendoDropDownTree({
                    dataSource: emptyDataSource,
                    placeholder: "Entity/Branch",
                });
            }
        }

        function Bind_Roles() {
            $("#ddlUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    
                },
                index: 0,
                dataSource: [
                    { text: "Role", value: "-1" },
                    { text: "Performer", value: "3" },
                    { text: "Reviewer", value: "4" }
                ]
            });
        }

        function Bind_AssignedUsers() {
            var customerID = -1;
            debugger;
            if ($('#ddlUsers').data('kendoDropDownTree')) {
                $('#ddlUsers').data('kendoDropDownTree').destroy();
                $('#divUsers').empty();
                $('#divUsers').append('<input id="ddlUsers" data-placeholder="User" style="width: 98%;" />');
            }
            var customerID = -1;
            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();
            }

            var apiURL = '';

            if (customerID !== -1) {
                apiURL = '<%=AVACOM_RLCS_API_URL%>GetUsers?custID=' + customerID + '&distID=<% =distID%>&spID=<% =spID%>';
            } else {
                apiURL = '<% =AVACOM_RLCS_API_URL%>GetAssignedSPOCs?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>';
            }

            $("#ddlUsers").kendoDropDownTree({
                placeholder: "Users",
                autoClose: false,
                tagMode: true,
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "UserName",
                dataTextField: "UserName",
                //dataValueField: "UserID",
                change: ApplyFilter,
                dataSource: {
                    transport: {
                        read: {
                            url: apiURL,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            if (response != null && response != undefined) {
                                return response.Result;
                            }
                        },
                        total: function (response) {
                            if (response.Result != null && response.Result != undefined) {
                                debugger;
                                if (response.Result.length > 0) {

                                }
                                return response.Result.length;
                            }
                        }
                    },
                    pageSize: 10,
                }
            });
        }

        function Bind_ComplianceType() {
            $("#ddlComplianceType").kendoDropDownList({
                placeholder: "Registers/Returns/Challans",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    
                },
                index: 0,
                dataSource: [
                    { text: "All", value: "-1" },
                    { text: "Registers", value: "Registers" },
                    { text: "Returns", value: "Returns" },
                    { text: "Challans", value: "Challans" },
                ]
            });
        }

        var exportExcelName = 'ComplianceAssignmentReport-' + '<%= DateTime.Now.ToString("ddMMyyyy") %>' + '.xlsx';

        function BindGrid() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<%=AVACOM_RLCS_API_URL%>GetComplianceAssignment?userID=<% =loggedInUserID%>&profileID=<% =loggedUserProfileID%>&custID=<% =customerID%>&distID=<% =distID%>&spID=<% =spID%>&roleCode=<% =loggedUserRole%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            if (response != null && response != undefined) {
                                return response.Result;
                            }
                        },
                        total: function (response) {
                            if (response.Result != null && response.Result != undefined) {
                                if (response.Result.length > 0) {

                                }
                                return response.Result.length;
                            }
                        }
                    },

                    pageSize: 10
                },
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                    filterable: true
                },
                excelExport: function (e) {
                    var columns = e.workbook.sheets[0].columns;
                  
                    columns[0].autoWidth = true;
                    columns[1].autoWidth = true;
                    columns[2].autoWidth = false;
                    columns[2].width = 550;
                    columns[3].autoWidth = true;
                    //columns[4].autoWidth = true;

                    columns[4].autoWidth = false;
                    columns[4].width = 100;//width adjusted
                    columns[5].autoWidth = false;//width adjusted
                    columns[5].width = 170;//width adjusted
                    columns[6].autoWidth = false;//width adjusted
                    columns[6].width = 170;//width adjusted
                   
                    
                },
                toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: false,
                    pageSizes: true,
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                selectable: true,
                
                columns: [
                    
                     {
                         field: "CustomerName", title: 'Customer', filterable: { multi: true, search: true },
                         width: "13%;", attributes: { style: 'white-space: nowrap;' },
                     },
                    {
                        field: "Branch", title: 'Location', filterable: { multi: true, search: true },
                        width: "10%;", attributes: { style: 'white-space: nowrap;' },
                    },


                    {
                        hidden: true,
                        field: "ActName", title: 'Act',menu:false, filterable: { multi: true, search: true },
                        width: "10%", attributes: { style: 'white-space: nowrap;' },
                    },
                    { field: "ComplianceID", title: 'Comp.Id', width: "10%;", attributes: { style: "vertical-align: top;white-space: nowrap;" } },
                    {
                        field: "ShortForm", title: 'Compliance', filterable: { multi: true, search: true },
                        width: "15%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "RequiredForms", title: 'Form', filterable: { multi: true, search: true },
                        width: "9%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "Frequency", title: 'Frequency', filterable: { multi: true, search: true },
                        width: "11%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "Performer", title: 'Performer', filterable: { multi: true, search: true },
                        width: "11%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "Reviewer", title: 'Reviewer', filterable: { multi: true, search: true },
                        width: "11%", attributes: { style: 'white-space: nowrap;' },
                    }
                ]
            });
            $("#btnExcel").kendoTooltip({
                //   filter: ".k-grid-edit3",
                content: function (e) {
                    return "Export to Excel";
                }
            });
            $("#grid").kendoTooltip({
                filter: "td:nth-child(1)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.CustomerName;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Branch;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ComplianceID;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ShortForm;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.RequiredForms;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Frequency;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(8)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Performer;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(9)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Reviewer;
                    return content;
                }
            }).data("kendoTooltip");
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            $('#iframeReAssign').attr('src', 'about:blank');
        }        

        function ShowReAssignPopup(CustomerID) {            
            $('#divReAssignCompliance').show();

            var myWindowAdv = $("#divReAssignCompliance");

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                //maxHeight: '90%',
                //minHeight:'50%',
                title: "Re-Assign Compliance",
                visible: false,
                actions: ["Close"],
                open: onOpen,
                close: onClose
            });

            $('#iframeReAssign').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeReAssign').attr('src', "../RLCS/ClientSetup/RLCS_ComplianceReAssign.aspx?CustID=" + CustomerID);

            return false;
        }

        function OpenReAssignPopup(e) {            
            e.preventDefault();
            var customerID = -1;

            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();

                if (customerID != -1)
                    ShowReAssignPopup(customerID);
            }
        }

        function ExportToExcel(e) {            
            e.preventDefault();
            //kendo.ui.progress($("#grid"), true);

            // trigger export of the products grid
            $("#grid").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#grid"), false);
            return false;
        }
        
        $(document).ready(function () {
            fhead('My Reports/Compliance Assignment Report');

            $('#divReAssignCompliance').hide();

            Bind_Customer();
            Bind_EntityBranch();
            Bind_AssignedUsers();
            //Bind_Roles();
            Bind_ComplianceType();

            BindGrid();

            $("#btnReAssign").click(function (e) {
                OpenReAssignPopup(e);
            });

            $("#btnExcel").click(function (e) {
                ExportToExcel(e);
            });

            kendo.ui.progress($("#grid"), false);

            $('#btnClearFilter').css('display', 'none');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div class="toolbar col-md-12 colpadding0">
            <div class="row">
                <div class="col-md-1 colpadding0" style="width: 25%;">
                    <input id="ddlCustomers" data-placeholder="Customer" style="width: 98%;" />
                </div>

                <div id="divBranches" class="col-md-1 colpadding0" style="width: 25%;">
                    <input id="ddlTreeEntityBranch" data-placeholder="Entity/Branch" style="width: 98%;" />
                </div>

                <div id="divUsers" class="col-md-1 colpadding0" style="width: 20%;">
                    <input id="ddlUsers" data-placeholder="User" style="width: 98%;" />
                </div>

                <div class="col-md-1 colpadding0" style="width: 10%;z-index:1000">
                    <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)"style="margin-left: 3px;"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                    <input id="ddlUserRole" data-placeholder="Role" style="width: 98%; display: none" />
                    <%--<input id="ddlComplianceType" data-placeholder="RRC" style="width: 98%;" />--%>
                </div>

                <div class="col-md-1 colpadding0" style="width: 25%; text-align: right; margin-left: -70px;">
                    <button id="btnReAssign" class="btn btn-primary">Re-Assign</button>                    
                    <button id="btnExcel" class="btn btn-primary" data-placement="bottom"style="margin-left:27px;"><span class="k-icon k-i-excel"></span>Export</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-2" style="width: 13.6%;">
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 10px;">
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 0px;">
                </div>
                <div class="col-md-2" style="width: 3%;">
                </div>
                <div class="col-md-1" style="padding-left: 435px;">
                    <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filterscustomerstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filterrole">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filteruser">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filterrrc">&nbsp;</div>
        <div id="grid"></div>
    </div>

    <div id="divReAssignCompliance" >
        <iframe id="iframeReAssign" style="width: 100%; height: 100%; border: none;"></iframe>
    </div>
</asp:Content>
