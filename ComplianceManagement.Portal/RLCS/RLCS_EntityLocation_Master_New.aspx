<%@ Page Title="Entity/Branch :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EntityLocation_Master_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EntityLocation_Master_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

   <%-- <style type="text/css">
        div.k-window-content {
            position: relative;
            height: 100%;
            padding: .58em;
             overflow:hidden;
            outline: 0;
        }
        .k-grid-content {
            min-height: auto;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
            border-width:0 1px 0 0;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0 1px 0px 0px; 
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 0 0px;
            padding-left:30px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-icon-plus {
            background-position: -48px -64px;
        }

        .k-i-foo {
            background-color: #f00;
            background-image: url(".place.your.own.icon.here.");
        }

        .k-grid tbody button.k-button {
            min-width: 30px;
            min-height: 25px;
        }

        .k-icon, .k-tool-icon {
            position: relative;
            display: inline-block;
            overflow: hidden;
            width: 1em;
            height: 1em;
            text-align: center;
            vertical-align: middle;
            background-image: none;
            font: 16px/1 WebComponentsIcons;
            speak: none;
            font-variant: normal;
            text-transform: none;
            text-indent: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: inherit;
        }
        div.k-confirm
                {
                    width:411px;
                    left:500.5px;
                }
   
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
         .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 1px 0px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 22px;
            vertical-align: middle;
        }
           .k-filter-row th, .k-grid-header th[data-title='Action'] {
            border-width: 0px 0px 0px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color:#535b6a;
            height: 22px;
            text-align:center;
        }  
    </style>--%>
     <style type="text/css">
         .k-textbox>input{
         text-align:left;
     }

         .btn{
             height:36px !important;
         }

          label.k-label:hover{
              color:#1fd9e1;
          }
             .k-grid-content {
            min-height: auto !important;
        }
               .k-dropdown-wrap-hover,.k-state-default-hover, .k-state-hover, .k-state-hover:hover {
    color: #2e2e2e;
    background-color: #d8d6d6 !important;
}
           .k-multiselect-wrap .k-input {
               padding-top:6px;
           }
           .k-filter-row th, .k-grid-header th[data-title='Action'] {
            text-align:center;
        }  
        .k-grid tbody .k-button {
           min-width: 19px !important;
           min-height: 25px;
           background-color: transparent;
           border: none;
           margin-left: 4px;
           margin-right: 4px;
           padding-left: 2px;
           padding-right: 2px;
           color:black;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
             .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
            .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }
         .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }
           .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
           .k-grid, .k-listview {
               margin-top: 10px;
           }
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }
        
              .k-tooltip-content{
         width: max-content;
         
        
         }
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0px -3px;
            line-height: normal;
        }
        .k-calendar-container {
    background-color: white;
    width: 217px;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
            margin-top:0px;
        }


        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

       
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }
       .k-calendar .k-alt, .k-calendar th, .k-dropzone-hovered, .k-footer-template td, .k-grid-footer, .k-group, .k-group-footer td, .k-grouping-header, .k-pager-wrap, .k-popup, .k-toolbar, .k-widget .k-status {
             background-color: #f5f5f5;
             /*width: 98.5%;*/
             width:auto !important;
        }
        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            margin-left:2.7px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }


        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: 0px;
            border-width:0px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: #535b6a;
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
            height:34px;
        }
        .k-multiselect-wrap, .k-floatwrap{
            height:34px;
        }
        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: max-content !important;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 3px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
            margin:-1px;
        }
        .k-dropdown .k-input, .k-dropdown .k-state-focused .k-input, .k-menu .k-popup, .k-multiselect .k-button, .k-multiselect .k-button:hover {
    color: #515967;
    padding-top: 5px;
}
        .k-tooltip{
            margin-top:5px;
        }
       
    </style>
 
    <script type="text/javascript">

        function BindGrid(){
            var grid = $('#treelist').data('kendoTreeList');
            if(grid != undefined) {
                $('#treelist').data('kendoTreeList').destroy();
                $('#treelist').empty();
            }

            var customerID=-1;
            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerID=$("#ddlCustomer").val();
            } else {
                customerID=<%= CustId%>;
            }

            var gridview = $("#treelist").kendoTreeList({
                dataSource: { 
                    //serverPaging: false,
                    //pageSize: 10,
                    transport: {
                        read: {                        
                            url: '<%=avacomRLCSAPI_URL%>GetCustomerBranchDetail?customerID=' + customerID,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    batch: true,
                    //pageSize: 10,
                    schema: {
                        data: function (response) {
                            if (response.Result != null)
                                return response.Result;
                        },
                        total: function (response) {
                            if (response.Result != null && response.Result != undefined) {
                                if (response.Result.length > 0) {

                                }
                                return response.Result.length;
                            }
                        },
                        model: {
                            id: "ID",
                            parentId: "ParentID",
                            fields: {
                                ID: { type: "number", nullable: false },
                                ParentID: { field: "ParentID", nullable: true }
                            },
                            expanded: true
                        }, 
                    },
                },
                editable:true,
                sortable: true,
                filterable: true,
                reorderable: true,
                resizeable: true,
                multi:true,
                selectable:true,
                //serverPaging: true,
                //pageable:true,
                pageable: {
                    //numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    //pageSize: 10,
                    //buttonCount: 3,
                },

                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                //toolbar: [{ name: "create", text: " " }],
                columnMenu: true,
                columns: [
                    { hidden: true, field: "Type",menu:false, },
                    { field: "Name", expandable: true, title: "Entity/Branch", width: "20%", 
                        filterable: {
                            //extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { field: "CM_ClientID", title: "Client ID", width: "10%",
                        filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { field: "CM_State", title: "State Code", width: "13%",
                        filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { field: "SM_Name", title: "State", width: "12%",
                        filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { field: "CM_City", title: "Location Code", width: "15%",
                        filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { field: "LM_Name", title: "City", width: "10%",
                        filterable: {
                            extra: true,
                            search: true,
                            multi: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { field: "CM_EstablishmentType", title: "Establishment Type", hidden: true,menu:false, },
                    {
                        title: "Action",
                        command: [
                                 //{ name: "View", text: " ", imageClass: "k-i-info", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" },
                                { name: "x", text: " ", imageClass: "k-i-edit", iconClass: ".k-icon .k-i-eye ", className: "my-edit ob-download " },
                                { name: "del", text: " ", iconClass: ".k-icon k-i-eye", imageClass: "k-i-delete", className: "my-del " },
                                { name: "Branch", text: " ", iconClass: ".k-icon k-i-plus", imageClass: "k-i-plus", className: "my-branch " }
                                                        
                        ], lock: true,
                        width: "13%",
                       
                    }
                ],
                dataBound: onDataBound,
              
            });

        }

        function ClearAllFilterMain(e) {

            e.preventDefault();

            $("#ddlCustomer").data("kendoDropDownList").value("");
            $('#ClearfilterMain').css('display', 'none');

            $("#treelist").data("kendoTreeList").dataSource.data([]);
            $("#txtSearch").val('');
        
        };

        function Bind_Customers() {
            $("#ddlCustomer").kendoDropDownList({
                placeholder: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            <%--url: '<%=avacomRLCSAPIURL%>GetCustomers?custID=' + <%=custID%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>,--%>
                            url: '<%=avacomRLCSAPI_URL%>GetHRCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=RoleCode%>&prodType=2',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    }
                }
            });
        }
       
        $(document).ready(function () {

            BindGrid();

            Bind_Customers();

            var dropdownlist = $("#ddlCustomer").data("kendoDropDownList");
            dropdownlist.value(Number(<%= CustId%>));
            //dropdownlist.refresh();

            $('#txtSearch').keyup(function () {
                  
                if($(this).val()){
                    $('#ClearfilterMain').show();
                }
                else {
                    $('#ClearfilterMain').hide();
                }
            });

            $("#btnSearch").click(function (e) {

                e.preventDefault();      
                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#treelist").data("kendoTreeList");
                    gridview.dataSource.query({
                        //page: 1,
                        //pageSize: 50,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "Name", operator: "contains", value: $x },
                              { field: "CM_ClientID", operator: "contains", value: $x },
                              { field: "CM_State", operator: "contains", value: $x },
                              { field: "SM_Name", operator: "contains", value: $x },
                              { field: "CM_City", operator: "contains", value: $x },
                              { field: "LM_Name", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#treelist").data("kendoTreeList").dataSource;
                    dataSource.filter({});
                    BindGrid();
                }
                // return false;
            });

            $('#txtSearch').on('keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) { 
                    e.preventDefault();
                    return false;
                }
            });

            $('#ddlCustomer').change(function () {
                $('#hdnSelectedCustomerId').val($(this).val());
                BindGrid();
                if($(this).val()){
                    $('#ClearfilterMain').show();
                }
                else{
                    $('#ClearfilterMain').hide();
                }
            });

            HideUploadSelected();
            $("#btnAddEntity").kendoTooltip({ content: "Add New-Entity" });
            $("#btnAddBranch").kendoTooltip({ content: "Add New-Branch" });
            $("#btnUpload").kendoTooltip({ content: "Upload Entity/Branch" });
            $("#ContentPlaceHolder1_btnPaycode").kendoTooltip({ content: "Paycode" });
            //tooltip code for asp button type
            //$("#ContentPlaceHolder1_btnAttMapping").kendoTooltip({content:"Attendance Mapping"});
            $('#divRLCSCustomerBranchesDialog').hide();
            $('#divRLCSCustomerLocationDialog').hide();
            $('#divUploadLocationDetails').hide();            
            $('#divRLCSEmployeeUploadDialogBulk').hide();
            $('#divRLCSEmployeeUploadDialogBulkLoc').hide();
            fhead('Masters/ Entity-Branch');
            //debugger;

           
            $("#treelist").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "bottom",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if ($.trim(content) != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    //else
                        //e.preventDefault();
                }
            }).data("kendoTooltip");
            $("#treelist").kendoTooltip({
                filter: ".my-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#treelist").kendoTooltip({
                filter: ".my-del",
                content: function (e) {
                    return "Delete";
                }
            }); 
            $("#treelist").kendoTooltip({
                filter: ".my-branch",
                content: function (e) {
                    return "Add Branch";
                }
            }); 
            $("#btnBulkUploadSelected").kendoTooltip({
              content: function(e){
                  return "Update Selected Column Entity";
                   }
                });
            $("#btnUploadSelectedLocation").kendoTooltip({
                position:"bottom-left",
                content:function(e){
                    return "Update Selected Column Branch";
                }
                });
           });

          
        function onDataBound(e) {

            var data = this.dataSource.view();
            for (var i = 0; i < data.length; i++) {
                var uid = data[i].uid;
                var row = this.table.find("tr[data-uid='" + uid + "']");
                if (data[i].BranchType == "B") {
                    row.find(".my-branch").hide();
                }
            }

        }
     
        
        $(document).on("click", "#treelist tbody tr .ob-overviewMain", function (e) {
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            
            if (item.BranchType == "E") {
                OpenRLCSEntityClientPopup(item.ID, "1");
            }
            if (item.BranchType == "B") {
                OpenRLCSLocationBranchPopup(item.ID, "1");
            }
            return false;
        });

        // $(document).on("click", "#treelist tbody tr .my-edit", function (e) {
        $(document).on("click", "#treelist tbody tr .my-edit", function (e) {
            //debugger;            
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            
            if (item.BranchType == "E") {
                OpenRLCSEntityClientPopup(item.ID, "1");
            }
            if (item.BranchType == "B") {
                OpenRLCSLocationBranchPopup(item.ID, "1");
            }
            return false;
        });

        $(document).on("click", "#treelist tbody tr .my-del", function (e) {
            var tr = $(e.target).closest("tr");
           
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            var custBranchID = item.ID;
            var ConfirmationUrl = "";
            if (item.BranchType == "E") {
                ConfirmationUrl = "DeleteCustomerEntityConfirmation"
            }
            else if (item.BranchType == "B"){
                ConfirmationUrl="DeleteCustomerBranchConfirmation";
            }

            var $tr = $(this).closest("tr");
            window.kendoCustomConfirm("Are you  want to delete this?", "Confirm").then(function () {
                $.ajax({
                        type: 'POST',
                        url: '<%=avacomRLCSAPI_URL%>' + ConfirmationUrl + '?CustomerBranchID=' + custBranchID,
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            xhr.setRequestHeader('Content-Type', 'application/json');
                        },
                        success: function (result) {
                            debugger;
                            if (result.Result) {
                                DeleteBranch(custBranchID);
                            }
                            else {
                                window.kendoCustomConfirm("Compliance Assign to this branch, Are you  want to delete this?", "Confirm").then(function () {
                                    DeleteBranch(custBranchID);
                                }, function () {

                                });

                            }
                            $('#treelist').data('kendoTreeList').dataSource.read();
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }

                    });
            }, function () {
                // For Cancel
            });

            return false;
        });
        function DeleteBranch(custBranchID) {
             $.ajax({
                        type: 'POST',
                        url: '<%=avacomRLCSAPI_URL%>DeleteCustomerBranch?CustomerBranchID=' + custBranchID,
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            xhr.setRequestHeader('Content-Type', 'application/json');
                        },
                        success: function (result) {
                            alert("Deleted Successfully");
                            console.log(result);
                            $("#treelist").data("kendoTreeList").dataSource.read();
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }

                    });
        }

        $(document).on("click", "#treelist tbody tr .my-branch", function (e) {
            debugger;
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            if (item.ID != undefined && item.ID != null)
                if(item.BranchType=="E")
                    OpenRLCSLocationBranchPopup(item.ID, "");
                else
                    OpenRLCSLocationBranchPopup(item.ParentID, "");
        });

        function OpenRLCSEntityClientPopup(branchID, mode) {
            var customerId = -1;
            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerId = $("#ddlCustomer").val();
            }
            else {
                customerId = <%=CustId%>;
            }
            $('#divRLCSCustomerBranchesDialog').show();

            var myWindowAdv = $("#divRLCSCustomerBranchesDialog");

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                content: "/RLCS/RLCS_CustomerBranchDetails_New.aspx?CustomerID=" + customerId + "&Mode=" + mode + "&BranchID=" + branchID,
                iframe: true,
                title: "Entity/Client Details-HR Compliance",
                visible: false,
                actions: ["Close"],
                close: onCloseEntityLocation
            });

            myWindowAdv.data("kendoWindow").center().open();           

            return false;
        }

        function OpenRLCSLocationBranchPopup(ID, Master) {
            $('#divRLCSCustomerLocationDialog').show();

            var myWindowAdv = $("#divRLCSCustomerLocationDialog");

            myWindowAdv.kendoWindow({
                width: "87%",
                height: "90%",
                title: "Branch Details-HR+ Compliance",
                visible: false,
                actions: ["Close"],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            //$('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=" + Master);
            
            if (Master == "")
            {
                $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=" + Master + "&Edit=NewBranch");
            }
            else
            {
                $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=" + Master + "&Edit=YES");
            }
            return false;
        }

        function onClose() {
            $('#treelist').data('kendoTreeList').dataSource.read();
        }
        function onCloseEntityLocation() {
            var customerId = -1;
            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerId = $("#ddlCustomer").val();
            }
            else {
                customerId = <%=CustId%>;
            }
            $('#treelist').data('kendoTreeList').dataSource.read();
            if ($('#divRLCSCustomerBranchesDialog').val()==="Save") {
                $('#divRLCSCustomerBranchesDialog').val("");
                setTimeout(function () {
                    if (confirm("Please Add Paycode for Entity.")) {
                        window.location.replace("/RLCS/RLCS_PaycodeMappingDetails_Updated.aspx?CustomerID=" + customerId);
                    }
                    else{
                        window.location.reload();
                    }
                }, 100);
            }else{
                window.location.reload();
            }
        }
        
        function OpenUploadPopup() {
            var ServiceProviderID='<%=ServiceProviderID%>';
            $('#divUploadLocationDetails').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeLocationUpload').attr('src', "/Setup/UploadLocationFiles?SPID="+ServiceProviderID);
            var myWindowAdv = $("#divUploadLocationDetails");

            myWindowAdv.kendoWindow({
                width: "50%",
                height: "40%",
                title: "Upload Entity/Branch",
                visible: false,
                actions: ["Close"],
                close: function(){
                    window.location.reload()
                }
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }
        //ADD NEW FUNCTION
         function OpenWindowSelectedBulkUploadEntity() {
            var customerId = -1;
            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerId = $("#ddlCustomer").val();
            }
            else{
                customerId = <%=CustId%>;
            }
            var ServiceProviderID='<%=ServiceProviderID%>';
            $('#divRLCSEmployeeUploadDialogBulk').show();
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulk");
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                title: "Update Selected Column Client/Entity",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
           
            $('#iframeUploadBulk').attr('src', "/Setup/UploadEntityUpdateSelected?CustID="+ customerId +"&UserID="+ <%=UserId%> +"&SPID="+ServiceProviderID); 
            return false;
        }
          //ADD NEW FUNCTION
         function OpenWindowSelectedBulkUploadLocation() {
            var ServiceProviderID='<%=ServiceProviderID%>';
            var customerId = -1;
            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerId = $("#ddlCustomer").val();
            }
            else{
                customerId = <%=CustId%>;
            }
            $('#divRLCSEmployeeUploadDialogBulkLoc').show();
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulkLoc");
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                title: "Update Selected Column Location/Branch",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            $("#ContentPlaceHolder1_updateProgress").show();
            $('#iframeUploadBulkLoc').attr('src', "/Setup/UploadLocationUpdateSelected?CustID="+ customerId +"&UserID="+<%=UserId%>+"&SPID="+ServiceProviderID);
            return false;
         }
        //RLCS only
        function HideUploadSelected()
        {
            var ServiceProviderID='<%=ServiceProviderID%>';
            if (ServiceProviderID==94) {
                $("#btnUploadSelectedLocation").show();
            }
            else {
                $("#btnUploadSelectedLocation").hide();
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton> <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <asp:HiddenField ID="hdnSelectedCustomerId"
        runat="server"
        ClientIDMode="Static" />

    <div class="row" style="margin-top: 10px">
        <div class="col-md-6 colpadding0">
            <div class="col-sm-4" style="padding-right:0px">
                <input id="ddlCustomer" style="width: 95%" />
            </div>
            <div class="col-md-5 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 69%; height: 36px" placeholder="Type to search" autocomplete="off" />
                <button id="btnSearch" class="btn btn-primary" style="font-weight: 400;">Search</button>
            </div>
            <div class="col-sm-3" style="padding-left:1px">
                <button id="ClearfilterMain" class="btn btn-primary" style="margin-left: 10px; display:none; float: none; margin-top: 0px; height: 36px;" onclick="ClearAllFilterMain(event)">
                    <span class="k-icon k-i-filter-clear";">
                    </span><font size="2" style="zoom: 1.1">Clear Filter</font>
                </button>
            </div>
        </div>
        <div class="col-md-6 colpadding0">
            <div class="col-sm-2">
                <button id="btnAddEntity" class="btn btn-primary k-btnUpload" style="font-weight: 400;" onclick="return OpenRLCSEntityClientPopup('0','0')">
                    <span class="k-icon k-i-plus-outline" title="Hello World"></span>Entity
                </button>
            </div>
            <div class="col-sm-2">
                <button id="btnUpload" class="btn btn-primary k-btnUpload" onclick="return OpenUploadPopup()" style="font-weight: 400;">
                    <span class="k-icon k-i-upload"></span>Upload
                </button>
            </div>
            <div class="col-sm-2">
                <%-- <button id="btnPaycode1" class="btn btn-primary"onclick="btnPaycode_Click"style="margin-right:3px;font-weight:400;"><span></span>PayCode</button>--%>
                <asp:Button Text="Paycode" ID="btnPaycode" runat="server" CssClass="btn btn-primary" OnClick="btnPaycode_Click" Style="margin-right: 3px; font-weight: 400;"></asp:Button>
            </div>
            <div class="col-sm-4" style="display:none;">
                <asp:Button Text="Attendance Mapping" ID="btnAttMapping" runat="server" CssClass="btn btn-primary" OnClick="btnAttendanceMapping_Click" Style="font-weight:400;"></asp:Button>
            </div>
            <div class="col-sm-2 colpadding0">
                <button id="btnBulkUploadSelected" class="btn btn-primary k-btnUpload" onclick="return OpenWindowSelectedBulkUploadEntity()">
                    <span class="k-icon k-i-upload"></span><%--Update Selected Column Entity--%>
                </button>
                <button id="btnUploadSelectedLocation" class="btn btn-primary k-btnUpload" onclick="return OpenWindowSelectedBulkUploadLocation()" style="font-weight: 400;">
                    <span class="k-icon k-i-upload"></span><%--Update Selected Column Branch--%>
                </button>
            </div>
        </div>
    </div>


    <div class="row col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
        <div id="treelist"></div>
     <%-- <div class="k-pager-wrap k-grid-pager k-widget k-floatwrap" data-role="treelistpager"></div>--%>
    </div>

    <div id="divRLCSCustomerBranchesDialog">
        <iframe id="iframeRLCSEntityClient" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>

    <div id="divRLCSCustomerLocationDialog">
        <iframe id="iframeRLCSLocationBranch" style="width: 100%; height: 99%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>

    <div id="divUploadLocationDetails">
        <div class="chart-loading" id="loader"></div>
        <iframe id="iframeLocationUpload" style="width: 580px; height: 200px; border: none; overflow:hidden"></iframe>
    </div>
    <div id="divRLCSEmployeeUploadDialogBulk">
        <iframe id="iframeUploadBulk" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>
      <div id="divRLCSEmployeeUploadDialogBulkLoc">
        <iframe id="iframeUploadBulkLoc" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>
    
</asp:Content>
