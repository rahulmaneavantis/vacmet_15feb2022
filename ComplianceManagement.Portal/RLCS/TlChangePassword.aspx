﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TlChangePassword.aspx.cs" EnableEventValidation="false" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.TlChangePassword" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>Change Password :: AVANTIS - Products that simplify</title>

    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
     <script src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <!-- Custom styles -->
    <link href="../NewCSS/style.css" rel="stylesheet" />
    <link href="../NewCSS/style-responsive.css" rel="stylesheet" />
   
    <%--<link href="../NewCSS/ionicons.min.css" rel="stylesheet" />--%>
    <script src="../Newjs/ChangePassword.js"></script>
    <script src="../Newjs/sweetalert-dev.js"></script>
    
  <%--<script src="../Newjs/passwordStrength.js"></script>--%>
    <link href="../NewCSS/modal-popup.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {           
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
        });
    </script>
    
    <style>
  body { background-color:#333; font-family:'Roboto';}
  h1 { color:#fff;}
  .container { margin:150px auto; max-width:400px;}
.mt-10 { margin-top: 20px; }
.password-box { margin:20px auto
}
.password-progress {
  /*height: 12px;*/
  /*margin-top: 10px;*/
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}

.progress-bar {
  float: left;
  height: 100%;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

.bg-red {
  background: #E74C3C;
  border: 1px solid #E74C3C;
}

.bg-orange {
  background: #F39C12;
  border: 1px solid #F39C12;
}

.bg-green {
  background: #1ABB9C;
  border: 1px solid #1ABB9C;
}

.form-description {
    padding: .5em;
    /* z-index: 1000; */
    position: fixed;
    right: 391px;
    top: 290px;
    width: 240px;
    color: #6e6e6e;
    font-size: 12px;
    background: #fff;
    border-radius: 3px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1);
    box-shadow: inset 0 1px 0 rgba(255,255,255,.1);
    min-width: 200px;
    border: 1px solid #e8e5e5;
}
</style>

    <style type="text/css">
        .VeryPoorStrength {
            background: Red;
            color: White;
            font-weight: bold;
        }

        .WeakStrength {
            background: Gray;
            color: White;
            font-weight: bold;
        }

        .AverageStrength {
            background: orange;
            color: black;
            font-weight: bold;
        }

        .GoodStrength {
            background: blue;
            color: White;
            font-weight: bold;
        }

        .ExcellentStrength {
            background: Green;
            color: White;
            font-weight: bold;
        }

        .BarBorder {
            width: 400px;
            height: 1px;
        }

        .BarBorder1 {
            margin-left: 400px;
            margin-bottom: 1px;
        }
    </style>
     <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92029518-1', 'auto');
        ga('send', 'pageview');

        function settracknew(e, t, n, r) {
            try {
                ga('send', 'event', e, t, n + "#" + r)
            } catch (t) { } return !0
        }
        function clear() {
            debugger;
            document.getElementById("txtoldpassword").value = "";
            document.getElementById("txtNewPassword").value = "";
            document.getElementById("txtConfirmPassword").value = "";
            document.getElementById("txtanswer").value = "";
        }

        function settracknewnonInteraction(e, t, n, r) {
            ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
        }
        var OldPasswordmatch = 0;

        function ValidateOldPassword() {
            var OldPassword = $('input[id$=txtOldPassword]').val();
            var ApiPath = $('input[id$=hdnApiPath]').val();

            $.ajax({
                type: 'post',
                url: ApiPath + 'Login/ValidateOldPassword?Password=' + encodeURIComponent(OldPassword),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', $('input[id$=hdnAuthKey]').val());
                    xhr.setRequestHeader('X-User-Id', $('input[id$=hdnProfileID]').val());  //
                },
                success: function (status) {


                    if (status == 1) {
                        OldPasswordmatch = 1;
                        $('#divoldpwd').addClass('has-success');
                        $('#divoldpwd').removeClass('has-error');
                        // $('#lblsucess').css('display', 'block');
                        // $('#lblerror').css('display', 'none');
                    }
                    else {
                        OldPasswordmatch = 0;
                        $('#divoldpwd').addClass('has-error');
                        swal("Enter Correct Old Password", "", "error");
                        //  $('#lblerror').css('display', 'block');
                        // $('#lblsucess').css('display', 'none');
                        //  clear();
                        $('#txtoldpassword').val('');
                    }
                },
                processData: false,
                contentType: false,
                error: function () {
                    
                    swal("Try Later", "Error while cancelling details", "error");
                   
                }
            });



        }
        function fillSecurityQuestions() {
            $.ajax({
                type: "GET",
                url: "https://tlconnectapi.teamlease.com/api/GeneralInformation/GetQuestionsForChangePassword",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', $('input[id$=hdnAuthKey]').val());
                    xhr.setRequestHeader('X-User-Id', $('input[id$=hdnProfileID]').val());
                },
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var res = msg;
                    $("#ddlquest").get(0).options[$("#ddlquest").get(0).options.length] = new Option('Select Security Question', '0');
                    $.each(msg, function (index, item) {
                        $("#ddlquest").get(0).options[$("#ddlquest").get(0).options.length] = new Option(item.Name, item.Code);
                    });
                    $("#ddlquest").prop('selectedIndex', 0);
                  //  $(".select2").select2();

                },
                error: function () {
                    swal("Try Later", "Failed to load security questions", "error");
                }
            });


        }
        function UpdatePassword() {

            debugger;
            var OldPassword = $('input[id$=txtOldPassword]').val();
            var NewPassword = $('input[id$=txtNewPassword]').val();
            var ConfirmPassword = $('input[id$=txtConfirmPassword]').val();

            if (NewPassword.length <= 0) {
                swal("", "Please enter new password", "warning");
            }
            else if (ConfirmPassword.length <= 0) {
                swal("", "Please enter confirm password", "warning");
            }

            else if (NewPassword != ConfirmPassword) {
                swal("", "New password and confirm password doesn't match", "warning");

            }

            else if ($("select[id$=ddlquest]").val() == "0") {
                swal("", "Please select security question", "warning");
            }


            else if ($('#txtanswer').val() == "") {
                swal("", "Please enter security answer", "warning");

            }


            else if (NewPassword.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[_#^$@%*&])[A-Za-z\d_#^@$%*&]{8,15}/) == false) {
                $('#divalertmsg').css('display', 'block');

            }
            else if (CheckStrengthwhileSubmission(NewPassword) == false) {

                return (false);
            }
            else {

                var ManagerId = $('input[id$=hdnProfileID]').val();
                var Password = $('input[id$=txtConfirmPassword]').val();
                var ApiPath = $('input[id$=hdnApiPath]').val();
                var questionId = $("select[id$=ddlquest]").val();

                var answer = $('#txtanswer').val();

                if (OldPasswordmatch == 1) {
                $.ajax({
                    type: 'post',
                    url: ApiPath + 'ChangePasswordFirstTime/UpdatePasswordFirstTime?UserID=' + ManagerId + "&Password=" + encodeURIComponent(Password) + "&questionId=" + questionId + "&answer=" + encodeURIComponent(answer),
                    success: function (status) {
                        if (status == 1) {
                          
                   
                                //swal("", "Password Changed Successfully,Please Log in With the New Password", "success");
                                //window.location.href = "/Partners";
                                $('#btnClose').click();
                                swal({
                                    title: "Password changed successfully",
                                    text: "Please Log in With the New Password",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                }, function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "/Login.aspx";
                                    }
                                    else {
                                        window.location.href = "/Login.aspx";
                                    }
                                });
                                clear();
                            
                       
                        }

                        else {
                            $('#hdnOldPasswrdmatch').val(0);
                            $('#txtanswer').val('');
                            swal("Try Later", "Error while updating passowrd", "error");
                           
                            return (false);
                        }
                    },
                    processData: false,
                    contentType: false,
                    error: function () {
                        $('#hdnOldPasswrdmatch').val(0);
                       
                        swal("Try Later", "Error while updating passowrd", "error");
                      
                        return (false);
                    }
                    });
            }
        else
                {
                  
                    swal("Try Later", "Enter Correct Old Password", "warning");
                    $('#hdnOldPasswrdmatch').val(0);
                   
                    return (false);
        }
            }


        }
    </script>
</head>


<body style="background: #eeeeee;">
    <form class="login-form" runat="server" name="login" id="Form1" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div class="col-md-12 login-form-head">
            <p class="login-img">
                <img src="../Images/TeamLease.png" />
            </p>
        </div>
        <div runat="server" id="divMasterQuestions">
            <%--<asp:UpdatePanel ID="upChangePassword" runat="server" UpdateMode="Conditional">
                <ContentTemplate>--%>
            <div class="login-wrap">
                <h1>Change Password</h1>
                <div class="clearfix" style="height: 10px"></div>
                <div class="input-group" style="width: 100%">
                    <asp:TextBox runat="server" ID="txtOldPassword" MaxLength="15" class="form-control" onchange="ValidateOldPassword();" placeholder="Old Password"
                         TextMode="Password" data-toggle="tooltip" data-placement="right" ToolTip="Old Password" />
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Old Password can not be empty." ControlToValidate="txtOldPassword"
                                runat="server" Display="None" />--%>
                </div>
                <asp:Label ID="lblerror" runat="server" Text="Label" Style="display: none">
                    <i class="ion-alert-circled"><span class="m-l-xs">Wrong password</span></i>
                </asp:Label>
                <asp:Label ID="lblsucess" runat="server" Text="Label" Style="display: none">
                    <i class="ion-checkmark-circled"></i><span class="m-l-xs">Success</span>
                </asp:Label>

                <div class="input-group password-box" style="width: 100%">
                    <asp:TextBox runat="server" ID="txtNewPassword" name="txtNewPassword" MaxLength="15" placeholder="New Password" class="form-control" 
                        onchange="ValidateOldPasswordwithnew();" TextMode="Password" onkeyup="CheckStrength();" 
                        data-toggle="tooltip" data-placement="right" ToolTip="New Password" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="New Password can not be empty." ControlToValidate="txtNewPassword"
                        runat="server" Display="None" />
                </div>

                <div id="divalertmsg" class="form-description" style="display: none; z-index: 1000;">
                    Password should be minimum <span>8-15</span> characters.<br>
                    It should contain minimum one caps letter,one small letter, one number and one special character.<br>
                    Allowed Special characters are <span>@,#,$,%,^,&amp;,*,_</span>
                    <br>
                </div>

                <div class="input-group" style="width: 100%">
                    <asp:TextBox runat="server" ID="txtConfirmPassword" onchange="ValidateNewConfirmPassword();" MaxLength="15" class="form-control" 
                        placeholder="Confirm Password" TextMode="Password" data-toggle="tooltip" data-placement="right" ToolTip="Confirm Password" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Confirm Password can not be empty." ControlToValidate="txtConfirmPassword"
                        runat="server" Display="None" />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="New Passwords and Confirm Password do not match."
                        ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" Display="None" />
                </div>

                <div class="input-group" style="width: 100%">
                    <asp:DropDownList ID="ddlquest" placeholder="Select Security Question" CssClass="form-control input-lg m-bot15" runat="server">
                    </asp:DropDownList>
                </div>

                <div class="input-group" style="width: 100%">
                    <asp:TextBox ID="txtanswer" runat="server" class="form-control" placeholder="Enter Answer"></asp:TextBox>
                </div>
                <div class="clearfix"></div>

                <asp:HiddenField ID="hdnApiPath" runat="server" />
                <asp:HiddenField ID="hdnAuthKey" runat="server" />
                <asp:HiddenField ID="hdnProfileID" runat="server" />
                <asp:HiddenField ID="hdnOldPasswrdmatch" runat="server" />
                <%--<div class="input-group" style="width: 100%">
                            <ajax:PasswordStrength ID="PasswordStrength1" runat="server" TargetControlID="txtConfirmPassword"
                                StrengthIndicatorType="BarIndicator" Enabled="true"
                                HelpStatusLabelID="lblhelp1" PreferredPasswordLength="8"
                                MinimumNumericCharacters="1" MinimumSymbolCharacters="1"
                                BarBorderCssClass="BarBorder"
                                TextStrengthDescriptionStyles="VeryPoorStrength;WeakStrength;AverageStrength;GoodStrength;ExcellentStrength"></ajax:PasswordStrength>
                        </div>--%>
                <div class="clearfix"></div>
                
                <div class="row">
                    <div class="col-md-6">
                        <asp:Button ID="btnSavePassword" CssClass="btn btn-primary btn-lg btn-block" runat="server" Text="Submit" OnClick="btnSavePassword_Click"
                            OnClientClick="UpdatePassword()" />
                    </div>
                    <div class="col-md-6">
                        <asp:Button ID="btnClear" CssClass="btn btn-primary btn-lg btn-block" runat="server" Text="Clear" />
                    </div>
                </div>
                
                <div class="clearfix" style="height: 15px"></div>
                
                <p>
                    <asp:LinkButton ID="lnklogin" runat="server" Text="Click here to go back" CausesValidation="false" PostBackUrl="~/RLCS/RLCS_HRMDashboardNew.aspx"></asp:LinkButton>
                    <%--href="javascript:history.go(-1);"--%>
                </p>

                <asp:ValidationSummary ID="validationSummary2" class="alert alert-block alert-danger fade in" runat="server" />
                <asp:CustomValidator ID="CustomValidator1" runat="server" class="alert alert-block alert-danger fade in" Display="None" EnableClientScript="False"></asp:CustomValidator>
            </div>
            <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </form>
</body>

<script>
    $(document).ready(function () {

        fillSecurityQuestions();
      
        $('#destroy').click(function () {
            obj.destroy();
        });
        $('#reset').click(function () {
            obj2.reset();
        });
     
    });
    function back() {
        window.history.back();
    };
   

  </script>
 
</html>
