﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_ComplianceStatusTransaction.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_ComplianceStatusTransaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>

	<link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
	<link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
	<link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
	<link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="/Newjs/jquery.js"></script>
	<script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
	<script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

	<link href="~/NewCSS/kendo.common.min.css" rel="stylesheet" />
	<link href="~/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
	<link href="~/NewCSS/kendo.default.min.css" rel="stylesheet" />
	<link href="~/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

	<style type="text/css">
		.reviewdoc {
			height: 100%;
			width: auto;
			display: inline-block;
			font-size: 18px;
			position: relative;
			margin: 0;
			line-height: 34px;
			font-weight: 400;
			letter-spacing: 0;
			color: #666;
		}

		tr.spaceUnder > td {
			padding-bottom: 1em;
		}

		.circle {
			/*background-color: green;*/
			width: 15px;
			height: 15px;
			border-radius: 50%;
			display: inline-block;
			margin-right: 20px;
		}

		.panel .panel-heading .panel-actions {
			height: 32px;
		}

		.panel .panel-heading h2 {
			font-size: 18px;
		}

		.btn-primary[disabled] {
			pointer-events: auto;
		}
	</style>
	<script>
		function openInNewTab(url) {
			var win = window.open(url, '_blank');
			win.focus();
		}

		function initializeDatePicker(date) {
			var startDate = new Date();
			$('#<%= tbxDate1.ClientID %>').datepicker({
				dateFormat: 'dd-mm-yy',
				maxDate: startDate,
				numberOfMonths: 1,
				changeMonth: true,
				changeYear: true,
			});

			if (date != null) {
				$("#<%= tbxDate1.ClientID %>").datepicker("option", "defaultDate", date);
			}
		}

		function enableControls1() {
			$("#<%= btnSave1.ClientID %>").removeAttr("disabled");
			$("#<%= rdbtnStatus1.ClientID %>").removeAttr("disabled");
			$("#<%= tbxRemarks1.ClientID %>").removeAttr("disabled");
			$("#<%= tbxDate1.ClientID %>").removeAttr("disabled");
			$("#<%= btnReject1.ClientID %>").removeAttr("disabled");
		}

		function fopendocfileReviewReviewer(file) {
			$('#DocumentReviewPopUp1').modal('show');
			enableControls1();
			$("#<%= docViewerReviewAll.ClientID %>").attr('src', "../docviewer.aspx?docurl=" + file);
			//$('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
		}

		function CloseModal() {
			$('#ComplainceReviewer').modal('show');
			return true;
		}

		function CloseAndBindData() {
			try {
				//window.parent.CloseModal();
				window.parent.$("#divreports").modal("hide");
			} catch (e) {

			}
		}

		$(document).ready(function () {
			$("body").tooltip({ selector: '[data-toggle=tooltip]' });
			debugger;
			$(window.parent.document).find("#divMyCompliances").children().first().hide();
		});

		function funUploadDocument() {
			fileUpload = document.getElementById('fuSampleFile');
			if (fileUpload.value != '') {
				document.getElementById("<%=UploadDocument.ClientID %>").click();
			}
		}

		function funDocumentViewer(filePath) {
			$('#DocumentViewer').modal('show');
			$('#iframeDocViewer').attr('src', "../docviewer.aspx?docurl=" + filePath);
		}

		function kendoCustomAlert(content, title) {
			$("<div></div>").kendoAlert({
				title: title,
				content: content
			}).data("kendoAlert").open();
		}

		function showAlert(message, title) {
			debugger;
			window.kendoCustomAlert(message, title);
		}
	</script>
</head>
<body style="background-color: #f7f7f7;">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="smRLCSComplianceStatusTxnPage" EnablePartialRendering="true" runat="server"></asp:ScriptManager>

		<asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional"
			OnLoad="upComplianceDetails1_Load">
			<ContentTemplate>
				<div style="margin: 5px">
					<div style="margin-bottom: 4px">
						<asp:ValidationSummary ID="vsSummary" runat="server" Style="padding-left: 5%" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup" />
						<asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
							ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
						<asp:Label ID="Labelmsgrev" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
						<asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
						<asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
						<asp:HiddenField runat="server" ID="hdnComplianceType" />
						<asp:HiddenField runat="server" ID="hdnComplianceCode" />
					</div>

					<div>
						<asp:Label ID="Label2" Text="This is a     " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
							maximunsize="300px" autosize="true" runat="server" />
						<div id="divRiskType1" runat="server" class="circle"></div>
						<asp:Label ID="lblRiskType1" Style="width: 300px; margin-left: -27px; font-weight: bold; font-size: 13px; color: #333;"
							maximunsize="300px" autosize="true" runat="server" />
						<asp:Label ID="Label1" Text="     Compliance" Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
							maximunsize="300px" autosize="true" runat="server" />
					</div>

					<div id="ActDetails1" class="row Dashboard-white-widget">
						<div class="dashboard">
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1">
											<a>
												<h2>Act Details</h2>
											</a>
											<div class="panel-actions">
												<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1"><i class="fa fa-chevron-up"></i></a>
											</div>
										</div>
									</div>
									<div id="collapseActDetails1" class="collapse">
										<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
												<table style="width: 100%;">
													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">Act</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblActName1" Style="width: 88%; font-size: 13px; color: #333;"
																autosize="true" runat="server" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">Section/Rule</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblRule1" Style="width: 88%; font-size: 13px; color: #333;"
																autosize="true" runat="server" />
														</td>
													</tr>
												</table>
											</div>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="ComplianceDetails1" class="row Dashboard-white-widget">
						<div class="dashboard">
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">

									<div class="panel panel-default" style="margin-bottom: 1px;">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1">
											<a>
												<h2>Compliance Details</h2>
											</a>
											<div class="panel-actions">
												<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1"><i class="fa fa-chevron-up"></i></a>
											</div>
										</div>
									</div>

									<div id="collapseComplianceDetails1" class="collapse">
										<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
												<table style="width: 100%">
													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">ComplianceID</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblComplianceID1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">Compliance</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblComplianceDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>

													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">Detailed Description</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblDetailedDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>

													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">Penalty</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblPenalty1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>

													<tr class="spaceUnder">
														<td style="width: 15%; font-weight: bold;">Frequency</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:Label ID="lblFrequency1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
												</table>
											</div>
										</fieldset>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div id="OthersDetails1" class="row Dashboard-white-widget">
						<div class="dashboard">
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel panel-default" style="margin-bottom: 1px;">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails1">
											<a>
												<h2>Additional Details</h2>
											</a>
											<div class="panel-actions">
												<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails1"><i class="fa fa-chevron-up"></i></a>
											</div>
										</div>
									</div>

									<div id="collapseOthersDetails1" class="collapse">
										<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
											<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
												<table style="width: 100%">
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Risk Type</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">
															<asp:Label ID="lblRisk1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">

															<asp:UpdatePanel ID="upsample1" runat="server" UpdateMode="Conditional">
																<ContentTemplate>
																	<asp:Label ID="lblFormNumber1" Style="width: 300px; font-size: 13px; color: #333;"
																		maximunsize="300px" autosize="true" runat="server" />
																	<asp:LinkButton ID="lbDownloadSample1" Style="width: 300px; font-size: 13px; color: blue"
																		runat="server" Font-Underline="false" OnClick="lbDownloadSample1_Click" />
																	<asp:Label ID="lblSlash1" Text="/" Style="color: blue;" runat="server" />
																	<asp:LinkButton ID="lnkViewSampleForm1" Text="View" Style="width: 150px; font-size: 13px; color: blue"
																		runat="server" Font-Underline="false" OnClientClick="fopendocReviewfile();" />
																	<asp:Label ID="lblpathsample1" runat="server" Style="display: none"></asp:Label>
																</ContentTemplate>
															</asp:UpdatePanel>
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Regulatory website link</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">
															<asp:LinkButton ID="lnkSampleForm1" Text="" Style="width: 300px; font-size: 13px; color: blue"
																runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">
															<asp:Label ID="lblRefrenceText1" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Location</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">
															<asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Period</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">
															<asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
													<tr class="spaceUnder">
														<td style="width: 25%; font-weight: bold;">Due Date</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 73%;">
															<asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
																maximunsize="300px" autosize="true" runat="server" />
														</td>
													</tr>
												</table>
											</div>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="Step1UpdateCompliance" class="row Dashboard-white-widget">
						<div class="dashboard">
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel panel-default" style="margin-bottom: 1px;">
										<div class="panel-heading">
											<a>
												<h2>Update Compliance Status </h2>
												<%--data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus"--%>
											</a>
											<div class="panel-actions">
												<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
											</div>
										</div>
									</div>

									<div id="Step1UpdateComplianceStatus" class="panel-collapse collapse in">
										<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">

											<% if (flag == "PA")
												{ %>
											<div style="margin-bottom: 7px;" runat="server" id="divUploadDocument">
												<table style="width: 100%">
													<tr>
														<td style="width: 20%;">
															<label style="font-weight: bold; vertical-align: text-top;">Upload-Document(s)</label>
														</td>
														<td style="width: 5%; font-weight: bold;">
															<label style="font-weight: bold; vertical-align: text-top;">: Type</label></td>
														<td style="width: 20%;">
															<asp:RadioButtonList ID="rbDocType" runat="server" RepeatDirection="Horizontal">
																<asp:ListItem Text="Working Files" Value="2" Selected="True"></asp:ListItem>
																<asp:ListItem Text="Compliance" Value="1" data-toggle="tooltip" title="(i.e. Acknowledgement/Return/Challan/Any other document)"></asp:ListItem>
															</asp:RadioButtonList>
														</td>
														<td style="width: 50%;">
															<asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" onchange="fFilesubmit()" Style="color: black" />

															<asp:Button ID="UploadDocument" runat="server" Text="Upload Document" Style="display: none;"
																class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
																CausesValidation="true" />
															<%--OnClick="UploadDocument_Click"--%>
                                                               
														</td>
														<td style="width: 20%;">
															<asp:UpdatePanel runat="server" UpdateMode="Conditional">
																<ContentTemplate>
																	<asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
																</ContentTemplate>
																<Triggers>
																	<asp:PostBackTrigger ControlID="btnUpload" />
																</Triggers>

																<%--  <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="UploadDocument" />
                                                            </Triggers>--%>
															</asp:UpdatePanel>
														</td>

														<td>
															<asp:UpdatePanel runat="server" UpdateMode="Conditional">
																<ContentTemplate>
																	<asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
																</ContentTemplate>
																<Triggers>
																	<asp:PostBackTrigger ControlID="btnSubmit" />
																</Triggers>
															</asp:UpdatePanel>
														</td>
													</tr>
												</table>
											</div>
											<%} %>
											<div style="margin-bottom: 7px;">
												<table style="width: 100%">
													<tr>
														<td style="width: 100%;">
															<asp:GridView runat="server" ID="grdComplianceDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
																PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
																OnRowCommand="grdComplianceDocument_RowCommand" OnRowDataBound="grdComplianceDocument_RowDataBound" OnPageIndexChanging="grdComplianceDocument_OnPageIndexChanging">
																<Columns>
																	<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
																		<ItemTemplate>
																			<%#Container.DataItemIndex+1 %>
																		</ItemTemplate>
																	</asp:TemplateField>

																	<asp:TemplateField HeaderText="Type" ItemStyle-Width="10%">
																		<ItemTemplate>
																			<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
																				<asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
																				<asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("FileType") %>' ToolTip='<%# Eval("FileType") %>'></asp:Label>
																				<%--<asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduledOnID") %>'></asp:Label>
                                                                                <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>--%>
																			</div>
																		</ItemTemplate>
																	</asp:TemplateField>

																	<asp:TemplateField HeaderText="Document" ItemStyle-Width="10%">
																		<ItemTemplate>
																			<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
																				<asp:Label ID="lblDocumentName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
																				<asp:LinkButton ID="lnkRedirectDocument" runat="server" data-toggle="tooltip" Style="color: blue; text-decoration: underline;"
																					OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %> Text='<%# Eval("FileName") %>' />
																			</div>
																		</ItemTemplate>
																	</asp:TemplateField>

																	<asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
																		<ItemTemplate>
																			<asp:Label ID="lblVersion" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Version") %>'></asp:Label>
																		</ItemTemplate>
																	</asp:TemplateField>

																	<asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
																		<ItemTemplate>
																			<asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
																				<ContentTemplate>
																					<asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download" ToolTip="Download" data-toggle="tooltip"
																						CommandArgument='<%# Eval("Id") %>'><img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /></asp:LinkButton>
																					<asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View" ToolTip="View" data-toggle="tooltip"
																						CommandArgument='<%# Eval("Id") %>'><img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View" /></asp:LinkButton>

																					<%
																						if (flag == "PA")
																						{%>
																					<asp:LinkButton ID="lnkDeleteDocument" CssClass="upddoc" runat="server" CommandName="DeleteDoc" ToolTip="Delete" data-toggle="tooltip"
																						CommandArgument='<%# Eval("Id") %>'><img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" /></asp:LinkButton>
																					<%}%>
																				</ContentTemplate>
																				<Triggers>
																					<asp:PostBackTrigger ControlID="lnkDownloadDocument" />
																				</Triggers>
																			</asp:UpdatePanel>
																		</ItemTemplate>
																	</asp:TemplateField>
																</Columns>
																<RowStyle CssClass="clsROWgrid" />
																<HeaderStyle CssClass="clsheadergrid" />
																<EmptyDataTemplate>
																	No Record Found
																</EmptyDataTemplate>
															</asp:GridView>
														</td>
													</tr>
												</table>
											</div>


											<%if (flag == "PR")
												{ %>
											<div id="submitdiv" style="margin-bottom: 7px;">
												<table style="width: 100%">
													<tr>
														<label id="lblStatus1" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
														<td style="width: 21%;">
															<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
															<label style="font-weight: bold; vertical-align: text-top;">Status</label>
														</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:RequiredFieldValidator Style="display: none" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Select Status"
																ValidationGroup="ReviewerComplianceValidationGroup" ControlToValidate="rdbtnStatus1"></asp:RequiredFieldValidator>
															<asp:HiddenField ID="hdnfieldPanaltyDisplay" runat="server" />
															<asp:RadioButtonList ID="rdbtnStatus1" runat="server" RepeatDirection="Horizontal">
																<asp:ListItem Text="Closed-Timely" Value="4"></asp:ListItem>
																<asp:ListItem Text="Closed-Delayed" Value="5"></asp:ListItem>
															</asp:RadioButtonList>
														</td>
													</tr>
													<tr>
														<div style="margin-bottom: 7px" id="divDated1" runat="server">
															<td style="width: 25%;">
																<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
																<label style="font-weight: bold; vertical-align: text-top;">Date</label>
															</td>
															<td style="width: 2%; font-weight: bold;">: </td>
															<td style="width: 83%;">
																<asp:TextBox runat="server" ID="tbxDate1" placeholder="DD-MM-YYYY" class="form-control" Style="width: 115px;" data-toggle="tooltip" data-placement="right" ToolTip="Compliance Completion Date" />
																<asp:RequiredFieldValidator ErrorMessage="Please Select Date" ControlToValidate="tbxDate1"
																	runat="server" ID="RequiredFieldValidator3" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
															</td>
														</div>
													</tr>
												</table>

												<table style="width: 100%">
													<tr>
														<td style="width: 25%;">
															<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
															<label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
														</td>
														<td style="width: 2%; font-weight: bold;">: </td>
														<td style="width: 83%;">
															<asp:TextBox runat="server" ID="tbxRemarks1" TextMode="MultiLine" class="form-control" Rows="2" />
														</td>
													</tr>
												</table>

												<div class="text-center" style="margin-bottom: 7px; margin-top: 10px;">
													<asp:Button Text="Submit" runat="server" ID="btnSave1" OnClick="btnSave1_Click" CssClass="btn btn-primary"
														ValidationGroup="ReviewerComplianceValidationGroup" data-toggle="tooltip" data-placement="bottom" />
													<asp:Button Text="Reject" runat="server" ID="btnReject1" Style="margin-left: 15px" OnClick="btnReject1_Click" CssClass="btn btn-primary"
														ValidationGroup="ReviewerComplianceValidationGroup" CausesValidation="false" Visible="false" />
													<asp:Button Text="Close" runat="server" OnClientClick="CloseAndBindData();" Style="margin-left: 15px" ID="btnClose" CssClass="btn btn-primary" data-dismiss="modal" />
												</div>
												<%--<asp:Label ID="lblNote" runat="server" Text="Note-Please download the attached document to verify and then you can able to change the status." Style="font-size: 12px; color: #666;"></asp:Label>--%>
												<asp:Label ID="lblNote" runat="server" Text="Note-Please make sure that at least one or more compliance document (i.e. Type-Compliance) should be there before compliance closure" Style="font-size: 12px; color: #666;"></asp:Label>
											</div>
											<% } %>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="Log1" class="row Dashboard-white-widget">
						<div class="dashboard">
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel panel-default" style="margin-bottom: 1px;">
										<div class="panel-heading">
											<a data-toggle="collapse" data-parent="#accordion" href="#AuditLog1">
												<h2>Audit Log</h2>
											</a>
											<div class="panel-actions">
												<a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#AuditLog1"><i class="fa fa-chevron-up"></i></a>
											</div>
										</div>
									</div>
									<div id="AuditLog1" class="collapse">
										<div runat="server" id="log" style="text-align: left;">
											<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
												<div style="margin-bottom: 7px; clear: both; margin-top: 10px">
													<asp:GridView runat="server" ID="grdTransactionAuditLog" AutoGenerateColumns="false" AllowSorting="true"
														AllowPaging="true" PageSize="5" GridLines="none" OnPageIndexChanging="grdTransactionAuditLog_OnPageIndexChanging"
														OnRowCreated="grdTransactionAuditLog_RowCreated" BorderWidth="0px" CssClass="table" OnSorting="grdTransactionAuditLog_Sorting"
														DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionAuditLog_RowCommand">
														<Columns>
															<asp:TemplateField HeaderText="Sr" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
																<ItemTemplate>
																	<%#Container.DataItemIndex+1 %>
																</ItemTemplate>
															</asp:TemplateField>

															<asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />

															<asp:BoundField DataField="Status" HeaderText="Status" />

															<asp:TemplateField HeaderText="Date">
																<ItemTemplate>
																	<%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
																</ItemTemplate>
															</asp:TemplateField>

															<asp:BoundField DataField="Interest" HeaderText="Interest" Visible="false" />
															<asp:BoundField DataField="Penalty" HeaderText="Penalty" Visible="false" />
															<asp:BoundField DataField="Remarks" HeaderText="Remarks" />
														</Columns>
														<PagerStyle HorizontalAlign="Right" />
														<PagerTemplate>
															<table style="display: none">
																<tr>
																	<td>
																		<asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
																	</td>
																</tr>
															</table>
														</PagerTemplate>
													</asp:GridView>
												</div>
											</fieldset>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
					<asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnDownload" />
				<asp:PostBackTrigger ControlID="btnSave1" />
				<asp:PostBackTrigger ControlID="lbDownloadSample1" />
			</Triggers>
		</asp:UpdatePanel>

		<div>
			<div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
				<div class="modal-dialog" style="width: 100%">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss-modal="DocumentReviewPopUp1" aria-hidden="true">×</button>
						</div>
						<div class="modal-body" style="height: 570px;">
							<div style="width: 100%;">
								<div style="float: left; width: 10%">
									<table width="100%" style="text-align: left; margin-left: 5%;">
										<thead>
											<tr>
												<td valign="top">
													<asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
														<ContentTemplate>
															<asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
																OnItemDataBound="rptComplianceVersionView_ItemDataBound">
																<HeaderTemplate>
																	<table id="tblComplianceDocumnets">
																		<thead>
																			<th>Versions</th>
																		</thead>
																</HeaderTemplate>
																<ItemTemplate>
																	<tr>
																		<td>
																			<asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
																				<ContentTemplate>
																					<asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
																						runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
																				</ContentTemplate>
																				<Triggers>
																					<asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
																				</Triggers>
																			</asp:UpdatePanel>
																		</td>
																	</tr>
																</ItemTemplate>
																<FooterTemplate>
																	</table>
																</FooterTemplate>
															</asp:Repeater>
														</ContentTemplate>
														<Triggers>
															<asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
														</Triggers>
													</asp:UpdatePanel>
												</td>
											</tr>
										</thead>
									</table>
								</div>
								<div style="float: left; width: 90%">
									<asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
										<ContentTemplate>
											<asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
										</ContentTemplate>
									</asp:UpdatePanel>
									<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
										<iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>

				<%--<div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="550px"></iframe>
                </div>
            </div>
        </div>--%>
			</div>
		</div>

		<div class="modal fade" id="SampleFileReviewPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
			<div class="modal-dialog" style="width: 100%;">
				<div class="modal-content">
					<div class="modal-header">
						<%-- data-dismiss-modal="modal2"--%>
						<button type="button" class="close" onclick="$('#SampleFileReviewPopUp').modal('hide');" aria-hidden="true">×</button>
					</div>
					<div class="modal-body" style="height: 570px;">
						<%-- <div style="width: 100%;">--%>
						<div style="float: left; width: 10%">
							<table width="100%" style="text-align: left; margin-left: 5%;">
								<thead>
									<tr>
										<td valign="top">
											<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
												<ContentTemplate>
													<asp:Repeater ID="rptComplianceSampleView1" runat="server" OnItemCommand="rptComplianceSampleView1_ItemCommand"
														OnItemDataBound="rptComplianceSampleView1_ItemDataBound">
														<HeaderTemplate>
															<table id="tblComplianceDocumnets1">
																<thead>
																	<th>Sample Forms</th>
																</thead>
														</HeaderTemplate>
														<ItemTemplate>
															<tr>
																<td>
																	<asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
																		<ContentTemplate>
																			<asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
																				runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
																		</ContentTemplate>
																		<Triggers>
																			<asp:AsyncPostBackTrigger ControlID="lblSampleView" />
																		</Triggers>
																	</asp:UpdatePanel>
																</td>
															</tr>
														</ItemTemplate>
														<FooterTemplate>
															</table>
														</FooterTemplate>
													</asp:Repeater>
												</ContentTemplate>
												<Triggers>
													<asp:AsyncPostBackTrigger ControlID="rptComplianceSampleView1" />
												</Triggers>
											</asp:UpdatePanel>
										</td>
									</tr>
								</thead>
							</table>
						</div>
						<div style="float: left; width: 90%">
							<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
								<iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
							</fieldset>
						</div>
						<%-- </div>--%>
					</div>
				</div>
			</div>
		</div>

		<div>
			<div class="modal fade" id="modalDocumentReviewerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
				<div class="modal-dialog" style="width: 100%">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
						</div>
						<div class="modal-body" style="height: 570px;">
							<div style="width: 100%;">
								<div style="float: left; width: 10%">
									<table width="100%" style="text-align: left; margin-left: 5%;">
										<thead>
											<tr>
												<td valign="top">
													<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
														<ContentTemplate>
															<asp:Repeater ID="rptTaskVersionView" runat="server" OnItemCommand="rptTaskVersionView_ItemCommand">
																<HeaderTemplate>
																	<table id="tblComplianceDocumnets">
																		<thead>
																			<th>Versions</th>
																		</thead>
																</HeaderTemplate>
																<ItemTemplate>
																	<tr>
																		<td>
																			<asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
																				<ContentTemplate>
																					<asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblTaskDocumentVersionView"
																						runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
																				</ContentTemplate>
																				<Triggers>
																					<asp:AsyncPostBackTrigger ControlID="lblTaskDocumentVersionView" />
																				</Triggers>
																			</asp:UpdatePanel>
																		</td>
																	</tr>
																</ItemTemplate>
																<FooterTemplate>
																	</table>
																</FooterTemplate>
															</asp:Repeater>
														</ContentTemplate>
														<Triggers>
															<asp:AsyncPostBackTrigger ControlID="rptTaskVersionView" />
														</Triggers>
													</asp:UpdatePanel>
												</td>
											</tr>
										</thead>
									</table>
								</div>
								<div style="float: left; width: 90%">
									<asp:Label runat="server" ID="lblMessagetask" Style="color: red;"></asp:Label>
									<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
										<iframe src="about:blank" id="docTaskViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="DocumentViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
			<div class="modal-dialog" style="width: 100%">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" onclick="$('#DocumentViewer').modal('hide');" aria-hidden="true">×</button>
					</div>
					<div class="modal-body" style="height: 570px;">
						<div style="width: 100%;">
							<fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
								<iframe src="about:blank" id="iframeDocViewer" runat="server" width="100%" height="100%"></iframe>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
