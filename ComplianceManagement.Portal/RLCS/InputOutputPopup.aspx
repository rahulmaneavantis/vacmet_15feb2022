﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InputOutputPopup.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.InputOutputPopup" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew_V1.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme_V1.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <link href="../NewCSS/kendo_V1.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo_V1.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            var ComplianceId = '<% =this.ComplianceID_%>';
            var ComplianceType = '<% =this.ComplianceType_%>';
            var ReturnRegisterChallanID = '<% =this.ReturnRegisterChallanID_%>';
            var MonthId = '<% =this.MonthID_%>';
            var Year = '<% =this.Year_%>';
            var InputID = '<% =this.InputID_%>';
            if (ComplianceId != '') {

            }
        });

        function downloaderrorfile(e) {
            window.location.replace("DownloadErrorFile.aspx?fn=" + $(e).attr("filename"));
        }

        function hideDivBranch() {
            $('#divFilterLocation').hide("blind", null, 0, function () { });
        }
        function checkAllPeriods(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function checkAllFileInput(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkFileInput") > -1) {
                    cbox.checked = cb.checked;

                }
            }
        }
        function UncheckFileInputHeader() {

            var rowCheckBox = $("#RepeaterTable1 input[id*='chkFileInput']");
            var rowCheckBoxSelected = $("#RepeaterTable1 input[id*='chkFileInput']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable1 input[id*='FileInputSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function UncheckPeriodHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkPeriod']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkPeriod']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='PeriodSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }


        function iteratePages(object) {
            var str = "";
            if (object !== null && object.length > 0) {
                for (var x = 0; x < object.length; x++) {
                    str += "<ul style='margin-top: 5px;margin-left: -35px;'>" + object[x].PageName + "</ul>";
                }
            }
            return str;
        }///<img src="../img/icons/upload_pending1.png" />
        function iteratePages1(object) {
            var str = "";
            if (object !== null && object.length > 0) {
                for (var x = 0; x < object.length; x++) {
                    if (object[x].IsUploaded == "0") {
                        str += "<div class='customer-photo'" +
                           "style='background-image: url(../img/icons/upload_empty.png);margin-top: 0px;'></div>"
                    }
                    else if (object[x].IsUploaded == "1") {
                        str += "<div class='customer-photo'" +
                             "style='background-image: url(../img/icons/upload_success.png);margin-top: 0px;'></div>"
                    }
                    else {
                        str += "<div class='customer-photo'" +
                            "style='background-image: url(../img/icons/upload_error.png);margin-top: 0px;'></div>"

                    }

                }
            }
            return str;
        }
        function iteratePages2(object) {
            var str = "";
            if (object !== null && object.length > 0) {
                for (var x = 0; x < object.length; x++) {
                    if (object[x].IsValidated == "0") {
                        str += "<div class='customer-photo'" +
                            "style='background-image: url(../img/icons/upload_empty.png);margin-top: 0px;'></div>"
                    }
                    else if (object[x].IsValidated == "1") {
                        str += "<div class='customer-photo'" +
                             "style='background-image: url(../img/icons/upload_success.png);margin-top: 0px;'></div>"
                    }
                    else {
                        str += "<a onclick='downloaderrorfile(this)' style='cursor:pointer;' filename='"+ object[x].ErrorFileName +"'><div class='customer-photo'" +
                            "style='background-image: url(../img/icons/upload_error.png);margin-top: 0px;'></div></a>"

                    }
                }
            }
            return str;
        }
        function iteratePages3(object) {
            var str = "";
            if (object !== null && object.length > 0) {
                for (var x = 0; x < object.length; x++) {
                    if (object[x].IsProcessed == "0") {
                        str += "<div class='customer-photo'" +
                           "style='background-image: url(../img/icons/upload_empty.png);margin-top: 0px;'></div>"
                    }
                    else if (object[x].IsProcessed == "1") {
                        str += "<div class='customer-photo'" +
                             "style='background-image: url(../img/icons/upload_success.png);margin-top: 0px;'></div>"
                    }
                    else {
                        str += "<div class='customer-photo'" +
                            "style='background-image: url(../img/icons/upload_error.png);margin-top: 0px;'></div>"

                    }
                }
            }
            return str;
        }
        function initializeJQueryUIDeptDDL() {
            $("#<%= txtPeriodList.ClientID %>").unbind('click');
            $("#<%= txtPeriodList.ClientID %>").click(function () {
                $("#dvPeriod").toggle("blind", null, 100, function () { });
            });
            $("#<%= txtFileInputList.ClientID %>").unbind('click');
            $("#<%= txtFileInputList.ClientID %>").click(function () {
                $("#dvFileInput").toggle("blind", null, 100, function () { });
            });
        }
        
        $(document).on("click", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tvFilterLocation.ClientID %>') {
                $('<%= tvFilterLocation.ClientID %>').unbind('click');

                $('<%= tvFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

/////////////////Kendo//////////////
        function bindTreeListGrid1(CustomerID, ComplianceType, MonthId, Year, LoginUserID, JSONID) {
    // var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputToOutputBranchList?CustomerID=" + CustomerID + "BranchID=" + BranchId + "InputId=" + InputId + "ComplianceType=" + ComplianceType + "ActGroup=" + ActGroup + "ComplianceID=" + ComplianceID + "ReturnRegisterChallanID=" + ReturnRegisterChallanID + "DocumentType=" + DocumentType + "Month=" + Month + "Year=" + Year + "Period=" + Period;
   //var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputToOutputBranchList?CustomerID=" + CustomerID + "&BranchID=" + BranchId + "&InputId=" + InputId + "&ComplianceType=" + ComplianceType + "&ActGroup=" + ActGroup + "&ComplianceID=" + ComplianceID + "&ReturnRegisterChallanID=" + ReturnRegisterChallanID + "&DocumentType=" + DocumentType + "&MonthId=" + MonthId + "&Year=" + Year + "&Period=" + Period + "&UserID=" + LoginUserID;
            var crudServiceBaseUrl = "<%= avacomAPIUrl %>GetAllFileInputToOutputBranchList?CustomerID=" + CustomerID + "&ComplianceType=" + ComplianceType + "&MonthId=" + MonthId + "&Year=" + Year +"&UserID=" + LoginUserID + "&JSONID=" + JSONID;
      
    var initialLoad = true;
    $("#grid").kendoGrid({
        dataSource: {
            transport: {
                read: function (options) {
                            $.ajax({
                                type: 'POST',
                                url: crudServiceBaseUrl,
                                //data: IOJSON,
                                data: {},
                                dataType: "json", // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                                processData: true,
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                success: function (result) {
                                    // notify the data source that the request succeeded
                                    options.success(result);
                                },
                                error: function (result) {
                                    // notify the data source that the request failed
                                    options.error(result);
                                }
                            });
                        },

            },

            schema: {
                model: {

                    id: "ComplianceID",
                    id: "ReturnRegisterChallanID",
                    id: "InputID",
                },
                data: function (response) {
                    if (response != null && response != undefined) {
                        return response.Result;
                    }
                },
                total: function (response) {
                    if (response.Result != null && response.Result != undefined) {
                        if (response.Result.length > 0) {

                        }
                        return response.Result.length;
                    }
                }
            }

        },
        columns: [

            { field: "CustomerBranchID", hidden: true, },
            { field: "StateName", title: 'State', width: "10%;", attributes: { style: "vertical-align: top;text-align:left;border-width:0px;" } },
            {
                field: "BranchName", title: 'Branch Name', width: "15%;", attributes: { style: "vertical-align: top;text-align:left;" }//, attributes: { "class": "table-cell", style: "vertical-align: top;" }
            },
            { field: "StateID", title: 'StateID', hidden: true },
            { field: "AddressLine1", title: 'Address', width: "15%;", hidden: true },
            { field: "lstPages", title: "Input File ", template: "#= iteratePages(data.lstPages)#", width: "30%;", attributes: { style: "font-color: black;text-align:left;" } },
            { field: "lstPages", title: "Upload", headerAttributes: { style: "text-align: left;" }, template: "#= iteratePages1(data.lstPages)#", width: "6%;", attributes: { style: "font-color: black;text-align:left;" } },
            { field: "lstPages", title: "Validate", headerAttributes: { style: "text-align:left;" }, template: "#= iteratePages2(data.lstPages)#", width: "6%;", attributes: { style: "font-color: black;text-align:left;" } },
            { field: "lstPages", title: "Process", headerAttributes: { style: "text-align:left;" }, template: "#= iteratePages3(data.lstPages)#", width: "6%;", attributes: { style: "font-color: black;text-align:left;" } }

        ]

    });
}
function DivClick(e, id) {
    var ddlFileType = $('#ddlFileType').val();
    if (ddlFileType == "Other") {
        var a = e.innerText;
        $('#hdnInputId').val(id);
        $('#txtFileInputList').val(a);
        $('#btnFileSelectnOther').click();
        //alert(id);

    }

}
$(document).ready(function () {

    ////Panel 
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        //    // re-bind your jQuery events here
        $("#ddlYear").change(function () {
            $('#btnFileInput').click();
        });

        var ddlFileType = $('#ddlFileType').val();
        if (ddlFileType == "Other") {
            $('#divSelectAll').hide();
        }
        var btnSubmitVal = $('#hdnSubmitbtn').val();
        if (btnSubmitVal == "Submit") {
            var LoginUserID = '<% =this.LoginUserID%>';

                    var CustomerID = $('#hdnCustID').val();
                    var BranchId = $('#hdnBranchId').val();
                    //var InputId = $('#hdnInputId').val();
                    var ComplianceType = $('#hdnComplianceType').val();
                    //var ActGroup = $('#hdnActGroup').val();
                    //var ComplianceID = $('#hdnComplianceID').val();
                    //var ReturnRegisterChallanID = $('#hdnReturnRegisterChallanID').val();
                    //var DocumentType = $('#hdnDocumentType').val();
                    var Month = $('#hdnMonth').val();
                    var Year = $('#hdnYear').val();
                    //var Period = $('#hdnPeriod').val();
                    var JSONID = $('#hdnIOJSON').val();
                   
                    bindTreeListGrid1(CustomerID, ComplianceType, Month, Year, LoginUserID, JSONID);
                    //bindTreeListGrid1(CustomerID, BranchId, InputId, ComplianceType, ActGroup, ComplianceID, ReturnRegisterChallanID, DocumentType, Month, Year, Period, LoginUserID, IOJSON);
                    

                }

            });
        });


    </script>
    <style type="text/css">
        .customer-photo {
            display: inline-block;
            width: 24px;
            height: 24px;
            border-radius: 50%;
            background-size: 24px 24px;
            background-position: center center;
            vertical-align: middle;
            line-height: 24px;
            /*box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);*/
            margin-left: 30px;
        }

        .customer-name {
            display: inline-block;
            vertical-align: middle;
            line-height: 32px;
            padding-left: 3px;
        }

        .panel-body {
            padding: 1px;
            margin-top: 10px;
        }
        .btn{
            font-weight:400;
            height: 33px;
        }
    </style>
    <style type="text/css">
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
           top:0px;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
                border-bottom-width: 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-grid-content {
            min-height: 32px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-grid-header th {
            height: 15px;
        }

        .Dashboard-white-widget {
            padding: 2px 10px 0px;
            margin-bottom: 0px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0 0 0px 0px; 
            padding-top: 1px;
            padding-bottom: 1px;

           
        }
         .k-filter-row th, .k-grid-header th[data-index='1'] {
            border-width: 0px 0px 0px 0px;
         }
        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 0px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 0px;
        }

        #AdavanceSearch {
            border: .5px solid #dcdcdf;
            background-color: #f5f5f6;
        }

        .clearfix {
            height: 9px !important;
        }

        .container {
            max-width: 100% !important;
        }
        /*.k-grid-content{
            height:395px !important;
        }*/
        .sidebar-menu {
            margin-top: 53px !important;
        }
          .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 0px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 30px;
            vertical-align: middle;
        }
          .chosen-container-single .chosen-single div{
              margin-top:3px;
          }
          .k-alt, .k-pivot-layout > tbody > tr:first-child > td:first-child, .k-resource.k-alt, .k-separator {
    background-color: #fbfbfb;
    background-color: white;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDocGenPopup_Load">
            <ContentTemplate>
                <div class="col-md-12 colpadding0">
                    <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                    <asp:ValidationSummary ID="vsDocGen1" runat="server" class="alert alert-block alert-success fade in" ValidationGroup="success" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="DocGenValidationGroup" Display="None" />
                    <asp:CustomValidator ID="cvDuplicateEntrysucess" runat="server" EnableClientScript="False"
                        ValidationGroup="success" Display="None" />
                </div>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:Button ID="btnFileInput" runat="server" Text="" OnClick="btnFileInput_Click" CssClass="hidden" />
                <asp:Button ID="btnFileSelectnOther" runat="server" Text="" OnClick="btnFileSelectnOther_Click" CssClass="hidden" />
                <asp:HiddenField ID="hdnSubmitbtn" runat="server" Value="" />
                <asp:HiddenField ID="hdnCustID" runat="server" />
                <asp:HiddenField ID="hdnBranchId" runat="server" Value="" />
                <asp:HiddenField ID="hdnInputId" runat="server" Value="" />
                <asp:HiddenField ID="hdnComplianceType" runat="server" Value="" />
                <asp:HiddenField ID="hdnActGroup" runat="server" Value="" />
                <asp:HiddenField ID="hdnComplianceID" runat="server" Value="" />
                <asp:HiddenField ID="hdnReturnRegisterChallanID" runat="server" Value="" />
                <asp:HiddenField ID="hdnDocumentType" runat="server" Value="" />
                <asp:HiddenField ID="hdnMonth" runat="server" Value="" />
                <asp:HiddenField ID="hdnYear" runat="server" Value="0" />
                <asp:HiddenField ID="hdnPeriod" runat="server" Value="" />
                <asp:HiddenField ID="hdnFileType" runat="server" Value="" />
                <asp:HiddenField ID="hdnIOJSON" runat="server" Value="" />


                <div class="container">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="col-md-12 colpadding0">
                            <div class="panel-body body colpadding0">

                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <asp:Label Text="Input File Type" runat="server" CssClass="control-label" />

                                    <asp:DropDownList ID="ddlFileType" runat="server" Width="88%" CssClass="form-control" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="--Select--" Value="" />
                                        <asp:ListItem Text="Monthly" Value="Monthly" />
                                        <asp:ListItem Text="Annual" Value="Annual" />
                                        <asp:ListItem Text="Other" Value="Other" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2 colpadding0" style="width: 15%">
                                    <asp:Label Text=" Input File Selection" runat="server" CssClass="control-label" />

                                    <asp:TextBox runat="server" ID="txtFileInputList" Width="90%" Style="padding: 0px; margin: 0px; height: 34px;" CssClass="txtbox form-control" AutoComplete="off" />
                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 150%;" id="dvFileInput" class="dvDeptHideshow form-control">
                                        <asp:Repeater ID="rptFileInput" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable1">
                                                    <tr>
                                                        <td style="width: 100px;" colspan="2">
                                                            <div id="divSelectAll">
                                                                <asp:CheckBox ID="FileInputSelectAll" Text="Select All" runat="server" onclick="checkAllFileInput(this)" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkFileInput" runat="server" onclick=" UncheckFileInputHeader();" /></td>
                                                    <td style="width: 90%;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; padding-bottom: 5px;">
                                                            <asp:Label ID="lblFileInputID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblFileInputName" runat="server" Style="display: none" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                            <div id="divfileinput" style="margin-top: 3px;" onclick="return DivClick(this,<%# Eval("ID")%>)"><%# Eval("Name")%></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>

                                    </div>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 13%">
                                    <asp:Label Text="Period" runat="server" CssClass="control-label" />

                                    <asp:TextBox runat="server" ID="txtPeriodList" Width="88%" Style="padding: 0px; margin: 0px; height: 34px;" CssClass="txtbox form-control" />
                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 89%;" id="dvPeriod" class="dvDeptHideshow form-control">
                                        <asp:Repeater ID="rptPeriodList" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width: 100px;" colspan="2">
                                                            <asp:CheckBox ID="PeriodSelectAll" Text="Select All" runat="server" onclick="checkAllPeriods(this)" />
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkPeriod" runat="server" onclick="UncheckPeriodHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 118px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblPeriodID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 15%">
                                    <asp:Label Text="Year" runat="server" CssClass="control-label" />

                                    <asp:DropDownListChosen runat="server" ID="ddlYear" Width="90%" class="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                        <asp:ListItem Text="Select" Value="0" />
                                        <asp:ListItem Text="2020" Value="2020" />
                                        <asp:ListItem Text="2019" Value="2019" />
                                        <asp:ListItem Text="2018" Value="2018" />
                                        <asp:ListItem Text="2017" Value="2017" />
                                        <asp:ListItem Text="2016" Value="2016" />
                                        <asp:ListItem Text="2015" Value="2015" />
                                        <asp:ListItem Text="2014" Value="2014" />
                                        <asp:ListItem Text="2013" Value="2013" />
                                        <asp:ListItem Text="2012" Value="2012" />
                                        <asp:ListItem Text="2011" Value="2011" />
                                        <asp:ListItem Text="2010" Value="2010" />
                                        <asp:ListItem Text="2009" Value="2009" />
                                        <asp:ListItem Text="2008" Value="2008" />
                                        <asp:ListItem Text="2007" Value="2007" />
                                        <asp:ListItem Text="2006" Value="2006" />
                                        <asp:ListItem Text="2005" Value="2005" />
                                        <asp:ListItem Text="2004" Value="2004" />
                                        <asp:ListItem Text="2003" Value="2003" />
                                        <asp:ListItem Text="2002" Value="2002" />
                                        <asp:ListItem Text="2001" Value="2001" />
                                        <asp:ListItem Text="2000" Value="2000" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0" style="width: 15%">
                                    <asp:Label Text="Compliance Type" runat="server" CssClass="control-label" />

                                    <asp:DropDownList ID="ddlComplianceType" Width="90%" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-md-3 colpadding0" style="width: 22%">
                                    <asp:Label Text="Entity" runat="server" CssClass="control-label" />

                                    <asp:TextBox runat="server" Width="90%" AutoComplete="off" ID="tbxFilterLocation"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 34px; width: 102%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" placeholder="Entity/State/Location/Branch" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: 87%; margin-top: -20px;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="105%" NodeStyle-ForeColor="#8e8e93" ShowCheckBoxes="All" onclick="OnTreeClick(event)"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important; max-height: 300px;" ShowLines="true">
                                        </asp:TreeView>
                                        <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>
                                        <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation1_Click" Text="select" CssClass="hidden" />
                                        <asp:Button ID="btnClear" Visible="true" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="hidden" />
                                    </div>

                                </div>

                                <div class="col-md-1 colpadding0" style="width: 7%; margin-top: 20px;">
                                    <asp:Label ID="lblapp" runat="server"></asp:Label>
                                    <asp:Button ID="btnsubmit" Text="Apply" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnsubmit_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 colpadding0" style="overflow-y: auto; max-height: 425px; margin-top: 10px;">
                                <div id="grid"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script>
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
            }
            function fCheckTree(obj) {
                var id = $(obj).attr('data-attr');
                var elm = $("#" + id);
                $(elm).trigger('click');
            }

            function FnSearch() {

                var tree = document.getElementById('BodyContent_tvFilterLocation');
                var links = tree.getElementsByTagName('a');
                var keysrch = document.getElementById('tvFilterLocation').value.toLowerCase();
                var keysrchlen = keysrch.length
                if (keysrchlen > 2) {
                    $('#bindelement').html('');
                    for (var i = 0; i < links.length; i++) {

                        var anch = $(links[i]);
                        var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                        var getId = $(anch).attr('id');
                        var parendNode = '#' + getId + 'Nodes';
                        var childanchor = $(parendNode).find('a');
                        if (childanchor.length == 0) {
                            if (twoletter > -1) {

                                var idchild = $($(anch).siblings('input')).attr('name');
                                // var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  name="' + getId + 'CheckBox" id="' + getId + 'CheckBox"><a id="' + getId + '" href="' + $(anch).attr('href') + '" onclick="' + $(anch).attr('onclick') + '" >' + anch.html() + '</a></br>';
                                var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                                $('#bindelement').append(createanchor);
                            }
                        }

                    }
                    $(tree).hide();
                    $('#bindelement').show();
                } else {
                    $('#bindelement').html('');
                    $('#bindelement').hide();
                    $(tree).show();
                }

            }
            function OnTreeClick(evt) {
                var src = window.event != window.undefined ? window.event.srcElement : evt.target;
                var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
                if (isChkBoxClick) {
                    var parentTable = GetParentByTagName("table", src);
                    var nxtSibling = parentTable.nextSibling;
                    if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                    {
                        if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                        {
                            //check or uncheck children at all levels
                            CheckUncheckChildren(parentTable.nextSibling, src.checked);
                        }
                    }
                    //check or uncheck parents at all levels
                    CheckUncheckParents(src, src.checked);
                }
            }
            function CheckUncheckChildren(childContainer, check) {
                var childChkBoxes = childContainer.getElementsByTagName("input");
                var childChkBoxCount = childChkBoxes.length;
                for (var i = 0; i < childChkBoxCount; i++) {
                    childChkBoxes[i].checked = check;
                }
            }
            function CheckUncheckParents(srcChild, check) {
                var parentDiv = GetParentByTagName("div", srcChild);
                var parentNodeTable = parentDiv.previousSibling;

                if (parentNodeTable) {
                    var checkUncheckSwitch;

                    if (check) //checkbox checked
                    {
                        var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                        if (isAllSiblingsChecked)
                            checkUncheckSwitch = true;
                        else
                            return; //do not need to check parent if any(one or more) child not checked
                    }
                    else //checkbox unchecked
                    {
                        checkUncheckSwitch = false;
                    }

                    var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                    if (inpElemsInParentTable.length > 0) {
                        var parentNodeChkBox = inpElemsInParentTable[0];
                        parentNodeChkBox.checked = checkUncheckSwitch;
                        //do the same recursively
                        CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                    }
                }
            }
            function AreAllSiblingsChecked(chkBox) {
                var parentDiv = GetParentByTagName("div", chkBox);
                var childCount = parentDiv.childNodes.length;
                for (var i = 0; i < childCount; i++) {
                    if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                    {
                        if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                            var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                            //if any of sibling nodes are not checked, return false
                            if (!prevChkBox.checked) {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            //utility function to get the container of an element by tagname
            function GetParentByTagName(parentTagName, childElementObj) {
                var parent = childElementObj.parentNode;
                while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                    parent = parent.parentNode;
                }
                return parent;
            }
        </script>
    </form>
</body>
</html>
