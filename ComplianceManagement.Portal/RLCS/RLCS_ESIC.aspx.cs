﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSAPISettings;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
	public partial class RLCS_ESIC : System.Web.UI.Page
	{
		protected static string CId;
		protected static string UId;
		protected static string ProfileID;
		protected static string avacomAPIUrl;
		protected static string RLCSAPIURL;
		protected static string Client_ID;
		protected static string SubCode;
		protected static string EmployeeID;
		protected static DateTime DOJ;
		protected static long ESICNumber;
		protected static DateTime RequestDate;
		HttpResponseMessage response;
		protected static string FromDate;
		protected static string ToDate;
		private HttpClient _httpClient { set; get; }
		public RLCS_ESIC()
		{
			 response = null;
			_httpClient = new HttpClient();
			_httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["RLCSAPIURL"]);
			_httpClient.Timeout = new TimeSpan(0, 15, 0);
			_httpClient.DefaultRequestHeaders.Accept.Clear();
			_httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			_httpClient.DefaultRequestHeaders.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
			_httpClient.DefaultRequestHeaders.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");
			_httpClient.DefaultRequestHeaders.Add("RLCSWEBREQUEST", "true");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
	}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
				//TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];
				RLCSAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
				CId = Convert.ToString(AuthenticationHelper.CustomerID);
				UId = Convert.ToString(AuthenticationHelper.UserID);
				ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
			}
			if (!string.IsNullOrEmpty(Request.QueryString["Type"]) && Request.QueryString["Type"] == "D")
			{
				if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]) && !string.IsNullOrEmpty(Request.QueryString["SubCode"]) && !string.IsNullOrEmpty(Request.QueryString["EmployeeID"]) && !string.IsNullOrEmpty(Request.QueryString["DOJ"]) && !string.IsNullOrEmpty(Request.QueryString["ESICNumber"]) && !string.IsNullOrEmpty(Request.QueryString["RequestDate"]))
				{
					Client_ID = Request.QueryString["ClientID"];
					SubCode = Request.QueryString["SubCode"];
					EmployeeID = Request.QueryString["EmployeeID"];
					DOJ = Convert.ToDateTime(Request.QueryString["DOJ"]);
					RequestDate = Convert.ToDateTime(Request.QueryString["RequestDate"]);
					ESICNumber = Convert.ToInt64(Request.QueryString["ESICNumber"].ToString());
					bool response = GetRLCSESICDocuments(Client_ID, SubCode, EmployeeID, DOJ, RequestDate, ESICNumber);
					if(!response)
					ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Document Not avaliable');", true);
				}
			}
			if (!string.IsNullOrEmpty(Request.QueryString["Type"]) && Request.QueryString["Type"] == "E")
			{
				if (!string.IsNullOrEmpty(Request.QueryString["ClientID"]) && !string.IsNullOrEmpty(Request.QueryString["SubCode"]) && !string.IsNullOrEmpty(Request.QueryString["FromDate"]))
				{
					Client_ID = Request.QueryString["ClientID"];
					SubCode = Request.QueryString["SubCode"];
					FromDate= Request.QueryString["FromDate"];
					lbtnExportExcel_Click(Client_ID, SubCode, FromDate);
				}

			}
		}
		public  bool GetRLCSESICDocuments(string Client_ID, string SubCode, string EmployeeID, DateTime DOJ, DateTime RequestDate, long ESICNumber)
		{
			bool documentSuccess = true;
			string contentType = string.Empty;
			string filename = string.Empty;
			int month = RequestDate.Month;
			int year = RequestDate.Year;
			string month_year = month + "_" + year;
			string baseAddress = ConfigurationManager.AppSettings["ESICFilePath"].ToString();
			string empId_ESICNumber = EmployeeID + "_" + ESICNumber.ToString();
			try
			{
				string filePath = baseAddress + Client_ID + "/" + month_year + "/" + SubCode + "/" + empId_ESICNumber + ".pdf";
				
				HttpResponse res = HttpContext.Current.Response;
				response = _httpClient.GetAsync("Masters/Generate?FilePath=" + filePath).Result;
				var statuscode = Convert.ToInt32(response.StatusCode);
				if (statuscode == 200)
				{
					var byteFile = response.Content.ReadAsByteArrayAsync().Result;
					if (byteFile.Length > 0)
					{
						string extension = Path.GetExtension(filePath);
						contentType = "application/" + extension.ToString().Replace(".", "").ToLower() + "";
						filename = empId_ESICNumber + extension;

						res.Buffer = true;
						res.ClearContent();
						res.ClearHeaders();
						res.Clear();
						res.ContentType = contentType;
						res.AddHeader("content-disposition", "attachment; filename=" + empId_ESICNumber +  extension);
						res.BinaryWrite(byteFile);
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest();
					}
				}
				else { documentSuccess = false; }



				//if (File.Exists(filePath))
				//{
				//	HttpResponse response = HttpContext.Current.Response;
				//	response.Buffer = true;
				//	response.ClearContent();
				//	response.ClearHeaders();
				//	response.Clear();
				//	response.ContentType = "application/octet-stream";
				//	response.AddHeader("content-disposition", "attachment; filename=" + "ESIC" + "-Document-" + DateTime.Now.ToString("ddMMyy") + ".pdf");
				//	response.TransmitFile(filePath);
				//	response.Flush(); // Sends all currently buffered output to the client.
				//	response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
				//	HttpContext.Current.ApplicationInstance.CompleteRequest();
				//	response.End();
				//}
				//else { documentSuccess = false; }
			}
			catch (Exception ex)
			{
				documentSuccess = false;
			}
			return documentSuccess;
		}
		protected void lbtnExportExcel_Click(string Client_ID, string subcode,string fromdate)
		{
			try
			{

				response = _httpClient.GetAsync("api/Masters/GetESICDetails?EntityId=" + Client_ID + "&SubCode=" + subcode + "&FromDate=" + fromdate + "&ToDate=" + fromdate).Result;
				var records = response.Content.ReadAsAsync<List<RLCSApiResponse>>().Result;

				using (ExcelPackage exportPackge = new ExcelPackage())
				{

					string filterType, compType = string.Empty;

					filterType = "sheet";

					using (ComplianceDBEntities entities = new ComplianceDBEntities())
					{
						entities.Database.CommandTimeout = 180;

						ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(filterType);
						DataTable ExcelData = null;
						DataView view = new System.Data.DataView(records.ToDataTable());
						ExcelData = view.ToTable("Selected", false, "EmpId", "EmpName", "DOJ", "CardGeneratedDate", "SubCode", "IPNumber");

						exWorkSheet.Cells["A2"].LoadFromDataTable(ExcelData, false);
						exWorkSheet.Cells["A1"].Style.Font.Bold = true;
						exWorkSheet.Cells["A1"].Style.Font.Size = 12;
						exWorkSheet.Cells["A1"].Value = "EmplpyeeID";
						
						exWorkSheet.Cells["A1"].AutoFitColumns(20);

						exWorkSheet.Cells["B1"].Style.Font.Bold = true;
						exWorkSheet.Cells["B1"].Style.Font.Size = 12;
						exWorkSheet.Cells["B1"].Value = "EmployeeName";
						exWorkSheet.Cells["B1"].AutoFitColumns(35);

						exWorkSheet.Cells["C1"].Style.Font.Bold = true;
						exWorkSheet.Cells["C1"].Style.Font.Size = 12;
						exWorkSheet.Cells["C1"].Value = "DOJ";
						exWorkSheet.Cells["C1"].AutoFitColumns(20);

						exWorkSheet.Cells["D1"].Style.Font.Bold = true;
						exWorkSheet.Cells["D1"].Style.Font.Size = 12;
						exWorkSheet.Cells["D1"].Value = "CardGeneratedDate";
						exWorkSheet.Cells["D1"].AutoFitColumns(20);

						exWorkSheet.Cells["E1"].Style.Font.Bold = true;
						exWorkSheet.Cells["E1"].Style.Font.Size = 12;
						exWorkSheet.Cells["E1"].Value = "SubCode";
						exWorkSheet.Cells["E1"].AutoFitColumns(30);

						exWorkSheet.Cells["F1"].Style.Font.Bold = true;
						exWorkSheet.Cells["F1"].Style.Font.Size = 12;
						exWorkSheet.Cells["F1"].Value = "ESICNumber";
						exWorkSheet.Cells["F1"].AutoFitColumns(30);



						using (ExcelRange col = exWorkSheet.Cells[2, 3, 1 + ExcelData.Rows.Count, 4])
						{
							col.Style.WrapText = true;
							col.Style.Numberformat.Format = "dd/MMM/yyyy";
							col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
							col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

							//col.AutoFitColumns();

							// Assign borders
							//col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
							//col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
							//col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
							//col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
						}
						using (ExcelRange col = exWorkSheet.Cells[2, 5, 1 + ExcelData.Rows.Count, 6])
						{
							col.Style.WrapText = true;
							col.Style.Numberformat.Format = "#";
							col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
							col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

							//col.AutoFitColumns();

							// Assign borders
							//col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
							//col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
							//col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
							//col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
						}

						string filename = "";

						filename = "ESICDetails.xlsx";

						Byte[] fileBytes = exportPackge.GetAsByteArray();
						Response.ClearContent();
						Response.Buffer = true;
						Response.AddHeader("content-disposition", "attachment;filename=" + filename);
						Response.Charset = "";
						Response.ContentType = "application/vnd.ms-excel";
						StringWriter sw = new StringWriter();
						Response.BinaryWrite(fileBytes);
						HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
						HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
						HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    


					}
				}
			}
			catch (Exception ex)
			{
				LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
			}
		}

	}
	public class RLCSApiResponse
	{
		public int ID { get; set; }
		public string ClientId { get; set; }
		public string ClientName { get; set; }
		public string EmpId { get; set; }
		public string EmpName { get; set; }
		public DateTime DOJ { get; set; }
		public long SubCode { get; set; }
		public long IPNumber { get; set; }
		public DateTime RequestDate { get; set; }
		public string CardGeneratedDate { get; set; }
	}

}