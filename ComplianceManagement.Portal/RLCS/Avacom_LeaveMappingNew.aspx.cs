﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Microsoft.IdentityModel.Protocols;
using Newtonsoft.JsonResult;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class Avacom_LeaveMappingNew : System.Web.UI.Page
    {
        protected static string clientID = String.Empty;
        protected static int CustId;
        protected static string Path;
        protected static string searchedValue= String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            if (ddlClientList.SelectedIndex == 0)
            {
                clientID = String.Empty;
            }
            else
            {
                clientID = ddlClientList.SelectedValue;
                //  searchedValue = txtSearch.Value;
            }
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    CustId = Convert.ToInt32(Request.QueryString["CustomerID"]);

                }
                //USE CustId to bind Client DDL
                BindClients(Convert.ToInt32(CustId));

            }
        }

        private void BindClients(int CustomerID)
        {
            try
            {
                ddlClientList.DataTextField = "Name";
                ddlClientList.DataValueField = "ID";
                var list = GetAll_Entities(CustomerID);
                ddlClientList.DataSource = list;
                ddlClientList.DataBind();
                ddlClientList.Items.Insert(0, "Select Client");

            }
            catch (Exception ex)
            {

            }
        }
        public static List<object> GetAll_Entities(int CustomerID)
        {
            List<object> EntityList = new List<object>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    EntityList = (
                              from RCRM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                              join CB in entities.CustomerBranches on RCRM.AVACOM_BranchID equals CB.ID
                              where RCRM.BranchType == "E"
                              && RCRM.CM_Status == "A"
                              && RCRM.IsProcessed == true
                              && RCRM.AVACOM_CustomerID == CustomerID
                              && CB.IsDeleted == false
                              orderby RCRM.CM_ClientName ascending
                              select new
                              {
                                  ID = RCRM.CM_ClientID,
                                  Name = RCRM.CM_ClientName
                              }).Distinct().ToList<object>();

                    return EntityList;
                }
            }
            catch (Exception ex)
            {

            }
            return EntityList;
        }
        protected void ddlClientList_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlClientList.SelectedIndex == 0)
            {
                bulkUploadBtn.Visible = false;
                searchtype.Visible = false;
                clientID = "";
            }
            else if (ddlClientList.SelectedIndex != 0)
            {
                bulkUploadBtn.Visible = true;
                searchtype.Visible = true;
                clientID = ddlClientList.SelectedValue;
            }
        }

        [WebMethod]
        public static string SaveLeaveMappingDetails(Leave_MappingDetails DetailsObj)
        {
            bool Success = false;
            // bool APISuccess = false;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            // var c = DetailsObj.clientCode;
            //  var l = DetailsObj.LeaveType;
            RLCS_LeaveTypeMaster_Mapping _mappingObj = new RLCS_LeaveTypeMaster_Mapping();

            try
            {
                using (ComplianceDBEntities db = new ComplianceDBEntities())
                {
                    _mappingObj = new RLCS_LeaveTypeMaster_Mapping()
                    {
                        ClientID = clientID,
                        AVACOM_LeaveType = DetailsObj.LeaveType,
                        LeaveType = DetailsObj.clientCode,
                        IsActive = true
                    };
                    var isExisting = db.RLCS_LeaveTypeMaster_Mapping.FirstOrDefault(x => x.AVACOM_LeaveType.Equals(DetailsObj.LeaveType) && x.ClientID.Equals(clientID));
                    //Update Code
                    if (isExisting != null)
                    {
                        db.RLCS_LeaveTypeMaster_Mapping.Remove(isExisting);
                        db.SaveChanges();
                        db.RLCS_LeaveTypeMaster_Mapping.Add(_mappingObj);
                        db.SaveChanges();
                        Success = true;
                    }
                    else
                    {
                        db.RLCS_LeaveTypeMaster_Mapping.Add(_mappingObj);
                        db.SaveChanges();
                       // Success = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return serializer.Serialize(Success);
        }
    }
    public class Leave_MappingDetails
    {
        public string clientCode { get; set; }
        public string LeaveType { get; set; }

    }
}